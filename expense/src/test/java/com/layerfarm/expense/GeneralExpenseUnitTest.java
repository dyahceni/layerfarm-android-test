package com.layerfarm.expense;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.ExpenseGeneral;
import com.layerfarm.layerfarm.model.ExpenseGeneralList;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.ParameterCreateGeneralExpense;
import com.layerfarm.layerfarm.model.ParameterDeleteGeneralExpense;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.text.ValuePrinter;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import okhttp3.JavaNetCookieJar;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.matchers.text.ValuePrinter.print;

public class GeneralExpenseUnitTest {
    //    private Handler mHandler = mock(Handler.class);
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String nid;
    private String token2;
    private String baseUrl = "http://"+"nmcgyj"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void ConnectServer(){
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            ValuePrinter.print("tokenn1 = "+tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                ValuePrinter.print(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            System.out.println("uid = "+connection.getUser().getUid());
                            if (connection != null) {
                                user = connection.getUser();
                                System.out.println("uid = "+connection.getUser().getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            ValuePrinter.print("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                ValuePrinter.print("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            ValuePrinter.print("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public void getData() {
        System.out.println("Get Data");
//        if (ConnectServer()){
        String module = "layerfarm_android";
        String function_name = "general_expense_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ExpenseGeneralList> call = apiInterfaceJson.getGeneralExpenseLayerfarm(token2, parameter);
        try {
            ArrayList<ExpenseGeneral> empDataList = new ArrayList<>();
            empDataList.addAll(call.execute().body().getExpenseGeneralList());
            System.out.println("house size = "+empDataList.size());
//                assertEquals(data, curr.size()>0);
            assertTrue(empDataList.size()>0);
        }
        catch (Exception e){
            assertFalse(true);
        }

    }

    @Test
    public void test_expense_general() {
        boolean create = create();
        boolean edit = false, delete = false;
        if (create){
            edit = edit();
        }
        if (edit){
            delete = delete();
        }
        System.out.println("Create General Expense: "+create);
        System.out.println("Edit  General Expense: "+edit);
        System.out.println("Delete  General Expense: "+delete);
        if (!create){
            assertFalse(true);
        } else if (!edit){
            assertFalse(true);
        } else if (!delete){
            assertFalse(true);
        }
    }

    private boolean create(){
        String module = "costing";
        String function_name = "costing_create_expense_general";
        //Data
        String date = "2020-11-09";
        String vendor = "syah";
        String scope = "Global";
        String item = "dedak";
        String cost = "1000";
        String to_location = "";
        //
        ParameterCreateGeneralExpense.Arguments args = new ParameterCreateGeneralExpense.Arguments("", date, item, cost, scope,to_location, vendor);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateGeneralExpense parameter = new ParameterCreateGeneralExpense(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_general_expense(token2, parameter);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ temp);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    private boolean edit(){
        String module = "costing";
        String function_name = "costing_create_expense_general";
        //Data
        String date = "2020-11-09";
        String vendor = "syah";
        String scope = "Global";
        String item = "dedak";
        String cost = "10000";
        String to_location = "";
        //
        ParameterCreateGeneralExpense.Arguments args = new ParameterCreateGeneralExpense.Arguments(nid, date, item, cost, scope,to_location, vendor);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateGeneralExpense parameter = new ParameterCreateGeneralExpense(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_general_expense(token2, parameter);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ temp);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    private boolean delete(){
        String module = "costing";
        String function_name = "costing_delete_expense_general";
        //Data
        ParameterDeleteGeneralExpense.Arguments args = new ParameterDeleteGeneralExpense.Arguments(nid);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteGeneralExpense parameter = new ParameterDeleteGeneralExpense(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.layerfarm_delete_general_expense(token2, parameter);

        try {
            String[] node = call.execute().body();
            System.out.println("node = "+ node);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }
}
