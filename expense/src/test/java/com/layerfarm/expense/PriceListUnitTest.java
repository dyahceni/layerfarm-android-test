package com.layerfarm.expense;

import android.util.Log;
import android.widget.Toast;

import com.layerfarm.expense.PricelistFragment.EditPriceList;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.OvkPrice;
import com.layerfarm.layerfarm.model.ParameterSetPriceList;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.internal.matchers.text.ValuePrinter.print;

public class PriceListUnitTest {
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String token2;
    private String baseUrl = "http://"+"staging"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    String nodes = "";
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();


    @Before
    public void ConnectServer(){
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            print("tokenn1 = "+tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                print(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            if (connection != null) {
                                user = connection.getUser();
//                                Log.d("Layerfarm", "uid = " + user.getUid());
                                System.out.println("uid = "+user.getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            print("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                print("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            print("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public void Test(){
        boolean edit = false, delete = false;
        edit = Edit_Price_List();
        if (edit){
            delete = Delete_Price_List();
        }
        System.out.println("Edit  Sales: "+edit);
        System.out.println("Delete  Sales: "+delete);
        if (!edit){
            assertFalse(true);
        }
        else if (!delete){
            assertFalse(true);
        }
        else {
            assertTrue(true);
        }

    }
    public Boolean Edit_Price_List(){
        String item_nid = "1874";
        ArrayList<OvkPrice> data = new ArrayList<>();

        OvkPrice a = new OvkPrice();
        a.setEffective_date("2020-10-10");
        a.setPrice("100000");
        data.add(a);
        OvkPrice b = new OvkPrice();
        b.setPrice("200000");
        b.setEffective_date("2020-11-11");
        data.add(b);


        String module = "costing";
        String function_name = "costing_update_price_list_movement";

        ParameterSetPriceList.Arguments args = new ParameterSetPriceList.Arguments(item_nid, data);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterSetPriceList paramater = new ParameterSetPriceList(module, function_name, args);
        Call<OvkPrice> call = apiInterfaceJson.layerfarm_set_pricelist_ovk(token2, paramater);

        try {
            OvkPrice response = call.execute().body();
            print("response "+response.getEffective_date());
            return true;

        }catch (Exception e){
            print("error "+e);
            return false;

        }
    }

    public Boolean Delete_Price_List(){
        String item_nid = "1874";
        ArrayList<OvkPrice> data = new ArrayList<>();

        OvkPrice a = new OvkPrice();
        a.setEffective_date("2020-10-10");
        a.setPrice("100000");
        data.add(a);


        String module = "costing";
        String function_name = "costing_update_price_list_movement";

        ParameterSetPriceList.Arguments args = new ParameterSetPriceList.Arguments(item_nid, data);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterSetPriceList paramater = new ParameterSetPriceList(module, function_name, args);
        Call<OvkPrice> call = apiInterfaceJson.layerfarm_set_pricelist_ovk(token2, paramater);

        try {
            OvkPrice response = call.execute().body();
            print("response "+response.getEffective_date());
            return true;

        }catch (Exception e){
            print("error "+e);
            return false;

        }
    }

}
