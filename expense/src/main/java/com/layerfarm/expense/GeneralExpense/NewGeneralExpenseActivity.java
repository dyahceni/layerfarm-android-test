package com.layerfarm.expense.GeneralExpense;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.layerfarm.expense.BottomClass.ScopeBottomClass;
import com.layerfarm.expense.DateWheel.DatePickerPopWin;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.ParameterCreateGeneralExpense;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class  NewGeneralExpenseActivity extends AppCompatActivity {

    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener;
    EditText scopeEditText, dateEditText, vendorEditText, itemEditText, costEditText, locationEdText;
    public String url,username,password;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    ProgressDialog loading;
    LinearLayout location;
    List<Farm> list_location = new ArrayList<>();
    HashMap<String, String> hash_loc = new HashMap<>();
    String[] arr_location;
    Spinner spn_location;
    LinearLayout view_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_expense_input);
        setupUI(findViewById(R.id.parent));
        //Load Server Info
        SQLite = new DataHelper(getApplicationContext());
        location = (LinearLayout) findViewById(R.id.location);
        scopeEditText = (EditText) findViewById(R.id.EdScope);
        dateEditText = (EditText) findViewById(R.id.EdDate);
        vendorEditText = (EditText) findViewById(R.id.EdScope);
        itemEditText = (EditText) findViewById(R.id.EdScope);
        costEditText = (EditText) findViewById(R.id.EdScope);
        spn_location = (Spinner) findViewById(R.id.SpnLocation);
        view_location = (LinearLayout) findViewById(R.id.location);

        Intent mIntent = getIntent();
        list_location = (List<Farm>) mIntent.getSerializableExtra("list_location");

        if (!list_location.isEmpty()){
            for (int i =0; i< list_location.size(); i++){
                hash_loc.put(list_location.get(i).getNid(), list_location.get(i).getName());
            }
        }
        arr_location = hash_loc.values().toArray(new String[0]);

        // inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, arr_location);

        // mengeset Array Adapter tersebut ke Spinner
        spn_location.setAdapter(adapter);
        spn_location.setSelected(true);

        //dateEditText.setEnabled(true);
        dateEditText.requestFocus();

        //get Scope
        scopeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ScopeBottomClass bottom_scope = new ScopeBottomClass();
                bottom_scope.show(getSupportFragmentManager(),"UoM");

                bottom_scope.setOnBottomSheetClickListener(new ScopeBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        scopeEditText.setText(text);
                        if(text.equals("Location")){
                            location.setVisibility(View.VISIBLE);
                        }
                        else {
                            location.setVisibility(View.GONE);
                        }
                        bottom_scope.dismiss();
                    }
                });
            }
        });


        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(NewGeneralExpenseActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        dateEditText.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(NewGeneralExpenseActivity.this);
            }
        });
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(NewGeneralExpenseActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public void Back(View view) {
        finish();
    }

    public void confirmExpense(View view) {
        boolean valid=true;
        if (dateEditText.getText().toString().isEmpty() || vendorEditText.getText().toString().isEmpty()
                || scopeEditText.getText().toString().isEmpty() || itemEditText.getText().toString().isEmpty()
                || costEditText.getText().toString().isEmpty()) {
            AskOption();

            }
                else {
            //create_layerfarm_general_expense();
            //saveDataGeneralExpense();
            //LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Data Save...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want save this data?");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.ic_plus);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                    create_layerfarm_general_expense();
                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                    finish();
                }
            });

            // Showing Alert Message
            alertDialog.show();
            //ChickInMainActivity refresh= new ChickInMainActivity();
            //refresh.Submit();

        }
    }

 /*   private void save_general_expense(){

        String date = (String)(dateEditText.getText().toString());
        String vendor = (String)(vendorEditText.getText().toString());
        String scope = (String)(scopeEditText.getText().toString());
        String item = (String)(itemEditText.getText().toString());
        String cost = (String)(costEditText.getText().toString());

        ExpenseGeneral exp_general = new ExpenseGeneral();
        exp_general.setDate(date);
        exp_general.setVendor(vendor);
        exp_general.setScope(scope);
        exp_general.setItem(item);
        exp_general.setCost(cost);
        create_layerfarm_general_expense();
    }*/

    public void create_layerfarm_general_expense() {
        loading = ProgressDialog.show(this, "", "Please wait...", true, false);

        EditText a = (EditText)(findViewById(R.id.EdDate));
        String date = (String)(a.getText().toString());

        EditText b = (EditText)(findViewById(R.id.EdVendor));
        String vendor = (String)(b.getText().toString());

        EditText c = (EditText)(findViewById(R.id.EdScope));
        String scope = (String)(c.getText().toString());

        EditText d = (EditText)(findViewById(R.id.EdItem));
        String item = (String)(d.getText().toString());

        EditText e = (EditText)(findViewById(R.id.EdCost));
        String cost = (String)(e.getText().toString());
        String to_location = "";
        if (scope.equals("Location")){
            String location = spn_location.getSelectedItem().toString();
            to_location = getKey(hash_loc,location);
        }

        String module = "costing";
        String function_name = "costing_create_expense_general";

        ParameterCreateGeneralExpense.Arguments args = new ParameterCreateGeneralExpense.Arguments("", date, item, cost, scope,to_location, vendor);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateGeneralExpense parameter = new ParameterCreateGeneralExpense(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.layerfarm_create_general_expense(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                loading.dismiss();
                SubmitDialog();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                loading.dismiss();
                WarningDialog();

            }
        });

    }

    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Upload Success")
                .setMessage("General Expense Successfully Added")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        NewGeneralExpenseActivity.this.finish();
                    }
                })
                .show();

        //ExpenseMainActivity.ma.finish();

        return myQuittingDialogBox;

    }

    private AlertDialog WarningDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Upload Error")
                .setMessage("General Expense Upload Error, Please Try Again")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        NewGeneralExpenseActivity.this.finish();
                    }
                })
                .show();

        return myQuittingDialogBox;

    }

    public void saveDataGeneralExpense() {

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please fill all data")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
        }).show();
        return myQuittingDialogBox;
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
