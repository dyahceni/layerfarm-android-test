package com.layerfarm.expense.PricelistFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.layerfarm.expense.Adapter.FeedRvAdapter;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.model.AllPriceList;
import com.layerfarm.layerfarm.model.Feedlist;
import com.layerfarm.layerfarm.model.Ovklist;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.DecimalFormat;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PricelistFragmentFeed extends Fragment {

    RecyclerView mRecyclerViewFeed;
    RecyclerView.Adapter mAdapterFeed;
    ArrayList<Feedlist> data = new ArrayList<>();
    ProgressDialog loading;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pricelist_fragment_feed, container, false);
        mRecyclerViewFeed = (RecyclerView) view.findViewById(R.id.recycler_view_feed);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewFeed.setLayoutManager(layoutManager);
        mAdapterFeed = new FeedRvAdapter(data);
        mRecyclerViewFeed.setAdapter(mAdapterFeed);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        data.clear();
        getPriceList();

    }

    public void getPriceList(){
        loading = ProgressDialog.show(getContext(), "", "Please wait...", true, false);

        String module = "costing";
        String function_name = "costing_get_price_list";
        String[] args = {"feed"};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ArrayList<Feedlist>> call = apiInterfaceJson.getPriceListFeed(token2, parameter);

        call.enqueue(new Callback<ArrayList<Feedlist>>() {
            @Override
            public void onResponse(Call<ArrayList<Feedlist>> call, Response<ArrayList<Feedlist>> response) {

                //Toast.makeText()

                if (response != null && response.isSuccessful()) {
                    try {
                        ArrayList<Feedlist> val = response.body();
                        data.addAll(val);
                        mAdapterFeed.notifyDataSetChanged();
                        loading.dismiss();
                    }catch (Exception e){
                        //e.printStackTrace();
                        // String error = e.toString();
                        loading.dismiss();
                        Toast.makeText(getContext(),e.toString(),Toast.LENGTH_LONG).show();
                    }


                } else {
                    //Log.i("onEmptyResponse", "Returned empty response");
                    loading.dismiss();
                    Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();

                }



            }

            @Override
            public void onFailure(Call<ArrayList<Feedlist>> call, Throwable t) {
                //Log.d("Layerfarm", "Data failure = " + t.getMessage());
                loading.dismiss();
                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }


}
