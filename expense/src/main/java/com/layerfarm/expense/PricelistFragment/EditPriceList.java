package com.layerfarm.expense.PricelistFragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.expense.DateWheel.DatePickerPopWin;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.OvkPrice;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterSetPriceList;
import com.layerfarm.layerfarm.model.PriceList;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPriceList extends AppCompatActivity {

    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener, mDateMoreOvkSetListener;
//    EditText eDPriceMore, eDDateMore, eDPrice, eDDate;
    Button confirmOvkButton;
    private ViewGroup more_ovkPriceList;
    //    public String url, code, username, password;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    boolean firsEditText, secondEditText;
    String nid, item_nid ,name ,capacity_weight, unit, vendor , type, status ,price;
    private ViewGroup more_price_list;
    private ProgressBar pgsBar;

    private ArrayList <OvkPrice> ListItemOvkPricelist;
    private ArrayList <PriceList> ListItemPriceList;

    TextView tx_more_price_list;



    LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_price_list);
        setupUI(findViewById(R.id.parent));

        more_price_list = (ViewGroup) findViewById(R.id.more_price_list);
        tx_more_price_list = (TextView) findViewById(R.id.add_more_price_list);
        tx_more_price_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MorePricelist();
            }
        });
//        more_ovkPriceList = (ViewGroup) findViewById(R.id.parent_linear_layout_ovk);
//        container = (LinearLayout) findViewById(R.id.container);
//        eDDate = (EditText) findViewById(R.id.EdDateOvk);
        confirmOvkButton =(Button) findViewById(R.id.confirmButton);
//        eDPrice = (EditText) findViewById(R.id.EdCostOvk);
//
//        eDPriceMore = (EditText) findViewById(R.id.EdCostOvkMore);
//        eDDateMore = (EditText) findViewById(R.id.EdDateOvkMore);

        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        pgsBar.setVisibility(View.VISIBLE);

        Intent mIntent = getIntent();
        nid = mIntent.getStringExtra("nid");
        item_nid = mIntent.getStringExtra("item_nid");
        name = mIntent.getStringExtra("name");
        capacity_weight = mIntent.getStringExtra("capacity_weight");
        unit = mIntent.getStringExtra("unit");
        vendor = mIntent.getStringExtra("vendor");
        type = mIntent.getStringExtra("type");
        status = mIntent.getStringExtra("status");
        price = mIntent.getStringExtra("price");

//        eDPrice.setText(mIntent.getStringExtra("price"));
        Log.d("zzz","layerfarm item_nid = "+item_nid);

        getData();


        /* Di dalam Activity */
// Mengatur Toolbar untuk sebagai ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_title_ovk);
        setSupportActionBar(toolbar);
// Menghapus title default
        getSupportActionBar().setDisplayShowTitleEnabled(false);
// Mengambil akses TextView yang ada di dalam Toolbar
        TextView mTitle = (TextView) toolbar.findViewById(R.id.title_ovk);
        mTitle.setText(name);

        confirmOvkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                    // Setting Dialog Title
                    alertDialog.setTitle("Confirm Edit Data...");
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want edit this data?");
                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.ic_plus);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                            submitEditPricelist();
                        }
                    });
                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event
                            dialog.cancel();
                            // finish();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
            }
        });

    }
    public void getData(){
        String module = "costing";
        String function_name = "costing_get_price_movement";
        String[] args;
        args = new String[]{nid};
        Log.d("Layerfarm", "item_nid = " + item_nid);
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ArrayList<PriceList>> call = apiInterfaceJson.getPriceListItem(token2, parameter);

        call.enqueue(new Callback<ArrayList<PriceList>>() {
            @Override
            public void onResponse(Call<ArrayList<PriceList>> call, Response<ArrayList<PriceList>> response) {
                ArrayList<PriceList> item_price_list = response.body();
                if (item_price_list.isEmpty()){
                    MorePricelist();
                }
                pgsBar.setVisibility(View.GONE);
//                btm_state = true;
//                bottomNavigationView.setVisibility(View.VISIBLE);
                ListItemPriceList = item_price_list;
                SetPricelist();


            }

            @Override
            public void onFailure(Call<ArrayList<PriceList>> call, Throwable t) {
                pgsBar.setVisibility(View.GONE);
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks recording failure = " + t.getMessage());
            }
        });
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(EditPriceList.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public void MorePricelist() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.more_price_list, null);
        TextView delete = (TextView) rowView.findViewById(R.id.fabDeleteOvk);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ((LinearLayout)rowView.getParent()).removeView(rowView);
            }
        });
        final EditText datePriceList = (EditText)rowView.findViewById(R.id.EdDate);
        datePriceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditPriceList.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        datePriceList.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(EditPriceList.this);
            }
        });
        more_price_list.addView(rowView, more_price_list.getChildCount()-1);
    }

    public void SetPricelist(){
        for (int i =0; i< ListItemPriceList.size(); i++){
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View pricelistView = inflater.inflate(R.layout.more_price_list,null);
            TextView delete = (TextView) pricelistView.findViewById(R.id.fabDeleteOvk);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ((LinearLayout)pricelistView.getParent()).removeView(pricelistView);
                }
            });
            final EditText date = (EditText)(pricelistView.findViewById(R.id.EdDate));
            final String date_ = ListItemPriceList.get(i).getEffective_date();
            EditText total_price = (EditText)(pricelistView.findViewById(R.id.EdCost));
            date.setText(date_);
            date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditPriceList.this, new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                            //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                            date.setText(dateDesc);
                        }
                    }).textConfirm("CONFIRM") //text of confirm button
                            .textCancel("CANCEL") //text of cancel button
                            .btnTextSize(16) // button text size
                            .viewTextSize(25) // pick view text size
                            .colorCancel(Color.parseColor("#999999")) //color of cancel button
                            .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                            .minYear(1990) //min year in loop
                            .maxYear(2550) // max year in loop
                            .dateChose(date_) // date chose when init popwindow
                            .build();

                    pickerPopWin.showPopWin(EditPriceList.this);
                }
            });

            total_price.setText(ListItemPriceList.get(i).getPrice());
            more_price_list.addView(pricelistView, more_price_list.getChildCount()-1);
        }
    }

    private void submitEditPricelist(){
        List <Integer> ListChickin = new ArrayList <>();
        ListItemOvkPricelist = new ArrayList <>();
        int childCount = more_price_list.getChildCount()-1;
        Log.e("childCount",""+ childCount);

        for (int c = 0; c < childCount; c++) {
            OvkPrice ovkprice = new OvkPrice();

            String itemNid = item_nid;
            View childView = more_price_list.getChildAt(c);
            EditText date = (EditText)(childView.findViewById(R.id.EdDate));
            String effectivedateInsert = (String)(date.getText().toString());

            EditText price = (EditText)(childView.findViewById(R.id.EdCost));
            String priceInsert = (String)(price.getText().toString());

//            ovkprice.setItemNid(itemNid);
            ovkprice.setPrice(priceInsert);
            ovkprice.setEffective_date(effectivedateInsert);
            ListItemOvkPricelist.add(ovkprice);



            Log.e("itemNid",""+ itemNid);
            Log.e("priceInsert",""+ priceInsert);
            Log.e("effectivedateInsert",""+ effectivedateInsert);
//            set_layerfarm_Pricelist(itemNid, effectivedateInsert, priceInsert);


        }
        SubmitPriceList(item_nid, ListItemOvkPricelist);


    }
    public void SubmitPriceList(String item_nid, ArrayList<OvkPrice> ListData){
        final ProgressDialog loading = ProgressDialog.show(EditPriceList.this, "", "Please wait...", true, false);
        Log.d("zzz","size = "+ListItemOvkPricelist.size());
        String module = "costing";
        String function_name = "costing_update_price_list_movement";

        ParameterSetPriceList.Arguments args = new ParameterSetPriceList.Arguments(item_nid, ListData);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterSetPriceList paramater = new ParameterSetPriceList(module, function_name, args);
        Call<OvkPrice> call = apiInterfaceJson.layerfarm_set_pricelist_ovk(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback<OvkPrice>() {@Override
        public void onResponse(Call <OvkPrice> call, Response<OvkPrice> response) {
            Toast.makeText(EditPriceList.this,"Item Successfully Added",Toast.LENGTH_LONG).show();
            loading.dismiss();
//            SubmitPriceList();
        }
            @Override
            public void onFailure(Call <OvkPrice> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                loading.dismiss();
                Toast.makeText(EditPriceList.this, "Items Failed to Update", Toast.LENGTH_LONG).show();
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });

//        if (ListItemOvkPricelist.size()>0){
//            String itemNid = ListItemOvkPricelist.get(0).getItemNid();
//            String effectivedateInsert = ListItemOvkPricelist.get(0).getEffective_date();
//            String priceInsert = ListItemOvkPricelist.get(0).getPrice();
//            String module = "costing";
//            String function_name = "costing_set_price";
//
//            ParameterSetPriceList.Arguments args = new ParameterSetPriceList.Arguments(itemNid, effectivedateInsert, priceInsert);
//            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//            String token2 = RetrofitData.getInstance().getToken2();
//            ParameterSetPriceList paramater = new ParameterSetPriceList(module, function_name, args);
//            Call<OvkPrice> call = apiInterfaceJson.layerfarm_set_pricelist_ovk(token2, paramater);
//            Log.d("Layerfarm", "token 2 = " + token2);
//
//            call.enqueue(new Callback<OvkPrice>() {@Override
//            public void onResponse(Call <OvkPrice> call, Response<OvkPrice> response) {
//                Toast.makeText(EditPriceList.this,"Item Successfully Added",Toast.LENGTH_LONG).show();
//                ListItemOvkPricelist.remove(0);
//                SubmitPriceList();
//            }
//                @Override
//                public void onFailure(Call <OvkPrice> call, Throwable t) {
//                    Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
//                    //LayerFarm(farmName, getEmail(), getPassword());
//                }
//            });
//        }
//        else {
//            Toast.makeText(EditPriceList.this,"All Item Successfully Added",Toast.LENGTH_LONG).show();
//            SubmitDialog();
//        }

    }

    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Edit Success")
                .setMessage("Pricelist Successfully Edited")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        EditPriceList.this.finish();
                    }
                })
                .show();

        //ExpenseMainActivity.ma.finish();

        return myQuittingDialogBox;

    }

    public void back_pricelist(View view) {
        finish();
    }
}
