package com.layerfarm.expense.Adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layerfarm.expense.PricelistFragment.EditPriceList;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.model.Ovklist;

import java.util.ArrayList;

public class OvkRvAdapter extends RecyclerView.Adapter<OvkRvAdapter.ExpViewHolder> {


    AlertDialog.Builder dialog;
    private ArrayList<Ovklist> dataList;

    public OvkRvAdapter(ArrayList<Ovklist> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ExpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rv_pricelist_ovk, parent, false);


        return new ExpViewHolder(view);


    }

    @Override
    public void onBindViewHolder(ExpViewHolder holder, final int position) {

        if (String.valueOf(dataList.get(position).getType()) .equals("Disinfectan")) {
            holder.imageScope.setImageResource(R.drawable.ic_ovk);
        } else if (String.valueOf(dataList.get(position).getType()) .equals("Vaccine")){
            holder.imageScope.setImageResource(R.drawable.ic_vaciine);
        }else if (String.valueOf(dataList.get(position).getType()) .equals("Medicine")){
            holder.imageScope.setImageResource(R.drawable.ic_medicine);
        }else if (String.valueOf(dataList.get(position).getType()) .equals("Vitamin")){
            holder.imageScope.setImageResource(R.drawable.ic_vitamin);
        }else if (String.valueOf(dataList.get(position).getType()) .equals("Probiotic")){
            holder.imageScope.setImageResource(R.drawable.ic_probiotic);
        }

        holder.txtNid.setText(dataList.get(position).getNid());
        holder.txtNidItem.setText(dataList.get(position).getItemNid());
        holder.txtVendor.setText(dataList.get(position).getVendor());
        holder.txtItem.setText(dataList.get(position).getOvkName());
        holder.txtPrice.setText(dataList.get(position).getPrice());
        holder.txtCapacity.setText(dataList.get(position).getCapacityWeight());
        holder.txtUnit.setText(dataList.get(position).getUnit());
        holder.txtEffective_Date.setText(dataList.get(position).getEffective_date());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(getActivity().getBaseContext(),
                        TargetActivity.class);
                intent.putExtra("message", message);
                getActivity().startActivity(intent);*/


                Intent mIntent = new Intent(view.getContext(), EditPriceList.class);
                mIntent.putExtra("nid", dataList.get(position).getNid());
                mIntent.putExtra("item_nid", dataList.get(position).getItemNid());
                mIntent.putExtra("name", dataList.get(position).getOvkName());
                mIntent.putExtra("capacity_weight", dataList.get(position).getCapacityWeight());
                mIntent.putExtra("unit", dataList.get(position).getUnit());
                mIntent.putExtra("vendor", dataList.get(position).getVendor());
                mIntent.putExtra("type", dataList.get(position).getType());
                mIntent.putExtra("status", dataList.get(position).getStatus());
                mIntent.putExtra("price", dataList.get(position).getPrice());

                view.getContext().startActivity(mIntent);


            }
        });


/*        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //mFragment.onItemLongClicked(position);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:

                                break;
                            case 1:
                                break;
                        }
                    }
                }).show();
                return true;
            }

        });*/



    }

    @Override
    public int getItemCount() {
        return (dataList == null) ? 0 : dataList.size();
    }

    class ExpViewHolder extends RecyclerView.ViewHolder {
        TextView txtNid, txtNidItem, txtVendor, txtPrice, txtItem, txtCapacity, txtUnit, txtEffective_Date;
        ImageView imageScope;
        ExpViewHolder(View itemView) {
            super(itemView);
            txtNid = (TextView) itemView.findViewById(R.id.tVNid);
            txtNidItem = (TextView) itemView.findViewById(R.id.tVDate);
            txtVendor = (TextView) itemView.findViewById(R.id.tvVendor);
            txtCapacity = (TextView) itemView.findViewById(R.id.tvCapacity);
            txtUnit = (TextView) itemView.findViewById(R.id.tvUnit);
            txtItem = (TextView) itemView.findViewById(R.id.tvItem);
            txtPrice = (TextView) itemView.findViewById(R.id.tvCost);
            txtEffective_Date = (TextView) itemView.findViewById(R.id.effective_date);
            imageScope = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }


}
