package com.layerfarm.expense.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layerfarm.expense.GeneralExpense.EditGeneralExpenseActivity;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.model.ExpenseGeneral;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.ParameterDeleteGeneralExpense;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralExpenseRvAdapter extends RecyclerView.Adapter<GeneralExpenseRvAdapter.ExpViewHolder> {

    Context c;
    android.app.AlertDialog.Builder dialog;
    private ArrayList<ExpenseGeneral> dataList;
    List<Farm> location = new ArrayList<>();



    public GeneralExpenseRvAdapter(ArrayList<ExpenseGeneral> dataList, List<Farm> location) {
        this.dataList = dataList;
        this.location = location;
    }

    @Override
    public ExpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rv_expense_general, parent, false);


        return new ExpViewHolder(view);


    }

    @Override
    public void onBindViewHolder(ExpViewHolder holder, final int position) {
        Log.d("scope", "scope = " + dataList.get(position).getScope());
        if (dataList.get(position).getScope() .equals("Global")) {
            holder.imageScope.setImageResource(R.drawable.ic_global);
        } else{
            holder.imageScope.setImageResource(R.drawable.ic_location);
        }

        holder.txtNid.setText(dataList.get(position).getNid());
        holder.txtdate.setText(dataList.get(position).getDate());
        holder.txtVendor.setText(dataList.get(position).getVendor());
        //holder.txtScope.setText(dataList.get(position).getScope());
        double cost = Double.parseDouble(dataList.get(position).getCost());
        NumberFormat formatter = new DecimalFormat("###,###,##0.00");
        String formattedNumber = formatter.format(cost);
        holder.txtItem.setText(dataList.get(position).getItem());
        holder.txtCost.setText(formattedNumber);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                final List<Farm> loc = location;
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                Intent mIntent = new Intent(view.getContext(), EditGeneralExpenseActivity.class);
                                mIntent.putExtra("nid", dataList.get(position).getNid());
                                mIntent.putExtra("date", dataList.get(position).getDate());
                                mIntent.putExtra("scope", dataList.get(position).getScope());
                                mIntent.putExtra("vendor", dataList.get(position).getVendor());
                                mIntent.putExtra("item", dataList.get(position).getItem());
                                mIntent.putExtra("cost", dataList.get(position).getCost());
                                mIntent.putExtra("location", dataList.get(position).getLocation());
                                mIntent.putExtra("list_location", (Serializable) loc);
                                view.getContext().startActivity(mIntent);
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        final ProgressDialog loading = ProgressDialog.show(view.getContext(), "Delete Data", "Please wait...", true, false);
                                        String module = "costing";
                                        String function_name = "costing_delete_expense_general";

                                        String nid = dataList.get(position).getNid();
                                        ParameterDeleteGeneralExpense.Arguments args = new ParameterDeleteGeneralExpense.Arguments(nid);

                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                        String token2 = RetrofitData.getInstance().getToken2();
                                        ParameterDeleteGeneralExpense parameter = new ParameterDeleteGeneralExpense(module, function_name, args);

                                        Call<String[]> call = apiInterfaceJson.layerfarm_delete_general_expense(token2, parameter);
                                        call.enqueue(new Callback<String[]>() {
                                            @Override
                                            public void onResponse(Call<String[]> call, Response<String[]> response) {

                                                if (response != null && response.isSuccessful()) {
                                                    loading.dismiss();
                                                    dataList.remove(position);
                                                    notifyItemRemoved(position);
                                                    notifyItemRangeChanged(position, dataList.size());

                                                    AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
                                                    builder.setTitle("Information");
                                                    builder.setMessage("General Expense Data Deleted!");
                                                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<String[]> call, Throwable t) {
                                                loading.dismiss();
                                                AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
                                                builder.setTitle("Information");
                                                builder.setMessage("Failed to deleted General Expense!");
                                                builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                }).show();
                                            }
                                        });

                                        notifyDataSetChanged();

                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();
                                break;
                        }
                    }
                }).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return (dataList == null) ? 0 : dataList.size();
    }

    class ExpViewHolder extends RecyclerView.ViewHolder {
        TextView txtNid, txtdate, txtVendor, txtScope, txtLocation, txtItem, txtCost;
        ImageView imageScope;
        ExpViewHolder(View itemView) {
            super(itemView);
            txtNid = (TextView) itemView.findViewById(R.id.tVNid);
            txtdate = (TextView) itemView.findViewById(R.id.tVDate);
            txtVendor = (TextView) itemView.findViewById(R.id.tvVendor);
            txtItem = (TextView) itemView.findViewById(R.id.tvItem);
            txtCost = (TextView) itemView.findViewById(R.id.tvCost);
            imageScope = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }



}
