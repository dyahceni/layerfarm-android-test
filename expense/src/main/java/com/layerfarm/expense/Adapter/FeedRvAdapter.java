package com.layerfarm.expense.Adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layerfarm.expense.PricelistFragment.EditPriceList;
import com.layerfarm.expense.R;
import com.layerfarm.layerfarm.model.Feedlist;

import java.util.ArrayList;

public class FeedRvAdapter extends RecyclerView.Adapter<FeedRvAdapter.ExpViewHolder> {

    AlertDialog.Builder dialog;
    private ArrayList<Feedlist> dataList;

    public FeedRvAdapter(ArrayList<Feedlist> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ExpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rv_pricelist_feed, parent, false);


        return new ExpViewHolder(view);


    }

    @Override
    public void onBindViewHolder(ExpViewHolder holder, int position) {

        final int pos = position;
        holder.txtNid.setText(dataList.get(position).getNid());
        holder.txtNidItem.setText(dataList.get(position).getItemNid());
        holder.txtVendor.setText(dataList.get(position).getDescription());
        holder.txtItem.setText(dataList.get(position).getFeedName());
        holder.txtPrice.setText(dataList.get(position).getPrice());
        holder.txtCapacity.setText(dataList.get(position).getStatus());
        holder.txteffective_date.setText(dataList.get(position).getEffective_date());
        //holder.txtUnit.setText(dataList.get(position).getUnit());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(getActivity().getBaseContext(),
                        TargetActivity.class);
                intent.putExtra("message", message);
                getActivity().startActivity(intent);*/

                Intent mIntent = new Intent(view.getContext(), EditPriceList.class);
                mIntent.putExtra("nid", dataList.get(pos).getNid());
                mIntent.putExtra("item_nid", dataList.get(pos).getItemNid());
                mIntent.putExtra("name", dataList.get(pos).getFeedName());
                mIntent.putExtra("description", dataList.get(pos).getDescription());
                mIntent.putExtra("status", dataList.get(pos).getStatus());
                mIntent.putExtra("price", dataList.get(pos).getPrice());


                view.getContext().startActivity(mIntent);


            }
        });

        /*holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //mFragment.onItemLongClicked(position);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:

                                break;
                            case 1:
                                break;
                        }
                    }
                }).show();
                return true;
            }

        });*/

    }

    @Override
    public int getItemCount() {
        return (dataList == null) ? 0 : dataList.size();
    }

    class ExpViewHolder extends RecyclerView.ViewHolder {
        TextView txtNid, txtNidItem, txtVendor, txtPrice, txtItem, txtCapacity, txtUnit, txteffective_date;
        ImageView imageScope;
        ExpViewHolder(View itemView) {
            super(itemView);
            txtNid = (TextView) itemView.findViewById(R.id.tVNid);
            txtNidItem = (TextView) itemView.findViewById(R.id.tVDate);
            txtVendor = (TextView) itemView.findViewById(R.id.tvVendor);
            txtCapacity = (TextView) itemView.findViewById(R.id.tvCapacity);
            txtUnit = (TextView) itemView.findViewById(R.id.tvUnit);
            txtItem = (TextView) itemView.findViewById(R.id.tvItem);
            txtPrice = (TextView) itemView.findViewById(R.id.tvCost);
            txteffective_date = (TextView) itemView.findViewById(R.id.effective_date);
            imageScope = (ImageView) itemView.findViewById(R.id.imageView);

        }
    }

}
