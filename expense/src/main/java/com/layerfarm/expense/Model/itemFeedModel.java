package com.layerfarm.expense.Model;


public class itemFeedModel {

    String nid;
    String item_nid;
    String feed_name;
    String description ;
    String status ;
    String price;

    public itemFeedModel() {

    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getItem_nid() {
        return item_nid;
    }

    public void setItem_nid(String item_nid) {
        this.item_nid = item_nid;
    }

    public String getFeed_name() {
        return feed_name;
    }

    public void setFeed_name(String feed_name) {
        this.feed_name = feed_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
