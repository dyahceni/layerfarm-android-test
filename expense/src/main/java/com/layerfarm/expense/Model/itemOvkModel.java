package com.layerfarm.expense.Model;


public class itemOvkModel {

    String nid;
    String item_nid;
    String ovk_name;
    String capacity_weight;
    String unit;
    String vendor;
    String type;
    String status;
    String price;
    String effectivedate;

    public itemOvkModel() {

    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getItem_nid() {
        return item_nid;
    }

    public void setItem_nid(String item_nid) {
        this.item_nid = item_nid;
    }

    public String getOvk_name() {
        return ovk_name;
    }

    public void setOvk_name(String ovk_name) {
        this.ovk_name = ovk_name;
    }

    public String getCapacity_weight() {
        return capacity_weight;
    }

    public void setCapacity_weight(String capacity_weight) {
        this.capacity_weight = capacity_weight;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(String effectivedate) {
        this.effectivedate = effectivedate;
    }
}
