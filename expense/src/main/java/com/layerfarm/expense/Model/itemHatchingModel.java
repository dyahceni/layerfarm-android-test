package com.layerfarm.expense.Model;


public class itemHatchingModel {

    String nid;
    String item_nid;
    String hatch_date;
    String price;

    public itemHatchingModel() {

    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getItem_nid() {
        return item_nid;
    }

    public void setItem_nid(String item_nid) {
        this.item_nid = item_nid;
    }

    public String getHatch_date() {
        return hatch_date;
    }

    public void setHatch_date(String hatch_date) {
        this.hatch_date = hatch_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
