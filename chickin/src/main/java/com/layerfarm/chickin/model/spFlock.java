package com.layerfarm.chickin.model;


public class spFlock {

    private String nid;
    private String name;

    public spFlock(String nid, String name){
        this.nid = nid;
        this.name =name;
    }

    public String getNid() {
        return nid;
    }
    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


}
