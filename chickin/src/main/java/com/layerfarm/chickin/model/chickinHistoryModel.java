package com.layerfarm.chickin.model;


public class chickinHistoryModel {
    String nid;
    String rare_type;
    String hatching_date;
    String strain_name;
    public chickinHistoryModel(){}



    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getRare_type() {
        return rare_type;
    }

    public void setRare_type(String rare_type) {
        this.rare_type = rare_type;
    }

    public String getHatching_date() {
        return hatching_date;
    }

    public void setHatching_date(String hatching_date) {
        this.hatching_date = hatching_date;
    }

    public String getStrain_name() {
        return strain_name;
    }

    public void setStrain_name(String strain_name) {
        this.strain_name = strain_name;
    }
}
