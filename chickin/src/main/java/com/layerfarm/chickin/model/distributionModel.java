package com.layerfarm.chickin.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class distributionModel {
    private Arguments args;

    public distributionModel(Arguments args) {
        this.args = args;
    }
    public static class Arguments{
        @SerializedName("distribution")
        private ArrayList<distribution> distribution;

        public Arguments(ArrayList<distribution> distribution){
            this.distribution = distribution;
        }
    }
    public static class distribution{
        @SerializedName("house_nid")
        private String house_nid;
        @SerializedName("number_of_birds")
        private String number_of_birds;
        @SerializedName("start_recording")
        private String start_recording;

        public String getHouse_nid() {
            return house_nid;
        }

        public void setHouse_nid(String house_nid) {
            this.house_nid = house_nid;
        }

        public String getNumber_of_birds() {
            return number_of_birds;
        }

        public void setNumber_of_birds(String number_of_birds) {
            this.number_of_birds = number_of_birds;
        }

        public String getStart_recording() {
            return start_recording;
        }

        public void setStart_recording(String start_recording) {
            this.start_recording = start_recording;
        }
    }
}
