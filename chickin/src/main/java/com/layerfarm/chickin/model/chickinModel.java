package com.layerfarm.chickin.model;

public class  chickinModel {

    String id;
    String chick_in_type;
    String hatch_date;
    String age_in_week;
    String age_in_days;
    String age_on_date;
    String strains_id;
    String rid;
    String number_of_bird;
    String recording_date;

    public chickinModel() {

    }
    /*
    chickinModel(String chick_in_type, String hatch_date, String age_in_week, String age_in_days, String age_on_date, String strains_id, String chick_in_id, String flock_id, String number_of_bird, String start_recording) {
        this.chick_in_type = chick_in_type;
        this.hatch_date = hatch_date;
        this.age_in_week = age_in_week;
        this.age_in_days = age_in_days;
        this.age_on_date = age_on_date;
        this.strains_id = strains_id;
        this.chick_in_id = chick_in_id;
        this.flock_id = flock_id;
        this.number_of_bird = number_of_bird;
        this.start_recording = start_recording;
    }
    */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChick_in_type() {
        return chick_in_type;
    }

    public void setChick_in_type(String chick_in_type) {
        this.chick_in_type = chick_in_type;
    }

    public String getHatch_date() {
        return hatch_date;
    }

    public void setHatch_date(String hatch_date) {
        this.hatch_date = hatch_date;
    }

    public String getAge_in_week() {
        return age_in_week;
    }

    public void setAge_in_week(String age_in_week) {
        this.age_in_week = age_in_week;
    }

    public String getAge_in_days() {
        return age_in_days;
    }

    public void setAge_in_days(String age_in_days) {
        this.age_in_days = age_in_days;
    }

    public String getAge_on_date() {
        return age_on_date;
    }

    public void setAge_on_date(String age_on_date) {
        this.age_on_date = age_on_date;
    }

    public String getStrains_id() {
        return strains_id;
    }

    public void setStrains_id(String strains_id) {
        this.strains_id = strains_id;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getNumber_of_bird() {
        return number_of_bird;
    }

    public void setNumber_of_bird(String number_of_bird) {
        this.number_of_bird = number_of_bird;
    }

    public String getRecording_date() {
        return recording_date;
    }

    public void setRecording_date(String recording_date) {
        this.recording_date = recording_date;
    }
}
