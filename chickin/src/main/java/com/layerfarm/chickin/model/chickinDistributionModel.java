package com.layerfarm.chickin.model;

public class chickinDistributionModel {

    String id;
    String chick_in_id;
    String flock_id;
    String number_of_bird;
    String start_recording;
    String type;
    String name;
    String location_name;

    public chickinDistributionModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChick_in_id() {
        return chick_in_id;
    }

    public void setChick_in_id(String chick_in_id) {
        this.chick_in_id = chick_in_id;
    }

    public String getFlock_id() {
        return flock_id;
    }

    public void setFlock_id(String flock_id) {
        this.flock_id = flock_id;
    }

    public String getNumber_of_bird() {
        return number_of_bird;
    }

    public void setNumber_of_bird(String number_of_bird) {
        this.number_of_bird = number_of_bird;
    }

    public String getStart_recording() {
        return start_recording;
    }

    public void setStart_recording(String start_recording) {
        this.start_recording = start_recording;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
}
