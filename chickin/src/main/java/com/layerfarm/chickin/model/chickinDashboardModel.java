package com.layerfarm.chickin.model;

public class chickinDashboardModel {

    String location;
    String flock_name;
    String flock_type;
    String number_bird;

    public chickinDashboardModel() {

    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFlock_name() {
        return flock_name;
    }

    public void setFlock_name(String flock_name) {
        this.flock_name = flock_name;
    }

    public String getFlock_type() {
        return flock_type;
    }

    public void setFlock_type(String flock_type) {
        this.flock_type = flock_type;
    }

    public String getNumber_bird() {
        return number_bird;
    }

    public void setNumber_bird(String number_bird) {
        this.number_bird = number_bird;
    }
}
