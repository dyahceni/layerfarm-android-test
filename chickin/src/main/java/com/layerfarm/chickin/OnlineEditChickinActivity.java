package com.layerfarm.chickin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ImageView;

import com.layerfarm.setting.DateWheel.DatePickerPopWin;
import com.layerfarm.chickin.database.DBDataSource;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Node;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.ParamaterHatch;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.chickinHistory;
import com.layerfarm.layerfarm.model.getStrainModel;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.chickin.listItemChickin.FlockListActivity;
import com.layerfarm.chickin.listItemChickin.StrainListActivity;
import com.layerfarm.setting.maskededittext.MaskedEditText;
import com.layerfarm.setting.maskededittext.MaskedFormatter;
import com.layerfarm.setting.model.Flock;
import com.layerfarm.setting.model.Location;
import com.layerfarm.setting.model.Strain;
import com.layerfarm.chickin.model.chickinDistributionModelAge;
import com.layerfarm.chickin.model.chickinDistributionModelHatch;
import com.layerfarm.chickin.model.distributionModel;
import com.layerfarm.setting.DataHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineEditChickinActivity extends AppCompatActivity {

    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener, mDateSetListenerHatch, mDateSetListenerAge, OnDateSetListener;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    CursorAdapter mAdapter;
    private Button saveButton;

    private EditText Edon_date,
            start_recording_date,
            edTotalBirdHatch,
            inWeek,
            inDays,
            selected_date_age,
            edBirdAge, EdStrain , EdStrainId;

    public String[] strain_value,
            locationAge_value,
            locationHatch_value,
            locationNid,
            strainNid;
    public String dist[];
    public String url,
            code,
            username,
            password;

    private ViewGroup more_ageView,
            more_hatchView;

    private String hatchtype = "";

    //inisialisasi array list untuk masing2 pengelompokan di recording
    private ArrayList <Flock> ListFlock;
    private ArrayList <Strain> ListStrain;
    private ArrayList <Location> ListLocation;
    private ArrayList <chickinDistributionModelAge> ListchickinDistributionAge;
    private ArrayList <chickinDistributionModelHatch> ListchickinDistributionHatch;
    private ArrayList < Integer > ListChickin;
    private ArrayList <distributionModel> ListDistribuiton;

    private Spinner spStrain;
    private Spinner spLocationAge;
    private Spinner spLocationHatch;
    private Spinner spChickin;
    private String[] chickinmode = {
            "Hatch Date",
            "Age"
    };


    private LinearLayout parentLinearLayoutHatch,
            parentLinearLayoutAge;

    private MaskedEditText mEdtMaskedCustom;
    private EditText mEdtMasked,on_date,
            rec_date_age,
            mEdtMaskedHatch,
            rec_date_hatch,
            hatch_date,
            totalBirdAge,
            totalBirdHatch;
    private EditText start_recording_date_hatch;
    private MaskedFormatter formatter;
    private DBDataSource dataSource;

    public String idFlockData,
            total_birdData,
            start_recordingData;

    private int flock_child;
    private String[] separated;
    private String flock_name;
    private String nid;
    private String created = "";
    private String strainHatch, strStrain;
    ProgressDialog loading;
    private static final int ONLINE_EDIT_ACTIVITY_REQUEST_CODE = 1;
    private static final int FLOCK_LIST_ACTIVITY_REQUEST_CODE = 2;

    TabLayout tabLayout;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_edit_chickin);
        setupUI(findViewById(R.id.parent));


        loading = ProgressDialog.show(this, null, "Please wait...", true, false);
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabsChickin);

        //get value from xml


        EdStrain = (EditText) findViewById(R.id.EdStrainOption);
        EdStrainId = (EditText) findViewById(R.id.EdIdStrain);

        hatch_date = (EditText) findViewById(R.id.hatchDate);

        Edon_date = (EditText) findViewById(R.id.on_date);
        inWeek = (EditText) findViewById(R.id.inWeek);
        inDays = (EditText) findViewById(R.id.inDays);

        more_ageView = (ViewGroup) findViewById(R.id.parent_linear_layout_age);
        more_hatchView = (ViewGroup) findViewById(R.id.parent_linear_layout_hatch);

        //pilih strain
        EdStrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnlineEditChickinActivity.this, StrainListActivity.class);
                startActivityForResult(intent, ONLINE_EDIT_ACTIVITY_REQUEST_CODE);
            }
        });

        Edon_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(OnlineEditChickinActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        Edon_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(OnlineEditChickinActivity.this);
            }
        });

        //set Date Hatch Type
        hatch_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(OnlineEditChickinActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        hatch_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(OnlineEditChickinActivity.this);

            }
        });

        //get intent
        final ArrayList<chickinHistory.distribution_history> myList = (ArrayList<chickinHistory.distribution_history>) getIntent().getSerializableExtra("chickin_distribution");
/*
        String value = "";
        for (int i =0; i< myList.size(); i++){

            value += myList.get(i).getFlock_name()+"\n";
            value += " Type Cage = "+ myList.get(i).getType()+"\n";
            value += " Date = "+ myList.get(i).getRecording_date()+"\n";
            value += " flock Name = "+ myList.get(i).getFlock_name()+"\n";
            value += " flock Name = "+ flock_name+"\n";
            value += " Number of Birds = "+myList.get(i).getNumber_of_bird()+"\n";

        }
        Log.d("value",""+ value);

*/

        nid  = (getIntent().getStringExtra("nid"));
        hatchtype = (getIntent().getStringExtra("type"));
        String strain_id  = (getIntent().getStringExtra("strain_id"));
        String strain_name  = (getIntent().getStringExtra("strain_name"));
        String hatch_date_value  = (getIntent().getStringExtra("hatch_date"));
        String on_date_value  = (getIntent().getStringExtra("on_date"));
        String in_day  = (getIntent().getStringExtra("in_day"));
        String in_week  = (getIntent().getStringExtra("in_week"));

        Log.d("strain_id","strain_id " + strain_id);
        Log.d("strain_name","strain_name " + strain_name);
        Log.d("layerfarm", "hatch_type = "+hatchtype);


        EdStrainId.setText(strain_id);
        EdStrain.setText(strain_name);

        if(hatchtype != null){
            if(hatchtype.equals("Age")) {
                mTabLayout.getTabAt(1).select();
                onTabTapped(1);
                Edon_date.setText(on_date_value);
                inDays.setText(in_day);
                inWeek.setText(in_week);

            } else{
                mTabLayout.getTabAt(0).select();
                onTabTapped(0);
                hatch_date.setText(hatch_date_value);
            }
        }
        else{
            Log.d("layerfarm", "hatch_type = "+hatchtype);
        }
//    if(){
        //looping data distribusi
        if (!myList.isEmpty()){
            for (int i = 0; i < myList.size(); i++) {

                separated = myList.get(i).getFlock_name().split(" - ");
                flock_name = separated[1].trim();
                //add input age
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rowView = inflater.inflate(R.layout.more_age, null);
                EditText flockAgeNid = (EditText) (rowView.findViewById(R.id.morelocationAgeNid));
                EditText flockName = (EditText) (rowView.findViewById(R.id.morelocationAge));
                EditText edBirdAge = (EditText) (rowView.findViewById(R.id.edBirdAge));
                final EditText start_rec_age = (EditText) (rowView.findViewById(R.id.start_recording_date_age));
                ImageView buttonRemove = (ImageView)(rowView.findViewById(R.id.remove));

                flockName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewGroup vg = ((ViewGroup) rowView.getParent());
                        int index = vg.indexOfChild(rowView);
                        flock_child = index;
                        // Start the SecondActivity
                        Intent intent = new Intent(OnlineEditChickinActivity.this, FlockListActivity.class);
                        startActivityForResult(intent, FLOCK_LIST_ACTIVITY_REQUEST_CODE);
                        //Toast.makeText(ChickinEntryActivity.this, "flock_child "+ index, Toast.LENGTH_SHORT).show();
                    }
                });

                final View.OnClickListener thisListener = new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        ((LinearLayout)rowView.getParent()).removeView(rowView);
                    }
                };

                buttonRemove.setOnClickListener(thisListener);

                flockAgeNid .setText(myList.get(i).getFlock_nid());
                flockName.setText(myList.get(i).getFlock_name());
                start_rec_age.setText(myList.get(i).getRecording_date());
                edBirdAge.setText(myList.get(i).getNumber_of_bird());
                //set value

                more_ageView.addView(rowView, more_ageView.getChildCount());

                //set Date
                start_rec_age.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String currentDateandTime = sdf.format(new Date());
                        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(OnlineEditChickinActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                                //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                                start_rec_age.setText(dateDesc);
                            }
                        }).textConfirm("CONFIRM") //text of confirm button
                                .textCancel("CANCEL") //text of cancel button
                                .btnTextSize(16) // button text size
                                .viewTextSize(25) // pick view text size
                                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                                .minYear(1990) //min year in loop
                                .maxYear(2550) // max year in loop
                                .dateChose(currentDateandTime) // date chose when init popwindow
                                .build();

                        pickerPopWin.showPopWin(OnlineEditChickinActivity.this);

                    }
                });
            }
        }
        /*-----akhir looping----*/
//    }
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_entry_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ChickinEntry> call = apiInterfaceJson.get_chickin_data_entry(token2, parameter);

        call.enqueue(new Callback<ChickinEntry>() {
            @Override
            public void onResponse(Call<ChickinEntry> call, Response<ChickinEntry>response) {
                ChickinEntry flock = response.body();

                ChickinEntry.getInstance().setHouse(flock.getHouse());
                ChickinEntry.getInstance().setStrain(flock.getStrain());
                loading.dismiss();
//                if(flock!=null && flock.size()>0) {
//                locationHatch_value = new String[flock.size()];
//                locationAge_value = new String[flock.size()];
//                locationNid = new String[flock.size()];
//
//                    for (int i = 0; i < flock.size(); i++) {
//                        locationHatch_value[i] = flock.get(i).getName();
//                        locationAge_value[i] = flock.get(i).getName();
//                        locationNid[i] = flock.get(i).getNid();
//                        Log.d("locationHatch_value", "locationHatch_value = " +flock.get(i).getName());
//                        Log.d("locationAge_value[i]", "locationAge_value[i] = " +flock.get(i).getName());
//                        Log.d("flock.get(i).getNid()", "flock.get(i).getNid() = " +flock.get(i).getNid());
//                    }
//
//                }

//                getStrain();

            }

            @Override
            public void onFailure(Call<ChickinEntry>call, Throwable t) {
                loading.dismiss();
                Log.d("Layerfarm", "Chickin Entry failure = " + t.getMessage());

            }
        });
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
    }

    @Override
    public void onBackPressed() {

        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    public void close(View view){
        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setIcon(R.drawable.check_mark)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
//                        Fragment fragment = new RecordingFragment();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(OnlineEditChickinActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that it is the SecondActivity with an OK result
        if (requestCode == ONLINE_EDIT_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // Get String data from Intent
                String strainId = data.getStringExtra("strainId");
                String strainName = data.getStringExtra("strainName");

                EdStrainId.setText(strainId);
                EdStrain.setText(strainName);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (requestCode == FLOCK_LIST_ACTIVITY_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                String flock_name = data.getStringExtra("flock_name");
                String flock_nid = data.getStringExtra("flock_nid");

                if(hatchtype.equals("Age")) {
                    View childViewAge = more_ageView.getChildAt(flock_child);
                    EditText flock_age_ed = (EditText) (childViewAge.findViewById(R.id.morelocationAge));
                    EditText flock_nid_ed = (EditText) (childViewAge.findViewById(R.id.morelocationAgeNid));
                    flock_age_ed.setText(flock_name);
                    flock_nid_ed.setText(flock_nid);
                }else{
                    View childViewHatch = more_ageView.getChildAt(flock_child);
                    EditText flock_age_ed = (EditText) (childViewHatch.findViewById(R.id.morelocationAge));
                    EditText flock_nid_ed = (EditText) (childViewHatch.findViewById(R.id.morelocationAgeNid));
                    flock_age_ed.setText(flock_name);
                    flock_nid_ed.setText(flock_nid);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void onTabTapped(int position) {

        CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
        CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
        switch (position) {
            case 0:
                hatchtype = "Hatch Date";
                card_view_hatch.setVisibility(View.VISIBLE);
                card_view_age.setVisibility(View.GONE);
                Toast.makeText(OnlineEditChickinActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                hatchtype = "Age";
                card_view_hatch.setVisibility(View.GONE);
                card_view_age.setVisibility(View.VISIBLE);
                Toast.makeText(OnlineEditChickinActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            default:

        }
    }

    public void AddMoreAge(View view) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.more_age, null);
        EditText flockAgeNid = (EditText) (rowView.findViewById(R.id.morelocationAgeNid));
        EditText flockName = (EditText) (rowView.findViewById(R.id.morelocationAge));
        EditText edBirdAge = (EditText) (rowView.findViewById(R.id.edBirdAge));
        final EditText start_rec_age = (EditText) (rowView.findViewById(R.id.start_recording_date_age));
        ImageView buttonRemove = (ImageView)(rowView.findViewById(R.id.remove));

        flockName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup vg = ((ViewGroup) rowView.getParent());
                int index = vg.indexOfChild(rowView);
                flock_child = index;
                // Start the SecondActivity
                Intent intent = new Intent(OnlineEditChickinActivity.this, FlockListActivity.class);
                startActivityForResult(intent, FLOCK_LIST_ACTIVITY_REQUEST_CODE);
                //Toast.makeText(ChickinEntryActivity.this, "flock_child "+ index, Toast.LENGTH_SHORT).show();
            }
        });

        final View.OnClickListener thisListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ((LinearLayout)rowView.getParent()).removeView(rowView);
            }
        };

        buttonRemove.setOnClickListener(thisListener);
        rec_date_age = (rowView.findViewById(R.id.start_recording_date_age));

        //set Date
        rec_date_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(OnlineEditChickinActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        rec_date_age.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(OnlineEditChickinActivity.this);

            }
        });

        more_ageView.addView(rowView, more_ageView.getChildCount());
    }

    public void AddMoreHatch(View view) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.more_hatch, null);
        //Spinner location_hatch = (Spinner)(rowView.findViewById(R.id.spLocationHatch));
        //location_hatch.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, locationHatch_value));
        //location_hatch.setSelected(true);
        rec_date_hatch = (rowView.findViewById(R.id.start_recording_date_hatch));

        //set Date
        rec_date_hatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(OnlineEditChickinActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        rec_date_hatch.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(OnlineEditChickinActivity.this);

            }
        });
        more_hatchView.addView(rowView, more_hatchView.getChildCount());
    }


    private void layerfarm_send_data_hatch_date() {
        List< Integer > ListChickin = new ArrayList < >();
//        ListchickinDistributionHatch = new ArrayList < >();
//        String showallPrompt = "";
//        int childCount = more_hatchView.getChildCount();
//        showallPrompt += "chilcCount :" + childCount + "\n";
//        SQLite = new DataHelper(this);
//        SQLiteDatabase dbase = db.getWritableDatabase();
//
//        for (int c = 0; c < childCount; c++) {
//
//            chickinDistributionModelHatch chickinHatch = new chickinDistributionModelHatch();
//
//            EditText x = (EditText)(findViewById(R.id.hatchDate));
//            String hatch_date_check = (String)(x.getText().toString());
//
//            View childView = more_ageView.getChildAt(c);
//            EditText a = (EditText)(childView.findViewById(R.id.morelocationHatchNid));
//            String nidFlock = (String)(a.getText().toString());
//            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_hatch));
//            String start_recording = (String)(b.getText().toString());
//            EditText d = (EditText)(childView.findViewById(R.id.edBirdHatch));
//            String total_bird = (String)(d.getText().toString());
//
//            chickinHatch.setFlock_id(nidFlock);
//            chickinHatch.setStart_recording(start_recording);
//            chickinHatch.setNumber_of_bird(total_bird);
//            ListchickinDistributionHatch.add(chickinHatch);
//        }
        ListchickinDistributionAge = new ArrayList < >();
        String showallPrompt = "";
        int childCount = more_ageView.getChildCount();
        showallPrompt += "chilcCount :" + childCount + "\n";
        //Toast.makeText(ChickinEntryActivity.this,"Jumlah:"+childCount , Toast.LENGTH_SHORT).show();

        SQLite = new DataHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();

        for (int c = 0; c < childCount; c++) {

            chickinDistributionModelAge chickinAge = new chickinDistributionModelAge();

            EditText x = (EditText)(findViewById(R.id.on_date));
            String hatch_date_check = (String)(x.getText().toString());

            View childView = more_ageView.getChildAt(c);
            EditText a = (EditText)(childView.findViewById(R.id.morelocationAgeNid));
            String nidFlock = (String)(a.getText().toString());
            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_age));
            String start_recording = (String)(b.getText().toString());
            EditText d = (EditText)(childView.findViewById(R.id.edBirdAge));
            String total_bird = (String)(d.getText().toString());

            chickinAge.setFlock_id(nidFlock);
            chickinAge.setStart_recording(start_recording);
            chickinAge.setNumber_of_bird(total_bird);
            ListchickinDistributionAge.add(chickinAge);
        }

            //send data to layerfarm............................./
            String nidStrain = EdStrainId.getText().toString().trim();
            String hatch_date_result = (String)(hatch_date.getText().toString());


        try {
                Log.d("hatch_date_result", "tgl = " + hatch_date_result);
                // TODO Auto-generated method stub
                create_rearing_by_hatch_date(nid, hatch_date_result, nidStrain, created);

            } catch(Exception e) {
                e.printStackTrace();
            }
    }

    private void layerfarm_send_data_age() {

        List < Integer > ListChickin = new ArrayList < >();
        ListchickinDistributionAge = new ArrayList < >();
        String showallPrompt = "";
        int childCount = more_ageView.getChildCount();
        showallPrompt += "chilcCount :" + childCount + "\n";
        //Toast.makeText(ChickinEntryActivity.this,"Jumlah:"+childCount , Toast.LENGTH_SHORT).show();

        SQLite = new DataHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();

        for (int c = 0; c < childCount; c++) {

            chickinDistributionModelAge chickinAge = new chickinDistributionModelAge();

            EditText x = (EditText)(findViewById(R.id.on_date));
            String hatch_date_check = (String)(x.getText().toString());

            View childView = more_ageView.getChildAt(c);
            EditText a = (EditText)(childView.findViewById(R.id.morelocationAgeNid));
            String nidFlock = (String)(a.getText().toString());
            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_age));
            String start_recording = (String)(b.getText().toString());
            EditText d = (EditText)(childView.findViewById(R.id.edBirdAge));
            String total_bird = (String)(d.getText().toString());

            chickinAge.setFlock_id(nidFlock);
            chickinAge.setStart_recording(start_recording);
            chickinAge.setNumber_of_bird(total_bird);
            ListchickinDistributionAge.add(chickinAge);
        }
            //send data to layerfarm............................./
            String nidStrain = EdStrainId.getText().toString().trim();
            String onDate = Edon_date.getText().toString().trim();
            String inWeekAge = inWeek.getText().toString().trim();
            String inDaysAge = inDays.getText().toString().trim();
            //String LocationAge = spLocationAge.getSelectedItem().toString();

            try {
                // TODO Auto-generated method stub
                create_rearing_by_age(nid, inDaysAge, inWeekAge, onDate, nidStrain, created);

            } catch (Exception e) {
                e.printStackTrace();
            }
    }


    public void create_rearing_by_hatch_date(String nid, String hatch_date, String strain_id, String created) {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_hatch_date";
        ArrayList < ParamaterHatch.distribution > distribution = new ArrayList < >();


//        for (int i = 0; i < ListchickinDistributionHatch.size(); i++) {
//            ParamaterHatch.distribution dist = new ParamaterHatch.distribution();
//            dist.setHouse_nid(ListchickinDistributionHatch.get(i).getFlock_id());
//            dist.setNumber_of_birds(ListchickinDistributionHatch.get(i).getNumber_of_bird());
//            dist.setStart_recording(ListchickinDistributionHatch.get(i).getStart_recording());
//            Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionHatch.get(i).getNumber_of_bird());
//            distribution.add(dist);
//        }
        for (int i = 0; i < ListchickinDistributionAge.size(); i++) {
            ParamaterHatch.distribution dist = new ParamaterHatch.distribution();
            dist.setHouse_nid(ListchickinDistributionAge.get(i).getFlock_id());
            dist.setNumber_of_birds(ListchickinDistributionAge.get(i).getNumber_of_bird());
            dist.setStart_recording(ListchickinDistributionAge.get(i).getStart_recording());
            Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionAge.get(i).getNumber_of_bird());
            distribution.add(dist);
        }

        Log.d("nid", "nid = " + nid);
        Log.d("hatch_date", "hatch_date = " + hatch_date);
        Log.d("strain_id", "strain_id = " + strain_id);
        Log.d("created", "created = " + created);

        ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(nid, hatch_date, strain_id, distribution, created);
        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterHatch paramater = new ParamaterHatch(module, function_name, args);
        Call< String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_hatch_date(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback< String[] >() {
            @Override
            public void onResponse(Call < String[] > call, Response< String[] > response) {
                //                final String token1  = response.body();
                String[] node = response.body();
                try{
                    int temp = Integer.parseInt(node[0]);
                    Log.d("Layerfarm", "node = " + node);
                    loading.dismiss();
                    AskFinish();
                }catch(Exception e){
                    loading.dismiss();
                    String temp = node[0].replace("<em class=\"placeholder\">","");
                    temp = temp.replace("</em>","");
                    AskOption(temp);
                }

            }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                loading.dismiss();
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });

    }

    public void create_rearing_by_age(String nid, String day, String week, String on_date, String strain_id, String created) {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_age";
        ArrayList < ParamaterAge.distribution > distribution = new ArrayList < >();

        for (int i = 0; i < ListchickinDistributionAge.size(); i++) {
            ParamaterAge.distribution dist = new ParamaterAge.distribution();
            dist.setHouse_nid(ListchickinDistributionAge.get(i).getFlock_id());
            dist.setNumber_of_birds(ListchickinDistributionAge.get(i).getNumber_of_bird());
            dist.setStart_recording(ListchickinDistributionAge.get(i).getStart_recording());
            //Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionAge.get(i).getNumber_of_bird());
            distribution.add(dist);

        }

        ParamaterAge.Arguments args = new ParamaterAge.Arguments(nid, day, week, on_date, strain_id, distribution, created);
        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterAge paramaterage = new ParamaterAge(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramaterage);
        //Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback < String[] > () {
            @Override
            public void onResponse(Call < String[] > call, Response< String[] > response) {
                //                final String token1  = response.body();
                String[] node = response.body();
                try{
                    int temp = Integer.parseInt(node[0]);
                    Log.d("Layerfarm", "node = " + node);
                    loading.dismiss();
                    AskFinish();
                }catch(Exception e){
                    loading.dismiss();
                    String temp = node[0].replace("<em class=\"placeholder\">","");
                    temp = temp.replace("</em>","");
                    AskOption(temp);
                }

            }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                loading.dismiss();
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });
    }

    public void updateDataChickin(View view) {
        //LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Update Date...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want update this data?");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.ic_plus);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                loading = ProgressDialog.show(OnlineEditChickinActivity.this, null, "Please wait...", true, false);
                if (hatchtype.equals("Age")) {
                    //addChickinDistributionAge();
                    int val = 1;
                    if (EdStrain.getText().toString().isEmpty()){
                        EdStrain.setError("Please fill this data");
                        val =0;
                    }
                    if (inWeek.getText().toString().isEmpty()){
                        inWeek.setError("Please fill data");
                        val = 0;
                    }
                    if (inDays.getText().toString().isEmpty()){
                        inDays.setError("Please fill data");
                        val = 0;
                    }
                    if (val == 1){
//                        loading = ProgressDialog.show(ChickinEntryActivity.this, null, "Please wait...", true, false);
                        Log.d("Layerfarm", "age success");
                        layerfarm_send_data_age();

                    }
                    else{
                        loading.dismiss();
                        AskOption("Complete data");
                    }
                } else {
                    int val = 1;
                    if(hatch_date.getText().toString().isEmpty()){
                        hatch_date.setError("Please fill this data");
                        val =0;
                    }
                    if (EdStrain.getText().toString().isEmpty()){
                        EdStrain.setError("Please fill this data");
                        val =0;
                    }
                    if (EdStrainId.getText().toString().isEmpty()){
                        EdStrainId.setError("Please fill this data");
                        val =0;
                    }
                    if (val == 1){
                        Log.d("Layerfarm", "hatchdate success");
                        layerfarm_send_data_hatch_date();
                    }
                    else{
                        loading.dismiss();
                        AskOption("Complete data");
                    }
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        //ChickInMainActivity refresh= new ChickInMainActivity();
        //refresh.Submit();

    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }

    private void getStrain(){
        //get flock from layerfarm isi ke spinner
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_strain";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<getStrainModel>> call = apiInterfaceJson.getStrainSpinner(token2, parameter);

        call.enqueue(new Callback<List<getStrainModel>>() {
            @Override
            public void onResponse(Call<List<getStrainModel>> call, Response<List<getStrainModel>>response) {
                List<getStrainModel> strain = response.body();
                strain_value = new String[strain.size()];
                strainNid = new String[strain.size()];
                if(strain!=null && strain.size()>0) {
                    for (int i = 0; i < strain.size(); i++) {
                        strain_value[i] = strain.get(i).getName();
                        strainNid[i] = strain.get(i).getRid();
                        Log.d("strain_value", "strain_value = " +strain.get(i).getName() + ", NID = " +strain.get(i).getRid());
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<getStrainModel>>call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }


    private AlertDialog AskFinish() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Information!!")
                .setMessage("Upload data finish")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener()
                {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    OnlineEditChickinActivity.this.finish();
                }
                }).show();
        return myQuittingDialogBox;
    }
    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage(notif)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
