package com.layerfarm.chickin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.chickin.R;
import com.layerfarm.chickin.model.ListChickin;

import java.util.List;


public class AdapterListChickin extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ListChickin> items;

    public AdapterListChickin(Activity activity, List<ListChickin> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_data_row_chickin, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView type = (TextView) convertView.findViewById(R.id.typeChickin);
        TextView date = (TextView) convertView.findViewById(R.id.dateChickin);

        ListChickin locationmodel = items.get(position);

        //id.setText(locationmodel.getId());
        type.setText("Chickin Type :"+locationmodel.getType());
        date.setText("Date :"+locationmodel.getDate());
        //rid.setText(locationmodel.getRid());

        return convertView;
    }
}
