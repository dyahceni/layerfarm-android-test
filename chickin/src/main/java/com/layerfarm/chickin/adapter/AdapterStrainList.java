package com.layerfarm.chickin.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.StrainsSync;
import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.List;


public class AdapterStrainList extends BaseAdapter implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<StrainsSync> items;

    private List<StrainsSync>originalData = null;
    private List<StrainsSync>filteredData = null;
    private ItemFilter mFilter = new ItemFilter();

    private LayoutInflater mInflater;

    public AdapterStrainList(Activity activity, List<StrainsSync> items) {
        this.activity = activity;
        this.items = items;
        this.originalData = items;
        this.filteredData = items;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int StrainsSync) {
        return filteredData.get(StrainsSync);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_strain, null);

        TextView id = (TextView) convertView.findViewById(R.id.nid);
        TextView name = (TextView) convertView.findViewById(R.id.name);

        StrainsSync strainModel = filteredData.get(position);

        //id.setText(strainModel.getRid());
        name.setText(strainModel.getName());
        //rid.setText(vendormodel.getRid());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            Log.d("zzz","constraint "+ filterString);
            FilterResults results = new FilterResults();

            final List<StrainsSync> list = originalData;

            int count = list.size();
            final ArrayList<StrainsSync> nlist = new ArrayList<StrainsSync>(count);

            String filterableString ;
//            MedicationVaccination test;

            for (int i = 0; i < count; i++) {
                StrainsSync test;
                test = list.get(i);
                filterableString = list.get(i).getName().toLowerCase();
//                Log.d("zzz","filter "+filterableString);
                if (filterableString.toLowerCase().contains(filterString)) {
                    Log.d("zzz","add "+filterableString);
                    Log.d("zzz","test "+test.getName().toLowerCase());
                    nlist.add(test);
                }
            }
            for (int i =0; i< nlist.size(); i++){
                Log.d("zzz","nlist "+nlist.get(i).getName());
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<StrainsSync>) results.values;
            for (int i =0; i< filteredData.size(); i++){
                Log.d("zzz","filter data = "+filteredData.get(i).getName());
            }
            notifyDataSetChanged();
        }

    }
}
