package com.layerfarm.chickin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.layerfarm.chickin.R;
import com.layerfarm.chickin.model.chickinModel;

import java.util.List;

/**
 * Created by Kuncoro on 22/12/2016.
 */

public class chickinAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<chickinModel> itemsChickinModel;

    public chickinAdapter(Activity activity, List<chickinModel> items) {
        this.activity = activity;
        this.itemsChickinModel = items;
    }

    @Override
    public int getCount() {
        return itemsChickinModel.size();
    }

    @Override
    public Object getItem(int location) {
        return itemsChickinModel.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_medication, null);

        //TextView id = (TextView) convertView.findViewById(R.id.id);
        //TextView name = (TextView) convertView.findViewById(R.id.name);
        //TextView address = (TextView) convertView.findViewById(R.id.address);

        chickinModel data = itemsChickinModel.get(position);

        /*
        id.setText(data.getId());
        name.setText(data.getName());
        address.setText(data.getAddress());
        */

        return convertView;
    }
}
