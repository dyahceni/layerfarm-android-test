package com.layerfarm.chickin.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.chickin.ChickInMainActivity;
import com.layerfarm.chickin.EditChickinActivity;
import com.layerfarm.chickin.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.chickin.model.chickinDistributionModel;

import java.util.ArrayList;

public class FlockAdapter extends RecyclerView.Adapter<FlockAdapter.ViewHolder> {
    private ArrayList<chickinDistributionModel> chickin_List_items;
    private String idData="id";
    private String chick_in_id = "chick_in_id";
    private String flock_id = "flock_id";
    private String number_of_bird = "number_of_bird";
    private String start_recording = "start_recording";
    android.app.AlertDialog.Builder dialog;
    private SQLiteDatabase database;
    DatabaseHelper db;

    public FlockAdapter(ArrayList<chickinDistributionModel> inputData){
        chickin_List_items = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //public TextView kandang;
        public TextView jenis, idchickDist, chickinid, flockid, bird;
        //public TextView dot;
        public CardView card;
        public ViewHolder(View v) {
            super(v);

            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            //kandang = (TextView) v.findViewById(R.id.kandang);
            jenis = (TextView) v.findViewById(R.id.jenis);
            bird = (TextView) v.findViewById(R.id.bird);
            //dot = idchickDist
            card = (CardView) v.findViewById(R.id.cv_flock);
            idchickDist = (TextView) v.findViewById(R.id.id);
            chickinid = (TextView) v.findViewById(R.id.chickinid);
            flockid = (TextView) v.findViewById(R.id.flockid);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.flock_chickin, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        final String idData = chickin_List_items.get(position).getId();
        final String chickinid = chickin_List_items.get(position).getChick_in_id();
        final String flockid = chickin_List_items.get(position).getFlock_id();
        final String location_name = chickin_List_items.get(position).getLocation_name();
        final String type = chickin_List_items.get(position).getType();
        final String flock_name = chickin_List_items.get(position).getName();
        final String number_of_bird = chickin_List_items.get(position).getNumber_of_bird();

        /*
        //Log.d("flock_id","flock:"+id);
        Log.d("flock_id","location name:"+location_name);
        Log.d("flock_id","flock name:"+flock_name);
        Log.d("flock_id","type:"+type);
        Log.d("flock_id","jumlah ayam:"+number_of_bird);
*/

        //holder.kandang.setText(id);
        // Displaying dot from HTML character code
        //holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.idchickDist.setText(idData);
        holder.chickinid.setText(chickinid);
        holder.flockid.setText(flockid);
        holder.jenis.setText(location_name +" - "+ flock_name );
        holder.bird.setText(" - ( "+type+" ) = "+number_of_bird+" birds");


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final Context context = view.getContext();
                final Intent i = new Intent(context, EditChickinActivity.class);
                final Intent i2 = new Intent(context, ChickInMainActivity.class);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new android.app.AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:


                                i.putExtra("id", idData);
                                i.putExtra("chickinid", chickinid);
                                i.putExtra("flock_id",flockid);
                                i.putExtra("location_name",location_name);
                                i.putExtra("type",type);
                                i.putExtra("flock_name",flock_name);
                                i.putExtra("number_of_bird",number_of_bird);
                                Log.e("id ", idData);
                                Log.e("chickinid ", chickinid);
                                Log.e("flockid ", flockid);


                                context.startActivity(i);
                                ((Activity)context).finish();
                                break;
                            case 1:

                                android.app.AlertDialog.Builder builder=new android.app.AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {


                                        //check if in other table
                                        Cursor cursor = null;
                                        String sql ="select cache_hatch_date.chick_in_id from cache_hatch_date\n" +
                                                "INNER join chick_in_distribution on chick_in_distribution.chick_in_id = cache_hatch_date.chick_in_id";
                                        cursor= database.rawQuery(sql,null);
                                        //Log("Cursor Count : " + cursor.getCount());

                                        if(cursor.getCount()>0){
                                            //PID Found
                                            AlertDialog myQuittingDialogBox =new AlertDialog.Builder(view.getContext())
                                                    .setTitle("Warning!!")
                                                    .setMessage("Can't Deleted This Data")
                                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    })
                                                    .show();
                                        }else{
                                            //PID Not Found
                                            String updateQuery = "DELETE FROM chick_in_distribution where id = "+idData +" and chick_in_id= "+chickinid+" and flock_id ="+flock_id+"";
                                            Log.e("update sqlite ", updateQuery);
                                            database.execSQL(updateQuery);
                                            database.close();

                                            ((Activity)context).finish();
                                        }
                                        cursor.close();

                                    }
                                });

                                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                android.app.AlertDialog alert=builder.create();
                                alert.show();


                                break;
                        }
                    }
                }).show();

                /*

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
                // Setting Dialog Title
                alertDialog.setTitle("Confirm Edit...");
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want edit this?");
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_mode_edit);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Context context = view.getContext();
                        Intent i = new Intent(context, EditChickinActivity.class);

                        i.putExtra("id", id);
                        i.putExtra("chickinid", chickinid);
                        i.putExtra("flock_id",flockid);
                        i.putExtra("location_name",location_name);
                        i.putExtra("type",type);
                        i.putExtra("flock_name",flock_name);
                        i.putExtra("number_of_bird",number_of_bird);
                        context.startActivity(i);
                    }
                });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
                */

                /*
                //script delete
                SQLiteDatabase db = SQLite.getWritableDatabase();

                String updateQuery = "UPDATE chick_in_distribution SET number_of_bird =" +number+ " where id ="+id+" and chick_in_id= "+chickinid+" and flock_id ="+flock_id+"";
                Log.e("update sqlite ", updateQuery);
                db.execSQL(updateQuery);
                db.close();
                Intent intent = new Intent(this, ChickInMainActivity.class);
                //Intent intent = new Intent(this, SpinnerTestActivity.class);
                startActivity(intent);
                finish();
                */

                // Showing Alert Message
                //alertDialog.show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return chickin_List_items.size();
    }

}
