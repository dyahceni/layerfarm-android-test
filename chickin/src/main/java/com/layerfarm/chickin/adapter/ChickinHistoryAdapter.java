package com.layerfarm.chickin.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.layerfarm.chickin.OnlineEditChickinActivity;
import com.layerfarm.chickin.R;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.chickinHistory;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChickinHistoryAdapter extends RecyclerView.Adapter<ChickinHistoryAdapter.ChickinHistoryViewHolder>{

    Context c;
    AlertDialog.Builder dialog;
    private View mView;
    private ListView distListLV;
    private ArrayList<String> quesList;
    private ArrayList<String> dataList;

    private List<chickinHistory> chickin;
    private ArrayList<chickinHistory.distribution_history> distribution;
    private int rowLayout;
    private Context context;
    int counter = 0;
    ArrayList<chickinHistory.distribution_history> myChickinDistribution;


    public static class ChickinHistoryViewHolder extends RecyclerView.ViewHolder {
        LinearLayout historyLayout;
        TextView historyNid;
        TextView historyRareType;
        TextView historyhatchdate;

        TextView historyondate;
        TextView historyinweek;
        TextView historyinday;

        TextView historyStrainName;
        TextView distribution_flock_name;
        TextView distribution_period;
        TextView distribution_number_of_bird;
        public TextView dot;

        public ChickinHistoryViewHolder(View v) {
            super(v);
            historyLayout = (LinearLayout) v.findViewById(R.id.chickin_history_layout);
            historyNid = (TextView) v.findViewById(R.id.tv_nid_history);
            historyRareType = (TextView) v.findViewById(R.id.tv_rare_type_history);
            historyhatchdate = (TextView) v.findViewById(R.id.tv_hatching_date_history);
            historyondate = (TextView) v.findViewById(R.id.tv_on_date_history);
            historyinweek = (TextView) v.findViewById(R.id.tv_hatching_in_week_history);
            historyinday = (TextView) v.findViewById(R.id.tv_hatching_in_days_history);
            historyStrainName = (TextView) v.findViewById(R.id.tv_strain_name_history);
            dot = (TextView) v.findViewById(R.id.dot);
            distribution_flock_name = (TextView) v.findViewById(R.id.tv_flock_name);
            distribution_period = (TextView) v.findViewById(R.id.tv_period);
            distribution_number_of_bird = (TextView) v.findViewById(R.id.tv_number_of_bird);
        }
    }

    public ChickinHistoryAdapter(List<chickinHistory> chickin) {
        this.chickin = chickin;
    }

    @Override
    public ChickinHistoryAdapter.ChickinHistoryViewHolder onCreateViewHolder(ViewGroup parent,
                                                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chickin_history, parent, false);


        return new ChickinHistoryViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ChickinHistoryViewHolder holder, final int position) {

        quesList = new ArrayList<String>();
        dataList = new ArrayList<String>();
        quesList.add("3 + 3");
        quesList.add("4 + 3");
        quesList.add("3 + 3");
        quesList.add("6 + 3");
        quesList.add("8 + 3");

        holder.dot.setText(Html.fromHtml("&#8226;"));
        String strain = "Strain : ";
        holder.historyNid.setText(strain + chickin.get(position).getStrain_name());
        holder.historyRareType.setText(chickin.get(position).getRare_type() +" : " + chickin.get(position).getHatching_date());
        //holder.historyondate.setText("On Date: " + chickin.get(position).getAge_on_date());
        //holder.historyinday.setText("Days: " + chickin.get(position).getIn_days());
        //holder.historyinweek.setText("Week: "+chickin.get(position).getIn_week());


        //holder.historyStrainName.setText(chickin.get(position).getStrain_name());
        String value = "";
        final ArrayList<chickinHistory.distribution_history> distribution = chickin.get(position).getDistribution_history_data();

        for (int i =0; i< distribution.size(); i++){
            value += distribution.get(i).getFlock_name()+ " - " + distribution.get(i).getType()+ " - " +distribution.get(i).getNumber_of_bird()+"\n";
            dataList.add(distribution.get(i).getId());
            dataList.add(distribution.get(i).getFlock_name());
            dataList.add(distribution.get(i).getType());
            dataList.add(distribution.get(i).getNumber_of_bird());

        }

        //Log.d("distribution",""+ distribution);
        //Log.d("dataList",""+ dataList);




        holder.distribution_number_of_bird.setText(value);
        //holder.distribution_period.setText(chickin.get(position).getNid());
        //holder.distribution_number_of_bird.setText(distribution.get(i).getNumber_of_birds());

/*        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               *//* Bundle b = new Bundle();
                b.putSerializable("questions", (Serializable) distribution);
                Intent mIntent = new Intent(view.getContext(), OnlineEditChickinActivity.class);
                mIntent.putExtras(b);
                mIntent.putExtra("nid", chickin.get(position).getNid());
                mIntent.putExtra("type", chickin.get(position).getRare_type());
                mIntent.putExtra("hatch_date", chickin.get(position).getHatching_date());
                mIntent.putExtra("strain_name", chickin.get(position).getStrain_name());
                view.getContext().startActivity(mIntent);
*//*
                Context context = view.getContext();
                Intent i = new Intent(context, OnlineEditChickinActivity.class);
                i.putExtra("chickin_distribution", (Serializable) distribution);
                i.putExtra("nid", chickin.get(position).getNid());
                i.putExtra("type", chickin.get(position).getRare_type());
                i.putExtra("hatch_date", chickin.get(position).getHatching_date());
                i.putExtra("strain_name", chickin.get(position).getStrain_name());
                context.startActivity(i);
            }
        });*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.d("zzz","clicked = "+position);
                if (chickin.get(position).getDelete_status().equals("0")){
                    final CharSequence[] dialogitem = {"Edit", "Delete"};
                    dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Context context = view.getContext();
                                    Intent i = new Intent(context, OnlineEditChickinActivity.class);
                                    i.putExtra("chickin_distribution", (Serializable) distribution);
                                    i.putExtra("nid", chickin.get(position).getNid());
                                    i.putExtra("type", chickin.get(position).getRare_type());
                                    i.putExtra("hatch_date", chickin.get(position).getHatching_date());
                                    i.putExtra("on_date", chickin.get(position).getAge_on_date());
                                    i.putExtra("in_day", chickin.get(position).getIn_days());
                                    i.putExtra("in_week", chickin.get(position).getIn_week());
                                    i.putExtra("strain_id", chickin.get(position).getStrain_id());
                                    i.putExtra("strain_name", chickin.get(position).getStrain_name());
                                    context.startActivity(i);
                                    break;
                                case 1:
                                    AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                    builder.setMessage("Do you want to delete this data?");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {

                                            String module = "layerfarm_android";
                                            String function_name = "delete_setting_data";

                                            ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(chickin.get(position).getNid(),"chickin");

                                            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                            String token2 = RetrofitData.getInstance().getToken2();
                                            ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

                                            Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
                                            call.enqueue(new Callback<String>() {
                                                @Override
                                                public void onResponse(Call<String> call, Response<String> response) {

                                                    //if (response != null && response.isSuccessful()) {
                                                    String message = response.body();
                                                    //Log.d("message","get_expense_general_respons : "+message);
                                                    if (message.equalsIgnoreCase("fail")){
                                                        AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
                                                        builder.setTitle("Warning!!");
                                                        builder.setMessage("Chickin Can't Delete!");
                                                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                dialog.dismiss();
                                                                //close activity
                                                                // ((Activity)view.getContext()).finish();
                                                            }
                                                        })
                                                                .show();
                                                    }

                                                    if (message.equalsIgnoreCase("ok")){
                                                        AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
                                                        builder.setTitle("Information!!");
                                                        builder.setMessage("Chickin Data Deleted!");
                                                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                dialog.dismiss();
                                                                //close activity
                                                                // ((Activity)view.getContext()).finish();
                                                                ((Activity)view.getContext()).finish();
                                                            }
                                                        })
                                                                .show();
                                                    }

                                                    //((Activity)view.getContext()).finish();

                                               /* if(mContext instanceof YourActivityName){
                                                    ((YourActivityName)mContext).yourActivityMethod();*/

                                                    //}

                                                }

                                                @Override
                                                public void onFailure(Call<String> call, Throwable t) {
                                                    //String alert = t.getMessage();
                                                    //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                                                }
                                            });


                                       /* String module = "layerfarm_android";
                                        String function_name = "delete_general_expense_data";

                                        String nid = chickin.get(position).getNid();
                                        ParameterDeleteGeneralExpense.Arguments args = new ParameterDeleteGeneralExpense.Arguments(nid);

                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                        String token2 = RetrofitData.getInstance().getToken2();
                                        ParameterDeleteGeneralExpense parameter = new ParameterDeleteGeneralExpense(module, function_name, args);

                                        Call<String[]> call = apiInterfaceJson.layerfarm_delete_general_expense(token2, parameter);
                                        call.enqueue(new Callback<String[]>() {
                                            @Override
                                            public void onResponse(Call<String[]> call, Response<String[]> response) {

                                                if (response != null && response.isSuccessful()) {

                                                    AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
                                                    builder.setTitle("Warning!!");
                                                    builder.setMessage("Chickin Data Deleted!");
                                                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                            //close activity
                                                           // ((Activity)view.getContext()).finish();
                                                        }
                                                    })
                                                            .show();
                                                }
                                                //String[] get_expense_general_respons = response.body();
                                                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());

                                            }

                                            @Override
                                            public void onFailure(Call<String[]> call, Throwable t) {
                                                //String alert = t.getMessage();
                                                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                                            }
                                        });
                                        notifyItemChanged(position);*/


                                        }
                                    });

                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                                    AlertDialog alert=builder.create();
                                    alert.show();
                                    break;
                            }
                        }
                    }).show();
                }
                else {
                    final CharSequence[] dialogitem = {"Edit"};
                    dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Context context = view.getContext();
                                    Intent i = new Intent(context, OnlineEditChickinActivity.class);
                                    i.putExtra("chickin_distribution", (Serializable) distribution);
                                    i.putExtra("nid", chickin.get(position).getNid());
                                    i.putExtra("type", chickin.get(position).getRare_type());
                                    i.putExtra("hatch_date", chickin.get(position).getHatching_date());
                                    i.putExtra("on_date", chickin.get(position).getAge_on_date());
                                    i.putExtra("in_day", chickin.get(position).getIn_days());
                                    i.putExtra("in_week", chickin.get(position).getIn_week());
                                    i.putExtra("strain_id", chickin.get(position).getStrain_id());
                                    i.putExtra("strain_name", chickin.get(position).getStrain_name());
                                    context.startActivity(i);
                                    break;
                            }
                        }
                    }).show();
                }
            }
        });

//        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(final View view) {
//                //mFragment.onItemLongClicked(position);
//                Log.d("zzz","clicked = "+position);
//                if (chickin.get(position).getDelete_status().equals("0")){
//                    final CharSequence[] dialogitem = {"Edit", "Delete"};
//                    dialog = new AlertDialog.Builder(view.getContext());
//                    dialog.setCancelable(true);
//                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // TODO Auto-generated method stub
//                            switch (which) {
//                                case 0:
//                                    Context context = view.getContext();
//                                    Intent i = new Intent(context, OnlineEditChickinActivity.class);
//                                    i.putExtra("chickin_distribution", (Serializable) distribution);
//                                    i.putExtra("nid", chickin.get(position).getNid());
//                                    i.putExtra("type", chickin.get(position).getRare_type());
//                                    i.putExtra("hatch_date", chickin.get(position).getHatching_date());
//                                    i.putExtra("on_date", chickin.get(position).getAge_on_date());
//                                    i.putExtra("in_day", chickin.get(position).getIn_days());
//                                    i.putExtra("in_week", chickin.get(position).getIn_week());
//                                    i.putExtra("strain_id", chickin.get(position).getStrain_id());
//                                    i.putExtra("strain_name", chickin.get(position).getStrain_name());
//                                    context.startActivity(i);
//                                    break;
//                                case 1:
//                                    AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
//                                    builder.setMessage("Do you want to delete this data?");
//                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int id) {
//
//                                            String module = "layerfarm_android";
//                                            String function_name = "delete_setting_data";
//
//                                            ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(chickin.get(position).getNid(),"chickin");
//
//                                            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                            String token2 = RetrofitData.getInstance().getToken2();
//                                            ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);
//
//                                            Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
//                                            call.enqueue(new Callback<String>() {
//                                                @Override
//                                                public void onResponse(Call<String> call, Response<String> response) {
//
//                                                    //if (response != null && response.isSuccessful()) {
//                                                    String message = response.body();
//                                                    //Log.d("message","get_expense_general_respons : "+message);
//                                                    if (message.equalsIgnoreCase("fail")){
//                                                        AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
//                                                        builder.setTitle("Warning!!");
//                                                        builder.setMessage("Chickin Can't Delete!");
//                                                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
//                                                            @Override
//                                                            public void onClick(DialogInterface dialog, int which) {
//                                                                dialog.dismiss();
//                                                                //close activity
//                                                                // ((Activity)view.getContext()).finish();
//                                                            }
//                                                        })
//                                                                .show();
//                                                    }
//
//                                                    if (message.equalsIgnoreCase("ok")){
//                                                        AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
//                                                        builder.setTitle("Information!!");
//                                                        builder.setMessage("Chickin Data Deleted!");
//                                                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
//                                                            @Override
//                                                            public void onClick(DialogInterface dialog, int which) {
//                                                                dialog.dismiss();
//                                                                //close activity
//                                                                // ((Activity)view.getContext()).finish();
//                                                                ((Activity)view.getContext()).finish();
//                                                            }
//                                                        })
//                                                                .show();
//                                                    }
//
//                                                    //((Activity)view.getContext()).finish();
//
//                                               /* if(mContext instanceof YourActivityName){
//                                                    ((YourActivityName)mContext).yourActivityMethod();*/
//
//                                                    //}
//
//                                                }
//
//                                                @Override
//                                                public void onFailure(Call<String> call, Throwable t) {
//                                                    //String alert = t.getMessage();
//                                                    //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                                                }
//                                            });
//
//
//                                       /* String module = "layerfarm_android";
//                                        String function_name = "delete_general_expense_data";
//
//                                        String nid = chickin.get(position).getNid();
//                                        ParameterDeleteGeneralExpense.Arguments args = new ParameterDeleteGeneralExpense.Arguments(nid);
//
//                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                        String token2 = RetrofitData.getInstance().getToken2();
//                                        ParameterDeleteGeneralExpense parameter = new ParameterDeleteGeneralExpense(module, function_name, args);
//
//                                        Call<String[]> call = apiInterfaceJson.layerfarm_delete_general_expense(token2, parameter);
//                                        call.enqueue(new Callback<String[]>() {
//                                            @Override
//                                            public void onResponse(Call<String[]> call, Response<String[]> response) {
//
//                                                if (response != null && response.isSuccessful()) {
//
//                                                    AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext());
//                                                    builder.setTitle("Warning!!");
//                                                    builder.setMessage("Chickin Data Deleted!");
//                                                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener(){
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            dialog.dismiss();
//                                                            //close activity
//                                                           // ((Activity)view.getContext()).finish();
//                                                        }
//                                                    })
//                                                            .show();
//                                                }
//                                                //String[] get_expense_general_respons = response.body();
//                                                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
//
//                                            }
//
//                                            @Override
//                                            public void onFailure(Call<String[]> call, Throwable t) {
//                                                //String alert = t.getMessage();
//                                                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                                            }
//                                        });
//                                        notifyItemChanged(position);*/
//
//
//                                        }
//                                    });
//
//                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.cancel();
//                                        }
//                                    });
//
//                                    AlertDialog alert=builder.create();
//                                    alert.show();
//                                    break;
//                            }
//                        }
//                    }).show();
//                }
//                else {
//                    final CharSequence[] dialogitem = {"Edit"};
//                    dialog = new AlertDialog.Builder(view.getContext());
//                    dialog.setCancelable(true);
//                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // TODO Auto-generated method stub
//                            switch (which) {
//                                case 0:
//                                    Context context = view.getContext();
//                                    Intent i = new Intent(context, OnlineEditChickinActivity.class);
//                                    i.putExtra("chickin_distribution", (Serializable) distribution);
//                                    i.putExtra("nid", chickin.get(position).getNid());
//                                    i.putExtra("type", chickin.get(position).getRare_type());
//                                    i.putExtra("hatch_date", chickin.get(position).getHatching_date());
//                                    i.putExtra("on_date", chickin.get(position).getAge_on_date());
//                                    i.putExtra("in_day", chickin.get(position).getIn_days());
//                                    i.putExtra("in_week", chickin.get(position).getIn_week());
//                                    i.putExtra("strain_id", chickin.get(position).getStrain_id());
//                                    i.putExtra("strain_name", chickin.get(position).getStrain_name());
//                                    context.startActivity(i);
//                                    break;
//                            }
//                        }
//                    }).show();
//                }
//
//                return true;
//            }
//
//        });

    }

    @Override
    public int getItemCount() {
        return chickin.size();
    }

}
