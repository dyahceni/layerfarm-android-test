package com.layerfarm.chickin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView;

import java.text. * ;
import java.util.Date;
import java.util.List;

import com.layerfarm.setting.DateWheel.DatePickerPopWin;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Node;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.ParamaterHatch;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.getStrainModel;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.chickin.listItemChickin.FlockListActivity;
import com.layerfarm.chickin.listItemChickin.StrainListActivity;
import com.layerfarm.chickin.model.chickinDistributionModelAge;
import com.layerfarm.chickin.model.chickinDistributionModelHatch;
import com.layerfarm.chickin.model.distributionModel;
import com.layerfarm.purchase.
        function.Helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.maskededittext.MaskedEditText;
import com.layerfarm.setting.maskededittext.MaskedFormatter;
import com.layerfarm.chickin.database.DBDataSource;
import com.layerfarm.purchase.sql.DatabaseMiddleman;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChickinEntryActivity extends AppCompatActivity {
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener, mDateSetListeneronDate, mDateSetListenerHatch, mDateSetListenerAge;
    int flock_child;
    ProgressDialog loading;
    DatabaseMiddleman mDb;
    CursorAdapter mAdapter;
    private Button saveButton;
    private ImageView remove;
    private EditText on_date,
            start_recording_date,
            edTotalBirdHatch,
            inWeek,
            inDays,
            selected_date_age,
            start_recording_date_age,
            edBirdAge;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    Helper help = new Helper(this);
    public String[] strain_value,
            locationAge_value,
            locationHatch_value,
            locationNid,
            strainNid;
    public String dist[];
    public String url,
            code,
            username,
            password;
    private String hatchtype;
    private ViewGroup more_ageView,
            more_hatchView;

    //inisialisasi array list untuk masing2 pengelompokan di recording
    private ArrayList <chickinDistributionModelAge> ListchickinDistributionAge;
    private ArrayList <chickinDistributionModelHatch> ListchickinDistributionHatch;
    private ArrayList <Integer> ListChickin;
    private ArrayList <distributionModel> ListDistribuiton;


    private MaskedEditText mEdtMaskedCustom;
    private EditText
            rec_date_age,
            rec_date_hatch,
            hatch_date,
            totalBirdAge,
            totalBirdHatch,
            EdStrain,
            EdStrainId,
            EdFlockHatch, EdFlockAge,
            EdFlockHatchID, EdFlockAgeID,  Edon_date ,Edhatch_date;
    private EditText start_recording_date_hatch;
    private MaskedFormatter formatter;
    private DBDataSource dataSource;
    private TextView descChickin;

    public String idFlockData,total_birdData,start_recordingData;

    private String created = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chickin_entry);
        setupUI(findViewById(R.id.parent));

        overridePendingTransition(R.anim.enter, R.anim.exit);

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabsChickin);

        //get value from xml
        Edon_date = (EditText) findViewById(R.id.on_date);
        Edhatch_date = (EditText) findViewById(R.id.hatchDate);
        EdStrain = (EditText) findViewById(R.id.EdStrainOption);
        EdStrainId = (EditText) findViewById(R.id.EdIdStrain);
        descChickin = (TextView) findViewById(R.id.txtDescriptionType);

        inWeek = (EditText) findViewById(R.id.inWeek);
        inDays = (EditText) findViewById(R.id.inDays);

        more_ageView = (ViewGroup) findViewById(R.id.parent_linear_layout_age);
        more_hatchView = (ViewGroup) findViewById(R.id.parent_linear_layout_hatch);

        SQLite = new DataHelper(getApplicationContext());
        loading = ProgressDialog.show(this, null, "Please wait...", true, false);

        //get url information
        SQLiteDatabase dbSQL = db.getReadableDatabase();

        //pilih strain
        EdStrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChickinEntryActivity.this, StrainListActivity.class);
                startActivity(intent);
            }
        });


        //set Date
        Edhatch_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(ChickinEntryActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        Edhatch_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(ChickinEntryActivity.this);

            }
        });

        //set Date
        Edon_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(ChickinEntryActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        Edon_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(ChickinEntryActivity.this);

            }
        });

        getData();
 /*       Cursor cursorsp = dbase.rawQuery("SELECT * FROM flock", null);
        locationHatch_value = new String[cursorsp.getCount()];
        cursorsp.moveToFirst();
        for (int i = 0; i < cursorsp.getCount(); i++) {
            cursorsp.moveToPosition(i);
            locationHatch_value[i] = cursorsp.getString(1).toString();
        }*/

        //spinner 2

     /*   Cursor cursorsp2 = dbase.rawQuery("SELECT * FROM flock", null);
        locationAge_value = new String[cursorsp2.getCount()];
        cursorsp2.moveToFirst();
        for (int i = 0; i < cursorsp2.getCount(); i++) {
            cursorsp2.moveToPosition(i);
            //locationAge_value[i] = cursorsp2.getString(1).toString();
            locationHatch_value[i] = cursorsp2.getString(1).toString();
        }*/

        //Load spLocation value
        //strainName();

/*        chickinMode();

        // inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter < String > adapter = new ArrayAdapter < >(this, android.R.layout.simple_spinner_item, chickinmode);
        // mengeset Array Adapter tersebut ke Spinner
        spChickin.setAdapter(adapter);
        // mengeset listener untuk mengetahui saat item dipilih
        spChickin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {@Override
        public void onItemSelected(AdapterView < ?>adapterView, View view, int i, long l) {
            // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)

            if (adapterView.getSelectedItem().toString() == "Age") {
                //Toast.makeText(adapterView.getContext(), adapterView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.GONE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.VISIBLE);

                Toast.makeText(ChickinEntryActivity.this, "Selected " + adapter.getItem(i) + " Mode", Toast.LENGTH_SHORT).show();

            }

            else {
                CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.VISIBLE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.GONE);

                Toast.makeText(ChickinEntryActivity.this, "Selected " + adapter.getItem(i) + " Mode", Toast.LENGTH_SHORT).show();
            }

        }

            @Override
            public void onNothingSelected(AdapterView < ?>adapterView) {

            }
        });*/

        //set default tab mulai dari 0
        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        ViewHatch();
        ViewAge();
    }
    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(ChickinEntryActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

       // if (requestCode == 1) {
       if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                String flock_name = data.getStringExtra("flock_name");
                String flock_nid = data.getStringExtra("flock_nid");

                if(hatchtype.equals("Age")) {
                    View childViewAge = more_ageView.getChildAt(flock_child);
                    EditText flock_age_ed = (EditText) (childViewAge.findViewById(R.id.morelocationAge));
                    EditText flock_nid_ed = (EditText) (childViewAge.findViewById(R.id.morelocationAgeNid));
                    flock_age_ed.setText(flock_name);
                    flock_nid_ed.setText(flock_nid);
                }else{
                    View childViewHatch = more_hatchView.getChildAt(flock_child);
                    EditText flock_age_ed = (EditText) (childViewHatch.findViewById(R.id.morelocationHatch));
                    EditText flock_nid_ed = (EditText) (childViewHatch.findViewById(R.id.morelocationHatchNid));
                    flock_age_ed.setText(flock_name);
                    flock_nid_ed.setText(flock_nid);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        // Check that it is the SecondActivity with an OK result
       /* if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d("zzz","index child = "+flock_child);
                // Get String data from Intent
               *//* String returnString = data.getStringExtra("keyName");
                Toast.makeText(ChickinEntryActivity.this, returnString, Toast.LENGTH_SHORT).show();*//*

            }
        }*/
    }

    private void onTabTapped(int position) {

        CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
        CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
        switch (position) {
            case 0:
                hatchtype = "Hatch Date";
                descChickin.setText("Choose this option if you know the date when the birds are hatched");
                card_view_hatch.setVisibility(View.VISIBLE);
                card_view_age.setVisibility(View.GONE);
                Toast.makeText(ChickinEntryActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                hatchtype = "Age";
                descChickin.setText("Choose this option if you don’t know when the birds are hatched, but you know the age of the birds.");
                card_view_hatch.setVisibility(View.GONE);
                card_view_age.setVisibility(View.VISIBLE);
                Toast.makeText(ChickinEntryActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            default:

        }

    }

    public void getData(){
        //get flock from layerfarm isi ke spinner
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_entry_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ChickinEntry> call = apiInterfaceJson.get_chickin_data_entry(token2, parameter);

        call.enqueue(new Callback<ChickinEntry>() {
            @Override
            public void onResponse(Call<ChickinEntry> call, Response<ChickinEntry>response) {
                ChickinEntry flock = response.body();

                ChickinEntry.getInstance().setHouse(flock.getHouse());
                ChickinEntry.getInstance().setStrain(flock.getStrain());
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<ChickinEntry>call, Throwable t) {
                loading.dismiss();
                Toast.makeText(ChickinEntryActivity.this, "Can't reach the data "+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Chickin Entry failure = " + t.getMessage());

            }
        });
    }

    public void ViewHatch(){

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.more_hatch, null);
        ImageView buttonRemove = (ImageView) (rowView.findViewById(R.id.remove));
        // Add the new row before the add field button.
        EditText moreFlockHatch = (EditText)(rowView.findViewById(R.id.morelocationHatch));
        final EditText dateHatch = (EditText)(rowView.findViewById(R.id.start_recording_date_hatch));

        moreFlockHatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup vg = ((ViewGroup) rowView.getParent());
                int index = vg.indexOfChild(rowView);
                flock_child = index;
                // Start the SecondActivity
                Intent intent = new Intent(ChickinEntryActivity.this, FlockListActivity.class);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                //Toast.makeText(ChickinEntryActivity.this, "flock_child "+ index, Toast.LENGTH_SHORT).show();
            }
        });

        final View.OnClickListener thisListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ((LinearLayout)rowView.getParent()).removeView(rowView);

            }
        };

        buttonRemove.setOnClickListener(thisListener);

        /*location_hatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //first,  we have to retrieve the item position as a string
                // then, we can change string value into integer
                String item_position = String.valueOf(position);

                int positonInt = Integer.valueOf(item_position);

                Toast.makeText(ChickinEntryActivity.this, "value is "+ positonInt, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.hatchDate));
                EditText mEditTextDate = (EditText)(rowView.findViewById(R.id.start_recording_date_hatch));

                String hatch_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_hatch = sdf.parse(hatch_date_check);
                    if (date_recording.before(date_hatch)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };

        dateHatch.addTextChangedListener(watcher);

        //set Date
        dateHatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                mDateSetListenerHatch = new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        String date = year + "-" + month + "-" + dayOfMonth;
                        dateHatch.setText(date);
                    }
                };

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        ChickinEntryActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerHatch,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(ChickinEntryActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        dateHatch.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(ChickinEntryActivity.this);

            }
        });
        more_hatchView.addView(rowView, more_hatchView.getChildCount());
    }

    public void ViewAge(){

        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.more_age, null);
        // Add the new row before the add field button.
        ImageView buttonRemove = (ImageView)(rowView.findViewById(R.id.remove));
        final EditText moreFlockAge = (EditText)(rowView.findViewById(R.id.morelocationAge));
        final EditText dateAge = (EditText)(rowView.findViewById(R.id.start_recording_date_age));

        moreFlockAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup vg = ((ViewGroup) rowView.getParent());
                int index = vg.indexOfChild(rowView);
                flock_child = index;
                // Start the SecondActivity
                Intent intent = new Intent(ChickinEntryActivity.this, FlockListActivity.class);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                //Toast.makeText(ChickinEntryActivity.this, "flock_child "+ index, Toast.LENGTH_SHORT).show();
            }
        });

        final View.OnClickListener thisListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ((LinearLayout)rowView.getParent()).removeView(rowView);
            }
        };

        buttonRemove.setOnClickListener(thisListener);
        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.on_date));
                EditText mEditTextDate = (EditText)(rowView.findViewById(R.id.start_recording_date_age));

                String on_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_on_date = sdf.parse(on_date_check);
                    if (date_recording.before(date_on_date)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };


        //set Date
        dateAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                mDateSetListenerAge = new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        String date = year + "-" + month + "-" + dayOfMonth;
                        dateAge.setText(date);
                    }
                };

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        ChickinEntryActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerAge,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(ChickinEntryActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        dateAge.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(ChickinEntryActivity.this);

            }
        });

        dateAge.addTextChangedListener(watcher);
        more_ageView.addView(rowView, more_ageView.getChildCount());
    }

    public void AddMoreAge(View view) {
        ViewAge();
    }

    public void AddMoreHatch(View view) {

        ViewHatch();
    }

    public void saveDataChickin(View view) {
        //get value from xml
//        Edon_date = (EditText) findViewById(R.id.on_date);
//        Edhatch_date = (EditText) findViewById(R.id.hatchDate);
//        EdStrain = (EditText) findViewById(R.id.EdStrainOption);
//        EdStrainId = (EditText) findViewById(R.id.EdIdStrain);
//        descChickin = (TextView) findViewById(R.id.txtDescriptionType);
//
//        inWeek = (EditText) findViewById(R.id.inWeek);
//        inDays = (EditText) findViewById(R.id.inDays);
//        if (Edon_date.getText().toString().isEmpty()){
//            Edon_date.setError("Please fill this data");
//            Log.d("zzz","kosong tanggalnya");
//        }

//        //LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Data Save...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want save this data?");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.ic_plus);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                //if (spnLocale.getSelectedItem().toString() == "Age") {
                if (hatchtype.equals("Age")) {
                    //Toast.makeText(ChickinEntryActivity.this, "save age ", Toast.LENGTH_SHORT).show();
                    int val = 1;
                    if (EdStrain.getText().toString().isEmpty()){
                        EdStrain.setError("Please fill this data");
                        val =0;
                    }
                    if (inWeek.getText().toString().isEmpty()){
                        inWeek.setError("Please fill data");
                        val = 0;
                    }
                    if (inDays.getText().toString().isEmpty()){
                        inDays.setError("Please fill data");
                        val = 0;
                    }
                    if (val == 1){
//                        loading = ProgressDialog.show(ChickinEntryActivity.this, null, "Please wait...", true, false);
                        Log.d("Layerfarm", "age success");
                        layerfarm_send_data_age();

                    }
                    else{
                        AskOption();
                    }
                                //create_rearing_by_age_test();
                }
                else {
                    //Toast.makeText(ChickinEntryActivity.this, "save hatch ", Toast.LENGTH_SHORT).show();
                    int val = 1;
                    if(Edhatch_date.getText().toString().isEmpty()){
                        Edhatch_date.setError("Please fill this data");
                        val =0;
                    }
                    if (EdStrain.getText().toString().isEmpty()){
                        EdStrain.setError("Please fill this data");
                        val =0;
                    }
                    if (EdStrainId.getText().toString().isEmpty()){
                        EdStrainId.setError("Please fill this data");
                        val =0;
                    }
                    if (val == 1){
                        Log.d("Layerfarm", "hatchdate success");
                        layerfarm_send_data_hatch_date();
                    }
                    else{
                        AskOption();
                    }

                                //finish();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        //ChickInMainActivity refresh= new ChickInMainActivity();
        //refresh.Submit();
    }


    public ArrayList < chickinDistributionModelHatch > getListHatch() {
        ListchickinDistributionHatch = new ArrayList < >();
        ListChickin = new ArrayList < >();
        String showallPrompt = "";
        int childCount = more_hatchView.getChildCount();
        showallPrompt += "chilcCount :" + childCount + "\n";
        //Toast.makeText(ChickinEntryActivity.this,"Jumlah:"+childCount , Toast.LENGTH_SHORT).show();

        for (int c = 0; c < childCount; c++) {
            EditText x = (EditText)(findViewById(R.id.hatchDate));
            String hatch_date_check = (String)(x.getText().toString());

            chickinDistributionModelHatch chickinHatch = new chickinDistributionModelHatch();
            View childView = more_hatchView.getChildAt(c);
            EditText a = (EditText)(childView.findViewById(R.id.morelocationHatchNid));
            String flock_nid = (String)(a.getText().toString());
            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_hatch));
            String start_recording = (String)(b.getText().toString());
            EditText d = (EditText)(childView.findViewById(R.id.edBirdHatch));
            String total_bird = (String)(d.getText().toString());

            chickinHatch.setFlock_id(flock_nid);
            chickinHatch.setStart_recording(start_recording);
            chickinHatch.setNumber_of_bird(total_bird);
            ListchickinDistributionHatch.add(chickinHatch);
        }
        //Toast.makeText(this,showallPrompt,Toast.LENGTH_LONG).show();
        return ListchickinDistributionHatch;
    }


    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please Complate data")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener()
                {@Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
        }).show();
        return myQuittingDialogBox;
    }
    private AlertDialog CloseAskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setIcon(R.drawable.check_mark)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
//                        Fragment fragment = new RecordingFragment();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }
    private AlertDialog AskDistribution() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please fill distribution data correctly")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener()
                {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return myQuittingDialogBox;
    }

    private AlertDialog AskFinish() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Information!!")
                .setMessage("Upload data finish")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener()
                {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
                }).show();
        return myQuittingDialogBox;
    }

  /*  private TextWatcher mHatchDateWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean isValid = true;
            EditText x = (EditText)(findViewById(R.id.hatchDate));
            String hatch_date_check = (String)(x.getText().toString());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SQLiteDatabase dbase = db.getWritableDatabase();

                //get last record chickin
                Cursor cursor = dbase.rawQuery("SELECT hatch_date FROM chick_in WHERE hatch_date='" + hatch_date_check + "' AND chick_in_type ='Hatch Date'", null);
                String hatch_date_exist = "";
                if (cursor.moveToFirst()) hatch_date_exist = cursor.getString(cursor.getColumnIndex("hatch_date"));
                cursor.close();

                Date date_recording = sdf.parse(hatch_date_exist);
                Date date_hatch = sdf.parse(hatch_date_check);
                if (date_recording.after(date_hatch) || date_recording.equals(date_hatch)) {
                    isValid = false;

                } else {
                    isValid = true;

                }

            } catch(Exception e) {
                e.printStackTrace();
            }

            if (!isValid) {
                x.setError("Hatch Date Exist, Please Use Other Hatch Date");
                x.setFocusable(true);
            } else {
                x.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    };*/

    private TextWatcher mDateEntryWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean isValid = true;

            EditText mDateEntryField = (EditText)(findViewById(R.id.hatchDate));
            EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_hatch));

            String hatch_date_check = (String)(mDateEntryField.getText().toString());
            String start_recording = (String)(mEditTextDate.getText().toString());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date_recording = sdf.parse(start_recording);
                Date date_hatch = sdf.parse(hatch_date_check);
                if (date_recording.before(date_hatch)) {
                    isValid = false;

                } else {
                    isValid = true;

                }

            } catch(Exception e) {
                e.printStackTrace();
            }

            if (!isValid) {
                mEditTextDate.setError("Recording date can't smaller than hatch date");
                mEditTextDate.setFocusable(true);
            } else {
                mEditTextDate.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    };

   /* private TextWatcher mAgeDateWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean isValid = true;
            EditText x = (EditText)(findViewById(R.id.on_date));
            String hatch_date_check = (String)(x.getText().toString());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SQLiteDatabase dbase = db.getWritableDatabase();

                //get last record chickin
                Cursor cursor = dbase.rawQuery("SELECT age_on_date FROM chick_in WHERE hatch_date='" + hatch_date_check + "' AND chick_in_type ='Age'", null);
                String hatch_date_exist = "";
                if (cursor.moveToFirst()) hatch_date_exist = cursor.getString(cursor.getColumnIndex("hatch_date"));
                cursor.close();

                Date date_recording = sdf.parse(hatch_date_exist);
                Date date_hatch = sdf.parse(hatch_date_check);
                if (date_recording.after(date_hatch) || date_recording.equals(date_hatch)) {
                    isValid = false;

                } else {
                    isValid = true;

                }

            } catch(Exception e) {
                e.printStackTrace();
            }

            if (!isValid) {
                x.setError("Hatch Date Exist, Please Use Other Hatch Date");
                x.setFocusable(true);
            } else {
                x.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    };*/

    private TextWatcher mOnDateEntryWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean isValid = true;

            EditText mDateEntryField = (EditText)(findViewById(R.id.on_date));
            EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_age));

            String hatch_date_check = (String)(mDateEntryField.getText().toString());
            String start_recording = (String)(mEditTextDate.getText().toString());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date_recording = sdf.parse(start_recording);
                Date date_hatch = sdf.parse(hatch_date_check);
                if (date_recording.before(date_hatch)) {
                    isValid = false;

                } else {
                    isValid = true;

                }

            } catch(Exception e) {
                e.printStackTrace();
            }

            if (!isValid) {
                mEditTextDate.setError("Recording date can't smaller than hatch date");
                mEditTextDate.setFocusable(true);
            } else {
                mEditTextDate.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    };

    public void create_rearing_by_hatch_test() {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_hatch_date";
        ArrayList < ParamaterHatch.distribution > distribution = new ArrayList < >();
        ParamaterHatch.distribution dist = new ParamaterHatch.distribution();

        dist.setHouse_nid("22173");
        dist.setNumber_of_birds("999");
        dist.setStart_recording("2019-07-29");

        distribution.add(dist);

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        ParamaterHatch.Arguments args = new ParamaterHatch.Arguments("","2019-07-28", "26543", distribution, "");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterHatch paramater = new ParamaterHatch(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_hatch_date(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback < String[] > () {@Override
        public void onResponse(Call < String[] > call, Response < String[] > response) {
            //                final String token1  = response.body();
            String[] node = response.body();
            Log.d("Layerfarm", "node = " + node);
        }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });
    }

    public void create_rearing_by_hatch_date(String nid, String hatch_date, String strain_id, String created) {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_hatch_date";
        ArrayList < ParamaterHatch.distribution> distribution = new ArrayList<>();

        for (int i = 0; i < ListchickinDistributionHatch.size(); i++) {
            ParamaterHatch.distribution dist = new ParamaterHatch.distribution();
            dist.setHouse_nid(ListchickinDistributionHatch.get(i).getFlock_id());
            dist.setNumber_of_birds(ListchickinDistributionHatch.get(i).getNumber_of_bird());
            dist.setStart_recording(ListchickinDistributionHatch.get(i).getStart_recording());
            //Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionHatch.get(i).getNumber_of_bird());
            distribution.add(dist);

        }

        nid = "";
        ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(nid, hatch_date, strain_id, distribution, created);
        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterHatch paramater = new ParamaterHatch(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_hatch_date(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback < String[] > () {@Override
        public void onResponse(Call < String[] > call, Response < String[] > response) {
            //                final String token1  = response.body();
            String[] node = response.body();
            try{
                int temp = Integer.parseInt(node[0]);
                Log.d("Layerfarm", "node = " + node);
                loading.dismiss();
                AskFinish();
            }catch(Exception e){
                loading.dismiss();
                String temp = node[0].replace("<em class=\"placeholder\">","");
                temp = temp.replace("</em>","");
                AskOption(temp);
            }
//            AskFinish();
        }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                Toast.makeText(ChickinEntryActivity.this, "Failed to upload data", Toast.LENGTH_SHORT).show();
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });

    }

    public void create_rearing_by_age(String no_id, String day, String week, String on_date, String strain_id, String created) {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_age";
        ArrayList < ParamaterAge.distribution > distribution = new ArrayList < >();

        for (int i = 0; i < ListchickinDistributionAge.size(); i++) {
            ParamaterAge.distribution dist = new ParamaterAge.distribution();
            dist.setHouse_nid(ListchickinDistributionAge.get(i).getFlock_id());
            dist.setNumber_of_birds(ListchickinDistributionAge.get(i).getNumber_of_bird());
            dist.setStart_recording(ListchickinDistributionAge.get(i).getStart_recording());
            Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionAge.get(i).getNumber_of_bird());
            distribution.add(dist);

        }

        no_id ="";
        ParamaterAge.Arguments args = new ParamaterAge.Arguments(no_id, day, week, on_date, strain_id, distribution, created);
        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterAge paramaterage = new ParamaterAge(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramaterage);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback < String[] > () {@Override
        public void onResponse(Call < String[] > call, Response < String[] > response) {
            //                final String token1  = response.body();
            String[] node = response.body();
            try{
                int temp = Integer.parseInt(node[0]);
                Log.d("Layerfarm", "node = " + node);
                loading.dismiss();
                AskFinish();
            }catch(Exception e){
                loading.dismiss();
                String temp = node[0].replace("<em class=\"placeholder\">","");
                temp = temp.replace("</em>","");
                AskOption(temp);
            }
//            AskFinish();
        }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                Toast.makeText(ChickinEntryActivity.this,"Failed to upload data",Toast.LENGTH_SHORT).show();
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });
    }

    public void delete_draft_data() {

        SQLiteDatabase dbase = db.getWritableDatabase();
        //get last record chickin
        String selectQuery = "SELECT * FROM chick_in ORDER BY id DESC LIMIT 1";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if (cursor.moveToFirst()) strlastchickin = cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();

        //delete distribution
        String delchickindistributionQuery = "DELETE FROM chick_in_distribution WHERE chick_in_id =" + "'" + strlastchickin + "'";
        Log.e("delete sqlite ", delchickindistributionQuery);
        dbase.execSQL(delchickindistributionQuery);
        dbase.close();

        //delete distribution
        String delchickinQuery = "DELETE FROM chick_in WHERE id =" + "'" + strlastchickin + "'";
        Log.e("delete sqlite ", delchickinQuery);
        dbase.execSQL(delchickinQuery);
        dbase.close();
    }

    private void layerfarm_send_data_hatch_date() {

        List < Integer > ListChickin = new ArrayList < >();
        List<Integer> validation = new ArrayList<>();
        ListchickinDistributionHatch = new ArrayList < >();
        String showallPrompt = "";
        int childCount = more_hatchView.getChildCount();
        showallPrompt += "chilcCount :" + childCount + "\n";
        int valid = 1;
        SQLite = new DataHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();

        for (int c = 0; c < childCount; c++) {

            chickinDistributionModelHatch chickinHatch = new chickinDistributionModelHatch();

            EditText x = (EditText)(findViewById(R.id.hatchDate));
            String hatch_date_check = (String)(x.getText().toString());

            View childView = more_hatchView.getChildAt(c);
            EditText a = (EditText)(childView.findViewById(R.id.morelocationHatchNid));
            String nidFlock = (String)(a.getText().toString());
            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_hatch));
            String start_recording = (String)(b.getText().toString());
            EditText d = (EditText)(childView.findViewById(R.id.edBirdHatch));
            String total_bird = (String)(d.getText().toString());

            if (a.getText().toString().isEmpty() || b.getText().toString().isEmpty() || d.getText().toString().isEmpty()){
                Log.d("zzz", "kosong");
                AskDistribution();
                valid = 0;
            }
            else {
                //compare two date
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_hatch = sdf.parse(hatch_date_check);
                    if (date_recording.after(date_hatch) || date_recording.equals(date_hatch)) {
                        //Toast.makeText(ChickinEntryActivity.this, "Tgl Recording lebih tinggi", Toast.LENGTH_SHORT).show();
                        ListChickin.add(1);
                    } else {
                        //Toast.makeText(ChickinEntryActivity.this, "Tgl Recording lebih rendah", Toast.LENGTH_SHORT).show();
                        ListChickin.add(0);
                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                chickinHatch.setFlock_id(nidFlock);
                chickinHatch.setStart_recording(start_recording);
                chickinHatch.setNumber_of_bird(total_bird);
                ListchickinDistributionHatch.add(chickinHatch);
                valid = 1;
            }


        }

        //Toast.makeText(ChickinEntryActivity.this, "List Chick In = " + showallPrompt, Toast.LENGTH_SHORT).show();
        if (ListChickin.contains(0) || ListchickinDistributionHatch.isEmpty()) {
            //Toast.makeText(ChickinEntryActivity.this, "Please Entry Recording Date ", Toast.LENGTH_SHORT).show();
            AskOption();
            //finish();
        } else {

            //send data to layerfarm............................./
            String nidStrain = EdStrainId.getText().toString().trim();
            String hatch_date_result = (String)(Edhatch_date.getText().toString());


            try {
                Log.d("hatch_date_result", "tgl = " + hatch_date_result);
                // TODO Auto-generated method stub

                loading = ProgressDialog.show(ChickinEntryActivity.this, null, "Please wait...", true, false);
                String nid ="";
                create_rearing_by_hatch_date(nid, hatch_date_result, nidStrain, created);
//                loading.dismiss();

            } catch(Exception e) {
                e.printStackTrace();
            }

        }


    }

    private void layerfarm_send_data_age() {

        List < Integer > ListChickin = new ArrayList < >();
        ListchickinDistributionAge = new ArrayList < >();
        String showallPrompt = "";
        int childCount = more_ageView.getChildCount();
        showallPrompt += "chilcCount :" + childCount + "\n";
        //Toast.makeText(ChickinEntryActivity.this,"Jumlah:"+childCount , Toast.LENGTH_SHORT).show();

        SQLite = new DataHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();

        for (int c = 0; c < childCount; c++) {

            chickinDistributionModelAge chickinAge = new chickinDistributionModelAge();

            EditText x = (EditText)(findViewById(R.id.on_date));
            String hatch_date_check = (String)(x.getText().toString());

            View childView = more_ageView.getChildAt(c);
            EditText a = (EditText)(childView.findViewById(R.id.morelocationAgeNid));
            String nidFlock = (String)(a.getText().toString());
            EditText b = (EditText)(childView.findViewById(R.id.start_recording_date_age));
            String start_recording = (String)(b.getText().toString());
            EditText d = (EditText)(childView.findViewById(R.id.edBirdAge));
            String total_bird = (String)(d.getText().toString());

            if (a.getText().toString().isEmpty() || b.getText().toString().isEmpty() || d.getText().toString().isEmpty()){
                Log.d("zzz", "kosong");
                AskDistribution();
            }
            else {
                //compare two date
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_hatch = sdf.parse(hatch_date_check);
                    if (date_recording.after(date_hatch) || date_recording.equals(date_hatch)) {
                        //Toast.makeText(ChickinEntryActivity.this, "Tgl Recording lebih tinggi", Toast.LENGTH_SHORT).show();
                        ListChickin.add(1);
                    } else {
                        //Toast.makeText(ChickinEntryActivity.this, "Tgl Recording lebih rendah", Toast.LENGTH_SHORT).show();
                        ListChickin.add(0);
                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                chickinAge.setFlock_id(nidFlock);
                chickinAge.setStart_recording(start_recording);
                chickinAge.setNumber_of_bird(total_bird);
                ListchickinDistributionAge.add(chickinAge);
            }


        }

        if (ListChickin.contains(0) || ListchickinDistributionAge.isEmpty()) {
            AskOption();

        } else {

            //send data to layerfarm............................./
            //String strain = spStrain.getSelectedItem().toString();
            String nidStrain = (String)(EdStrainId.getText().toString());
            String onDate = Edon_date.getText().toString().trim();
            String inWeekAge = inWeek.getText().toString().trim();
            String inDaysAge = inDays.getText().toString().trim();


            try {

                // TODO Auto-generated method stub
                String nid ="";
                loading = ProgressDialog.show(ChickinEntryActivity.this, null, "Please wait...", true, false);
                create_rearing_by_age(nid, inDaysAge, inWeekAge, onDate, nidStrain, created);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //finish();

    }

    public void create_rearing_by_age_test() {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_age";
        ArrayList < ParamaterAge.distribution > distribution = new ArrayList < >();
        ParamaterAge.distribution dist = new ParamaterAge.distribution();

        dist.setHouse_nid("22173");
        dist.setNumber_of_birds("999");
        dist.setStart_recording("2019-08-12");

        distribution.add(dist);

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        ParamaterAge.Arguments args = new ParamaterAge.Arguments("","2", "23","2019-08-11","26543", distribution,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterAge paramater = new ParamaterAge(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        call.enqueue(new Callback < String[] > () {@Override
        public void onResponse(Call < String[] > call, Response < String[] > response) {
            //                final String token1  = response.body();
            String[] node = response.body();
            Log.d("Layerfarm", "node = " + node);
            loading.dismiss();
        }

            @Override
            public void onFailure(Call < String[] > call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }

    private void getStrain(){
        //get flock from layerfarm isi ke spinner
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_strain";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<getStrainModel>> call = apiInterfaceJson.getStrainSpinner(token2, parameter);

        call.enqueue(new Callback<List<getStrainModel>>() {
            @Override
            public void onResponse(Call<List<getStrainModel>> call, Response<List<getStrainModel>>response) {
                List<getStrainModel> strain = response.body();
                try {
                strain_value = new String[strain.size()];
                strainNid = new String[strain.size()];
                if(strain!=null && strain.size()>0) {
                    for (int i = 0; i < strain.size(); i++) {
                        strain_value[i] = strain.get(i).getName();
                        strainNid[i] = strain.get(i).getRid();
                        Log.d("strain_value", "strain_value = " +strain.get(i).getName() + ", NID = " +strain.get(i).getRid());
                    }
                    loading.dismiss();
                }
                }catch (Exception e){
                    //e.printStackTrace();
                    Toast.makeText(ChickinEntryActivity.this,"No Data",Toast.LENGTH_LONG).show();
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<getStrainModel>>call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EdStrain.setText(StrainListActivity.getInstance().getStrain());
        EdStrainId.setText(StrainListActivity.getInstance().getStrainNid());
    }

    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage(notif)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    public void close_input_chickin(View view) {
        Log.d("zzz","close");
        AlertDialog diaBox = CloseAskOption();
        diaBox.show();
    }
}