package com.layerfarm.chickin;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

public class EditChickinActivity extends AppCompatActivity {
    DatabaseHelper SQLite = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_chickin);
        setupUI(findViewById(R.id.parent));


        EditText hatchDate = (EditText) findViewById(R.id.hatchDateEditText);
        EditText tvData1 = (EditText) findViewById(R.id.idChickInEditText);
        EditText tvData2 = (EditText) findViewById(R.id.chickinidEditText);
        EditText tvData3 = (EditText) findViewById(R.id.flockidEditText);
        EditText tvData4 = (EditText) findViewById(R.id.location_nameEditText);
        EditText tvData5 = (EditText) findViewById(R.id.typeEditText);
        EditText tvData6 = (EditText) findViewById(R.id.flock_nameEditText);
        EditText tvData7 = (EditText) findViewById(R.id.number_of_birdEditText);
        tvData7.requestFocus();
        hatchDate.setEnabled(false);
        tvData4.setEnabled(false);
        tvData5.setEnabled(false);
        tvData6.setEnabled(false);


        tvData1.setText(getIntent().getStringExtra("id"));
        tvData2.setText(getIntent().getStringExtra("chickinid"));
        tvData3.setText(getIntent().getStringExtra("flock_id"));
        tvData4.setText(getIntent().getStringExtra("location_name"));
        tvData5.setText(getIntent().getStringExtra("type"));
        tvData6.setText(getIntent().getStringExtra("flock_name"));
        tvData7.setText(getIntent().getStringExtra("number_of_bird"));

        String id = (String)(tvData1.getText().toString());
        String chickinid = (String)(tvData2.getText().toString());
        String flock_id = (String)(tvData3.getText().toString());
        String location_name = (String)(tvData4.getText().toString());
        String type = (String)(tvData5.getText().toString());
        String flock_name = (String)(tvData6.getText().toString());
        String number_of_bird = (String)(tvData7.getText().toString());

        //get hatchdate from chickin ID

        SQLite = new DatabaseHelper(this);
        SQLiteDatabase db = SQLite.getWritableDatabase();
        String selectQuery = "SELECT * FROM chick_in ORDER BY id DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String strHatchDate = "";
        if (cursor.moveToFirst())
            strHatchDate = cursor.getString(cursor.getColumnIndex("hatch_date"));
        cursor.close();

        hatchDate.setText(strHatchDate);

    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(EditChickinActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public void editDataChickin(View view) {

        EditText number_of_bird = (EditText) findViewById(R.id.number_of_birdEditText);
        EditText tvData1 = (EditText) findViewById(R.id.idChickInEditText);
        EditText tvData2 = (EditText) findViewById(R.id.chickinidEditText);
        EditText tvData3 = (EditText) findViewById(R.id.flockidEditText);

        String number = (String)(number_of_bird.getText().toString());
        String id = (String)(tvData1.getText().toString());
        String chickinid = (String)(tvData2.getText().toString());
        String flock_id = (String)(tvData3.getText().toString());

        if (number.isEmpty()) {
            number_of_bird.setError("Number of bird can't be blank");
            number_of_bird.requestFocus();
            return;
        }else{

            SQLiteDatabase db = SQLite.getWritableDatabase();

            String updateQuery = "UPDATE chick_in_distribution SET number_of_bird =" +number+ " where id ="+id+" and chick_in_id= "+chickinid+" and flock_id ="+flock_id+"";
            Log.e("update sqlite ", updateQuery);
            db.execSQL(updateQuery);
            db.close();
            Intent intent = new Intent(this, ChickInMainActivity.class);
            //Intent intent = new Intent(this, SpinnerTestActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
