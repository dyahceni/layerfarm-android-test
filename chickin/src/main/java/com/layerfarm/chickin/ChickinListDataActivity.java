package com.layerfarm.chickin;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.layerfarm.chickin.adapter.AdapterListChickin;
import com.layerfarm.chickin.model.ListChickin;
import com.layerfarm.setting.DataHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChickinListDataActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    List<ListChickin> itemList = new ArrayList<ListChickin>();
    AdapterListChickin adapter;
    DataHelper SQLite = new DataHelper(this);

    public static final String TAG_ID = "id";
    public static final String TAG_TYPE = "type";
    public static final String TAG_DATE = "date";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data);

        SQLite = new DataHelper(getApplicationContext());
        listView = (ListView) findViewById(R.id.list_view_chickin_data);


        adapter = new AdapterListChickin(ChickinListDataActivity.this, itemList);
        listView.setAdapter(adapter);


    }


    private void getAllData() {
        ArrayList<HashMap<String, String>> row = SQLite.getAllChickinList();

        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get(TAG_ID);
            String type = row.get(i).get(TAG_TYPE);
            String date = row.get(i).get(TAG_DATE);



            ListChickin data = new ListChickin();

            data.setId(id);
            data.setType(type);
            data.setDate(date);


            itemList.add(data);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        getAllData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_chickin_entry) {
            Intent intent = new Intent(this, ChickInMainActivity.class);
            this.startActivity(intent);

        }else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }
}
