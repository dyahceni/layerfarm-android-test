package com.layerfarm.chickin.listItemChickin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.layerfarm.chickin.adapter.AdapterFlockList;
import com.layerfarm.chickin.R;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.mFlock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FlockListActivity extends AppCompatActivity {

    ProgressDialog loading;
    ListView listView;
    EditText inputSearch;
    List<mFlock> itemList = new ArrayList<mFlock>();
    AdapterFlockList adapter;
    List<mFlock> flockList = new ArrayList<>();
    private static String flock_items;
    private static String flock_items_nid;
    private static String ed_items;
    private static FlockListActivity staticInstance;
    private String idEd;
    HashMap<String, String> list_flock = new HashMap<>();

    public static FlockListActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new FlockListActivity();
        }
        return staticInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flock_list);

        listView = (ListView) findViewById(R.id.list_view_flock_option);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        adapter = new AdapterFlockList(FlockListActivity.this, itemList);
        listView.setAdapter(adapter);

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                FlockListActivity.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                mFlock item = (mFlock) adapter.getItem(position);
//                mFlock item = itemList.get(position);
//                final String name = listView.getItemAtPosition(position).toString();
                final String name = item.getName();
                Log.d("zzz","flock name = "+name);
//                final String nid = getKey(list_flock, name);
                final String nid = item.getNid();
                Log.d("zzz","flock nid = "+nid);
                setFlockName(name);
                setFlockNid(nid);
                Intent intent = new Intent();
                intent.putExtra("flock_name", name);
                intent.putExtra("flock_nid", nid);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }



    private void get_flock(){
        loading = ProgressDialog.show(this, null, "Please wait...", true, false);

        if (!ChickinEntry.getInstance().getHouse().isEmpty()){
            LinkedHashMap<String, String> house = ChickinEntry.getInstance().getHouse();
            for ( Map.Entry<String, String> entry : house.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                mFlock data = new mFlock();
                data.setNid(key);
                data.setName(value);
                itemList.add(data);
                // do something with key and/or tab
            }
            adapter.notifyDataSetChanged();
            loading.dismiss();
        }
        else {
            Toast.makeText(FlockListActivity.this,"No Data",Toast.LENGTH_LONG).show();
            loading.dismiss();
        }

        //get flock from layerfarm isi ke spinner
//        String module = "layerfarm_android";
//        String function_name = "layerfarm_android_get_flock_active";
//        String[] args = {};
//        Parameter parameter = new Parameter(module, function_name, args);
//
//        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//        String token2 = RetrofitData.getInstance().getToken2();
//        Call<List<mFlock>> call = apiInterfaceJson.getFlock_1(token2, parameter);
//
//        call.enqueue(new Callback<List<mFlock>>() {
//            @Override
//            public void onResponse(Call<List<mFlock>> call, Response<List<mFlock>> response) {
//                flockList = response.body();
//                try {
//                    for (int i =0; i< flockList.size() ; i++){
//                        String nid = flockList.get(i).getNid();
//                        String name = flockList.get(i).getName();
//
//
//                        mFlock data = new mFlock(nid, name,"","","","","");
//                        itemList.add(data);
//    //                    list_flock.put(nid, name);
//                    }
//                    adapter.notifyDataSetChanged();
//                    loading.dismiss();
//                }catch (Exception e){
//                    //e.printStackTrace();
//                    Toast.makeText(FlockListActivity.this,"No Data",Toast.LENGTH_LONG).show();
//                    loading.dismiss();
//                }
//                for (int i =0; i< flockList.size() ; i++){
//                    String nid = flockList.get(i).getNid();
//                    String name = flockList.get(i).getName();
//                    Log.d("zzz","flock name = "+name);
//                    Log.d("zzz","flock nid = "+nid);
//
//                    list_flock.put(nid, name);
//                    //                    list_flock.put(nid, name);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<mFlock>>call, Throwable t) {
//                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        get_flock();
    }

    public void setFlockName(String warehouse_items){
        this.flock_items = warehouse_items;
    }
    public String getFlockName(){
        return flock_items;
    }

    public void setFlockNid(String nid_items){
        this.flock_items_nid = nid_items;
    }
    public String getFlockNid(){
        return flock_items_nid;
    }


    public void setEdID(String warehouse_items){
        this.ed_items = warehouse_items;
    }
    public String getEdID(){
        return ed_items;
    }


    public void cls_flock(View view) {
/*        Intent intent = new Intent();
        String name = "";
        String nid = "";
        intent.putExtra("flock_name", name);
        intent.putExtra("flock_nid", nid);
        setResult(RESULT_OK, intent);*/
        finish();
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
