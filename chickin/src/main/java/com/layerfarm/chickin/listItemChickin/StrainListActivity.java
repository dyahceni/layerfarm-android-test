package com.layerfarm.chickin.listItemChickin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.layerfarm.chickin.adapter.AdapterStrainList;
import com.layerfarm.chickin.R;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.StrainsSync;

import java.util.ArrayList;
import java.util.List;

public class StrainListActivity extends AppCompatActivity {
    ProgressDialog loading;
    ListView listView;
    EditText inputSearch;
    List<StrainsSync> itemList = new ArrayList<StrainsSync>();
    AdapterStrainList adapter;
    List<StrainsSync> strainList = new ArrayList<>();
    private static String strain_items;
    private static String strain_items_nid;
    private static String ed_items;
    private static StrainListActivity staticInstance;


    public static StrainListActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new StrainListActivity();
        }
        return staticInstance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strain_list);
        listView = (ListView) findViewById(R.id.list_view_strain_option);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        adapter = new AdapterStrainList(StrainListActivity.this, itemList);
        listView.setAdapter(adapter);


        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                StrainListActivity.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                StrainsSync item = (StrainsSync) adapter.getItem(position);
                final String nid = item.getRid();
                final String name = item.getName();
                setStrain(name);
                setStrainNid(nid);

                // Put the String to pass back into an Intent and close this activity
                Intent intent = new Intent();
                intent.putExtra("strainId", nid);
                intent.putExtra("strainName", name);
                setResult(RESULT_OK, intent);
                finish();

            }
        });
      /*  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = listView.getItemAtPosition(position).toString();
                setStrain(s);
                finish();
            }
        });*/
    }

    public void get_strain(){
        loading = ProgressDialog.show(this, null, "Please wait...", true, false);
        if (!ChickinEntry.getInstance().getStrain().isEmpty()){
            strainList = ChickinEntry.getInstance().getStrain();
            for (int i =0; i< strainList.size(); i++){
                String name = strainList.get(i).getName();
                String rid = strainList.get(i).getRid();
                if (name != null){
                    Log.d("name", "name = " + name);
                    Log.d("rid", "rid = " + rid);
                    StrainsSync data = new StrainsSync(name, rid);
                    itemList.add(data);
                }
            }
            adapter.notifyDataSetChanged();
            loading.dismiss();
        }
        else {
            Toast.makeText(StrainListActivity.this,"No Data",Toast.LENGTH_LONG).show();
            loading.dismiss();
        }
//        String module = "layerfarm_android";
//        String function_name = "layerfarm_android_get_strain";
//        String[] args = {};
//        Parameter parameter = new Parameter(module, function_name, args);
//
//        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//        String token2 = RetrofitData.getInstance().getToken2();
//        Call<List<StrainsSync>> call = apiInterfaceJson.getStrain(token2, parameter);
//
//        call.enqueue(new Callback<List<StrainsSync>>() {
//
//            @Override
//            public void onResponse(Call<List<StrainsSync>> call, Response<List<StrainsSync>> response) {
//                strainList = response.body();
//                try {
//                for (int i =0; i< strainList.size(); i++){
//                    String name = strainList.get(i).getName();
//                    String rid = strainList.get(i).getRid();
//                    Log.d("name", "name = " + name);
//                    Log.d("rid", "rid = " + rid);
//                    StrainsSync data = new StrainsSync(name, rid);
//                    itemList.add(data);
//                }
//                    adapter.notifyDataSetChanged();
//                    loading.dismiss();
//                }catch (Exception e){
//                    //e.printStackTrace();
//                    Toast.makeText(StrainListActivity.this,"No Data",Toast.LENGTH_LONG).show();
//                    loading.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<StrainsSync>> call, Throwable t) {
//                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        get_strain();
    }

    public void setStrain(String warehouse_items){
        this.strain_items = warehouse_items;
    }
    public String getStrain(){
        return strain_items;
    }

    public void setStrainNid(String nid_items){
        this.strain_items_nid = nid_items;
    }
    public String getStrainNid(){
        return strain_items_nid;
    }

    public void cls_strain(View view) {
/*        Intent intent = new Intent();
        String name = "";
        String nid = "";
        intent.putExtra("flock_name", name);
        intent.putExtra("flock_nid", nid);
        setResult(RESULT_OK, intent);*/
        finish();
    }
}
