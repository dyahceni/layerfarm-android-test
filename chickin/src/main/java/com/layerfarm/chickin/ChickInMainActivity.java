package com.layerfarm.chickin;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.List;

import com.layerfarm.chickin.Pinger;
import com.layerfarm.layerfarm.LayerfarmHelper;

import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.chickin.adapter.FlockAdapter;
import com.layerfarm.chickin.database.DBDataSource;
import com.layerfarm.chickin.model.chickinDistributionModel;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.chickin.model.chickinDistributionModelAge;
import com.layerfarm.chickin.model.chickinDistributionModelHatch;

import java.util.ArrayList;


public class ChickInMainActivity extends AppCompatActivity implements Pinger.OnPingListener{

    public static final String TAG = ChickInMainActivity.class.getSimpleName();

    public String url, code, username, password;
    public LayerfarmHelper layerFarmHelper;
    DatabaseHelper db;
    private Spinner spn_location, spn_date;
    private DBDataSource dataSource;
    ArrayList <chickinDistributionModel> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<chickinDistributionModel> modelList = new ArrayList<>();
    private ArrayList <chickinDistributionModelHatch> ListchickinDistributionHatch;
    private ArrayList <chickinDistributionModelAge> ListchickinDistributionAge;
    public int nombr = 0;

    private Calendar myCalendar = Calendar.getInstance();
    private EditText etDate;

    Toolbar toolbar ;
    TextView toolbar_title;

    public static final String TAG_ID = "distributionId";
    public static final String TAG_CHICK_ID = "idChickin";
    public static final String TAG_FLOCK_ID = "idFlock";
    public static final String TAG_LOCATION_NAME = "location_name";
    public static final String TAG_FLOCK_NAME = "name";
    public static final String TAG_TYPE = "type";
    public static final String TAG_NUMBER_BIRD = "number_of_bird";
    private String nid="";
    private String created ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chick_in_main);
        //etDate = (EditText) findViewById(R.id.etDate);

        //tool bar
        toolbar = (Toolbar) findViewById(R.id.toolbar_main_chickin);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(com.layerfarm.setting.R.id.title);
        toolbar_title.setText("Chick In Draft Data");

        layerFarmHelper = new LayerfarmHelper(this);
        layerFarmHelper.last_chick_in_id();

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        //Toast.makeText(ChickInMainActivity.this, "unixtimestamp = " + timeStamp, Toast.LENGTH_SHORT).show();


        FloatingActionButton fab = (FloatingActionButton) findViewById(com.layerfarm.setting.R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChickInMainActivity.this, ChickinEntryActivity.class);
                startActivity(intent);
                finish();
            }
        });


        dataSource =  new DBDataSource(this);
        dataSource.open();

        db = new DatabaseHelper(this);

        //spinner date
        ArrayList<String> spdate = dataSource.getStrainDate("");
        spn_date = (Spinner) findViewById(R.id.spDate);
        spn_date.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, spdate));
        spn_date.setSelected(true);



        spn_date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //spinner strain
        ArrayList<String> location = dataSource.getStrainRecording("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);

        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Submit();
                submitSpinner();
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rvFlock = (RecyclerView) findViewById(R.id.rv_flock);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);
    }

   /*
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        //Intent i = new Intent(this, SelectMenu.class);
        //startActivity(i);
    }

*/

    public void Submit(){
        String strain_name = spn_location.getSelectedItem().toString();
        String hatch_date = spn_date.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getStrainRecording(strain_name).get(0);
        //Toast.makeText(ChickInMainActivity.this, "strain_id = " + loc_id, Toast.LENGTH_SHORT).show();
        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT location.name as location_name, flock.name, flock.type, chick_in_distribution.number_of_bird, chick_in_distribution.id as distributionId, chick_in_distribution.chick_in_id as idChickin, chick_in_distribution.flock_id as idFlock from chick_in, location, flock, chick_in_distribution where chick_in_distribution.chick_in_id = chick_in.id and flock.location_id = location.rid and chick_in_distribution.flock_id = flock.rid and chick_in.strains_id = '"+loc_id+"' and chick_in.hatch_date = '"+hatch_date+"'",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            chickinDistributionModel flock = new chickinDistributionModel();
            flock.setId(cursor.getString(cursor.getColumnIndex("distributionId")));
            flock.setChick_in_id(cursor.getString(cursor.getColumnIndex("idChickin")));
            flock.setFlock_id(cursor.getString(cursor.getColumnIndex("idFlock")));
            flock.setLocation_name(cursor.getString(cursor.getColumnIndex("location_name")));
            flock.setName(cursor.getString(cursor.getColumnIndex("name")));
            flock.setType(cursor.getString(cursor.getColumnIndex("type")));
            flock.setNumber_of_bird(cursor.getString(cursor.getColumnIndex("number_of_bird")));
            //flock.setStatus(cursor.getString(cursor.getColumnIndex("status")));
            //flock.setLocation_name(location);
            flock_items.add(flock);
        }
        adapter = new FlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    public void submitSpinner(){
        String strain_name = spn_location.getSelectedItem().toString();
        String loc_id = dataSource.getStrainRecording(strain_name).get(0);
       // Toast.makeText(ChickInMainActivity.this, "strains_id = " + loc_id, Toast.LENGTH_SHORT).show();
        //getStringDate(loc_id);
        List<String> lables = this.getStringDate(loc_id);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_date.setAdapter(dataAdapter);
    }


    public List<String> getStringDate(String strain_id){
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT hatch_date from chick_in where strains_id = '"+ strain_id + "'";

        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    //Ping utilities
    @Override
    public void onPingSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Pinger pinger = new Pinger();
                Toast.makeText(ChickInMainActivity.this, "Success njuhbub", Toast.LENGTH_SHORT).show();
                pinger.cancel();
            }
        });
    }

    @Override
    public void onPingFailure() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ChickInMainActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPingFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ChickInMainActivity.this, "Finish", Toast.LENGTH_SHORT).show();
            }
        });
    }

    static public boolean isURLReachable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL("http://192.168.1.13");   // Change to "http://google.com" for www  test.
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(10 * 1000);          // 10 s.
                urlc.connect();
                if (urlc.getResponseCode() == 200) {        // 200 = "OK" code (http connection is fine).
                    Log.wtf("Connection", "Success !");
                    return true;
                } else {
                    return false;
                }
            } catch (MalformedURLException e1) {
                return false;
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public boolean isConnectedToServer(String url, int timeout) {
        try{
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            return true;
        } catch (Exception e) {
            // Handle your exceptions
            return false;
        }
    }

    public boolean isConnected() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                // Network is available but check if we can get access from the
                // network.
                URL url = new URL("www.google.com");
                HttpURLConnection urlc = (HttpURLConnection) url
                        .openConnection();
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(2000); // Timeout 2 seconds.
                urlc.connect();

                if (urlc.getResponseCode() == 200) // Successful response.
                {
                    return true;
                } else {
                    Log.d("NO INTERNET", "NO INTERNET");

                    return false;
                }
            } else {
                Log.d("NO INTERNET", "NO INTERNET");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

        if (activeNetworkInfo != null) { // connected to the internet
            Toast.makeText(context, activeNetworkInfo.getTypeName(), Toast.LENGTH_SHORT).show();

            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        }
        return false;
    }


//    public void upload_data_to_layer(View view) {
//
//        final SQLiteDatabase dbase = db.getReadableDatabase();
//        //get url information
//        String selectQueryUrl = "SELECT value FROM variables";
//        Cursor curl = dbase.rawQuery(selectQueryUrl, null);
//        ArrayList < String > values = new ArrayList < >();
//        // looping through all rows and adding to list
//        if (curl.moveToFirst()) {
//            do {
//                String value = curl.getString(curl.getColumnIndex("value"));
//                values.add(value);
//            } while ( curl . moveToNext ());
//
//        }
//        curl.close();
//
//        if (values != null && !values.isEmpty()) {
////            url = values.get(0);
//            code = values.get(0);
//            username = values.get(1);
//            password = values.get(2);
//            //LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
//        }
//
//
//
//        //count chickin record
//
//        String selectQuery = "SELECT * FROM chick_in";
//        Cursor cursor = dbase.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst() && cursor.getCount() > 0) {
//            nombr = cursor.getCount();
//            System.out.println("Count :" + nombr); // Display current time
//            cursor.close();
//        }
//
//        //Process start
//        if (nombr == 0) {
//            Toast.makeText(ChickInMainActivity.this, "Nothing data to upload", Toast.LENGTH_SHORT).show();
//        }else{
//
//
//        //final Spinner spnLocale = (Spinner) findViewById(R.id.chickinMode);
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
//        // Setting Dialog Title
//        alertDialog.setTitle("Confirm upload data...");
//        // Setting Dialog Message
//        alertDialog.setMessage("Are you sure you want upload this data?");
//        // Setting Icon to Dialog
//        alertDialog.setIcon(R.drawable.ic_plus);
//        // Setting Positive "Yes" Button
//        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//        //now = new Date(); // initialize date
//
//
//        try {
//
//                new AsyncTask< String,
//                        Void,
//                        Boolean >() {@Override
//                protected Boolean doInBackground(String...destinations) {
//                    try {
//                        Pinger pinger = new Pinger();
//                        for (String destination: destinations)
//                            if (pinger.ping(destination, 60)) return true;
//                    } catch(InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    return false;
//                }
//
//                    @Override
//                    protected void onPostExecute(Boolean result) {
//                        //Check Internet Connection
//                        if (result == true) {
//                            //Jika internet ada akan check server
///*                            CharSequence text = LayerFarm.getInstance().getConnect();
//                            Log.d("text age", "text = " + text);
//                            if (text != null) {
//                                Toast.makeText(ChickInMainActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
//                                finish();
//
//                            } else {*/
//                                //Toast.makeText(ChickinEntryActivity.this, "ok", Toast.LENGTH_SHORT).show();
//
//                                for(int x = 0; x <= nombr; x++){
//
//                                //get last record chickin
//                                String selectLastChickinQuery = "SELECT * FROM chick_in order by id desc";
//                                Cursor cursorLastChickin = dbase.rawQuery(selectLastChickinQuery, null);
///*                                String strlastchickinId = "";
//                                String strlastchickinType = "";
//                                String strlastchickinHatch = "";
//                                String strlastchickinWeek = "";
//                                String strlastchickinDays = "";
//                                String strlastchickinAge = "";
//                                String strlastchickinStrain = "";*/
//                                if (cursorLastChickin.moveToFirst()) {
//
//                                    final String strlastchickinId = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("id"));
//                                    final String strlastchickinType = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("chick_in_type"));
//                                    final String strlastchickinHatch = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("hatch_date"));
//                                    final String strlastchickinWeek = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("age_in_week"));
//                                    final String strlastchickinDays = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("age_in_days"));
//                                    final String strlastchickinAge = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("age_on_date"));
//                                    final String strlastchickinStrain = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("strains_id"));
//                                    cursorLastChickin.close();
//                                    Log.d("id", "" + strlastchickinId);
//                                    if (strlastchickinType == "Age") {
//
//                                        //send data to layer server with hatchtype Age
//                                        Cursor c = dbase.rawQuery("SELECT * FROM chick_in_distribution WHERE chick_in_id = '" + strlastchickinId + "'", null);
//                                        ListchickinDistributionAge = new ArrayList<>();
//                                        c.moveToFirst();
//                                        while (!c.isAfterLast()) {
//
//
//                                            chickinDistributionModelAge chickinAge = new chickinDistributionModelAge();
//
//                                            chickinAge.setFlock_id(c.getString(c.getColumnIndex("flock_id")));
//                                            chickinAge.setNumber_of_bird(c.getString(c.getColumnIndex("number_of_bird")));
//                                            chickinAge.setStart_recording(c.getString(c.getColumnIndex("start_recording")));
//                                            ListchickinDistributionAge.add(chickinAge);
//                                            c.moveToNext();
//                                        }
//                                        // make sure to close the cursor
//                                        c.close();
//
//
//                                        String module = "layerfarm_android";
//                                        String function_name = "create_rearing_by_age";
//                                        ArrayList<ParamaterAge.distribution> distribution = new ArrayList<>();
//
//                                        for (int i = 0; i < ListchickinDistributionAge.size(); i++) {
//                                            ParamaterAge.distribution dist = new ParamaterAge.distribution();
//                                            dist.setHouse_nid(ListchickinDistributionAge.get(i).getFlock_id());
//                                            dist.setNumber_of_birds(ListchickinDistributionAge.get(i).getNumber_of_bird());
//                                            dist.setStart_recording(ListchickinDistributionAge.get(i).getStart_recording());
//                                            Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionAge.get(i).getNumber_of_bird());
//                                            distribution.add(dist);
//
//                                        }
//
//                                        ParamaterAge.Arguments args = new ParamaterAge.Arguments(nid,strlastchickinDays, strlastchickinWeek, strlastchickinAge, strlastchickinStrain, distribution,created);
//                                        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
//                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                        String token2 = RetrofitData.getInstance().getToken2();
//                                        ParamaterAge paramaterage = new ParamaterAge(module, function_name, args);
//                                        Call<Node> call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramaterage);
//                                        Log.d("Layerfarm", "token 2 = " + token2);
//
//                                        call.enqueue(new Callback<Node>() {
//                                            @Override
//                                            public void onResponse(Call<Node> call, Response<Node> response) {
//                                                //                final String token1  = response.body();
//                                                Node node = response.body();
//                                                if (node != null) {
//                                                    //delete data
//                                                    delete_draft_data(strlastchickinId);
//                                                    Toast.makeText(ChickInMainActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
//
//                                                }
//
//
//                                                Log.d("Layerfarm", "node = " + node);
//                                            }
//
//                                            @Override
//                                            public void onFailure(Call<Node> call, Throwable t) {
//                                                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
//                                                //LayerFarm(farmName, getEmail(), getPassword());
//                                            }
//                                        });
//
//                                        finish();
//
//                                    } else {
//
//                                        //send data to layer server with hatchtype hatch Date
//                                        //get distribution
//                                        Cursor c = dbase.rawQuery("SELECT * FROM chick_in_distribution WHERE chick_in_id = '" + strlastchickinId + "'", null);
//                                        c.moveToFirst();
//                                        ListchickinDistributionHatch = new ArrayList<>();
//                                        while (!c.isAfterLast()) {
//
//
//                                            chickinDistributionModelHatch chickinHatch = new chickinDistributionModelHatch();
//
//                                            chickinHatch.setFlock_id(c.getString(c.getColumnIndex("flock_id")));
//                                            chickinHatch.setNumber_of_bird(c.getString(c.getColumnIndex("number_of_bird")));
//                                            chickinHatch.setStart_recording(c.getString(c.getColumnIndex("start_recording")));
//                                            ListchickinDistributionHatch.add(chickinHatch);
//                                            c.moveToNext();
//                                        }
//                                        // make sure to close the cursor
//                                        c.close();
//                                        //Log.d("id of edit text", "" + ListchickinDistributionHatch);
//
//                                        String module = "layerfarm_android";
//                                        String function_name = "create_rearing_hatch_date";
//                                        ArrayList<ParamaterHatch.distribution> distribution = new ArrayList<>();
//
//                                        for (int i = 0; i < ListchickinDistributionHatch.size(); i++) {
//                                            ParamaterHatch.distribution dist = new ParamaterHatch.distribution();
//                                            dist.setHouse_nid(ListchickinDistributionHatch.get(i).getFlock_id());
//                                            dist.setNumber_of_birds(ListchickinDistributionHatch.get(i).getNumber_of_bird());
//                                            dist.setStart_recording(ListchickinDistributionHatch.get(i).getStart_recording());
//                                            Log.d("Number_of_bird()", "Number_of_bird() = " + ListchickinDistributionHatch.get(i).getNumber_of_bird());
//                                            distribution.add(dist);
//
//                                        }
//
//                                        ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(nid, strlastchickinHatch, strlastchickinStrain, distribution, created);
//                                        //ParamaterHatch.Arguments args = new ParamaterHatch.Arguments(hatch_date, strain_id, distribution);
//                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                        String token2 = RetrofitData.getInstance().getToken2();
//                                        ParamaterHatch paramater = new ParamaterHatch(module, function_name, args);
//                                        Call<Node> call = apiInterfaceJson.layerfarm_create_rearing_by_hatch_date(token2, paramater);
//                                        Log.d("Layerfarm", "token 2 = " + token2);
//
//                                        call.enqueue(new Callback<Node>() {
//                                            @Override
//                                            public void onResponse(Call<Node> call, Response<Node> response) {
//                                                //                final String token1  = response.body();
//                                                Node node = response.body();
//                                                if (node != null) {
//                                                    //delete data
//                                                    delete_draft_data(strlastchickinId);
//                                                    Toast.makeText(ChickInMainActivity.this, "Upload Successfully", Toast.LENGTH_SHORT).show();
//
//                                                }
//                                                Log.d("Layerfarm", "node = " + node);
//                                            }
//
//                                            @Override
//                                            public void onFailure(Call<Node> call, Throwable t) {
//                                                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
//                                                //LayerFarm(farmName, getEmail(), getPassword());
//                                            }
//                                        });
//
//                                        finish();
//
//                                    }
//
//                                }
//
//                            }//end of looping
//                         //   }
//
//                        } else {
//                            //Internet Failure
//                            Toast.makeText(ChickInMainActivity.this, "Internet Connection Error", Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                }.execute(url);
//
//                //System.out.println("strlastchickin :" + distribution); // Display current time
//
//        } catch (SQLiteException e) {
//            e.getMessage();
//        }
//
//        }
//        });
//        // Setting Negative "NO" Button
//        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//public void onClick(DialogInterface dialog, int which) {
//        // Write your code here to invoke NO event
//        dialog.cancel();
//        }
//        });
//
//        // Showing Alert Message
//        alertDialog.show();
//        }
//    }

    public void delete_draft_data(String id) {

        final SQLiteDatabase dbase = db.getReadableDatabase();
        //delete distribution
        String delchickindistributionQuery = "DELETE FROM chick_in_distribution WHERE chick_in_id =" + "'" + id + "'";
        Log.e("delete sqlite ", delchickindistributionQuery);
        dbase.execSQL(delchickindistributionQuery);


        Integer chick_id = Integer.parseInt(id);
        //delete distribution
        String delchickinQuery = "DELETE FROM chick_in WHERE id =" + "'" + chick_id + "'";
        Log.e("delete sqlite ", delchickinQuery);
        dbase.execSQL(delchickinQuery);
        dbase.close();
    }
}


