package com.layerfarm.chickin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.layerfarm.chickin.adapter.ChickinHistoryAdapter;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.chickinHistory;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChickinHistoryMainActivity extends AppCompatActivity {
    ProgressDialog loading;
    private LinearLayoutManager linearlaout;
    List<chickinHistory> itemList = new ArrayList<chickinHistory>();
    ArrayList<chickinHistory.distribution_history> distList = new ArrayList<chickinHistory.distribution_history>();
    public String url, username, password;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper db = new DatabaseHelper(this);
    private RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chickin_history_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(com.layerfarm.setting.R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent createFarmActivity = new Intent(getApplicationContext(), ChickinEntryActivity.class);
                startActivity(createFarmActivity);
                //overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                //finish();

            }
        });
        loading = ProgressDialog.show(this, null, "Please wait...", true, false);
        getChickinHash();

//        linearlaout
        recyclerView = (RecyclerView) findViewById(R.id.chickin_history_recycler_view);
        Log.i("itemList", "" + itemList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);


        //get url information
/*
        SQLiteDatabase dbSQL = db.getReadableDatabase();
        String selectQuery = "SELECT value FROM variables";
        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
        ArrayList< String > values = new ArrayList < >();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String value = cursor.getString(cursor.getColumnIndex("value"));
                values.add(value);
            } while ( cursor . moveToNext ());

        }
        cursor.close();

        if (values != null && !values.isEmpty()) {
            url = values.get(0);
            username = values.get(1);
            password = values.get(2);

        }
        LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
*/






    }

    private void getChickinHistory(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_history_link";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<String>> call = apiInterfaceJson.getSringChickinHistory(token2, parameter);

        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                List<String> chickinHistorySyncList = response.body();
               // Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        String jsonresponse = response.body().toString();
                        Log.i("onSuccess", response.body().toString());
/*
                        String jsonresponse = response.body().toString();
                        //writeTv(jsonresponse);
                        Log.d("ssss", "name = " + flockSyncList.get(i).getName());*/



/*                        if(chickinHistorySyncList!=null && chickinHistorySyncList.size()>0) {
                            for (int i = 0; i < chickinHistorySyncList.size(); i++) {
                        Log.d("ssss", "" + chickinHistorySyncList.get(i).getNid());
                        Log.d("ssss", "" + chickinHistorySyncList.get(i).getRare_type());
                        Log.d("ssss", "" + chickinHistorySyncList.get(i).getHatching_date());
                        Log.d("ssss", "" + chickinHistorySyncList.get(i).getStrain_name());


                            }
                        }*/



                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });

    }

    private void getChickinHash(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_history_link";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LinkedHashMap<String, chickinHistory>> call = apiInterfaceJson.getLinkedMapChickinHistory(token2, parameter);
        call.enqueue(new Callback<LinkedHashMap<String, chickinHistory>>() {
            @Override
            public void onResponse(Call<LinkedHashMap<String, chickinHistory>> call, Response<LinkedHashMap<String, chickinHistory>> response) {
                if (response != null) {

                    LinkedHashMap<String, chickinHistory> map = response.body();
                    Collection<chickinHistory> values = map.values();
                    ArrayList<chickinHistory> listOfValues = new ArrayList<chickinHistory>(values);
                    adapter = new ChickinHistoryAdapter(listOfValues);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    loading.dismiss();
                    //getFlock();
                }
            }

            @Override
            public void onFailure(Call<LinkedHashMap<String, chickinHistory>> call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                loading.dismiss();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        getChickinHash();
    }

    private void getStrain(){

    }

    public void close_main_chickin(View view) {finish();
    }


/*
    private void getFlock(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_flock";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<getFlockModel> call = apiInterfaceJson.getFlockSpinner(token2, parameter);

        call.enqueue(new Callback<getFlockModel>() {
            @Override
            public void onResponse(Call<getFlockModel> call, Response<getFlockModel> response) {
                  getFlockModel flock = response.body();
                  getFlockModel.getInstance().setD(flock.getD());
                  Log.d("Layerfarm", "Flocks failure = " +getFlockModel.getInstance().getD());
            }

            @Override
            public void onFailure(Call<getFlockModel> call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }
*/

   /* private void getAllFlock(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_flock";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<mFlock>> call = apiInterfaceJson.getFlock(token2, parameter);

        call.enqueue(new Callback<List<mFlock>>() {
            @Override
            public void onResponse(Call<List<mFlock>> call, Response<List<mFlock>> response) {
                List<mFlock> flockSyncList = response.body();
                if(flockSyncList!=null && flockSyncList.size()>0) {
                    for (int i = 0; i < flockSyncList.size(); i++) {
                        mFlock.getInstance().setNid(flockSyncList.get(i).getNid());
                        mFlock.getInstance().setLocationId(flockSyncList.get(i).getLocation_id());
                        mFlock.getInstance().setCapacity(flockSyncList.get(i).getCapacity());
                        mFlock.getInstance().setName(flockSyncList.get(i).getName());
                        mFlock.getInstance().setPeriod(flockSyncList.get(i).getPeriod());
                        mFlock.getInstance().setStatus(flockSyncList.get(i).getStatus());
                        mFlock.getInstance().setType(flockSyncList.get(i).getType());
                        //Log.d("Layerfarm", "Flocks failure = " + flockSyncList.get(i).getNid());
                    }
                }


            }

            @Override
            public void onFailure(Call<List<mFlock>> call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }*/
}
