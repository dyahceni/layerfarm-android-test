package com.layerfarm.chickin.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class DBDataSource {
    //inisialisasi SQLite Database
    private SQLiteDatabase database;

    //inisialisasi kelas helper
    private DatabaseHelper db;

    //mengambil semua nama kolom
    private String[] chickinColumn = {"id",
            "chick_in_type", "hatch_date", "age_in_week",
            "age_in_days", "age_on_date", "strain_id",
            "rid"};

    private String[] chickinDistributionColumn = {"id",
            "chick_in_id", "flock_id", "number_of_bird",
            "start_recording"};

    //DBHelper diinstantiasi pada constructor
    public DBDataSource(Context context)
    {

        db = new DatabaseHelper(context);
    }

    //membuat sambungan ke database
    public void open() throws SQLException {
        database= db.getWritableDatabase();
    }

    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    public ArrayList<String> getLocation(String location_name){
        //jika bukan nama kosong maka mengambil semua
        ArrayList location = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = null;
        if (location_name == ""){
            cursor = dbase.rawQuery("SELECT * FROM flock ",null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                location.add(cursor.getString(cursor.getColumnIndex("name")));
            }
        }
        else if (location_name != "") {
            cursor = dbase.rawQuery("SELECT * FROM flock where name = '" + location_name + "' ", null);
            if(cursor.moveToFirst())
                location.add(cursor.getString(cursor.getColumnIndex("id")));
            cursor.close();
        }

        return location;

    }

    public ArrayList<String> getStrain(String strain_name){
        //jika bukan nama kosong maka mengambil semua
        ArrayList strain = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = null;
        if (strain_name == ""){
            cursor = dbase.rawQuery("SELECT * FROM strain ",null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                strain.add(cursor.getString(cursor.getColumnIndex("name")));
            }
        }
        else if (strain_name != "") {
            cursor = dbase.rawQuery("SELECT * FROM strain where name = '" + strain_name + "' ", null);
            if(cursor.moveToFirst())
                strain.add(cursor.getString(cursor.getColumnIndex("rid")));
            cursor.close();
        }

        return strain;

    }

    public ArrayList<String> getStrainRecording(String strain_recording_name){
        //jika bukan nama kosong maka mengambil semua
        ArrayList strainRecording = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = null;
        if (strain_recording_name == ""){
            cursor = dbase.rawQuery("SELECT DISTINCT strain.name FROM strain, chick_in where chick_in.strains_id = strain.rid ",null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                strainRecording.add(cursor.getString(cursor.getColumnIndex("name")));
            }
        }
        else if (strain_recording_name != "") {
            cursor = dbase.rawQuery("SELECT DISTINCT strain.rid FROM strain, chick_in where chick_in.strains_id = strain.rid and name = '" + strain_recording_name + "' ", null);
            if(cursor.moveToFirst())
                strainRecording.add(cursor.getString(cursor.getColumnIndex("rid")));
            cursor.close();
        }

        return strainRecording;

    }

    public ArrayList<String> getStrainDate(String hatch_date){
        //jika bukan nama kosong maka mengambil semua
        ArrayList hatchDate = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = null;
        if (hatch_date == ""){
            cursor = dbase.rawQuery("SELECT hatch_date from chick_in ",null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                hatchDate.add(cursor.getString(cursor.getColumnIndex("hatch_date")));
            }
        }
        else if (hatch_date != "") {
            cursor = dbase.rawQuery("SELECT hatch_date from chick_in ",null);
            if(cursor.moveToFirst())
                hatchDate.add(cursor.getString(cursor.getColumnIndex("hatch_date")));
            cursor.close();
        }

        return hatchDate;

    }

    /*

    //methhod untuk create/ insert ke database
    public void createChickinAge(ArrayList<chickinAgeDistributionModel> chickinAgeDistributionModel, long id){
        for (int c =0; c<chickinAgeDistributionModel.size();c++){
            chickinAgeDistributionModel chickinage = chickinAgeDistributionModel.get(c);
            String name = chickinage.getFlock_name();
            String bird = chickinage.getNumber_of_bird_age();
            String start = chickinage.getStart_recording_age();

            //Get flock id
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ name + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String flockID = "";
            if(cursorFlock.moveToFirst())
                flockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();

            //get last record chickin
            String selectQuery= "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
            Cursor cursor = database.rawQuery(selectQuery, null);
            String chickinID = "";
            if(cursor.moveToFirst())
                chickinID  =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();

            ContentValues recording_chickin_age = new ContentValues();
            recording_chickin_age.put("id", id);
            recording_chickin_age.put("chick_in_id", chickinID);
            recording_chickin_age.put("flock_id", flockID);
            recording_chickin_age.put("number_of_bird", bird);
            recording_chickin_age.put("start_recording", start);
            long insertChickinAge = database.insert("chick_in", null, recording_chickin_age);

        }
    }
    */

    /*
    //methhod untuk create/ insert ke database
    public void createChickinHatch(ArrayList<chickinHatchDistributionModel> chickinHatchDistributionModel){
        for (int c =0; c<chickinHatchDistributionModel.size();c++){
            chickinHatchDistributionModel chickinhatch = chickinHatchDistributionModel.get(c);
            String name = chickinhatch.getFlock_name_hatch();
            String bird = chickinhatch.getNumber_of_bird_hatch();
            String start = chickinhatch.getStart_recording_hatch();

            //Get flock id
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ name + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String flockID = "";
            if(cursorFlock.moveToFirst())
                flockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();

            //get last record chickin
            String selectQuery= "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
            Cursor cursor = database.rawQuery(selectQuery, null);
            String chickinID = "";
            if(cursor.moveToFirst())
                chickinID  =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();

            ContentValues recording_chickin_hatch = new ContentValues();
            recording_chickin_hatch.put("chick_in_id", chickinID);
            recording_chickin_hatch.put("flock_id", flockID);
            recording_chickin_hatch.put("number_of_bird", bird);
            recording_chickin_hatch.put("start_recording", start);
            long insertChickinHatch = database.insert("chick_in_distribution", null, recording_chickin_hatch);

        }
    }
    */

    /*
    public void createChickin(String chick_in_type, String hatch_date, String age_in_week, String age_in_days, String age_on_date, String strains_id, String rid){


        //RECORDING TRANSFER
        ContentValues recording_chick_in = new ContentValues();
        recording_chick_in.put("daily_recording_id", id);
        recording_chick_in.put("chick_in_id", ChickInID);
        recording_chick_in.put("to_flock_id", FlockID);
        recording_transfer.put("number_of_bird", BirdMove);
        long insertFeed = database.insert("chick_in", null, recording_transfer);
    }
    */
}
