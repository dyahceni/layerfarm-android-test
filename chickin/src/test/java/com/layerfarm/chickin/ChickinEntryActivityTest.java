package com.layerfarm.chickin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import okhttp3.JavaNetCookieJar;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.matchers.text.ValuePrinter.print;

public class ChickinEntryActivityTest {
    //    private Handler mHandler = mock(Handler.class);
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String token2;
    private String baseUrl = "http://"+"staging"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    String nodes = "";
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void ConnectServer(){
        if (TextUtils.isEmpty(uniqueID)) {
            uniqueID = UUID.randomUUID().toString();
        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            print("tokenn1 = "+tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                print(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            if (connection != null) {
                                user = connection.getUser();
                                Log.d("Layerfarm", "uid = " + user.getUid());
                                System.out.println("uid = "+user.getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            print("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                print("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            print("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public void getData() {
        int data = 51;
        System.out.println("Get Data");
//        if (ConnectServer()){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_entry_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ChickinEntry> call = apiInterfaceJson.get_chickin_data_entry(token2, parameter);
        try {
            ChickinEntry flock = call.execute().body();
            LinkedHashMap<String, String > curr = flock.getHouse();
            System.out.println("house size = "+curr.size());
//                assertEquals(data, curr.size()>0);
            assertTrue(curr.size()>0);
        }
        catch (Exception e){
            assertFalse(true);
        }
//        }
//        else {
//            assertFalse(true);
//        }

    }

    @Test
    public void test_ChickIn(){
        boolean create = create_rearing_by_age_test();
        boolean edit = false, delete = false;
        if (create){
            edit = edit_rearing_by_age_test();
        }
        if (edit){
            delete = delete_rearing_by_age_test();
        }
        System.out.println("Create Chickin: "+create);
        System.out.println("Edit  Chickin: "+edit);
        System.out.println("Delete  Chickin: "+delete);
        if (!create){
            assertFalse(true);
        } else if (!edit){
            assertFalse(true);
        } else if (!delete){
            assertFalse(true);
        }
    }


    public Boolean create_rearing_by_age_test() {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_age";
        ArrayList< ParamaterAge.distribution > distribution = new ArrayList < >();
        ParamaterAge.distribution dist = new ParamaterAge.distribution();

        dist.setHouse_nid("1870");
        dist.setNumber_of_birds("150");
        dist.setStart_recording("2020-09-09");

        distribution.add(dist);

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        ParamaterAge.Arguments args = new ParamaterAge.Arguments(nodes,"2", "0","2020-09-01","1", distribution,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterAge paramater = new ParamaterAge(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            nodes = node[0];
            System.out.println("node = "+node[0]);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }


    public Boolean edit_rearing_by_age_test() {
        String module = "layerfarm";
        String function_name = "layerfarm_create_rearing_by_age";
        ArrayList< ParamaterAge.distribution > distribution = new ArrayList < >();
        ParamaterAge.distribution dist = new ParamaterAge.distribution();

        dist.setHouse_nid("1870");
        dist.setNumber_of_birds("150");
        dist.setStart_recording("2020-09-09");

        distribution.add(dist);

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        ParamaterAge.Arguments args = new ParamaterAge.Arguments(nodes,"2", "0","2020-09-01","1", distribution,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParamaterAge paramater = new ParamaterAge(module, function_name, args);
        Call < String[] > call = apiInterfaceJson.layerfarm_create_rearing_by_age(token2, paramater);
        Log.d("Layerfarm", "token 2 = " + token2);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            System.out.println("node = "+node[0]);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    public Boolean delete_rearing_by_age_test(){
        String module = "layerfarm_android";
        String function_name = "delete_setting_data";

        ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(nodes,"chickin");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

        Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);

        try{
            String message = call.execute().body();
            System.out.println(message);
            return true;
        }catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

}