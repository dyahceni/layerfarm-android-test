package com.layerfarm.purchase.BottomClass;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.layerfarm.purchase.R;

public class UoMBottomClass extends BottomSheetDialogFragment implements AdapterView.OnItemClickListener {
    private BottomSheetListener mListener;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private String[] UoM={
            "kg","gram","liter","mililiter","miligram"
    };

    public void setOnBottomSheetClickListener(BottomSheetListener l) {
        mListener = l;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onButtonClicked(UoM[position]);
        dismiss();
    }


    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_unit_measurement, container, false);
        listView = (ListView) v.findViewById(R.id.UoM);
        arrayAdapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,UoM);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        return v;
    }
}
