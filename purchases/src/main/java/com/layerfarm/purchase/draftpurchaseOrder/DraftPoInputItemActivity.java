package com.layerfarm.purchase.draftpurchaseOrder;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.layerfarm.purchase.BottomClass.UoMBottomClass;
import com.layerfarm.purchase.R;
import com.layerfarm.purchase.function.Helper;
import com.layerfarm.purchase.listActivity.*;
import com.layerfarm.layerfarm.DatabaseHelper;

public class DraftPoInputItemActivity extends AppCompatActivity {

    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener;
    private EditText productdEditText, scheduledateEditText, unitEditText, quantityEditText, unitPriceEditText,taxesEditText;
    private TextView totalValue;
    DatabaseHelper SQLite = new DatabaseHelper(this);
    Helper help = new Helper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_po_input_item);
        productdEditText = (EditText) findViewById(R.id.productdEditText);
        scheduledateEditText = (EditText) findViewById(R.id.scheduledateEditText);
        unitEditText  = (EditText) findViewById(R.id.unitEditText);
        quantityEditText = (EditText) findViewById(R.id.quantityEditText);
        unitPriceEditText = (EditText) findViewById(R.id.unitPriceEditText);
        taxesEditText = (EditText) findViewById(R.id.taxesEditText);
        totalValue = (TextView) findViewById(R.id.totalValue);
        this.setTitle("New Item");

        ProductListActivity.getInstance().setProduct("");

        //get Product
        productdEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputItemActivity.this, ProductListActivity.class);
                startActivity(intent);
            }
        });

        //Show Date
        scheduledateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        DraftPoInputItemActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();
            }
        });
        mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                scheduledateEditText.setText(date);
            }
        };

        //get Product
        unitEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UoMBottomClass bottom_UoM = new UoMBottomClass();
                bottom_UoM.show(getSupportFragmentManager(),"UoM");

                bottom_UoM.setOnBottomSheetClickListener(new UoMBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        unitEditText.setText(text);
                        bottom_UoM.dismiss();
                    }
                });
            }
        });

        quantityEditText.addTextChangedListener(new GenericTextWatcher(quantityEditText));
        unitPriceEditText.addTextChangedListener(new GenericTextWatcher(unitPriceEditText));
        taxesEditText.addTextChangedListener(new GenericTextWatcher(taxesEditText));

    }

    @Override
    protected void onResume() {
        super.onResume();
        productdEditText.setText(ProductListActivity.getInstance().getProduct());
    }


    public void saveItem(View view) {
        if (productdEditText.getText().toString().isEmpty() || scheduledateEditText.getText().toString().isEmpty() || quantityEditText.getText().toString().isEmpty() || unitEditText.getText().toString().isEmpty()) {
            AskOption();
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Add Item...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure to add item?");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.ic_plus);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    String product_name = (String) (productdEditText.getText().toString());
                    String unit = (String) (unitEditText.getText().toString());
                    String qty = (String) (quantityEditText.getText().toString());
                    String price = (String) (unitPriceEditText.getText().toString());
                    String tax = (String) (taxesEditText.getText().toString());
                    String total = (String) (totalValue.getText().toString());
                    String schedule = (String) (scheduledateEditText.getText().toString());
                    float subtotal = Float.parseFloat(qty) * Float.parseFloat(price);

                    SQLiteDatabase db = SQLite.getWritableDatabase();
                    String strlastPO = help.getLastPurchaseOrderID();

                    Log.d("zzzs", "PO= " + strlastPO);

                    Date todayDate = Calendar.getInstance().getTime();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String today = formatter.format(todayDate);

                    //insert to items
                    String insertShopSQL = "INSERT INTO purchase_order_line \n" +
                            "(shop_id, item_name, item_quantity,item_price, item_done, item_tax, item_sub_total, item_total, deleted, item_unit, state, item_category, create_date, schedule_date) VALUES \n" +
                            "(?, ? ,?, ?, ?, ?, ?, ?, ?,?,?,?,?,?);";
                    db.execSQL(insertShopSQL, new String[]{strlastPO, product_name, qty, price,"1", tax, String.valueOf(subtotal), total, "0",unit,"draft",null,today,schedule });


                    Intent intent = new Intent(DraftPoInputItemActivity.this,DraftPoInputActivity.class);
                    intent.putExtra("purchase_id", strlastPO);
                    startActivity(intent);
                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;
        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.quantityEditText) {
                if((!quantityEditText.getText().toString().equals("")) && (!unitPriceEditText.getText().toString().equals(""))){
                    //int val = Integer.parseInt(qty.getText().toString()) * Integer.parseInt(price.getText().toString());
                    float val = Float.parseFloat(unitPriceEditText.getText().toString())*Float.parseFloat(quantityEditText.getText().toString());
                    totalValue.setText(Float.toString(val));
                }

            } else if (i == R.id.unitPriceEditText) {
                if((!quantityEditText.getText().toString().equals("")) && (!unitPriceEditText.getText().toString().equals(""))){
                    //int val = Integer.parseInt(qty.getText().toString()) * 2;
                    float val = Float.parseFloat(unitPriceEditText.getText().toString())*Float.parseFloat(quantityEditText.getText().toString());
                    totalValue.setText(Float.toString(val));
                }
            }

            else if (i == R.id.taxesEditText) {
                if((!totalValue.getText().toString().equals("")) && (!taxesEditText.getText().toString().equals(""))){
                    //int val = Integer.parseInt(qty.getText().toString()) * 2;
                    float val = (Float.parseFloat(unitPriceEditText.getText().toString())* Float.parseFloat(quantityEditText.getText().toString()) + Float.parseFloat(taxesEditText.getText().toString()));
                    totalValue.setText(Float.toString(val));
                }
            }
        }
    }


    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Entry Not Correctly")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    private AlertDialog AskSave(String name) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Information!!")
                .setMessage("Item " +name+ " Successfully Added")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
}


