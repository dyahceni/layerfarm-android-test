package com.layerfarm.purchase.draftpurchaseOrder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.purchase.R;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.text.NumberFormat;
import java.util.Locale;

import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_CATEGORY;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_IS_DONE;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_NAME;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_QUANTITY;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_UNIT;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_PRICE;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_TAX;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_SUB_TOTAL;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_TOTAL;
import static com.layerfarm.purchase.sql.DBContract.SelectShopItemsQuery.INDEX_ID;


public class DraftPoItemAdapter extends CursorAdapter {

    DatabaseHelper db;
    private SQLiteDatabase database;

    private static class ViewHolder {
        public final TextView mItemName;
        //public final TextView mShopName;
        //public final TextView mNotDoneItems;
        //public final TextView mAllItems;
        //public final TextView mTotal;
        //public final TextView mDate;
        //public final TextView mStatus;
        //public final TextView mVendor;
        //public final TextView mPriceTotal;




        public ViewHolder(View view) {
            mItemName = (TextView) view.findViewById(R.id.tv_item_product);
            //mNotDoneItems = (TextView) view.findViewById(R.id.list_pending);
            //mAllItems = (TextView) view.findViewById(R.id.list_size);
            //mTotal = (TextView) view.findViewById(R.id.tv_total);
            //mDate = (TextView) view.findViewById(R.id.morderdate);
            //mStatus = (TextView) view.findViewById(R.id.tv_bills);

            //mVendor = (TextView) view.findViewById(R.id.tv_vendor_list);
            //mPriceTotal = (TextView) view.findViewById(R.id.total);



        }
    }

    final View.OnTouchListener mTouchListener;

    public DraftPoItemAdapter(Context context, Cursor c, int flags, View.OnTouchListener listener) {
        super(context, c, flags);

        mTouchListener = listener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        view.setOnTouchListener(mTouchListener);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        context = view.getContext();

        db = new DatabaseHelper(view.getContext());
        database = db.getWritableDatabase();


        /*
        Log.d("INDEX_ID",cursor.getString(INDEX_ID));
        Log.d("INDEX_NAME",cursor.getString(INDEX_NAME));
        Log.d("INDEX_ORDER_DATE",cursor.getString(INDEX_ORDER_DATE));
        Log.d("INDEX_WAREHOUSE",cursor.getString(INDEX_WAREHOUSE));
        Log.d("INDEX_STATE",cursor.getString(INDEX_STATE));
        Log.d("INDEX_STATUS_INV",cursor.getString(INDEX_STATUS_INV));
        Log.d("INDEX_ALL_ITEMS_COUNT",cursor.getString(INDEX_ALL_ITEMS_COUNT));
        Log.d("INDEX_SUM_ALL_ITEMS",cursor.getString(INDEX_SUM_ALL_ITEMS));
        Log.d("INDEX_PRICE",cursor.getString(INDEX_PRICE));
        Log.d("INDEX_TAX",cursor.getString(INDEX_TAX));
        Log.d("INDEX_SUB_TOTAL",cursor.getString(INDEX_SUB_TOTAL));
        Log.d("INDEX_TOTAL",cursor.getString(INDEX_TOTAL));
        Log.d("INDEX_VENDOR",cursor.getString(INDEX_VENDOR));
        Log.d("INDEX_DONE_ITEMS_COUNT",cursor.getString(INDEX_DONE_ITEMS_COUNT));
        Log.d("INDEX_NOT_DONE",cursor.getString(INDEX_NOT_DONE_ITEMS_COUNT));
        Log.d("INDEX_CURR",cursor.getString(INDEX_CURR));
    */

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        //detailHarga.setText(formatRupiah.format((double)hargarumah));
        //int sub_total = Integer.parseInt(cursor.getString(INDEX_SUB_TOTAL));


        //Log.d("INDEX_CURR",cursor.getString(INDEX_STATE));
        viewHolder.mItemName.setText(cursor.getString(INDEX_NAME));
        //viewHolder.mDate.setText(cursor.getString(INDEX_ORDER_DATE));
        //viewHolder.mVendor.setText(cursor.getString(INDEX_TOTAL));

 /*

        if(cursor.getString(INDEX_PRICE) == null) {
            viewHolder.mTotal.setText(curSymbol +" 0");
        }else{
            float sub_total = Float.parseFloat(cursor.getString(INDEX_SUB_TOTAL));
            //viewHolder.mTotal.setText(curSymbol + " " + cursor.getString(INDEX_SUB_TOTAL));
            String total = String.valueOf(sub_total);
            viewHolder.mTotal.setText(curSymbol +" "+total);
        }

        */

    }

}