package com.layerfarm.purchase.draftpurchaseOrder;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.CursorAdapter;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ListView;


import com.layerfarm.purchase.R;
import com.layerfarm.purchase.sql.DatabaseMiddleman;
import com.layerfarm.purchase.utils.TouchAndClickListener;
import com.layerfarm.setting.DataHelper;

import java.util.Stack;


import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_ID;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_NAME;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_STATE;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_REST_PARTNER;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_STOCK_WAREHOUSE;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_CURR;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_ORDER_DATE;

public class DraftPoActivity extends AppCompatActivity implements TouchAndClickListener.ClickListener{

    ListView mListView;

    DataHelper dbase = new DataHelper(this);
    DatabaseMiddleman mDb;
    CursorAdapter mAdapter;
    Stack<Pair<String, Long>> undo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_po_list);

        undo = null;
        mDb = new DatabaseMiddleman(this);
        mListView = (ListView)findViewById(R.id.draft_po_list);
        this.setTitle("Draft PO");


        mDb.open();
        dbase = new DataHelper(this.getApplication());
        TouchAndClickListener t = new TouchAndClickListener(ViewConfiguration.get(this), mListView);
        t.setOnClick(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                Intent intent = new Intent(DraftPoActivity.this, DraftPoInputActivity.class);
                startActivity(intent);
            }
        });

        mAdapter = new DraftPoAdapter(DraftPoActivity.this, mDb.fetchAllShops(), 0, t);
        mListView.setAdapter(mAdapter);
        mDb.open();

        /*
         ListView list = (ListView) findViewById(R.id.draft_po_list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(DraftPoActivity.this, DraftPoInputAcceptActivity.class);
                startActivity(intent);
            }
        });

         */
    }

    @Override
    public void onClick(ListView listView, View view) {
        int position = mListView.getPositionForView(view);
        Cursor c = (Cursor) mListView.getItemAtPosition(position);
        long shopId = c.getLong(INDEX_ID);
        String shopName = c.getString(INDEX_NAME);
        String status = c.getString(INDEX_STATE);
        String vendor = c.getString(INDEX_REST_PARTNER);
        String warehouse = c.getString(INDEX_STOCK_WAREHOUSE);
        String currency = c.getString(INDEX_CURR);
        String order_date = c.getString(INDEX_ORDER_DATE);
        //Log.e("shopId ", "" + shopId);

        /*
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        */

        Intent intent = new Intent(this, DraftPoInputAcceptActivity.class);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_ID_LONG, shopId);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_NAME_STRING, shopName);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_REST_PARTNER, vendor);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_STOCK_WAREHOUSE, warehouse);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_CURRENCY, currency);
        intent.putExtra(DraftPoInputAcceptActivity.INTENT_SHOP_ORDER_DATE, order_date);
        startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();

        // this may be unnecessary, but we just force the notification to recompute
        // each list's done and total counts
        mDb.open();
        // since items table may have changed while we paused.
        mAdapter.changeCursor(mDb.fetchAllShops());
        mAdapter.notifyDataSetChanged();

        //mShakeSensor.onResume();
    }

}
