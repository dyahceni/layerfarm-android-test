package com.layerfarm.purchase.draftpurchaseOrder;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.layerfarm.purchase.R;
import com.layerfarm.purchase.listActivity.CurrencyListActivity;
import com.layerfarm.purchase.listActivity.DeliverListActivity;
import com.layerfarm.purchase.listActivity.VendorListActivity;
import com.layerfarm.purchase.sql.DatabaseMiddleman;
import com.layerfarm.purchase.utils.TouchAndClickListener;
import com.layerfarm.purchase.utils.Utilities;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_ID;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_NAME;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_STATE;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_VENDOR;

public class DraftPoInputAcceptActivity extends AppCompatActivity{

    static public final String INTENT_SHOP_ID_LONG = "SHOP_ID";
    static public final String INTENT_SHOP_NAME_STRING = "SHOP_NAME";
    static public final String INTENT_SHOP_REST_PARTNER = "SHOP_RES_PARTNER";
    static public final String INTENT_SHOP_STOCK_WAREHOUSE = "SHOP_STOCK_WAREHOUSE";
    static public final String INTENT_SHOP_CURRENCY = "SHOP_CURRENCY";
    static public final String INTENT_SHOP_ORDER_DATE = "SHOP_ORDERDATE";


    DatabaseHelper db;
    private SQLiteDatabase database;

    final List<Utilities.Triple<String, Float, String, String, String, String, String, String>> list = new LinkedList<>();
    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener;
    private LinearLayout addItemAccept;
    private EditText orderDateEdit;
    private EditText currencyEdit;
    private EditText vendorEdit;
    private EditText delivertoEdit;
    DataHelper dbase = new DataHelper(this);
    DatabaseMiddleman mDb;
    CursorAdapter mAdapter;
    ListView mListView;
    long mShopId;
    String mShopName;
    String mShopResPartner;
    String mShopStockWarehouse;
    Stack<Pair<String, Long>> undo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_po_input_accept);
        mListView = (ListView) findViewById(R.id.item_po_draft_list_accept);

        mDb = new DatabaseMiddleman(this);

        Intent intent = this.getIntent();
        // intentionally let app crash if intent not provided (fatal error anyway)
        mShopName = intent.getStringExtra(DraftPoInputAcceptActivity.INTENT_SHOP_NAME_STRING);
        mShopId = intent.getLongExtra(DraftPoInputAcceptActivity.INTENT_SHOP_ID_LONG, 0);
        mShopResPartner = intent.getStringExtra(DraftPoInputAcceptActivity.INTENT_SHOP_REST_PARTNER);
        mShopStockWarehouse = intent.getStringExtra(DraftPoInputAcceptActivity.INTENT_SHOP_STOCK_WAREHOUSE);

        orderDateEdit = (EditText) findViewById(R.id.orderDateEditText);
        currencyEdit= (EditText) findViewById(R.id.currencyEditText);
        vendorEdit= (EditText) findViewById(R.id.vendorEditText);
        delivertoEdit= (EditText) findViewById(R.id.deliveredEditText);

        this.setTitle(mShopName);


        Log.d("mShopResPartner",""+mShopResPartner);
        Log.d("mShopStockWarehouse",""+mShopStockWarehouse);

        mDb.open();
        if (undo == null) {
            mDb.gcItems();
            undo = new Stack<>();
        }

        TouchAndClickListener t = new TouchAndClickListener(ViewConfiguration.get(this), mListView);
        //t.setOnClick(this);

        mAdapter = new DraftPoItemAdapter(this, mDb.fetchShopItems(mShopId), 0, t);
        mListView.setAdapter(mAdapter);
        vendorEdit.setText(mShopResPartner);
        delivertoEdit.setText(mShopStockWarehouse);
        //get ResPartner name
        db = new DatabaseHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();

        String selectQueryName = "SELECT name FROM res_partner WHERE id = '" + mShopResPartner + "' LIMIT 1";
        Cursor cursorName = dbase.rawQuery(selectQueryName, null);
        String strName = "";
        if (cursorName.moveToFirst())
            strName = cursorName.getString(cursorName.getColumnIndex("name"));
        cursorName.close();

        //get warehouse
        String selectQueryWarehouse = "SELECT name FROM stock_warehouse WHERE id = '" + mShopStockWarehouse + "' LIMIT 1";
        Cursor cursorWarehouse = dbase.rawQuery(selectQueryWarehouse, null);
        String strWarehouse = "";
        if (cursorWarehouse.moveToFirst())
            strWarehouse = cursorWarehouse.getString(cursorWarehouse.getColumnIndex("name"));
        cursorWarehouse.close();



        //Show Date
        orderDateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        DraftPoInputAcceptActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();
            }
        });
        mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                orderDateEdit.setText(date);
            }
        };

        delivertoEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputAcceptActivity.this, DeliverListActivity.class);
                startActivity(intent);
            }
        });

        currencyEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputAcceptActivity.this, CurrencyListActivity.class);
                startActivity(intent);
            }
        });

        vendorEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputAcceptActivity.this, VendorListActivity.class);
                startActivity(intent);
            }
        });

        /*
        addItemAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputAcceptActivity.this, DraftPoInputItemActivity.class);
                startActivity(intent);
            }
        });
        */


    }



    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Entry Not Correctly")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    public void onClick(ListView listView, View view) {
        //
        // launches item activity
        //
        int position = mListView.getPositionForView(view);
        Cursor c = (Cursor) mListView.getItemAtPosition(position);
        long shopId = c.getLong(INDEX_ID);
        String shopName = c.getString(INDEX_NAME);
        String status = c.getString(INDEX_STATE);
        Log.e("status ", "" + status);

        /*
        Intent intent = new Intent(getActivity(), ItemsActivity.class);
        // links to position in the DB
        intent.putExtra(ItemsActivity.INTENT_SHOP_ID_LONG, shopId);
        intent.putExtra(ItemsActivity.INTENT_SHOP_NAME_STRING, shopName);
        startActivity(intent);
*/
/*
        if(status.equals("draft")) {
            Intent intent = new Intent(getActivity(), ItemsActivity.class);
            // links to position in the DB
            intent.putExtra(ItemsActivity.INTENT_SHOP_ID_LONG, shopId);
            intent.putExtra(ItemsActivity.INTENT_SHOP_NAME_STRING, shopName);
            startActivity(intent);
        }else if (status.equals("purchase")){
            Intent intent = new Intent(getActivity(), ValidateActivity.class);
            // links to position in the DB
            intent.putExtra(ItemsActivity.INTENT_SHOP_ID_LONG, shopId);
            intent.putExtra(ItemsActivity.INTENT_SHOP_NAME_STRING, shopName);
            startActivity(intent);
        }



        // for animation
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        */
    }
}

