package com.layerfarm.purchase.draftpurchaseOrder;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.layerfarm.purchase.R;
import com.layerfarm.purchase.function.Helper;
import com.layerfarm.purchase.listActivity.*;
import com.layerfarm.purchase.sql.DatabaseMiddleman;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.purchase.utils.*;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import static android.view.View.VISIBLE;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_ID;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_NAME;
import static com.layerfarm.purchase.sql.DBContract.JoinShopItemQuery.INDEX_STATE;

public class DraftPoInputActivity extends AppCompatActivity{

    final List<Utilities.Triple<String, Float, String, String, String, String, String, String>> list = new LinkedList<>();
    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener;
    private LinearLayout linearLayoutInputDraftPO;
    private EditText orderDateEdit;
    private EditText currencyEdit;
    private EditText vendorEdit;
    private EditText delivertoEdit;
    DatabaseHelper dbase = new DatabaseHelper(this);
    Helper help = new Helper(this);
    DatabaseMiddleman mDb;
    CursorAdapter mAdapter;
    ListView mListView;
    Stack<Pair<String, Long>> undo;
    long mShopId;
    String po_Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_po_input);

        Intent intent = this.getIntent();
        if(getIntent().getExtras()!=null){
            po_Id = intent.getStringExtra("purchase_id");
        }

        Log.d("po_Id_1",""+po_Id);
        mListView = (ListView) findViewById(R.id.item_po_draft_list);

        orderDateEdit = (EditText) findViewById(R.id.orderDateEditText);
        currencyEdit= (EditText) findViewById(R.id.currencyEditText);
        vendorEdit= (EditText) findViewById(R.id.vendorEditText);
        delivertoEdit= (EditText) findViewById(R.id.deliveredEditText);
        linearLayoutInputDraftPO = (LinearLayout) findViewById(R.id.linearLayoutInputDraftPO);
        TouchAndClickListener t = new TouchAndClickListener(ViewConfiguration.get(this), mListView);
        this.setTitle("New");

        if(po_Id != null)
        {
            long number = 0;
            Log.d("po_Id_1",""+number);
            mAdapter = new DraftPoItemAdapter(this, mDb.fetchShopItems(number), 0, t);
        }else{
            long number = Long.valueOf(po_Id).longValue();
            Log.d("po_Id_1",""+number);
            mAdapter = new DraftPoItemAdapter(this, mDb.fetchShopItems(number), 0, t);
        }

        mListView.setAdapter(mAdapter);
        CurrencyListActivity.getInstance().setCurrency("");
        VendorListActivity.getInstance().setVendor("");
        DeliverListActivity.getInstance().setDelivery("");

        String name = help.getAutoNumber();
        Log.d("zzzs", "PO= " + name);
        //Show Date
        orderDateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        DraftPoInputActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();
            }
        });
        mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                orderDateEdit.setText(date);
            }
        };

        delivertoEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputActivity.this, DeliverListActivity.class);
                startActivity(intent);
            }
        });

        currencyEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputActivity.this, CurrencyListActivity.class);
                startActivity(intent);
            }
        });

        vendorEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputActivity.this, VendorListActivity.class);
                startActivity(intent);
            }
        });

        linearLayoutInputDraftPO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DraftPoInputActivity.this, DraftPoInputItemActivity.class);
                startActivity(intent);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        delivertoEdit.setText(DeliverListActivity.getInstance().getDelivery());
        currencyEdit.setText(CurrencyListActivity.getInstance().getCurrency());
        vendorEdit.setText(VendorListActivity.getInstance().getVendor());
    }


    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Entry Not Correctly")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    private AlertDialog AskSave(String po_number) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Information!!")
                .setMessage("Save Data with " +po_number+ " Number Successfully")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    public void save(View view) {
        if (orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty()) {
            AskOption();
        } else {
            if (orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty() || orderDateEdit.getText().toString().isEmpty()) {
                AskOption();
            } else {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                // Setting Dialog Title
                alertDialog.setTitle("Confirm Create PO...");
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure to create PO?");
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_plus);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                        //create draft PO
                        SQLiteDatabase db = dbase.getWritableDatabase();
                        //Save to Purchase Order
                        String date = orderDateEdit.getText().toString().trim();
                        String vendor = vendorEdit.getText().toString().trim();
                        String currency = currencyEdit.getText().toString().trim();
                        String warehouse = delivertoEdit.getText().toString().trim();

                        //get autonumber
                        String name = help.getAutoNumber();

                        //Get vendor id
                        String strVendor = help.getVendroID(vendor);

                        //Get warehouse id
                        String strWarehouse = help.getWarehouseID(warehouse);

                        Date todayDate = Calendar.getInstance().getTime();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        String today = formatter.format(todayDate);

                        //insert to shop
                        String insertShopSQL = "INSERT INTO purchase_order \n" +
                                "(shop_name, vendor_id, delivered_to, currency,state, invoice_status, amount_untaxed, amount_total, bill_status, order_date, date_planned, date_approve, deleted) VALUES \n" +
                                "(?, ? ,?, ?, ?, ?, ?, ?, ?,?,?,?,?);";
                        db.execSQL(insertShopSQL, new String[]{name, strVendor, strWarehouse, currency, "draft", "no",null,null, "Waiting_Bills",today,today,today, "0"});


                        AskSave(name);
                        linearLayoutInputDraftPO.setVisibility(VISIBLE);
                        //popUp(format(R.string.LIST_ADDED, name));
                        //Toast.makeText(DraftPoInputActivity.this, name + " Draft Created", Toast.LENGTH_SHORT).show();

                        /*
                        Intent i = new Intent(getApplicationContext(), DraftPoActivity.class);
                        startActivity(i);
                        finish();
                        */
                    }
                });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        }

    }

    public void Back(View view) {
        Intent intent = new Intent(DraftPoInputActivity.this, DraftPoActivity.class);
        startActivity(intent);
    }

    public void getTotal(){

    }
}

