package com.layerfarm.purchase.function;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;

public class Helper {
    DatabaseHelper db;
    private SQLiteDatabase database;
    public Helper(Context context){
        db = new DatabaseHelper(context);
    }
    //membuat sambungan ke database
    public void open() throws SQLException {
        database= db.getWritableDatabase();

    }
    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    public String getLastPurchaseOrderID(){
        String id = "0";
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT _id FROM purchase_order WHERE state = 'draft' ORDER BY _id DESC LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex("_id"));
        }
        return id;
    }

    public String getVendroID(String vendor){
        String id = "0";
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM res_partner WHERE name = '" + vendor + "' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex("id"));
        }
        return id;
    }

    public String getWarehouseID(String warehouse){
        String id = "0";
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM stock_warehouse WHERE name like '" + warehouse + "' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex("id"));
        }
        return id;
    }

    //fungsi digunakan untuk create di table Sale Order
    public long createSaleOrder(String total, String date_order, String partner_id, String invoice_status, String pricelist, String warehouse){
        database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM sale_order ORDER BY id DESC LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("id"));
        cursor.close();
        String name = "SO/000"+(ID+1);
        database = db.getWritableDatabase();
        //RECORDING MedicineVaccination
        ContentValues sale_order = new ContentValues();
        sale_order.put("amount_total", total);
        sale_order.put("name", name);
        sale_order.put("date_order", date_order);
        sale_order.put("partner_id", partner_id);
        sale_order.put("invoice_status", invoice_status);
        sale_order.put("pricelist_id",pricelist);
        sale_order.put("warehouse_id", warehouse);
        sale_order.put("state","draft");
        long insertSO = database.insert("sale_order", null, sale_order);
        return insertSO;
    }

    //get auto number
    public String getAutoNumber(){
        String autoNumber = "";
        database = db.getReadableDatabase();
        String selectQuery= "SELECT MAX(substr(shop_name,6)) FROM purchase_order";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                if (cursor.moveToFirst() == false) {
                    autoNumber = "PO000001";
                } else {
                    cursor.moveToLast();
                    int auto_id = cursor.getInt(0) + 1;
                    String no = String.valueOf(auto_id);
                    int NomorJual = no.length();
                    for (int j = 0; j < 6 - NomorJual; j++) {
                        no = "0" + no;
                    }
                    autoNumber = "PO" + no;
                }
            }
        }

        return autoNumber;
    }

}
