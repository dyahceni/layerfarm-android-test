package com.layerfarm.purchase.sql;


import android.app.DownloadManager;
import android.provider.BaseColumns;
import android.util.Log;

public interface DBContract {
    // note in SQLite 'false' is '0' and 'true' is '1'

    interface vendor extends BaseColumns {
        String TABLE_NAME = "vendor";
        String ID = "id";
        String COLUMN_NAME = "name";

    }

    interface restPartner extends BaseColumns {
        String TABLE_NAME = "res_partner";

        String ID = "id";
        String COLUMN_NAME_PARTNER = "name";

    }

    interface warehouse extends BaseColumns {
        String TABLE_NAME = "stock_warehouse";

        String ID = "id";
        String COLUMN_NAME_WAREHOUSE = "name";

    }

    interface ShopEntry extends BaseColumns {
        String TABLE_NAME = "purchase_order";

        String COLUMN_SHOP_NAME = "shop_name";
        String COLUMN_VENDOR_ID = "vendor_id";
        String COLUMN_DELIVERED = "delivered_to";
        String COLUMN_CURR = "currency";
        String COLUMN_STATUS = "bill_status";
        String COLUMN_ORDER_DATE = "order_date";
        String COLUMN_DELETED = "deleted";
        String COLUMN_STATE = "state";
        String COLUMN_INVOICE_STATUS = "invoice_status";
        String COLUMN_AMOUNT_TOTAL = "amount_total";
        String COLUMN_AMOUNTH_UNTAXED = "amount_untaxed";
        String COLUMN_DATE_PLANNED = "date_planned";
        String COLUMN_DATE_APPROVE = "date_approve";
    }


    interface ItemEntry extends BaseColumns {
        String TABLE_NAME = "purchase_order_line";

        String COLUMN_ITEM_NAME = "item_name";
        String COLUMN_ITEM_QUANTITY = "item_quantity";
        String COLUMN_ITEM_PRICE = "item_price";
        String COLUMN_ITEM_TAX = "item_tax";
        String COLUMN_ITEM_SUB_TOTAL = "item_sub_total";
        String COLUMN_ITEM_TOTAL = "item_total";
        String COLUMN_ITEM_DONE = "item_done";
        String COLUMN_ITEM_SHOP_ID_FK = "shop_id";
        String COLUMN_DELETED = "deleted";
        String COLUMN_ITEM_UNIT = "item_unit";
        String COLUMN_ITEM_STATE = "state";
        String COLUMN_ITEM_CATEGORY = "item_category";
        String COLUMN_ITEM_CREATE_DATE = "create_date";
        String COLUMN_ITEM_SCHEDULE_DATE = "schedule_date";
        String COLUMN_ITEM_ID = "item_id";
    }

    //
    // Queries
    //

    interface JoinShopItemQuery extends BaseColumns {
        String COLUMN_NAME = "join_name";
        String COLUMN_ALL_ITEMS_COUNT = "all_items_count";
        String COLUMN_DONE_ITEMS_COUNT = "done_items_count";
        String COLUMN_NOT_DONE_ITEMS_COUNT = "not_done_items_count";
        String COLUMN_SUM_ALL_ITEMS = "sum_all_items";
        String COLUMN_ORDER = "order_date";
        String COLUMN_PRICE = "item_price";
        String COLUMN_TAX = "item_tax";
        String COLUMN_SUB_TOTAL = "item_sub_total";
        String COLUMN_TOTAL = "item_total";
        String COLUMN_STATUS = "bill_status";
        String COLUMN_VENDOR = "vendor";
        String COLUMN_WAREHOUSE = "delivered_to";
        String COLUMN_STATE = "state";
        String COLUMN_INVOICE = "invoice_status";
        String COLUMN_CURR = "currency";
        String COLUMN_PARTNER = "nama_partner";
        String COLUMN_STOCK_WAREHOUSE = "nama_gudang";

        // query
        String QUERY = "SELECT " +
                // shop_id
                ("L."+ ShopEntry._ID) + " AS " + JoinShopItemQuery._ID + ", " +
                // shop_name
                ("L."+ ShopEntry.COLUMN_SHOP_NAME) + " AS " + JoinShopItemQuery.COLUMN_NAME +", " +
                //order_date
                "L." + ShopEntry.COLUMN_ORDER_DATE + " AS " + JoinShopItemQuery.COLUMN_ORDER + ", " +
                "L." + ShopEntry.COLUMN_DELIVERED + " AS " + JoinShopItemQuery.COLUMN_WAREHOUSE + ", " +
                "L." + ShopEntry.COLUMN_STATE + " AS " + JoinShopItemQuery.COLUMN_STATE + ", " +
                "L." + ShopEntry.COLUMN_INVOICE_STATUS + " AS " + JoinShopItemQuery.COLUMN_INVOICE + ", " +
                "L." + ShopEntry.COLUMN_CURR + " AS " + JoinShopItemQuery.COLUMN_CURR + ", " +
                // all_items_count
                "COUNT( R." + ItemEntry._ID + " ) AS " + JoinShopItemQuery.COLUMN_ALL_ITEMS_COUNT + ", " +
                //sum all item
                "SUM( R." + ItemEntry.COLUMN_ITEM_QUANTITY + " ) AS " + JoinShopItemQuery.COLUMN_SUM_ALL_ITEMS + ", " +
                "SUM( R." + ItemEntry.COLUMN_ITEM_PRICE + " ) AS " + JoinShopItemQuery.COLUMN_PRICE + ", " +
                "SUM( R." + ItemEntry.COLUMN_ITEM_TAX + " ) AS " + JoinShopItemQuery.COLUMN_TAX + ", " +
                "SUM( R." + ItemEntry.COLUMN_ITEM_SUB_TOTAL + " ) AS " + JoinShopItemQuery.COLUMN_SUB_TOTAL + ", " +
                "SUM( R." + ItemEntry.COLUMN_ITEM_TOTAL + " ) AS " + JoinShopItemQuery.COLUMN_TOTAL + ", " +
                "A." + vendor.COLUMN_NAME + " AS " + JoinShopItemQuery.COLUMN_VENDOR + ", " +
                "B." + restPartner.COLUMN_NAME_PARTNER + " AS " + JoinShopItemQuery.COLUMN_PARTNER + ", " +
                "C." + warehouse.COLUMN_NAME_WAREHOUSE + " AS " + JoinShopItemQuery.COLUMN_STOCK_WAREHOUSE + ", " +
                // items_done_sum
                "IFNULL( SUM( R." + ItemEntry.COLUMN_ITEM_DONE +" ), 0 ) AS " + JoinShopItemQuery.COLUMN_DONE_ITEMS_COUNT +", " +
                // items_not_done_sume
                "IFNULL( SUM( NOT R." + ItemEntry.COLUMN_ITEM_DONE +" ), 0 ) AS " + JoinShopItemQuery.COLUMN_NOT_DONE_ITEMS_COUNT +
                " FROM " +
                // shops table where not deleted
                "(SELECT * FROM " + ShopEntry.TABLE_NAME+ " WHERE "+ ShopEntry.COLUMN_DELETED + " = 0) L"+
                " LEFT JOIN " +
                // items table where not deleted
                "(SELECT * FROM " + ItemEntry.TABLE_NAME+ " WHERE "+ ItemEntry.COLUMN_DELETED + " = 0) R"+
                " ON " +
                ("L."+ ShopEntry._ID) +" = " + ("R."+ ItemEntry.COLUMN_ITEM_SHOP_ID_FK) +
                " INNER JOIN " +
                // items table where not deleted
                "(SELECT * FROM " + vendor.TABLE_NAME+") A " +
                " ON " +
                ("L."+ ShopEntry.COLUMN_VENDOR_ID) +" = " + ("A."+ vendor.ID) +
                " LEFT JOIN " +
                // items table where not deleted
                "(SELECT * FROM " + restPartner.TABLE_NAME+") B " +
                " ON " +
                ("L."+ ShopEntry.COLUMN_VENDOR_ID) +" = " + ("B."+ restPartner.ID) +
                " LEFT JOIN " +
                // items table where not deleted
                "(SELECT * FROM " + warehouse.TABLE_NAME+") C " +
                " ON " +
                ("L."+ ShopEntry.COLUMN_DELIVERED) +" = " + ("C."+ warehouse.ID) +
                " WHERE L." + ShopEntry.COLUMN_STATE + " = 'draft'"+
                " GROUP BY " + ("L."+ ShopEntry._ID)+" ;";

        // indexes, assumed using the same order as the query above
        int INDEX_ID = 0;
        int INDEX_NAME = 1;
        int INDEX_ORDER_DATE = 2;
        int INDEX_WAREHOUSE = 3;
        int INDEX_STATE = 4;
        int INDEX_STATUS_INV = 5;
        int INDEX_CURR = 6;
        int INDEX_ALL_ITEMS_COUNT = 7;
        int INDEX_SUM_ALL_ITEMS = 8;
        int INDEX_PRICE = 9;
        int INDEX_TAX = 10;
        int INDEX_SUB_TOTAL = 11;
        int INDEX_TOTAL = 12;
        int INDEX_VENDOR = 13;
        int INDEX_REST_PARTNER = 14;
        int INDEX_STOCK_WAREHOUSE = 15;
        int INDEX_DONE_ITEMS_COUNT = 16;
        int INDEX_NOT_DONE_ITEMS_COUNT = 17;

    }


    interface SelectShopItemsQuery {
        String QUERY = "SELECT " +
                ItemEntry._ID+", "+
                ItemEntry.COLUMN_ITEM_NAME+", "+
                ItemEntry.COLUMN_ITEM_QUANTITY+", "+
                ItemEntry.COLUMN_ITEM_DONE+", "+
                ItemEntry.COLUMN_ITEM_UNIT +", "+
                ItemEntry.COLUMN_ITEM_PRICE +", "+
                ItemEntry.COLUMN_ITEM_TAX +", "+
                ItemEntry.COLUMN_ITEM_SUB_TOTAL +", "+
                ItemEntry.COLUMN_ITEM_TOTAL +", "+
                ItemEntry.COLUMN_ITEM_CATEGORY +" "+
                " FROM " + ItemEntry.TABLE_NAME + " WHERE " +
                ItemEntry.COLUMN_DELETED + " = 0 AND " +
                ItemEntry.COLUMN_ITEM_SHOP_ID_FK + "=? ORDER BY " +
                    ItemEntry.COLUMN_ITEM_DONE + ", " +
                    // only sorts by category if not done
                    " CASE WHEN "+ ItemEntry.COLUMN_ITEM_DONE+" = 0 THEN " + ItemEntry.COLUMN_ITEM_CATEGORY + " END, "+
                    ItemEntry.COLUMN_ITEM_NAME + " COLLATE NOCASE ;";

        // indexes of query above, if order above changes so must the values below
        int INDEX_ID = 0;
        int INDEX_NAME = 1;
        int INDEX_QUANTITY = 2;
        int INDEX_IS_DONE = 3;
        int INDEX_UNIT = 4;
        int INDEX_PRICE = 5;
        int INDEX_TAX = 6;
        int INDEX_SUB_TOTAL = 7;
        int INDEX_TOTAL = 8;
        int INDEX_CATEGORY = 9;

    }

    interface SelectShopItemsQuantitiesQuery {
        String QUERY = "SELECT " +
                "COUNT( " + ItemEntry._ID + " ) AS " + JoinShopItemQuery.COLUMN_ALL_ITEMS_COUNT + ", " +
                "IFNULL( SUM( NOT " + ItemEntry.COLUMN_ITEM_DONE +" ), 0 ) AS " + JoinShopItemQuery.COLUMN_NOT_DONE_ITEMS_COUNT +
                " FROM " + ItemEntry.TABLE_NAME + " WHERE " +
                ItemEntry.COLUMN_DELETED + " = 0 AND " +
                ItemEntry.COLUMN_ITEM_SHOP_ID_FK + "=? ;";

        // indexes of query above, if order above changes so must the values below
        int INDEX_ALL_ITEMS = 0;
        int INDEX_NOT_DONE = 1;
    }

    interface ShopsQuery {
        String QUERY = "SELECT " +
                ShopEntry._ID+", "+
                ShopEntry.COLUMN_SHOP_NAME +
                " FROM  " + ShopEntry.TABLE_NAME + " WHERE " + ShopEntry.COLUMN_DELETED+ " = 0 ;";

        // indexes of query above, if order above changes so must the values below
        int INDEX_ID = 0;
        int INDEX_NAME = 1;
    }

    interface ItemNameQuery {
        String QUERY = "SELECT DISTINCT name" +
                " FROM  product_template" +
                " ORDER BY name";

        int INDEX_NAME = 0;
    }

    interface UnitsQuery {
        String QUERY = "SELECT DISTINCT " +
                ItemEntry.COLUMN_ITEM_UNIT+
                " FROM  " + ItemEntry.TABLE_NAME +
                " WHERE " + ItemEntry.COLUMN_ITEM_UNIT + " IS NOT NULL "+
                " ORDER BY "+ ItemEntry.COLUMN_ITEM_UNIT+" ;";

        int INDEX_NAME = 0;
    }

    interface CategoryQuery {
        String QUERY = "SELECT DISTINCT " +
                ItemEntry.COLUMN_ITEM_CATEGORY+
                " FROM  " + ItemEntry.TABLE_NAME +
                " WHERE " + ItemEntry.COLUMN_ITEM_CATEGORY + " IS NOT NULL "+
                " ORDER BY "+ ItemEntry.COLUMN_ITEM_CATEGORY +" ;";

        int INDEX_NAME = 0;
    }

    interface StockPickingNameQuery {
        String QUERY = "SELECT DISTINCT name" +
                " FROM  product_template" +
                " ORDER BY name";

        int INDEX_NAME = 0;
    }

    interface StockPickingTransaction {
        String QUERY_STOCK = "SELECT\n" +
                "stock_picking._id,\n" +
                "stock_picking.origin,\n" +
                "stock_picking.date_done,\n" +
                "stock_picking.location_id,\n" +
                "stock_picking.picking_type_id,\n" +
                "stock_picking.partner_id,\n" +
                "stock_picking.move_type,\n" +
                "stock_picking.state,\n" +
                "stock_picking.backorder_id,\n" +
                "stock_picking.name,\n" +
                "stock_picking.location_dest_id,\n" +
                "sum(purchase_order_line.item_quantity) as jumlah\n" +
                "FROM\n" +
                "stock_picking\n" +
                "INNER JOIN purchase_order ON purchase_order.shop_name = stock_picking.origin\n" +
                "INNER JOIN purchase_order_line on purchase_order_line.shop_id = purchase_order._id\n" +
                "GROUP BY stock_picking.origin\n";

        int INDEX_ID = 0;
        int INDEX_ORIGIN = 1;
        int INDEX_DATE_DONE = 2;
        int INDEX_LOC_ID = 3;
        int INDEX_PICK_TYPE_ID = 4;
        int INDEX_PARTNER_ID = 5;
        int INDEX_MOVE_TYPE = 6;
        int INDEX_STATE_PICK = 7;
        int INDEX_BACKORDER_ID = 8;
        int INDEX_NAME_STOCK_PICK = 9;
        int INDEX_LOC_DEST_ID = 10;
        int INDEX_QUANTITY= 11;

    }

}
