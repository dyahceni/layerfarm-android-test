package com.layerfarm.purchase.listActivity;

import android.app.ActivityOptions;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.layerfarm.purchase.R;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeliverListActivity extends AppCompatActivity{
    private static DeliverListActivity staticInstance;
    EditText search;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    DatabaseHelper SQLite = new DatabaseHelper(this);
    private static String deliver_items;

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    public static DeliverListActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new DeliverListActivity();
        }
        return staticInstance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_list);

        search = (EditText) findViewById(R.id.search);
        listView=(ListView) findViewById(R.id.listViewDeliver);

        List<String> deliverlist=new ArrayList<>();
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor=dbase.rawQuery("SELECT name FROM stock_warehouse",null);
        if(cursor.moveToFirst())
        {
            do {
                deliverlist.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,deliverlist);
        listView.setAdapter(arrayAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                DeliverListActivity.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = listView.getItemAtPosition(position).toString();
                setDelivery(s);
                finish();
            }
        });

    }

    public void setDelivery(String warehouse_items){
        this.deliver_items = warehouse_items;
    }
    public String getDelivery(){
        return deliver_items;
    }
}
