package com.layerfarm.purchase.listActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.layerfarm.purchase.R;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductListActivity extends AppCompatActivity {

    private static ProductListActivity staticInstance;
    EditText search;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    DatabaseHelper SQLite = new DatabaseHelper(this);
    private static String product_items;

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    public static ProductListActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new ProductListActivity();
        }
        return staticInstance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        search = (EditText) findViewById(R.id.search);
        listView=(ListView) findViewById(R.id.listViewProduct);

        List<String> product_list=new ArrayList<>();
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor=dbase.rawQuery("SELECT name FROM product_template",null);
        if(cursor.moveToFirst())
        {
            do {
                product_list.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        cursor.close();
        dbase.close();

        arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,product_list);
        listView.setAdapter(arrayAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                ProductListActivity.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = listView.getItemAtPosition(position).toString();
                setProduct(s);
                finish();
            }
        });

    }

    public void setProduct(String warehouse_items){
        this.product_items = warehouse_items;
    }
    public String getProduct(){
        return product_items;
    }
}
