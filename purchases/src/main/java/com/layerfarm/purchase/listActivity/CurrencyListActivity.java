package com.layerfarm.purchase.listActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.layerfarm.purchase.R;

public class CurrencyListActivity extends AppCompatActivity implements OnItemClickListener {

    private EditText mEditText;
    private static String currency_items;
    private static CurrencyListActivity staticInstance;
    EditText search;
    ListView listView;
    ArrayAdapter<String> arrayAdapter;
    private String[] currency={
            "IDR","USD","SGD","INR","EUR","JPY"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_currency);

        search = (EditText) findViewById(R.id.search);
        listView=(ListView) findViewById(R.id.listViewCurrency);

        // Adding items to listview
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, currency);
        listView.setAdapter(arrayAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                CurrencyListActivity.this.arrayAdapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listView.setOnItemClickListener(this);
    }

    public static CurrencyListActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new CurrencyListActivity();
        }
        return staticInstance;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String s = listView.getItemAtPosition(position).toString();
        setCurrency(s);
        finish();
    }


    public void setCurrency(String customer_items){
        this.currency_items = customer_items;
    }
    public String getCurrency(){
        return currency_items;
    }



}
