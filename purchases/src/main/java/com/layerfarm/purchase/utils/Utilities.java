package com.layerfarm.purchase.utils;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    static public final class Triple<A, B, C, D, E, F, G, H> {
        final public A satu;
        final public B dua;
        final public C tiga;
        final public D empat;
        final public E lima;
        final public F enam;
        final public G tujuh;
        final public H delapan;

        public Triple(A a, B b, C c, D d, E e, F f, G g, H h) {
            satu = a;
            dua = b;
            tiga = c;
            empat = d;
            lima = e;
            enam = f;
            tujuh = g;
            delapan = h;
        }
    }

}
