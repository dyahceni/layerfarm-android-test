package com.layerfarm.purchase.utils;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import com.layerfarm.purchase.R;

/**
 * Extensions to 'Fragment' to include some convenient methods for formatting string resources
 * and showing pop-up toasts on the top of the screen.
 */
public class UtilFragment extends Fragment {

    private static final int REQUEST_FILE_SAVE = 42;
    private static final int REQUEST_FILE_LOAD = 24;

    public String format(int id, Object... args) {
        return String.format(getActivity().getResources().getString(id), args);
    }

    public void popUp(String notification) {
        Toast t = Toast.makeText(getActivity().getApplicationContext(),
                notification,
                Toast.LENGTH_SHORT);
        t.setGravity(Gravity.TOP, 0, 0);
        t.show();
    }

    //
    // Save Dialog
    //



    protected File getSaveFile(String file) {
        return null;
    }

    protected File getLoadFile(String file) {
        return null;
    }

    protected void save(String file) {
        // does nothing
    }

    protected void load(String file) {
        // does nothing
    }

    //
    // convenient class to enable an item when a dialog is first shown
    //

    protected static final class EnableOnShow implements DialogInterface.OnShowListener {

        MenuItem mItem;

        public EnableOnShow(MenuItem item) {
            mItem = item;
        }

        @Override
        public void onShow(DialogInterface dialog) {
            // after dialog is showing, restore button to enabled
            mItem.setEnabled(true);
        }
    }

}
