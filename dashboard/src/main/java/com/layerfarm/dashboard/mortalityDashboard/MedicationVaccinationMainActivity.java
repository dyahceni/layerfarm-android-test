package com.layerfarm.dashboard.mortalityDashboard;

import android.support.v7.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.adapter.MedicationVaccinationAdapter;
import com.layerfarm.dashboard.model.Flock;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.setting.utils.MyDividerItemDecoration;

import java.util.ArrayList;

public class MedicationVaccinationMainActivity extends AppCompatActivity {
    DatabaseHelper SQLite = new DatabaseHelper(this);

    private DBDataSource dataSource;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Spinner spn_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_vaccination_main);
        SQLite = new DatabaseHelper(getApplicationContext());
        dataSource =  new DBDataSource(this);
        dataSource.open();

        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rvFlock = (RecyclerView) findViewById(R.id.recycler_view_ovk);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 25));
        rvFlock.setLayoutManager(layoutManager);

    }
    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
        cursor.moveToFirst();
        ArrayList<Flock> flock_items = new ArrayList<>();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Flock flock = new Flock();
            flock.setLocation_name(location);
            flock.setFlock_name(cursor.getString(cursor.getColumnIndex("name")));
            flock.setFlock_type(cursor.getString(cursor.getColumnIndex("type")));
            flock.setFlock_period(cursor.getString(cursor.getColumnIndex("period")));
            flock.setFlockId(cursor.getString(cursor.getColumnIndex("id")));
            flock_items.add(flock);
            Log.d("feed","adapter loc"+location);
            Log.d("feed","adapter loc"+cursor.getString(cursor.getColumnIndex("name")));
        }
        cursor.close();
        adapter = new MedicationVaccinationAdapter(flock_items);
        rvFlock.setAdapter(adapter);
        Log.d("feed","adapter");
    }
}
