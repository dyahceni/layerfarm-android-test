package com.layerfarm.dashboard.mortalityDashboard;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.layerfarm.dashboard.R;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;


public class DashboardMortalityActivity extends AppCompatActivity implements OnChartGestureListener, OnChartValueSelectedListener {

    LineChart lineChart ;
    private ArrayList<String> eggNumber;
    DatabaseHelper SQLite = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_mortality);
        createView() ;
    }

    private void createView() {

        ArrayList<Float> allMortality = new ArrayList<Float>();
        String selectQuery = "SELECT * FROM recording_mortality";
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst())
            do {
                allMortality.add(Float.valueOf(cursor.getString(4))); // 1 = columnindex
            } while (cursor.moveToNext());
        Log.d("zzzs)","isi mortalityCategory: "+allMortality);





        lineChart = (LineChart) findViewById(R.id.lineChartMortality);
        lineChart.setOnChartGestureListener(this);
        lineChart.setOnChartValueSelectedListener(this);

        //List<Entry> set1 = new ArrayList<Entry>() ;
        List<Entry> set2 = new ArrayList<Entry>() ;


        /*
        Entry x1 = new Entry(0f , 1f) ;
        Entry x2 = new Entry(1f , -1.1f) ;
        Entry x3 = new Entry(2f , 1.2f) ;
        Entry x4 = new Entry(3f , -1.3f) ;
        Entry x5 = new Entry(4f , 0.9f) ;
        set1.add(x1) ;
        set1.add(x2) ;
        set1.add(x3) ;
        set1.add(x4) ;
        set1.add(x5) ;

*/


/*
        Entry y1 = new Entry(5f , 2f) ;
        Entry y2 = new Entry(6f , -2.10f) ;
        Entry y3 = new Entry(7f , 2.20f) ;
        Entry y4 = new Entry(8f , -2.30f) ;
        Entry y5 = new Entry(9f , 0.2f) ;
        set2.add(y1) ;
        set2.add(y2) ;
        set2.add(y3) ;
        set2.add(y4) ;
        set2.add(y5) ;
        */

        //float[] valuesx = new float[]{1,2,3,4,5,6,7,8,9};
        //float[] valuesy = new float[]{8, 12, 13, 14, 15, 6, 7};
        //for (int i = 0; i < valuesx.length; i++) {
        //    //set2.add(new Entry(valuesx[i], stringAllEggNumber[i]));

        //}

        for (int i = 0; i < allMortality.size(); i++)
            set2.add(new Entry(allMortality.get(i),allMortality.get(i)));


        //LineDataSet lineDataSet = new LineDataSet(set1 , "表一") ;
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Weight") ;
        /*
        lineDataSet.setCircleSize(3f);
        lineDataSet.setCircleColor(Color.WHITE);
        lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet.setCubicIntensity(0.9f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setFillColor(Color.rgb(0 , 255 , 255));
        lineDataSet.setLineWidth(5f);
        lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
        */


        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));

        //List<ILineDataSet>
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChart.setData(lineData);
        //lineChart.animateX(3000);
        lineChart.invalidate();


    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: "+ me.getX() + ",y: "+me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            lineChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: "
                + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + lineChart.getLowestVisibleX()
                + ", high: " + lineChart.getHighestVisibleX());

        Log.i("MIN MAX", "xmin: " + lineChart.getXChartMin()
                + ", xmax: " + lineChart.getXChartMax()
                + ", ymin: " + lineChart.getYChartMin()
                + ", ymax: " + lineChart.getYChartMax());
    }


    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

}