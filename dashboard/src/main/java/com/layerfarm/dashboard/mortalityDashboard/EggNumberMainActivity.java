package com.layerfarm.dashboard.mortalityDashboard;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;

import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.adapter.EggNumberAdapter;
import com.layerfarm.dashboard.model.EggNumber;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.layerfarm.DatabaseHelper;

import com.layerfarm.setting.utils.MyDividerItemDecoration;

public class EggNumberMainActivity extends AppCompatActivity {

    DatabaseHelper SQLite = new DatabaseHelper(this);

    private DBDataSource dataSource;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Spinner spn_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_egg_number_main);
        SQLite = new DatabaseHelper(getApplicationContext());
        dataSource =  new DBDataSource(this);
        dataSource.open();

        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*rvFlock = (RecyclerView) findViewById(R.id.recycler_view_mortality);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);*/

        rvFlock = (RecyclerView) findViewById(R.id.recycler_view_egg_number);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 25));
        rvFlock.setLayoutManager(layoutManager);

    }
    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
        cursor.moveToFirst();
        ArrayList<EggNumber> egg_number_items = new ArrayList<>();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            EggNumber eggnumber = new EggNumber();
            eggnumber.setLocation_name(location);
            eggnumber.setFlock_name(cursor.getString(cursor.getColumnIndex("name")));
            eggnumber.setFlock_type(cursor.getString(cursor.getColumnIndex("type")));
            eggnumber.setFlock_period(cursor.getString(cursor.getColumnIndex("period")));
            eggnumber.setFlock_id(cursor.getString(cursor.getColumnIndex("id")));
            egg_number_items.add(eggnumber);
        }
        adapter = new EggNumberAdapter(egg_number_items);
        rvFlock.setAdapter(adapter);
    }


}
