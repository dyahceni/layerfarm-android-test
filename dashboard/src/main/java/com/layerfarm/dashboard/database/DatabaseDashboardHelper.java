package com.layerfarm.dashboard.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.layerfarm.dashboard.model.recordingMortality;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravi on 15/03/18.
 */

public class DatabaseDashboardHelper {
    private DatabaseHelper db;

    public List<recordingMortality> getAllMortality() {
        List<recordingMortality> notes = new ArrayList<>();
        String selectQuery = "SELECT DISTINCT location.name as Nama_lokasi, flock.name as Nama_Flock, flock.type\n" +
                "from location, flock where flock.location_id = location.id";
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                recordingMortality note = new recordingMortality();
                note.setLocation_name(cursor.getString(cursor.getColumnIndex("Nama_lokasi")));
                note.setFlock_name(cursor.getString(cursor.getColumnIndex("Nama_Flock")));
                note.setFlock_type(cursor.getString(cursor.getColumnIndex("type")));

                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    /*
    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + Note.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }
    */


}
