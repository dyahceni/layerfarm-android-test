package com.layerfarm.dashboard.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class TabPageAdapter extends FragmentPagerAdapter {
    public static int PAGE_COUNT =3;
    private final ArrayList<Fragment> mFragmentList = new ArrayList<>();
    private final ArrayList<String> mFragmentTitleList = new ArrayList<>();
    private String judulTab[] = new String[]{"Satu Fragment", "Dua Fragment", "Tiga Fragment"};
    public TabPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i){
            case 0:
                return new SatuFragment();
            case 1:
                return new DuaFragment();
            case 2:
                return new TigaFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }


}
