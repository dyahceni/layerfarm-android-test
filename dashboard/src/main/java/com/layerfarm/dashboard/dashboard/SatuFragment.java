package com.layerfarm.dashboard.dashboard;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.layerfarm.dashboard.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SatuFragment extends Fragment {


    public static SatuFragment newInstance() {
        // Required empty public constructor
        return new SatuFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_satu, container, false);
    }

}
