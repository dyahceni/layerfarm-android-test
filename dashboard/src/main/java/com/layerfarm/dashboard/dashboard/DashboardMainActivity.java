package com.layerfarm.dashboard.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.mortalityDashboard.*;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;


import java.util.ArrayList;
import java.util.Map;

public class DashboardMainActivity extends AppCompatActivity implements OnChartGestureListener, OnChartValueSelectedListener {
    LineChart lineChart;
    ViewPager mImageViewPager;
    TabLayout tabLayout;
    TabPageAdapter mAdapter;
    private DBDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_main);

        lineChart = (LineChart) findViewById(R.id.chart);
        lineChart.setOnChartGestureListener(this);
        lineChart.setOnChartValueSelectedListener(this);


        setData();


        dataSource =  new DBDataSource(this);
        String hatcdateMax = dataSource.gethatchDateMaxBird("1");
        Toast.makeText(DashboardMainActivity.this, "Hatchdate " + hatcdateMax, Toast.LENGTH_SHORT).show();


        Legend l = lineChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);

        mImageViewPager = (ViewPager) findViewById(R.id.pager);

        tabLayout = (TabLayout) findViewById(R.id.tabDots);
        mAdapter = new TabPageAdapter(getSupportFragmentManager());
        mImageViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mImageViewPager, true);
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: "+ me.getX() + ",y: "+me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            lineChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: "
                + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + lineChart.getLowestVisibleX()
                + ", high: " + lineChart.getHighestVisibleX());

        Log.i("MIN MAX", "xmin: " + lineChart.getXChartMin()
                + ", xmax: " + lineChart.getXChartMax()
                + ", ymin: " + lineChart.getYChartMin()
                + ", ymax: " + lineChart.getYChartMax());
    }


    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    // This is used to store x-axis values
    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");

        return xVals;
    }
    // This is used to store Y-axis values
    private ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(60, 0));
        yVals.add(new Entry(48, 1));
        yVals.add(new Entry(70.5f, 2));
        yVals.add(new Entry(100, 3));
        yVals.add(new Entry(180.9f, 4));

        return yVals;
    }

    private void setData() {
        ArrayList<String> xVals = setXAxisValues();

        ArrayList<Entry> yVals = setYAxisValues();

        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(yVals, "DataSet 1");
        set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);

        // set data
        lineChart.setData(data);

    }


    public void mortality_click(View view) {
        //Intent intent = new Intent(this, com.layerfarm.mortalityDashboard.MortalityMainActivity.class);
        Intent intent = new Intent(this, MortalityMainActivity.class);
        startActivity(intent);
    }

    public void chart_click(View view) {
        Intent intent = new Intent(this, FeedMainActivity.class);
        startActivity(intent);
    }

    public void population_click(View view) {
        Intent intent = new Intent(this, PopulationMainActivity.class);
        startActivity(intent);
    }

    public void egg_number_click(View view) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        try {
            // Perform action on click
            Intent intent = new Intent(this, EggNumberMainActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            AlertDialog ad = adb.create();
            ad.setMessage("Failed to Launch");
            ad.show();

        }

    }

    public void egg_weight_click(View view) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        try {
            // Perform action on click
            Intent intent = new Intent(this, EggWeightMainActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            AlertDialog ad = adb.create();
            ad.setMessage("Failed to Launch");
            ad.show();

        }
    }

    public void henday_click(View view) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        try {
            // Perform action on click
            Intent intent = new Intent(this, HendayMainActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            AlertDialog ad = adb.create();
            ad.setMessage("Failed to Launch");
            ad.show();

        }
    }

    public void fcr_click(View view) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        try {
            // Perform action on click
            Intent intent = new Intent(this, FcrMainActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            AlertDialog ad = adb.create();
            ad.setMessage("Failed to Launch");
            ad.show();

        }
    }
}
