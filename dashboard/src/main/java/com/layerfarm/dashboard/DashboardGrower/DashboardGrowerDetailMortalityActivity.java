package com.layerfarm.dashboard.DashboardGrower;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerAdapter.DashboardGrowerPerformanceDetailMortalityAdapter;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.setting.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerDetailMortalityActivity extends AppCompatActivity {

    DatabaseHelper SQLite = new DatabaseHelper(this);

    private DBDataSource dataSource;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Spinner spn_location;
    int index;
    ArrayList<String> location = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mortality_main);

//        getSupportActionBar().setTitle("Dashboard Grower");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        Set<String> keySet = DashboardGrowerModel.getInstance().getPerformance().getDetail().getLocation_flock().keySet();
//        ArrayList<String> location = new ArrayList<>(keySet);
        getDetailMortality();
        Bundle i = getIntent().getExtras();
        index = i.getInt("index");
        spn_location = (Spinner) findViewById(R.id.find_location);
//        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
//        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*rvFlock = (RecyclerView) findViewById(R.id.recycler_view_mortality);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);*/

        rvFlock = (RecyclerView) findViewById(R.id.recycler_view_mortality);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 25));
        rvFlock.setLayoutManager(layoutManager);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
    public void getDetailMortality(){
        final ProgressDialog loading = ProgressDialog.show(this, "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_grower_performance_detail_mortality";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.DashboardGrowerDetail(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                DashboardGrowerModel.GrowerPerformanceDetail dashboardLayerDetail = response.body();
                DashboardGrowerModel.GrowerPerformanceDetailFlockData data = dashboardLayerDetail.getFlock_data();
                Log.d("zzz","log= "+data.toString());
                DashboardGrowerModel.getInstance().setLocation_flock(dashboardLayerDetail.getLocation_flock());
                DashboardGrowerModel.getInstance().setFlock_data(dashboardLayerDetail.getFlock_data());
                //setupViewPager(viewPager);
                Set<String> keySet = DashboardGrowerModel.getInstance().getLocation_flock().keySet();
                location = new ArrayList<>(keySet);
                spn_location.setAdapter(new ArrayAdapter(DashboardGrowerDetailMortalityActivity.this, android.R.layout.simple_spinner_dropdown_item, location));
                spn_location.setSelected(true);
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void Submit(){
        String location = spn_location.getSelectedItem().toString();

        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = DashboardGrowerModel.getInstance().getLocation_flock().get(location);

        adapter = new DashboardGrowerPerformanceDetailMortalityAdapter(items, index);
        rvFlock.setAdapter(adapter);
    }
    public void cls_mortality(View v){
        finish();
    }

}
