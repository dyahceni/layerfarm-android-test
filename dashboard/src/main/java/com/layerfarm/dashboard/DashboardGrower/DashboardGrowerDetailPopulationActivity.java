package com.layerfarm.dashboard.DashboardGrower;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerAdapter.DashboardGrowerPerformanceDetailPopulationAdapter;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Population;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerDetailPopulationActivity extends AppCompatActivity {

    DatabaseHelper SQLite = new DatabaseHelper(this);

    ArrayList<Population> recording_population_items;
    private DBDataSource dataSource;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Spinner spn_location;
    helper help;
    int index;
    ArrayList<String> location = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_population_main);

//        getSupportActionBar().setTitle("Dashboard Grower");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Set<String> keySet = new HashSet<>();
        getDetailPopulation();
//        try{
//            keySet = DashboardGrowerModel.getInstance().getPerformance().getDetail().getLocation_flock().keySet();
//        }catch (Exception e){}
        ArrayList<String> location = new ArrayList<>(keySet);
        Bundle i = getIntent().getExtras();
        index = i.getInt("index");
        spn_location = (Spinner) findViewById(R.id.find_location);
//        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
//        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rvFlock = (RecyclerView) findViewById(R.id.recycler_population);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 25));
        rvFlock.setLayoutManager(layoutManager);

    }
    public void getDetailPopulation(){
        final ProgressDialog loading = ProgressDialog.show(this, "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_grower_performance_detail_population";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.DashboardGrowerDetail(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                DashboardGrowerModel.GrowerPerformanceDetail dashboardLayerDetail = response.body();
                DashboardGrowerModel.GrowerPerformanceDetailFlockData data = dashboardLayerDetail.getFlock_data();
                Log.d("zzz","log= "+data.toString());
                DashboardGrowerModel.getInstance().setLocation_flock(dashboardLayerDetail.getLocation_flock());
                DashboardGrowerModel.getInstance().setFlock_data(dashboardLayerDetail.getFlock_data());
                //setupViewPager(viewPager);
                Set<String> keySet = DashboardGrowerModel.getInstance().getLocation_flock().keySet();
                location = new ArrayList<>(keySet);
                spn_location.setAdapter(new ArrayAdapter(DashboardGrowerDetailPopulationActivity.this, android.R.layout.simple_spinner_dropdown_item, location));
                spn_location.setSelected(true);
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void Submit(){
        String location = spn_location.getSelectedItem().toString();

        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = DashboardGrowerModel.getInstance().getLocation_flock().get(location);

        adapter = new DashboardGrowerPerformanceDetailPopulationAdapter(items, index);
        rvFlock.setAdapter(adapter);
    }
    public Population population(String location, String flock, String type){
        Population population = new Population();
        return  population;
    }
    public void cls_population(View v){
        finish();
    }

}
