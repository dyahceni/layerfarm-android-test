package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerAdapter;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.recording.helper.helper;

import java.util.ArrayList;
import java.util.List;

public class DashboardGrowerPerformanceDetailPopulationAdapter extends RecyclerView.Adapter<DashboardGrowerPerformanceDetailPopulationAdapter.ViewHolder>  {
    LineChart linePopulationCart ;
    private ArrayList<String> population;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;

    private ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> recording_population_items;
    int index;

    public DashboardGrowerPerformanceDetailPopulationAdapter(ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> inputData, int index){
        recording_population_items = inputData;
        this.index = index;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        public LineChart linePopulationCart;
        //public CardView card;
        public ViewHolder(final View v) {
            super(v);

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();



            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valueTabel);
            linePopulationCart = (LineChart) v.findViewById(R.id.lineChartpopulation);
            MyMarkerView mv = new MyMarkerView(v.getContext(), R.layout.custom_marker_view_layout);
            mv.setChartView(linePopulationCart); // For bounds control
            linePopulationCart.setMarker(mv);
            linePopulationCart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    Log.i("Activity", "Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
                }

                @Override
                public void onNothingSelected() {

                }
            });
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            capacity = (TextView) v.findViewById(R.id.capacity);
            number_bird = (TextView) v.findViewById(R.id.bird_population);
            utilization = (TextView) v.findViewById(R.id.valueUtilization);

            //card = (CardView) v.findViewById(R.id.cv_flock);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Context context = v.getContext();
//                    Intent intent = new Intent(context, TreatmentListViewActivity.class);
//                    context.startActivity(intent);
//                }
//            });
        }
    }

    public class MyMarkerView extends MarkerView {

        private final TextView tvContent;

        public MyMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            tvContent = findViewById(R.id.tvContent);
        }

        // runs every time the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {

            if (e instanceof CandleEntry) {

                CandleEntry ce = (CandleEntry) e;

                tvContent.setText(
                        "Age : "+ Utils.formatNumber(ce.getHigh(), 0, true)+
                                " days\nPopulation : "+Utils.formatNumber(e.getY(), 0, true));
            } else {

                tvContent.setText("Age : "+Utils.formatNumber(e.getX(), 0, true)+
                        " days\nPopulation : "+Utils.formatNumber(e.getY(), 0, true));
            }

            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }

    //public MortalityAdapter(@NonNull List<GraphListItemData> pData) {
    //    data = pData;
    //}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.population_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = recording_population_items.get(position).getFarm_name();
        final String flockname = recording_population_items.get(position).getFlock_name();
        final String flocktype = recording_population_items.get(position).getFlock_type();
        final String flockperiod = recording_population_items.get(position).getFlock_period();
        final String flockid = recording_population_items.get(position).getFlock_nid();
        final String capacity = recording_population_items.get(position).getFlock_capacity();
        final String bird_population = recording_population_items.get(position).getPopulation();
        final String utilization = recording_population_items.get(position).getUtilization();
        final int age = recording_population_items.get(position).getAge();

        DashboardGrowerModel.GrowerPerformanceDetailFlockData population_data = DashboardGrowerModel.getInstance().getFlock_data();

        ArrayList<String> arr_recording_date = population_data.getRecording_date().get(Integer.parseInt(flockid));
        ArrayList<Integer> arr_population = population_data.getPopulation().get(Integer.parseInt(flockid));

        Log.d("adapter","arr_recording_date ="+arr_recording_date.size());
//        Log.d("adapter", "val_size = "+val_size);
        Log.d("adapter", "index = "+index);

        int val_size = arr_recording_date.size()-1;

        ArrayList<String> ar_recording_date = new ArrayList<>();
        ArrayList<Integer> ar_population = new ArrayList<>();
        String lastRecording = "";
        Integer total_ayam = 0;
        if (val_size <= 0){
            ar_recording_date = new ArrayList<>();
            ar_population = new ArrayList<>();
            lastRecording = "";
        }
        else {
            ar_recording_date = new ArrayList<>(arr_recording_date);
            ar_population = new ArrayList<>(arr_population);
            if (index != 0){
                for (int a = 0; a < index; a++){
                    ar_recording_date.remove(ar_recording_date.size() - 1);
                    ar_population.remove(ar_population.size() - 1);
                    Log.d("adapter", "rec_date = "+ar_recording_date.get(ar_recording_date.size() - 1));
                }
            }
//            Log.d("adapter","ar rec size = "+ar_recording_date.size());
//            Log.d("adapter","ar pop size = "+ar_recording_date.size());
            lastRecording = ar_recording_date.get(ar_recording_date.size()-1);
//            Log.d("adapter", "las = "+lastRecording);
            total_ayam = ar_population.get(ar_population.size()-1);
        }
        Log.d("adapter","ar rec size = "+ar_recording_date.size());
        Log.d("adapter","ar pop size = "+ar_recording_date.size());

//        String Query= "SELECT sum(cache_total_bird.total_bird) as total_ayam, daily_recording.recording_date, flock.name FROM cache_total_bird \n" +
//                "INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
//                "inner join flock on flock.id = daily_recording.flock_id\n" +
//                "WHERE daily_recording.flock_id = '"+flockid+"' AND daily_recording.recording_date = '"+lastRecording +"'";
//
//
//        Cursor cursor = database.rawQuery(Query, null);
//        String total_ayam = "";
//        if (cursor.moveToFirst()){
//            total_ayam = cursor.getString(cursor.getColumnIndex("total_ayam"));
//        }
//        cursor.close();

        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");
//        holder.valuePercentage.setText(total_ayam);
        holder.lastrecording.setText(recording_population_items.get(position).getLast_recorded());
        holder.capacity.setText(capacity);
        holder.number_bird.setText(bird_population);
        holder.utilization.setText(utilization+"%");
        ArrayList<Integer> index = new ArrayList<>();
        for (int i=1; i<= ar_population.size();i++){
            index.add(i);
        }


        //INISIALISASI DATA CHART
        ArrayList<Integer> valueY = ar_population;
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = index;
        int counter = age - valueY.size();
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i)+counter, valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "POPULATION") ;
        lineDataSet1.setColor(Color.BLUE);
        lineDataSet1.setCircleColor(Color.BLUE);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(1f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.linePopulationCart.setTouchEnabled(true);
        holder.linePopulationCart.setDragDecelerationFrictionCoef(0.9f);
        holder.linePopulationCart.setDragEnabled(true);
        holder.linePopulationCart.getDescription().setEnabled(false);
        holder.linePopulationCart.setScaleEnabled(false);
        holder.linePopulationCart.setDrawGridBackground(false);
        holder.linePopulationCart.setHighlightPerDragEnabled(true);
        holder.linePopulationCart.setPinchZoom(false);
        holder.linePopulationCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.linePopulationCart.animateX(500);

        holder.linePopulationCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.linePopulationCart.notifyDataSetChanged();
        holder.linePopulationCart.setData(lineData);
        holder.linePopulationCart.invalidate();


        XAxis xAxis = holder.linePopulationCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.linePopulationCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.linePopulationCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.linePopulationCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        //lineDataSet1.setLineWidth(5f);
        //lineDataSet1.setCircleRadius(0f);
        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setLineWidth(2f);
        lineDataSet1.setColor(Color.BLUE);

        //lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        //    LineData data = new LineData(lineDataSet1);


        //    holder.lineChart.setData(data);

        holder.linePopulationCart.getData().notifyDataChanged();
        holder.linePopulationCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return recording_population_items.size();
    }


//    public ArrayList<Integer> population (String flockId){
//        ArrayList<Integer> population = new ArrayList<>();
//        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
//        String Query = "";
//        if (!tgl_kosong.isEmpty()){
//            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
//                    "WHERE flock_id = '"+flockId+"' AND recording_date > '"+tgl_kosong+"'";
//
//        }
//        else {
//            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
//                    "WHERE flock_id = '"+flockId+"'";
//        }
//        Cursor cursor = database.rawQuery(Query, null);
//        cursor.moveToFirst();
//        for (int i=0; i < cursor.getCount(); i++){
//            cursor.moveToPosition(i);
//            String total = cursor.getString(cursor.getColumnIndex("total_bird"));
//            int qty = Integer.parseInt(total);
//            population.add(qty);
//        }
//        return population;
//    }


}
