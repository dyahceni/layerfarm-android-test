package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerAdapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerAdapter.DashboardLayerPerformanceDetailPopulationAdapter;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.adapter.MortalityItemsAdapter;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.layerfarm.model.SyncMortality;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.Mortality.Mortality;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DashboardGrowerPerformanceDetailMortalityAdapter extends RecyclerView.Adapter<DashboardGrowerPerformanceDetailMortalityAdapter.ViewHolder>  {
    LineChart lineMortalityCart ;
    private ArrayList<String> mortality;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    private RecyclerView.Adapter adapter;
    View view;
    ArrayList <Report.Mortality> mortality_items = new ArrayList<>();

    private ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> recordingMortalityItems;
    int index;

    public DashboardGrowerPerformanceDetailMortalityAdapter(ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> inputData, int index){
        recordingMortalityItems = inputData;
        this.index = index;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView dot;
        public TextView valuePercentage;
        public RecyclerView list_mortality;
        public LineChart lineMortalityCart;
        public ViewGroup leftLinear, rightLinear;
        ViewHolder holder;
        public ViewHolder(final View v) {
            super(v);

            view = v;

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            dataSource =  new DBDataSource(v.getContext());
            dataSource.open();

            context = v.getContext();

            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineMortalityCart = (LineChart) v.findViewById(R.id.lineChart);
            MyMarkerView mv = new MyMarkerView(v.getContext(), R.layout.custom_marker_view_layout);
            mv.setChartView(lineMortalityCart); // For bounds control
            lineMortalityCart.setMarker(mv);
            lineMortalityCart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    Log.i("Activity", "Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
                }

                @Override
                public void onNothingSelected() {

                }
            });
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            leftLinear = (ViewGroup) v.findViewById(R.id.linear_left);
            rightLinear = (ViewGroup) v.findViewById(R.id.linear_right);
            list_mortality = (RecyclerView) v.findViewById(R.id.mortality_list);
            //mortalityType = (TextView) v.findViewById(R.id.mortality_type);
            //mortalityValue = (TextView) v.findViewById(R.id.value);

        }
    }

    public class MyMarkerView extends MarkerView {

        private final TextView tvContent;

        public MyMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            tvContent = findViewById(R.id.tvContent);
        }

        // runs every time the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            DecimalFormat df = new DecimalFormat("###.###");
            if (e instanceof CandleEntry) {

                CandleEntry ce = (CandleEntry) e;

                tvContent.setText(
                        "Age : "+ Utils.formatNumber(ce.getHigh(), 0, true)+
                                " days\nMortality : "+df.format(e.getY()));
            } else {

                tvContent.setText("Age : "+Utils.formatNumber(e.getX(), 0, true)+
                        " days\nMortality : "+df.format(e.getY()));
            }

            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mortality_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = recordingMortalityItems.get(position).getFarm_name();
        final String flockname = recordingMortalityItems.get(position).getFlock_name();
        final String flocktype = recordingMortalityItems.get(position).getFlock_type();
        final String flockperiod = recordingMortalityItems.get(position).getFlock_period();
        final String flockid = recordingMortalityItems.get(position).getFlock_nid();
        final int age = recordingMortalityItems.get(position).getAge();
        final String total_percentage = recordingMortalityItems.get(position).getTotal_percent();
//        final ArrayList<SyncMortality> available_mortality = recordingMortalityItems.get(position).getAvailable_mortality();
//        ArrayList<Mortality> mortalities = new ArrayList<>();
//        try {
//            if (available_mortality.size()>0){
//                for (int i =0; i<available_mortality.size(); i++){
//                    Mortality mortality = new Mortality();
//                    mortality.setName(available_mortality.get(i).getName());
////                    Field field = DashboardGrowerModel.GrowerPerformanceDetailLocationFlock.class.getField(available_mortality.get(i).getName());
////                    DataObject o = new DataObject();
////                    mortality.setValue(recordingMortalityItems.get(position).getmo);
//
//                }
//            }
//        }catch (Exception e){}

        DashboardGrowerModel.GrowerPerformanceDetailFlockData mortality_data = DashboardGrowerModel.getInstance().getFlock_data();
        ArrayList<String> arr_recording_date = mortality_data.getRecording_date().get(Integer.parseInt(flockid));
        ArrayList<Float> arr_mortality = mortality_data.getMortality().get(Integer.parseInt(flockid));

        int val_size = arr_recording_date.size() - index;
        ArrayList<String> ar_recording_date = new ArrayList<>();
        ArrayList<Float> ar_mortality = new ArrayList<>();
        String lastRecording ="";
        if (val_size <= 0){
            ar_recording_date = new ArrayList<>();
            ar_mortality = new ArrayList<>();
            lastRecording = "";
        }
        else {
            ar_recording_date = new ArrayList<>(arr_recording_date);
            ar_mortality = new ArrayList<>(arr_mortality);
            if (index != 0){
                for (int a = 0; a < index; a++){
                    ar_recording_date.remove(arr_recording_date.size() - 1);
                    ar_mortality.remove(arr_mortality.size() - 1);
                }
            }
            lastRecording = ar_recording_date.get(ar_recording_date.size()-1);
        }

        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");

        holder.lastrecording.setText(recordingMortalityItems.get(position).getLast_recorded());
        holder.valuePercentage.setText(total_percentage);
        if (!recordingMortalityItems.get(position).getMortality().isEmpty()){
            mortality_items = recordingMortalityItems.get(position).getMortality();
            for (int i =0; i< mortality_items.size(); i++){
                Log.d("mortality", "name = "+mortality_items.get(i).getName()+" value = "+mortality_items.get(i).getValue());
            }
        }
        int numberOfColumns = 2;
        holder.list_mortality.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
        adapter = new MortalityItemsAdapter(mortality_items);
        holder.list_mortality.setAdapter(adapter);



//        String Query= "SELECT * FROM cache_mortality INNER JOIN daily_recording on daily_recording.id = cache_mortality.daily_recording_id \n" +
//                "WHERE flock_id = '"+flockid+"' AND recording_date = '"+lastRecording+"'";
//        Cursor cursor = database.rawQuery(Query, null);
//        String value = "";
//        if (cursor.moveToFirst()){
//            value = cursor.getString(cursor.getColumnIndex("value"));
//        }
//        cursor.close();
//        String[] mor ;
//        if (!value.isEmpty()){
//            mor = value.split("/");
//            String[] total_mortality = mor[0].split("_");
//            double total_value = Integer.parseInt(total_mortality[1]);
//            double total_population = help.getTotalBird(flockid, lastRecording);
//            double persentage = (total_value / total_population)*100;
//            DecimalFormat df = new DecimalFormat("#.###");
//            holder.valuePercentage.setText(df.format(persentage) + " %");
//
//            ArrayList<Mortality> mortalities = new ArrayList<>();
//            for (int i = 1; i < mor.length; i++){
//                Mortality mortality = new Mortality();
//                String[] sp = mor[i].split("_");
//                String name = getMortalityCategory(Integer.parseInt(sp[0]));
//                String die = sp[1];
//                mortality.setName(name);
//                mortality.setValue(die);
//                mortalities.add(mortality);
//            }
//
//            int left = mortalities.size() / 2;
//            int mod = mortalities.size() % 2;
//            int val_left = left+mod;
//
//            for (int a = 0; a<mortalities.size(); a++){
//                if (a<val_left)
//                    AddLeft(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
//                else
//                    AddRight(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
//            }
//        }
        ArrayList<Integer> index = new ArrayList<>();
        for (int i=1; i<= ar_mortality.size();i++){
            index.add(i);
        }

        //INISIALISASI DATA CHART
        ArrayList<Float> valueY = ar_mortality;
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = index;
        int counter = age - valueY.size();
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i)+counter, valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "MORTALITY") ;
        lineDataSet1.setColor(Color.RED);
        lineDataSet1.setCircleColor(Color.RED);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(1f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineMortalityCart.setTouchEnabled(true);
        holder.lineMortalityCart.setDragDecelerationFrictionCoef(0.9f);
        holder.lineMortalityCart.setDragEnabled(true);
        holder.lineMortalityCart.getDescription().setEnabled(false);
        holder.lineMortalityCart.setScaleEnabled(false);
        holder.lineMortalityCart.setDrawGridBackground(false);
        holder.lineMortalityCart.setHighlightPerDragEnabled(true);
        holder.lineMortalityCart.setPinchZoom(false);
        holder.lineMortalityCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineMortalityCart.animateX(500);

        holder.lineMortalityCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineMortalityCart.notifyDataSetChanged();
        holder.lineMortalityCart.setData(lineData);
        holder.lineMortalityCart.invalidate();


        XAxis xAxis = holder.lineMortalityCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineMortalityCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineMortalityCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineMortalityCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);

        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setColor(Color.RED);

        holder.lineMortalityCart.getData().notifyDataChanged();
        holder.lineMortalityCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return recordingMortalityItems.size();
    }

    public void AddLeft(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.mortality_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.mortality_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.leftLinear.addView(leftView, holder.leftLinear.getChildCount());
    }
    public void AddRight(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.mortality_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.mortality_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.rightLinear.addView(leftView, holder.rightLinear.getChildCount());
    }
}
