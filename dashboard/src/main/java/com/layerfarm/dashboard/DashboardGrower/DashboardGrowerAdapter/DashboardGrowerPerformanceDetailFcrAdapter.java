package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerAdapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerAdapter.DashboardLayerPerformanceDetailPopulationAdapter;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.recording.helper.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DashboardGrowerPerformanceDetailFcrAdapter extends RecyclerView.Adapter<DashboardGrowerPerformanceDetailFcrAdapter.ViewHolder>  {
    LineChart lineFcrCart ;
    private ArrayList<String> fcr;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;

    private ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> fcr_items;
    int index;
    public DashboardGrowerPerformanceDetailFcrAdapter(ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> inputData, int index){
        fcr_items = inputData;
        this.index = index;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        public LineChart lineChartFcr;

        public TextView egg_consume;
        public TextView feed_consume;
        //public CardView card;
        public ViewHolder(final View v) {
            super(v);

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();



            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineChartFcr = (LineChart) v.findViewById(R.id.lineChartFcr);
            MyMarkerView mv = new MyMarkerView(v.getContext(), R.layout.custom_marker_view_layout);
            mv.setChartView(lineChartFcr); // For bounds control
            lineChartFcr.setMarker(mv);
            lineChartFcr.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    Log.i("Activity", "Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
                }

                @Override
                public void onNothingSelected() {

                }
            });
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            capacity = (TextView) v.findViewById(R.id.capacity);
            number_bird = (TextView) v.findViewById(R.id.bird_population);
            utilization = (TextView) v.findViewById(R.id.valueUtilization);
            egg_consume  = (TextView) v.findViewById(R.id.egglaidValue);
            feed_consume  = (TextView) v.findViewById(R.id.feedValue);

            //card = (CardView) v.findViewById(R.id.cv_flock);
        }
    }

    public class MyMarkerView extends MarkerView {

        private final TextView tvContent;

        public MyMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            tvContent = findViewById(R.id.tvContent);
        }

        // runs every time the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {

            if (e instanceof CandleEntry) {

                CandleEntry ce = (CandleEntry) e;

                tvContent.setText(
                        "Age : "+ Utils.formatNumber(ce.getHigh(), 0, true)+
                                " days\nPopulation : "+Utils.formatNumber(e.getY(), 0, true));
            } else {

                tvContent.setText("Age : "+Utils.formatNumber(e.getX(), 0, true)+
                        " days\nPopulation : "+Utils.formatNumber(e.getY(), 0, true));
            }

            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }

    //public MortalityAdapter(@NonNull List<GraphListItemData> pData) {
    //    data = pData;
    //}

    @Override
    public DashboardGrowerPerformanceDetailFcrAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fcr_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        DashboardGrowerPerformanceDetailFcrAdapter.ViewHolder vh = new DashboardGrowerPerformanceDetailFcrAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DashboardGrowerPerformanceDetailFcrAdapter.ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = fcr_items.get(position).getFarm_name();
        final String flockname = fcr_items.get(position).getFlock_name();
        final String flocktype = fcr_items.get(position).getFlock_type();
        final String flockperiod = fcr_items.get(position).getFlock_period();
        final String flockid = fcr_items.get(position).getFlock_nid();
        final String feedconsumption = fcr_items.get(position).getFeed_consumption();
        final String productionweight = fcr_items.get(position).getProduction_weight();
        final int age = fcr_items.get(position).getAge();
        //final String capacity = fcr_items.get(position).getCapacity();
//        final String bird_population = fcr_items.get(position).getNumber_bird();
        //final String utilization = fcr_items.get(position).getUtility();

//        final String lastRecording = help.getDateLastRecording(flockid);

        //feed total
//        String Query= "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
//                "from daily_recording\n" +
//                "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
//                "inner join flock on flock.id = daily_recording.flock_id \n" +
//                "WHERE flock_id = '" + flockid + "' AND daily_recording.recording_date = '"+lastRecording +"'group by flock.id\n" +
//                "ORDER by daily_recording.recording_date asc";
//
//        Cursor cursor = database.rawQuery(Query, null);
//        String total_pakan = "";
//        if (cursor.moveToFirst()){
//            total_pakan = cursor.getString(cursor.getColumnIndex("jumlah_pakan"));
//        }
//        cursor.close();
//
//        //get egg total
//        Cursor cursorEgg = database.rawQuery("SELECT recording_production.egg_weight as jumlah ,flock.name, daily_recording.recording_date from daily_recording\n" +
//                "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
//                "inner join flock on flock.id = daily_recording.flock_id \n" +
//                "WHERE flock_id = '" + flockid + "'AND daily_recording.recording_date = '"+lastRecording +"'  group by flock.id \n" +
//                "ORDER by daily_recording.recording_date asc", null);
//        String eggTotal = "";
//        if (cursorEgg.moveToFirst())
//            eggTotal = cursorEgg.getString(cursorEgg.getColumnIndex("jumlah"));
//        cursorEgg.close();

//        if (!eggTotal.isEmpty()) {
//            double total_feed = Integer.parseInt(total_pakan);
//            double total_telur = Integer.parseInt(eggTotal);
//            double persentage = (total_feed / total_telur);
//            DecimalFormat df = new DecimalFormat("#.###");
////
////            // Displaying dot from HTML character code
////            holder.dot.setText(Html.fromHtml("&#8226;"));
////            holder.location_name.setText(location + "-" + flockname + "-" + "(" + flocktype + " " + flockperiod + ")");
////            holder.valuePercentage.setText(df.format(persentage) + " %");
////            holder.lastrecording.setText(lastRecording);
////            holder.egg_consume.setText(eggTotal);
////            holder.feed_consume.setText(total_pakan);
////            //holder.utilization.setText(utilization);
//
//        }
        DashboardGrowerModel.GrowerPerformanceDetailFlockData fcr_data = DashboardGrowerModel.getInstance().getFlock_data();
        ArrayList<String> arr_recording_date = new ArrayList<>();
        arr_recording_date = fcr_data.getFcr_recording_date().get(Integer.parseInt(flockid));
        ArrayList<Double> arr_fcr = fcr_data.getFcr().get(Integer.parseInt(flockid));
        ArrayList<String> ar_recording_date = new ArrayList<>();
        ArrayList<Double>  ar_fcr = new ArrayList<>();
        String lastRecording  ="";
        try {
            int val_size = arr_recording_date.size() - index;

            if (val_size <= 0){
                ar_recording_date = new ArrayList<>();
                ar_fcr = new ArrayList<>();
            }
            else {
                ar_recording_date = new ArrayList<>(arr_recording_date);
                ar_fcr = new ArrayList<>(arr_fcr);
                if (index != 0){
                    for (int a = 0; a < index; a++){
                        ar_recording_date.remove(ar_recording_date.size() - 1);
                        ar_fcr.remove(ar_fcr.size() - 1);
                    }
                }
                lastRecording = ar_recording_date.get(ar_recording_date.size()-1);
            }

            holder.lastrecording.setText(lastRecording);
        }catch (Exception e){}

        DecimalFormat df = new DecimalFormat("#.###");
        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" + "(" + flocktype + " " + flockperiod + ")");
        holder.egg_consume.setText(productionweight);
        holder.feed_consume.setText(feedconsumption);
//        holder.valuePercentage.setText(df.format(persentage) + " %");

//        holder.egg_consume.setText(eggTotal);
//        holder.feed_consume.setText(total_pakan);
        //holder.utilization.setText(utilization);



        ArrayList<Integer> indexs = new ArrayList<>();
        for (int i=1; i<= ar_fcr.size();i++){
            indexs.add(i);
        }
        //INISIALISASI DATA CHART
        ArrayList<Double> valueY = ar_fcr;
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = indexs;
        int counter = age - valueY.size();
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i)+counter, valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "FCR") ;
        lineDataSet1.setColor(Color.GREEN);
        lineDataSet1.setCircleColor(Color.GREEN);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(1f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineChartFcr.setTouchEnabled(true);
        holder.lineChartFcr.setDragDecelerationFrictionCoef(0.9f);
        holder.lineChartFcr.setDragEnabled(true);
        holder.lineChartFcr.getDescription().setEnabled(true);
        holder.lineChartFcr.setScaleEnabled(false);
        holder.lineChartFcr.setDrawGridBackground(false);
        holder.lineChartFcr.setHighlightPerDragEnabled(true);
        holder.lineChartFcr.setPinchZoom(false);
        holder.lineChartFcr.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineChartFcr.animateX(500);

        holder.lineChartFcr.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineChartFcr.notifyDataSetChanged();
        holder.lineChartFcr.setData(lineData);
        holder.lineChartFcr.invalidate();


        XAxis xAxis = holder.lineChartFcr.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineChartFcr.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineChartFcr.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineChartFcr.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        //lineDataSet1.setLineWidth(5f);
        //lineDataSet1.setCircleRadius(0f);
        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setColor(Color.GREEN);

        //lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        //    LineData data = new LineData(lineDataSet1);


        //    holder.lineChart.setData(data);

        holder.lineChartFcr.getData().notifyDataChanged();
        holder.lineChartFcr.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return fcr_items.size();
    }


    public ArrayList<Float> fcr (String flockId){
        ArrayList<Float> fcrList = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String QueryTotalPakan = "";
        String QueryTotalTelur = "";
        if (!tgl_kosong.isEmpty()){
            //get feed total
            QueryTotalPakan = "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
                    "from daily_recording\n" +
                    "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' AND daily_recording.recording_date > '"+tgl_kosong +"'group by flock.id\n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur = "SELECT recording_production.egg_weight as jumlah_telur ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "'AND daily_recording.recording_date > '"+tgl_kosong +"'  group by flock.id \n" +
                    "ORDER by daily_recording.recording_date asc";


        }
        else {
            //get feed total
            QueryTotalPakan = "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
                    "from daily_recording\n" +
                    "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur ="SELECT recording_production.egg_weight as jumlah_telur ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

        }

        Cursor cursorAyam = database.rawQuery(QueryTotalPakan, null);
        Cursor cursorTelur = database.rawQuery(QueryTotalTelur, null);

        cursorAyam.moveToFirst();
        cursorTelur.moveToFirst();

        for (int i=0; i < cursorAyam.getCount(); i++){
            cursorAyam.moveToPosition(i);
            String totalpakan = cursorAyam.getString(cursorAyam.getColumnIndex("jumlah_pakan"));

            cursorTelur.moveToPosition(i);
            String totaltelur = cursorTelur.getString(cursorTelur.getColumnIndex("jumlah_telur"));

            float total_egg = Integer.parseInt(totaltelur);
            float total_feed = Integer.parseInt(totalpakan);
            Log.d("TELUR", "" + total_egg);
            Log.d("AYAM", "" + total_feed);

            float hendaypercentage = (total_feed / total_egg);
            Log.d("PERSEN", "" + hendaypercentage);
            fcrList.add(hendaypercentage);

        }
        Log.d("fcrList", "" + fcrList);
        return fcrList;
    }

}
