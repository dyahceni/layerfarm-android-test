package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SyncMortality;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.github.mikephil.charting.data.Entry;

public class DashboardGrowerFragmentOverview extends Fragment {
    PieChart pieChart;
    ArrayList<String> value_chart;
    TextView quantity;
    TabLayout mTabLayout;
    LinearLayout timeout, main_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_grower_fragment_overview, container, false);
        mTabLayout = (TabLayout) view.findViewById(R.id.tabs);
//        Button try_again = (Button) view.findViewById(R.id.try_again);
//        try_again.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                refresh();
//            }
//        });
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);
        Log.d("Fragment", "Fragment grower overview");
        pieChart = (PieChart) view.findViewById(R.id.piechart);
        pieChart.setUsePercentValues(true);
        //Chart Mortality
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf"));
        pieChart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);
        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);
        pieChart.setDrawCenterText(true);
        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        int colorBlack = Color.parseColor("#000000");
        pieChart.setEntryLabelColor(colorBlack);
        pieChart.setDrawEntryLabels(false);
        pieChart.setDrawSliceText(false);

        pieChart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        //l.setEnabled(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        quantity = (TextView)view.findViewById(R.id.quantity);
        final ProgressDialog loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_grower_data";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel> call = apiInterfaceJson.DashboardGrower(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel> call, Response<DashboardGrowerModel> response) {
                main_layout.setVisibility(View.VISIBLE);
                timeout.setVisibility(View.GONE);
                DashboardGrowerModel dashboardGrowerModel = response.body();
                DashboardGrowerModel.getInstance().setOverview(dashboardGrowerModel.getOverview());
                DashboardGrowerModel.getInstance().setLast_recording(dashboardGrowerModel.getLast_recording());
                DashboardGrowerModel.getInstance().setAvailable_mortality(dashboardGrowerModel.getAvailable_mortality());
//                DashboardGrowerModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
//                DashboardGrowerModel.getInstance().setFinancial(dashboardGrowerModel.getFinancial());
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//                Date myDate = null;
//                String sMyDate="";
//                try {
//                    myDate = sdf.parse(dashboardGrowerModel.getLast_recording());
//                    sdf.applyPattern("EEEE, d MMM yyyy");
//                    sMyDate = sdf.format(myDate);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
                onTabTapped(0);
                loading.dismiss();

//                date.setText(sMyDate);
//                setupViewPager(viewPager);
            }

            @Override
            public void onFailure(Call<DashboardGrowerModel> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });


        //set default tab mulai dari 0

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });


//        pieChart.setEntryLabelTextSize(14);


        return view;
    }
    public void refresh(){
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }
    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                // Do something when first tab is tapped here
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_today();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 1:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_1w();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 2:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_1m();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 3:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_3m();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 4:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_6m();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 5:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_ytd();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 6:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_1y();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            case 7:
                value_chart = DashboardGrowerModel.getInstance().getOverview().getM_2y();
                setChart(value_chart);
                Log.d("click","position : "+position);
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    public void setChart(ArrayList<String> value){
        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        ArrayList<SyncMortality> available_mortality = DashboardGrowerModel.getInstance().getAvailable_mortality();
        ArrayList<PieEntry> yvalues = new ArrayList<PieEntry>();
        int qty =0;
        for (int i=0; i< available_mortality.size(); i++){
            Log.d("zzz", "valu i = "+value.get(i));
            if (Integer.parseInt(value.get(i))>0) {
                yvalues.add(new PieEntry(Integer.parseInt(value.get(i)), available_mortality.get(i).getHuman_name()));
                qty += Integer.parseInt(value.get(i));
            }
        }
        BigDecimal quant = new BigDecimal(qty);
        DecimalFormat dFormat = new DecimalFormat("####,###,###.##");
        quantity.setText(dFormat.format(quant)+" birds");

        PieDataSet dataSet = new PieDataSet(yvalues, "Mortality");

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);


        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setUsingSliceColorAsValueLineColor(true);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.BLACK);


        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();
    }
}
