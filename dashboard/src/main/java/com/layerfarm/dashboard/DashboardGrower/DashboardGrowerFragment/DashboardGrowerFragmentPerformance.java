package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerDetailFcrActivity;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerDetailMortalityActivity;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerDetailPopulationActivity;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerFragmentPerformance extends Fragment {
    LineChart chart_population, chart_fcr, chart_mortality;
    ArrayList<String> value_chart;
    ArrayList<Integer> population;
    ArrayList<Float> mortality;
    ArrayList<Float> fcr;
    TextView population_value, fcr_value, mortality_value, population_percentage, fcr_percentage, mortality_percentage;
    ImageView img_population, img_fcr, img_mortality;
    LinearLayout detail_population, detail_fcr, detail_mortality;
    LinearLayout timeout, main_layout;
    int index = 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_grower_fragment_performance, container, false);
        Log.d("Fragment", "Fragment grower performance");
//        Button try_again = (Button) view.findViewById(R.id.try_again);
//        try_again.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                refresh();
//            }
//        });
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);
        TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.tabs);
        population_value = (TextView)view.findViewById(R.id.population_value);
        fcr_value = (TextView) view.findViewById(R.id.fcr_value);
        mortality_value = (TextView) view.findViewById(R.id.mortality_value);
        population_percentage = (TextView) view.findViewById(R.id.population_percentage);
        fcr_percentage = (TextView) view.findViewById(R.id.fcr_percentage);
        mortality_percentage = (TextView) view.findViewById(R.id.mortality_percentage);
        chart_population = (LineChart) view.findViewById(R.id.chart_population);
        chart_fcr = (LineChart)view.findViewById(R.id.chart_fcr);
        chart_mortality = (LineChart)view.findViewById(R.id.chart_mortality);
        detail_population = (LinearLayout)view.findViewById(R.id.detail_population);
        detail_fcr = (LinearLayout) view.findViewById(R.id.detail_fcr);
        detail_mortality = (LinearLayout) view.findViewById(R.id.detail_mortality);
        detail_population.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DashboardGrowerDetailPopulationActivity.class);
                intent.putExtra("index", index);
                startActivity(intent);
            }
        });
        detail_mortality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DashboardGrowerDetailMortalityActivity.class);
                intent.putExtra("index", index);
                startActivity(intent);
            }
        });
        detail_fcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DashboardGrowerDetailFcrActivity.class);
                intent.putExtra("index", index);
                startActivity(intent);
            }
        });

        img_population = (ImageView)view.findViewById(R.id.img_population);
        img_fcr = (ImageView)view.findViewById(R.id.img_fcr);
        img_mortality = (ImageView)view.findViewById(R.id.img_mortality);

        final ProgressDialog loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_grower_performance";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel> call = apiInterfaceJson.DashboardGrower(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel> call, Response<DashboardGrowerModel> response) {
                main_layout.setVisibility(View.VISIBLE);
                timeout.setVisibility(View.GONE);
                DashboardGrowerModel dashboardGrowerModel = response.body();
                DashboardGrowerModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
                population = DashboardGrowerModel.getInstance().getPerformance().getPopulation();
                mortality = DashboardGrowerModel.getInstance().getPerformance().getMortality();
                fcr = DashboardGrowerModel.getInstance().getPerformance().getFcr();
                onTabTapped(0);
                loading.dismiss();
            }

            @Override
            public void onFailure(Call<DashboardGrowerModel> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });



        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        return view;
    }

    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                index = 0;
                // Do something when first tab is tapped here
                setChartPopulation(1);
                setChartMortality(1);
                setChartFcr(1);
                break;
            case 1:
                index = 1;
                setChartPopulation(2);
                setChartMortality(2);
                setChartFcr(2);
                break;
            case 2:
                index = 2;
                setChartPopulation(3);
                setChartMortality(3);
                setChartFcr(3);
                break;
            case 3:
                index = 3;
                setChartPopulation(4);
                setChartMortality(4);
                setChartFcr(4);
                break;
            case 4:
                index = 4;
                setChartPopulation(5);
                setChartMortality(5);
                setChartFcr(5);
                break;
            case 5:
                index = 5;
                setChartPopulation(6);
                setChartMortality(6);
                setChartFcr(6);
                break;
            case 6:
                index = 6;
                setChartPopulation(7);
                setChartMortality(7);
                setChartFcr(7);
                break;
            case 7:
                index = 7;
                setChartPopulation(8);
                setChartMortality(8);
                setChartFcr(8);
                break;
            default:
                index = 8;
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }

    public void refresh(){
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }

    public void setChartPopulation(int index){
//        if (population.size() > 0)
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        int diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try {
                set2.add(new Entry(i, population.get(range)));
                population_qty += population.get(range);
                if (i == 0){
                    diff = population.get(range);
                }
                else
                    diff = population.get(range) - population.get(range+1);
//            }
//            catch (Exception e){
//                Log.d("zzz","null performance");
//            }

//            Log
//            Log.d("zzz","range "+population.get(range));
//            Log.d("zzz","range +1"+population.get(range+1));
            Log.d("zzz","difff: "+diff);
            range --;
        }
        if (population.get(a) != null) {
            population_value.setText(String.valueOf(df.format(population.get(a))));
        }
        if (diff < 0){
            img_population.setImageResource(R.drawable.ic_down);
        }
        else
            img_population.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" populasi: "+population.get(a));
//        try{
            int val = population.get(a);
            float persen =0;
            if (val != 0){
                persen = diff  * 100 / val ;
            }
            else
                persen = diff * 100;
            Log.d("zzz"," persen: "+persen);
            population_percentage.setText(diff+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}



        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        chart_population.setData(lineData);
        chart_population.setDrawGridBackground(false);
        chart_population.getAxisLeft().setEnabled(false);
        chart_population.getAxisRight().setEnabled(false);
        chart_population.setNoDataText("There is no Data");
        chart_population.getXAxis().setEnabled(false);
        chart_population.getDescription().setEnabled(false);
        chart_population.animateX(2000);
        chart_population.invalidate();
    }
    public void setChartMortality(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try {
                set2.add(new Entry(i, mortality.get(range)));
                population_qty += mortality.get(range);
                if (i == 0){
                    diff = mortality.get(range);
                }
                else
                    diff = mortality.get(range) - mortality.get(range+1);
//            }catch (Exception e){}

            Log.d("zzz","difff: "+diff);
            range --;
        }
        if (mortality.get(a) != null){
            mortality_value.setText(String.valueOf(df.format(mortality.get(a))));
        }
        if (diff < 0){
            img_mortality.setImageResource(R.drawable.ic_down);
//            diff = Math.abs(diff);
        }
        else
            img_mortality.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" populasi: "+population.get(a));
//        try {
            float val = mortality.get(a);
            float persen;
            if (val != 0){
                persen = diff  * 100 / val ;
            }
            else
                persen = diff * 100;
            Log.d("zzz"," persen: "+persen);
            mortality_percentage.setText(diff+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Mortality") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        chart_mortality.setData(lineData);
        chart_mortality.setDrawGridBackground(false);
        chart_mortality.getAxisLeft().setEnabled(false);
        chart_mortality.getAxisRight().setEnabled(false);
        chart_mortality.setNoDataText("There is no Data");
        chart_mortality.getXAxis().setEnabled(false);
        chart_mortality.getDescription().setEnabled(false);
        chart_mortality.animateX(2000);
        chart_mortality.invalidate();
    }
    public void setChartFcr(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try{
                set2.add(new Entry(i, fcr.get(range)));
                population_qty += fcr.get(range);
                if (i == 0){
                    diff = fcr.get(range);
                }
                else
                    diff = fcr.get(range) - fcr.get(range+1);
                Log.d("zzz","difff: "+diff);
//            }catch (Exception e){
//            }

            range --;
        }
        if (fcr.get(a) != null){
            fcr_value.setText(String.valueOf(df.format(fcr.get(a))));
        }
        if (diff < 0){
            img_fcr.setImageResource(R.drawable.ic_down);
        }
        else
            img_fcr.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" populasi: "+fcr.get(a));
//        try {
            float val = fcr.get(a);
            float persen =0;
            if (val != 0){
                persen = diff  * 100 / val ;
            }
            else
                persen = diff * 100;
            Log.d("zzz"," persen: "+persen);
            fcr_percentage.setText(diff+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        chart_fcr.setData(lineData);
        chart_fcr.setDrawGridBackground(false);
        chart_fcr.getAxisLeft().setEnabled(false);
        chart_fcr.getAxisRight().setEnabled(false);
        chart_fcr.setNoDataText("There is no Data");
        chart_fcr.getXAxis().setEnabled(false);
        chart_fcr.getDescription().setEnabled(false);
        chart_fcr.animateX(2000);
        chart_fcr.invalidate();
    }

}
