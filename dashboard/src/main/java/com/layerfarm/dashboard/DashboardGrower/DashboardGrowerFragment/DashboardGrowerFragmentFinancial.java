package com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerFragmentFinancial extends Fragment {
    TextView finance_status, currency, finance_value, income, expense, product_price;
    DashboardGrowerModel.GrowerFinancialModel value_chart = new DashboardGrowerModel.GrowerFinancialModel();
    LinearLayout timeout, main_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_grower_fragment_financial, container, false);
        Log.d("Fragment", "Fragment grower financial");
        TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.tabs);
//        Button try_again = (Button) view.findViewById(R.id.try_again);
//        try_again.setOnClickListener(this);
//        try_again.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                refresh();
//            }
//        });
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);
        finance_status = (TextView)view.findViewById(R.id.finance_status);
        finance_value = (TextView)view.findViewById(R.id.finance_value);
        currency = (TextView)view.findViewById(R.id.currency);
        income = (TextView)view.findViewById(R.id.income);
        expense = (TextView)view.findViewById(R.id.expense);
        product_price = (TextView)view.findViewById(R.id.product_price);

        final ProgressDialog loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_grower_financial";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel> call = apiInterfaceJson.DashboardGrower(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel> call, Response<DashboardGrowerModel> response) {
                DashboardGrowerModel dashboardGrowerModel = response.body();
                main_layout.setVisibility(View.VISIBLE);
                timeout.setVisibility(View.GONE);
//                DashboardGrowerModel.getInstance().setOverview(dashboardGrowerModel.getOverview());
//                DashboardGrowerModel.getInstance().setLast_recording(dashboardGrowerModel.getLast_recording());
//                DashboardGrowerModel.getInstance().setAvailable_mortality(dashboardGrowerModel.getAvailable_mortality());
//                DashboardGrowerModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
                DashboardGrowerModel.getInstance().setFinancial(dashboardGrowerModel.getFinancial());
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//                Date myDate = null;
//                String sMyDate="";
//                try {
//                    myDate = sdf.parse(dashboardGrowerModel.getLast_recording());
//                    sdf.applyPattern("EEEE, d MMM yyyy");
//                    sMyDate = sdf.format(myDate);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
                onTabTapped(0);
                loading.dismiss();

//                date.setText(sMyDate);
//                setupViewPager(viewPager);
            }

            @Override
            public void onFailure(Call<DashboardGrowerModel> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });



        //set default tab mulai dari 0
//        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        return view;
    }

    public void refresh(){
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }

    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                // Do something when first tab is tapped here
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_today();
                    setChart(value_chart);
                }
                catch (Exception e){
//                    value_chart = new DashboardGrowerModel.GrowerFinancialModel();
                }

//                if (value_chart.equals(null))
//                    value_chart = new ArrayList<>();

                break;
            case 1:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_1w();
                    setChart(value_chart);
                }
                catch (Exception e){}

                break;
            case 2:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_1m();
                    setChart(value_chart);
                }catch (Exception e){}

                break;
            case 3:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_3m();
                    setChart(value_chart);
                }catch (Exception e){}

                break;
            case 4:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_6m();
                    setChart(value_chart);
                }catch (Exception e){}

                break;
            case 5:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_ytd();
                    setChart(value_chart);
                }catch (Exception e){}

                break;
            case 6:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_1y();
                    setChart(value_chart);
                }catch (Exception e){}

                break;
            case 7:
                try {
                    value_chart = DashboardGrowerModel.getInstance().getFinancial().getM_2y();
                    setChart(value_chart);
                }
                catch (Exception e){}

                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    public void setChart(DashboardGrowerModel.GrowerFinancialModel value){
        BigDecimal inc = new BigDecimal(value.getIncome_total());
        BigDecimal exp = new BigDecimal(value.getExpense_total());
        BigDecimal status = inc.subtract(exp).abs();
        double price = Double.parseDouble(value.getPrice());
        DecimalFormat dFormat = new DecimalFormat("###,###,###,###,###.##");
        int comp = inc.compareTo(exp);
        if (comp == 0 || comp == 1){
            //maka profit
            finance_status.setTextColor(getResources().getColor(R.color.colorPrimary));
            finance_status.setText("Profit");
            finance_value.setTextColor(getResources().getColor(R.color.colorPrimary));
            currency.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        else{
            //maka loss
            finance_status.setTextColor(Color.RED);
            finance_status.setText("Loss");
            finance_value.setTextColor(Color.RED);
            currency.setTextColor(Color.RED);
        }
        finance_value.setText(String.valueOf(dFormat.format(status.setScale(3, RoundingMode.HALF_UP))));
        income.setText(String.valueOf(dFormat.format(inc.setScale(3, RoundingMode.HALF_UP))));
        expense.setText(String.valueOf(dFormat.format(exp.setScale(3, RoundingMode.HALF_UP))));
        product_price.setText(String.valueOf(dFormat.format(price)));

    }

//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.try_again) {
//            refresh();
//        }
//    }
}
