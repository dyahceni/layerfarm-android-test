package com.layerfarm.dashboard.DashboardGrower;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentFinancial;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentOverview;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentPerformance;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerMainActivity extends AppCompatActivity {
    ViewPager viewPager;
    TextView date;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_grower_activity);

//        // setting toolbar
        Toolbar toolbarTop = (Toolbar) findViewById(com.layerfarm.recording.R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(com.layerfarm.recording.R.id.title);
        mTitle.setText("Dashboard Grower");

        // setting view pager
//        viewPager = findViewById(R.id.viewPager);
        date = (TextView) findViewById(R.id.date);

//        getSupportActionBar().setTitle("Dashboard Grower");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_close_foreground);
//        getSupportActionBar().setLogo(R.drawable.close);

        // setting tabLayout
        TabLayout tabLayout = findViewById(R.id.tabLayout);
//        tabLayout.setupWithViewPager(viewPager);

//        setupViewPager(viewPager);
//        String module = "layerfarm_android";
//        String function_name = "layerfarm_android_dashboard_grower_data";
//        String[] args={};
//
//        Parameter parameter = new Parameter(module, function_name, args);
//
//        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//        String token2 = RetrofitData.getInstance().getToken2();
//        Call<DashboardGrowerModel> call = apiInterfaceJson.DashboardGrower(token2, parameter);
//
//        call.enqueue(new Callback<DashboardGrowerModel>() {
//            @Override
//            public void onResponse(Call<DashboardGrowerModel> call, Response<DashboardGrowerModel> response) {
//                DashboardGrowerModel dashboardGrowerModel = response.body();
//                DashboardGrowerModel.getInstance().setOverview(dashboardGrowerModel.getOverview());
//                DashboardGrowerModel.getInstance().setLast_recording(dashboardGrowerModel.getLast_recording());
//                DashboardGrowerModel.getInstance().setAvailable_mortality(dashboardGrowerModel.getAvailable_mortality());
//                DashboardGrowerModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
//                DashboardGrowerModel.getInstance().setFinancial(dashboardGrowerModel.getFinancial());
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//                Date myDate = null;
//                String sMyDate="";
//                try {
//                    myDate = sdf.parse(dashboardGrowerModel.getLast_recording());
//                    sdf.applyPattern("EEEE, d MMM yyyy");
//                    sMyDate = sdf.format(myDate);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//
//                date.setText(sMyDate);
//                setupViewPager(viewPager);
//            }
//
//            @Override
//            public void onFailure(Call<DashboardGrowerModel> call, Throwable t) {
//                String alert = t.getMessage();
//                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//            }
//        });

    }
    private void setupViewPager(ViewPager viewPager) {
        Adapter mainFragmentPagerAdapter = new Adapter(getSupportFragmentManager());
        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentOverview(), "Overview");
        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentPerformance(), "Performance");
        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentFinancial(), "Financial");
        viewPager.setAdapter(mainFragmentPagerAdapter);
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void close(View view){
        finish();
    }

}
