package com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerMainActivity;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.github.mikephil.charting.data.Entry;

public class DashboardLayerFragmentOverview extends Fragment {

    private  TextView tx_title;
    private  TextView textWeight;
    private  TextView textEgg;
    private  TextView textMor;
    private PieChart chart;
    private PieChart chart2;


    private Typeface tf;
    private Typeface tf2;

    private  double eggToday, eggLastDay, egg1Week, egg1Month, egg3Month, egg6Month, egg1Year, egg2Year, eggYtd;
    private  double weightToday, weightLastDay, weight1Week, weight1Month, weight3Month, weight6Month, weight1Year, weight2Year, weightYtd;
    private  int morToday, morLastDay, mor1Week, mor1Month, mor3Month, mor6Month, mor1Year, mor2Year, morYtd;
    private String last_recording_date;
    LinearLayout timeout, main_layout;

    private ArrayList type_name = new ArrayList();
    private  ArrayList mor_name = new ArrayList();

    private  ArrayList mor_value_today = new ArrayList();
    private  ArrayList mor_value_lastday = new ArrayList();
    private  ArrayList mor_value_week = new ArrayList();
    private  ArrayList mor_value_1month = new ArrayList();
    private  ArrayList mor_value_3month = new ArrayList();
    private  ArrayList mor_value_6month = new ArrayList();
    private  ArrayList mor_value_1year = new ArrayList();
    private  ArrayList mor_value_2year = new ArrayList();
    private  ArrayList mor_value_ytd = new ArrayList();

    private  ArrayList prod_value_today = new ArrayList();
    private  ArrayList prod_value_lastday = new ArrayList();
    private  ArrayList prod_value_week = new ArrayList();
    private  ArrayList prod_value_1month = new ArrayList();
    private  ArrayList prod_value_3month = new ArrayList();
    private  ArrayList prod_value_6month = new ArrayList();
    private  ArrayList prod_value_1year = new ArrayList();
    private  ArrayList prod_value_2year = new ArrayList();
    private  ArrayList prod_value_ytd = new ArrayList();

    private ArrayList<Double> eggSell = new ArrayList();
    private  ArrayList income = new ArrayList();
    private  ArrayList expense = new ArrayList();

    private ArrayList date_performance = new ArrayList<>();
    private ArrayList population = new ArrayList<>();
    private ArrayList egglaidegg = new ArrayList<>();
    private ArrayList egglaidweight = new ArrayList<>();
    private ArrayList fcr = new ArrayList<>();
    private ArrayList henday = new ArrayList<>();
    private ArrayList mortality = new ArrayList<>();
    ArrayList<Double> expenseValue = new ArrayList<Double>();

    ArrayList<String> qualityType = new ArrayList<>();
    ArrayList<String> mortalityType = new ArrayList<>();

    ProgressDialog loading;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_layer_fragment_overview, container, false);
        tx_title = (TextView) view.findViewById(R.id.tx_title);
        textWeight = (TextView) view.findViewById(R.id.tvweight);
        textEgg = (TextView) view.findViewById(R.id.tvegg);
        textMor = (TextView) view.findViewById(R.id.tvdeathbird);
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);
/*        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActivity().setContentView(R.layout.dashboard_layer_activity);*/
        getLayerFarmDashboard();
        final TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.tabs);
//       String lastRecording = ((DashboardLayerMainActivity) getActivity()).getLastRecording();

        //Chart Mortality
        chart = view.findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);
        chart.setDragDecelerationFrictionCoef(0.95f);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");
        chart.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf"));
        chart.setCenterText(generateCenterSpannableTextProd());
        chart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);
        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);
        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);
        chart.setDrawCenterText(true);
        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        int colorBlack = Color.parseColor("#000000");
        chart.setEntryLabelColor(colorBlack);
        chart.setDrawEntryLabels(false);
        chart.setDrawSliceText(false);
        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        //l.setEnabled(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);


        //Chart Production
        chart2 = view.findViewById(R.id.chart2);
        chart2.setUsePercentValues(true);
        chart2.getDescription().setEnabled(false);
        chart2.setExtraOffsets(5, 10, 5, 5);
        chart2.setDragDecelerationFrictionCoef(0.95f);
        tf2 = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");
        chart2.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf"));
        chart2.setCenterText(generateCenterSpannableTextMor());
        chart2.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        chart2.setDrawHoleEnabled(true);
        chart2.setHoleColor(Color.WHITE);
        chart2.setTransparentCircleColor(Color.WHITE);
        chart2.setTransparentCircleAlpha(110);
        chart2.setHoleRadius(58f);
        chart2.setTransparentCircleRadius(61f);
        chart2.setDrawCenterText(true);
        chart2.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart2.setRotationEnabled(true);
        chart2.setHighlightPerTapEnabled(true);
        chart2.setEntryLabelColor(colorBlack);
        chart2.setDrawEntryLabels(false);
        chart2.setDrawSliceText(false);
        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        chart2.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l2 = chart2.getLegend();
        l2.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l2.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l2.setOrientation(Legend.LegendOrientation.VERTICAL);
        l2.setDrawInside(false);
        //l2.setEnabled(false);
        l2.setXEntrySpace(7f);
        l2.setYEntrySpace(0f);
        l2.setYOffset(0f);

        //onTabTapped(0);


        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                onTabTapped(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        return view;
    }

    public void getLayerFarmDashboard(){
        loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_layer_overview";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LayerDataDashboard> call = apiInterfaceJson.getDashboardLayerfarm(token2, parameter);

        call.enqueue(new Callback<LayerDataDashboard>() {
            @Override
            public void onResponse(Call<LayerDataDashboard> call, Response<LayerDataDashboard> response) {
                //Log.i("Responsestring", response.body().toString());
                //Toast.makeText()

                if (response != null && response.isSuccessful()) {
                    try {
                        LayerDataDashboard layerdata = response.body();
                        last_recording_date = layerdata.getLastRecording();
                        last_recording_date = layerdata.getLastRecording();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        String sMyDate="";
                        Date myDate = null;
                        try {
                            myDate = sdf.parse(last_recording_date);
                            sdf.applyPattern("EEEE, d MMM yyyy");
                            sMyDate = sdf.format(myDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String null_value = "0";
                        //Get Production Array Data
                        ArrayList<LayerDataDashboard.PToday> type = layerdata.getPToday();
                        if (type != null && type.size() > 0) {
                            for (int i = 0; i < type.size(); i++) {
                                type_name.add(type.get(i).getType());
                                int myPtoday;
                                double myWtoday;

                                if (type.get(i).getEgg() != null && !type.get(i).getEgg().isEmpty()) {
                                    myPtoday = Integer.parseInt(type.get(i).getEgg());
                                } else {
                                    myPtoday = 0;
                                }

                                if (type.get(i).getWeight() != null && !type.get(i).getWeight().isEmpty()) {
                                    myWtoday = Float.parseFloat(type.get(i).getWeight());
                                } else {
                                    myWtoday = 0.00;
                                }

                                prod_value_today.add(myPtoday);

                                eggToday = eggToday + myPtoday;
                                weightToday = weightToday + myWtoday;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1w> b = layerdata.getP1w();
                        if (b != null && b.size() > 0) {
                            for (int i = 0; i < b.size(); i++) {

                                int myP1w;
                                double myW1w;

                                if (b.get(i).getEgg() != null && !b.get(i).getEgg().isEmpty()) {
                                    myP1w = Integer.parseInt(b.get(i).getEgg());
                                } else {
                                    myP1w = 0;
                                }

                                if (b.get(i).getWeight() != null && !b.get(i).getWeight().isEmpty()) {
                                    myW1w = Double.parseDouble(b.get(i).getWeight());
                                } else {
                                    myW1w = 0.00;
                                }

                                prod_value_week.add(myP1w);
                                egg1Week = egg1Week + myP1w;
                                weight1Week = weight1Week +myW1w;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1m> c = layerdata.getP1m();
                        if (c != null && c.size() > 0) {
                            for (int i = 0; i < c.size(); i++) {

                                int myP1m;
                                double myW1m;

                                if (c.get(i).getEgg() != null && !c.get(i).getEgg().isEmpty()) {
                                    myP1m = Integer.parseInt(c.get(i).getEgg());
                                } else {
                                    myP1m = 0;
                                }

                                if (c.get(i).getWeight() != null && !c.get(i).getWeight().isEmpty()) {
                                    myW1m = Double.parseDouble(c.get(i).getWeight());
                                } else {
                                    myW1m = 0.00;
                                }

                                prod_value_1month.add(myP1m);
                                egg1Month = egg1Month + myP1m;
                                weight1Month = weight1Month + myW1m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P3m> d = layerdata.getP3m();
                        if (d != null && d.size() > 0) {
                            for (int i = 0; i < type.size(); i++) {

                                int myP3m;
                                double myW3m;

                                if (d.get(i).getEgg() != null && !d.get(i).getEgg().isEmpty()) {
                                    myP3m = Integer.parseInt(d.get(i).getEgg());
                                } else {
                                    myP3m = 0;
                                }

                                if (d.get(i).getWeight() != null && !d.get(i).getWeight().isEmpty()) {
                                    myW3m = Double.parseDouble(d.get(i).getWeight());
                                } else {
                                    myW3m = 0.00;
                                }

                                prod_value_3month.add(myP3m);
                                egg3Month = egg3Month + myP3m;
                                weight3Month = weight3Month + myW3m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P6m> e = layerdata.getP6m();
                        if (e != null && e.size() > 0) {
                            for (int i = 0; i < e.size(); i++) {

                                int myP6m;
                                double myW6m;

                                if (e.get(i).getEgg() != null && !e.get(i).getEgg().isEmpty()) {
                                    myP6m = Integer.parseInt(e.get(i).getEgg());
                                } else {
                                    myP6m = 0;
                                }

                                if (e.get(i).getWeight() != null && !e.get(i).getWeight().isEmpty()) {
                                    myW6m = Double.parseDouble(e.get(i).getWeight());
                                } else {
                                    myW6m = 0.00;
                                }

                                prod_value_6month.add(myP6m);
                                egg6Month = egg6Month + myP6m;
                                weight6Month = weight6Month + myW6m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1y> f = layerdata.getP1y();
                        if (f != null && f.size() > 0) {
                            for (int i = 0; i < f.size(); i++) {

                                int myP1y;
                                double myW1y;

                                if (f.get(i).getEgg() != null && !f.get(i).getEgg().isEmpty()) {
                                    myP1y = Integer.parseInt(f.get(i).getEgg());
                                } else {
                                    myP1y = 0;
                                }

                                if (f.get(i).getWeight() != null && !f.get(i).getWeight().isEmpty()) {
                                    myW1y = Double.parseDouble(f.get(i).getWeight());
                                } else {
                                    myW1y = 0.00;
                                }

                                prod_value_1year.add(myP1y);
                                egg1Year = egg1Year + myP1y;
                                weight1Year = weight1Year + myW1y;
                            }
                        }

                        ArrayList<LayerDataDashboard.P2y> g = layerdata.getP2y();
                        if (g != null && g.size() > 0) {
                            for (int i = 0; i < g.size(); i++) {

                                int myP2y;
                                double myW2y;

                                if (f.get(i).getEgg() != null && !f.get(i).getEgg().isEmpty()) {
                                    myP2y = Integer.parseInt(f.get(i).getEgg());
                                } else {
                                    myP2y = 0;
                                }

                                if (f.get(i).getWeight() != null && !f.get(i).getWeight().isEmpty()) {
                                    myW2y = Double.parseDouble(f.get(i).getWeight());
                                } else {
                                    myW2y = 0.00;
                                }

                                prod_value_2year.add(myP2y);
                                egg2Year = egg2Year + myP2y;
                                weight2Year = weight2Year + myW2y;
                            }
                        }

                        ArrayList<LayerDataDashboard.PYtd> h = layerdata.getPYtd();
                        if (h != null && h.size() > 0) {
                            for (int i = 0; i < h.size(); i++) {

                                int myPYtd;
                                double myWPYtd;

                                if (h.get(i).getEgg() != null && !h.get(i).getEgg().isEmpty()) {
                                    myPYtd = Integer.parseInt(h.get(i).getEgg());
                                } else {
                                    myPYtd = 0;
                                }

                                if (h.get(i).getWeight() != null && !h.get(i).getWeight().isEmpty()) {
                                    myWPYtd = Double.parseDouble(h.get(i).getWeight());
                                } else {
                                    myWPYtd = 0.00;
                                }

                                prod_value_ytd.add(myPYtd);
                                eggYtd = eggYtd + myPYtd;
                                weightYtd = weightYtd + myWPYtd;
                            }
                        }

                        //Get Mortality Array Data
                        ArrayList<LayerDataDashboard.MToday> mv_today = layerdata.getMToday();
                        if (mv_today != null && mv_today.size() > 0) {
                            for (int i = 0; i < mv_today.size(); i++) {
                                mor_name.add(mv_today.get(i).getMortalityType());

                                int MToday;

                                if (mv_today.get(i).getMortalityValue() != null && !mv_today.get(i).getMortalityValue().isEmpty()) {
                                    MToday = Integer.parseInt(mv_today.get(i).getMortalityValue());
                                } else {
                                    MToday = 0;
                                }


                                mor_value_today.add(MToday);
                                morToday = morToday + MToday;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1w> mv_week = layerdata.getM1w();
                        if (mv_week != null && mv_week.size() > 0) {
                            for (int i = 0; i < mv_week.size(); i++) {

                                int M1w;

                                if (mv_week.get(i).getMortalityValue() != null && !mv_week.get(i).getMortalityValue().isEmpty()) {
                                    M1w = Integer.parseInt(mv_week.get(i).getMortalityValue());
                                } else {
                                    M1w = 0;
                                }

                                mor_value_week.add(M1w);
                                mor1Week = mor1Week + M1w;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1m> mv_1month = layerdata.getM1m();
                        if (mv_1month != null && mv_1month.size() > 0) {
                            for (int i = 0; i < mv_1month.size(); i++) {

                                int M1m;

                                if (mv_1month.get(i).getMortalityValue() != null && !mv_1month.get(i).getMortalityValue().isEmpty()) {
                                    M1m = Integer.parseInt(mv_1month.get(i).getMortalityValue());
                                } else {
                                    M1m = 0;
                                }

                                mor_value_1month.add(M1m);
                                mor1Month = mor1Month + M1m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M3m> mv_3month = layerdata.getM3m();
                        if (mv_3month != null && mv_3month.size() > 0) {
                            for (int i = 0; i < mv_3month.size(); i++) {

                                int M3m;

                                if (mv_3month.get(i).getMortalityValue() != null && !mv_3month.get(i).getMortalityValue().isEmpty()) {
                                    M3m = Integer.parseInt(mv_3month.get(i).getMortalityValue());
                                } else {
                                    M3m = 0;
                                }

                                mor_value_3month.add(M3m);
                                mor3Month = mor3Month + M3m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M6m> mv_6month = layerdata.getM6m();
                        if (mv_6month != null && mv_6month.size() > 0) {
                            for (int i = 0; i < mv_6month.size(); i++) {

                                int M6m;

                                if (mv_6month.get(i).getMortalityValue() != null && !mv_6month.get(i).getMortalityValue().isEmpty()) {
                                    M6m = Integer.parseInt(mv_6month.get(i).getMortalityValue());
                                } else {
                                    M6m = 0;
                                }

                                mor_value_6month.add(M6m);
                                mor6Month = mor6Month + M6m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1y> mv_1year = layerdata.getM1y();
                        if (mv_1year != null && mv_1year.size() > 0) {
                            for (int i = 0; i < mv_1year.size(); i++) {

                                int M1y;

                                if (mv_1year.get(i).getMortalityValue() != null && !mv_1year.get(i).getMortalityValue().isEmpty()) {
                                    M1y = Integer.parseInt(mv_1year.get(i).getMortalityValue());
                                } else {
                                    M1y = 0;
                                }

                                mor_value_1year.add(M1y);
                                mor1Year = mor1Year + M1y;
                            }
                        }

                        ArrayList<LayerDataDashboard.M2y> mv_2year = layerdata.getM2y();
                        if (mv_2year != null && mv_2year.size() > 0) {
                            for (int i = 0; i < mv_2year.size(); i++) {

                                int M2y;

                                if (mv_2year.get(i).getMortalityValue() != null && !mv_2year.get(i).getMortalityValue().isEmpty()) {
                                    M2y = Integer.parseInt(mv_2year.get(i).getMortalityValue());
                                } else {
                                    M2y = 0;
                                }

                                mor_value_2year.add(M2y);
                                mor2Year = mor2Year + M2y;
                            }
                        }

                        ArrayList<LayerDataDashboard.MYtd> mv_ytd = layerdata.getMYtd();
                        if (mv_ytd != null && mv_ytd.size() > 0) {
                            for (int i = 0; i < mv_ytd.size(); i++) {

                                int MYtd;

                                if (mv_ytd.get(i).getMortalityValue() != null && !mv_ytd.get(i).getMortalityValue().isEmpty()) {
                                    MYtd = Integer.parseInt(mv_ytd.get(i).getMortalityValue());
                                } else {
                                    MYtd = 0;
                                }

                                mor_value_ytd.add(MYtd);
                                morYtd = morYtd + MYtd;
                            }
                        }

                        qualityType = type_name;
                        mortalityType = mor_name;
                        onTabTapped(0);
                        loading.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                } else {
                    Log.i("onEmptyResponse", "Returned empty response");
//                    Toast.makeText(this(),"Nothing returned",Toast.LENGTH_LONG).show();
//                    AskOption();
                    loading.dismiss();
                }



            }

            @Override
            public void onFailure(Call<LayerDataDashboard> call, Throwable t) {
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });


    }

    private void setDataToday(ArrayList<Integer> mor_value_today, ArrayList<Integer> prod_value_today) {

//        ArrayList<String> qualityType = type_name;
//        ArrayList<String> mortalityType = mor_name;

        Integer[] IntegerArrayMtoday;
        ArrayList<Integer> nummvtoday = mor_value_today;
        IntegerArrayMtoday = nummvtoday.toArray(new Integer[nummvtoday.size()]);
        //Log.i("IntegerArray", IntegerArray.toString());

        Integer[] IntegerArrayPtoday;
        ArrayList<Integer> numpvtoday = prod_value_today;
        IntegerArrayPtoday = numpvtoday.toArray(new Integer[numpvtoday.size()]);


        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<PieEntry> entries2 = new ArrayList<>();


        if (qualityType != null && qualityType.size() > 0) {
            for (int i = 0; i < qualityType.size(); i++) {
                entries.add(new PieEntry((float) IntegerArrayPtoday[i], qualityType.get(i)));
            }

        PieDataSet dataSet = new PieDataSet(entries, "Production");

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);


        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setUsingSliceColorAsValueLineColor(true);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(tf);


        chart.setData(data);
        chart.highlightValues(null);
        chart.invalidate();
        }

        if (mortalityType != null && mortalityType.size() > 0) {
            for (int i = 0; i < mortalityType.size(); i++) {
                entries2.add(new PieEntry((float)IntegerArrayMtoday[i], mortalityType.get(i)));
                Log.i("IntegerArray", IntegerArrayMtoday[i].toString());
            }

            PieDataSet dataSet2 = new PieDataSet(entries2, "Mortality");

            dataSet2.setSliceSpace(3f);
            dataSet2.setSelectionShift(5f);

            ArrayList<Integer> colors = new ArrayList<>();

            for (int c : ColorTemplate.VORDIPLOM_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.JOYFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.LIBERTY_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);

            colors.add(ColorTemplate.getHoloBlue());

            dataSet2.setColors(colors);
            //dataSet.setSelectionShift(0f);


            dataSet2.setValueLinePart1OffsetPercentage(80.f);
            dataSet2.setValueLinePart1Length(0.2f);
            dataSet2.setValueLinePart2Length(0.4f);
            //dataSet.setUsingSliceColorAsValueLineColor(true);

            //dataSet2.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
            dataSet2.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

            PieData data2 = new PieData(dataSet2);
            data2.setValueFormatter(new PercentFormatter());
            data2.setValueTextSize(10f);
            data2.setValueTextColor(Color.BLACK);
            data2.setValueTypeface(tf);

            chart2.setData(data2);
            chart2.highlightValues(null);
            chart2.invalidate();
        }
    }

    private void onTabTapped(int position) {
        DecimalFormat dFormat = new DecimalFormat("####,###,###.##");
        switch (position) {
            case 0:
                // Do something when first tab is tapped here
                tx_title.setText("1D");
                textWeight.setText(dFormat.format(weightToday) + " kg");
                textEgg.setText(dFormat.format(eggToday)+ " eggs");
                textMor.setText(dFormat.format(morToday)+ " birds");
                setDataToday(mor_value_today, prod_value_today);
                break;
            case 1:
                tx_title.setText("1W");
                textWeight.setText(dFormat.format(weight1Week) + " kg");
                textEgg.setText(dFormat.format(egg1Week)+ " eggs");
                textMor.setText(dFormat.format(mor1Week)+ " birds");
                setDataToday(mor_value_week, prod_value_week);
                break;
            case 2:
                tx_title.setText("1M");
                textWeight.setText(dFormat.format(weight1Month) + " kg");
                textEgg.setText(dFormat.format(egg1Month)+ " eggs");
                textMor.setText(dFormat.format(mor1Month)+ " birds");
                setDataToday(mor_value_1month, prod_value_1month);
                break;
            case 3:
                tx_title.setText("3M");
                textWeight.setText(dFormat.format(weight3Month) + " kg");
                textEgg.setText(dFormat.format(egg3Month)+ " eggs");
                textMor.setText(dFormat.format(mor3Month)+ " birds");
                setDataToday(mor_value_3month, prod_value_3month);
                break;
            case 4:
                tx_title.setText("6M");
                textWeight.setText(dFormat.format(weight6Month) + " kg");
                textEgg.setText(dFormat.format(egg6Month)+ " eggs");
                textMor.setText(dFormat.format(mor6Month)+ " birds");
                setDataToday(mor_value_6month, prod_value_6month);
                break;
            case 5:
                tx_title.setText("YTD");
                textWeight.setText(dFormat.format(weightYtd) + " kg");
                textEgg.setText(dFormat.format(eggYtd)+ " eggs");
                textMor.setText(dFormat.format(morYtd)+ " birds");
                setDataToday(mor_value_ytd, prod_value_ytd);
                break;
            case 6:

//                double x = ((DashboardLayerMainActivity) getActivity()).getEgg1y();
                tx_title.setText("1Y");
                textWeight.setText(dFormat.format(weight1Year) + " kg");
                textEgg.setText(dFormat.format(egg1Year)+ " eggs");
                textMor.setText(dFormat.format(mor1Year)+ " birds");
                setDataToday(mor_value_1year, prod_value_1year);
                break;
            case 7:
                tx_title.setText("2Y");
                textWeight.setText(dFormat.format(weight2Year) + " kg");
                textEgg.setText(dFormat.format(egg2Year)+ " eggs");
                textMor.setText(dFormat.format(mor2Year)+ " birds");
                setDataToday(mor_value_2year, prod_value_2year);
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }

    private SpannableString generateCenterSpannableTextProd() {
        SpannableString s = new SpannableString("Production Chart");
        s.setSpan(new RelativeSizeSpan(1.5f), 0, 16, 0);
        return s;
    }

    private SpannableString generateCenterSpannableTextMor() {

        SpannableString s = new SpannableString("Mortality Chart");
        s.setSpan(new RelativeSizeSpan(1.5f), 0, 15, 0);
        return s;
    }

    private String NumFormat(Double number) {
        double compare;
        String st_result;
        compare = 10000000;
        if (number>compare)
        {
            DecimalFormat formatter = new DecimalFormat("0.###E0");
            st_result = formatter.format(number);
        }
        else
        {
            DecimalFormat simpler = new DecimalFormat("###");
            st_result = simpler.format(number);
        }
        return st_result;
    }
}
