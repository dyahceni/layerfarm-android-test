package com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerMainActivity;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerMainActivity;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.security.acl.LastOwnerException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardLayerFragmentPerformance extends Fragment implements OnChartGestureListener, OnChartValueSelectedListener {

    LineChart lineChartPopulation;
    LineChart lineChartEggLainEgg;
    LineChart lineChartEggLaidWeight;
    LineChart lineChartFcr;
    LineChart lineChartHenday;
    LineChart lineChartMortality;
    TextView tvpopulation_value, tvegg_laid_weight_value, tvegg_laid_value, tvfcr_value, tvhen_day_value, tvmortality_value;
    TextView tvpopulation_percentage,tvegg_laid_weight_percentage, tvegg_laid_percentage, tvfcr_percentage, tvhen_day_percentage, tvmortality_percentage;
    ImageView imagePop, imageegg_laid_weight, imageegg_laid, imagefcr, imagehen_day, imagemortality;
    LinearLayout timeout, main_layout;

    float resultpop, percentagepop;
    float resulteegg_laid_weight, percentageeegg_laid_weight;
    float resultegg_laid, percentageegg_laid;
    float resultfcr, percentagefcr;
    float resulthen_day, percentagehen_day;
    float resultmortality, percentagemortality;

    ArrayList<Integer> population = new ArrayList<>();
    ArrayList<Integer> eggLaidEgg = new ArrayList<>();
    ArrayList<Float> eggLaidweight = new ArrayList<>();
    ArrayList<Float> fcr = new ArrayList<>();
    ArrayList<Float> henday = new ArrayList<>();
    ArrayList<Float> mor = new ArrayList<>();

    ProgressDialog loading;

    int index = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_layer_fragment_performance, container, false);
        tvpopulation_value = (TextView) view.findViewById(R.id.population_value);
        tvegg_laid_weight_value = (TextView) view.findViewById(R.id.egg_laid_weight_value);
        tvegg_laid_value = (TextView) view.findViewById(R.id.egg_laid_value);
        tvfcr_value= (TextView) view.findViewById(R.id.fcr_value);
        tvhen_day_value = (TextView) view.findViewById(R.id.hen_day_value);
        tvmortality_value = (TextView) view.findViewById(R.id.mortality_value);

        tvpopulation_percentage = (TextView) view.findViewById(R.id.population_percentage);
        tvegg_laid_weight_percentage = (TextView) view.findViewById(R.id.egg_laid_weight_percentage);
        tvegg_laid_percentage = (TextView) view.findViewById(R.id.egg_laid_percentage);
        tvfcr_percentage = (TextView) view.findViewById(R.id.fcr_percentage);
        tvhen_day_percentage = (TextView) view.findViewById(R.id.hen_day_percentage);
        tvmortality_percentage = (TextView) view.findViewById(R.id.mortality_percentage);

        lineChartPopulation = (LineChart) view.findViewById(R.id.chart_population);
        lineChartEggLainEgg = (LineChart) view.findViewById(R.id.chart_egg_laid);
        lineChartEggLaidWeight = (LineChart) view.findViewById(R.id.chart_weight_egg_laid);
        lineChartFcr = (LineChart) view.findViewById(R.id.chart_fcr);
        lineChartHenday = (LineChart) view.findViewById(R.id.chart_hen_day);
        lineChartMortality = (LineChart) view.findViewById(R.id.chart_mortality);

        imagePop = (ImageView) view.findViewById(R.id.image_population);
        imageegg_laid_weight = (ImageView) view.findViewById(R.id.image_egg_weight);
        imageegg_laid= (ImageView) view.findViewById(R.id.image_egg);
        imagefcr= (ImageView) view.findViewById(R.id.image_fcr);
        imagehen_day= (ImageView) view.findViewById(R.id.image_henday);
        imagemortality= (ImageView) view.findViewById(R.id.image_mortality);
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);

        final TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.tabs);

        getPerformance();
        LinearLayout population = (LinearLayout) view.findViewById(R.id.population_linar_layout);
        population.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailPopulationActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout mortality = (LinearLayout) view.findViewById(R.id.linear_mortality);
        mortality.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailMortalityActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout eggWeight = (LinearLayout) view.findViewById(R.id.egg_weight_linear);
        eggWeight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailEggWeightActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout eggLaid = (LinearLayout) view.findViewById(R.id.egg_laid_linear);
        eggLaid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailEggNumberActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout henday = (LinearLayout) view.findViewById(R.id.henday_linear);
        henday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailHendayActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout fcr = (LinearLayout) view.findViewById(R.id.fcr_linear);
        fcr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), DashboardLayerDetailFcrActivity.class);
                intent.putExtra("index", index);
                //Intent intent = new Intent(this, BodyWeightMainActivity.class);
                startActivity(intent);
            }
        });

        Legend l = lineChartPopulation.getLegend();
        l.setForm(Legend.LegendForm.LINE);

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        return view;
    }


    private void getPerformance(){
        loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_layer_performance";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LayerDataDashboard> call = apiInterfaceJson.getDashboardLayerfarm(token2, parameter);

        call.enqueue(new Callback<LayerDataDashboard>() {
            @Override
            public void onResponse(Call<LayerDataDashboard> call, Response<LayerDataDashboard> response) {

                if (response != null && response.isSuccessful()) {
                    try {
                        LayerDataDashboard layerdata = response.body();
                        Log.d("zzz","layerdata instance = "+LayerDataDashboard.getInstance());
                        LayerDataDashboard.getInstance().setPopulation(layerdata.getPopulation());
                        LayerDataDashboard.getInstance().setEggLaidEgg(layerdata.getEggLaidEgg());
                        LayerDataDashboard.getInstance().setEggLaidWeight(layerdata.getEggLaidWeight());
                        LayerDataDashboard.getInstance().setFcr(layerdata.getFcr());
                        LayerDataDashboard.getInstance().setHenday(layerdata.getHenday());
                        LayerDataDashboard.getInstance().setMortality(layerdata.getMortality());
                        onTabTapped(0);
                        loading.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                        loading.dismiss();
                    }
                }
            }
            @Override
            public void onFailure(Call<LayerDataDashboard> call, Throwable t) {
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    private void onTabTapped(int position) {

        if (LayerDataDashboard.getInstance().getPopulation() != null && LayerDataDashboard.getInstance().getPopulation().size() > 0) {
            for (int i = 0; i < LayerDataDashboard.getInstance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(LayerDataDashboard.getInstance().getPopulation().get(i)));
            }
        }
        if (LayerDataDashboard.getInstance().getEggLaidEgg() != null && LayerDataDashboard.getInstance().getEggLaidEgg().size() > 0){
            for (int i =0; i< LayerDataDashboard.getInstance().getEggLaidEgg().size(); i++){
                eggLaidEgg.add(Integer.parseInt(LayerDataDashboard.getInstance().getEggLaidEgg().get(i)));
            }
        }
        if (LayerDataDashboard.getInstance().getEggLaidWeight() != null && LayerDataDashboard.getInstance().getEggLaidWeight().size() > 0){
            for (int i =0; i< LayerDataDashboard.getInstance().getEggLaidWeight().size(); i++){
                eggLaidweight.add(Float.parseFloat(LayerDataDashboard.getInstance().getEggLaidWeight().get(i)));
            }
        }
        if (LayerDataDashboard.getInstance().getFcr() != null && LayerDataDashboard.getInstance().getFcr().size() > 0) {
            for (int i = 0; i < LayerDataDashboard.getInstance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(LayerDataDashboard.getInstance().getFcr().get(i)));
            }
        }
        if (LayerDataDashboard.getInstance().getMortality() != null && LayerDataDashboard.getInstance().getMortality().size() > 0) {
            for (int i = 0; i < LayerDataDashboard.getInstance().getMortality().size(); i++) {
                mor.add(Float.parseFloat(LayerDataDashboard.getInstance().getMortality().get(i)));
            }
        }
        if (LayerDataDashboard.getInstance().getHenday() != null && LayerDataDashboard.getInstance().getHenday().size() > 0) {
            for (int i = 0; i < LayerDataDashboard.getInstance().getHenday().size(); i++) {
                henday.add(Float.parseFloat(LayerDataDashboard.getInstance().getHenday().get(i)));
            }
        }

        DecimalFormat df = new DecimalFormat("####,###,###.##");
        switch (position) {
            case 0:
                index = 0;
                setChartPopulation(1);
                setChartMortality(1);
                setChartFcr(1);
                setChartEggLaid(1);
                setChartEggWeight(1);
                setChartHenday(1);
                break;
            case 1:
                index = 1;
                setChartPopulation(2);
                setChartMortality(2);
                setChartFcr(2);
                setChartEggLaid(2);
                setChartEggWeight(2);
                setChartHenday(2);
                break;
            case 2:
                index = 2;
                setChartPopulation(3);
                setChartMortality(3);
                setChartFcr(3);
                setChartEggLaid(3);
                setChartEggWeight(3);
                setChartHenday(3);
                break;
            case 3:
                index = 3;
                setChartPopulation(4);
                setChartMortality(4);
                setChartFcr(4);
                setChartEggLaid(4);
                setChartEggWeight(4);
                setChartHenday(4);
                break;
            case 4:
                index = 4;
                setChartPopulation(5);
                setChartMortality(5);
                setChartFcr(5);
                setChartEggLaid(5);
                setChartEggWeight(5);
                setChartHenday(5);
                break;
            case 5:
                index = 5;
                setChartPopulation(6);
                setChartMortality(6);
                setChartFcr(6);
                setChartEggLaid(6);
                setChartEggWeight(6);
                setChartHenday(6);
                break;
            case 6:
                index = 6;
                setChartPopulation(7);
                setChartMortality(7);
                setChartFcr(7);
                setChartEggLaid(7);
                setChartEggWeight(7);
                setChartHenday(7);
                break;
            case 7:
                index = 7;
                setChartPopulation(8);
                setChartMortality(8);
                setChartFcr(8);
                setChartEggLaid(8);
                setChartEggWeight(8);
                setChartHenday(8);
                break;
            default:
                index = 8;
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        //Log.i("Gesture", "START, x: "+ me.getX() + ",y: "+me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        ////Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            // or highlightTouch(null) for callback to onNothingSelected(...)
            lineChartPopulation.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: "
                + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + lineChartPopulation.getLowestVisibleX()
                + ", high: " + lineChartPopulation.getHighestVisibleX());

        Log.i("MIN MAX", "xmin: " + lineChartPopulation.getXChartMin()
                + ", xmax: " + lineChartPopulation.getXChartMax()
                + ", ymin: " + lineChartPopulation.getYChartMin()
                + ", ymax: " + lineChartPopulation.getYChartMax());
    }


    @Override
    public void onNothingSelected() {
        //Log.i("Nothing selected", "Nothing selected.");
    }

    private ArrayList<String> setXAxisValues(){

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < 20; i++) {
            i = i + 1;
            xVals.add(String.valueOf(i));
        }

        return xVals;
    }


    public void setChartPopulation(int index){
//        if (population.size() > 0)
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        int diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try {
            set2.add(new Entry(i, population.get(range)));
            population_qty += population.get(range);
            if (i == 0){
                diff = population.get(range);
            }
            else
                diff = population.get(range) - population.get(range+1);
//            }
//            catch (Exception e){
//                Log.d("zzz","null performance");
//            }

//            Log
//            Log.d("zzz","range "+population.get(range));
//            Log.d("zzz","range +1"+population.get(range+1));
            Log.d("zzz","difff: "+diff);
            range --;
        }
        if (population.get(a) != null) {
            tvpopulation_value.setText(String.valueOf(df.format(population.get(a))));
        }
        if (diff < 0){
            imagePop.setImageResource(R.drawable.ic_down);
        }
        else
            imagePop.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" populasi: "+population.get(a));
//        try{
        int val = population.get(a);
        float persen =0;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvpopulation_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}



        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartPopulation.setData(lineData);
        lineChartPopulation.setDrawGridBackground(false);
        lineChartPopulation.getAxisLeft().setEnabled(false);
        lineChartPopulation.getAxisRight().setEnabled(false);
        lineChartPopulation.setNoDataText("There is no Data");
        lineChartPopulation.getXAxis().setEnabled(false);
        lineChartPopulation.getDescription().setEnabled(false);
        lineChartPopulation.animateX(2000);
        lineChartPopulation.invalidate();
    }
    public void setChartMortality(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try {
            set2.add(new Entry(i, mor.get(range)));
            population_qty += mor.get(range);
            if (i == 0){
                diff = mor.get(range);
            }
            else
                diff = mor.get(range) - mor.get(range+1);
//            }catch (Exception e){}

            Log.d("zzz","difff: "+diff);
            range --;
        }
        if (mor.get(a) != null){
            tvmortality_value.setText(String.valueOf(df.format(mor.get(a))));
        }
        if (diff < 0){
            imagemortality.setImageResource(R.drawable.ic_down);
//            diff = Math.abs(diff);
        }
        else
            imagemortality.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" mortality: "+mor.get(a));
//        try {
        float val = mor.get(a);
        float persen;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvmortality_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Mortality") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartMortality.setData(lineData);
        lineChartMortality.setDrawGridBackground(false);
        lineChartMortality.getAxisLeft().setEnabled(false);
        lineChartMortality.getAxisRight().setEnabled(false);
        lineChartMortality.setNoDataText("There is no Data");
        lineChartMortality.getXAxis().setEnabled(false);
        lineChartMortality.getDescription().setEnabled(false);
        lineChartMortality.animateX(2000);
        lineChartMortality.invalidate();
    }
    public void setChartFcr(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try{
            set2.add(new Entry(i, fcr.get(range)));
            population_qty += fcr.get(range);
            if (i == 0){
                diff = fcr.get(range);
            }
            else
                diff = fcr.get(range) - fcr.get(range+1);
            Log.d("zzz","difff: "+diff);
//            }catch (Exception e){
//            }

            range --;
        }
        if (fcr.get(a) != null){
            tvfcr_value.setText(String.valueOf(df.format(fcr.get(a))));
        }
        if (diff < 0){
            imagefcr.setImageResource(R.drawable.ic_down);
        }
        else
            imagefcr.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" fcr: "+fcr.get(a));
//        try {
        float val = fcr.get(a);
        float persen =0;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvfcr_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartFcr.setData(lineData);
        lineChartFcr.setDrawGridBackground(false);
        lineChartFcr.getAxisLeft().setEnabled(false);
        lineChartFcr.getAxisRight().setEnabled(false);
        lineChartFcr.setNoDataText("There is no Data");
        lineChartFcr.getXAxis().setEnabled(false);
        lineChartFcr.getDescription().setEnabled(false);
        lineChartFcr.animateX(2000);
        lineChartFcr.invalidate();
    }
    public void setChartEggLaid(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try{
            set2.add(new Entry(i, eggLaidEgg.get(range)));
            population_qty += eggLaidEgg.get(range);
            if (i == 0){
                diff = eggLaidEgg.get(range);
            }
            else
                diff = eggLaidEgg.get(range) - eggLaidEgg.get(range+1);
            Log.d("zzz","difff: "+diff);
//            }catch (Exception e){
//            }

            range --;
        }
        if (eggLaidEgg.get(a) != null){
            tvegg_laid_value.setText(String.valueOf(df.format(eggLaidEgg.get(a))));
        }
        if (diff < 0){
            imageegg_laid.setImageResource(R.drawable.ic_down);
        }
        else
            imageegg_laid.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" egglaidegg: "+eggLaidEgg.get(a));
//        try {
        float val = eggLaidEgg.get(a);
        float persen =0;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvegg_laid_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartEggLainEgg.setData(lineData);
        lineChartEggLainEgg.setDrawGridBackground(false);
        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
        lineChartEggLainEgg.getAxisRight().setEnabled(false);
        lineChartEggLainEgg.setNoDataText("There is no Data");
        lineChartEggLainEgg.getXAxis().setEnabled(false);
        lineChartEggLainEgg.getDescription().setEnabled(false);
        lineChartEggLainEgg.animateX(2000);
        lineChartEggLainEgg.invalidate();
    }
    public void setChartEggWeight(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try{
            set2.add(new Entry(i, eggLaidweight.get(range)));
            population_qty += eggLaidweight.get(range);
            if (i == 0){
                diff = eggLaidweight.get(range);
            }
            else
                diff = eggLaidweight.get(range) - eggLaidweight.get(range+1);
            Log.d("zzz","difff: "+diff);
//            }catch (Exception e){
//            }

            range --;
        }
        if (eggLaidweight.get(a) != null){
            tvegg_laid_weight_value.setText(String.valueOf(df.format(eggLaidweight.get(a))));
        }
        if (diff < 0){
            imageegg_laid_weight.setImageResource(R.drawable.ic_down);
        }
        else
            imageegg_laid_weight.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" populasi: "+eggLaidweight.get(a));
//        try {
        float val = eggLaidweight.get(a);
        float persen =0;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvegg_laid_weight_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartEggLaidWeight.setData(lineData);
        lineChartEggLaidWeight.setDrawGridBackground(false);
        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
        lineChartEggLaidWeight.setNoDataText("There is no Data");
        lineChartEggLaidWeight.getXAxis().setEnabled(false);
        lineChartEggLaidWeight.getDescription().setEnabled(false);
        lineChartEggLaidWeight.animateX(2000);
        lineChartEggLaidWeight.invalidate();
    }
    public void setChartHenday(int index){
        List<Entry> set2 = new ArrayList<Entry>() ;
        DecimalFormat df = new DecimalFormat("#,###,###.##");
        //population
        int a = Math.abs(1- index);
        int range = a + 10;
        int pop = range;
        int population_qty = 0;
        float diff = 0;
        for (int i = 0 ; i <= 10; i++) {
//            try{
            set2.add(new Entry(i, henday.get(range)));
            population_qty += henday.get(range);
            if (i == 0){
                diff = henday.get(range);
            }
            else
                diff = henday.get(range) - henday.get(range+1);
            Log.d("zzz","difff: "+diff);
//            }catch (Exception e){
//            }

            range --;
        }
        if (henday.get(a) != null){
            tvhen_day_value.setText(String.valueOf(df.format(henday.get(a))));
        }
        if (diff < 0){
            imagehen_day.setImageResource(R.drawable.ic_down);
        }
        else
            imagehen_day.setImageResource(R.drawable.ic_up);
        Log.d("zzz","diff: "+diff+" henday: "+henday.get(a));
//        try {
        float val = henday.get(a);
        float persen =0;
        if (val != 0){
            persen = diff  * 100 / val ;
        }
        else
            persen = diff * 100;
        Log.d("zzz"," persen: "+persen);
        tvhen_day_percentage.setText(df.format(diff)+" ("+df.format(persen)+"%)");
//        }catch (Exception e){}


        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Population") ;
        lineDataSet1.setLineWidth(5f);
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet1.setColor(getResources().getColor(R.color.colorPrimary));
        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        lineChartHenday.setData(lineData);
        lineChartHenday.setDrawGridBackground(false);
        lineChartHenday.getAxisLeft().setEnabled(false);
        lineChartHenday.getAxisRight().setEnabled(false);
        lineChartHenday.setNoDataText("There is no Data");
        lineChartHenday.getXAxis().setEnabled(false);
        lineChartHenday.getDescription().setEnabled(false);
        lineChartHenday.animateX(2000);
        lineChartHenday.invalidate();
    }


    //Get YAXIS for Population
    private ArrayList<Entry> setYAxisPopulationDValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(0, 10));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        Log.i("IntegerArrayM", "IntegerArrayM "+ IntegerArrayM);

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation1DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(1, 11));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y1", "y1 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation2DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(2, 12));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y2", "y2 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation3DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(3, 13));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y3", "y3 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation4DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(4, 14));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y4", "y4 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation5DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(5, 15));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y5", "y5 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation6DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(6, 16));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y6", "y6 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisPopulation7DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> population = ((DashboardLayerMainActivity) getActivity()).getPopulation();
        ArrayList<Integer> population = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().size() ; i++) {
                population.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getPopulation().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(population.subList(7, 17));
        IntegerArrayM = y.toArray(new Integer[population.size()]);
        ////Log.i("y7", "y7 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    //Get YAXIS EggLAIDEGG
    private ArrayList<Entry> setYAxisEggLaidEggDValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(0, 10));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg1DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(1, 11));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y1", "y1 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg2DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(2, 12));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y2", "y2 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg3DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(3, 13));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y3", "y3 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg4DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(4, 14));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y4", "y4 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            for (int i = 0; i < y.size(); i++) {
                yVals.add(new Entry(i, (float) IntegerArrayM[i]));
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg5DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(5, 15));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y5", "y5 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg6DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(6, 16));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y6", "y6 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidEgg7DValues(){
        Integer[] IntegerArrayM;
//        ArrayList<Integer> eggLaid = ((DashboardLayerMainActivity) getActivity()).getEgglaidegg();
        ArrayList<Integer> eggLaid = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().size(); i++){
                eggLaid.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidEgg().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(eggLaid.subList(7, 17));
        IntegerArrayM = y.toArray(new Integer[eggLaid.size()]);
        ////Log.i("y7", "y7 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    //Get YAXIS EggLAIDWEIGHT
    private ArrayList<Entry> setYAxisEggLaidWeightDValues(){

//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(0, 10));
        //////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight1DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(1, 11));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight2DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(2, 12));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight3DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(3, 13));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight4DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(4, 14));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight5DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(5, 15));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight6DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(6, 16));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisEggLaidWeight7DValues(){
//        ArrayList<Float> eggWeight = ((DashboardLayerMainActivity) getActivity()).getEgglaidweight();
        ArrayList<Float> eggWeight = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size() > 0){
            for (int i =0; i< DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().size(); i++){
                eggWeight.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getEggLaidWeight().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(eggWeight.subList(7, 17));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    //Get YAXIS Henday
    private ArrayList<Entry> setYAxisHendayDValues(){

//        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(hd.subList(0, 10));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday1DValues(){
//        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(hd.subList(1, 11));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday2DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(2, 12));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday3DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(3, 13));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday4DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(4, 14));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday5DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(5, 15));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday6DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(6, 16));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisHenday7DValues(){
        //        ArrayList<Float> hd = ((DashboardLayerMainActivity) getActivity()).getHenday();
        ArrayList<Float> hd = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().size(); i++) {
                hd.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getHenday().get(i)));
            }
        }

        ArrayList<Float> y = new ArrayList<Float>(hd.subList(7, 17));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    //Get YAXIS FCR
    private ArrayList<Entry> setYAxisFcrDValues(){

//        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(0, 10));
        ////Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        ////Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr1DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(1, 11));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr2DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(2, 12));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr3DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(3, 13));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr4DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(4, 14));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr5DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(5, 15));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr6DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(6, 16));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisFcr7DValues(){
        //        ArrayList<Float> fcr = ((DashboardLayerMainActivity) getActivity()).getFcr();
        ArrayList<Float> fcr = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().size(); i++) {
                fcr.add(Float.parseFloat(DashboardLayerDetailProductionModel.getInstance().getPerformance().getFcr().get(i)));
            }
        }
        ArrayList<Float> y = new ArrayList<Float>(fcr.subList(7, 17));
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }


    //Get YAXIS for Mortality
    private ArrayList<Entry> setYAxisMortalityDValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(0, 10));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y", "y "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality1DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(1, 11));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y1", "y1 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality2DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(2, 12));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y2", "y2 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality3DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(3, 13));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y3", "y3 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality4DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(4, 14));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y4", "y4 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality5DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(5, 15));
        IntegerArrayM = y.toArray(new Integer[ mor.size()]);
        //Log.i("y5", "y5 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality6DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(6, 16));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y6", "y6 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }

    private ArrayList<Entry> setYAxisMortality7DValues(){
        Integer[] IntegerArrayM;
        ArrayList<Integer> mor = new ArrayList<>();
        if (DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality() != null && DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size() > 0) {
            for (int i = 0; i < DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().size(); i++) {
                mor.add(Integer.parseInt(DashboardLayerDetailProductionModel.getInstance().getPerformance().getMortality().get(i)));
            }
        }
        ArrayList<Integer> y = new ArrayList<Integer>(mor.subList(7, 17));
        IntegerArrayM = y.toArray(new Integer[mor.size()]);
        //Log.i("y7", "y7 "+ y);

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if (y != null && y.size() > 0) {
            int a = 9;
            for (int i = 0; i < 10; i++) {
                yVals.add(new Entry(i, (float) y.get(a)));
                a--;
            }
        }
        //Log.i("yVals", "yVals "+ yVals);
        return yVals;
    }
//    private void setDataD() {
//
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulationDValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEggDValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeightDValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcrDValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHendayDValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortalityDValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData1D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation1DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg1DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight1DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr1DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday1DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality1DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData2D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation2DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg2DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight2DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr2DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday2DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality2DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData3D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation3DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg3DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight3DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr3DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday3DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality3DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData4D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation4DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg4DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight4DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr4DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday4DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality4DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        //lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData5D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation5DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg5DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight5DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr5DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday5DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality5DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        //lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData6D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation6DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg6DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight6DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr6DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday6DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality6DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
//
//    private void setData7D() {
//
//        //Chart Population
//        ArrayList<String> xVals = setXAxisValues();
//        ArrayList<Entry> yVals = setYAxisPopulation7DValues();
//        LineDataSet set1;
//        set1 = new LineDataSet(yVals, "Population Data");
//        set1.setFillAlpha(110);
//        set1.setLineWidth(5f);
//        set1.setDrawCircles(false);
//        set1.setDrawCircleHole(false);
//        set1.setDrawValues(false);
//        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        set1.setColor(getResources().getColor(R.color.performance_chart));
//        //set1.setCircleColor(Color.BLACK);
//        //set1.setCircleRadius(3f);
//        //set1.setDrawCircleHole(false);
//        //set1.setValueTextSize(9f);
//        //set1.setDrawFilled(true);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(set1); // add the datasets
//        // create a data object with the datasets
//        LineData data = new LineData(dataSets);
//        // set data
//        lineChartPopulation.setData(data);
//        lineChartPopulation.setDrawGridBackground(false);
//        lineChartPopulation.getAxisLeft().setEnabled(false);
//        lineChartPopulation.getAxisRight().setEnabled(false);
//        lineChartPopulation.setNoDataText("There is no Data");
//        lineChartPopulation.getXAxis().setEnabled(false);
//        lineChartPopulation.getDescription().setEnabled(false);
//        lineChartPopulation.animateX(2000);
//        lineChartPopulation.invalidate();
//
//        //Chart EggLaidEgg
//        ArrayList<String> xValsEgg = setXAxisValues();
//        ArrayList<Entry> yValsEgg = setYAxisEggLaidEgg7DValues();
//        LineDataSet setEgg;
//        // create a dataset and give it a type
//        setEgg = new LineDataSet(yValsEgg, "Egg Laid Egg Data");
//        setEgg.setFillAlpha(110);
//        setEgg.setLineWidth(5f);
//        setEgg.setDrawCircles(false);
//        setEgg.setDrawCircleHole(false);
//        setEgg.setDrawValues(false);
//        setEgg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setEgg.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsEgg = new ArrayList<ILineDataSet>();
//        dataSetsEgg.add(setEgg); // add the datasets
//        // create a data object with the datasets
//        LineData dataEgg = new LineData(dataSetsEgg);
//        // set data
//        lineChartEggLainEgg.setData(dataEgg);;
//        lineChartEggLainEgg.setDrawGridBackground(false);
//        lineChartEggLainEgg.getAxisLeft().setEnabled(false);
//        lineChartEggLainEgg.getAxisRight().setEnabled(false);
//        lineChartEggLainEgg.setNoDataText("There is no Data");
//        lineChartEggLainEgg.getXAxis().setEnabled(false);
//        lineChartEggLainEgg.getDescription().setEnabled(false);
//        lineChartEggLainEgg.animateX(2000);
//        lineChartEggLainEgg.invalidate();
//
//        //Chart EggLaidWeight
//        ArrayList<String> xValsWeight = setXAxisValues();
//        ArrayList<Entry> yValsWeight = setYAxisEggLaidWeight7DValues();
//        LineDataSet setWeight;
//        // create a dataset and give it a type
//        setWeight = new LineDataSet(yValsWeight, "Egg Laid Weight Data");
//        setWeight.setFillAlpha(110);
//        setWeight.setLineWidth(5f);
//        setWeight.setDrawCircles(false);
//        setWeight.setDrawCircleHole(false);
//        setWeight.setDrawValues(false);
//        setWeight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setWeight.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsWeight = new ArrayList<ILineDataSet>();
//        dataSetsWeight.add(setWeight); // add the datasets
//        // create a data object with the datasets
//        LineData dataWeight = new LineData(dataSetsWeight);
//        // set data
//        lineChartEggLaidWeight.setData(dataWeight);
//        lineChartEggLaidWeight.setDrawGridBackground(false);
//        lineChartEggLaidWeight.getAxisLeft().setEnabled(false);
//        lineChartEggLaidWeight.getAxisRight().setEnabled(false);
//        lineChartEggLaidWeight.setNoDataText("There is no Data");
//        lineChartEggLaidWeight.getXAxis().setEnabled(false);
//        lineChartEggLaidWeight.getDescription().setEnabled(false);
//        lineChartEggLaidWeight.animateX(2000);
//        lineChartEggLaidWeight.invalidate();
//
//        //Chart FCR
//        ArrayList<String> xValsFcr = setXAxisValues();
//        ArrayList<Entry> yValsFcr = setYAxisFcr7DValues();
//        LineDataSet setFcr;
//        // create a dataset and give it a type
//        setFcr = new LineDataSet(yValsFcr, "FCR Data");
//        setFcr.setFillAlpha(110);
//        setFcr.setLineWidth(5f);
//        setFcr.setDrawCircles(false);
//        setFcr.setDrawCircleHole(false);
//        setFcr.setDrawValues(false);
//        setFcr.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setFcr.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsFcr = new ArrayList<ILineDataSet>();
//        dataSetsFcr.add(setFcr); // add the datasets
//        // create a data object with the datasets
//        LineData dataFcr = new LineData(dataSetsFcr);
//        // set data
//        lineChartFcr.setData(dataFcr);
//        lineChartFcr.setDrawGridBackground(false);
//        lineChartFcr.getAxisLeft().setEnabled(false);
//        lineChartFcr.getAxisRight().setEnabled(false);
//        lineChartFcr.setNoDataText("There is no Data");
//        lineChartFcr.getXAxis().setEnabled(false);
//        lineChartFcr.getDescription().setEnabled(false);
//        lineChartFcr.animateX(2000);
//        lineChartFcr.invalidate();
//
//        //Chart Henday
//        ArrayList<String> xValsHd = setXAxisValues();
//        ArrayList<Entry> yValsHd = setYAxisHenday7DValues();
//        LineDataSet setHd;
//        // create a dataset and give it a type
//        setHd = new LineDataSet(yValsHd, "Hen-day Data");
//        setHd.setFillAlpha(110);
//        setHd.setLineWidth(5f);
//        setHd.setDrawCircles(false);
//        setHd.setDrawCircleHole(false);
//        setHd.setDrawValues(false);
//        setHd.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setHd.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsHd = new ArrayList<ILineDataSet>();
//        dataSetsHd.add(setHd); // add the datasets
//        // create a data object with the datasets
//        LineData dataHd = new LineData(dataSetsHd);
//        // set data
//        lineChartHenday.setData(dataHd);
//        lineChartHenday.setDrawGridBackground(false);
//        lineChartHenday.getAxisLeft().setEnabled(false);
//        lineChartHenday.getAxisRight().setEnabled(false);
//        lineChartHenday.setNoDataText("There is no Data");
//        lineChartHenday.getXAxis().setEnabled(false);
//        lineChartHenday.getDescription().setEnabled(false);
//        lineChartHenday.animateX(2000);
//        lineChartHenday.invalidate();
//
//        //Chart Mortality
//        ArrayList<String> xValsMor = setXAxisValues();
//        ArrayList<Entry> yValsMor = setYAxisMortality7DValues();
//        LineDataSet setMor;
//        // create a dataset and give it a type
//        setMor = new LineDataSet(yValsMor, "Mortality Data");
//        setMor.setFillAlpha(110);
//        setMor.setLineWidth(5f);
//        setMor.setDrawCircles(false);
//        setMor.setDrawCircleHole(false);
//        setMor.setDrawValues(false);
//        setMor.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        setMor.setColor(getResources().getColor(R.color.performance_chart));
//        ArrayList<ILineDataSet> dataSetsMor = new ArrayList<ILineDataSet>();
//        dataSetsMor.add(setMor); // add the datasets
//        // create a data object with the datasets
//        LineData dataMor = new LineData(dataSetsMor);
//        // set data
//        lineChartMortality.setData(dataMor);
//        lineChartMortality.setDrawGridBackground(false);
//        lineChartMortality.getAxisLeft().setEnabled(false);
//        lineChartMortality.getAxisRight().setEnabled(false);
//        lineChartMortality.setNoDataText("There is no Data");
//        lineChartMortality.getXAxis().setEnabled(false);
//        lineChartMortality.getDescription().setEnabled(false);
//        lineChartMortality.animateX(2000);
//        lineChartMortality.invalidate();
//    }
}
