package com.layerfarm.dashboard.DashboardLayer.DashboardLayerAdapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.recording.helper.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DashboardLayerPerformanceDetailEggNumberAdapter extends RecyclerView.Adapter<DashboardLayerPerformanceDetailEggNumberAdapter.ViewHolder>  {
    LineChart linePopulationCart ;
    private ArrayList<String> population;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    int index;
    private ArrayList<DashboardLayerDetailModel.LayerPerformanceDetailLocationFlock> recording_egg_number_items;

    public DashboardLayerPerformanceDetailEggNumberAdapter(ArrayList<DashboardLayerDetailModel.LayerPerformanceDetailLocationFlock> inputData, int index){
        recording_egg_number_items = inputData;
        this.index = index;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView production_eggs;
        public TextView totale_ggs;
        public TextView dot;
        public TextView valuePercentage;
        public LineChart lineEggNumberCart;
        public ViewGroup leftLinear, rightLinear;
        ViewHolder holder;
        public ViewHolder(final View v) {
            super(v);
            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineEggNumberCart = (LineChart) v.findViewById(R.id.lineChartEggNumber);
            MyMarkerView mv = new MyMarkerView(v.getContext(), R.layout.custom_marker_view_layout);
            mv.setChartView(lineEggNumberCart); // For bounds control
            lineEggNumberCart.setMarker(mv);
            lineEggNumberCart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    Log.i("Activity", "Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
                }

                @Override
                public void onNothingSelected() {

                }
            });
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            //production_eggs = (TextView) v.findViewById(R.id.productionEggs);
            totale_ggs = (TextView) v.findViewById(R.id.totalEggs);
            leftLinear = (ViewGroup) v.findViewById(R.id.linear_left);
            rightLinear = (ViewGroup) v.findViewById(R.id.linear_right);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.egg_number_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public class MyMarkerView extends MarkerView {

        private final TextView tvContent;

        public MyMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            tvContent = findViewById(R.id.tvContent);
        }

        // runs every time the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {

            if (e instanceof CandleEntry) {

                CandleEntry ce = (CandleEntry) e;

                tvContent.setText(
                        "Age : "+ Utils.formatNumber(ce.getHigh(), 0, true)+
                                " days\nEgg Number : "+Utils.formatNumber(e.getY(), 0, true)+" Eggs");
            } else {

                tvContent.setText("Age : "+Utils.formatNumber(e.getX(), 0, true)+
                        " days\nEgg Number : "+Utils.formatNumber(e.getY(), 0, true)+" Eggs");
            }

            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        DecimalFormat dFormat = new DecimalFormat("####,###,###.##");
        final String location = recording_egg_number_items.get(position).getFarm_name();
        final String flockname = recording_egg_number_items.get(position).getFlock_name();
        final String flocktype = recording_egg_number_items.get(position).getFlock_type();
        final String flockperiod = recording_egg_number_items.get(position).getFlock_period();
        final String flockid = recording_egg_number_items.get(position).getFlock_nid();
        final String capacity = recording_egg_number_items.get(position).getFlock_capacity();
        final int age = recording_egg_number_items.get(position).getAge();
        final String lastRecording = recording_egg_number_items.get(position).getLast_recording();
        //final String productioneggs = recording_egg_number_items.get(position).getProduction_eggs();
        double total = Double.parseDouble(recording_egg_number_items.get(position).getTotal_eggs());
        final String totaleggs = dFormat.format(total);
        Log.d("adapter","tes "+flockid);

        if (flockid != null) {


            DashboardLayerDetailModel.LayerPerformanceDetailFlockData population_data = DashboardLayerDetailModel.getInstance().getFlock_data();

            ArrayList<String> arr_recording_date = population_data.getRecording_date().get(Integer.parseInt(flockid));
            ArrayList<Integer> arr_eggnumber = population_data.getTotal_egg().get(Integer.parseInt(flockid));
            ArrayList<String> ar_recording_date = new ArrayList<>();
            ArrayList<Integer> ar_eggnumber = new ArrayList<>();

            int val_size = arr_recording_date.size() - index;

            if (val_size <= 0) {
                ar_recording_date = new ArrayList<>();
                ar_eggnumber = new ArrayList<>();
            } else {
                ar_recording_date = new ArrayList<>(arr_recording_date);
                ar_eggnumber = new ArrayList<>(arr_eggnumber);
                if (index != 0) {
                    for (int a = 0; a < index; a++) {
                        ar_recording_date.remove(ar_recording_date.size() - 1);
                        ar_eggnumber.remove(ar_eggnumber.size() - 1);
                    }
                }
                //lastRecording = ar_recording_date.get(ar_recording_date.size() - 1);
                Log.d("adapter", "tes " + ar_eggnumber.get(ar_recording_date.size() - 1));
                String egg_number = String.valueOf(ar_eggnumber.get(ar_recording_date.size() - 1));
                holder.valuePercentage.setText(egg_number);
            }

            // Displaying dot from HTML character code
            holder.dot.setText(Html.fromHtml("&#8226;"));
            holder.lastrecording.setText(lastRecording);
            holder.location_name.setText(location + "-" + flockname + "-" + "(" + flocktype + " " + flockperiod + ")");
            holder.totale_ggs.setText(totaleggs);
            //holder.production_eggs.setText(productioneggs);

//        holder.capacity.setText(capacity);
//        holder.number_bird.setText(String.valueOf(total_ayam));
//        holder.utilization.setText(utilization);
            ArrayList<Integer> index = new ArrayList<>();
            for (int i = 1; i <= ar_eggnumber.size(); i++) {
                index.add(i);
            }


            //INISIALISASI DATA CHART
            ArrayList<Integer> valueY = ar_eggnumber;
            List<Entry> set2 = new ArrayList<Entry>();
            ArrayList<Integer> valueX = index;
            int counter = age - valueY.size();
            for (int i = 0; i < valueY.size(); i++) {
                set2.add(new Entry(valueX.get(i)+counter, valueY.get(i)));
            }
            LineDataSet lineDataSet1 = new LineDataSet(set2, "Egg Number");
            lineDataSet1.setColor(Color.YELLOW);
            lineDataSet1.setCircleColor(Color.YELLOW);
            lineDataSet1.setLineWidth(1f);
            lineDataSet1.setCircleRadius(1f);
            lineDataSet1.setDrawCircleHole(false);
            lineDataSet1.setValueTextSize(9f);
            lineDataSet1.setDrawFilled(true);

            List<ILineDataSet> list = new ArrayList<ILineDataSet>();
            //list.add(lineDataSet) ;
            list.add(lineDataSet1);
            LineData lineData = new LineData(list);
            holder.lineEggNumberCart.setTouchEnabled(true);
            holder.lineEggNumberCart.setDragDecelerationFrictionCoef(0.9f);
            holder.lineEggNumberCart.setDragEnabled(true);
            holder.lineEggNumberCart.getDescription().setEnabled(true);
            holder.lineEggNumberCart.setScaleEnabled(false);
            holder.lineEggNumberCart.setDrawGridBackground(false);
            holder.lineEggNumberCart.setHighlightPerDragEnabled(true);
            holder.lineEggNumberCart.setPinchZoom(false);
            holder.lineEggNumberCart.setAutoScaleMinMaxEnabled(true);
            //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.lineEggNumberCart.animateX(500);

            holder.lineEggNumberCart.setLayerType(View.LAYER_TYPE_NONE, null);

            holder.lineEggNumberCart.notifyDataSetChanged();
            holder.lineEggNumberCart.setData(lineData);
            holder.lineEggNumberCart.invalidate();


            XAxis xAxis = holder.lineEggNumberCart.getXAxis();
            xAxis.setTextSize(11f);
            xAxis.setTextColor(Color.RED);
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            YAxis leftAxis = holder.lineEggNumberCart.getAxisLeft();
            leftAxis.setTextColor(ColorTemplate.getHoloBlue());
            leftAxis.setAxisMinimum(0f);
            leftAxis.setDrawGridLines(true);
            leftAxis.setGranularityEnabled(true);
            leftAxis.setEnabled(true);

            YAxis rightAxis = holder.lineEggNumberCart.getAxisRight();
            rightAxis.setTextColor(Color.RED);
            rightAxis.setAxisMinimum(0);
            rightAxis.setDrawGridLines(true);
            rightAxis.setEnabled(true);
            rightAxis.setDrawZeroLine(true);
            rightAxis.setGranularityEnabled(true);


            Legend l = holder.lineEggNumberCart.getLegend();
            l.setForm(Legend.LegendForm.LINE);
            l.setTextSize(11f);
            l.setTextColor(Color.BLUE);
            l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
            l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
            l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
            l.setDrawInside(false);


            lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);

            lineDataSet1.setFillAlpha(60);
            lineDataSet1.setDrawCircleHole(false);
            lineDataSet1.setDrawValues(false);
            lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            lineDataSet1.setLineWidth(1f);
            lineDataSet1.setColor(Color.YELLOW);

            holder.lineEggNumberCart.getData().notifyDataChanged();
            holder.lineEggNumberCart.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return recording_egg_number_items.size();
    }


   /* public ArrayList<Integer> population (String flockId){
        ArrayList<Integer> population = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String Query = "";
        if (!tgl_kosong.isEmpty()){
            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"' AND recording_date > '"+tgl_kosong+"'";

        }
        else {
            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"'";
        }
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String total = cursor.getString(cursor.getColumnIndex("total_bird"));
            int qty = Integer.parseInt(total);
            population.add(qty);
        }
        return population;
    }
*/

}
