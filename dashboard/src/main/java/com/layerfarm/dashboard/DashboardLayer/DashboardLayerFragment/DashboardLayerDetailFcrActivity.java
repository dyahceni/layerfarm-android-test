package com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.dashboard.DashboardLayer.DashboardLayerAdapter.DashboardLayerPerformanceDetailFcrAdapter;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Population;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardLayerDetailFcrActivity extends AppCompatActivity {

    DatabaseHelper SQLite = new DatabaseHelper(this);

    ArrayList<Population> recording_population_items;
    private DBDataSource dataSource;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Spinner spn_location;
    helper help;
    int index;
    ArrayList<String> location = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fcr_main);
        SQLite = new DatabaseHelper(getApplicationContext());
        dataSource =  new DBDataSource(this);
        dataSource.open();
        getDetailPopulation();

        try{
            Bundle intentIndex = getIntent().getExtras();
            index = intentIndex.getInt("index");
            Log.d("ssss","index = "+index);
            Log.d("ssss","location = "+location);
            spn_location = (Spinner) findViewById(R.id.find_location);

            spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Submit();
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }catch (NullPointerException e ){
        Toast.makeText(getApplicationContext(),"No Data Show",Toast.LENGTH_SHORT).show();
    }

        rvFlock = (RecyclerView) findViewById(R.id.recycler_fcr);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 25));
        rvFlock.setLayoutManager(layoutManager);

    }

    public void getDetailPopulation(){
        final ProgressDialog loading = ProgressDialog.show(this, "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_layer_performance_detail_fcr";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardLayerDetailModel.LayerPerformanceDetail> call = apiInterfaceJson.DashboardLayerDetailPopulation(token2, parameter);

        call.enqueue(new Callback<DashboardLayerDetailModel.LayerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardLayerDetailModel.LayerPerformanceDetail> call, Response<DashboardLayerDetailModel.LayerPerformanceDetail> response) {
                DashboardLayerDetailModel.LayerPerformanceDetail dashboardLayerDetail = response.body();
                DashboardLayerDetailModel.LayerPerformanceDetailFlockData data = dashboardLayerDetail.getFlock_data();
                Log.d("zzz","log= "+data.toString());
                DashboardLayerDetailModel.getInstance().setLocation_flock(dashboardLayerDetail.getLocation_flock());
                DashboardLayerDetailModel.getInstance().setFlock_data(dashboardLayerDetail.getFlock_data());
                //setupViewPager(viewPager);
                Set<String> keySet = DashboardLayerDetailModel.getInstance().getLocation_flock().keySet();
                location = new ArrayList<>(keySet);
                spn_location.setAdapter(new ArrayAdapter(DashboardLayerDetailFcrActivity.this, android.R.layout.simple_spinner_dropdown_item, location));
                spn_location.setSelected(true);
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<DashboardLayerDetailModel.LayerPerformanceDetail> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }

    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        //String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
//        SQLiteDatabase dbase = SQLit/*e.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
//        ArrayList<Population> popula*/tion_items = new ArrayList<>();
        ArrayList<DashboardLayerDetailModel.LayerPerformanceDetailLocationFlock> items = DashboardLayerDetailModel.getInstance().getLocation_flock().get(location);
//        for (int i=0; /*i < items.size(); i++){
//            Population population = new Population();
//            population.setLocation_name(location);
//            population.setFlock_name(items.get(i).getFlock_name());
//            population.setFlock_type(items.get(i).getFlock_period());
//            population.setFlock_period(items.get(i).getFlock_period());
//            population.setFlock_id(items.get(i).getFlock_nid());
//            population_items.add(population);
//        }*/
        adapter = new DashboardLayerPerformanceDetailFcrAdapter(items,index);
        rvFlock.setAdapter(adapter);
    }
    public Population population(String location, String flock, String type){
        Population population = new Population();
        return  population;
    }


    public void cls_fcr(View view) {
        finish();
    }
}
