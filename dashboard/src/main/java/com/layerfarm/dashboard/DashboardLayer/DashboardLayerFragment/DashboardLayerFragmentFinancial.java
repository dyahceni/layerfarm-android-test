package com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.dashboard.DashboardLayer.DashboardLayerMainActivity;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.layerfarm.model.DashboardLayerFinancial;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardLayerFragmentFinancial extends Fragment {

    private TextView tx_min;
    private TextView tx_max;
    private TextView tx_profit;
    private TextView tx_income;
    private TextView tx_expense;
    private TextView tx_idr;
    private TextView tx_finance_status;
    LinearLayout timeout, main_layout;

    private ArrayList profitList = new ArrayList<>(9);
    private ArrayList expenseList = new ArrayList<>(9);
    private ArrayList incomeList = new ArrayList<>(9);
    private ArrayList min = new ArrayList<String>(9);
    private ArrayList max = new ArrayList<String>(9);

    ProgressDialog loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_layer_fragment_financial, container, false);
        tx_min = (TextView) view.findViewById(R.id.tvMin);
        tx_max = (TextView) view.findViewById(R.id.tvMax);
        tx_income = (TextView) view.findViewById(R.id.tv_income);
        tx_expense = (TextView) view.findViewById(R.id.tv_expense);
        tx_profit = (TextView) view.findViewById(R.id.tv_profit);
        tx_idr = (TextView) view.findViewById(R.id.tv_idr);
        tx_finance_status = (TextView) view.findViewById(R.id.tv_finance_status);
        final TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.tabs);
        timeout = (LinearLayout) view.findViewById(R.id.time_out);
        main_layout = (LinearLayout) view.findViewById(R.id.main_layout);
        getFinancial();

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
            return view;
        }

    private void getFinancial(){
        loading = ProgressDialog.show(getContext(), "Load Data", "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_layer_financial";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardLayerFinancial> call = apiInterfaceJson.DashboardLayerFinancial(token2, parameter);

        call.enqueue(new Callback<DashboardLayerFinancial>() {
            @Override
            public void onResponse(Call<DashboardLayerFinancial> call, Response<DashboardLayerFinancial> response) {
                DashboardLayerFinancial resp = response.body();
                DashboardLayerFinancial.getInstance().setEggSell(resp.getEggSell());
                DashboardLayerFinancial.getInstance().setExpense(resp.getExpense());
                DashboardLayerFinancial.getInstance().setIncome(resp.getIncome());
                DashboardLayerFinancial.EggSell egg = resp.getEggSell();

                min.addAll(egg.getMin());
                max.addAll(egg.getMax());

                //Get Income Array Data
                ArrayList<String> numIncome = new ArrayList<>();
                ArrayList<DashboardLayerFinancial.Income> incomelist = resp.getIncome();
                String null_value = "0";
                if (incomelist != null && incomelist.size() > 0) {
                    for (int i = 0; i < incomelist.size(); i++) {
                        if(incomelist.get(i).getIncome() == null){
                            numIncome.add(null_value);
                        }else{
                            numIncome.add(incomelist.get(i).getIncome());
                        }
                    }
                }

                //Get expense Array Data
                ArrayList<String> numExpense = new ArrayList<>();
                ArrayList<DashboardLayerFinancial.Expense> expenselist = resp.getExpense();
                if (expenselist != null && expenselist.size() > 0) {
                    for (int i = 0; i < expenselist.size(); i++) {
                        if(expenselist.get(i).getValue() == null){
                            numExpense.add(null_value);
                        }else{
                            numExpense.add(expenselist.get(i).getValue());
                        }
                    }
                }

                if (numIncome != null && numIncome.size() > 0) {
                    for (int i = 0; i < numIncome.size(); i++) {
                        profitList.add(Double.parseDouble(numIncome.get(i)) - Double.parseDouble(numExpense.get(i)));
                        incomeList.add(numIncome.get(i));
                        expenseList.add(numExpense.get(i));
                    }
                }

                onTabTapped(0);
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<DashboardLayerFinancial> call, Throwable t) {
                loading.dismiss();
                timeout.setVisibility(View.VISIBLE);
                main_layout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }

    private void onTabTapped(int position) {
        DecimalFormat dFormat = new DecimalFormat("###,###,###,###,###.##");
        double price_min ;
        double price_max ;

        switch (position) {
            case 0:
                price_min = Double.valueOf((String)min.get(0));
                price_max = Double.valueOf((String) max.get(0));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double a = (Double) profitList.get(0);
                tx_profit.setText(String.valueOf(dFormat.format(a)));

                String strExp =expenseList.get(0).toString();
                double exp = Double.parseDouble(strExp);
                tx_expense.setText(String.valueOf(dFormat.format(exp)));

                String strInc = incomeList.get(0).toString();
                double inc = Double.parseDouble(strInc);
                tx_income.setText(String.valueOf(dFormat.format(inc)));

                if ( a < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }

                break;
            case 1:
                price_min = Double.valueOf((String) min.get(1));
                price_max = Double.valueOf((String) max.get(1));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double b = (Double) profitList.get(1);
                tx_profit.setText(String.valueOf(dFormat.format(b)));

                String strExp1 =expenseList.get(1).toString();
                double exp1 = Double.parseDouble(strExp1);
                tx_expense.setText(String.valueOf(dFormat.format(exp1)));

                String strInc1 = incomeList.get(1).toString();
                double inc1 = Double.parseDouble(strInc1);
                tx_income.setText(String.valueOf(dFormat.format(inc1)));


                if ( b < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 2:
                price_min = Double.valueOf((String) min.get(2));
                price_max = Double.valueOf((String) max.get(2));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double c = (Double) profitList.get(2);
                tx_profit.setText(String.valueOf(dFormat.format(c)));

                String strExp2 =expenseList.get(2).toString();
                double exp2 = Double.parseDouble(strExp2);
                tx_expense.setText(String.valueOf(dFormat.format(exp2)));

                String strInc2 = incomeList.get(2).toString();
                double inc2 = Double.parseDouble(strInc2);
                tx_income.setText(String.valueOf(dFormat.format(inc2)));


                if ( c < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 3:
                price_min = Double.valueOf((String) min.get(3));
                price_max = Double.valueOf((String) max.get(3));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double d = (Double) profitList.get(3);
                tx_profit.setText(String.valueOf(dFormat.format(d)));

                String strExp3 =expenseList.get(3).toString();
                double exp3 = Double.parseDouble(strExp3);
                tx_expense.setText(String.valueOf(dFormat.format(exp3)));

                String strInc3 = incomeList.get(3).toString();
                double inc3 = Double.parseDouble(strInc3);
                tx_income.setText(String.valueOf(dFormat.format(inc3)));

                if ( d < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 4:
                price_min = Double.valueOf((String) min.get(4));
                price_max = Double.valueOf((String) max.get(4));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double e = (Double) profitList.get(4);
                tx_profit.setText(String.valueOf(dFormat.format(e)));

                String strExp4 =expenseList.get(4).toString();
                double exp4 = Double.parseDouble(strExp4);
                tx_expense.setText(String.valueOf(dFormat.format(exp4)));

                String strInc4 = incomeList.get(4).toString();
                double inc4 = Double.parseDouble(strInc4);
                tx_income.setText(String.valueOf(dFormat.format(inc4)));

                if ( e < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 5:
                price_min = Double.valueOf((String) min.get(7));
                price_max = Double.valueOf((String) max.get(7));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double f = (Double) profitList.get(7);
                tx_profit.setText(String.valueOf(dFormat.format(f)));

                String strExp5 =expenseList.get(7).toString();
                double exp5 = Double.parseDouble(strExp5);
                tx_expense.setText(String.valueOf(dFormat.format(exp5)));

                String strInc5 = incomeList.get(7).toString();
                double inc5 = Double.parseDouble(strInc5);
                tx_income.setText(String.valueOf(dFormat.format(inc5)));

                if ( f < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 6:
                price_min = Double.valueOf((String) min.get(5));
                price_max = Double.valueOf((String) max.get(5));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double g = (Double) profitList.get(5);
                tx_profit.setText(String.valueOf(dFormat.format(g)));

                String strExp6 =expenseList.get(5).toString();
                double exp6 = Double.parseDouble(strExp6);
                tx_expense.setText(String.valueOf(dFormat.format(exp6)));

                String strInc6 = incomeList.get(5).toString();
                double inc6 = Double.parseDouble(strInc6);
                tx_income.setText(String.valueOf(dFormat.format(inc6)));

                if ( g < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            case 7:
                price_min = Double.valueOf((String) min.get(6));
                price_max = Double.valueOf((String) max.get(6));
                tx_min.setText("IDR "+String.valueOf(dFormat.format(price_min))+" - "+String.valueOf(dFormat.format(price_max))+" /kg");

                double h = (Double) profitList.get(6);
                tx_profit.setText(String.valueOf(dFormat.format(h)));

                String strExp7 =expenseList.get(6).toString();
                double exp7 = Double.parseDouble(strExp7);
                tx_expense.setText(String.valueOf(dFormat.format(exp7)));

                String strInc7 = incomeList.get(6).toString();
                double inc7 = Double.parseDouble(strInc7);
                tx_income.setText(String.valueOf(dFormat.format(inc7)));

                if ( h < 0) {
                    tx_profit.setTextColor(getResources().getColor(R.color.red));
                    tx_idr.setTextColor(getResources().getColor(R.color.red));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tx_profit.setTextColor(getResources().getColor(R.color.green));
                    tx_idr.setTextColor(getResources().getColor(R.color.green));
                    tx_finance_status.setTextColor(getResources().getColor(R.color.green));
                }
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }
}