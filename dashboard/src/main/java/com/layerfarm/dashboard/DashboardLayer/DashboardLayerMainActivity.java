package com.layerfarm.dashboard.DashboardLayer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentFinancial;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentOverview;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentPerformance;
import com.layerfarm.dashboard.R;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardLayerMainActivity extends AppCompatActivity {
    ProgressDialog loading;
    private  ArrayList type_name = new ArrayList();
    private  ArrayList mor_name = new ArrayList();

    private  ArrayList mor_value_today = new ArrayList();
    private  ArrayList mor_value_lastday = new ArrayList();
    private  ArrayList mor_value_week = new ArrayList();
    private  ArrayList mor_value_1month = new ArrayList();
    private  ArrayList mor_value_3month = new ArrayList();
    private  ArrayList mor_value_6month = new ArrayList();
    private  ArrayList mor_value_1year = new ArrayList();
    private  ArrayList mor_value_2year = new ArrayList();
    private  ArrayList mor_value_ytd = new ArrayList();

    private  ArrayList prod_value_today = new ArrayList();
    private  ArrayList prod_value_lastday = new ArrayList();
    private  ArrayList prod_value_week = new ArrayList();
    private  ArrayList prod_value_1month = new ArrayList();
    private  ArrayList prod_value_3month = new ArrayList();
    private  ArrayList prod_value_6month = new ArrayList();
    private  ArrayList prod_value_1year = new ArrayList();
    private  ArrayList prod_value_2year = new ArrayList();
    private  ArrayList prod_value_ytd = new ArrayList();

    private ArrayList<Double> eggSell = new ArrayList();
    private  ArrayList income = new ArrayList();
    private  ArrayList expense = new ArrayList();

    private ArrayList date_performance = new ArrayList<>();
    private ArrayList population = new ArrayList<>();
    private ArrayList egglaidegg = new ArrayList<>();
    private ArrayList egglaidweight = new ArrayList<>();
    private ArrayList fcr = new ArrayList<>();
    private ArrayList henday = new ArrayList<>();
    private ArrayList mortality = new ArrayList<>();
    ArrayList<Double> expenseValue = new ArrayList<Double>();
    private  double eggToday, eggLastDay, egg1Week, egg1Month, egg3Month, egg6Month, egg1Year, egg2Year, eggYtd;
    private  double weightToday, weightLastDay, weight1Week, weight1Month, weight3Month, weight6Month, weight1Year, weight2Year, weightYtd;
    private  int morToday, morLastDay, mor1Week, mor1Month, mor3Month, mor6Month, mor1Year, mor2Year, morYtd;
    private String last_recording_date;



    private  ArrayList egg = new ArrayList();
    private  ArrayList weight = new ArrayList();

    private ProgressDialog dialog;
    private SQLiteDatabase database;
    DatabaseHelper db;
    public String nid;
    TextView txtLastRecording;
    ViewPager viewPager;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layer_activity);
        final TabLayout TabLayoutLayerMain  = (TabLayout) findViewById(R.id.tabLayout);

//        // setting toolbar
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        loading = ProgressDialog.show(this, null, "Please wait...", true, false);
        getLayerFarmDashboard();
        getLastRecording();
        //getLayerData();
        // setting view pager
//        viewPager = findViewById(R.id.viewPager);

        // setting tabLayout
        txtLastRecording = findViewById(R.id.txtLastRecording);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
//        tabLayout.setupWithViewPager(viewPager);
        onTabTapped(0);
        for (int i = 0; i < TabLayoutLayerMain.getTabCount(); i++) {

            TabLayout.Tab tab = TabLayoutLayerMain.getTabAt(i);
            if (tab != null) {

                TextView tabTextView = new TextView(this);
                tab.setCustomView(tabTextView);

                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                tabTextView.setText(tab.getText());
                tabTextView.setTextSize(20);

                if (i == 0) {
                    tabTextView.setTextSize(20);
                }

            }

        }

        TabLayoutLayerMain.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                ViewGroup vg = (ViewGroup) TabLayoutLayerMain.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTextSize(25);
                        ((TextView) tabViewChild).setTextColor(getResources().getColorStateList(R.color.blackColor));
                        ((TextView) tabViewChild).setTypeface(null, Typeface.BOLD);
                    }
                }

/*                if (tab.getPosition() == 0) {
                    mTabLayout.setTabTextColors(ContextCompat.getColorStateList(getContext(), R.color.white));
                    mTabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.blue));
                } else if (tab.getPosition() == 1) {
                    //mTabLayout.getTabAt(0).getIcon().setAlpha(100);

                    mTabLayout.setTabTextColors(ContextCompat.getColorStateList(getContext(), R.color.white));
                    mTabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.blue));

                } else if (tab.getPosition() == 2) {
                    //mTabLayout.getTabAt(0).getIcon().setAlpha(100);

                    mTabLayout.setTabTextColors(ContextCompat.getColorStateList(getContext(), R.color.white));
                    mTabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.blue));

                }*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ViewGroup vg = (ViewGroup) TabLayoutLayerMain.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTextSize(20);
                        ((TextView) tabViewChild).setTextColor(getResources().getColorStateList(R.color.white));
                        ((TextView) tabViewChild).setTypeface(null, Typeface.BOLD);
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

    }
    private void setupViewPager(ViewPager viewPager) {
        Adapter mainFragmentPagerAdapter = new Adapter(getSupportFragmentManager());
        mainFragmentPagerAdapter.addFragment(new DashboardLayerFragmentOverview(), "Overview");
        mainFragmentPagerAdapter.addFragment(new DashboardLayerFragmentPerformance(), "Performance");
        mainFragmentPagerAdapter.addFragment(new DashboardLayerFragmentFinancial(), "Financial");
        viewPager.setAdapter(mainFragmentPagerAdapter);
    }

    public void cls_layer_dashboard(View view) {
        finish();
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void getLayerFarmDashboard(){
        String module = "layerfarm_android";
        String function_name = "layer_android_dashboard_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LayerDataDashboard> call = apiInterfaceJson.getDashboardLayerfarm(token2, parameter);

        call.enqueue(new Callback<LayerDataDashboard>() {
            @Override
            public void onResponse(Call<LayerDataDashboard> call, Response<LayerDataDashboard> response) {
                //Log.i("Responsestring", response.body().toString());
                //Toast.makeText()

                if (response != null && response.isSuccessful()) {
                    try {
                        LayerDataDashboard layerdata = response.body();
                        //Log.d("Layerfarm","title = "+layerdata.getLastRecording().getValue());
                        last_recording_date = layerdata.getLastRecording();


                        //Get Date
                        if (layerdata.getPerformanceDate() != null && layerdata.getPerformanceDate().size() > 0) {
                            for (int i = 0; i < layerdata.getPerformanceDate().size(); i++) {
                                date_performance.add(layerdata.getPerformanceDate().get(i));
                            }
                        }
                        //Log.d("date_performance","date_performance = "+date_performance);

                        //Get Population
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getPopulation() != null && layerdata.getPopulation().size() > 0) {
                            for (int i = 0; i < layerdata.getPopulation().size() ; i++) {
                                population.add(Integer.parseInt(layerdata.getPopulation().get(i)));
                            }
                        }
                        Log.d("population","population = "+population);

                        //Get egg
                        //int egg_size = layerdata.getEggLaidEgg().size();
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getEggLaidEgg() != null && layerdata.getEggLaidEgg().size() > 0) {
                            for (int i = 0; i < layerdata.getEggLaidEgg().size(); i++) {
                                egglaidegg.add(Integer.parseInt(layerdata.getEggLaidEgg().get(i)));
                            }
                        }
                        Log.d("egglaidegg","egglaidegg = "+egglaidegg);

                        //Get egg weight
                        //int weight_size = layerdata.getEggLaidWeight().size();
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getEggLaidWeight() != null && layerdata.getEggLaidWeight().size() > 0) {
                            for (int i = 0; i < layerdata.getEggLaidWeight().size(); i++) {
                                egglaidweight.add(Float.parseFloat(layerdata.getEggLaidWeight().get(i)));
                            }
                        }
                        Log.d("egglaidweight","egglaidweight = "+egglaidweight);

                        //Get fcr
                        //int fcr_size = layerdata.getFcr().size();
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getFcr() != null && layerdata.getFcr().size() > 0) {
                            for (int i = 0; i < layerdata.getFcr().size(); i++) {
                                fcr.add(Float.parseFloat(layerdata.getFcr().get(i)));
                            }
                        }
                        Log.d("fcr","fcr = "+fcr);

                        //Get mortality
                        //int mortality_size = layerdata.getMortality().size();
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getMortality() != null && layerdata.getMortality().size() > 0) {
                            for (int i = 0; i < layerdata.getMortality().size(); i++) {
                                mortality.add(Integer.parseInt(layerdata.getMortality().get(i)));
                            }
                        }
                        Log.d("mortality","mortality = "+mortality);

                        //Get henday
                        //int henday_size = layerdata.getHenday().size();
                        //Log.d("tes","tes = "+tes);
                        if (layerdata.getHenday() != null && layerdata.getHenday().size() > 0) {
                            for (int i = 0; i < layerdata.getHenday().size(); i++) {
                                henday.add(Float.parseFloat(layerdata.getHenday().get(i)));
                            }
                        }
                        Log.d("henday","henday = "+henday);

                        //Get Income Array Data
                        ArrayList<LayerDataDashboard.Income> incomelist = layerdata.getIncome();
                        String null_value = "0";
                        if (incomelist != null && incomelist.size() > 0) {
                            for (int i = 0; i < incomelist.size(); i++) {
                                if(incomelist.get(i).getIncome() == null){
                                    income.add(null_value);
                                }else{
                                    income.add(incomelist.get(i).getIncome());
                                }
                            }
                        }
                        Log.d("income","income = "+income);

                        //Get expense Array Data
                        ArrayList<LayerDataDashboard.Expense> expenselist = layerdata.getExpense();
                        if (expenselist != null && expenselist.size() > 0) {
                            for (int i = 0; i < expenselist.size(); i++) {
                                if(expenselist.get(i).getValue() == null){
                                    expense.add(null_value);
                                    expenseValue.add(Double.parseDouble(null_value));
                                }else{
                                    expense.add(expenselist.get(i).getValue());
                                    expenseValue.add(Double.parseDouble(expenselist.get(i).getValue()));
                                }
                            }
                        }
                        Log.d("expenseValue","expenseValue = "+expenseValue);

                        //Get Production Array Data
                        ArrayList<LayerDataDashboard.PToday> type = layerdata.getPToday();
                        if (type != null && type.size() > 0) {
                            for (int i = 0; i < type.size(); i++) {
                                type_name.add(type.get(i).getType());
                                int myPtoday;
                                double myWtoday;

                                if (type.get(i).getEgg() != null && !type.get(i).getEgg().isEmpty()) {
                                    myPtoday = Integer.parseInt(type.get(i).getEgg());
                                } else {
                                    myPtoday = 0;
                                }

                                if (type.get(i).getWeight() != null && !type.get(i).getWeight().isEmpty()) {
                                    myWtoday = Float.parseFloat(type.get(i).getWeight());
                                } else {
                                    myWtoday = 0.00;
                                }

                                prod_value_today.add(myPtoday);

                                eggToday = eggToday + myPtoday;
                                weightToday = weightToday + myWtoday;
                            }
                        }

                        //Log.d("eggToday","eggToday = "+eggToday);
                        //Log.d("weightToday","weightToday = "+weightToday);
                        //get egg price

                        ArrayList<LayerDataDashboard.EggSell> egg = layerdata.getEggSell();
                        if (egg != null && egg.size() > 0) {
                            for (int i = 0; i < egg.size(); i++) {
                                eggSell.add(Double.parseDouble(egg.get(i).getEggPrice()));
                            }
                        }

                        //Log.d("eggSell","eggSell = "+ eggSell);

                        ArrayList<LayerDataDashboard.PLastday> a = layerdata.getPLastday();
                        if (a != null && a.size() > 0) {
                            for (int i = 0; i < a.size(); i++) {

                                int myPlasttoday;
                                double myWlastday;

                                if (a.get(i).getEgg() != null && !a.get(i).getEgg().isEmpty()) {
                                    myPlasttoday = Integer.parseInt(a.get(i).getEgg());
                                } else {
                                    myPlasttoday = 0;
                                }

                                if (a.get(i).getWeight() != null && !a.get(i).getWeight().isEmpty()) {
                                    myWlastday = Double.parseDouble(a.get(i).getWeight());
                                } else {
                                    myWlastday = 0.00;
                                }


                                prod_value_lastday.add(myPlasttoday);
                                eggLastDay = eggLastDay + myPlasttoday;
                                weightLastDay = weightLastDay + myWlastday;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1w> b = layerdata.getP1w();
                        if (b != null && b.size() > 0) {
                            for (int i = 0; i < b.size(); i++) {

                                int myP1w;
                                double myW1w;

                                if (b.get(i).getEgg() != null && !b.get(i).getEgg().isEmpty()) {
                                    myP1w = Integer.parseInt(b.get(i).getEgg());
                                } else {
                                    myP1w = 0;
                                }

                                if (b.get(i).getWeight() != null && !b.get(i).getWeight().isEmpty()) {
                                    myW1w = Double.parseDouble(b.get(i).getWeight());
                                } else {
                                    myW1w = 0.00;
                                }

                                prod_value_week.add(myP1w);
                                egg1Week = egg1Week + myP1w;
                                weight1Week = weight1Week +myW1w;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1m> c = layerdata.getP1m();
                        if (c != null && c.size() > 0) {
                            for (int i = 0; i < c.size(); i++) {

                                int myP1m;
                                double myW1m;

                                if (c.get(i).getEgg() != null && !c.get(i).getEgg().isEmpty()) {
                                    myP1m = Integer.parseInt(c.get(i).getEgg());
                                } else {
                                    myP1m = 0;
                                }

                                if (c.get(i).getWeight() != null && !c.get(i).getWeight().isEmpty()) {
                                    myW1m = Double.parseDouble(c.get(i).getWeight());
                                } else {
                                    myW1m = 0.00;
                                }

                                prod_value_1month.add(myP1m);
                                egg1Month = egg1Month + myP1m;
                                weight1Month = weight1Month + myW1m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P3m> d = layerdata.getP3m();
                        if (d != null && d.size() > 0) {
                            for (int i = 0; i < type.size(); i++) {

                                int myP3m;
                                double myW3m;

                                if (d.get(i).getEgg() != null && !d.get(i).getEgg().isEmpty()) {
                                    myP3m = Integer.parseInt(d.get(i).getEgg());
                                } else {
                                    myP3m = 0;
                                }

                                if (d.get(i).getWeight() != null && !d.get(i).getWeight().isEmpty()) {
                                    myW3m = Double.parseDouble(d.get(i).getWeight());
                                } else {
                                    myW3m = 0.00;
                                }

                                prod_value_3month.add(myP3m);
                                egg3Month = egg3Month + myP3m;
                                weight3Month = weight6Month + myW3m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P6m> e = layerdata.getP6m();
                        if (e != null && e.size() > 0) {
                            for (int i = 0; i < e.size(); i++) {

                                int myP6m;
                                double myW6m;

                                if (e.get(i).getEgg() != null && !e.get(i).getEgg().isEmpty()) {
                                    myP6m = Integer.parseInt(e.get(i).getEgg());
                                } else {
                                    myP6m = 0;
                                }

                                if (e.get(i).getWeight() != null && !e.get(i).getWeight().isEmpty()) {
                                    myW6m = Double.parseDouble(e.get(i).getWeight());
                                } else {
                                    myW6m = 0.00;
                                }

                                prod_value_6month.add(myP6m);
                                egg6Month = egg6Month + myP6m;
                                weight6Month = weight6Month + myW6m;
                            }
                        }

                        ArrayList<LayerDataDashboard.P1y> f = layerdata.getP1y();
                        if (f != null && f.size() > 0) {
                            for (int i = 0; i < f.size(); i++) {

                                int myP1y;
                                double myW1y;

                                if (f.get(i).getEgg() != null && !f.get(i).getEgg().isEmpty()) {
                                    myP1y = Integer.parseInt(f.get(i).getEgg());
                                } else {
                                    myP1y = 0;
                                }

                                if (f.get(i).getWeight() != null && !f.get(i).getWeight().isEmpty()) {
                                    myW1y = Double.parseDouble(f.get(i).getWeight());
                                } else {
                                    myW1y = 0.00;
                                }

                                prod_value_1year.add(myP1y);
                                egg1Year = egg1Year + myP1y;
                                weight1Year = weight1Year + myW1y;
                            }
                        }

                        ArrayList<LayerDataDashboard.P2y> g = layerdata.getP2y();
                        if (g != null && g.size() > 0) {
                            for (int i = 0; i < g.size(); i++) {

                                int myP2y;
                                double myW2y;

                                if (f.get(i).getEgg() != null && !f.get(i).getEgg().isEmpty()) {
                                    myP2y = Integer.parseInt(f.get(i).getEgg());
                                } else {
                                    myP2y = 0;
                                }

                                if (f.get(i).getWeight() != null && !f.get(i).getWeight().isEmpty()) {
                                    myW2y = Double.parseDouble(f.get(i).getWeight());
                                } else {
                                    myW2y = 0.00;
                                }

                                prod_value_2year.add(myP2y);
                                egg2Year = egg2Year + myP2y;
                                weight2Year = weight2Year + myW2y;
                            }
                        }

                        ArrayList<LayerDataDashboard.PYtd> h = layerdata.getPYtd();
                        if (h != null && h.size() > 0) {
                            for (int i = 0; i < h.size(); i++) {

                                int myPYtd;
                                double myWPYtd;

                                if (h.get(i).getEgg() != null && !h.get(i).getEgg().isEmpty()) {
                                    myPYtd = Integer.parseInt(h.get(i).getEgg());
                                } else {
                                    myPYtd = 0;
                                }

                                if (h.get(i).getWeight() != null && !h.get(i).getWeight().isEmpty()) {
                                    myWPYtd = Double.parseDouble(h.get(i).getWeight());
                                } else {
                                    myWPYtd = 0.00;
                                }

                                prod_value_ytd.add(myPYtd);
                                eggYtd = eggYtd + myPYtd;
                                weightYtd = weightYtd + myWPYtd;
                            }
                        }

                        //Get Mortality Array Data
                        ArrayList<LayerDataDashboard.MToday> mv_today = layerdata.getMToday();
                        if (mv_today != null && mv_today.size() > 0) {
                            for (int i = 0; i < mv_today.size(); i++) {
                                mor_name.add(mv_today.get(i).getMortalityType());

                                int MToday;

                                if (mv_today.get(i).getMortalityValue() != null && !mv_today.get(i).getMortalityValue().isEmpty()) {
                                    MToday = Integer.parseInt(mv_today.get(i).getMortalityValue());
                                } else {
                                    MToday = 0;
                                }


                                mor_value_today.add(MToday);
                                morToday = morToday + MToday;
                            }
                        }

                        ArrayList<LayerDataDashboard.MLastday> mv_lastDay = layerdata.getMLastday();
                        if (mv_lastDay != null && mv_lastDay.size() > 0) {
                            for (int i = 0; i < mv_lastDay.size(); i++) {

                                int MLastday;

                                if (mv_lastDay.get(i).getMortalityValue() != null && !mv_lastDay.get(i).getMortalityValue().isEmpty()) {
                                    MLastday = Integer.parseInt(mv_lastDay.get(i).getMortalityValue());
                                } else {
                                    MLastday = 0;
                                }

                                mor_value_lastday.add(MLastday);
                                morLastDay = morLastDay + MLastday;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1w> mv_week = layerdata.getM1w();
                        if (mv_week != null && mv_week.size() > 0) {
                            for (int i = 0; i < mv_week.size(); i++) {

                                int M1w;

                                if (mv_week.get(i).getMortalityValue() != null && !mv_week.get(i).getMortalityValue().isEmpty()) {
                                    M1w = Integer.parseInt(mv_week.get(i).getMortalityValue());
                                } else {
                                    M1w = 0;
                                }

                                mor_value_week.add(M1w);
                                mor1Week = mor1Week + M1w;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1m> mv_1month = layerdata.getM1m();
                        if (mv_1month != null && mv_1month.size() > 0) {
                            for (int i = 0; i < mv_1month.size(); i++) {

                                int M1m;

                                if (mv_1month.get(i).getMortalityValue() != null && !mv_1month.get(i).getMortalityValue().isEmpty()) {
                                    M1m = Integer.parseInt(mv_1month.get(i).getMortalityValue());
                                } else {
                                    M1m = 0;
                                }

                                mor_value_1month.add(M1m);
                                mor1Month = mor1Month + M1m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M3m> mv_3month = layerdata.getM3m();
                        if (mv_3month != null && mv_3month.size() > 0) {
                            for (int i = 0; i < mv_3month.size(); i++) {

                                int M3m;

                                if (mv_3month.get(i).getMortalityValue() != null && !mv_3month.get(i).getMortalityValue().isEmpty()) {
                                    M3m = Integer.parseInt(mv_3month.get(i).getMortalityValue());
                                } else {
                                    M3m = 0;
                                }

                                mor_value_3month.add(M3m);
                                mor3Month = mor3Month + M3m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M6m> mv_6month = layerdata.getM6m();
                        if (mv_6month != null && mv_6month.size() > 0) {
                            for (int i = 0; i < mv_6month.size(); i++) {

                                int M6m;

                                if (mv_6month.get(i).getMortalityValue() != null && !mv_6month.get(i).getMortalityValue().isEmpty()) {
                                    M6m = Integer.parseInt(mv_6month.get(i).getMortalityValue());
                                } else {
                                    M6m = 0;
                                }

                                mor_value_6month.add(M6m);
                                mor6Month = mor6Month + M6m;
                            }
                        }

                        ArrayList<LayerDataDashboard.M1y> mv_1year = layerdata.getM1y();
                        if (mv_1year != null && mv_1year.size() > 0) {
                            for (int i = 0; i < mv_1year.size(); i++) {

                                int M1y;

                                if (mv_1year.get(i).getMortalityValue() != null && !mv_1year.get(i).getMortalityValue().isEmpty()) {
                                    M1y = Integer.parseInt(mv_1year.get(i).getMortalityValue());
                                } else {
                                    M1y = 0;
                                }

                                mor_value_1year.add(M1y);
                                mor1Year = mor1Year + M1y;
                            }
                        }

                        ArrayList<LayerDataDashboard.M2y> mv_2year = layerdata.getM2y();
                        if (mv_2year != null && mv_2year.size() > 0) {
                            for (int i = 0; i < mv_2year.size(); i++) {

                                int M2y;

                                if (mv_2year.get(i).getMortalityValue() != null && !mv_2year.get(i).getMortalityValue().isEmpty()) {
                                    M2y = Integer.parseInt(mv_2year.get(i).getMortalityValue());
                                } else {
                                    M2y = 0;
                                }

                                mor_value_2year.add(M2y);
                                mor2Year = mor2Year + M2y;
                            }
                        }

                        ArrayList<LayerDataDashboard.MYtd> mv_ytd = layerdata.getMYtd();
                        if (mv_ytd != null && mv_ytd.size() > 0) {
                            for (int i = 0; i < mv_ytd.size(); i++) {

                                int MYtd;

                                if (mv_ytd.get(i).getMortalityValue() != null && !mv_ytd.get(i).getMortalityValue().isEmpty()) {
                                    MYtd = Integer.parseInt(mv_ytd.get(i).getMortalityValue());
                                } else {
                                    MYtd = 0;
                                }

                                mor_value_ytd.add(MYtd);
                                morYtd = morYtd + MYtd;
                            }
                        }

                        txtLastRecording.setText(last_recording_date);
                        //setupViewPager(viewPager);
                        getPerformanceDetailData();
                        //Log.i("Response", map.toString());
/*
                           for (HashMap.Entry c : map.entrySet()) {
                               LayerDataDashboard layerdashboard = (LayerDataDashboard) c.getValue();
                               nid = layerdashboard.getLastRecording();
                               //Log.i("nid", nid);

                               // membuat objek array list
                               //ArrayList type_name = new ArrayList();
                               ArrayList<LayerDataDashboard.PToday> type = layerdashboard.getPToday();
                               for (int i = 0; i < type.size(); i++){
                                    type_name.add(type.get(i).getType());
                               }

                               Log.i("1", type_name.toString());
                               //getLayerData();
                             }*/
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                } else {
                    //Log.i("onEmptyResponse", "Returned empty response");
                    //Toast.makeText(this(),"Nothing returned",Toast.LENGTH_LONG).show();
                    AskOption();
                }



            }

            @Override
            public void onFailure(Call<LayerDataDashboard> call, Throwable t) {
                Log.d("Layerfarm", "Data failure = " + t.getMessage());

            }
        });


    }

    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:

                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }
    public String daysNameOfWeek(String inputDate){
        String daysName = null;
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);
        try {

            Date date = df.parse(inputDate.trim());
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            daysName  = outFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return daysName;
    }
    public ArrayList<String> getMyList(){
        ArrayList<String> nama = new ArrayList<String>();
        nama.add("Februari");//Adding object in arraylist
        nama.add("Maret");
        nama.add("April");
        nama.add("Mei");
        nama.add("Juni");
        return nama;
    }


    public String getLastRecording(){

        return nid;
    }

    //Return Array to Pie chart for Production
    public ArrayList<String> getQualityType(){

        //Log.i("a", type_name.toString());
        return type_name;
    }

    public ArrayList<String> getMortalityType(){

        //Log.i("a", mor_name.toString());
        return mor_name;
    }


    public ArrayList<Integer> getProductionValueToday(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_today;
    }

    public ArrayList<Integer> getProductionValueLast(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_lastday;
    }

    public ArrayList<Integer> getProductionValue1Week(){

        ///Log.i("a", mor_value_today.toString());
        return prod_value_week;
    }

    public ArrayList<Integer> getProductionValue1Month(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_1month;
    }

    public ArrayList<Integer> getProductionValue3Month(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_3month;
    }

    public ArrayList<Integer> getProductionValue6Month(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_6month;
    }

    public ArrayList<Integer> getProductionValue1Year(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_1year;
    }

    public ArrayList<Integer> getProductionValue2Year(){

        //Log.i("a", prod_value_2year.toString());
        return prod_value_2year;
    }

    public ArrayList<Integer> getProductionValueTYD(){

        //Log.i("a", mor_value_today.toString());
        return prod_value_ytd;
    }


    //Return Array to Pie chart for Mortality
    public ArrayList<Integer> getMortalityValueToday(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_today;
    }

    public ArrayList<Integer> getMortalityValuelastday(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_lastday;
    }

    public ArrayList<Integer> getMortalityValue1week(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_week;
    }

    public ArrayList<Integer> getMortalityValue1month(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_1month;
    }

    public ArrayList<Integer> getMortalityValue3month(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_3month;
    }

    public ArrayList<Integer> getMortalityValue6month(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_6month;
    }

    public ArrayList<Integer> getMortalityValue1year(){

        //Log.i("a", mor_value_today.toString());
        return mor_value_1year;
    }

    public ArrayList<Integer> getMortalityValue2year(){

        //Log.i("a", mor_value_2year.toString());
        return mor_value_2year;
    }

    public ArrayList<Integer> getMortalityValueytd(){
        //Log.i("a", mor_value_today.toString());
        return mor_value_ytd;
    }

    public Double getEggToday(){
        //Log.i("a", mor_value_today.toString());
        return eggToday;
    }

    public Double getEggLast(){
        //Log.i("a", mor_value_today.toString());
        return eggLastDay;
    }

    public Double getEgg1week(){
        //Log.i("a", mor_value_today.toString());
        return egg1Week;
    }

    public Double getEgg1month(){
        //Log.i("a", mor_value_today.toString());
        return egg1Month;
    }

    public Double getEgg3month(){
        //Log.i("a", mor_value_today.toString());
        return egg3Month;
    }

    public Double getEgg6month(){
        //Log.i("a", mor_value_today.toString());
        return egg6Month;
    }

    public Double getEgg1y(){
        //Log.i("a", mor_value_today.toString());
        return egg1Year;
    }

    public Double getEgg2year(){
        //Log.i("a", mor_value_today.toString());
        return egg2Year;
    }

    public Double getEggytd(){
        //Log.i("a", mor_value_today.toString());
        return eggYtd;
    }

    public Double getWeightToday(){
        Log.i("a", mor_value_today.toString());
        return weightToday;
    }

    public Double getWeightLast(){
        //Log.i("a", mor_value_today.toString());
        return weightLastDay;
    }

    public Double getWeight1week(){
        //Log.i("a", mor_value_today.toString());
        return weight1Week;
    }

    public Double getWeight1month(){
        //Log.i("a", mor_value_today.toString());
        return weight1Month;
    }

    public Double getWeight3month(){
        //Log.i("a", mor_value_today.toString());
        return weight3Month;
    }

    public Double getWeight6month(){
        //Log.i("a", mor_value_today.toString());
        return weight6Month;
    }

    public Double getWeight1y(){
        //Log.i("a", mor_value_today.toString());
        return weight1Year;
    }

    public Double getWeight2year(){
        //Log.i("a", mor_value_today.toString());
        return weight2Year;
    }

    public Double getWeightytd(){
        //Log.i("a", mor_value_today.toString());
        return weightYtd;
    }

    public Integer getMorToday(){
        //Log.i("a", mor_value_today.toString());
        return morToday;
    }

    public Integer getMorLastday(){
        //Log.i("a", mor_value_today.toString());
        return morLastDay;
    }

    public Integer getMor1week(){
        //Log.i("a", mor_value_today.toString());
        return mor1Week;
    }

    public Integer getMor1month(){
        //Log.i("a", mor_value_today.toString());
        return mor1Month;
    }

    public Integer getMor3month(){
        //Log.i("a", mor_value_today.toString());
        return mor3Month;
    }

    public Integer getMor6month(){
        //Log.i("a", mor_value_today.toString());
        return mor6Month;
    }

    public Integer getMor1year(){
        //Log.i("a", mor_value_today.toString());
        return mor1Year;
    }

    public Integer getMor2year(){
        //Log.i("a", mor_value_today.toString());
        return mor2Year;
    }

    public Integer getMorytd(){
        //Log.i("a", mor_value_today.toString());
        return morYtd;
    }

    public ArrayList<Double> getEggSell(){
        //Log.i("a", mor_value_today.toString());
        return eggSell;
    }

    public ArrayList<String> getIncome(){

        //Log.i("a", type_name.toString());
        return income;
    }

    public ArrayList<String> getExpense(){

        //Log.i("a", type_name.toString());
        return expense;
    }

    public ArrayList<String> getDate(){

        //Log.i("a", type_name.toString());
        return date_performance;
    }

    public ArrayList<Integer> getPopulation(){

        //Log.i("a", type_name.toString());
        return population;
    }

    public ArrayList<Integer> getEgglaidegg(){

        //Log.i("a", type_name.toString());
        return egglaidegg;
    }

    public ArrayList<Float> getEgglaidweight(){

        //Log.i("a", type_name.toString());
        return egglaidweight;
    }

    public ArrayList<Float> getFcr(){

        //Log.i("a", type_name.toString());
        return fcr;
    }

    public ArrayList<Integer> getMortality(){

        //Log.i("a", type_name.toString());
        return mortality;
    }

    public ArrayList<Float> getHenday(){

        //Log.i("a", type_name.toString());
        return henday;
    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Response Empty, Please Try Again!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

    private void getPerformanceDetailData(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_detail_dashboard_layer_data";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardLayerDetailModel> call = apiInterfaceJson.DashboardLayerDetailPerformance(token2, parameter);

        call.enqueue(new Callback<DashboardLayerDetailModel>() {
            @Override
            public void onResponse(Call<DashboardLayerDetailModel> call, Response<DashboardLayerDetailModel> response) {
                DashboardLayerDetailModel dashboardGrowerModel = response.body();
                DashboardLayerDetailModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
                //setupViewPager(viewPager);
                getPerformanceDetailProductionData();

            }

            @Override
            public void onFailure(Call<DashboardLayerDetailModel> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }


    private void getPerformanceDetailProductionData(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_detail_dashboard_layer_data_production";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardLayerDetailProductionModel> call = apiInterfaceJson.DashboardLayerDetailPerformanceProduction(token2, parameter);

        call.enqueue(new Callback<DashboardLayerDetailProductionModel>() {
            @Override
            public void onResponse(Call<DashboardLayerDetailProductionModel> call, Response<DashboardLayerDetailProductionModel> response) {
                DashboardLayerDetailProductionModel dashboardGrowerModelProduction = response.body();
                DashboardLayerDetailProductionModel.getInstance().setPerformance(dashboardGrowerModelProduction.getPerformance());
                loading.dismiss();
                setupViewPager(viewPager);

            }

            @Override
            public void onFailure(Call<DashboardLayerDetailProductionModel> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }

}
