package com.layerfarm.dashboard.adapter;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Fcr;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FcrAdapter extends RecyclerView.Adapter<FcrAdapter.ViewHolder>  {
    LineChart lineFcrCart ;
    private ArrayList<String> fcr;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;

    private ArrayList<Fcr> fcr_items;

    public FcrAdapter(ArrayList<Fcr> inputData){
        fcr_items = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        public LineChart lineChartFcr;

        public TextView egg_consume;
        public TextView feed_consume;
        //public CardView card;
        public ViewHolder(View v) {
            super(v);

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();



            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineChartFcr = (LineChart) v.findViewById(R.id.lineChartFcr);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            capacity = (TextView) v.findViewById(R.id.capacity);
            number_bird = (TextView) v.findViewById(R.id.bird_population);
            utilization = (TextView) v.findViewById(R.id.valueUtilization);
            egg_consume  = (TextView) v.findViewById(R.id.egglaidValue);
            feed_consume  = (TextView) v.findViewById(R.id.feedValue);

            //card = (CardView) v.findViewById(R.id.cv_flock);
        }
    }

    //public MortalityAdapter(@NonNull List<GraphListItemData> pData) {
    //    data = pData;
    //}

    @Override
    public FcrAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fcr_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        FcrAdapter.ViewHolder vh = new FcrAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(FcrAdapter.ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = fcr_items.get(position).getLocation_name();
        final String flockname = fcr_items.get(position).getFlock_name();
        final String flocktype = fcr_items.get(position).getFlock_type();
        final String flockperiod = fcr_items.get(position).getFlock_period();
        final String flockid = fcr_items.get(position).getFlock_id();
        //final String capacity = fcr_items.get(position).getCapacity();
        final String bird_population = fcr_items.get(position).getNumber_bird();
        //final String utilization = fcr_items.get(position).getUtility();

        final String lastRecording = help.getDateLastRecording(flockid);

        //feed total
        String Query= "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
                "from daily_recording\n" +
                "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
                "inner join flock on flock.id = daily_recording.flock_id \n" +
                "WHERE flock_id = '" + flockid + "' AND daily_recording.recording_date = '"+lastRecording +"'group by flock.id\n" +
                "ORDER by daily_recording.recording_date asc";

        Cursor cursor = database.rawQuery(Query, null);
        String total_pakan = "";
        if (cursor.moveToFirst()){
            total_pakan = cursor.getString(cursor.getColumnIndex("jumlah_pakan"));
        }
        cursor.close();

        //get egg total
        Cursor cursorEgg = database.rawQuery("SELECT recording_production.egg_weight as jumlah ,flock.name, daily_recording.recording_date from daily_recording\n" +
                "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                "inner join flock on flock.id = daily_recording.flock_id \n" +
                "WHERE flock_id = '" + flockid + "'AND daily_recording.recording_date = '"+lastRecording +"'  group by flock.id \n" +
                "ORDER by daily_recording.recording_date asc", null);
        String eggTotal = "";
        if (cursorEgg.moveToFirst())
            eggTotal = cursorEgg.getString(cursorEgg.getColumnIndex("jumlah"));
        cursorEgg.close();

        if (!eggTotal.isEmpty()) {
            double total_feed = Integer.parseInt(total_pakan);
            double total_telur = Integer.parseInt(eggTotal);
            double persentage = (total_feed / total_telur);
            DecimalFormat df = new DecimalFormat("#.###");

            // Displaying dot from HTML character code
            holder.dot.setText(Html.fromHtml("&#8226;"));
            holder.location_name.setText(location + "-" + flockname + "-" + "(" + flocktype + " " + flockperiod + ")");
            holder.valuePercentage.setText(df.format(persentage) + " %");
            holder.lastrecording.setText(lastRecording);
            holder.egg_consume.setText(eggTotal);
            holder.feed_consume.setText(total_pakan);
            //holder.utilization.setText(utilization);

        }


        //INISIALISASI DATA CHART
        ArrayList<Float> valueY = fcr(flockid);
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = help.ageXData(flockid,lastRecording);
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i), valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "FCR") ;
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineChartFcr.setTouchEnabled(true);
        holder.lineChartFcr.setDragDecelerationFrictionCoef(0.9f);
        holder.lineChartFcr.setDragEnabled(true);
        holder.lineChartFcr.getDescription().setEnabled(true);
        holder.lineChartFcr.setScaleEnabled(true);
        holder.lineChartFcr.setDrawGridBackground(false);
        holder.lineChartFcr.setHighlightPerDragEnabled(true);
        holder.lineChartFcr.setPinchZoom(true);
        holder.lineChartFcr.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineChartFcr.animateX(500);

        holder.lineChartFcr.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineChartFcr.notifyDataSetChanged();
        holder.lineChartFcr.setData(lineData);
        holder.lineChartFcr.invalidate();


        XAxis xAxis = holder.lineChartFcr.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineChartFcr.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineChartFcr.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineChartFcr.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        //lineDataSet1.setLineWidth(5f);
        //lineDataSet1.setCircleRadius(0f);
        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);

        //lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        //    LineData data = new LineData(lineDataSet1);


        //    holder.lineChart.setData(data);

        holder.lineChartFcr.getData().notifyDataChanged();
        holder.lineChartFcr.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return fcr_items.size();
    }


    public ArrayList<Float> fcr (String flockId){
        ArrayList<Float> fcrList = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String QueryTotalPakan = "";
        String QueryTotalTelur = "";
        if (!tgl_kosong.isEmpty()){
            //get feed total
            QueryTotalPakan = "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
                    "from daily_recording\n" +
                    "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' AND daily_recording.recording_date > '"+tgl_kosong +"'group by flock.id\n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur = "SELECT recording_production.egg_weight as jumlah_telur ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "'AND daily_recording.recording_date > '"+tgl_kosong +"'  group by flock.id \n" +
                    "ORDER by daily_recording.recording_date asc";


        }
        else {
            //get feed total
            QueryTotalPakan = "SELECT recording_feed_consumption.feed_amount as jumlah_pakan ,flock.name, daily_recording.recording_date \n" +
                    "from daily_recording\n" +
                    "inner join recording_feed_consumption on recording_feed_consumption.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur ="SELECT recording_production.egg_weight as jumlah_telur ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

        }

        Cursor cursorAyam = database.rawQuery(QueryTotalPakan, null);
        Cursor cursorTelur = database.rawQuery(QueryTotalTelur, null);

        cursorAyam.moveToFirst();
        cursorTelur.moveToFirst();

        for (int i=0; i < cursorAyam.getCount(); i++){
            cursorAyam.moveToPosition(i);
            String totalpakan = cursorAyam.getString(cursorAyam.getColumnIndex("jumlah_pakan"));

            cursorTelur.moveToPosition(i);
            String totaltelur = cursorTelur.getString(cursorTelur.getColumnIndex("jumlah_telur"));

            float total_egg = Integer.parseInt(totaltelur);
            float total_feed = Integer.parseInt(totalpakan);
            Log.d("TELUR", "" + total_egg);
            Log.d("AYAM", "" + total_feed);

            float hendaypercentage = (total_feed / total_egg);
            Log.d("PERSEN", "" + hendaypercentage);
            fcrList.add(hendaypercentage);

        }
        Log.d("fcrList", "" + fcrList);
        return fcrList;
    }

}
