package com.layerfarm.dashboard.adapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.recordingMortality;
import com.github.mikephil.charting.components.YAxis;
import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.layerfarm.DatabaseHelper;

public class MortalityItemsAdapter extends RecyclerView.Adapter<MortalityItemsAdapter.ViewHolder>  {
    LineChart lineMortalityCart ;
    private ArrayList<String> mortality;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    View view;

    private ArrayList <Report.Mortality> recordingMortalityItems;

    public MortalityItemsAdapter(ArrayList <Report.Mortality> inputData){
        recordingMortalityItems = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mortality_type;
        public TextView mortality_value;
        public ViewHolder(View v) {
            super(v);

            view = v;
            mortality_type = (TextView) v.findViewById(R.id.mortality_type);
            mortality_value = (TextView) v.findViewById(R.id.value);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mortality_category, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String name = recordingMortalityItems.get(position).getName();
        final String value = recordingMortalityItems.get(position).getValue();
        Log.d("layerfarm", "MortalityItemsAdapter name "+recordingMortalityItems.get(position).getName()+" value = "+recordingMortalityItems.get(position).getValue());
        // Displaying dot from HTML character code
        holder.mortality_type.setText(name);
        holder.mortality_value.setText(value);

    }

    @Override
    public int getItemCount() {
        return recordingMortalityItems.size();
    }

}
