package com.layerfarm.dashboard.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Feed;
import com.layerfarm.dashboard.model.Flock;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class MedicationVaccinationAdapter extends RecyclerView.Adapter<MedicationVaccinationAdapter.ViewHolder>{
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    View view;
    private ArrayList<Flock> Flock;
    public MedicationVaccinationAdapter(ArrayList<Flock> inputData){
        Flock = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView dot;
        public TextView valuePercentage;
        public ViewGroup OvkItems;
        ViewHolder holder;
        public ViewHolder(View v) {
            super(v);
            view = v;
            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            dataSource =  new DBDataSource(v.getContext());
            dataSource.open();

            context = v.getContext();

            location_name = (TextView) v.findViewById(R.id.location);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            OvkItems = (ViewGroup) v.findViewById(R.id.content);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String location = Flock.get(position).getLocation_name();
        final String flockname = Flock.get(position).getFlock_name();
        final String flocktype = Flock.get(position).getFlock_type();
        final String flockperiod = Flock.get(position).getFlock_period();
        final String flockid = Flock.get(position).getFlockId();

        final String lastRecording = help.getDateLastRecording(flockid);
        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");

        holder.lastrecording.setText(lastRecording);

        String Query= "SELECT daily_recording_id FROM recording_medication_vaccination INNER JOIN daily_recording on daily_recording.id = recording_medication_vaccination.daily_recording_id\n" +
                "WHERE flock_id = '"+flockid+"' ORDER BY recording_date DESC LIMIT 1";
        Cursor cursor = database.rawQuery(Query, null);
        String id = "";
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex("daily_recording_id"));
        }
        cursor.close();
        //jika id ada isinya
        if (!id.isEmpty()){
            String QueryFeed= "SELECT product_name, amount_used FROM recording_medication_vaccination INNER JOIN daily_recording on daily_recording.id = recording_medication_vaccination.daily_recording_id\n" +
                    "INNER JOIN medication_vaccination on medication_vaccination.id = recording_medication_vaccination.medication_vaccination_id\n" +
                    "WHERE flock_id = '"+flockid+"' AND daily_recording_id = '"+id+"'";
            Cursor cursorFeed = database.rawQuery(QueryFeed, null);
            String name= "";
            String amount ="";
            cursorFeed.moveToFirst();
            for (int i=0; i< cursorFeed.getCount(); i++){
                cursorFeed.moveToPosition(i);
                name = cursorFeed.getString(cursorFeed.getColumnIndex("product_name"));
                amount = cursorFeed.getString(cursorFeed.getColumnIndex("amount_used"));
                AddFeed(holder,name,amount);
            }
            cursorFeed.close();

        }
    }
    public void AddFeed(ViewHolder holder, String name, String amount){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View View = inflater.inflate(R.layout.feed_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView feedName = (TextView) View.findViewById(R.id.name);
        TextView feedAmount = (TextView) View.findViewById(R.id.amount_used);
        feedName.setText("Name : "+name);
        feedAmount.setText("Amount Used : "+amount);
        holder.OvkItems.addView(View, holder.OvkItems.getChildCount());
    }

    @Override
    public int getItemCount() {
        return Flock.size();
    }
}
