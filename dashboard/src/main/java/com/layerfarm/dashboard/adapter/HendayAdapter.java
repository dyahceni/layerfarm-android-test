package com.layerfarm.dashboard.adapter;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Henday;
import com.layerfarm.dashboard.model.recordingMortality;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class HendayAdapter extends RecyclerView.Adapter<HendayAdapter.ViewHolder>  {
    LineChart lineHendayCart ;
    private ArrayList<String> henday;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;

    private ArrayList<Henday> hendayItems;

    public HendayAdapter(ArrayList<Henday> inputData){
        hendayItems = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView egglaid;
        public TextView population;
        public LineChart lineHendayCart;
        //public CardView card;
        public ViewHolder(View v) {
            super(v);

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineHendayCart = (LineChart) v.findViewById(R.id.lineChartHenday);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            egglaid = (TextView) v.findViewById(R.id.egglaidValue);
            population = (TextView) v.findViewById(R.id.populationValue);


            //card = (CardView) v.findViewById(R.id.cv_flock);
        }
    }

    //public MortalityAdapter(@NonNull List<GraphListItemData> pData) {
    //    data = pData;
    //}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.henday_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = hendayItems.get(position).getLocation_name();
        final String flockname = hendayItems.get(position).getFlock_name();
        final String flocktype = hendayItems.get(position).getFlock_type();
        final String flockperiod = hendayItems.get(position).getFlock_period();
        final String flockid = hendayItems.get(position).getFlock_id();

        final String lastRecording = help.getDateLastRecording(flockid);

        //get chick total
        Cursor cursorChick = database.rawQuery("SELECT sum(cache_total_bird.total_bird) as total_ayam, daily_recording.recording_date, flock.name FROM cache_total_bird \n" +
                "INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
                "inner join flock on flock.id = daily_recording.flock_id\n" +
                "WHERE daily_recording.flock_id = '" + flockid + "' AND daily_recording.recording_date = '"+lastRecording +"'", null);
        String chickTotal = "";
        if (cursorChick.moveToFirst())
            chickTotal = cursorChick.getString(cursorChick.getColumnIndex("total_ayam"));
        cursorChick.close();
        Log.d("jumlah ayam", "" + chickTotal);

        //get egg total
        Cursor cursorEgg = database.rawQuery("SELECT sum(recording_production.egg_number) as jumlah ,flock.name, daily_recording.recording_date from daily_recording\n" +
                "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                "inner join flock on flock.id = daily_recording.flock_id \n" +
                "WHERE flock_id = '" + flockid + "' AND recording_date = '"+lastRecording +"'group by flock.id \n" +
                "ORDER by daily_recording.recording_date asc", null);
        String eggTotal = "";
        if (cursorEgg.moveToFirst())
            eggTotal = cursorEgg.getString(cursorEgg.getColumnIndex("jumlah"));
        cursorEgg.close();


        if (!eggTotal.isEmpty()){
            double total_egg = Integer.parseInt(eggTotal);
        double total_population = Integer.parseInt(chickTotal);
        double persentage = (total_egg / total_population) * 100;
        DecimalFormat df = new DecimalFormat("#.###");

        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" + "(" + flocktype + " " + flockperiod + ")");
        holder.valuePercentage.setText(df.format(persentage) + " %");
        holder.lastrecording.setText(lastRecording);
        holder.egglaid.setText(eggTotal);
        holder.population.setText(chickTotal);
    }

        //INISIALISASI DATA CHART
        ArrayList<Float> valueY = henday(flockid);
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = help.ageXData(flockid,lastRecording);
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i), valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "Henday") ;
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineHendayCart.setTouchEnabled(true);
        holder.lineHendayCart.setDragDecelerationFrictionCoef(0.9f);
        holder.lineHendayCart.setDragEnabled(true);
        holder.lineHendayCart.getDescription().setEnabled(true);
        holder.lineHendayCart.setScaleEnabled(true);
        holder.lineHendayCart.setDrawGridBackground(false);
        holder.lineHendayCart.setHighlightPerDragEnabled(true);
        holder.lineHendayCart.setPinchZoom(true);
        holder.lineHendayCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineHendayCart.animateX(500);

        holder.lineHendayCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineHendayCart.notifyDataSetChanged();
        holder.lineHendayCart.setData(lineData);
        holder.lineHendayCart.invalidate();


        XAxis xAxis = holder.lineHendayCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineHendayCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineHendayCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineHendayCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        //lineDataSet1.setLineWidth(5f);
        //lineDataSet1.setCircleRadius(0f);
        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);

        //lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        //    LineData data = new LineData(lineDataSet1);


        //    holder.lineChart.setData(data);

        holder.lineHendayCart.getData().notifyDataChanged();
        holder.lineHendayCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return hendayItems.size();
    }

    public ArrayList<Float> henday (String flockId){
        ArrayList<Float> hendaylist = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String QueryTotalAyam = "";
        String QueryTotalTelur = "";
        if (!tgl_kosong.isEmpty()){
            //get chick total
            QueryTotalAyam = "SELECT sum(cache_total_bird.total_bird) as total_ayam, daily_recording.recording_date, flock.name FROM cache_total_bird\n" +
                    "INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id\n" +
                    "WHERE daily_recording.flock_id = '" + flockId + "' and  daily_recording.recording_date > '"+tgl_kosong+"' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur = "SELECT sum(recording_production.egg_number) as jumlah ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' and daily_recording.recording_date > '"+tgl_kosong+"' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";


        }
        else {
            //get chick total
            QueryTotalAyam = "SELECT sum(cache_total_bird.total_bird) as total_ayam, daily_recording.recording_date, flock.name FROM cache_total_bird\n" +
                    "INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id\n" +
                    "WHERE daily_recording.flock_id  = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

            //get egg total
            QueryTotalTelur ="SELECT sum(recording_production.egg_number) as jumlah ,flock.name, daily_recording.recording_date from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '" + flockId + "' group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

        }

        Cursor cursorAyam = database.rawQuery(QueryTotalAyam, null);
        Cursor cursorTelur = database.rawQuery(QueryTotalTelur, null);

        cursorAyam.moveToFirst();
        cursorTelur.moveToFirst();

        for (int i=0; i < cursorAyam.getCount(); i++){
            cursorAyam.moveToPosition(i);
            String totalayam = cursorAyam.getString(cursorAyam.getColumnIndex("total_ayam"));

            cursorTelur.moveToPosition(i);
            String totaltelur = cursorTelur.getString(cursorTelur.getColumnIndex("jumlah"));



            float total_egg = Integer.parseInt(totaltelur);
            float total_population = Integer.parseInt(totalayam);
            Log.d("TELUR", "" + total_egg);
            Log.d("AYAM", "" + total_population);

            float hendaypercentage = (total_egg / total_population) * 100;
            Log.d("PERSEN", "" + hendaypercentage);
            hendaylist.add(hendaypercentage);

        }
        Log.d("hendaylist", "" + hendaylist);
        return hendaylist;
    }

}
