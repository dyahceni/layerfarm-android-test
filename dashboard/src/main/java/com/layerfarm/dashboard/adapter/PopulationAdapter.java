package com.layerfarm.dashboard.adapter;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Population;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PopulationAdapter extends RecyclerView.Adapter<PopulationAdapter.ViewHolder>  {
    LineChart linePopulationCart ;
    private ArrayList<String> population;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;

    private ArrayList<Population> recording_population_items;

    public PopulationAdapter(ArrayList<Population> inputData){
        recording_population_items = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        public LineChart linePopulationCart;
        //public CardView card;
        public ViewHolder(View v) {
            super(v);

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();



            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            linePopulationCart = (LineChart) v.findViewById(R.id.lineChartpopulation);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            capacity = (TextView) v.findViewById(R.id.capacity);
            number_bird = (TextView) v.findViewById(R.id.bird_population);
            utilization = (TextView) v.findViewById(R.id.valueUtilization);

            //card = (CardView) v.findViewById(R.id.cv_flock);
        }
    }

    //public MortalityAdapter(@NonNull List<GraphListItemData> pData) {
    //    data = pData;
    //}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.population_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = recording_population_items.get(position).getLocation_name();
        final String flockname = recording_population_items.get(position).getFlock_name();
        final String flocktype = recording_population_items.get(position).getFlock_type();
        final String flockperiod = recording_population_items.get(position).getFlock_period();
        final String flockid = recording_population_items.get(position).getFlock_id();
        final String capacity = recording_population_items.get(position).getCapacity();
        final String bird_population = recording_population_items.get(position).getNumber_bird();
        final String utilization = recording_population_items.get(position).getUtility();

        final String lastRecording = help.getDateLastRecording(flockid);
        String Query= "SELECT sum(cache_total_bird.total_bird) as total_ayam, daily_recording.recording_date, flock.name FROM cache_total_bird \n" +
                "INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
                "inner join flock on flock.id = daily_recording.flock_id\n" +
                "WHERE daily_recording.flock_id = '"+flockid+"' AND daily_recording.recording_date = '"+lastRecording +"'";


        Cursor cursor = database.rawQuery(Query, null);
        String total_ayam = "";
        if (cursor.moveToFirst()){
            total_ayam = cursor.getString(cursor.getColumnIndex("total_ayam"));
        }
        cursor.close();

        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");
        holder.valuePercentage.setText(total_ayam);
        holder.lastrecording.setText(lastRecording);
        holder.capacity.setText(capacity);
        holder.number_bird.setText(bird_population);
        holder.utilization.setText(utilization);

        //INISIALISASI DATA CHART
        ArrayList<Integer> valueY = population(flockid);
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = help.ageXData(flockid,lastRecording);
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i), valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "POPULATION") ;
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.linePopulationCart.setTouchEnabled(true);
        holder.linePopulationCart.setDragDecelerationFrictionCoef(0.9f);
        holder.linePopulationCart.setDragEnabled(true);
        holder.linePopulationCart.getDescription().setEnabled(true);
        holder.linePopulationCart.setScaleEnabled(true);
        holder.linePopulationCart.setDrawGridBackground(false);
        holder.linePopulationCart.setHighlightPerDragEnabled(true);
        holder.linePopulationCart.setPinchZoom(true);
        holder.linePopulationCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.linePopulationCart.animateX(500);

        holder.linePopulationCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.linePopulationCart.notifyDataSetChanged();
        holder.linePopulationCart.setData(lineData);
        holder.linePopulationCart.invalidate();


        XAxis xAxis = holder.linePopulationCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.linePopulationCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.linePopulationCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.linePopulationCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        //lineDataSet1.setLineWidth(5f);
        //lineDataSet1.setCircleRadius(0f);
        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);

        //lineDataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        //    LineData data = new LineData(lineDataSet1);


        //    holder.lineChart.setData(data);

        holder.linePopulationCart.getData().notifyDataChanged();
        holder.linePopulationCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return recording_population_items.size();
    }


    public ArrayList<Integer> population (String flockId){
        ArrayList<Integer> population = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String Query = "";
        if (!tgl_kosong.isEmpty()){
            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"' AND recording_date > '"+tgl_kosong+"'";

        }
        else {
            Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"'";
        }
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String total = cursor.getString(cursor.getColumnIndex("total_bird"));
            int qty = Integer.parseInt(total);
            population.add(qty);
        }
        return population;
    }


}
