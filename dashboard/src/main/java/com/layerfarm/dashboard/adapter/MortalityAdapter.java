package com.layerfarm.dashboard.adapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.recordingMortality;
import com.github.mikephil.charting.components.YAxis;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.layerfarm.DatabaseHelper;

public class MortalityAdapter extends RecyclerView.Adapter<MortalityAdapter.ViewHolder>  {
    LineChart lineMortalityCart ;
    private ArrayList<String> mortality;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    View view;

    private ArrayList<recordingMortality> recordingMortalityItems;

    public MortalityAdapter(ArrayList<recordingMortality> inputData){
        recordingMortalityItems = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView dot;
        public TextView valuePercentage;
        //public TextView mortalityType, mortalityValue;
        public LineChart lineMortalityCart;
        public ViewGroup leftLinear, rightLinear;
        ViewHolder holder;
        public ViewHolder(View v) {
            super(v);

            view = v;

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            dataSource =  new DBDataSource(v.getContext());
            dataSource.open();

            context = v.getContext();

            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineMortalityCart = (LineChart) v.findViewById(R.id.lineChart);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            leftLinear = (ViewGroup) v.findViewById(R.id.linear_left);
            rightLinear = (ViewGroup) v.findViewById(R.id.linear_right);
            //mortalityType = (TextView) v.findViewById(R.id.mortality_type);
            //mortalityValue = (TextView) v.findViewById(R.id.value);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mortality_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = recordingMortalityItems.get(position).getLocation_name();
        final String flockname = recordingMortalityItems.get(position).getFlock_name();
        final String flocktype = recordingMortalityItems.get(position).getFlock_type();
        final String flockperiod = recordingMortalityItems.get(position).getFlock_period();
        final String flockid = recordingMortalityItems.get(position).getFlockId();

        final String lastRecording = help.getDateLastRecording(flockid);
        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");

        holder.lastrecording.setText(lastRecording);

        String Query= "SELECT * FROM cache_mortality INNER JOIN daily_recording on daily_recording.id = cache_mortality.daily_recording_id \n" +
                "WHERE flock_id = '"+flockid+"' AND recording_date = '"+lastRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String value = "";
        if (cursor.moveToFirst()){
            value = cursor.getString(cursor.getColumnIndex("value"));
        }
        cursor.close();
        String[] mor ;
        if (!value.isEmpty()){
            mor = value.split("/");
            String[] total_mortality = mor[0].split("_");
            double total_value = Integer.parseInt(total_mortality[1]);
            double total_population = help.getTotalBird(flockid, lastRecording);
            double persentage = (total_value / total_population)*100;
            DecimalFormat df = new DecimalFormat("#.###");
            holder.valuePercentage.setText(df.format(persentage) + " %");

            ArrayList<Mortality> mortalities = new ArrayList<>();
            for (int i = 1; i < mor.length; i++){
                Mortality mortality = new Mortality();
                String[] sp = mor[i].split("_");
                String name = getMortalityCategory(Integer.parseInt(sp[0]));
                String die = sp[1];
                mortality.setName(name);
                mortality.setValue(die);
                mortalities.add(mortality);
            }

            int left = mortalities.size() / 2;
            int mod = mortalities.size() % 2;
            int val_left = left+mod;

            for (int a = 0; a<mortalities.size(); a++){
                if (a<val_left)
                    AddLeft(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
                else
                    AddRight(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
            }
        }


        //INISIALISASI DATA CHART
        ArrayList<Integer> valueY = mortality(flockid);
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = help.ageXData(flockid,lastRecording);
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i), valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "MORTALITY") ;
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineMortalityCart.setTouchEnabled(true);
        holder.lineMortalityCart.setDragDecelerationFrictionCoef(0.9f);
        holder.lineMortalityCart.setDragEnabled(true);
        holder.lineMortalityCart.getDescription().setEnabled(true);
        holder.lineMortalityCart.setScaleEnabled(true);
        holder.lineMortalityCart.setDrawGridBackground(false);
        holder.lineMortalityCart.setHighlightPerDragEnabled(true);
        holder.lineMortalityCart.setPinchZoom(true);
        holder.lineMortalityCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineMortalityCart.animateX(500);

        holder.lineMortalityCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineMortalityCart.notifyDataSetChanged();
        holder.lineMortalityCart.setData(lineData);
        holder.lineMortalityCart.invalidate();


        XAxis xAxis = holder.lineMortalityCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineMortalityCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineMortalityCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineMortalityCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);

        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);

        holder.lineMortalityCart.getData().notifyDataChanged();
        holder.lineMortalityCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return recordingMortalityItems.size();
    }

    public ArrayList<Integer> mortality (String flockId){
        ArrayList<Integer> mortality = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String Query = "";
        if (!tgl_kosong.isEmpty()){
            Query= "SELECT * FROM cache_mortality INNER JOIN daily_recording on daily_recording.id = cache_mortality.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"' AND recording_date > '"+tgl_kosong+"'";

        }
        else {
            Query= "SELECT * FROM cache_mortality INNER JOIN daily_recording on daily_recording.id = cache_mortality.daily_recording_id \n" +
                    "WHERE flock_id = '"+flockId+"'";
        }
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String val = cursor.getString(cursor.getColumnIndex("value"));
            String[] arr = val.split("/");
            String[] total = arr[0].split("_");
            int qty = Integer.parseInt(total[1]);
            mortality.add(qty);
        }
        return mortality;
    }
    public void AddLeft(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.mortality_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.mortality_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.leftLinear.addView(leftView, holder.leftLinear.getChildCount());
    }
    public void AddRight(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.mortality_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.mortality_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.rightLinear.addView(leftView, holder.rightLinear.getChildCount());
    }
    public String getMortalityCategory(int id){
        String Query = "SELECT * FROM mortality_category where id = '"+id+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String name = "";
        if(cursor.moveToFirst())
            name = cursor.getString(cursor.getColumnIndex("name"));
        return name;
    }
}
