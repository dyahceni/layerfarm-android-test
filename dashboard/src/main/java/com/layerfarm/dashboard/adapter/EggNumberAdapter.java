package com.layerfarm.dashboard.adapter;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.EggNumber;
import com.github.mikephil.charting.components.YAxis;
import com.layerfarm.recording.model.Mortality.eggNumberModel;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.layerfarm.DatabaseHelper;

public class EggNumberAdapter extends RecyclerView.Adapter<EggNumberAdapter.ViewHolder>  {
    LineChart lineEggNumberCart ;
    private ArrayList<String> eggType;
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    View view;

    private ArrayList<EggNumber> recordingEggNumberItems;

    public EggNumberAdapter(ArrayList<EggNumber> inputData){
        recordingEggNumberItems = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView dot;
        public TextView valuePercentage;
        //public TextView mortalityType, mortalityValue;
        public LineChart lineEggNumberCart;
        public ViewGroup leftLinear, rightLinear;
        ViewHolder holder;
        public ViewHolder(View v) {
            super(v);

            view = v;

            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            dataSource =  new DBDataSource(v.getContext());
            dataSource.open();

            context = v.getContext();

            location_name = (TextView) v.findViewById(R.id.Location_name);
            valuePercentage = (TextView) v.findViewById(R.id.valuePercentage);
            lineEggNumberCart = (LineChart) v.findViewById(R.id.lineChartEggNumber);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            leftLinear = (ViewGroup) v.findViewById(R.id.linear_left);
            rightLinear = (ViewGroup) v.findViewById(R.id.linear_right);
            //mortalityType = (TextView) v.findViewById(R.id.mortality_type);
            //mortalityValue = (TextView) v.findViewById(R.id.value);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.egg_number_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        //final String id = chickin_List_items.get(position).getId();
        final String location = recordingEggNumberItems.get(position).getLocation_name();
        final String flockname = recordingEggNumberItems.get(position).getFlock_name();
        final String flocktype = recordingEggNumberItems.get(position).getFlock_type();
        final String flockperiod = recordingEggNumberItems.get(position).getFlock_period();
        final String flockid = recordingEggNumberItems.get(position).getFlock_id();

        final String lastRecording = help.getDateLastRecording(flockid);
        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");

        holder.lastrecording.setText(lastRecording);

        String Query= "SELECT sum(recording_production.egg_number) telur_pecah, egg_quality.name, flock.name FROM recording_production \n" +
                "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                "INNER JOIN daily_recording on daily_recording.id = recording_production.daily_recording_id\n" +
                "INNER join flock on flock.id = daily_recording.flock_id \n" +
                "WHERE flock_id = '"+flockid+"' and egg_quality.name like 'cracked' AND recording_date = '"+lastRecording+"'  GROUP BY flock.id ";
        Cursor cursor = database.rawQuery(Query, null);
        String telur_pecah = "";
        if (cursor.moveToFirst()){
            telur_pecah = cursor.getString(cursor.getColumnIndex("telur_pecah"));
        }

        String[] egg ;
        if (!telur_pecah.isEmpty()){
            //Set total telur pecah
            double total_telur_pecah = Integer.parseInt(telur_pecah);
            holder.valuePercentage.setText(total_telur_pecah + " Cracked");

            //get date from table and create array

            ArrayList<eggNumberModel> eggarray = new ArrayList<>();

            String selectQuery= "SELECT egg_quality.id,sum(recording_production.egg_number) as jumlah_telur, egg_quality.name FROM recording_production \n" +
            "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                    "INNER JOIN daily_recording on daily_recording.id = recording_production.daily_recording_id\n" +
                    "INNER join flock on flock.id = daily_recording.flock_id\n" +
                    "WHERE flock_id = '"+flockid+"' AND recording_date = '"+lastRecording+"' GROUP by egg_quality.id";
            Cursor c = database.rawQuery(selectQuery, null);
            String name = "";
            String amount = "";
            c.moveToFirst();

            for (int i=0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                eggNumberModel eggnum = new eggNumberModel();
                name = c.getString(2);
                amount = c.getString(1);
                eggnum.setQuality(name);
                eggnum.setValue(amount);
                eggarray.add(eggnum);
            }

            int left = eggarray.size() / 2;
            int mod = eggarray.size() % 2;
            int val_left = left+mod;

            for (int a = 0; a<eggarray.size(); a++){
                Log.d("nama","nama:"+eggarray.get(a).getQuality());
                Log.d("telur","telur:"+eggarray.get(a).getValue());
                if (a<val_left)
                    AddLeft(holder, eggarray.get(a).getQuality(),eggarray.get(a).getValue());
                else
                    AddRight(holder, eggarray.get(a).getQuality(),eggarray.get(a).getValue());
            }

            /*

            String QUERY_TELUR  = "SELECT egg_quality.id,sum(recording_production.egg_number) jumlah_telur, egg_quality.name FROM recording_production \n" +
                    "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                    "INNER JOIN daily_recording on daily_recording.id = recording_production.daily_recording_id\n" +
                    "INNER join flock on flock.id = daily_recording.flock_id\n" +
                    "where flock.id = 2 GROUP by egg_quality.id";

            Cursor c = database.rawQuery(QUERY_TELUR,null);
            ArrayList<eggNumberModel> array = new ArrayList<eggNumberModel>();

            String name = "";
            String value = "";
            eggNumberModel eggs = new eggNumberModel();
            if(cursor.moveToFirst()) {

                 name = c.getString(c.getColumnIndex("name"));
                 value = c.getString(c.getColumnIndex("telur_pecah"));


            }

            eggs.setQuality(name);
            eggs.setValue(value);
            array.add(eggs);

            int left = array.size() / 2;
            int mod = array.size() % 2;
            int val_left = left+mod;

            for (int a = 0; a<array.size(); a++){
                Log.d("nama","nama:"+array.get(a).getQuality());
                Log.d("telur","telur:"+array.get(a).getValue());
                if (a<val_left)
                    AddLeft(holder, array.get(a).getQuality(),array.get(a).getValue());
                else
                    AddRight(holder, array.get(a).getQuality(),array.get(a).getValue());
            }
            /*
            int left = mortalities.size() / 2;
            int mod = mortalities.size() % 2;
            int val_left = left+mod;

            for (int a = 0; a<mortalities.size(); a++){
                if (a<val_left)
                    AddLeft(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
                else
                    AddRight(holder, mortalities.get(a).getName(),mortalities.get(a).getValue());
            }

            */
        }


        //INISIALISASI DATA CHART
        ArrayList<Integer> valueY = eggNumber(flockid);
        List<Entry> set2 = new ArrayList<Entry>() ;
        ArrayList<Integer> valueX = help.ageXData(flockid,lastRecording);
        for (int i = 0; i < valueY.size(); i++) {
            set2.add(new Entry(valueX.get(i), valueY.get(i)));
        }
        LineDataSet lineDataSet1 = new LineDataSet(set2 , "EGG NUMBER") ;
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setCircleColor(Color.BLACK);
        lineDataSet1.setLineWidth(1f);
        lineDataSet1.setCircleRadius(3f);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setValueTextSize(9f);
        lineDataSet1.setDrawFilled(true);

        List<ILineDataSet> list = new ArrayList<ILineDataSet>() ;
        //list.add(lineDataSet) ;
        list.add(lineDataSet1) ;
        LineData lineData = new LineData(list) ;
        holder.lineEggNumberCart.setTouchEnabled(true);
        holder.lineEggNumberCart.setDragDecelerationFrictionCoef(0.9f);
        holder.lineEggNumberCart.setDragEnabled(true);
        holder.lineEggNumberCart.getDescription().setEnabled(true);
        holder.lineEggNumberCart.setScaleEnabled(true);
        holder.lineEggNumberCart.setDrawGridBackground(false);
        holder.lineEggNumberCart.setHighlightPerDragEnabled(true);
        holder.lineEggNumberCart.setPinchZoom(true);
        holder.lineEggNumberCart.setAutoScaleMinMaxEnabled(true);
        //holder.lineChart.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        holder.lineEggNumberCart.animateX(500);

        holder.lineEggNumberCart.setLayerType(View.LAYER_TYPE_NONE, null);

        holder.lineEggNumberCart.notifyDataSetChanged();
        holder.lineEggNumberCart.setData(lineData);
        holder.lineEggNumberCart.invalidate();


        XAxis xAxis = holder.lineEggNumberCart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.lineEggNumberCart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setEnabled(true);

        YAxis rightAxis = holder.lineEggNumberCart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);
        rightAxis.setDrawZeroLine(true);
        rightAxis.setGranularityEnabled(true);


        Legend l = holder.lineEggNumberCart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.BLUE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);



        lineDataSet1.setAxisDependency(YAxis.AxisDependency.RIGHT);

        lineDataSet1.setFillAlpha(60);
        lineDataSet1.setDrawCircleHole(false);

        holder.lineEggNumberCart.getData().notifyDataChanged();
        holder.lineEggNumberCart.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return recordingEggNumberItems.size();
    }

    public ArrayList<Integer> eggNumber (String flockId){
        ArrayList<Integer> eggnumber = new ArrayList<>();
        String tgl_kosong = help.getDateFlockEmpty(flockId,"");
        String Query = "";
        if (!tgl_kosong.isEmpty()){
            Query= "SELECT sum(recording_production.egg_number) as jumlah ,flock.name, daily_recording.recording_date, flock.id from daily_recording\n" +
            "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "WHERE flock_id = '"+flockId+"' AND recording_date > '"+tgl_kosong+"'group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";

        }
        else {
            Query= "SELECT sum(recording_production.egg_number) as jumlah ,flock.name, daily_recording.recording_date, flock.id from daily_recording\n" +
                    "inner join recording_production on recording_production.daily_recording_id = daily_recording.id\n" +
                    "inner join flock on flock.id = daily_recording.flock_id \n" +
                    "where flock_id = '"+flockId+ "'group by flock.id, daily_recording.recording_date \n" +
                    "ORDER by daily_recording.recording_date asc";
        }
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String val = cursor.getString(cursor.getColumnIndex("jumlah"));
            int qty = Integer.parseInt(val);
            eggnumber.add(qty);
        }
        return eggnumber;
    }
    public void AddLeft(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.egg_number_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.egg_number_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.leftLinear.addView(leftView, holder.leftLinear.getChildCount());
    }
    public void AddRight(ViewHolder holder, String type, String value){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View leftView = inflater.inflate(R.layout.egg_number_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView mortalityType = (TextView) leftView.findViewById(R.id.egg_number_type);
        TextView mortalityValue = (TextView) leftView.findViewById(R.id.value);
        mortalityType.setText(type);
        mortalityValue.setText(value);
        holder.rightLinear.addView(leftView, holder.rightLinear.getChildCount());
    }
    public String getEggQuality(int id){
        String Query = "SELECT * FROM egg_quality where id = '"+id+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String name = "";
        if(cursor.moveToFirst())
            name = cursor.getString(cursor.getColumnIndex("name"));
        return name;
    }
}
