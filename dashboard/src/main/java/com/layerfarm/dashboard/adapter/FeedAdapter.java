package com.layerfarm.dashboard.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.layerfarm.dashboard.R;
import com.layerfarm.dashboard.model.Feed;
import com.layerfarm.dashboard.model.recordingMortality;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{
    private helper help;
    private SQLiteDatabase database;
    DatabaseHelper db;
    DBDataSource dataSource;
    private Context context;
    View view;
    private ArrayList<Feed> recordingFeedItems;
    public FeedAdapter(ArrayList<Feed> inputData){
        recordingFeedItems = inputData;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView location_name;
        public TextView lastrecording;
        public TextView dot;
        public TextView valuePercentage;
        public ViewGroup feedItems;
        ViewHolder holder;
        public ViewHolder(View v) {
            super(v);
            view = v;
            help = new helper(v.getContext());
            help.open();
            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();

            dataSource =  new DBDataSource(v.getContext());
            dataSource.open();

            context = v.getContext();

            location_name = (TextView) v.findViewById(R.id.location);
            dot = (TextView) v.findViewById(R.id.dot);
            lastrecording = (TextView) v.findViewById(R.id.lastRecording);
            feedItems = (ViewGroup) v.findViewById(R.id.content);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String location = recordingFeedItems.get(position).getLocation_name();
        final String flockname = recordingFeedItems.get(position).getFlock_name();
        final String flocktype = recordingFeedItems.get(position).getFlock_type();
        final String flockperiod = recordingFeedItems.get(position).getFlock_period();
        final String flockid = recordingFeedItems.get(position).getFlockId();

        Log.d("feed","adapter loc ="+location);
        Log.d("feed","adapter flock ="+flockname);

        final String lastRecording = help.getDateLastRecording(flockid);
        // Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.location_name.setText(location + "-" + flockname + "-" +"(" +flocktype +" "+flockperiod+")");

        holder.lastrecording.setText(lastRecording);

        String Query= "SELECT daily_recording_id FROM recording_feed_consumption INNER JOIN daily_recording on daily_recording.id = recording_feed_consumption.daily_recording_id \n" +
                "WHERE flock_id = '"+flockid+"' AND recording_date = '"+lastRecording+"' ORDER BY recording_date DESC LIMIT 1";
        Cursor cursor = database.rawQuery(Query, null);
        String id = "";
        if (cursor.moveToFirst()){
            id = cursor.getString(cursor.getColumnIndex("daily_recording_id"));
        }
        cursor.close();
        //jika id ada isinya
        if (!id.isEmpty()){
            String QueryFeed= "SELECT name, feed_amount FROM recording_feed_consumption INNER JOIN daily_recording on daily_recording.id = recording_feed_consumption.daily_recording_id \n" +
                    "INNER JOIN feed on feed.id = recording_feed_consumption.feed_id\n" +
                    "WHERE flock_id = '"+flockid+"' AND daily_recording_id = '"+id+"'";
            Cursor cursorFeed = database.rawQuery(QueryFeed, null);
            String name= "";
            String amount ="";
            cursorFeed.moveToFirst();
            for (int i=0; i< cursorFeed.getCount(); i++){
                cursorFeed.moveToPosition(i);
                name = cursorFeed.getString(cursorFeed.getColumnIndex("name"));
                amount = cursorFeed.getString(cursorFeed.getColumnIndex("feed_amount"));
                AddFeed(holder,name,amount);
            }
            cursorFeed.close();

        }

    }
    public void AddFeed(ViewHolder holder, String name, String amount){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View View = inflater.inflate(R.layout.feed_category,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView feedName = (TextView) View.findViewById(R.id.name);
        TextView feedAmount = (TextView) View.findViewById(R.id.amount_used);
        feedName.setText("Name : "+name);
        feedAmount.setText("Amount Used : "+amount);
        holder.feedItems.addView(View, holder.feedItems.getChildCount());
    }

    @Override
    public int getItemCount() {
        return recordingFeedItems.size();
    }
}
