package com.layerfarm.dashboard.model;

import java.util.ArrayList;

public class Henday {

    String flock_id;
    String location_name;
    String flock_name;
    String flock_type;
    String flock_period;
    String last_record;
    String die_sick;
    String die_mechanic;
    String culled;
    String other;
    String total;
    String age;
    String number_bird;
    String weight;
    ArrayList<Integer> data;

     public Henday(){

     }

    public String getFlock_id() {
        return flock_id;
    }

    public void setFlock_id(String flock_id) {
        this.flock_id = flock_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getFlock_name() {
        return flock_name;
    }

    public void setFlock_name(String flock_name) {
        this.flock_name = flock_name;
    }

    public String getFlock_type() {
        return flock_type;
    }

    public void setFlock_type(String flock_type) {
        this.flock_type = flock_type;
    }

    public String getFlock_period() {
        return flock_period;
    }

    public void setFlock_period(String flock_period) {
        this.flock_period = flock_period;
    }

    public String getLast_record() {
        return last_record;
    }

    public void setLast_record(String last_record) {
        this.last_record = last_record;
    }

    public String getDie_sick() {
        return die_sick;
    }

    public void setDie_sick(String die_sick) {
        this.die_sick = die_sick;
    }

    public String getDie_mechanic() {
        return die_mechanic;
    }

    public void setDie_mechanic(String die_mechanic) {
        this.die_mechanic = die_mechanic;
    }

    public String getCulled() {
        return culled;
    }

    public void setCulled(String culled) {
        this.culled = culled;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNumber_bird() {
        return number_bird;
    }

    public void setNumber_bird(String number_bird) {
        this.number_bird = number_bird;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public ArrayList<Integer> getData() {
        return data;
    }

    public void setData(ArrayList<Integer> data) {
        this.data = data;
    }
}
