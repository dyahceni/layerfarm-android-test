package com.layerfarm.layerfarmmanager.constants;

/**
 * Created by Abhi on 19 Nov 2017 019.
 */

public class NavigationDrawerConstants {
    public final static String PROFILE_URL = "http://www.androiddeft.com/wp-content/uploads/2017/11/abhishek.jpg";
    public final static String BACKGROUND_URL = "http://www.androiddeft.com/wp-content/uploads/2017/11/nav_bg.jpg";
    public final static String TAG_HOME = "OFFLINE DASHBOARD";
    public final static String TAG_ACTIVE = "";
    public final static String TAG_GALLERY = "FARM DASHBOARD ONLINE";
    public final static String TAG_VIDEOS = "Videos";
    public final static String TAG_SETTINGS = "Settings";
    public final static String TAG_RECORDING = "RECORDING";
    public final static String TAG_SALES = "SALES";
    public final static String TAG_RECORDING_HISTORY = "RECORDING HISTORY";
    public final static String TAG_CHICKIN = "CHICK IN";
    public final static String TAG_PRICE_LIST = "PRICE LIST";
    public final static String TAG_GENERAL_EXPENSE = "GENERAL EXPENSE";
    public final static String TAG_DASHBOARD = "DASHBOARD";
    public final static String TAG_DASHBOARD_GROWER = "DASHBOARD GROWER";
    public final static String TAG_DASHBOARD_LAYER = "DASHBOARD LAYER";
    public final static String TAG_TIME_OUT = "TIME OUT";
    public final static String SITE_URL = "http://androiddeft.com";
    public final static String SHARE_TITLE = "Android Development Tutorials";
    public final static String SHARE_MESSAGE = "Hey Friend, I have found an awesome website for learning Android Programming: http://androiddeft.com";
    public final static String SHARE_VIA = "Share Via";
    public final static String SHARE_TEXT_TYPE = "text/plain";


}
