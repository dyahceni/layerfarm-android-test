package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;
import com.layerfarm.recording.activity.HistoryActivity;
import com.layerfarm.recording.adapter.HistoryFlockAdapter;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.Flock;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordingHistoryFragment extends Fragment {
    View view;
    helper help;
    DatabaseHelper dbase;
    private Spinner spn_location;
    private DBDataSource dataSource;
    ArrayList<Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    TabLayout mTabLayout;
    String flock_name;
    String location_name;
    ArrayList<String> location = new ArrayList<>();
    DashboardGrowerModel.GrowerPerformanceDetail resp;
    int tab_position = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_RECORDING_HISTORY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recording_history, container, false);

        help = new helper(getContext());
        help.open();
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_main);
//        setSupportActionBar(toolbar);
//        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.title);
//        toolbar_title.setText("Recording History");

        dbase = new DatabaseHelper(getContext());
        dataSource =  new DBDataSource(getContext());
        dataSource.open();
        sync();

//        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) view.findViewById(R.id.find_location);
//        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
//        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onTabTapped(tab_position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mTabLayout = (TabLayout)view.findViewById(R.id.tabs);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(
                        tab.getPosition());
                tab_position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        rvFlock = (RecyclerView) view.findViewById(R.id.rv_flock_history);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        rvFlock.setLayoutManager(layoutManager);


        // Inflate the layout for this fragment
        return view;
    }

    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                SubmitActive();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(getContext(), "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                SubmitInactive();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(getContext(), "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                SubmitAll();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(getContext(), "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    public void SubmitAll(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            Flock flock = new Flock();
            flock.setFlock_id(items.get(i).getFlock_nid());
            flock.setFlock_name(items.get(i).getFlock_name());
            flock.setLocation_id(items.get(i).getFarm_nid());
            flock.setType(items.get(i).getFlock_type());
            flock.setPeriod(items.get(i).getFlock_period());
            flock.setStatus(items.get(i).getFlock_status());
            flock.setLocation_name(items.get(i).getFarm_name());
            flock_items.add(flock);
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void SubmitActive(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            if (items.get(i).getFlock_status().equalsIgnoreCase("active")){
                Flock flock = new Flock();
                flock.setFlock_id(items.get(i).getFlock_nid());
                flock.setFlock_name(items.get(i).getFlock_name());
                flock.setLocation_id(items.get(i).getFarm_nid());
                flock.setType(items.get(i).getFlock_type());
                flock.setPeriod(items.get(i).getFlock_period());
                flock.setStatus(items.get(i).getFlock_status());
                flock.setLocation_name(items.get(i).getFarm_name());
                flock_items.add(flock);
            }
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void SubmitInactive(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            if (items.get(i).getFlock_status().equalsIgnoreCase("inactive")){
                Flock flock = new Flock();
                flock.setFlock_id(items.get(i).getFlock_nid());
                flock.setFlock_name(items.get(i).getFlock_name());
                flock.setLocation_id(items.get(i).getFarm_nid());
                flock.setType(items.get(i).getFlock_type());
                flock.setPeriod(items.get(i).getFlock_period());
                flock.setStatus(items.get(i).getFlock_status());
                flock.setLocation_name(items.get(i).getFarm_name());
                flock_items.add(flock);
            }
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void sync(){
        final ProgressDialog progress = new ProgressDialog(getContext());
//                                progress.setTitle("Loading");
//        progress.setMessage("Loading...");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        progress.setContentView(com.layerfarm.recording.R.layout.custom_progressdialog);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_selection_flock";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.getFlockLocationSelection(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                progress.dismiss();
                resp = response.body();
                if (resp.getLocation_flock() != null && resp.getLocation_flock().size()>0) {
                    Set<String> keySet = resp.getLocation_flock().keySet();
                    location = new ArrayList<>(keySet);
                    spn_location.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, location));
                    spn_location.setSelected(true);
//                String message = resp[0];
//                //Log.d("message","get_expense_general_respons : "+message);
//                if (message.equalsIgnoreCase("fail")){
//                    Toast.makeText(getApplicationContext(), "Egg can't updated" , Toast.LENGTH_LONG).show();
//                }
//
//                if (message.equalsIgnoreCase("ok")){
//                    Toast.makeText(getApplicationContext(), "Egg updated" , Toast.LENGTH_LONG).show();
//                    // TODO Auto-generated method stub
//                    SQLiteDatabase db = dbase.getWritableDatabase();
//                    db.execSQL("update egg_quality set name='"+
//                            EqqNameEditText.getText().toString() +"' where id='" +
//                            idEqqEditText.getText().toString()+"'");
//                    Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
//                    //EqqQualityActivity.ma.RefreshListEqq();
//
//                }
//                finish();
                }
            }
            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                progress.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                finish();
            }
        });

    }
}
