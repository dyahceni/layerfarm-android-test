package com.layerfarm.layerfarmmanager.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentFinancial;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentOverview;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerFragment.DashboardGrowerFragmentPerformance;
import com.layerfarm.dashboard.DashboardGrower.DashboardGrowerMainActivity;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardGrowerFragment extends Fragment {
    View view;
    ViewPager viewPager;
    TextView date;
    TabLayout tabLayout;
    Fragment fragment;
    Button try_again;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_DASHBOARD_GROWER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard_grower, container, false);

        date = (TextView) view.findViewById(R.id.date);
//        try_again = (Button) view.findViewById(R.id.try_again);
//        try_again.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                refresh();
//            }
//        });
        // setting tabLayout
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Overview"));
        tabLayout.addTab(tabLayout.newTab().setText("Performance"));
        tabLayout.addTab(tabLayout.newTab().setText("Financial"));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                    onTabTapped(tabLayout.getSelectedTabPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_grower_get_date";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel> call = apiInterfaceJson.DashboardGrower(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel> call, Response<DashboardGrowerModel> response) {
                DashboardGrowerModel dashboardGrowerModel = response.body();
                DashboardGrowerModel.getInstance().setLast_recording(dashboardGrowerModel.getLast_recording());
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date myDate = null;
                String sMyDate="";
                try {
                    myDate = sdf.parse(dashboardGrowerModel.getLast_recording());
                    sdf.applyPattern("EEEE, d MMM yyyy");
                    sMyDate = sdf.format(myDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                onTabTapped(0);
                date.setText(sMyDate);
//                setupViewPager(viewPager);
            }

            @Override
            public void onFailure(Call<DashboardGrowerModel> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
    public void refresh(){
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }
    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                fragment = new DashboardGrowerFragmentOverview();
                loadFragment(fragment);
                break;
            case 1:
                fragment = new DashboardGrowerFragmentPerformance();
                loadFragment(fragment);
                break;
            case 2:
                fragment = new DashboardGrowerFragmentFinancial();
                loadFragment(fragment);
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);

        }
    }
    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_test, fragment)
                    .commit();
            return true;
        }
        return false;
    }

//    private void setupViewPager(ViewPager viewPager) {
//        Adapter mainFragmentPagerAdapter = new Adapter(getFragmentManager());
//        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentOverview(), "Overview");
//        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentPerformance(), "Performance");
//        mainFragmentPagerAdapter.addFragment(new DashboardGrowerFragmentFinancial(), "Financial");
//        viewPager.setAdapter(mainFragmentPagerAdapter);
//    }
//    static class Adapter extends FragmentPagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public Adapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFragment(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
//    }
}
