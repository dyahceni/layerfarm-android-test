package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentFinancial;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentOverview;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerFragment.DashboardLayerFragmentPerformance;
import com.layerfarm.dashboard.DashboardLayer.DashboardLayerMainActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardLayerFragment extends Fragment {
    View view;

    ProgressDialog loading;
    private ArrayList type_name = new ArrayList();
    private  ArrayList mor_name = new ArrayList();

    private  ArrayList mor_value_today = new ArrayList();
    private  ArrayList mor_value_lastday = new ArrayList();
    private  ArrayList mor_value_week = new ArrayList();
    private  ArrayList mor_value_1month = new ArrayList();
    private  ArrayList mor_value_3month = new ArrayList();
    private  ArrayList mor_value_6month = new ArrayList();
    private  ArrayList mor_value_1year = new ArrayList();
    private  ArrayList mor_value_2year = new ArrayList();
    private  ArrayList mor_value_ytd = new ArrayList();

    private  ArrayList prod_value_today = new ArrayList();
    private  ArrayList prod_value_lastday = new ArrayList();
    private  ArrayList prod_value_week = new ArrayList();
    private  ArrayList prod_value_1month = new ArrayList();
    private  ArrayList prod_value_3month = new ArrayList();
    private  ArrayList prod_value_6month = new ArrayList();
    private  ArrayList prod_value_1year = new ArrayList();
    private  ArrayList prod_value_2year = new ArrayList();
    private  ArrayList prod_value_ytd = new ArrayList();

    private ArrayList<Double> eggSell = new ArrayList();
    private  ArrayList income = new ArrayList();
    private  ArrayList expense = new ArrayList();

    private ArrayList date_performance = new ArrayList<>();
    private ArrayList population = new ArrayList<>();
    private ArrayList egglaidegg = new ArrayList<>();
    private ArrayList egglaidweight = new ArrayList<>();
    private ArrayList fcr = new ArrayList<>();
    private ArrayList henday = new ArrayList<>();
    private ArrayList mortality = new ArrayList<>();
    ArrayList<Double> expenseValue = new ArrayList<Double>();
    private  double eggToday, eggLastDay, egg1Week, egg1Month, egg3Month, egg6Month, egg1Year, egg2Year, eggYtd;
    private  double weightToday, weightLastDay, weight1Week, weight1Month, weight3Month, weight6Month, weight1Year, weight2Year, weightYtd;
    private  int morToday, morLastDay, mor1Week, mor1Month, mor3Month, mor6Month, mor1Year, mor2Year, morYtd;
    private String last_recording_date;



    private  ArrayList egg = new ArrayList();
    private  ArrayList weight = new ArrayList();

    private ProgressDialog dialog;
    private SQLiteDatabase database;
    DatabaseHelper db;
    public String nid;
    TextView txtLastRecording;
    ViewPager viewPager;
    TabLayout tabLayout;
    Fragment fragment;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_DASHBOARD_LAYER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard_layer, container, false);

        // setting tabLayout
        txtLastRecording = view.findViewById(R.id.txtLastRecording);
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Overview"));
        tabLayout.addTab(tabLayout.newTab().setText("Performance"));
        tabLayout.addTab(tabLayout.newTab().setText("Financial"));
        //onTabTapped(0);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                onTabTapped(tabLayout.getSelectedTabPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        Log.d("zzz","layerdata instance = "+LayerDataDashboard.getInstance());
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_dashboard_layer_get_date";
        String[] args={};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LayerDataDashboard> call = apiInterfaceJson.getDashboardLayerfarm(token2, parameter);

        call.enqueue(new Callback<LayerDataDashboard>() {
            @Override
            public void onResponse(Call<LayerDataDashboard> call, Response<LayerDataDashboard> response) {
                LayerDataDashboard dashboard = response.body();
//                DashboardGrowerModel.getInstance().setOverview(dashboardGrowerModel.getOverview());
                LayerDataDashboard.getInstance().setLastRecording(dashboard.getLastRecording());
//                DashboardGrowerModel.getInstance().setAvailable_mortality(dashboardGrowerModel.getAvailable_mortality());
//                DashboardGrowerModel.getInstance().setPerformance(dashboardGrowerModel.getPerformance());
//                DashboardGrowerModel.getInstance().setFinancial(dashboardGrowerModel.getFinancial());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date myDate = null;
                String sMyDate="";
                try {
                    myDate = sdf.parse(dashboard.getLastRecording());
                    sdf.applyPattern("EEEE, d MMM yyyy");
                    sMyDate = sdf.format(myDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                onTabTapped(0);
                txtLastRecording.setText(sMyDate);
//                setupViewPager(viewPager);
            }

            @Override
            public void onFailure(Call<LayerDataDashboard> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
        //onTabTapped(0);
        // Inflate the layout for this fragment
        return view;
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                fragment = new DashboardLayerFragmentOverview();
                loadFragment(fragment);
                break;
            case 1:
                fragment = new DashboardLayerFragmentPerformance();
                loadFragment(fragment);
                break;
            case 2:
                fragment = new DashboardLayerFragmentFinancial();
                loadFragment(fragment);
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }
    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_test, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getContext())
                .setTitle("Warning!!")
                .setMessage("Data Response Empty, Please Try Again!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

}
