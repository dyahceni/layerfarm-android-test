package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.layerfarm.chickin.adapter.ChickinHistoryAdapter;
import com.layerfarm.chickin.ChickinEntryActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.chickinHistory;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;
import com.layerfarm.setting.DataHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChickinFragment extends Fragment {
    View view;

    ProgressDialog loading;
    private LinearLayoutManager linearlaout;
    List<chickinHistory> itemList = new ArrayList<chickinHistory>();
    ArrayList<chickinHistory.distribution_history> distList = new ArrayList<chickinHistory.distribution_history>();
    public String url, username, password;
    DataHelper SQLite = new DataHelper(getContext());
    DatabaseHelper db = new DatabaseHelper(getContext());
    private RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_CHICKIN);
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            //Write down your refresh code here, it will call every time user come to this fragment.
            //If you are using listview with custom adapter, just call notifyDataSetChanged().
            Log.d("zzz","Fragment Chick in setUSerVisibleHint");
        }

        if (getFragmentManager() != null) {

            getFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("zzz","Fragment Chick in onResume");
        getChickinHash();
//        if (getFragmentManager() != null) {
//
//            getFragmentManager()
//                    .beginTransaction()
//                    .detach(this)
//                    .attach(this)
//                    .commit();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chickin, container, false);


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent createFarmActivity = new Intent(getContext(), ChickinEntryActivity.class);
                startActivity(createFarmActivity);
                //overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                //finish();

            }
        });

//        getChickinHash();

//        linearlaout
        recyclerView = (RecyclerView) view.findViewById(R.id.chickin_history_recycler_view);
        Log.i("itemList", "" + itemList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void getChickinHash(){
        loading = ProgressDialog.show(getContext(), null, "Please wait...", true, false);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_chickin_history_link";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LinkedHashMap<String, chickinHistory>> call = apiInterfaceJson.getLinkedMapChickinHistory(token2, parameter);
        call.enqueue(new Callback<LinkedHashMap<String, chickinHistory>>() {
            @Override
            public void onResponse(Call<LinkedHashMap<String, chickinHistory>> call, Response<LinkedHashMap<String, chickinHistory>> response) {
                if (response != null) {

                    LinkedHashMap<String, chickinHistory> map = response.body();
                    Collection<chickinHistory> values = map.values();
                    ArrayList<chickinHistory> listOfValues = new ArrayList<chickinHistory>(values);
                    adapter = new ChickinHistoryAdapter(listOfValues);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    loading.dismiss();
                    //getFlock();
                }
            }

            @Override
            public void onFailure(Call<LinkedHashMap<String, chickinHistory>> call, Throwable t) {
                // Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                loading.dismiss();
                Toast.makeText(getContext(), "Failed to load data",Toast.LENGTH_LONG).show();

            }
        });
    }
}
