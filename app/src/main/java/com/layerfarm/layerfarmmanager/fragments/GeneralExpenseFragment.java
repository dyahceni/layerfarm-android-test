package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.layerfarm.expense.Adapter.GeneralExpenseRvAdapter;
import com.layerfarm.expense.GeneralExpense.NewGeneralExpenseActivity;
import com.layerfarm.layerfarm.model.ExpenseGeneral;
import com.layerfarm.layerfarm.model.ExpenseGeneralList;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralExpenseFragment extends Fragment {
    View view;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton fab ;
    public static GeneralExpenseFragment ma;
    ProgressDialog loading;
    ArrayList<ExpenseGeneral> empDataList = new ArrayList<>();
    List<Farm> location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_GENERAL_EXPENSE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_general_expense, container, false);

        ma = this;
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), NewGeneralExpenseActivity.class);
                intent.putExtra("list_location", (Serializable) location);
                startActivity(intent);

            }
        });



//        getLayerFarmGeneralExpense();

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getLayerFarmGeneralExpense();
    }

    public void getLayerFarmGeneralExpense(){
        loading = ProgressDialog.show(getContext(), "Syncronize Data", "Please wait...", true, false);
        empDataList.clear();

        String module = "layerfarm_android";
        String function_name = "general_expense_data";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ExpenseGeneralList> call = apiInterfaceJson.getGeneralExpenseLayerfarm(token2, parameter);

        call.enqueue(new Callback<ExpenseGeneralList>() {
            @Override
            public void onResponse(Call<ExpenseGeneralList> call, Response<ExpenseGeneralList> response) {

                if (response != null && response.isSuccessful()) {
                    try {
//                        loading.dismiss();
                        empDataList.addAll(response.body().getExpenseGeneralList());
                        getLocation();
//                        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_general_expense);
//                        mAdapter = new GeneralExpenseRvAdapter(empDataList);
//                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
//                        mRecyclerView.setLayoutManager(layoutManager);
//                        mRecyclerView.setAdapter(mAdapter);

                    }catch (Exception e){
                        Toast.makeText(getContext(),"No Data",Toast.LENGTH_LONG).show();
                        loading.dismiss();
                        fab.hide();
                        AskOption();
                    }


                } else {

                    loading.dismiss();
                    fab.hide();
                    AskOption();
                }



            }

            @Override
            public void onFailure(Call<ExpenseGeneralList> call, Throwable t) {
                loading.dismiss();
                fab.hide();
                AskOption();
                Log.d("Layerfarm", "Data failure = " + t.getMessage());

            }
        });


    }

    public void getLocation(){
//        loading = ProgressDialog.show(getContext(), "Syncronize Data", "Please wait...", true, false);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_farm";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<Farm>> call = apiInterfaceJson.getFarm(token2, parameter);

        call.enqueue(new Callback<List<Farm>>() {
            @Override
            public void onResponse(Call<List<Farm>> call, Response<List<Farm>> response) {

                if (response != null && response.isSuccessful()) {
                    try {
                        loading.dismiss();
                        location = response.body();


                    }catch (Exception e){
                        Toast.makeText(getContext(),"No Data",Toast.LENGTH_LONG).show();
                        location = new ArrayList<>();
                        loading.dismiss();
                    }
                    mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_general_expense);
                    mAdapter = new GeneralExpenseRvAdapter(empDataList, location);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    mRecyclerView.setLayoutManager(layoutManager);
                    mRecyclerView.setAdapter(mAdapter);

                } else {

                    loading.dismiss();
                    AskOption();
                }



            }

            @Override
            public void onFailure(Call<List<Farm>> call, Throwable t) {
                loading.dismiss();
                Log.d("Layerfarm", "Data failure = " + t.getMessage());

            }
        });


    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getContext())
                .setTitle("Warning!!")
                .setMessage("Connection Error. Please try again later.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }

}
