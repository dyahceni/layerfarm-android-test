package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.layerfarm.expense.PricelistFragment.PricelistFragmentFeed;
import com.layerfarm.expense.PricelistFragment.PricelistFragmentHatching;
import com.layerfarm.expense.PricelistFragment.PricelistFragmentOvk;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;

import java.util.ArrayList;
import java.util.List;

public class PriceListFragment extends Fragment {
    View view;
    Fragment fragment;
    private  ArrayList ovkDataList = new ArrayList();
    private  ArrayList feedDataList = new ArrayList();
    private  ArrayList hatchingDataList = new ArrayList();
    ViewPager viewPager;
    ProgressDialog loading;
    TabLayout tabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(NavigationDrawerConstants.TAG_PRICE_LIST);
        view = inflater.inflate(R.layout.fragment_price_list, container, false);

//        loading = ProgressDialog.show(getContext(), "Syncronize Data", "Please wait...", true, false);

//        getLayerFarmPriceList();

        tabLayout  = (TabLayout) view.findViewById(R.id.tabLayoutPricelist);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.medication_vaccination));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.feed));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.hatch_date));
        onTabTapped(0);
//        viewPager = view.findViewById(R.id.viewPagerPricelist);
//        TabLayout tabLayout = view.findViewById(R.id.tabLayoutPricelist);
//        tabLayout.setupWithViewPager(viewPager);
//        onTabTapped(0);
//        for (int i = 0; i < TabLayoutLayerMain.getTabCount(); i++) {
//
//            TabLayout.Tab tab = TabLayoutLayerMain.getTabAt(i);
//            if (tab != null) {
//
//                TextView tabTextView = new TextView(getContext());
//                tab.setCustomView(tabTextView);
//
//                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
//                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//
//                tabTextView.setText(tab.getText());
//                tabTextView.setTextSize(20);
//
//                if (i == 0) {
//                    tabTextView.setTextSize(20);
//                }
//
//            }
//
//        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tabLayout.getSelectedTabPosition());

//                ViewGroup vg = (ViewGroup) TabLayoutLayerMain.getChildAt(0);
//                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
//                int tabChildsCount = vgTab.getChildCount();
//                for (int i = 0; i < tabChildsCount; i++) {
//                    View tabViewChild = vgTab.getChildAt(i);
//                    if (tabViewChild instanceof TextView) {
//                        ((TextView) tabViewChild).setTextSize(25);
//                        ((TextView) tabViewChild).setTextColor(getResources().getColorStateList(com.layerfarm.expense.R.color.Black));
//                        ((TextView) tabViewChild).setTypeface(null, Typeface.BOLD);
//                    }
//                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                ViewGroup vg = (ViewGroup) TabLayoutLayerMain.getChildAt(0);
//                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
//                int tabChildsCount = vgTab.getChildCount();
//                for (int i = 0; i < tabChildsCount; i++) {
//                    View tabViewChild = vgTab.getChildAt(i);
//                    if (tabViewChild instanceof TextView) {
//                        ((TextView) tabViewChild).setTextSize(20);
//                        ((TextView) tabViewChild).setTextColor(getResources().getColorStateList(com.layerfarm.expense.R.color.white));
//                        ((TextView) tabViewChild).setTypeface(null, Typeface.BOLD);
//                    }
//                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

//    private void setupViewPager(ViewPager viewPager) {
//        Adapter mainFragmentPagerAdapter = new Adapter(getFragmentManager());
//        mainFragmentPagerAdapter.addFragment(new PricelistFragmentOvk(), "Medivac");
//        mainFragmentPagerAdapter.addFragment(new PricelistFragmentFeed(), "Feed");
//        mainFragmentPagerAdapter.addFragment(new PricelistFragmentHatching(), "Hatch");
//        viewPager.setAdapter(mainFragmentPagerAdapter);
//    }

//    public void close_pricelist(View view) {
//        finish();
//    }
    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_price_list, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                fragment = new PricelistFragmentOvk();
                loadFragment(fragment);
                break;
            case 1:
                fragment = new PricelistFragmentFeed();
                loadFragment(fragment);
                break;
            case 2:
                fragment = new PricelistFragmentHatching();
                loadFragment(fragment);
                break;
            case 3:
                Log.d("zzz","position 3");
                break;
            default:
                Toast.makeText(getContext(), "Tapped " + position, Toast.LENGTH_SHORT);
        }
    }

//    public void getLayerFarmPriceList(){
//
//        String module = "layerfarm_android";
//        String function_name = "price_list_data";
//        String[] args = {};
//        Parameter parameter = new Parameter(module, function_name, args);
//
//        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//        String token2 = RetrofitData.getInstance().getToken2();
//        Call<AllPriceList> call = apiInterfaceJson.getPriceListLayerfarm(token2, parameter);
//
//        call.enqueue(new Callback<AllPriceList>() {
//            @Override
//            public void onResponse(Call<AllPriceList> call, Response<AllPriceList> response) {
//
//                //Toast.makeText()
//
//                if (response != null && response.isSuccessful()) {
//                    try {
//                        AllPriceList allpricelistModel = response.body();
//                        AllPriceList.getInstance().setFeedlist(allpricelistModel.getFeedlist());
//                        AllPriceList.getInstance().setOvklist(allpricelistModel.getOvklist());
//                        AllPriceList.getInstance().setHatchinglist(allpricelistModel.getHatchinglist());
//
//                        //Log.i("ovkDataList", "" + ovkDataList);
////                        onTabTapped(0);
//                        loading.dismiss();
//                        setupViewPager(viewPager);
//                    }catch (Exception e){
//                        //e.printStackTrace();
//                        // String error = e.toString();
//                        Toast.makeText(getContext(),e.toString(),Toast.LENGTH_LONG).show();
//                    }
//
//
//                } else {
//                    //Log.i("onEmptyResponse", "Returned empty response");
//                    //Toast.makeText(this(),"Nothing returned",Toast.LENGTH_LONG).show();
//
//                }
//
//
//
//            }
//
//            @Override
//            public void onFailure(Call<AllPriceList> call, Throwable t) {
//                //Log.d("Layerfarm", "Data failure = " + t.getMessage());
//                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
//            }
//        });
//
//
//    }
}
