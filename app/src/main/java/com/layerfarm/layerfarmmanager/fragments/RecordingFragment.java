package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;
import com.layerfarm.recording.activity.RecordingMainActivity;
import com.layerfarm.recording.adapter.FlockAdapter;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.helperUnregistered;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordingFragment extends Fragment {
    View view;
    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSource dataSource;
    ArrayList<Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> location = new ArrayList<>();
    DashboardGrowerModel.GrowerPerformanceDetail resp;
    ScrollView content_view;
    FrameLayout fragment_recording;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivity().setTitle(NavigationDrawerConstants.TAG_RECORDING);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(NavigationDrawerConstants.TAG_RECORDING);
        Log.d("zzzz","ocCreate recording");
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_recording, container, false);

        fragment_recording = (FrameLayout) view.findViewById(R.id.frame_recording);
        dataSource =  new DBDataSource(getContext());
        dataSource.open();

        db = new DatabaseHelper(getContext());
        content_view = (ScrollView)view.findViewById(R.id.content_view);
        sync();

        spn_location = (Spinner) view.findViewById(R.id.find_location);

        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Layerfarm","testing");
                Submit();
//                getFragmentManager().popBackStack();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rvFlock = (RecyclerView) view.findViewById(R.id.rv_flock);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        rvFlock.setLayoutManager(layoutManager);


        return view;
    }
    public void Submit(){

        String location = spn_location.getSelectedItem().toString();

        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);
        flock_items = new ArrayList();
        for (int i=0; i < items.size(); i++){
            Flock flock = new Flock();
            flock.setFlock_id(items.get(i).getFlock_nid());
            flock.setFlock_name(items.get(i).getFlock_name());
            flock.setLocation_id(items.get(i).getFarm_nid());
            flock.setType(items.get(i).getFlock_type());
            flock.setPeriod(items.get(i).getFlock_period());
            flock.setStatus(items.get(i).getFlock_status());
            flock.setLocation_name(items.get(i).getFarm_name());
            flock_items.add(flock);
        }
        adapter = new FlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }

    public void sync(){
        final ProgressDialog progress = new ProgressDialog(getContext());
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        progress.setContentView(com.layerfarm.recording.R.layout.custom_progressdialog);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_selection_flock";
        String[] args = {"Recording"};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.getFlockLocationSelection(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                progress.dismiss();
                resp = response.body();
                Log.d("resp", "resp = "+resp);
                try {
                    if (resp.getLocation_flock() != null && resp.getLocation_flock().size()>0) {
                        content_view.setVisibility(View.VISIBLE);
                        Set<String> keySet = resp.getLocation_flock().keySet();
                        location = new ArrayList<>(keySet);
                        spn_location.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, location));
                        spn_location.setSelected(true);
                    }
                }
                catch (Exception e){
                    Toast.makeText(getContext(), "Null",Toast.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getContext(), "Can't Show data",Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                fragment_recording.setVisibility(View.GONE);
            }
        });

    }

}
