package com.layerfarm.layerfarmmanager.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.helperUnregistered;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class OfflineDashboardFragment extends Fragment {

    private ArrayList recording_history = new ArrayList();
    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSourceUnregistered dataSource;
    helperUnregistered help;
    private String recording_id, id_flock, mortality, total_bird, recording, hatch_date;
    long ID;
    TextView valuePopulation, valueKg, valueEgg, valueFcr, valueHenday, valueMortality, TvLastRecording, valueFi;
    private double henday, fcr, result, fi, resultFi;
    FrameLayout frame_offline;

    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_ACTIVE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_offline_dashboard, container, false);

        valuePopulation = (TextView) view.findViewById(R.id.valuePopulation);
        valueKg = (TextView) view.findViewById(R.id.valuekg);
        valueEgg = (TextView) view.findViewById(R.id.valueegg);
        valueFcr = (TextView) view.findViewById(R.id.valuefcr);
        valueHenday = (TextView) view.findViewById(R.id.valuehenday);
        valueMortality = (TextView) view.findViewById(R.id.valuemortality);
        valueFi = (TextView) view.findViewById(R.id.valuefi);
        frame_offline = (FrameLayout) view.findViewById(R.id.frame_offline);
        TvLastRecording  = (TextView) view.findViewById(R.id.textDate);



        help = new helperUnregistered( getActivity());
        help.open();

        dataSource =  new DBDataSourceUnregistered( getActivity());
        dataSource.open();
        db = new DatabaseHelper( getActivity());
        SQLiteDatabase dbase = db.getReadableDatabase();


        String selectQuery= "SELECT * FROM flock order by last_recording desc limit 1";
        Cursor cursor1 = dbase.rawQuery(selectQuery, null);
        String lastdate = "";
        if(cursor1.moveToFirst())
            lastdate  =  cursor1.getString(8);
        cursor1.close();


        TvLastRecording.setText(lastdate);
        Log.d("lastdate",""+ lastdate);
        if(lastdate == null){
            frame_offline.setVisibility(View.GONE);
        }else{
            frame_offline.setVisibility(View.VISIBLE);
        }


        Cursor cursor= null;
        cursor = dbase.rawQuery("select flock.id as flock_id, chick_in.hatch_date,daily_recording_chick_in.chick_in_id, daily_recording.id as daily_recording_id, daily_recording.recording_date, cache_total_bird.mortality, cache_total_bird.total_bird  from daily_recording\n" +
                "inner join cache_total_bird on cache_total_bird.daily_recording_id = daily_recording.id\n" +
                "inner join daily_recording_chick_in on daily_recording_chick_in.daily_recording_id = daily_recording.id\n" +
                "INNER join chick_in on chick_in.id = daily_recording_chick_in.chick_in_id\n" +
                "INNER JOIN flock on flock.id = daily_recording.flock_id where daily_recording.recording_date = '"+lastdate+"'",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            recording_id = cursor.getString(3);
            id_flock = cursor.getString(1);
            mortality = cursor.getString(5);
            total_bird = cursor.getString(6);
            recording = cursor.getString(4);
            hatch_date = cursor.getString(1);
            cursor.close();
        }

        //get all eggs
        String selectQueryEgg= "SELECT sum(recording_production.egg_number) as egg_number,sum(recording_production.egg_weight) as egg_weight, daily_recording.id, daily_recording.recording_date from recording_production\n" +
                "inner join daily_recording on daily_recording.id = recording_production.daily_recording_id\n" +
                "where  daily_recording.recording_date  = '"+lastdate+"'";
        Cursor cursorEgg = dbase.rawQuery(selectQueryEgg, null);
        String Egg = "";
        String Weight = "";
        if(cursorEgg.moveToFirst())
            Egg  =  cursorEgg.getString(0);
        Weight  =  cursorEgg.getString(1);
        cursorEgg.close();

        //get pakan
        String selectQueryFeed= "SELECT recording_feed_consumption.feed_amount, daily_recording.recording_date from recording_feed_consumption\n" +
                "inner join daily_recording on daily_recording.id = recording_feed_consumption.daily_recording_id\n" +
                "where  daily_recording.recording_date  = '"+lastdate+"'";
        Cursor cursorFeed = dbase.rawQuery(selectQueryFeed, null);
        String Feed = "";
        if(cursorFeed.moveToFirst())
            Feed  =  cursorFeed.getString(0);
        cursorFeed.close();

        if (Egg != null && !Egg.equals("") && total_bird != null && !total_bird.equals("")) {
            henday = Double.parseDouble(Egg) / Double.parseDouble(total_bird);
            result = henday * 100;
        }

        if (Feed != null && !Feed.equals("") && Weight != null && !Weight.equals("")) {
            fcr = Double.parseDouble(Feed) / Double.parseDouble(Weight);
        }


        if (Feed != null && !Feed.equals("") && total_bird != null && !total_bird.equals("")) {
            fi = Double.parseDouble(Feed) / Double.parseDouble(total_bird);
            resultFi = fi * 1000;
        }



        DecimalFormat form = new DecimalFormat("0.00");

        Log.d("henday", " = " + result);
        Log.d("Feed", " = " + Feed);
        Log.d("Egg", " = " + Egg);
        Log.d("Weight", " = " + Weight);


        Log.d("recording_id", " = " + recording_id);
        Log.d("id_flock", " = " + id_flock);
        Log.d("mortality", " = " + mortality);
        Log.d("total_bird", " = " + total_bird);
        Log.d("recording", " = " + recording);

        valuePopulation.setText(total_bird);
        valueMortality.setText(mortality);
        valueEgg.setText(Egg);
        valueKg.setText(Weight);
        valueHenday.setText(form.format(result));
        valueFcr.setText(form.format(fcr));
        valueFi.setText(form.format(resultFi));

        return view;
    }

}
