package com.layerfarm.layerfarmmanager.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.Sales;
import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.layerfarm.model.SyncEggQuality;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.layerfarmmanager.constants.NavigationDrawerConstants;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.sales.Adapter.SalesAdapter;
import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.Sales.SalesCreate;
import com.layerfarm.sales.SalesActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesFragment extends Fragment {
    View view;
    private RecyclerView sale_content;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private ProgressDialog progressDoalog;
    private ProgressBar progressBar;
    int page = 0;
    Boolean isScrolling = false;
    Boolean isLoading = false;
    int currentItems, totalItems, scrollOutItems, prevItems;
    ArrayList<Sales> recording = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(NavigationDrawerConstants.TAG_SALES);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_sales, container, false);
        eggquality();
        buyerList();
        locationList();
//        eggquality();
        Toolbar toolbarTop = (Toolbar) view.findViewById(R.id.toolbar_main);

        FloatingActionButton create = (FloatingActionButton) view.findViewById(R.id.fab);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sales sale = new Sales();
                Intent intent = new Intent(getContext(), SalesCreate.class);
                intent.putExtra("sales", (Serializable) sale);
                startActivity(intent);
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.progress_circular);
        sale_content = (RecyclerView) view.findViewById(R.id.sale_content);
        sale_content.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        sale_content.setLayoutManager(layoutManager);
        adapter = new SalesAdapter(recording);
        sale_content.setAdapter(adapter);
        // Set up progress before call
        sale_content.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                    Log.d("TAG", "Scrolling...");
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                Log.d("Sales", "findlast linear layout manager = "+layoutManager.findLastCompletelyVisibleItemPosition());
                Log.d("Sales", "currentItems = "+currentItems);
                Log.d("Sales", "totalItems = "+totalItems);
                Log.d("Sales", "scrollOutItems = "+scrollOutItems);
                if (!isLoading){
                    if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == recording.size() - 1) {
                        //bottom of list!
                        isScrolling = false;
                        Sync();

                    }
//                    if ((currentItems == totalItems) || (dy>0 && currentItems + scrollOutItems == totalItems)){
//                        isScrolling = false;
//                        Sync();
//                    }
                }

            }
        });

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        recording.clear();
        Sync();

    }
    public void getData(){
        eggquality();
    }
    public void Sync(){
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        isLoading = true;
        Log.d("zzz","selling_price_get_sale");
        String module = "selling_price";
        String function_name = "selling_price_get_sale";
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        String[] args = {null,null,Integer.toString(page)};
        Parameter parameter = new Parameter(module,function_name, args);
        Call<ArrayList<Sales>> call = apiInterfaceJson.getSales(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<ArrayList<Sales>>() {
            @Override
            public void onResponse(Call<ArrayList<Sales>> call, Response<ArrayList<Sales>> response) {
                ArrayList<Sales> data = response.body();
                Log.d("zzz","diluar if");
                if (!data.isEmpty()){
                    Log.d("zzz","masuk if");
                    recording.addAll(data);

                    adapter.notifyDataSetChanged();

                    page++;
                }
                else {
                    Log.d("zzz","masuk else");
                    Toast.makeText(getContext(), "no more page",Toast.LENGTH_SHORT).show();
                }

                progressDoalog.dismiss();
                isLoading = false;
            }

            @Override
            public void onFailure(Call<ArrayList<Sales>> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("zzz","on Failure : "+alert);
                progressDoalog.dismiss();
                isLoading = false;
            }
        });
    }
    public void buyerList(){

        final HashMap<String, String> buyer_hash = new HashMap<>();
        String module = "selling_price";
        String function_name = "selling_price_get_buyer";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<SyncBuyer>> call = apiInterfaceJson.getBuyer(token2, parameter);

        call.enqueue(new Callback<List<SyncBuyer>>() {

            @Override
            public void onResponse(Call<List<SyncBuyer>> call, Response<List<SyncBuyer>> response) {
//                ArrayList<HashMap<String, String>> row = SQLite.getAllBuyerList();
//                List<String> data = new ArrayList<>();
//                for (int i = 0; i < row.size(); i++) {
//                    String name = row.get(i).get("name").toLowerCase();
//                    data.add(name);
//                }
                List<SyncBuyer> buyerSyncList = response.body();
                for (int i =0; i< buyerSyncList.size() ; i++){
                    String name = buyerSyncList.get(i).getName();
                    String rid = buyerSyncList.get(i).getRid();
                    String address = buyerSyncList.get(i).getAddress();

                    buyer_hash.put(rid, name);
                }
                Log.d("buyer", "buyer list = "+buyer_hash.values());
                SalesModel.getInstance().setList_buyer(buyer_hash);
            }

            @Override
            public void onFailure(Call<List<SyncBuyer>> call, Throwable t) {
                Log.d("Layerfarm", "Buyer failure = " + t.getMessage());
            }
        });
        Log.d("buyer", "buyer2 = "+buyer_hash.values());

    }
    public void locationList(){
        final HashMap<String, String> location_hash = new HashMap<>();
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_farm";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<Farm>> call = apiInterfaceJson.getFarm(token2, parameter);

        call.enqueue(new Callback<List<Farm>>() {
            @Override
            public void onResponse(Call<List<Farm>> call, Response<List<Farm>> response) {
                List<Farm> farmSyncList = response.body();

                if(farmSyncList!=null && farmSyncList.size()>0) {
//                    db.delete("LOCATION", null, null);
                    for (int i = 0; i < farmSyncList.size(); i++) {
                        location_hash.put(farmSyncList.get(i).getNid(), farmSyncList.get(i).getName());
                    }
                }
                Log.d("location", "location list = "+location_hash.values());
                SalesModel.getInstance().setList_location(location_hash);
            }

            @Override
            public void onFailure(Call<List<Farm>> call, Throwable t) {
                Log.d("Layerfarm", "Location failure = " + t.getMessage());
            }
        });

    }
    public void eggquality(){
        final HashMap<String, String> egg_quality = new HashMap<>();
        String module = "layerfarm";
        String function_name = "layerfarm_get_available_quality";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<SyncEggQuality>> call = apiInterfaceJson.getEggQuality(token2, parameter);

        call.enqueue(new Callback<List<SyncEggQuality>>() {
            @Override
            public void onResponse(Call<List<SyncEggQuality>> call, Response<List<SyncEggQuality>> response) {
                List<SyncEggQuality> farmSyncList = response.body();

                if(farmSyncList!=null && farmSyncList.size()>0) {
//                    db.delete("LOCATION", null, null);
                    for (int i = 0; i < farmSyncList.size(); i++) {
                        egg_quality.put(farmSyncList.get(i).getName(), farmSyncList.get(i).getDisplay_name());
                    }
                }
                Log.d("location", "location list = "+egg_quality.values());
                SalesModel.getInstance().setList_quality(egg_quality);
//                Sync();
            }

            @Override
            public void onFailure(Call<List<SyncEggQuality>> call, Throwable t) {
                Log.d("Layerfarm", "Location failure = " + t.getMessage());
            }
        });

    }
}
