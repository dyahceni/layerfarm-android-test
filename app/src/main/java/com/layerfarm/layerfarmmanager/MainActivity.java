package com.layerfarm.layerfarmmanager;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


//billing
import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;


import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarmmanager.Navigation.AppListAccount;
import com.layerfarm.layerfarmmanager.Navigation.AppListFarm;
import com.layerfarm.layerfarmmanager.fragments.ChickinFragment;
import com.layerfarm.layerfarmmanager.fragments.DashboardGrowerFragment;
import com.layerfarm.layerfarmmanager.fragments.DashboardLayerFragment;
import com.layerfarm.layerfarmmanager.fragments.GeneralExpenseFragment;
import com.layerfarm.layerfarmmanager.fragments.OfflineDashboardFragment;
import com.layerfarm.layerfarmmanager.fragments.OnlineDashboardFragment;
import com.layerfarm.layerfarmmanager.fragments.PriceListFragment;
import com.layerfarm.layerfarmmanager.fragments.RecordingFragment;
import com.layerfarm.layerfarmmanager.fragments.SalesFragment;
import com.layerfarm.chickin.listItemChickin.FlockListActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.reported.ByFlock.ReportByFlockActivity;
import com.layerfarm.setting.BuyerActivity;
import com.layerfarm.setting.FeedActivity;
import com.layerfarm.setting.FlockActivity;
import com.layerfarm.setting.LocationActivity;
import com.layerfarm.setting.MedicationVaccinationNewActivity;
import com.layerfarm.setting.ServerSettingActivity;
import com.layerfarm.setting.StrainActivity;
import com.layerfarm.setting.UserActivityNew;
import com.layerfarm.setting.VendorActivity;
import com.layerfarm.unregistered.chickinUnregistered.UnregisteredChickinMainActivity;
import com.layerfarm.unregistered.intro.SplashScreenActivity;
import com.layerfarm.unregistered.historyRecording.activity.HistoryRecordingOfflineSubmenu;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
import java.util.Timer;
import java.util.TimerTask;*/



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , PurchasesUpdatedListener, AcknowledgePurchaseResponseListener{

    static MainActivity instance;
    //Deklarasi dan Inisialisasi SharedPreferences
    private SharedPreferences preferences;
    //Digunakan Untuk Konfigurasi SharedPreferences
    private SharedPreferences.Editor editor;
    private int purchase_state = 1;

    private int SubscribeStatus = 0;

    EditText connection;

    private ProgressDialog dialog;
    private SQLiteDatabase database;
    TextView txtLastRecording;
    ViewPager viewPager;
    DecimalFormat dFormat = new DecimalFormat("####,###,###.00");
    //------ end of online dashboard
    DatabaseHelper db;
    ArrayList<String> menus;
    Map<String, String> menu_map = new HashMap<>();
    private Handler mHandler = new Handler();
    String uname, passwd, code, url, urlserver, usernameserver, passwordserver;
    NavigationView navigationView;
    ProgressBar progressBar;
    TextView farm_name, farm_code, farm_role, user_name;
    ImageView imgDashboard;

    //billing api
    private BillingClient billingClient;
    List<SkuDetails> mySkuDetailsList;
    private Handler mHandlerCheckFarmStatus = new Handler();
    private Handler mHandlerServer = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        MultiDex.install(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.proggressBar);
        setSupportActionBar(toolbar);
        db = new DatabaseHelper(this);
        Log.d("zzzi","Main Activity on create");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.inflateHeaderView(R.layout.nav_header_main);
        hView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(purchase_state == 1) {
                    Intent intent;
                    intent = new Intent(getApplicationContext(), AppListAccount.class);
                    startActivity(intent);
//                    finish();
                }else{

                }
            }
        });
        ImageView imgvw = (ImageView)hView.findViewById(R.id.imageView);
        imgDashboard = (ImageView)hView.findViewById(R.id.imageDashboard);
        farm_name = (TextView)hView.findViewById(R.id.farm);
        farm_code = (TextView)hView.findViewById(R.id.code);
        farm_role = (TextView)hView.findViewById(R.id.user_status);
        user_name = (TextView)hView.findViewById(R.id.name);
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_main_drawer);

        if (RetrofitData.getInstance().getStatus() != "" && RetrofitData.getInstance().getStatus() != null){
            Start();
        }
        else {

            RetrofitData.getInstance().setStatus("");
            startActivity(new Intent(this, SplashScreenActivity.class));

        }
    }


    public void displaySelectedFragment(Fragment fragment, String TAG) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, TAG);
        fragmentTransaction.addToBackStack(TAG);
        Log.d("zzzz","Tag = "+TAG);
        fragmentTransaction.commit();
    }

    public void refresh(View v){
//        getFragmentManager()
//                .beginTransaction()
//                .detach(this)
//                .attach(this)
//                .commit();
        Intent i = getIntent();
        finish();
        overridePendingTransition(0, 0);
        startActivity(i);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        //-----start menu jika pakai billing

//        if(purchase_state == 1){
            MenuItem option = menu.findItem(R.id.action_notification1);
            option.setVisible(true);
//            MenuItem activation = menu.findItem(R.id.miActivation);
//            activation.setVisible(false);
//        }else{
//            MenuItem option = menu.findItem(R.id.action_notification1);
//            option.setVisible(false);
//            MenuItem activation = menu.findItem(R.id.miActivation);
//            activation.setVisible(true);
//        }

        //----end menu pakai billing

        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu)
    {

        boolean result = super.onPrepareOptionsMenu(menu);
//        styleMenuButton();
        //------- start menu jika pakai billing---//

        if (purchase_state == 1) {
            MenuItem farm = menu.findItem(R.id.farm);
            farm.setVisible(menu_map.containsValue("Farm"));
            MenuItem location = menu.findItem(R.id.location);
            location.setVisible(menu_map.containsValue("Location"));
            MenuItem flock = menu.findItem(R.id.flock);
            flock.setVisible(menu_map.containsValue("Flock"));
            MenuItem member = menu.findItem(R.id.member);
            member.setVisible(menu_map.containsValue("Member"));
            MenuItem strain = menu.findItem(R.id.strain);
            strain.setVisible(menu_map.containsValue("Strain"));
            MenuItem feed = menu.findItem(R.id.feed);
            feed.setVisible(menu_map.containsValue("Feed"));
            MenuItem medvac = menu.findItem(R.id.medvac);
            medvac.setVisible(menu_map.containsValue("Medivac"));
            MenuItem buyer = menu.findItem(R.id.buyer);
            buyer.setVisible(menu_map.containsValue("Buyer"));
            MenuItem vendor = menu.findItem(R.id.vendor);
            vendor.setVisible(menu_map.containsValue("Vendor"));
//            MenuItem setting = menu.findItem(R.id.setting);
//            setting.setVisible(menu_map.containsValue("Setting"));
            //MenuItem dashboard = navigationView.getMenu().findItem(R.id.dashboard);
            //dashboard.setVisible(menu_map.containsValue("Dashboard"));
            MenuItem dashboard_layer = navigationView.getMenu().findItem(R.id.dashboard_layer);
            dashboard_layer.setVisible(menu_map.containsValue("Layer"));
            MenuItem dashboard_grower = navigationView.getMenu().findItem(R.id.dashboard_grower);
            dashboard_grower.setVisible(menu_map.containsValue("Grower"));

//        MenuItem dashboard_layer
//            MenuItem analysis = navigationView.getMenu().findItem(R.id.analysis);
//            analysis.setVisible(menu_map.containsValue("Analysis"));
            MenuItem chick_in = navigationView.getMenu().findItem(R.id.chickin);
            chick_in.setVisible(menu_map.containsValue("Chick In"));
//            MenuItem chick_in_history = navigationView.getMenu().findItem(R.id.chickin_history);
//            chick_in_history.setVisible(menu_map.containsValue("Chick In"));
            MenuItem recording = navigationView.getMenu().findItem(R.id.recording);
            recording.setVisible(menu_map.containsValue("Recording"));
//            MenuItem recording_history = navigationView.getMenu().findItem(R.id.history);
//            recording_history.setVisible(menu_map.containsValue("Recording"));
            MenuItem sales = navigationView.getMenu().findItem(R.id.sales);
            sales.setVisible(menu_map.containsValue("Sales"));
//        MenuItem sales_order = navigationView.getMenu().findItem(R.id.sales_order);
//        sales_order.setVisible(menu_map.containsValue("Sales"));

            MenuItem price_list = navigationView.getMenu().findItem(R.id.price_list);
            price_list.setVisible(menu_map.containsValue("Expense"));
            MenuItem general_expense = navigationView.getMenu().findItem(R.id.general_expense);
            general_expense.setVisible(menu_map.containsValue("Expense"));


            MenuItem recording_offline = navigationView.getMenu().findItem(R.id.recording_offline);
            recording_offline.setVisible(false);
            MenuItem chickin_offline = navigationView.getMenu().findItem(R.id.chickin_offline);
            chickin_offline.setVisible(false);

        } else{

            MenuItem farm = menu.findItem(R.id.farm);
            farm.setVisible(false);
            MenuItem location = menu.findItem(R.id.location);
            location.setVisible(false);
            MenuItem flock = menu.findItem(R.id.flock);
            flock.setVisible(false);
            MenuItem member = menu.findItem(R.id.member);
            member.setVisible(false);
            MenuItem strain = menu.findItem(R.id.strain);
            strain.setVisible(false);
            MenuItem feed = menu.findItem(R.id.feed);
            feed.setVisible(false);
            MenuItem medvac = menu.findItem(R.id.medvac);
            medvac.setVisible(false);
            MenuItem buyer = menu.findItem(R.id.buyer);
            buyer.setVisible(false);
            MenuItem vendor = menu.findItem(R.id.vendor);
            vendor.setVisible(false);
//            MenuItem setting = menu.findItem(R.id.setting);
//            setting.setVisible(false);
            //MenuItem dashboard = navigationView.getMenu().findItem(R.id.dashboard);
            //dashboard.setVisible(menu_map.containsValue("Dashboard"));
            MenuItem dashboard_grower = navigationView.getMenu().findItem(R.id.dashboard_grower);
            dashboard_grower.setVisible(false);
            //        MenuItem dashboard_layer
            MenuItem dashboard_layer = navigationView.getMenu().findItem(R.id.dashboard_layer);
            dashboard_layer.setVisible(false);
            MenuItem price_list = navigationView.getMenu().findItem(R.id.price_list);
            price_list.setVisible(false);
            MenuItem general_expense = navigationView.getMenu().findItem(R.id.general_expense);
            general_expense.setVisible(false);

//            MenuItem analysis = navigationView.getMenu().findItem(R.id.analysis);
//            analysis.setVisible(false);
            MenuItem chick_in = navigationView.getMenu().findItem(R.id.chickin);
            chick_in.setVisible(false);
//            MenuItem chick_in_history = navigationView.getMenu().findItem(R.id.chickin_history);
//            chick_in_history.setVisible(false);
            MenuItem recording = navigationView.getMenu().findItem(R.id.recording);
            recording.setVisible(false);
//            MenuItem recording_history = navigationView.getMenu().findItem(R.id.history);
//            recording_history.setVisible(false);
            MenuItem sales = navigationView.getMenu().findItem(R.id.sales);
            sales.setVisible(false);
            MenuItem recording_offline = navigationView.getMenu().findItem(R.id.recording_offline);
            recording_offline.setVisible(false);
            MenuItem chickin_offline = navigationView.getMenu().findItem(R.id.chickin_offline);
            chickin_offline.setVisible(false);
        }

        //-------end menu jika pakai billing

        return result;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.location) {
            Intent intent = new Intent(this, LocationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.flock){
            Intent intent = new Intent(this, FlockActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.member){
            Toast.makeText(getApplicationContext(),"Member clicked",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, UserActivityNew.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.strain){
            Intent intent = new Intent(this, StrainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.feed){
            Intent intent = new Intent(this, FeedActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.medvac){
            Intent intent = new Intent(this, MedicationVaccinationNewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.buyer){
            Intent intent = new Intent(this, BuyerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else if (id == R.id.vendor){
            Intent intent = new Intent(this, VendorActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
//        else if (id == R.id.setting){
//            Intent intent = new Intent(this, SettingDashboardActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            startActivity(intent);
//        }
        else if (id == R.id.farm){
            Intent intent = new Intent(this, AppListFarm.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
//        else if (id == R.id.miActivation){
//
//            // check local data server
//            SQLiteDatabase dbSQL = db.getReadableDatabase();
//            String count = "SELECT count(*) FROM farm_expired_date";
//            Cursor mcursor = dbSQL.rawQuery(count, null);
//            mcursor.moveToFirst();
//            int icount = mcursor.getInt(0);
//            if(icount > 0){
//                Intent intent = new Intent(this, renewalGoogleBillingActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(intent);
//            } else{
//                Intent intent = new Intent(this, createAccountFarmMainActivity.class);
//                startActivity(intent);
//            }
//
//
//        }
        else if (id == R.id.help_feedback){
            Toast.makeText(getApplicationContext(),"Help & Feedback clicked",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, ReportByFlockActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        /*if (id == R.id.dashboard) {
            Intent intent = new Intent(this, DashboardMainActivity.class);
            startActivity(intent);
        } else */if (id == R.id.sales) {
//            Intent intent = new Intent(this, SalesActivity.class);
//            startActivity(intent);
            Fragment fragment = new SalesFragment();
            displaySelectedFragment(fragment, "SalesFragment");
//        } else if (id == R.id.sales_order) {
//            Intent intent = new Intent(this, DraftSaleMainActivity.class);
//            startActivity(intent);
        } else if (id == R.id.dashboard_grower) {
//            Intent intent = new Intent(this, DashboardGrowerMainActivity.class);
//            startActivity(intent);
            Fragment fragment = new DashboardGrowerFragment();
            displaySelectedFragment(fragment,"DashboardGrowerFragment");
        } else if (id == R.id.dashboard_layer) {
//            Intent intent = new Intent(this, DashboardLayerMainActivity.class);
//            startActivity(intent);
            Fragment fragment = new DashboardLayerFragment();
            displaySelectedFragment(fragment, "DashboardLayerFragment");
        } else if (id == R.id.price_list) {
//            Intent intent = new Intent(this, PricelistMainActivity.class);
//            startActivity(intent);
            Fragment fragment = new PriceListFragment();
            displaySelectedFragment(fragment, "PriceListFragment");
        }else if (id == R.id.general_expense) {
//            Intent intent = new Intent(this, ExpenseMainActivity.class);
//            startActivity(intent);
            Fragment fragment = new GeneralExpenseFragment();
            displaySelectedFragment(fragment, "GeneralExpenseFragment");
//        } else if (id == R.id.analysis) {
//            Intent intent = new Intent(this, AnalysisMainActivity.class);
//            startActivity(intent);
        } else if (id == R.id.recording) {
//            Intent intent = new Intent(this, RecordingMainActivity.class);
//            //Intent intent = new Intent(this, BodyWeightMainActivity.class);
//            startActivity(intent);
            Fragment fragment = new RecordingFragment();
            displaySelectedFragment(fragment, "RecordingFragment");
//        } else if (id == R.id.history) {
////            Intent intent = new Intent(this, HistoryActivity.class);
////            //Intent intent = new Intent(this, BodyWeightMainActivity.class);
////            startActivity(intent);
//            Fragment fragment = new RecordingHistoryFragment();
//            displaySelectedFragment(fragment, "RecordingHistoryFragment");
        } else if (id == R.id.chickin) {
//            Intent intent = new Intent(this, ChickinHistoryMainActivity.class);
//            //Intent intent = new Intent(this, FlockListActivity.class);
//            startActivity(intent);
            Fragment fragment = new ChickinFragment();
            displaySelectedFragment(fragment, "ChickinFragment");
            //LayerFarm.getInstance().processFlockStatus(22152);
//        } else if (id == R.id.chickin_history) {
//            Intent intent = new Intent(this, ChickinHistoryMainActivity.class);
//            startActivity(intent);

//        } else if (id == R.id.setting) {
//            // intent = new Intent(this, SettingMainActivity.class);
//            Intent intent = new Intent(this, SettingDashboardActivity.class);
//            //Intent intent = new Intent(this, SyncLayerfarmActivity.class);
//            startActivity(intent);
        } else if (id == R.id.dashboard_offline) {
            //Intent intent = new Intent(this, offlineDashboard.class);
            //Intent intent = new Intent(this, StrainListActivity.class);
            Intent intent = new Intent(this, FlockListActivity.class);
            startActivity(intent);
        } else if (id == R.id.recording_offline) {

            //Intent intent = new Intent(this, renewalGoogleBillingActivity.class);
            Intent intent = new Intent(this, HistoryRecordingOfflineSubmenu.class);
            //Intent intent = new Intent(this, RecordingOfflineMainActivity.class);
            startActivity(intent);
        } else if (id == R.id.chickin_offline) {
            Intent intent = new Intent(this, UnregisteredChickinMainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("App failed connect to server");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Are you want to setting server ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        startActivity(new Intent(MainActivity.this,ServerSettingActivity.class));
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //MainActivity.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("zzzi","Main Activity onResume");
    }
    public void Start(){
        mRun.run();
    }
    public void Stop(){
        //progressBar.setVisibility(View.GONE);

        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,100);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            Map<String, String> role = RetrofitData.getInstance().getUser().getRoles();
            Log.d("zzzz","try to connect website");
            Log.d("zzzz","status = "+RetrofitData.getInstance().getStatus());
            Log.d("zzzz","uname = "+RetrofitData.getInstance().getFarm_name()+" passwd = "+passwd);
            Log.d("zzzz","email = "+email+" passwd = "+passwd+" role = "+RetrofitData.getInstance().getUser().getRoles());
            Log.d("zzzz"," name = "+RetrofitData.getInstance().getUser().getName()+"url = "+RetrofitData.getInstance().getBaseUrl());
//            if (email == uname && passwd == passwd){
                Log.d("zzzz","email sama semua");
                if (RetrofitData.getInstance().getStatus() != "") {
                    Log.d("zzzz","status != null");
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        //loadingCheckfarm.dismiss();
                        String name = RetrofitData.getInstance().getFarm_name();
                        String url = RetrofitData.getInstance().getBaseUrl();
//                        User user = RetrofitData.getInstance().getUser();
                        user_name.setText(RetrofitData.getInstance().getUser().getName());
                        farm_name.setText(name);
                        farm_code.setText(url);
                        farm_role.setText(RetrofitData.getInstance().getUser().getRoles().toString());
                        Log.d("zzz","url = "+url+" role = "+RetrofitData.getInstance().getUser().getRoles());

                        //delay to prevent CSRF Error
//                        int secs = 2; // Delay in seconds
//                        delay.delay(secs, new delay.DelayCallback() {
//                            public void afterDelay() {
                                // Do something after delay

                                getLayerFarmDashboard();
//                            }
//                        });

                        Stop();
                    } else{
                        Toast.makeText(MainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Log.d("zzz","Failed dashboard");
//                        loadingCheckfarm.dismiss();
                        if (RetrofitData.getInstance().getStatus().equals("Wrong username or password")){
                            AskOption();
                        }
                        else {
                            Intent intent = new Intent(MainActivity.this, TimeOut.class);
                            startActivity(intent);
                        }
                        Stop();
                    }
                }
        }
    };
    public void getMenu(){
        User data_user = RetrofitData.getInstance().getUser();
        try {
            Map<String, String> role = data_user.getRoles();
            role.remove("2");
        if (role.containsValue("administrator")){
            Fragment fragment = new OnlineDashboardFragment();
            displaySelectedFragment(fragment,"OnlineDashboardFragment");
        }
        else if (role.containsValue("operator")){
            Fragment fragment = new RecordingFragment();
            displaySelectedFragment(fragment,"RecordingFragment");
        }
        else if (role.containsValue("sales")){
            Fragment fragment = new SalesFragment();
            displaySelectedFragment(fragment,"SalesFragment");
        }
            ArrayList<String> roles = new ArrayList<>(role.values());
            Log.d("zzz","role "+role.toString());
            String[] arr = roles.toArray(new String[0]);
            SQLiteDatabase dbSQL = db.getReadableDatabase();
            String query = "SELECT * FROM menu WHERE id in (SELECT menu_id FROM role_menu WHERE role_id in (SELECT id FROM role WHERE machine_name IN ("+makePlaceholders(arr.length)+")))";
            Cursor cursor = dbSQL.rawQuery(query, arr);
            menus = new ArrayList<>();
            menu_map = new HashMap<>();
            for (int i = 0; i< cursor.getCount(); i++){
                cursor.moveToPosition(i);
                menus.add(cursor.getString(cursor.getColumnIndex("name")));
                menu_map.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("name")));
            }
            ArrayList<String> values = new ArrayList<>();
            // looping through all rows and adding to list

        }catch (NullPointerException e ){
            Toast.makeText(getApplicationContext(),"Null Pointer Exception",Toast.LENGTH_SHORT).show();
        }
        String name = RetrofitData.getInstance().getFarm_name();
        user_name.setText(RetrofitData.getInstance().getUser().getName());
        farm_name.setText(name);
        farm_code.setText(RetrofitData.getInstance().getBaseUrl());
        farm_role.setText(RetrofitData.getInstance().getUser().getRoles().toString());
        try{
            Map<String, String> roles = RetrofitData.getInstance().getUser().getRoles();
            String role = "";
            for (Map.Entry<String, String> entry : roles.entrySet())
            {
                role += entry.getValue().toString().substring(0,1).toUpperCase()+entry.getValue().substring(1)+" ";

            }

            farm_role.setText(role);


        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Null Pointer Exception",Toast.LENGTH_SHORT).show();
        }

        invalidateOptionsMenu();
    }

    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    private void showDialogResetting(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Setting server wrong or server down");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Check your setting data ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        startActivity(new Intent(MainActivity.this,ServerSettingActivity.class));
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //MainActivity.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    //--------------start online dashboard------------------
    public static MainActivity getInstance() {
        return instance;
    }

    public void getLayerFarmDashboard(){
//        loadingCheckfarm.dismiss();
        //Fragment fragment = new OfflineDashboardFragment();
//                        Fragment fragment = new OnlineDashboardFragment();
//                        displaySelectedFragment(fragment);
        getMenu();

        if (SubscribeStatus == 1){
            AskSubcsBefore();
        }


    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Response Empty, Please Try Again!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }


    public void countToTen() {
        System.out.println("Hello, World!");
        for (int i = 1; i<=10; i++ ) {
            System.out.println(i);
        }
    }

    @Override
    protected void onDestroy() {
        //    billingClient.endConnection();
        super.onDestroy();
    }


    void querySkuList() {
        List skuList = new ArrayList<>();
        skuList.add("lfm1.0alfa_1");
        skuList.add("lfm1.0alfa_member");
        skuList.add("lfm1.0alfa_member_15");

/*        skuList.add("android.test.purchased");  // prepared by Google
        skuList.add("android.test.canceled");
        skuList.add("android.test.refunded");
        skuList.add("android.test.item_unavailable");*/
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        //params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        StringBuffer resultStr = new StringBuffer("");
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            mySkuDetailsList = skuDetailsList;
                            if (skuDetailsList != null) {
                                for (Object item : skuDetailsList) {
                                    SkuDetails skuDetails = (SkuDetails) item;
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();
                                    resultStr.append("Sku=" + sku + " Price=" + price + "\n");
                                }
                            } else {
                                resultStr.append("No Sku");
                            }
                            Log.d("resultStr"," "+ resultStr);
                            //textView1.setText(resultStr);
                        } else {
                            showResponseCode(responseCode);
                        }
                    }
                });
    }


    void startPurchase(String sku) {
        Log.d("sku", "sku= " + sku);
        SkuDetails skuDetails = getSkuDetails(sku);
        if (skuDetails != null) {
            BillingFlowParams params = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetails)
                    .build();
            BillingResult billingResult = billingClient.launchBillingFlow(this, params);
            showResponseCode(billingResult.getResponseCode());
        }
    }


    SkuDetails getSkuDetails(String sku) {
        SkuDetails skuDetails = null;
        if(mySkuDetailsList==null){
            //textView1.setText("Exec [Get Skus] first");
            Log.d("resultStr", "Exec [Get Skus] first");
        }else {
            for (SkuDetails sd : mySkuDetailsList) {
                if (sd.getSku().equals(sku)) skuDetails = sd;
            }
            if (skuDetails == null) {
                //textView1.setText(sku + " is not found");
                Log.d("resultStr", " "+ sku + " is not found");
            }
        }
        return skuDetails;
    }

    BillingClient mBillingClient;
    // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
    String base64Key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlpBRn+ek2E7Zb/HfPM4Z4rx3TIFo92h7XNPDMjfRDx7cBqEaJNo0WLe/b+9YNkHGD6D5rtD0d4Gta5KhEfzSRbQiEbqSQzlzJGGsl9uSqbLE4k8WS2apTgukU9n5pCdapeAp0613CmM7LoQjEDnknwVEOukCZjPExEPgLOU2rpq0vGibYaDeTsw3TGZboIIs/IP19Q4EmwmfZ0NtqWd/EndAwGRRax2fvZwp6Hq7OTIRTa/dAKriuABUQtOA272vj9ASrEG88UCAX4AycuBQeXr3IrMgg1XvvEZdxM7l3C5PfmtH+wroetcQ01SFrWEVqm7NUB4cJBgGn15aNhi1wwIDAQAB";

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {

        StringBuffer resultStr = new StringBuffer("");
        int billingResultCode = billingResult.getResponseCode();
        if (billingResultCode == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                String state = handlePurchase(purchase);
                String sku = purchase.getSku();
                resultStr.append(sku).append("\n");
                resultStr.append(" State=").append(state).append("\n");
/*

                if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
                    // Invalid purchase
                    // show error to user
                    Log.i("tes", "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
                    return;
                } else {
                    // purchase is valid
                    // Perform actions
                    Log.i("tes", "but signature is bad. Skipping...");
                }
*/

            }
            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        } else {
            // Handle error codes.
            showResponseCode(billingResultCode);
        }
    }


    String handlePurchase(Purchase purchase) {
        String stateStr = "error";
        int purchaseState = purchase.getPurchaseState();
        if (purchaseState == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.
            stateStr = "purchased";
            // Acknowledge the purchase if it hasn't already been acknowledged.
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, this);
            }
        }else if(purchaseState == Purchase.PurchaseState.PENDING){
            stateStr = "pending";
        }else if(purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE){
            stateStr = "unspecified state";
        }
        return stateStr;
    }



    @Override
    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
        int responseCode = billingResult.getResponseCode();
        if(responseCode != BillingClient.BillingResponseCode.OK) {
            showResponseCode(responseCode);
        }
    }


//    void queryOwned(){
//        StringBuffer resultStr = new StringBuffer("");
//        Purchase.PurchasesResult purchasesResult
//                = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
//        int responseCode = purchasesResult.getResponseCode ();
//        if(responseCode== BillingClient.BillingResponseCode.OK){
//            resultStr.append("Query Success\n");
//            List<Purchase> purchases = purchasesResult.getPurchasesList();
//            if(purchases.isEmpty()){
//                resultStr.append("Owned Nothing");
//                Fragment fragment = new OfflineDashboardFragment();
//                displaySelectedFragment(fragment, "OfflineDashboardFragment");
//            } else {
//                for (Purchase purchase : purchases) {
//                    resultStr.append(purchase.getSku()).append("\n");
//                    resultStr.append(purchase.getPurchaseState()).append("\n");
//                    //resultStr.append(purchase.isAutoRenewing()).append("\n");
//                    /*if (purchase.isAutoRenewing()){
//                        member_items.add(purchase.getSku());
//                    }*/
//                    purchase_state = purchase.getPurchaseState();
//
//                    Log.d("purchase", " "+ purchase.getPurchaseState());
//                    if (purchase_state == 1){
//                        //countToTen();
//                        SQLiteDatabase dbSQL = db.getReadableDatabase();
//                        String selectQuery = "SELECT value FROM variables";
//                        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
//                        ArrayList<String> values = new ArrayList<>();
//                        // looping through all rows and adding to list
//                        if (cursor.moveToFirst()) {
//                            do {
//                                String value = cursor.getString(cursor.getColumnIndex("value"));
//                                values.add(value);
//                            } while (cursor.moveToNext());
//
//
//                        }
//                        cursor.close();
//                        //Log.d("ssss", "values = " + values);
//
//                        if (values.isEmpty()){
//                            //Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
//                            //showDialog();
//                            //getData();
//
//
//
//                        }else {
//                            url = values.get(0);
//                            uname = values.get(1);
//                            passwd = values.get(2);
//                            LayerFarm.getInstance().Connect("http://"+url+"/", uname, passwd);
//                            RetrofitData.getInstance().setStatus("");
//                            Start();
//
//
//                            //upload_data_to_layer();
//
//               /* if (status == "fail"){
//                    showDialogResetting();
//                }else{
//                    LayerFarm.getInstance().Connect("http://"+url+"/", username, password);
//                }*/
//                        }
//                    }else{
//                        Fragment fragment = new OfflineDashboardFragment();
//                        displaySelectedFragment(fragment, "OfflineDashboardFragment");
//                    }
//                }
//
//            }
//            //textView1.setText(resultStr);
//            //Log.d("resultStr", " "+ resultStr);
//
//            //Toast.makeText(getApplicationContext(),resultStr,Toast.LENGTH_SHORT).show();
//            //Toast.makeText(getApplicationContext(),resultStr,Toast.LENGTH_SHORT).show();
//
//        }else{
//            showResponseCode(responseCode);
//        }
//    }
//
//
//
//    void queryPurchaseHistory() {
//        billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS,
//                new PurchaseHistoryResponseListener() {
//                    @Override
//                    public void onPurchaseHistoryResponse(BillingResult billingResult,
//                                                          List<PurchaseHistoryRecord> purchasesList) {
//                        int responseCode = billingResult.getResponseCode();
//                        if (responseCode == BillingClient.BillingResponseCode.OK) {
//                            if (purchasesList == null || purchasesList.size() == 0) {
//                                //textView1.setText("No History");
//                                Log.d("No History", " "+ "No History");
//                            } else {
//                                for (PurchaseHistoryRecord purchase : purchasesList) {
//                                    // Process the result.
//                                    //textView1.setText("Purchase History=" + purchase.toString() + "\n");
//                                    Log.d("No History", " "+ purchase.toString());
//                                }
//                            }
//                        } else {
//                            showResponseCode(responseCode);
//                        }
//                    }
//                });
//    }

    void showResponseCode(int responseCode){
        Fragment fragment = new OfflineDashboardFragment();
        switch(responseCode){
            case BillingClient.BillingResponseCode.OK:
                // textView1.setText("OK");
                Log.d("resultStr", " "+ "OK");
                break;
            case BillingClient.BillingResponseCode.USER_CANCELED:
                //textView1.setText("USER_CANCELED");
                Log.d("resultStr", " "+ "USER CANCELLED");

                break;
            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                //textView1.setText("SERVICE_UNAVAILABLE");
                Log.d("resultStr", " "+ "SERVICE_UNAVAILABLE");

                break;
            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                Log.d("resultStr", " "+ "BILLING_UNAVAILABLE");

                //textView1.setText("BILLING_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                Log.d("resultStr", " "+ "ITEM_UNAVAILABLE");

                //textView1.setText("ITEM_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                Log.d("resultStr", " "+ "DEVELOPER_ERROR");

                //textView1.setText("DEVELOPER_ERROR");
                break;
            case BillingClient.BillingResponseCode.ERROR:
                Log.d("resultStr", " "+ "ERROR");

                //textView1.setText("ERROR");
                break;
            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                Log.d("resultStr", " "+ "ITEM_ALREADY_OWNED");
                //textView1.setText("ITEM_ALREADY_OWNED");
                break;
            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                Log.d("resultStr", " "+ "ITEM_NOT_OWNED");

                //textView1.setText("ITEM_NOT_OWNED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                Log.d("resultStr", " "+ "SERVICE_DISCONNECTED");
                //Fragment fragment = new OfflineDashboardFragment();

                //textView1.setText("SERVICE_DISCONNECTED");
                break;
            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                Log.d("resultStr", " "+ "FEATURE_NOT_SUPPORTED");
                //textView1.setText("FEATURE_NOT_SUPPORTED");

                break;
        }
    }

//    private void getExpiredFarm(){
//        //countToTen();
//
//        SQLiteDatabase dbSQL = db.getReadableDatabase();
//        String selectQuery = "SELECT * FROM farm_expired_date";
//        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst()) {
//            do {
//                expiredDateData = cursor.getString(2);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//
//        Calendar calendar = Calendar.getInstance();
//
//
//        Log.d("expiredDateData : ", " "+ expiredDateData);
//        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
//        String strDateNow = myFormat.format(calendar.getTime());
//        Log.d("strDateNow : ", " "+ strDateNow);
//        String dateBeforeString = "2014-01-31";
//        String dateAfterString = "2014-02-32";
//
//        if (expiredDateData != null && !expiredDateData.isEmpty()) {
//
//
//            try {
//            /*Date dateBefore = myFormat.parse(dateBeforeString);
//            Date dateAfter = myFormat.parse(dateAfterString);*/
//
//                Date dateBefore = myFormat.parse(strDateNow);
//                Date dateAfter = myFormat.parse(expiredDateData);
//
//                long difference = dateAfter.getTime() - dateBefore.getTime();
//                daysBetween = (difference / (1000 * 60 * 60 * 24));
//                Log.d("Number of Days : ", " " + daysBetween);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            if (daysBetween <= 0.0) {
//                //Toast.makeText(getApplicationContext(),"Subcs limit",Toast.LENGTH_SHORT).show();
//                AskSubcsEnd();
//                purchase_state = 0;
//                Log.d("Farm : ", " "+"Farm masih kosong");
//                Log.d("purchase_state : ", " "+purchase_state);
//                Fragment fragment = new OfflineDashboardFragment();
//                displaySelectedFragment(fragment, "OfflineDashboardFragment");
//            } else if (daysBetween >= 1 && daysBetween <= 7) {
//                //Toast.makeText(getApplicationContext(),"Subcs will end i 7 days",Toast.LENGTH_SHORT).show();
//                AskSubcsBefore();
//            } else {
//
//            }
//        }else{
//
//        }
//
//    }

    private AlertDialog AskSubcsBefore() {
        AlertDialog myQuittingDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Your subscription close to the end")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return myQuittingDialogBox;
    }


//    private void changeDashboard(){
////        loadingCheckfarm = ProgressDialog.show(MainActivity.this, "Connect", "Please wait...", true, false);
//
//        if (purchase_state == 1){
////            if ()
//            //countToTen();
//            SQLiteDatabase dbSQL = db.getReadableDatabase();
//            String selectQuery = "SELECT value FROM variables";
//            Cursor cursor = dbSQL.rawQuery(selectQuery, null);
//            ArrayList<String> values = new ArrayList<>();
//            // looping through all rows and adding to list
//            if (cursor.moveToFirst()) {
//                do {
//                    String value = cursor.getString(cursor.getColumnIndex("value"));
//                    values.add(value);
//                } while (cursor.moveToNext());
//            }
//            cursor.close();
//            //Log.d("ssss", "values = " + values);
////            RetrofitData.getInstance().setStatus("");
//
//            if (values.isEmpty()){
//                Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
//                //loadingCheckfarm.dismiss();
//                purchase_state = 0;
//                changeDashboard();
//                //showDialog();
//                //getData();
//            }else {
////                url = values.get(0);
//                code = values.get(0);
//                uname = values.get(1);
//                passwd = values.get(2);
//                Log.d("xxx", "code = " + code);
//                Log.d("xxx", "uname = " + uname);
//                Log.d("xxx", "passwd = " + passwd);
//
//                LayerFarm.getInstance(this).Connect("http://"+code+"."+Env.DOMAIN+"/", uname, passwd);
//                if (RetrofitData.getInstance().getStatus() == ""){
//                    RetrofitData.getInstance().setStatus("");
//                    startActivity(new Intent(this, SplashScreenActivity.class));
//                }
////
//
////                finish();
//                else {
//                    Start();
//                }
//
//            }
//        }else{
////            loadingCheckfarm.dismiss();
////            imgDashboard.setVisibility(View.GONE);
////            Fragment fragment = new OfflineDashboardFragment();
////            displaySelectedFragment(fragment, "OfflineDashboardFragment");
//            startActivity(new Intent(this, IntroActivity.class));
//            finish();
//
//        }
//    }
//
//    public void Start_Connect(){
//        mRunServer.run();
//    }
//    public void Stop_connect(){
//
//        mHandlerServer.removeCallbacks(mRunServer);
//    }
//    private Runnable mRunServer = new Runnable() {
//        @Override
//        public void run() {
//            mHandlerServer.postDelayed(mRunServer,5000);
//            String email = RetrofitData.getInstance().getEmail();
//            String passwd = RetrofitData.getInstance().getPassword();
//
////            if (email == usernameserver && passwd == passwordserver){
//                if (RetrofitData.getInstance().getStatus() != "") {
//                    if (RetrofitData.getInstance().getStatus() == "Success") {
//                        Toast.makeText(MainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
//                        Stop_connect();
////                        StartCheckFarmStatus();
//
//                        purchase_state = 1;
//                        changeDashboard();
//                    } else{
//                        Toast.makeText(MainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
////                        loadingCheckfarm.dismiss();
////                        Fragment fragment = new TimeOutFragment();
////                        displaySelectedFragment(fragment, "TimeOutFragment");
//                        Intent intent = new Intent(MainActivity.this, TimeOut.class);
//                        startActivity(intent);
//                        Stop_connect();
//                    }
//                }
////            }
//
//        }
//    };


}
