package com.layerfarm.layerfarmmanager.Navigation.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.layerfarmmanager.Navigation.Model.ModelAccount;
import com.layerfarm.layerfarmmanager.R;

import java.util.ArrayList;

public class AdapterRecyclerAccount extends RecyclerView.Adapter<AdapterRecyclerAccount.ViewHolder> {
    ArrayList<ModelAccount> model_account;
    String active_user;
    public AdapterRecyclerAccount(String active_user, ArrayList<ModelAccount> model_account){
        this.model_account = model_account;
        this.active_user = active_user;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView farm_name;
        TextView farm_code;
        TextView farm_role;
        CardView cv_account;
        public ViewHolder(View itemView) {
            super(itemView);
            farm_name = (TextView) itemView.findViewById(R.id.farm_name);
            farm_code = (TextView) itemView.findViewById(R.id.farm_code);
            farm_role = (TextView) itemView.findViewById(R.id.farm_role);
            cv_account = (CardView) itemView.findViewById(R.id.cv_account);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_rv_account, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerAccount.ViewHolder holder, int position) {
        final String name = model_account.get(position).getFarm_name();
        final String code = model_account.get(position).getFarm_code();
        final String role = model_account.get(position).getFarm_role();
        Log.d("zzzz","active = "+active_user);
        Log.d("zzzz","userbame = "+name);
        if (active_user.equals(name)){
            Log.d("zzzz","same");
            holder.cv_account.setBackgroundColor(Color.GREEN);
        }
        holder.farm_name.setText(name);
        holder.farm_code.setText(code);
        holder.farm_role.setText(role);
        holder.cv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Log.d("zzzz","Clicked");
            }
        });
    }


    @Override
    public int getItemCount() {
        return model_account.size();
    }
}
