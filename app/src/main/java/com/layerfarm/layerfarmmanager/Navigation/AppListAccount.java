package com.layerfarm.layerfarmmanager.Navigation;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarmmanager.MainActivity;
import com.layerfarm.layerfarmmanager.Navigation.Adapter.AdapterListAccount;
import com.layerfarm.layerfarmmanager.Navigation.Model.ModelAccount;
import com.layerfarm.layerfarmmanager.R;

import java.util.ArrayList;

public class AppListAccount extends Activity {
    ViewGroup farm, partner;
//    TextView add_partner;
    ProgressBar progressBar;
//    RecyclerView listAccount;
    ListView listAccount;
//    private RecyclerView.Adapter adapter;
    AdapterListAccount adapter;
    private LinearLayoutManager layoutManager;
    ArrayList<ModelAccount> modelAccounts = new ArrayList<>();
    DatabaseHelper db;
    SQLiteDatabase dbase;
    private Handler mHandler = new Handler();
    String url, code, uname, passwd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_list_account);

        db = new DatabaseHelper(this);
        dbase = db.getReadableDatabase();

        Toolbar toolbarTop = (Toolbar) findViewById(com.layerfarm.recording.R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(com.layerfarm.recording.R.id.title);
        mTitle.setText("Connection");

//        listAccount = (RecyclerView) findViewById(R.id.rv_listAccount);
        listAccount = (ListView) findViewById(R.id.rv_listAccount);
//        add_partner = (TextView) findViewById(R.id.add_partner_farm);
        partner = (ViewGroup) findViewById(R.id.partner_farm_list);
        progressBar = (ProgressBar) findViewById(R.id.proggressBar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(getApplicationContext(), AppAddPartner.class);
                startActivity(intent);

            }
        });

        //Jika menggunakan recyclerview
//        listAccount.setHasFixedSize(true);
//        layoutManager = new LinearLayoutManager(this);
//        listAccount.setLayoutManager(layoutManager);
        listAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String full_name = modelAccounts.get(position).getFull_name();
                final String farm_code = modelAccounts.get(position).getFarm_code();
                final String password = modelAccounts.get(position).getPassword();
//                url = farm_code;
                code = farm_code;
                uname = full_name;
                passwd = password;
                connect_server();

            }
        });

//        add_partner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent;
//                intent = new Intent(getApplicationContext(), AppAddPartner.class);
//                startActivity(intent);
//            }
//        });
        
        fetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        modelAccounts.clear();
        fetchData();
    }
    @Override
    public void onBackPressed() {
//        Intent intent;
//        intent = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(intent);
        finish();

    }

    public void close(View view){
//        Intent intent;
//        intent = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(intent);
        finish();
    }
    public void fetchData(){
        modelAccounts.clear();
        SQLiteDatabase dbase = db.getWritableDatabase();
        String value_username ="";
        String value_url = "";
        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        String code = variables.getString("code", null);
        String uname = variables.getString("username", null);
        String passwd = variables.getString("password", null);
        if(code != null && uname != null && passwd != null){
            value_username = uname;
            value_url = passwd;
        }
//        String select = "SELECT value from variables WHERE name = 'username'";
//        Cursor cursor_active = dbase.rawQuery(select,null);
//        if (cursor_active.moveToFirst()) {
//            value_username = cursor_active.getString(cursor_active.getColumnIndex("value"));
//        }
//        cursor_active.close();
//
////        String select_url = "SELECT value from variables WHERE name = 'url'";
////        Cursor cursor_active_url = dbase.rawQuery(select_url,null);
//        String select_code = "SELECT value from variables WHERE name = 'url'";
//        Cursor cursor_active_url = dbase.rawQuery(select_code,null);
//        if (cursor_active_url.moveToFirst()) {
//            value_url = cursor_active_url.getString(cursor_active_url.getColumnIndex("value"));
//        }
//        cursor_active_url.close();

        String selectQuery = "SELECT * FROM user_farm";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            ModelAccount model = new ModelAccount();
            model.id = cursor.getString(cursor.getColumnIndex("id"));
            model.full_name = cursor.getString(cursor.getColumnIndex("full_name"));
            model.farm_name = cursor.getString(cursor.getColumnIndex("farm_name"));
            model.farm_code = cursor.getString(cursor.getColumnIndex("farm_code"));
            model.farm_role = cursor.getString(cursor.getColumnIndex("role"));
            model.password = cursor.getString(cursor.getColumnIndex("password"));
            model.is_owner = cursor.getString(cursor.getColumnIndex("is_owner"));
            modelAccounts.add(model);
        }
        cursor.close();
        adapter = new AdapterListAccount(AppListAccount.this, value_username, value_url, modelAccounts);
        adapter.notifyDataSetChanged();
        listAccount.setAdapter(adapter);

    }

    private void connect_server(){
//
//        //get new layerfarm class
//        String selectQuery = "SELECT value FROM variables";
//        Cursor cursor = dbase.rawQuery(selectQuery, null);
//        ArrayList<String> values = new ArrayList<>();
//        if (cursor.moveToFirst()) {
//            do {
//                String value = cursor.getString(cursor.getColumnIndex("value"));
//                values.add(value);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        if (values.isEmpty()){
//            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();

//        }else {
//            url = values.get(0);
//            uname = values.get(1);
//            passwd = values.get(2);
            LayerFarm.getInstance().Connect(this, "http://"+code+ "." + Env.DOMAIN+"/", uname, passwd);
            RetrofitData.getInstance().setStatus("");
            Start();
//        }
    }
    public void Start(){
        progressBar.setVisibility(View.VISIBLE);
        mRun.run();
    }
    public void Stop(){
        progressBar.setVisibility(View.GONE);
        mHandler.removeCallbacks(mRun);
        fetchData();
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            Map<String, String> role = RetrofitData.getInstance().getUser().getRoles();
//            if (email == uname && passwd == passwd){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(AppListAccount.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        InsertUser();
                        Stop();
//                        finish();
                    } else{
                        Toast.makeText(AppListAccount.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Stop();
                        progressBar.setVisibility(View.GONE);
                        if (RetrofitData.getInstance().getStatus().equals("Wrong username or password")){
                            AskOption();
                        }
//                        finish();
                    }
                }
//            }

        }
    };
    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Log In Failed!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void InsertUser(){
        String url_address = code;
        String url_username = uname;
        String url_password = passwd;
        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = variables.edit();
        editor.putString("code", url_address);
        editor.putString("username", url_username);
        editor.putString("password", url_password);
        editor.apply();
        Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();

//
//        Cursor result = dbase.rawQuery("SELECT value FROM variables", null);
//        if (result.getCount() == 0) {
//            // TODO Auto-generated method stub
//
////            String[] _VARIABLES_INSERT = {"INSERT INTO variables (name, value) VALUES ('url', '"+url+"')","INSERT INTO VARIABLE (name, value) VALUES ('username', '"+uname+"')", "INSERT INTO VARIABLE (name, value) VALUES ('password', '"+passwd+"')"};
//            String[] _VARIABLES_INSERT = {"INSERT INTO variables (name, value) VALUES ('url', '"+code+"')","INSERT INTO variables (name, value) VALUES ('username', '"+uname+"')", "INSERT INTO variables (name, value) VALUES ('password', '"+passwd+"')"};
//            for(String s : _VARIABLES_INSERT) {
//                dbase.execSQL(s);
//            }
//            Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();
//
//        }else {
//
//            // TODO Auto-generated method stub
//
////            String[] _VARIABLES_UPDATE = {"UPDATE variables SET value = '"+url+"' WHERE name = 'url'", "UPDATE variable SET value = '"+uname+"' WHERE name = 'username'", "UPDATE variable SET value = '"+passwd+"' WHERE name = 'password'"};
//            String[] _VARIABLES_UPDATE = {"UPDATE variables SET value = '"+code+"' WHERE name = 'url'", "UPDATE variables SET value = '"+uname+"' WHERE name = 'username'", "UPDATE variables SET value = '"+passwd+"' WHERE name = 'password'"};
//            for(String s : _VARIABLES_UPDATE) {
//                dbase.execSQL(s);
//            }
//            Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();
//        }
        Intent intent;
        intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }


}