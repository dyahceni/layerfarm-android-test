package com.layerfarm.layerfarmmanager.Navigation.Model;

public class ModelAccount {
    public String farm_name;
    public String farm_code;
    public String farm_role;
    public String full_name;
    public String password;
    public String is_owner;
    public String id;

    public String getFarm_name() {
        return farm_name;
    }

    public void setFarm_name(String farm_name) {
        this.farm_name = farm_name;
    }

    public String getFarm_code() {
        return farm_code;
    }

    public void setFarm_code(String farm_code) {
        this.farm_code = farm_code;
    }

    public String getFarm_role() {
        return farm_role;
    }

    public void setFarm_role(String farm_role) {
        this.farm_role = farm_role;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(String is_owner) {
        this.is_owner = is_owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
