package com.layerfarm.layerfarmmanager.Navigation;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarmmanager.MainActivity;
import com.layerfarm.layerfarmmanager.Navigation.Adapter.AdapterListAccount;
import com.layerfarm.layerfarmmanager.Navigation.Adapter.AdapterListFarm;
import com.layerfarm.layerfarmmanager.Navigation.Model.ModelAccount;
import com.layerfarm.layerfarmmanager.R;
import com.layerfarm.setting.ServerSettingActivity;
import com.layerfarm.unregistered.ActivationUserMainActivity;

import java.util.ArrayList;

public class AppListFarm extends Activity {
    ViewGroup farm, partner;
    TextView add_farm, add_partner;
    ProgressBar progressBar;
    //    RecyclerView listAccount;
    ListView listFarm;
    //    private RecyclerView.Adapter adapter;
    AdapterListFarm adapter;
    private LinearLayoutManager layoutManager;
    ArrayList<ModelAccount> modelAccounts = new ArrayList<>();
    DatabaseHelper db;
    SQLiteDatabase dbase;
    private Handler mHandler = new Handler();
    String url, uname, passwd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_list_farm);

        db = new DatabaseHelper(this);
        dbase = db.getReadableDatabase();

        Toolbar toolbarTop = (Toolbar) findViewById(com.layerfarm.recording.R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(com.layerfarm.recording.R.id.title);
        mTitle.setText("Farm");

//        listAccount = (RecyclerView) findViewById(R.id.rv_listAccount);
        listFarm = (ListView) findViewById(R.id.rv_listFarm);
        add_partner = (TextView) findViewById(R.id.add_partner_farm);
        partner = (ViewGroup) findViewById(R.id.partner_farm_list);
        progressBar = (ProgressBar) findViewById(R.id.proggressBar);

        //Jika menggunakan recyclerview
//        listAccount.setHasFixedSize(true);
//        layoutManager = new LinearLayoutManager(this);
//        listAccount.setLayoutManager(layoutManager);
        listFarm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String full_name = modelAccounts.get(position).getFull_name();
                final String farm_code = modelAccounts.get(position).getFarm_code();
                final String password = modelAccounts.get(position).getPassword();

                showDialog(modelAccounts.get(position));

            }
        });

        add_partner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(getApplicationContext(), ActivationUserMainActivity.class);
                startActivity(intent);
            }
        });

        fetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        modelAccounts.clear();
        fetchData();
    }
    public void fetchData(){
        modelAccounts.clear();
        SQLiteDatabase dbase = db.getWritableDatabase();
        String value ="";

        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        String code = variables.getString("code", null);
        String uname = variables.getString("username", null);
        String passwd = variables.getString("password", null);

        if(code != null && uname != null && passwd != null){
            value = uname;
        }

//        String select = "SELECT value from variables WHERE name = 'username'";
//        Cursor cursor_active = dbase.rawQuery(select,null);
//        if (cursor_active.moveToFirst()) {
//            value = cursor_active.getString(cursor_active.getColumnIndex("value"));
//        }
//        cursor_active.close();

        String selectQuery = "SELECT * FROM user_farm where is_owner = 1";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            ModelAccount model = new ModelAccount();
            model.full_name = cursor.getString(cursor.getColumnIndex("full_name"));
            model.farm_name = cursor.getString(cursor.getColumnIndex("farm_name"));
            model.farm_code = cursor.getString(cursor.getColumnIndex("farm_code"));
            model.farm_role = cursor.getString(cursor.getColumnIndex("role"));
            model.password = cursor.getString(cursor.getColumnIndex("password"));
            model.is_owner = cursor.getString(cursor.getColumnIndex("is_owner"));
            model.id = cursor.getString(cursor.getColumnIndex("id"));
            modelAccounts.add(model);
        }
        if(!modelAccounts.isEmpty()){
            partner.setVisibility(View.GONE);
        }
        Log.d("zzz","model account = "+modelAccounts.size());
        cursor.close();
        adapter = new AdapterListFarm(AppListFarm.this, value, modelAccounts);
        adapter.notifyDataSetChanged();
        listFarm.setAdapter(adapter);

    }
    public void close(View view){
//        Intent intent;
//        intent = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(intent);
        finish();
    }
    private void showDialog(final ModelAccount data){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Warning");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Are you sure want to delete this farm ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                        dialog.cancel();
                        delete(data);
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AppListFarm.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
    public void delete(ModelAccount data){
        String query_chick_in = "DELETE FROM user_farm WHERE id = '"+data.id+"'";
        dbase.execSQL(query_chick_in);
        Toast.makeText(AppListFarm.this, "Delete farm success", Toast.LENGTH_SHORT).show();
        fetchData();
    }

}