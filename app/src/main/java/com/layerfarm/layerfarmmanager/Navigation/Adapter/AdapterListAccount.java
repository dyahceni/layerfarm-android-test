package com.layerfarm.layerfarmmanager.Navigation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.components.Legend;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarmmanager.Navigation.Model.ModelAccount;
import com.layerfarm.layerfarmmanager.R;

import java.util.ArrayList;

public class AdapterListAccount extends BaseAdapter {
    private Activity activity;
    ArrayList<ModelAccount> model_account;
    String active_user;
    String active_url;
    private LayoutInflater inflater;
    DatabaseHelper db;
    SQLiteDatabase dbase;

    public AdapterListAccount(Activity activity, String active_user, String active_url, ArrayList<ModelAccount> model_account){
        this.model_account = model_account;
        this.active_user = active_user;
        this.active_url = active_url;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return model_account.size();
    }

    @Override
    public Object getItem(int position) {
        return model_account.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.app_rv_account, null);
        ViewGroup delete = (ViewGroup) convertView.findViewById(R.id.delete);
        TextView user_name = (TextView) convertView.findViewById(R.id.user_name);
        TextView farm_name = (TextView) convertView.findViewById(R.id.farm_name);
        TextView farm_code = (TextView) convertView.findViewById(R.id.farm_code);
        TextView farm_role = (TextView) convertView.findViewById(R.id.farm_role);
        CardView cv_account = (CardView) convertView.findViewById(R.id.cv_account);
        Log.d("zzzz","active user = "+active_user);
        Log.d("zzzz","account = "+model_account.get(position).full_name);
        Log.d("zzzz","active url = "+active_url);
        Log.d("zzzz","url = "+model_account.get(position).farm_code);
        if (model_account.get(position).is_owner.equals("1"))
            cv_account.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightgray));
        int active =0;
        if (active_user.equals(model_account.get(position).full_name) && active_url.equals(model_account.get(position).farm_code)){
            Log.d("zzzz","same");
            cv_account.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            delete.setVisibility(View.INVISIBLE);
            active = 1;
        }
        else
            delete.setVisibility(View.VISIBLE);
        db = new DatabaseHelper(activity);
        dbase = db.getReadableDatabase();
        user_name.setText(model_account.get(position).full_name);
        farm_name.setText(model_account.get(position).farm_name);
        farm_code.setText(model_account.get(position).farm_code);
        farm_role.setText(model_account.get(position).farm_role);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(model_account.get(position), position);
//                String query_chick_in = "DELETE FROM user_farm WHERE id = '"+model_account.get(position).id+"'";
//                dbase.execSQL(query_chick_in);
//                Toast.makeText(activity, "Delete farm success", Toast.LENGTH_SHORT).show();

            }
        });

        return convertView;

    }
    private void showDialog(final ModelAccount data, final int position){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set title dialog
        alertDialogBuilder.setTitle("Warning");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Are you sure want to delete this connection ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                        dialog.cancel();
                        delete(data, position);
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
//                        AppListFarm.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
    public void delete(ModelAccount data, int position){
        model_account.remove(position);
        String query_chick_in = "DELETE FROM user_farm WHERE id = '"+data.id+"'";
        dbase.execSQL(query_chick_in);
        Toast.makeText(activity, "Delete farm success", Toast.LENGTH_SHORT).show();
        notifyDataSetChanged();
    }
}
