package com.layerfarm.layerfarmmanager.Navigation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.layerfarmmanager.Navigation.Model.ModelAccount;
import com.layerfarm.layerfarmmanager.R;

import java.util.ArrayList;

public class AdapterListFarm extends BaseAdapter {
    private Activity activity;
    ArrayList<ModelAccount> model_account;
    String active_user;
    private LayoutInflater inflater;

    public AdapterListFarm(Activity activity, String active_user, ArrayList<ModelAccount> model_account){
        this.model_account = model_account;
        this.active_user = active_user;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return model_account.size();
    }

    @Override
    public Object getItem(int position) {
        return model_account.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.app_rv_farm, null);

        TextView farm_name = (TextView) convertView.findViewById(R.id.farm_name);
        TextView farm_code = (TextView) convertView.findViewById(R.id.farm_code);
        TextView farm_role = (TextView) convertView.findViewById(R.id.farm_role);
        CardView cv_account = (CardView) convertView.findViewById(R.id.cv_account);
        Log.d("zzzz","active user farm = "+active_user);
        Log.d("zzzz","account farm = "+model_account.get(position).full_name);
//        if (active_user.equals(model_account.get(position).full_name)){
//            Log.d("zzzz","same");
//            cv_account.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
//        }
//        else
//            cv_account.setBackgroundColor(Color.WHITE);

        farm_name.setText(model_account.get(position).farm_name);
        farm_code.setText(model_account.get(position).farm_code);
        farm_role.setText(model_account.get(position).farm_role);

        return convertView;
    }

}
