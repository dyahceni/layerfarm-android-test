package com.layerfarm.layerfarmmanager;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ParameterRegistrationCreateLayerfarmUser;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class renewalGoogleBillingActivity extends AppCompatActivity implements PurchasesUpdatedListener, AcknowledgePurchaseResponseListener {

    //billing api
    private BillingClient billingClient;
    List<SkuDetails> mySkuDetailsList;
    CheckBox chxAggree;
    Button activateButton;
    EditText eDUrlName,eDFarmAddress;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    private String short_name;
    private Handler mHandler = new Handler();
    public String url,username,password,_username_retrofit,_password_retrofit;
    ProgressDialog loading, loadingRenewalFarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renewal_google_billing);

        eDUrlName= (EditText) findViewById(R.id.url_name);
        eDFarmAddress= (EditText) findViewById(R.id.farm_address);
        chxAggree = (CheckBox) findViewById(R.id.cb_click_activation) ;
        activateButton = (Button)findViewById(R.id.activateButton) ;
        activateButton.setEnabled(false);
        activateButton.setBackgroundColor(Color.parseColor("#ceced2"));

        init_billing();


        chxAggree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(chxAggree.isChecked())
                {
                    activateButton.setEnabled(true);
                    activateButton.setBackgroundColor(Color.parseColor("#008577"));
                }
                else
                {
                    activateButton.setEnabled(false);
                    activateButton.setBackgroundColor(Color.parseColor("#ceced2"));
                }

            }

        });

        activateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //JIka pakai Billing
              /*   querySkuList();
                 startPurchase("lfm1.0alfa_1");
                 queryOwned();*/

                 //bypass billing
                cekFarmStatus(short_name);
            }

        });
    }


    private void init_billing(){
        //init billing
        billingClient = BillingClient.newBuilder(this)
                .setListener(this).enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                int responseCode = billingResult.getResponseCode();
                if (responseCode == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    querySkuList();
                    queryOwned();
                    Toast.makeText(renewalGoogleBillingActivity.this, "Setup Billing OK", Toast.LENGTH_SHORT).show();
                    connect_server();
                } else {
                    showResponseCode(responseCode);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Toast.makeText(renewalGoogleBillingActivity.this, "Billing Service Disconnect", Toast.LENGTH_SHORT).show();
            }
        });


    }



    @Override
    protected void onDestroy() {
        billingClient.endConnection();
        super.onDestroy();
    }


    void querySkuList() {
        List skuList = new ArrayList<>();
        skuList.add("lfm1.0alfa_1");
        skuList.add("lfm1.0alfa_member");
        skuList.add("lfm1.0alfa_member_15");

/*        skuList.add("android.test.purchased");  // prepared by Google
        skuList.add("android.test.canceled");
        skuList.add("android.test.refunded");
        skuList.add("android.test.item_unavailable");*/
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        //params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        StringBuffer resultStr = new StringBuffer("");
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            mySkuDetailsList = skuDetailsList;
                            if (skuDetailsList != null) {
                                for (Object item : skuDetailsList) {
                                    SkuDetails skuDetails = (SkuDetails) item;
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();
                                    resultStr.append("Sku=" + sku + " Price=" + price + "\n");
                                }
                            } else {
                                resultStr.append("No Sku");
                            }
                            Log.d("resultStr"," "+ resultStr);
                            //textView1.setText(resultStr);
                        } else {
                            showResponseCode(responseCode);
                        }
                    }
                });
    }


    void startPurchase(String sku) {
        Log.d("sku", "sku= " + sku);
        SkuDetails skuDetails = getSkuDetails(sku);
        if (skuDetails != null) {
            BillingFlowParams params = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetails)
                    .build();
            BillingResult billingResult = billingClient.launchBillingFlow(this, params);
            showResponseCode(billingResult.getResponseCode());
        }
    }


    SkuDetails getSkuDetails(String sku) {
        SkuDetails skuDetails = null;
        if(mySkuDetailsList==null){
            //textView1.setText("Exec [Get Skus] first");
            Log.d("resultStr", "Exec [Get Skus] first");
        }else {
            for (SkuDetails sd : mySkuDetailsList) {
                if (sd.getSku().equals(sku)) skuDetails = sd;
            }
            if (skuDetails == null) {
                //textView1.setText(sku + " is not found");
                Log.d("resultStr", " "+ sku + " is not found");
            }
        }
        return skuDetails;
    }


    @Override
    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {

        StringBuffer resultStr = new StringBuffer("");
        int billingResultCode = billingResult.getResponseCode();
        if (billingResultCode == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                String state = handlePurchase(purchase);
                String sku = purchase.getSku();
                resultStr.append(sku).append("\n");
                resultStr.append(" State=").append(state).append("\n");
/*

                if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
                    // Invalid purchase
                    // show error to user
                    Log.i("tes", "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
                    return;
                } else {
                    // purchase is valid
                    // Perform actions
                    Log.i("tes", "but signature is bad. Skipping...");
                }
*/

            }
            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        } else {
            // Handle error codes.
            showResponseCode(billingResultCode);
        }
    }


    String handlePurchase(Purchase purchase) {
        String stateStr = "error";
        int purchaseState = purchase.getPurchaseState();
        if (purchaseState == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.
            stateStr = "purchased";

            cekFarmStatus(short_name);


            // Acknowledge the purchase if it hasn't already been acknowledged.
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, this);
            }
        }else if(purchaseState == Purchase.PurchaseState.PENDING){
            stateStr = "pending";
        }else if(purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE){
            stateStr = "unspecified state";
        }
        return stateStr;
    }



    @Override
    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
        int responseCode = billingResult.getResponseCode();
        if(responseCode != BillingClient.BillingResponseCode.OK) {
            showResponseCode(responseCode);
        }
    }


    void queryOwned(){
        StringBuffer resultStr = new StringBuffer("");
        Purchase.PurchasesResult purchasesResult
                = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
        int responseCode = purchasesResult.getResponseCode ();
        if(responseCode== BillingClient.BillingResponseCode.OK){
            resultStr.append("Query Success\n");
            List<Purchase> purchases = purchasesResult.getPurchasesList();
            if(purchases.isEmpty()){
                resultStr.append("Owned Nothing");
            } else {
                for (Purchase purchase : purchases) {
                    resultStr.append(purchase.getSku()).append("\n");
                }
            }
            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        }else{
            Log.d("xxx", " "+ resultStr);
            showResponseCode(responseCode);
        }
    }


    void queryPurchaseHistory() {
        billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS,
                new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(BillingResult billingResult,
                                                          List<PurchaseHistoryRecord> purchasesList) {
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            if (purchasesList == null || purchasesList.size() == 0) {
                                //textView1.setText("No History");
                                Log.d("No History", " "+ "No History");
                            } else {
                                for (PurchaseHistoryRecord purchase : purchasesList) {
                                    // Process the result.
                                    //textView1.setText("Purchase History=" + purchase.toString() + "\n");
                                    Log.d("No History", " "+ purchase.toString());
                                }
                            }
                        } else {
                            showResponseCode(responseCode);
                        }
                    }
                });
    }

    void showResponseCode(int responseCode){
        switch(responseCode){
            case BillingClient.BillingResponseCode.OK:
                // textView1.setText("OK");
                Log.d("resultStr", " "+ "OK");
                break;
            case BillingClient.BillingResponseCode.USER_CANCELED:
                //textView1.setText("USER_CANCELED");
                Log.d("resultStr", " "+ "USER CANCELLED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                //textView1.setText("SERVICE_UNAVAILABLE");
                Log.d("resultStr", " "+ "SERVICE_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                Log.d("resultStr", " "+ "BILLING_UNAVAILABLE");
                //textView1.setText("BILLING_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                Log.d("resultStr", " "+ "ITEM_UNAVAILABLE");
                //textView1.setText("ITEM_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                Log.d("resultStr", " "+ "DEVELOPER_ERROR");
                //textView1.setText("DEVELOPER_ERROR");
                break;
            case BillingClient.BillingResponseCode.ERROR:
                Log.d("resultStr", " "+ "ERROR");
                //textView1.setText("ERROR");
                break;
            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                Log.d("resultStr", " "+ "ITEM_ALREADY_OWNED");
                //textView1.setText("ITEM_ALREADY_OWNED");
                break;
            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                Log.d("resultStr", " "+ "ITEM_NOT_OWNED");
                //textView1.setText("ITEM_NOT_OWNED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                Log.d("resultStr", " "+ "SERVICE_DISCONNECTED");
                //textView1.setText("SERVICE_DISCONNECTED");
                break;
            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                Log.d("resultStr", " "+ "FEATURE_NOT_SUPPORTED");
                //textView1.setText("FEATURE_NOT_SUPPORTED");
                break;
        }
    }

    private void getServerData(){
        //countToTen();
        SQLiteDatabase dbSQL = db.getReadableDatabase();
        String selectQuery = "SELECT * FROM farm_expired_date";
        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                short_name = cursor.getString(cursor.getColumnIndex("short_name"));
            } while (cursor.moveToNext());
        }
        cursor.close();
        //Log.d("ssss", "values = " + values);
        if (short_name.isEmpty()){
            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
            //showDialog();
            //getData();

        }else {

            String url = short_name;
//            String frmaddress = short_name+".edufren.com";
            String frmaddress = short_name + "." + Env.DOMAIN;
            Log.d("ssss", "url = " + url);
            Log.d("ssss", "frmaddress = " + frmaddress);
            eDUrlName.setText(url);
            eDFarmAddress.setText(frmaddress);

        }

    }

    private void set_date_expired(String url_key){

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
        String strDate = mdformat.format(calendar.getTime());

        calendar.add(Calendar.MONTH, 1);  // number of days to add
        String end_date = mdformat.format(calendar.getTime());


        String url_address = url_key;
        String url_expired_date = end_date;

        SQLite = new DataHelper(getApplication());
        SQLiteDatabase dbase = db.getWritableDatabase();
        Cursor result = dbase.rawQuery("SELECT * FROM farm_expired_date", null);
        if (result.getCount() == 0) {
            // TODO Auto-generated method stub

            ContentValues x = new ContentValues();
            x.put("short_name", url_address);
            x.put("expired", url_expired_date);
            dbase.insert("farm_expired_date",null,x);
            //Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();
        }else {
            // TODO Auto-generated method stub
            String updateQuery = "UPDATE farm_expired_date SET expired ='" + url_expired_date + "'"
                    + " WHERE short_name =" + "'" + url_address + "'";
            Log.e("update sqlite ", updateQuery);
            dbase.execSQL(updateQuery);
            dbase.close();
            Toast.makeText(getApplicationContext(), "Renewal Successfully", Toast.LENGTH_LONG).show();
            loadingRenewalFarm.dismiss();
            restart();
/*
            ContentValues x = new ContentValues();
            x.put("short_name", url_address);
            x.put("expired", url_expired_date);
            dbase.update("farm_expired_date", x, "short_name" + " = " + url_address, null);*/
        }
    }

    private void renewalFarmServer(final String _urlFarm){
        loadingRenewalFarm = ProgressDialog.show(renewalGoogleBillingActivity.this, "Renewal Farm", "Please wait...", true, false);

        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_farm_activate";

        Log.d("_urlFarm",""+ _urlFarm);
        ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments("", "","",_urlFarm,"","","", "","");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

        Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //String[] get_expense_general_respons = response.body();
                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                if (response.isSuccessful()) {
                    String exist_farm = response.body();
                    Log.d("xxxxxx", "exist_farm = " + exist_farm);

                    if (exist_farm.equals("1")){
                        set_date_expired(_urlFarm);
                        //Toast.makeText(renewalGoogleBillingActivity.this, "Renewal farm succesfully" , Toast.LENGTH_SHORT).show();

                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(renewalGoogleBillingActivity.this, "Renewal farm fail" , Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void connect_server(){
        loading = ProgressDialog.show(renewalGoogleBillingActivity.this, null, "Please wait...", true, false);
//        url = "edufren.com";
        url = Env.DOMAIN;
        username = "root";
        password = "sapua#@!";
        LayerFarm.getInstance().Connect(this, "http://"+url+"/", username, password);
        RetrofitData.getInstance().setStatus("");
        Start();

    }

    public void Start(){
        mRun.run();
    }
    public void Stop(){
        loading.dismiss();
        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
            if (email == username && passwd == password){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(renewalGoogleBillingActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        getServerData();
                        Stop();

                    } else{
                        Stop();
                        finish();
                    }
                }
            }

        }
    };

    public void restart(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        ActivityCompat.finishAfterTransition(renewalGoogleBillingActivity.this);
    }

    public void BackRenewal_1(View view) {finish();
    }

    public void BackRenewal_2(View view) {finish();
    }


    private void cekFarmStatus(final String _urlFarmstatus) {
        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_check_farm_status";

        ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments("", "","",_urlFarmstatus,"","","", "","");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

        Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //String[] get_expense_general_respons = response.body();
                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                if (response.isSuccessful()) {
                    String farm_status = response.body();
                    Log.d("Layerfarm_status", "Layerfarm_status = " + farm_status);
                    String status = farm_status.split("\\/")[0];
                    String date = farm_status.split("\\/")[1];
                    Log.d("xxx", "Layerfarm_status = " + status);
                    Log.d("xxx", "Layerfarm_status = " + date);
                    if (status.equals("1")){
                        Toast.makeText(getApplicationContext(), "Farm Active", Toast.LENGTH_LONG).show();

                    }else{
                        //Toast.makeText(getApplicationContext(), "Farm InActive", Toast.LENGTH_LONG).show();
                        renewalFarmServer(_urlFarmstatus);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }
}
