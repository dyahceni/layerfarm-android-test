package com.layerfarm.layerfarmmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.layerfarm.layerfarmmanager.Navigation.AppAddPartner;

public class Reconnect extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reconnect);
    }
    public void reconnect(View v){
        Intent intent;
        intent = new Intent(getApplicationContext(), AppAddPartner.class);
        startActivity(intent);
        finish();
    }
}
