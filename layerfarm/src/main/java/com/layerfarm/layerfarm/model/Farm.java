package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Farm implements Serializable {

    @SerializedName("nid")
    private String nid;

    @SerializedName("name")
    private String name;

    @SerializedName("delete_status")
    private String delete_status;

    @SerializedName("address")
    private String address;

//    public Farm(String nid, String name){
//        this.nid = nid;
//        this.name = name;
//    }

    public String getNid() {

        return nid;
    }
    public void setNid(String nid) {

        this.nid = nid;
    }

    public String getName() {

        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDelete_status() {
        return delete_status;
    }

    public void setDelete_status(String delete_status) {
        this.delete_status = delete_status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
