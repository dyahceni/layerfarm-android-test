package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SyncFlock {
    @SerializedName("list_flock")
    ArrayList<mFlock> list_flock = new ArrayList<>();
    @SerializedName("list_location")
    LinkedHashMap<String, String> list_location;

    public ArrayList<mFlock> getList_flock() {
        return list_flock;
    }

    public void setList_flock(ArrayList<mFlock> list_flock) {
        this.list_flock = list_flock;
    }

    public LinkedHashMap<String, String> getList_location() {
        return list_location;
    }

    public void setList_location(LinkedHashMap<String, String> list_location) {
        this.list_location = list_location;
    }
}
