package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class DailyRecordingOvk {
    @SerializedName("ovk_id")
    private String ovk_id;
    @SerializedName("amount")
    private String amount;

    public String getOvk_id() {
        return ovk_id;
    }

    public void setOvk_id(String ovk_id) {
        this.ovk_id = ovk_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
