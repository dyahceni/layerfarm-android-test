package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;


public class ParameterDeleteGeneralExpense {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterDeleteGeneralExpense(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;


        public Arguments(String nid){
            this.a = nid;
        }
    }
}
