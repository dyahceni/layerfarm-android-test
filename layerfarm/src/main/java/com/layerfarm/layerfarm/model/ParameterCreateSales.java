package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class ParameterCreateSales {
    @SerializedName("module")
    private String module;
    @SerializedName("function_name")
    private String function_name;
    @SerializedName("args")
    private Sales args;

    public ParameterCreateSales(String module, String function_name, Sales args){
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public Sales getArgs() {
        return args;
    }

    public void setArgs(Sales args) {
        this.args = args;
    }
}
