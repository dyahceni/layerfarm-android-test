package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DashboardLayerFinancial {
    private static DashboardLayerFinancial staticInstance;

    public static DashboardLayerFinancial getInstance(){
        if (staticInstance==null){
            staticInstance = new DashboardLayerFinancial();
        }
        return staticInstance;
    }
    @SerializedName("egg_sell")
    @Expose
    private EggSell eggSell = null;

    @SerializedName("income")
    @Expose
    private ArrayList<Income> income = null;

    @SerializedName("expense")
    @Expose
    private ArrayList<Expense> expense = null;

    public EggSell getEggSell() {
        return eggSell;
    }

    public void setEggSell(EggSell eggSell) {
        this.eggSell = eggSell;
    }

    public ArrayList<Income> getIncome() {
        return income;
    }

    public void setIncome(ArrayList<Income> income) {
        this.income = income;
    }

    public ArrayList<Expense> getExpense() {
        return expense;
    }

    public void setExpense(ArrayList<Expense> expense) {
        this.expense = expense;
    }
    public class EggSell {

        @SerializedName("min")
        @Expose
        private ArrayList<String> min;

        @SerializedName("max")
        @Expose
        private ArrayList<String> max;

        public ArrayList<String> getMin() {
            return min;
        }

        public void setMin(ArrayList<String> min) {
            this.min = min;
        }

        public ArrayList<String> getMax() {
            return max;
        }

        public void setMax(ArrayList<String> max) {
            this.max = max;
        }
    }
    public class Income {

        @SerializedName("income")
        @Expose
        private String income;

        public String getIncome() { return income; }

        public void setIncome(String income) { this.income = income; }

    }

    public class Expense {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }
}
