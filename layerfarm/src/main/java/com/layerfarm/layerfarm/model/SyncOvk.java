package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class SyncOvk {
    @SerializedName("list_ovk")
    ArrayList<OvkSync> list_ovk = new ArrayList<>();
    @SerializedName("list_vendor")
    LinkedHashMap<String, String> list_vendor;

    public ArrayList<OvkSync> getList_ovk() {
        return list_ovk;
    }

    public void setList_ovk(ArrayList<OvkSync> list_ovk) {
        this.list_ovk = list_ovk;
    }

    public LinkedHashMap<String, String> getVendor_list() {
        return list_vendor;
    }

    public void setVendor_list(LinkedHashMap<String, String> vendor_list) {
        this.list_vendor = list_vendor;
    }

}
