package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class ParamaterActivateMember {
    @SerializedName("module")
    private String module;
    @SerializedName("function_name")
    private String function_name;
    @SerializedName("args")
    private ActivateMember args;

    public ParamaterActivateMember(String module, String function_name, ActivateMember args){
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public ActivateMember getArgs() {
        return args;
    }

    public void setArgs(ActivateMember args) {
        this.args = args;
    }

    public static class ActivateMember{
        @SerializedName("name")
        HashMap<String, String> a;
        @SerializedName("date")
        String b;

        public ActivateMember(HashMap<String, String> name, String date){
            this.a = name;
            this.b = date;
        }
    }

}
