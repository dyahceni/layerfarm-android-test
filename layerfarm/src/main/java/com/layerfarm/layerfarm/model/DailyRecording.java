package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class DailyRecording {
    @SerializedName("recording_date")
    private String a;
    @SerializedName("house_id")
    private String b;
    @SerializedName("production")
    private HashMap<String, String> c = new HashMap<>();
    //    @SerializedName("body_weight")
//    private HashMap<String, String> d = new HashMap<>();
    @SerializedName("ovk")
    private ArrayList<DailyRecordingOvk> e = new ArrayList<>();
    @SerializedName("feed_consumption")
    private ArrayList<DailyRecordingFeed> f = new ArrayList<>();
    @SerializedName("mortality")
    private ArrayList<LinkedHashMap<String, String>> g = new ArrayList<>();
    @SerializedName("light_intensity")
    private String h = "";
    @SerializedName("light_duration")
    private ArrayList<String> i = new ArrayList<>();
    @SerializedName("temp")
    private ArrayList<String> j = new ArrayList<>();
    @SerializedName("humid")
    private ArrayList<String> k = new ArrayList<>();
    @SerializedName("pindah_kandang")
    private ArrayList<DailyRecordingTransfer> l = new ArrayList<>();
    @SerializedName("afkir")
    private ArrayList<DailyRecordingSpentHen> m = new ArrayList<>();
    @SerializedName("note")
    private String n = "";
    @SerializedName("foto")
    private String o = "";
    @SerializedName("force_date")
    private String p = "";
    @SerializedName("created")
    private String q = "";

    public String getRecordingDate() {
        return a;
    }
    public void setRecordingDate(String a) {
        this.a = a;
    }

    public String getHouseId(){
        return b;
    }
    public void setHouseId(String b){
        this.b = b;
    }

    public HashMap<String, String> getProduction(){
        return c;
    }
    public void setProduction(HashMap<String, String> c){
        this.c = c;
    }

//    public HashMap<String, String> getBodyWeight() {
//        return d;
//    }
//    public void setBodyWeight(HashMap<String, String> d) {
//        this.d = d;
//    }

    public ArrayList<DailyRecordingOvk> getOvk() {
        return e;
    }
    public void setOvk(ArrayList<DailyRecordingOvk> e) {
        this.e = e;
    }

    public ArrayList<DailyRecordingFeed> getFeed(){ return f; }
    public void setFeed(ArrayList<DailyRecordingFeed> f){ this.f = f; }

    public ArrayList<LinkedHashMap<String, String>> getMortality(){ return g; }
    public void setMortality(ArrayList<LinkedHashMap<String, String>> g){ this.g = g; }

    public String getLightIntensity(){ return h;}
    public void setLightIntensity(String h){ this.h = h; }

    public ArrayList<String> getLightDuration(){ return  i;}
    public void  setLightDuration(ArrayList<String> i) { this.i = i; }

    public ArrayList<String> getTemp(){ return  j;}
    public void  setTemp(ArrayList<String> j){ this.j = j; }

    public ArrayList<String> getHumid(){ return  k; }
    public void setHumid(ArrayList<String> k ){ this.k = k; }

    public ArrayList<DailyRecordingTransfer> getPindahKandang(){ return  l;}
    public void setPindahKandang(ArrayList<DailyRecordingTransfer> l){ this.l = l; }

    public ArrayList<DailyRecordingSpentHen> getAfkir(){ return  m; }
    public void setAfkir(ArrayList<DailyRecordingSpentHen> m){ this.m = m; }

    public String getNote(){ return  n; }
    public void setNote(String n){ this.n = n; }

    public String getFoto(){ return  o; }
    public void setFoto(String o){ this.o = o; }

    public String getForceDate(){ return  p;}
    public void setForceDate(String p){ this.p = p; }

    public String getCreated(){ return q; }
    public void setCreated(String q){ this.q = q; }

}
