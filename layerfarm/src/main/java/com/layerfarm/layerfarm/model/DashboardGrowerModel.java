package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class DashboardGrowerModel {
    private static DashboardGrowerModel staticInstance;

    public static DashboardGrowerModel getInstance(){
        if (staticInstance==null){
            staticInstance = new DashboardGrowerModel();
        }
        return staticInstance;
    }

    @SerializedName("last_recording")
    String last_recording;
    @SerializedName("available_mortality")
    ArrayList<SyncMortality> available_mortality;
    @SerializedName("overview")
    GrowerOverview overview;
    @SerializedName("performance")
    GrowerPerformance performance;
    @SerializedName("financial")
    GrowerFinancial financial;
    @SerializedName("detail")
    GrowerPerformanceDetail detail;

    @SerializedName("location_flock")
    LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock = new LinkedHashMap<>();
    @SerializedName("flock_data")
    GrowerPerformanceDetailFlockData flock_data;

    public String getLast_recording() {
        return last_recording;
    }

    public void setLast_recording(String last_recording) {
        this.last_recording = last_recording;
    }

    public ArrayList<SyncMortality> getAvailable_mortality() {
        return available_mortality;
    }

    public void setAvailable_mortality(ArrayList<SyncMortality> available_mortality) {
        this.available_mortality = available_mortality;
    }

    public GrowerOverview getOverview() {
        return overview;
    }

    public void setOverview(GrowerOverview overview) {
        this.overview = overview;
    }

    public GrowerPerformance getPerformance() {
        return performance;
    }

    public void setPerformance(GrowerPerformance performance) {
        this.performance = performance;
    }

    public GrowerFinancial getFinancial() {
        return financial;
    }

    public void setFinancial(GrowerFinancial financial) {
        this.financial = financial;
    }

    public GrowerPerformanceDetail getDetail() {
        return detail;
    }

    public void setDetail(GrowerPerformanceDetail detail) {
        this.detail = detail;
    }

    public LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> getLocation_flock() {
        return location_flock;
    }

    public void setLocation_flock(LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock) {
        this.location_flock = location_flock;
    }

    public GrowerPerformanceDetailFlockData getFlock_data() {
        return flock_data;
    }

    public void setFlock_data(GrowerPerformanceDetailFlockData flock_data) {
        this.flock_data = flock_data;
    }

    public class GrowerOverview{
        @SerializedName("m_today")
        ArrayList<String> m_today = new ArrayList<>();
        @SerializedName("m_1w")
        ArrayList<String> m_1w= new ArrayList<>();
        @SerializedName("m_1m")
        ArrayList<String> m_1m = new ArrayList<>();
        @SerializedName("m_3m")
        ArrayList<String> m_3m = new ArrayList<>();
        @SerializedName("m_6m")
        ArrayList<String> m_6m = new ArrayList<>();
        @SerializedName("m_1y")
        ArrayList<String> m_1y = new ArrayList<>();
        @SerializedName("m_2y")
        ArrayList<String> m_2y = new ArrayList<>();
        @SerializedName("m_ytd")
        ArrayList<String> m_ytd = new ArrayList<>();

        public ArrayList<String> getM_today() {
            return m_today;
        }

        public void setM_today(ArrayList<String> m_today) {
            this.m_today = m_today;
        }

        public ArrayList<String> getM_1w() {
            return m_1w;
        }

        public void setM_1w(ArrayList<String> m_1w) {
            this.m_1w = m_1w;
        }

        public ArrayList<String> getM_1m() {
            return m_1m;
        }

        public void setM_1m(ArrayList<String> m_1m) {
            this.m_1m = m_1m;
        }

        public ArrayList<String> getM_3m() {
            return m_3m;
        }

        public void setM_3m(ArrayList<String> m_3m) {
            this.m_3m = m_3m;
        }

        public ArrayList<String> getM_6m() {
            return m_6m;
        }

        public void setM_6m(ArrayList<String> m_6m) {
            this.m_6m = m_6m;
        }

        public ArrayList<String> getM_1y() {
            return m_1y;
        }

        public void setM_1y(ArrayList<String> m_1y) {
            this.m_1y = m_1y;
        }

        public ArrayList<String> getM_2y() {
            return m_2y;
        }

        public void setM_2y(ArrayList<String> m_2y) {
            this.m_2y = m_2y;
        }

        public ArrayList<String> getM_ytd() {
            return m_ytd;
        }

        public void setM_ytd(ArrayList<String> m_ytd) {
            this.m_ytd = m_ytd;
        }

    }
    public class GrowerPerformance{
        @SerializedName("population")
        ArrayList<Integer> population;
        @SerializedName("mortality")
        ArrayList<Float> mortality;
        @SerializedName("fcr")
        ArrayList<Float> fcr;
        @SerializedName("detail")
        GrowerPerformanceDetail detail;

        public ArrayList<Integer> getPopulation() {
            return population;
        }

        public void setPopulation(ArrayList<Integer> population) {
            this.population = population;
        }

        public ArrayList<Float> getMortality() {
            return mortality;
        }

        public void setMortality(ArrayList<Float> mortality) {
            this.mortality = mortality;
        }

        public ArrayList<Float> getFcr() {
            return fcr;
        }

        public void setFcr(ArrayList<Float> fcr) {
            this.fcr = fcr;
        }

        public GrowerPerformanceDetail getDetail() {
            return detail;
        }

        public void setDetail(GrowerPerformanceDetail detail) {
            this.detail = detail;
        }
    }
    public class GrowerPerformanceDetail{
        @SerializedName("location_flock")
        LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock = new LinkedHashMap<>();
        @SerializedName("flock_data")
        GrowerPerformanceDetailFlockData flock_data;

        public LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> getLocation_flock() {
            return location_flock;
        }

        public void setLocation_flock(LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock) {
            this.location_flock = location_flock;
        }

        public GrowerPerformanceDetailFlockData getFlock_data() {
            return flock_data;
        }

        public void setFlock_data(GrowerPerformanceDetailFlockData flock_data) {
            this.flock_data = flock_data;
        }
    }
    public class GrowerPerformanceDetailLocationFlock{
        @SerializedName("flock_nid")
        String flock_nid;
        @SerializedName("farm_nid")
        String farm_nid;
        @SerializedName("flock_name")
        String flock_name;
        @SerializedName("farm_name")
        String farm_name;
        @SerializedName("flock_period")
        String flock_period;
        @SerializedName("flock_type")
        String flock_type;
        @SerializedName("flock_capacity")
        String flock_capacity;
        @SerializedName("flock_status")
        String flock_status;
        @SerializedName("age")
        Integer age;
        @SerializedName("population")
        String population;
        @SerializedName("last_recorded")
        String last_recorded;

        @SerializedName("mortality")
        ArrayList<Report.Mortality> mortality;

        @SerializedName("utilization")
        String utilization;
        @SerializedName("mortality1")
        String mortality1;
        @SerializedName("mortality2")
        String mortality2;
        @SerializedName("mortality3")
        String mortality3;
        @SerializedName("mortality4")
        String mortality4;
        @SerializedName("mortality5")
        String mortality5;
        @SerializedName("chick_out")
        String chick_out;
        @SerializedName("total_percent")
        String total_percent;
        @SerializedName("mortality_list")
        ArrayList<SyncMortality> available_mortality;
        @SerializedName("feed_consumption")
        String feed_consumption;
        @SerializedName("production_weight")
        String production_weight;
        @SerializedName("fcr")
        String fcr;

        public String getFlock_nid() {
            return flock_nid;
        }

        public void setFlock_nid(String flock_nid) {
            this.flock_nid = flock_nid;
        }

        public String getFarm_nid() {
            return farm_nid;
        }

        public void setFarm_nid(String farm_nid) {
            this.farm_nid = farm_nid;
        }

        public String getFlock_name() {
            return flock_name;
        }

        public void setFlock_name(String flock_name) {
            this.flock_name = flock_name;
        }

        public String getFarm_name() {
            return farm_name;
        }

        public void setFarm_name(String farm_name) {
            this.farm_name = farm_name;
        }

        public String getFlock_period() {
            return flock_period;
        }

        public void setFlock_period(String flock_period) {
            this.flock_period = flock_period;
        }

        public String getFlock_type() {
            return flock_type;
        }

        public void setFlock_type(String flock_type) {
            this.flock_type = flock_type;
        }

        public String getFlock_capacity() {
            return flock_capacity;
        }

        public void setFlock_capacity(String flock_capacity) {
            this.flock_capacity = flock_capacity;
        }

        public String getFlock_status() {
            return flock_status;
        }

        public void setFlock_status(String flock_status) {
            this.flock_status = flock_status;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getPopulation() {
            return population;
        }

        public void setPopulation(String population) {
            this.population = population;
        }

        public String getLast_recorded() {
            return last_recorded;
        }

        public void setLast_recorded(String last_recorded) {
            this.last_recorded = last_recorded;
        }

        public ArrayList<Report.Mortality> getMortality() {
            return mortality;
        }

        public void setMortality(ArrayList<Report.Mortality> mortality) {
            this.mortality = mortality;
        }

        public String getUtilization() {
            return utilization;
        }

        public void setUtilization(String utilization) {
            this.utilization = utilization;
        }

        public String getMortality1() {
            return mortality1;
        }

        public void setMortality1(String mortality1) {
            this.mortality1 = mortality1;
        }

        public String getMortality2() {
            return mortality2;
        }

        public void setMortality2(String mortality2) {
            this.mortality2 = mortality2;
        }

        public String getMortality3() {
            return mortality3;
        }

        public void setMortality3(String mortality3) {
            this.mortality3 = mortality3;
        }

        public String getMortality4() {
            return mortality4;
        }

        public void setMortality4(String mortality4) {
            this.mortality4 = mortality4;
        }

        public String getMortality5() {
            return mortality5;
        }

        public void setMortality5(String mortality5) {
            this.mortality5 = mortality5;
        }

        public String getChick_out() {
            return chick_out;
        }

        public void setChick_out(String chick_out) {
            this.chick_out = chick_out;
        }

        public String getTotal_percent() {
            return total_percent;
        }

        public void setTotal_percent(String total_percent) {
            this.total_percent = total_percent;
        }

        public ArrayList<SyncMortality> getAvailable_mortality() {
            return available_mortality;
        }

        public void setAvailable_mortality(ArrayList<SyncMortality> available_mortality) {
            this.available_mortality = available_mortality;
        }

        public String getFeed_consumption() {
            return feed_consumption;
        }

        public void setFeed_consumption(String feed_consumption) {
            this.feed_consumption = feed_consumption;
        }

        public String getProduction_weight() {
            return production_weight;
        }

        public void setProduction_weight(String production_weight) {
            this.production_weight = production_weight;
        }

        public String getFcr() {
            return fcr;
        }

        public void setFcr(String fcr) {
            this.fcr = fcr;
        }
    }
    public class GrowerPerformanceDetailFlockData{
        @SerializedName("recording_date")
        LinkedHashMap<Integer, ArrayList<String>> recording_date = new LinkedHashMap<>();
        @SerializedName("detail_mortality")
        LinkedHashMap<Integer, ArrayList<Float>> mortality = new LinkedHashMap<>();
        @SerializedName("detail_population")
        LinkedHashMap<Integer, ArrayList<Integer>> population = new LinkedHashMap<>();
        @SerializedName("detail_fcr")
        LinkedHashMap<Integer, ArrayList<Double>> fcr = new LinkedHashMap<>();
        @SerializedName("fcr_recording_date")
        LinkedHashMap<Integer, ArrayList<String>> fcr_recording_date = new LinkedHashMap<>();

        public LinkedHashMap<Integer, ArrayList<String>> getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(LinkedHashMap<Integer, ArrayList<String>> recording_date) {
            this.recording_date = recording_date;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getMortality() {
            return mortality;
        }

        public void setMortality(LinkedHashMap<Integer, ArrayList<Float>> mortality) {
            this.mortality = mortality;
        }

        public LinkedHashMap<Integer, ArrayList<Integer>> getPopulation() {
            return population;
        }

        public void setPopulation(LinkedHashMap<Integer, ArrayList<Integer>> population) {
            this.population = population;
        }

        public LinkedHashMap<Integer, ArrayList<Double>> getFcr() {
            return fcr;
        }

        public void setFcr(LinkedHashMap<Integer, ArrayList<Double>> fcr) {
            this.fcr = fcr;
        }

        public LinkedHashMap<Integer, ArrayList<String>> getFcr_recording_date() {
            return fcr_recording_date;
        }

        public void setFcr_recording_date(LinkedHashMap<Integer, ArrayList<String>> fcr_recording_date) {
            this.fcr_recording_date = fcr_recording_date;
        }
    }
    public class GrowerFinancial{
        @SerializedName("m_today")
        GrowerFinancialModel m_today;
        @SerializedName("m_1w")
        GrowerFinancialModel m_1w;
        @SerializedName("m_1m")
        GrowerFinancialModel m_1m;
        @SerializedName("m_3m")
        GrowerFinancialModel m_3m;
        @SerializedName("m_6m")
        GrowerFinancialModel m_6m;
        @SerializedName("m_1y")
        GrowerFinancialModel m_1y;
        @SerializedName("m_2y")
        GrowerFinancialModel m_2y;
        @SerializedName("m_ytd")
        GrowerFinancialModel m_ytd;

        public GrowerFinancialModel getM_today() {
            return m_today;
        }

        public void setM_today(GrowerFinancialModel m_today) {
            this.m_today = m_today;
        }

        public GrowerFinancialModel getM_1w() {
            return m_1w;
        }

        public void setM_1w(GrowerFinancialModel m_1w) {
            this.m_1w = m_1w;
        }

        public GrowerFinancialModel getM_1m() {
            return m_1m;
        }

        public void setM_1m(GrowerFinancialModel m_1m) {
            this.m_1m = m_1m;
        }

        public GrowerFinancialModel getM_3m() {
            return m_3m;
        }

        public void setM_3m(GrowerFinancialModel m_3m) {
            this.m_3m = m_3m;
        }

        public GrowerFinancialModel getM_6m() {
            return m_6m;
        }

        public void setM_6m(GrowerFinancialModel m_6m) {
            this.m_6m = m_6m;
        }

        public GrowerFinancialModel getM_1y() {
            return m_1y;
        }

        public void setM_1y(GrowerFinancialModel m_1y) {
            this.m_1y = m_1y;
        }

        public GrowerFinancialModel getM_2y() {
            return m_2y;
        }

        public void setM_2y(GrowerFinancialModel m_2y) {
            this.m_2y = m_2y;
        }

        public GrowerFinancialModel getM_ytd() {
            return m_ytd;
        }

        public void setM_ytd(GrowerFinancialModel m_ytd) {
            this.m_ytd = m_ytd;
        }
    }
    public static class GrowerFinancialModel{
        @SerializedName("expense_total")
        String expense_total;
        @SerializedName("expense_graph")
        ArrayList<String> expense_graph;
        @SerializedName("income_total")
        String income_total;
        @SerializedName("income_graph")
        ArrayList<String> income_graph;
        @SerializedName("price")
        String price;

        public String getExpense_total() {
            return expense_total;
        }

        public void setExpense_total(String expense_total) {
            this.expense_total = expense_total;
        }

        public ArrayList<String> getExpense_graph() {
            return expense_graph;
        }

        public void setExpense_graph(ArrayList<String> expense_graph) {
            this.expense_graph = expense_graph;
        }

        public String getIncome_total() {
            return income_total;
        }

        public void setIncome_total(String income_total) {
            this.income_total = income_total;
        }

        public ArrayList<String> getIncome_graph() {
            return income_graph;
        }

        public void setIncome_graph(ArrayList<String> income_graph) {
            this.income_graph = income_graph;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }


}
