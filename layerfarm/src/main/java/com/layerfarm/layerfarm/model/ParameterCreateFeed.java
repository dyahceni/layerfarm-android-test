package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;


public class ParameterCreateFeed {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterCreateFeed(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;

        @SerializedName("farm_name")
        private String b;

        @SerializedName("description")
        private String c;

        @SerializedName("status")
        private String d;




        public Arguments(String nid, String farm_name, String description, String status){
            this.a = nid;
            this.b = farm_name;
            this.c = description;
            this.d = status;
        }
    }
}
