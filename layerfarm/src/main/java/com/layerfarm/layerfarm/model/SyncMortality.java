package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class SyncMortality {
    @SerializedName("name")
    String name;
    @SerializedName("human_name")
    String human_name;
    @SerializedName("old_name")
    String old_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHuman_name() {
        return human_name;
    }

    public void setHuman_name(String human_name) {
        this.human_name = human_name;
    }

    public String getOld_name() {
        return old_name;
    }

    public void setOld_name(String old_name) {
        this.old_name = old_name;
    }
}
