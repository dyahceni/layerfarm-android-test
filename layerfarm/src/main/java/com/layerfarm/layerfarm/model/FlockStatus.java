package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by agungsuyono on 01/02/18.
 * Flock Status
 *
 */
public class FlockStatus {

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("kandang nid")
    @Expose
    private Integer kandangNid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("farm nid")
    @Expose
    private String farmNid;
    @SerializedName("farm title")
    @Expose
    private String farmTitle;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tgl_tetas")
    @Expose
    private List<Hatching> tglTetas = null;

    public Integer getKandangNid() {
        return kandangNid;
    }

    public void setKandangNid(Integer kandangNid) {
        this.kandangNid = kandangNid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFarmNid() {
        return farmNid;
    }

    public void setFarmNid(String farmNid) {
        this.farmNid = farmNid;
    }

    public String getFarmTitle() {
        return farmTitle;
    }

    public void setFarmTitle(String farmTitle) {
        this.farmTitle = farmTitle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Hatching> getTglTetas() {
        return tglTetas;
    }

    public void setTglTetas(List<Hatching> tglTetas) {
        this.tglTetas = tglTetas;
    }

    public class Hatching {

        @SerializedName("tgl tetas nid")
        @Expose
        private String tglTetasNid;
        @SerializedName("tgl_tetas")
        @Expose
        private String tglTetas;
        @SerializedName("jumlah_ayam")
        @Expose
        private Integer jumlahAyam;
        @SerializedName("umur")
        @Expose
        private String umur;
        @SerializedName("start recording")
        @Expose
        private Object startRecording;
        @SerializedName("strain_nid")
        @Expose
        private String strainNid;

        public String getTglTetasNid() {
            return tglTetasNid;
        }

        public void setTglTetasNid(String tglTetasNid) {
            this.tglTetasNid = tglTetasNid;
        }

        public String getTglTetas() {
            return tglTetas;
        }

        public void setTglTetas(String tglTetas) {
            this.tglTetas = tglTetas;
        }

        public Integer getJumlahAyam() {
            return jumlahAyam;
        }

        public void setJumlahAyam(Integer jumlahAyam) {
            this.jumlahAyam = jumlahAyam;
        }

        public String getUmur() {
            return umur;
        }

        public void setUmur(String umur) {
            this.umur = umur;
        }

        public Object getStartRecording() {
            return startRecording;
        }

        public void setStartRecording(Object startRecording) {
            this.startRecording = startRecording;
        }

        public String getStrainNid() {
            return strainNid;
        }

        public void setStrainNid(String strainNid) {
            this.strainNid = strainNid;
        }

    }

}



/*

public class FlockStatus {
    private Integer flockNid;
    private String name;
    private Integer locationNid;
    private String locationName;
    private String type;
    private String period;
    private String status;

    private class Hatching {
        private Integer tanggalTetasNid;
        private String tanggalTetas;
        private Integer jumlahAyam;
        private String umur;
        private String tanggalStartRecording;
        private Integer strainNid;
    }

}

*/

