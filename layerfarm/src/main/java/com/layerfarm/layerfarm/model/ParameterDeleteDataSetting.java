package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;


public class ParameterDeleteDataSetting {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterDeleteDataSetting(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;

        @SerializedName("type")
        private String b;


        public Arguments(String nid, String type){

            this.a = nid;
            this.b = type;
        }
    }
}
