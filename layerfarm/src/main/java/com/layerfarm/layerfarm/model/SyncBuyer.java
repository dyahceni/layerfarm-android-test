package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class SyncBuyer {
    @SerializedName("id")
    String id ="";
    @SerializedName("name")
    String name="";
    @SerializedName("nid")
    String rid="";
    @SerializedName("address")
    String address="";

    public SyncBuyer(String id, String name, String rid, String address){
        this.name = name;
        this.rid = rid;
        this.address = address;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
