
package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hatchinglist {

    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("item_nid")
    @Expose
    private String itemNid;
    @SerializedName("hatch_date")
    @Expose
    private String hatchDate;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("effective_date")
    @Expose
    private String effective_date;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getItemNid() {
        return itemNid;
    }

    public void setItemNid(String itemNid) {
        this.itemNid = itemNid;
    }

    public String getHatchDate() {
        return hatchDate;
    }

    public void setHatchDate(String hatchDate) {
        this.hatchDate = hatchDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }
}
