package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class SyncVendor {
    @SerializedName("id")
    String id;
    @SerializedName("name")
    String name;
    @SerializedName("nid")
    String rid;

     public SyncVendor(String name, String rid){
         this.name = name;
         this.rid = rid;
     }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
