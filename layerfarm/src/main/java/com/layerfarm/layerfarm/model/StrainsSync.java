package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class StrainsSync {
    @SerializedName("name")
    private String name;
    @SerializedName("rid")
    private String rid;

    public StrainsSync(String name, String rid){

        this.name = name;
        this.rid =rid;
    }
    public String getRid(){
        return rid;
    }
    public void setRid(String rid){
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
