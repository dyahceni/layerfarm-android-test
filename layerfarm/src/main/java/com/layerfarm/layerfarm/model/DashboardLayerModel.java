package com.layerfarm.layerfarm.model;

public class DashboardLayerModel {
    private static DashboardLayerModel staticInstance;
    public static DashboardLayerModel getInstance(){
        if (staticInstance==null){
            staticInstance = new DashboardLayerModel();
        }
        return staticInstance;
    }
}
