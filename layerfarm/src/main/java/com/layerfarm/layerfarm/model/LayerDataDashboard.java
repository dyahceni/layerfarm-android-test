package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LayerDataDashboard {

    private static LayerDataDashboard staticInstance;

    public static LayerDataDashboard getInstance(){
        if (staticInstance==null){
            staticInstance = new LayerDataDashboard();
        }
        return staticInstance;
    }

    @SerializedName("last_recording")
    @Expose
    private String lastRecording;

    @SerializedName("egg_sell")
    @Expose
    private ArrayList<EggSell> eggSell = null;

    @SerializedName("m_today")
    @Expose
    private ArrayList<MToday> mToday = null;
    @SerializedName("m_lastday")
    @Expose
    private ArrayList<MLastday> mLastday = null;
    @SerializedName("m_1w")
    @Expose
    private ArrayList<M1w> m1w = null;
    @SerializedName("m_1m")
    @Expose
    private ArrayList<M1m> m1m = null;
    @SerializedName("m_3m")
    @Expose
    private ArrayList<M3m> m3m = null;
    @SerializedName("m_6m")
    @Expose
    private ArrayList<M6m> m6m = null;
    @SerializedName("m_1y")
    @Expose
    private ArrayList<M1y> m1y = null;
    @SerializedName("m_2y")
    @Expose
    private ArrayList<M2y> m2y = null;
    @SerializedName("m_ytd")
    @Expose
    private ArrayList<MYtd> mYtd = null;
    @SerializedName("p_today")
    @Expose
    private ArrayList<PToday> pToday = null;
    @SerializedName("p_lastday")
    @Expose
    private ArrayList<PLastday> pLastday = null;
    @SerializedName("p_1w")
    @Expose
    private ArrayList<P1w> p1w = null;
    @SerializedName("p_1m")
    @Expose
    private ArrayList<P1m> p1m = null;
    @SerializedName("p_3m")
    @Expose
    private ArrayList<P3m> p3m = null;
    @SerializedName("p_6m")
    @Expose
    private ArrayList<P6m> p6m = null;
    @SerializedName("p_1y")
    @Expose
    private ArrayList<P1y> p1y = null;
    @SerializedName("p_2y")
    @Expose
    private ArrayList<P2y> p2y = null;
    @SerializedName("p_ytd")
    @Expose
    private ArrayList<PYtd> pYtd = null;
    @SerializedName("exp_today")
    @Expose
    private ExpToday expToday;
    @SerializedName("exp_lastday")
    @Expose
    private ExpLastday expLastday;
    @SerializedName("exp_1w")
    @Expose
    private Exp1w exp1w;
    @SerializedName("exp_1m")
    @Expose
    private Exp1m exp1m;
    @SerializedName("exp_3m")
    @Expose
    private Exp3m exp3m;
    @SerializedName("exp_6m")
    @Expose
    private Exp6m exp6m;
    @SerializedName("exp_1y")
    @Expose
    private Exp1y exp1y;
    @SerializedName("exp_2y")
    @Expose
    private Exp2y exp2y;
    @SerializedName("exp_ytd")
    @Expose
    private ExpYtd expYtd;

    @SerializedName("income")
    @Expose
    private ArrayList<Income> income = null;

    @SerializedName("expense")
    @Expose
    private ArrayList<Expense> expense = null;

    @SerializedName("performance_date")
    @Expose
    private ArrayList<String> performanceDate = null;
    @SerializedName("population")
    @Expose
    private ArrayList<String> population = null;
    @SerializedName("egg_laid_egg")
    @Expose
    private ArrayList<String> eggLaidEgg = null;
    @SerializedName("egg_laid_weight")
    @Expose
    private ArrayList<String> eggLaidWeight = null;
    @SerializedName("fcr")
    @Expose
    private ArrayList<String> fcr = null;
    @SerializedName("mortality")
    @Expose
    private ArrayList<String> mortality = null;
    @SerializedName("henday")
    @Expose
    private ArrayList<String> henday = null;

    public String getLastRecording() {
        return lastRecording;
    }

    public void setLastRecording(String lastRecording) {
        this.lastRecording = lastRecording;
    }

    public ArrayList<EggSell> getEggSell() {
        return eggSell;
    }

    public void setEggSell(ArrayList<EggSell> eggSell) {
        this.eggSell = eggSell;
    }

    public ArrayList<MToday> getMToday() {
        return mToday;
    }

    public void setMToday(ArrayList<MToday> mToday) {
        this.mToday = mToday;
    }

    public ArrayList<MLastday> getMLastday() {
        return mLastday;
    }

    public void setMLastday(ArrayList<MLastday> mLastday) {
        this.mLastday = mLastday;
    }

    public ArrayList<M1w> getM1w() {
        return m1w;
    }

    public void setM1w(ArrayList<M1w> m1w) {
        this.m1w = m1w;
    }

    public ArrayList<M1m> getM1m() {
        return m1m;
    }

    public void setM1m(ArrayList<M1m> m1m) {
        this.m1m = m1m;
    }

    public ArrayList<M3m> getM3m() {
        return m3m;
    }

    public void setM3m(ArrayList<M3m> m3m) {
        this.m3m = m3m;
    }

    public ArrayList<M6m> getM6m() {
        return m6m;
    }

    public void setM6m(ArrayList<M6m> m6m) {
        this.m6m = m6m;
    }

    public ArrayList<M1y> getM1y() {
        return m1y;
    }

    public void setM1y(ArrayList<M1y> m1y) {
        this.m1y = m1y;
    }

    public ArrayList<M2y> getM2y() {
        return m2y;
    }

    public void setM2y(ArrayList<M2y> m2y) {
        this.m2y = m2y;
    }

    public ArrayList<MYtd> getMYtd() {
        return mYtd;
    }

    public void setMYtd(ArrayList<MYtd> mYtd) {
        this.mYtd = mYtd;
    }

    public ArrayList<PToday> getPToday() {
        return pToday;
    }

    public void setPToday(ArrayList<PToday> pToday) {
        this.pToday = pToday;
    }

    public ArrayList<PLastday> getPLastday() {
        return pLastday;
    }

    public void setPLastday(ArrayList<PLastday> pLastday) {
        this.pLastday = pLastday;
    }

    public ArrayList<P1w> getP1w() {
        return p1w;
    }

    public void setP1w(ArrayList<P1w> p1w) {
        this.p1w = p1w;
    }

    public ArrayList<P1m> getP1m() {
        return p1m;
    }

    public void setP1m(ArrayList<P1m> p1m) {
        this.p1m = p1m;
    }

    public ArrayList<P3m> getP3m() {
        return p3m;
    }

    public void setP3m(ArrayList<P3m> p3m) {
        this.p3m = p3m;
    }

    public ArrayList<P6m> getP6m() {
        return p6m;
    }

    public void setP6m(ArrayList<P6m> p6m) {
        this.p6m = p6m;
    }

    public ArrayList<P1y> getP1y() {
        return p1y;
    }

    public void setP1y(ArrayList<P1y> p1y) {
        this.p1y = p1y;
    }

    public ArrayList<P2y> getP2y() {
        return p2y;
    }

    public void setP2y(ArrayList<P2y> p2y) {
        this.p2y = p2y;
    }

    public ArrayList<PYtd> getPYtd() {
        return pYtd;
    }

    public void setPYtd(ArrayList<PYtd> pYtd) {
        this.pYtd = pYtd;
    }

    public ExpToday getExpToday() {
        return expToday;
    }

    public void setExpToday(ExpToday expToday) {
        this.expToday = expToday;
    }

    public ExpLastday getExpLastday() {
        return expLastday;
    }

    public void setExpLastday(ExpLastday expLastday) {
        this.expLastday = expLastday;
    }

    public Exp1w getExp1w() {
        return exp1w;
    }

    public void setExp1w(Exp1w exp1w) {
        this.exp1w = exp1w;
    }

    public Exp1m getExp1m() {
        return exp1m;
    }

    public void setExp1m(Exp1m exp1m) {
        this.exp1m = exp1m;
    }

    public Exp3m getExp3m() {
        return exp3m;
    }

    public void setExp3m(Exp3m exp3m) {
        this.exp3m = exp3m;
    }

    public Exp6m getExp6m() {
        return exp6m;
    }

    public void setExp6m(Exp6m exp6m) {
        this.exp6m = exp6m;
    }

    public Exp1y getExp1y() {
        return exp1y;
    }

    public void setExp1y(Exp1y exp1y) {
        this.exp1y = exp1y;
    }

    public Exp2y getExp2y() {
        return exp2y;
    }

    public void setExp2y(Exp2y exp2y) {
        this.exp2y = exp2y;
    }

    public ExpYtd getExpYtd() {
        return expYtd;
    }

    public void setExpYtd(ExpYtd expYtd) {
        this.expYtd = expYtd;
    }

    public ArrayList<Expense> getExpense() {
        return expense;
    }

    public void setExpense(ArrayList<Expense> expense) {
        this.expense = expense;
    }

    public ArrayList<Income> getIncome() {
        return income;
    }

    public void setIncome(ArrayList<Income> income) {
        this.income = income;
    }


    public ArrayList<String> getPerformanceDate() {
        return performanceDate;
    }

    public void setPerformanceDate(ArrayList<String> performanceDate) {
        this.performanceDate = performanceDate;
    }

    public ArrayList<String> getPopulation() {
        return population;
    }

    public void setPopulation(ArrayList<String> population) {
        this.population = population;
    }

    public ArrayList<String> getEggLaidEgg() {
        return eggLaidEgg;
    }

    public void setEggLaidEgg(ArrayList<String> eggLaidEgg) {
        this.eggLaidEgg = eggLaidEgg;
    }

    public ArrayList<String> getEggLaidWeight() {
        return eggLaidWeight;
    }

    public void setEggLaidWeight(ArrayList<String> eggLaidWeight) {
        this.eggLaidWeight = eggLaidWeight;
    }

    public ArrayList<String> getFcr() {
        return fcr;
    }

    public void setFcr(ArrayList<String> fcr) {
        this.fcr = fcr;
    }

    public ArrayList<String> getMortality() {
        return mortality;
    }

    public void setMortality(ArrayList<String> mortality) {
        this.mortality = mortality;
    }

    public ArrayList<String> getHenday() {
        return henday;
    }

    public void setHenday(ArrayList<String> henday) {
        this.henday = henday;
    }


    public class Exp1m {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Exp1w {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Exp1y {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Exp2y {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Exp3m {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Exp6m {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class ExpLastday {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class ExpToday {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class ExpYtd {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public class Income {

        @SerializedName("income")
        @Expose
        private String income;

        public String getIncome() { return income; }

        public void setIncome(String income) { this.income = income; }

    }

    public class Expense {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public class LastRecording {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public class EggSell {

        @SerializedName("egg_price")
        @Expose
        private String eggPrice;

        public String getEggPrice() {
            return eggPrice;
        }

        public void setEggPrice(String eggPrice) {
            this.eggPrice = eggPrice;
        }

    }

    public class M1m {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class M1w {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class M1y {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class M2y {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class M3m {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class M6m {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class MLastday {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class MToday {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class MYtd {

        @SerializedName("mortality_type")
        @Expose
        private String mortalityType;
        @SerializedName("mortality_value")
        @Expose
        private String mortalityValue;

        public String getMortalityType() {
            return mortalityType;
        }

        public void setMortalityType(String mortalityType) {
            this.mortalityType = mortalityType;
        }

        public String getMortalityValue() {
            return mortalityValue;
        }

        public void setMortalityValue(String mortalityValue) {
            this.mortalityValue = mortalityValue;
        }

    }


    public class P1m {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class P1w {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class P1y {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class P2y {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class P3m {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class P6m {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class PLastday {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class PToday {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

    }


    public class PYtd {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("egg")
        @Expose
        private String egg;
        @SerializedName("weight")
        @Expose
        private String weight;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEgg() {
            return egg;
        }

        public void setEgg(String egg) {
            this.egg = egg;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }
    }



}


