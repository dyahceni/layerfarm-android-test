package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DashboardLayerDetailProductionModel {
    private static DashboardLayerDetailProductionModel staticInstance;

    public static DashboardLayerDetailProductionModel getInstance(){
        if (staticInstance==null){
            staticInstance = new DashboardLayerDetailProductionModel();
        }
        return staticInstance;
    }

    @SerializedName("performance")
    GrowerPerformance performance;

    public GrowerPerformance getPerformance() {
        return performance;
    }

    public void setPerformance(GrowerPerformance performance) {
        this.performance = performance;
    }



    public class GrowerPerformance{
        @SerializedName("performance_date")
        private ArrayList<String> performanceDate = new ArrayList<>();
        @SerializedName("population")
        private ArrayList<String> population = new ArrayList<>();
        @SerializedName("egg_laid_egg")
        private ArrayList<String> eggLaidEgg = new ArrayList<>();
        @SerializedName("egg_laid_weight")
        private ArrayList<String> eggLaidWeight = new ArrayList<>();
        @SerializedName("fcr")
        private ArrayList<String> fcr = new ArrayList<>();
        @SerializedName("mortality")
        private ArrayList<String> mortality = new ArrayList<>();
        @SerializedName("henday")
        private ArrayList<String> henday = new ArrayList<>();
        @SerializedName("detail")
        GrowerPerformanceDetail detail;
        public ArrayList<String> getPerformanceDate() {
            return performanceDate;
        }

        public void setPerformanceDate(ArrayList<String> performanceDate) {
            this.performanceDate = performanceDate;
        }

        public ArrayList<String> getPopulation() {
            return population;
        }

        public void setPopulation(ArrayList<String> population) {
            this.population = population;
        }

        public ArrayList<String> getEggLaidEgg() {
            return eggLaidEgg;
        }

        public void setEggLaidEgg(ArrayList<String> eggLaidEgg) {
            this.eggLaidEgg = eggLaidEgg;
        }

        public ArrayList<String> getEggLaidWeight() {
            return eggLaidWeight;
        }

        public void setEggLaidWeight(ArrayList<String> eggLaidWeight) {
            this.eggLaidWeight = eggLaidWeight;
        }

        public ArrayList<String> getFcr() {
            return fcr;
        }

        public void setFcr(ArrayList<String> fcr) {
            this.fcr = fcr;
        }

        public ArrayList<String> getMortality() {
            return mortality;
        }

        public void setMortality(ArrayList<String> mortality) {
            this.mortality = mortality;
        }

        public ArrayList<String> getHenday() {
            return henday;
        }

        public void setHenday(ArrayList<String> henday) {
            this.henday = henday;
        }



        public GrowerPerformanceDetail getDetail() {
            return detail;
        }

        public void setDetail(GrowerPerformanceDetail detail) {
            this.detail = detail;
        }
    }
    public class GrowerPerformanceDetail{
        @SerializedName("location_flock")
        LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock;

        @SerializedName("flock_data")
        GrowerPerformanceDetailFlockData flock_data;


        public LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> getLocation_flock() {
            return location_flock;
        }

        public void setLocation_flock(LinkedHashMap<String, ArrayList<GrowerPerformanceDetailLocationFlock>> location_flock) {
            this.location_flock = location_flock;
        }

        public GrowerPerformanceDetailFlockData getFlock_data() {
            return flock_data;
        }

        public void setFlock_data(GrowerPerformanceDetailFlockData flock_data) {
            this.flock_data = flock_data;
        }


    }


    public class GrowerPerformanceDetailLocationFlock{
        @SerializedName("flock_nid")
        String flock_nid;
        @SerializedName("farm_nid")
        String farm_nid;
        @SerializedName("flock_name")
        String flock_name;
        @SerializedName("farm_name")
        String farm_name;
        @SerializedName("flock_period")
        String flock_period;
        @SerializedName("flock_type")
        String flock_type;
        @SerializedName("flock_capacity")
        String flock_capacity;

        public String getFlock_nid() {
            return flock_nid;
        }

        public void setFlock_nid(String flock_nid) {
            this.flock_nid = flock_nid;
        }

        public String getFarm_nid() {
            return farm_nid;
        }

        public void setFarm_nid(String farm_nid) {
            this.farm_nid = farm_nid;
        }

        public String getFlock_name() {
            return flock_name;
        }

        public void setFlock_name(String flock_name) {
            this.flock_name = flock_name;
        }

        public String getFarm_name() {
            return farm_name;
        }

        public void setFarm_name(String farm_name) {
            this.farm_name = farm_name;
        }

        public String getFlock_period() {
            return flock_period;
        }

        public void setFlock_period(String flock_period) {
            this.flock_period = flock_period;
        }

        public String getFlock_type() {
            return flock_type;
        }

        public void setFlock_type(String flock_type) {
            this.flock_type = flock_type;
        }

        public String getFlock_capacity() {
            return flock_capacity;
        }

        public void setFlock_capacity(String flock_capacity) {
            this.flock_capacity = flock_capacity;
        }
    }
    public class GrowerPerformanceDetailFlockData{
        @SerializedName("recording_date")
        LinkedHashMap<Integer, ArrayList<String>> recording_date = new LinkedHashMap<>();


        @SerializedName("detail_total_egg")
        LinkedHashMap<Integer, ArrayList<Integer>> total_egg = new LinkedHashMap<>();

        @SerializedName("detail_total_weight")
        LinkedHashMap<Integer, ArrayList<Float>> total_weight = new LinkedHashMap<>();

        @SerializedName("detail_henday")
        LinkedHashMap<Integer, ArrayList<Float>> henday = new LinkedHashMap<>();

        @SerializedName("detail_fcr")
        LinkedHashMap<Integer, ArrayList<Float>> fcr = new LinkedHashMap<>();
    //    @SerializedName("detail_fcr")
    //    ArrayList<String> fcr;

        public LinkedHashMap<Integer, ArrayList<String>> getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(LinkedHashMap<Integer, ArrayList<String>> recording_date) {
            this.recording_date = recording_date;
        }



     //   public ArrayList<String> getFcr() {
     //       return fcr;
     //   }

     //   public void setFcr(ArrayList<String> fcr) {
    //        this.fcr = fcr;
    //    }


        public LinkedHashMap<Integer, ArrayList<Integer>> getTotal_egg() {
            return total_egg;
        }

        public void setTotal_egg(LinkedHashMap<Integer, ArrayList<Integer>> total_egg) {
            this.total_egg = total_egg;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(LinkedHashMap<Integer, ArrayList<Float>> total_weight) {
            this.total_weight = total_weight;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getHenday() {
            return henday;
        }

        public void setHenday(LinkedHashMap<Integer, ArrayList<Float>> henday) {
            this.henday = henday;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getFcr() {
            return fcr;
        }

        public void setFcr(LinkedHashMap<Integer, ArrayList<Float>> fcr) {
            this.fcr = fcr;
        }
    }

}
