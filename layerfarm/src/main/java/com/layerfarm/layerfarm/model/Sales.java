package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Sales implements Serializable {

    @SerializedName("date")
    String a = "";
    @SerializedName("buyer")
    String b ="";
    @SerializedName("product")
    String c ="";
    @SerializedName("quality")
    String d = "";
    @SerializedName("price")
    String e;
    @SerializedName("quantity")
    String f;
    @SerializedName("farm")
    ArrayList<String> g;
    @SerializedName("note")
    String h;
    @SerializedName("nid")
    String i = "";
    @SerializedName("buyer_name")
    String j = "";

    public String getNid() {
        return i;
    }

    public void setNid(String nid) {
        this.i = nid;
    }

    public String getDate() {
        return a;
    }

    public void setDate(String date) {
        this.a = date;
    }

    public String getBuyer() {
        return b;
    }

    public void setBuyer(String buyer) {
        this.b = buyer;
    }

    public String getProduct() {
        return c;
    }

    public void setProduct(String product) {
        this.c = product;
    }

    public String getQuality() {
        return d;
    }

    public void setQuality(String quality) {
        this.d = quality;
    }

    public String getPrice() {
        return e;
    }

    public void setPrice(String price) {
        this.e = price;
    }

    public String getQuantity() {
        return f;
    }

    public void setQuantity(String quantity) {
        this.f = quantity;
    }

    public ArrayList<String> getFarm() {
        return g;
    }

    public void setFarm(ArrayList<String> farm) {
        this.g = farm;
    }

    public String getNote() {
        return h;
    }

    public void setNote(String note) {
        this.h = note;
    }

    public String getBuyerName() {
        return j;
    }

    public void setBuyerName(String buyerName
    ) {
        this.j = j;
    }
}
