package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class mFeed {
    @SerializedName("nid")
    private String nid;
    @SerializedName("name")
    private String name;
    @SerializedName("status")
    private String status;
    @SerializedName("description")
    private String description;
    private String delete_status;


    public mFeed(String nid, String name, String status, String description){
        this.nid = nid;
        this.name = name;
        this.status = status;
        this.description = description;
        this.delete_status = delete_status;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeleteStatus() {
        return delete_status;
    }
    public void setDeleteStatus(String delete_status) {
        this.delete_status = delete_status;
    }
}
