package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserValidate {
    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("email")
    @Expose
    private String email;

    public String getStatus() {
        return status;
    }

    public String getEmail(){
        return email;
    }
}
