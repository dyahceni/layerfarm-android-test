package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class SyncEggQuality {
    @SerializedName("name")
    String name;
    @SerializedName("display_name")
    String display_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}
