package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ChickinEntry {
    private static ChickinEntry staticInstance;
    public static ChickinEntry getInstance(){
        if (staticInstance==null){
            staticInstance = new ChickinEntry();
        }
        return staticInstance;
    }

    @SerializedName("house")
    LinkedHashMap<String, String> house = new LinkedHashMap<>();
    @SerializedName("strain")
    List<StrainsSync> strain = new ArrayList<>();

    public LinkedHashMap<String, String> getHouse() {
        return house;
    }

    public void setHouse(LinkedHashMap<String, String> house) {
        this.house = house;
    }

    public List<StrainsSync> getStrain() {
        return strain;
    }

    public void setStrain(List<StrainsSync> strain) {
        this.strain = strain;
    }
}
