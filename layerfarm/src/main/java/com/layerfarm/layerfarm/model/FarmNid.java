package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class FarmNid {
    @SerializedName("nid")
    String nid;
    @SerializedName("vid")
    String vid;
    @SerializedName("type")
    String type;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
