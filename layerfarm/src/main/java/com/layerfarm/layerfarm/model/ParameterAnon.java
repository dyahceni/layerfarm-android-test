package com.layerfarm.layerfarm.model;

/**
 * Created by agungsuyono on 17/02/18.
 *
 */

public class ParameterAnon {
    private String module;
    private String function_name;
    private String[] args;
    private String anonymous;

    public ParameterAnon(String module, String function_name, String[] args, String anonymous) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
        this.anonymous = anonymous;
    }
}
