package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by agungsuyono on 29/12/17.
 *
 * Login Status
 */
public class LoginStatus {

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

}
