package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class getStrainModel {

    private static getStrainModel staticInstance;

    public static getStrainModel getInstance(){
        if (staticInstance==null){
            staticInstance = new getStrainModel();
        }
        return staticInstance;
    }


    @SerializedName("name")
    private String name;
    @SerializedName("rid")
    private String rid;

    public getStrainModel(){

    }
    public String getRid(){
        return rid;
    }
    public void setRid(String rid){
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
