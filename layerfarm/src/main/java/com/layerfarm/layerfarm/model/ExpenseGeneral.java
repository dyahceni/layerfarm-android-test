package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExpenseGeneral {

    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("item")
    @Expose
    private String item;
    @SerializedName("cost")
    @Expose
    private String cost;

    public ExpenseGeneral(String nid, String date, String vendor, String scope, String location, String item, String cost) {
        this.nid = nid;
        this.date = date;
        this.vendor = vendor;
        this.scope = scope;
        this.location = location;
        this.item = item;
        this.cost = cost;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }


}
