package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DashboardLayerDetailModel {
    private static DashboardLayerDetailModel staticInstance;

    public static DashboardLayerDetailModel getInstance(){
        if (staticInstance==null){
            staticInstance = new DashboardLayerDetailModel();
        }
        return staticInstance;
    }

    @SerializedName("performance")
    LayerPerformance performance;

    @SerializedName("detail")
    LayerPerformanceDetail detail;

    @SerializedName("location_flock")
    LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> location_flock;

    @SerializedName("flock_data")
    LayerPerformanceDetailFlockData flock_data;

    public LayerPerformance getPerformance() {
        return performance;
    }

    public void setPerformance(LayerPerformance performance) {
        this.performance = performance;
    }

    public LayerPerformanceDetail getDetail() {
        return detail;
    }

    public void setDetail(LayerPerformanceDetail detail) {
        this.detail = detail;
    }

    public LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> getLocation_flock() {
        return location_flock;
    }

    public void setLocation_flock(LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> location_flock) {
        this.location_flock = location_flock;
    }

    public LayerPerformanceDetailFlockData getFlock_data() {
        return flock_data;
    }

    public void setFlock_data(LayerPerformanceDetailFlockData flock_data) {
        this.flock_data = flock_data;
    }

    public class LayerPerformance{

        @SerializedName("detail")
        LayerPerformanceDetail detail;


        public LayerPerformanceDetail getDetail() {
            return detail;
        }

        public void setDetail(LayerPerformanceDetail detail) {
            this.detail = detail;
        }
    }
    public class LayerPerformanceDetail{
        @SerializedName("location_flock")
        LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> location_flock;

        @SerializedName("flock_data")
        LayerPerformanceDetailFlockData flock_data;


        public LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> getLocation_flock() {
            return location_flock;
        }

        public void setLocation_flock(LinkedHashMap<String, ArrayList<LayerPerformanceDetailLocationFlock>> location_flock) {
            this.location_flock = location_flock;
        }

        public LayerPerformanceDetailFlockData getFlock_data() {
            return flock_data;
        }

        public void setFlock_data(LayerPerformanceDetailFlockData flock_data) {
            this.flock_data = flock_data;
        }


    }


    public class LayerPerformanceDetailLocationFlock{
        @SerializedName("flock_nid")
        String flock_nid;
        @SerializedName("farm_nid")
        String farm_nid;
        @SerializedName("flock_name")
        String flock_name;
        @SerializedName("farm_name")
        String farm_name;
        @SerializedName("flock_period")
        String flock_period;
        @SerializedName("flock_type")
        String flock_type;
        @SerializedName("flock_capacity")
        String flock_capacity;
        @SerializedName("utilization")
        String utilization;
        @SerializedName("age")
        Integer age;
        @SerializedName("population")
        String population;
        @SerializedName("last_recorded")
        String last_recorded;
        @SerializedName("production")
        String production;
        @SerializedName("total_eggs")
        String total_eggs;
        @SerializedName("total_weight")
        String total_weight;
        @SerializedName("feed_consumption")
        String feed_consumption;
        @SerializedName("production_weight")
        String production_weight;


        public String getFlock_nid() {
            return flock_nid;
        }

        public void setFlock_nid(String flock_nid) {
            this.flock_nid = flock_nid;
        }

        public String getFarm_nid() {
            return farm_nid;
        }

        public void setFarm_nid(String farm_nid) {
            this.farm_nid = farm_nid;
        }

        public String getFlock_name() {
            return flock_name;
        }

        public void setFlock_name(String flock_name) {
            this.flock_name = flock_name;
        }

        public String getFarm_name() {
            return farm_name;
        }

        public void setFarm_name(String farm_name) {
            this.farm_name = farm_name;
        }

        public String getFlock_period() {
            return flock_period;
        }

        public void setFlock_period(String flock_period) {
            this.flock_period = flock_period;
        }

        public String getFlock_type() {
            return flock_type;
        }

        public void setFlock_type(String flock_type) {
            this.flock_type = flock_type;
        }

        public String getFlock_capacity() {
            return flock_capacity;
        }

        public void setFlock_capacity(String flock_capacity) {
            this.flock_capacity = flock_capacity;
        }

        public String getUtilization() {
            return utilization;
        }

        public void setUtilization(String utilization) {
            this.utilization = utilization;
        }

        public String getProduction() {
            return production;
        }

        public void setProduction(String production) {
            this.production = production;
        }

        public String getTotal_eggs() {
            return total_eggs;
        }

        public void setTotal_eggs(String total_eggs) {
            this.total_eggs = total_weight;
        }

        public String getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(String total_weight) {
            this.total_weight = total_weight;
        }

        public String getFeed_consumption() {
            return feed_consumption;
        }

        public void setFeed_consumption(String feed_consumption) {
            this.feed_consumption = feed_consumption;
        }

        public String getProduction_weight() {
            return production_weight;
        }

        public void setProduction_weight(String production_weight) {
            this.production_weight = production_weight;
        }

        public String getPopulation() {
            return population;
        }

        public void setPopulation(String population) {
            this.population = population;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getLast_recording() {
            return last_recorded;
        }

        public void setLast_recording(String last_recorded) {
            this.last_recorded = last_recorded;
        }
    }
    public class LayerPerformanceDetailFlockData{
        @SerializedName("recording_date")
        LinkedHashMap<Integer, ArrayList<String>> recording_date = new LinkedHashMap<>();
        @SerializedName("detail_mortality")
        LinkedHashMap<Integer, ArrayList<Float>> mortality = new LinkedHashMap<>();
        @SerializedName("detail_population")
        LinkedHashMap<Integer, ArrayList<Integer>> population = new LinkedHashMap<>();


        @SerializedName("detail_total_egg")
        LinkedHashMap<Integer, ArrayList<Integer>> total_egg = new LinkedHashMap<>();

        @SerializedName("detail_total_weight")
        LinkedHashMap<Integer, ArrayList<Float>> total_weight = new LinkedHashMap<>();

        @SerializedName("detail_henday")
        LinkedHashMap<Integer, ArrayList<Float>> henday = new LinkedHashMap<>();

        @SerializedName("detail_fcr")
        LinkedHashMap<Integer, ArrayList<Float>> fcr = new LinkedHashMap<>();
    //    @SerializedName("detail_fcr")
    //    ArrayList<String> fcr;

        public LinkedHashMap<Integer, ArrayList<String>> getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(LinkedHashMap<Integer, ArrayList<String>> recording_date) {
            this.recording_date = recording_date;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getMortality() {
            return mortality;
        }

        public void setMortality(LinkedHashMap<Integer, ArrayList<Float>> mortality) {
            this.mortality = mortality;
        }

        public LinkedHashMap<Integer, ArrayList<Integer>> getPopulation() {
            return population;
        }

        public void setPopulation(LinkedHashMap<Integer, ArrayList<Integer>> population) {
            this.population = population;
        }



     //   public ArrayList<String> getFcr() {
     //       return fcr;
     //   }

     //   public void setFcr(ArrayList<String> fcr) {
    //        this.fcr = fcr;
    //    }


        public LinkedHashMap<Integer, ArrayList<Integer>> getTotal_egg() {
            return total_egg;
        }

        public void setTotal_egg(LinkedHashMap<Integer, ArrayList<Integer>> total_egg) {
            this.total_egg = total_egg;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(LinkedHashMap<Integer, ArrayList<Float>> total_weight) {
            this.total_weight = total_weight;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getHenday() {
            return henday;
        }

        public void setHenday(LinkedHashMap<Integer, ArrayList<Float>> henday) {
            this.henday = henday;
        }

        public LinkedHashMap<Integer, ArrayList<Float>> getFcr() {
            return fcr;
        }

        public void setFcr(LinkedHashMap<Integer, ArrayList<Float>> fcr) {
            this.fcr = fcr;
        }
    }

}
