package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class chickinHistory {
    @SerializedName("nid")
    String nid;

    @SerializedName("rare_type")
    String rare_type;

    @SerializedName("hatching_date")
    String hatching_date;

    @SerializedName("in_week")
    String in_week;

    @SerializedName("in_days")
    String in_days;

    @SerializedName("age_on_date")
    String age_on_date;

    @SerializedName("strain_id")
    String strain_id;

    @SerializedName("strain_name")
    String strain_name;

    @SerializedName("distribution")
    ArrayList<distribution_history> distribution_history_data;

    @SerializedName("delete_status")
    String delete_status;

    public chickinHistory(){

    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getRare_type() {
        return rare_type;
    }

    public void setRare_type(String rare_type) {
        this.rare_type = rare_type;
    }

    public String getHatching_date() {
        return hatching_date;
    }

    public void setHatching_date(String hatching_date) {
        this.hatching_date = hatching_date;
    }

    public String getStrain_id() {
        return strain_id;
    }

    public void setStrain_id(String strain_id) {
        this.strain_id = strain_id;
    }

    public String getStrain_name() {
        return strain_name;
    }

    public void setStrain_name(String strain_name) {
        this.strain_name = strain_name;
    }


    public String getIn_week() {
        return in_week;
    }

    public void setIn_week(String in_week) {
        this.in_week = in_week;
    }

    public String getIn_days() {
        return in_days;
    }

    public void setIn_days(String in_days) {
        this.in_days = in_days;
    }

    public String getAge_on_date() {
        return age_on_date;
    }

    public void setAge_on_date(String age_on_date) {
        this.age_on_date = age_on_date;
    }

    public ArrayList<distribution_history> getDistribution_history_data() {
        return distribution_history_data;
    }

    public void setDistribution_history_data(ArrayList<distribution_history> distribution_history_data) {
        this.distribution_history_data = distribution_history_data;
    }

    public String getDelete_status() {
        return delete_status;
    }

    public void setDelete_status(String delete_status) {
        this.delete_status = delete_status;
    }

    public static class distribution_history implements Serializable {
        @SerializedName("id")
        private String id;
        @SerializedName("flock_nid")
        private String flock_nid;
        @SerializedName("flock_name")
        private String flock_name;
        @SerializedName("type")
        private String type;
        @SerializedName("number_of_bird")
        private String number_of_bird;
        @SerializedName("recording_date")
        private String recording_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFlock_nid() {
            return flock_nid;
        }

        public void setFlock_nid(String flock_nid) {
            this.flock_nid = flock_nid;
        }

        public String getNumber_of_bird() {
            return number_of_bird;
        }

        public String getFlock_name() {
            return flock_name;
        }

        public void setFlock_name(String flock_name) {
            this.flock_name = flock_name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }


        public void setNumber_of_bird(String number_of_bird) {
            this.number_of_bird = number_of_bird;
        }

        public String getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(String recording_date) {
            this.recording_date = recording_date;
        }
    }
}
