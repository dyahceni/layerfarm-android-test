package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;
import com.layerfarm.layerfarm.Env;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class RecordingEntry {
    private static RecordingEntry staticInstance;

    public static RecordingEntry getInstance(){
        if (staticInstance==null){
            staticInstance = new RecordingEntry();
        }
        return staticInstance;
    }
    @SerializedName("profil_kandang")
    private ProfileKandang profil_kandang;
    @SerializedName("recording")
    private RecordingData recording;
    @SerializedName("body_weight")
    private BodyWeightData body_weight;
    @SerializedName("transfer")
    private TransferData transfer;
    @SerializedName("environtment")
    private Environtment environtment;

    public ProfileKandang getProfil_kandang() {
        return profil_kandang;
    }
    public void setProfil_kandang(ProfileKandang profil_kandang) {
        this.profil_kandang = profil_kandang;
    }

    public RecordingEntry.RecordingData getRecording() {
        return recording;
    }
    public void setRecording(RecordingEntry.RecordingData recording) {
        this.recording = recording;
    }

    public TransferData getTransfer() {
        return transfer;
    }
    public void setTransfer(TransferData transfer) {
        this.transfer = transfer;
    }

    public BodyWeightData getBody_weight() {
        return body_weight;
    }
    public void setBody_weight(BodyWeightData body_weight) {
        this.body_weight = body_weight;
    }

    public Environtment getEnvirontment() {
        return environtment;
    }
    public void setEnvirontment(Environtment environtment) {
        this.environtment = environtment;
    }

    public static class ProfileKandang {
        @SerializedName("title")
        private String title = "";
        @SerializedName("recording_date")
        private String recording_date = "";
        @SerializedName("kandang")
        private Kandang kandang;
        @SerializedName("hatching")
        private ArrayList<Hatching> hatching = new ArrayList<>();

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Kandang getKandang() {
            return kandang;
        }

        public void setKandang(Kandang kandang) {
            this.kandang = kandang;
        }

        public ArrayList<Hatching> getHatching() {
            return hatching;
        }

        public void setHatching(ArrayList<Hatching> hatching) {
            this.hatching = hatching;
        }

        public String getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(String recording_date) {
            this.recording_date = recording_date;
        }
    }
    public class Kandang{
        @SerializedName("prefix")
        private String previx = "";
        @SerializedName("markup")
        private String markup = "";

        public String getPrevix() {
            return previx;
        }

        public void setPrevix(String previx) {
            this.previx = previx;
        }

        public String getMarkup() {
            return markup;
        }

        public void setMarkup(String markup) {
            this.markup = markup;
        }

    }
    public class Hatching{
        @SerializedName("tgl_tetas_nid")
        private String tgl_tetas_nid;
        @SerializedName("hatch_date")
        private String hatch_date;
        @SerializedName("strain")
        private String strain;
        @SerializedName("total_bird_title")
        private String total_bird_title;
        @SerializedName("total_bird_value")
        private String total_bird_value;
        @SerializedName("age_bird_title")
        private String age_bird_title;
        @SerializedName("age_bird_value")
        private String age_bird_value;

        public String getTgl_tetas_nid(){
            return tgl_tetas_nid;
        }
        public void setTgl_tetas_nid(String tgl_tetas_nid){
            this.tgl_tetas_nid = tgl_tetas_nid;
        }
        public String getHatch_date() {
            return hatch_date;
        }

        public void setTgl_tetasHatch_date(String hatch_date) {
            this.hatch_date = hatch_date;
        }
        public String getStrain() {
            return strain;
        }

        public void setStrain(String strain) {
            this.strain = strain;
        }

        public String getTotal_bird_title() {
            return total_bird_title;
        }

        public void setTotal_bird_title(String total_bird_title) {
            this.total_bird_title = total_bird_title;
        }

        public String getTotal_bird_value() {
            return total_bird_value;
        }

        public void setTotal_bird_value(String total_bird_value) {
            this.total_bird_value = total_bird_value;
        }

        public String getAge_bird_title() {
            return age_bird_title;
        }

        public void setAge_bird_title(String age_bird_title) {
            this.age_bird_title = age_bird_title;
        }

        public String getAge_bird_value() {
            return age_bird_value;
        }

        public void setAge_bird_value(String age_bird_value) {
            this.age_bird_value = age_bird_value;
        }


    }
    public static class RecordingData{
        @SerializedName("recording_date")
        private String recording_date;
        @SerializedName("production")
        private ArrayList<Production> production = new ArrayList<>();
        @SerializedName("production_value")
        private ArrayList<ProductionValue> productionValues = new ArrayList<>();
        @SerializedName("options_feed")
        private LinkedHashMap<Integer, String> options_feed = new LinkedHashMap<>();
        @SerializedName("feed_value")
        private LinkedHashMap<Integer, String> feed_value = new LinkedHashMap<>();
        @SerializedName("options_medivac")
        private LinkedHashMap<Integer, String> options_medivac = new LinkedHashMap<>();
        @SerializedName("medivac_value")
        private LinkedHashMap<Integer, String> medivac_value = new LinkedHashMap<>();
        @SerializedName("mortality_type")
        private LinkedHashMap<String, String> mortality_type;
        @SerializedName("mortality_value")
        private ArrayList<LinkedHashMap<String, String>> mortality_value = new ArrayList<>();

        public String getRecording_date() {
            return recording_date;
        }

        public void setRecording_date(String recording_date) {
            this.recording_date = recording_date;
        }

        public ArrayList<Production> getProduction() {
            return production;
        }

        public void setProduction(ArrayList<Production> production) {
            this.production = production;
        }

        public ArrayList<ProductionValue> getProductionValues() {
            return productionValues;
        }

        public void setProductionValues(ArrayList<ProductionValue> productionValues) {
            this.productionValues = productionValues;
        }

        public LinkedHashMap<Integer, String> getOptions_feed() {
            return options_feed;
        }

        public void setOptions_feed(LinkedHashMap<Integer, String> options_feed) {
            this.options_feed = options_feed;
        }

        public LinkedHashMap<Integer, String> getOptions_medivac() {
            return options_medivac;
        }

        public void setOptions_medivac(LinkedHashMap<Integer, String> options_medivac) {
            this.options_medivac = options_medivac;
        }

        public LinkedHashMap<String, String> getMortality_type() {
            return mortality_type;
        }

        public void setMortality_type(LinkedHashMap<String, String> mortality_type) {
            this.mortality_type = mortality_type;
        }

        public LinkedHashMap<Integer, String> getFeed_value() {
            return feed_value;
        }

        public void setFeed_value(LinkedHashMap<Integer, String> feed_value) {
            this.feed_value = feed_value;
        }

        public LinkedHashMap<Integer, String> getMedivac_value() {
            return medivac_value;
        }

        public void setMedivac_value(LinkedHashMap<Integer, String> medivac_value) {
            this.medivac_value = medivac_value;
        }

        public ArrayList<LinkedHashMap<String, String>> getMortality_value() {
            return mortality_value;
        }

        public void setMortality_value(ArrayList<LinkedHashMap<String, String>> mortality_value) {
            this.mortality_value = mortality_value;
        }
    }
    public class Production{
        @SerializedName("name")
        private String name;
        @SerializedName("eggs")
        private String eggs;
        @SerializedName("weight")
        private String weight;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProduction_eggs() {
            return eggs;
        }

        public void setProduction_eggs(String eggs) {
            this.eggs = eggs;
        }

        public String getProduction_weight() {
            return weight;
        }

        public void setProduction_weight(String weight) {
            this.weight = weight;
        }
    }
    public class ProductionValue{
        @SerializedName("name")
        private String name;
        @SerializedName("production_kg")
        private String production_kg;
        @SerializedName("production_weight")
        private String production_weight;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProduction_kg() {
            return production_kg;
        }

        public void setProduction_kg(String production_kg) {
            this.production_kg = production_kg;
        }

        public String getProduction_weight() {
            return production_weight;
        }

        public void setProduction_weight(String production_weight) {
            this.production_weight = production_weight;
        }
    }
    public static class TransferData{
        @SerializedName("options_hatch_date")
        private LinkedHashMap<Integer, String> options_hatch_date;
        @SerializedName("options_houses")
        private LinkedHashMap<Integer, String> options_houses;
        @SerializedName("transfer_value")
        private ArrayList<DailyRecordingTransfer> transfer_value;
        @SerializedName("afkir_value")
        private ArrayList<DailyRecordingSpentHen> afkir_value;

        public LinkedHashMap<Integer, String> getOptions_hatch_date() {
            return options_hatch_date;
        }
        public void setOptions_hatch_date(LinkedHashMap<Integer, String> options_hatch_date) {
            this.options_hatch_date = options_hatch_date;
        }

        public LinkedHashMap<Integer, String> getOptions_houses() {
            return options_houses;
        }
        public void setOptions_houses(LinkedHashMap<Integer, String> options_houses) {
            this.options_houses = options_houses;
        }

        public ArrayList<DailyRecordingTransfer> getTransfer_value() {
            return transfer_value;
        }

        public void setTransfer_value(ArrayList<DailyRecordingTransfer> transfer_value) {
            this.transfer_value = transfer_value;
        }

        public ArrayList<DailyRecordingSpentHen> getAfkir_value() {
            return afkir_value;
        }

        public void setAfkir_value(ArrayList<DailyRecordingSpentHen> afkir_value) {
            this.afkir_value = afkir_value;
        }
    }
    public static class BodyWeightData{
        @SerializedName("bw_value")
        String bw_value;

        public String getBw_value() {
            return bw_value;
        }

        public void setBw_value(String bw_value) {
            this.bw_value = bw_value;
        }
    }
    public static class Environtment{
        @SerializedName("light_duration")
        LinkedHashMap<String, String> light_duration;

        @SerializedName("light_intensity")
        String light_intensity;

        @SerializedName("house_temperature")
        LinkedHashMap<String, String> house_temperature;

        @SerializedName("house_humidity")
        LinkedHashMap<String, String> house_humidity;

        public LinkedHashMap<String, String> getLight_duration() {
            return light_duration;
        }

        public void setLight_duration(LinkedHashMap<String, String> light_duration) {
            this.light_duration = light_duration;
        }

        public String getLight_intensity() {
            return light_intensity;
        }

        public void setLight_intensity(String light_intensity) {
            this.light_intensity = light_intensity;
        }

        public LinkedHashMap<String, String> getHouse_temperature() {
            return house_temperature;
        }

        public void setHouse_temperature(LinkedHashMap<String, String> house_temperature) {
            this.house_temperature = house_temperature;
        }

        public LinkedHashMap<String, String> getHouse_humidity() {
            return house_humidity;
        }

        public void setHouse_humidity(LinkedHashMap<String, String> house_humidity) {
            this.house_humidity = house_humidity;
        }
    }
    public class LightDuration{
        @SerializedName("light_start")
        String light_start;

        @SerializedName("light_end")
        String light_end;

        public String getLight_start() {
            return light_start;
        }

        public void setLight_start(String light_start) {
            this.light_start = light_start;
        }

        public String getLight_end() {
            return light_end;
        }

        public void setLight_end(String light_end) {
            this.light_end = light_end;
        }
    }
    public class HouseTemperature{
        @SerializedName("temperature_1")
        String temperature_1;

        @SerializedName("temperature_2")
        String temperature_2;

        @SerializedName("temperature_3")
        String temperature_3;

        public String getTemperature_1() {
            return temperature_1;
        }

        public void setTemperature_1(String temperature_1) {
            this.temperature_1 = temperature_1;
        }

        public String getTemperature_2() {
            return temperature_2;
        }

        public void setTemperature_2(String temperature_2) {
            this.temperature_2 = temperature_2;
        }

        public String getTemperature_3() {
            return temperature_3;
        }

        public void setTemperature_3(String temperature_3) {
            this.temperature_3 = temperature_3;
        }
    }
    public class HouseHumidity{
        @SerializedName("humidity_1")
        String humidity_1;
        @SerializedName("humidity_2")
        String humidity_2;
        @SerializedName("humidity_3")
        String humidity_3;

        public String getHumidity_1() {
            return humidity_1;
        }

        public void setHumidity_1(String humidity_1) {
            this.humidity_1 = humidity_1;
        }

        public String getHumidity_2() {
            return humidity_2;
        }

        public void setHumidity_2(String humidity_2) {
            this.humidity_2 = humidity_2;
        }

        public String getHumidity_3() {
            return humidity_3;
        }

        public void setHumidity_3(String humidity_3) {
            this.humidity_3 = humidity_3;
        }
    }

}
