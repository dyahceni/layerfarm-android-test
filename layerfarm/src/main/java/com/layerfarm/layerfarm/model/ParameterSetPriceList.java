package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ParameterSetPriceList {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterSetPriceList(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("item_nid")
        private String a;

        @SerializedName("price_movement")
        private ArrayList <OvkPrice> b;

//        @SerializedName("effective_date")
//        private String c;

        public Arguments(String item_nid, ArrayList <OvkPrice> price_movement){
            this.a = item_nid;
            this.b = price_movement;
        }
    }
}
