package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by agungsuyono on 25/12/17.
 *
 * Connect Status
 */

public class Connect {
    @SerializedName("sessid")
    @Expose
    private String sessid;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("session_name")
    @Expose
    private String sessionName;
    @SerializedName("user")
    @Expose
    private User user;

    public String getSessid() {
        return sessid;
    }

    public void setSessid(String sessid) {
        this.sessid = sessid;
    }

    public String getToken() { return token; }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
