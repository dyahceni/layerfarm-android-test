package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class DailyRecordingTransfer {
    @SerializedName("number_hen_move")
    private String number_hen_move;
    @SerializedName("hatch_date_move")
    private String hatch_date_move;
    @SerializedName("to_house")
    private String to_house;

    public String getNumber_hen_move() {
        return number_hen_move;
    }

    public void setNumber_hen_move(String number_hen_move) {
        this.number_hen_move = number_hen_move;
    }

    public String getHatch_date_move() {
        return hatch_date_move;
    }

    public void setHatch_date_move(String hatch_date_move) {
        this.hatch_date_move = hatch_date_move;
    }

    public String getTo_house() {
        return to_house;
    }

    public void setTo_house(String to_house) {
        this.to_house = to_house;
    }
}
