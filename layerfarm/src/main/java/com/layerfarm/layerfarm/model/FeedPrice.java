package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedPrice {
    @SerializedName("item_nid")
    @Expose
    private String itemNid;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("effective_date")
    @Expose
    private String effective_date;



    public FeedPrice(){

    }

    public String getItemNid() {
        return itemNid;
    }

    public void setItemNid(String itemNid) {
        this.itemNid = itemNid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }
}
