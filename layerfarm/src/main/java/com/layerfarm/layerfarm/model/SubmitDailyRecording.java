package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class SubmitDailyRecording {
    @SerializedName("module")
    private String module;
    @SerializedName("function_name")
    private String function_name;
    @SerializedName("args")
    private DailyRecording args;

    public SubmitDailyRecording(String module, String function_name, DailyRecording args){
//        Log.d("Layerfarm","args submit : "+args.getHouseId());
        this.module = module;
        this.function_name = function_name;
//        args.setHouseId("1882");
//        args.setRecordingDate("2019-07-19");
        this.args = args;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public DailyRecording getArgs() {
        return args;
    }

    public void setArgs(DailyRecording args) {
        this.args = args;
    }

}
