package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class Report implements Serializable{
    private static Report staticInstance;

    public static Report getInstance(){
        if (staticInstance==null){
            staticInstance = new Report();
        }
        return staticInstance;
    }
    @SerializedName("age")
    HashMap<String, Age> age = new HashMap<>();
    @SerializedName("population")
    Population population = new Population();
    @SerializedName("treatment")
    Treatment treatment = new Treatment();
    @SerializedName("production")
    Production production = new Production();

    public HashMap<String, Age> getAge() {
        return age;
    }

    public void setAge(HashMap<String, Age> age) {
        this.age = age;
    }

    public Population getPopulation() {
        return population;
    }

    public void setPopulation(Population population) {
        this.population = population;
    }

    public Treatment getTreatment() {
        return treatment;
    }

    public void setTreatment(Treatment treatment) {
        this.treatment = treatment;
    }

    public Production getProduction() {
        return production;
    }

    public void setProduction(Production production) {
        this.production = production;
    }

    public class Population implements Serializable{
        @SerializedName("chick_in")
        String chick_in;
        @SerializedName("transfer_in")
        String transfer_in;
        @SerializedName("transfer_out")
        String transfer_out;
        @SerializedName("mortality")
        String mortality;
        @SerializedName("chick_out")
        String chick_out;
        @SerializedName("total_bird")
        String total_bird;

        public String getChick_in() {
            return chick_in;
        }

        public void setChick_in(String chick_in) {
            this.chick_in = chick_in;
        }

        public String getTransfer_in() {
            return transfer_in;
        }

        public void setTransfer_in(String transfer_in) {
            this.transfer_in = transfer_in;
        }

        public String getTransfer_out() {
            return transfer_out;
        }

        public void setTransfer_out(String transfer_out) {
            this.transfer_out = transfer_out;
        }

        public String getMortality() {
            return mortality;
        }

        public void setMortality(String mortality) {
            this.mortality = mortality;
        }

        public String getChick_out() {
            return chick_out;
        }

        public void setChick_out(String chick_out) {
            this.chick_out = chick_out;
        }

        public String getTotal_bird() {
            return total_bird;
        }

        public void setTotal_bird(String total_bird) {
            this.total_bird = total_bird;
        }
    }
    public class Treatment implements Serializable{
        @SerializedName("medivac")
        String medivac;
        @SerializedName("bw")
        String bw;
        @SerializedName("feed")
        String feed;
        @SerializedName("total_feed")
        String total_feed;
        @SerializedName("feed_intake")
        String feed_intake;

        public String getMedivac() {
            return medivac;
        }

        public void setMedivac(String medivac) {
            this.medivac = medivac;
        }

        public String getBw() {
            return bw;
        }

        public void setBw(String bw) {
            this.bw = bw;
        }

        public String getFeed() {
            return feed;
        }

        public void setFeed(String feed) {
            this.feed = feed;
        }

        public String getTotal_feed() {
            return total_feed;
        }

        public void setTotal_feed(String total_feed) {
            this.total_feed = total_feed;
        }

        public String getFeed_intake() {
            return feed_intake;
        }

        public void setFeed_intake(String feed_intake) {
            this.feed_intake = feed_intake;
        }
    }
    public class Production implements Serializable{
        @SerializedName("egg")
        ArrayList<Egg> egg = new ArrayList<>();
        @SerializedName("total_egg")
        String total_egg;
        @SerializedName("weight")
        ArrayList<Egg> weight = new ArrayList<>();
        @SerializedName("total_weight")
        String total_weight;
        @SerializedName("henday")
        String henday;
        @SerializedName("fcr")
        String fcr;
        @SerializedName("henhoused")
        String henhoused;
        @SerializedName("egg_weight")
        String egg_weight;

        public ArrayList<Egg> getEgg() {
            return egg;
        }

        public void setEgg(ArrayList<Egg> egg) {
            this.egg = egg;
        }

        public String getTotal_egg() {
            return total_egg;
        }

        public void setTotal_egg(String total_egg) {
            this.total_egg = total_egg;
        }

        public ArrayList<Egg> getWeight() {
            return weight;
        }

        public void setWeight(ArrayList<Egg> weight) {
            this.weight = weight;
        }

        public String getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(String total_weight) {
            this.total_weight = total_weight;
        }

        public String getHenday() {
            return henday;
        }

        public void setHenday(String henday) {
            this.henday = henday;
        }

        public String getFcr() {
            return fcr;
        }

        public void setFcr(String fcr) {
            this.fcr = fcr;
        }

        public String getHenhoused() {
            return henhoused;
        }

        public void setHenhoused(String henhoused) {
            this.henhoused = henhoused;
        }

        public String getEgg_weight() {
            return egg_weight;
        }

        public void setEgg_weight(String egg_weight) {
            this.egg_weight = egg_weight;
        }
    }
    public class Egg implements Serializable{
        @SerializedName("name")
        String name;
        @SerializedName("display_name")
        String display_name;
        @SerializedName("qty")
        String qty;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDisplay_name() {
            return display_name;
        }

        public void setDisplay_name(String display_name) {
            this.display_name = display_name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }
    }
    public class Age implements Serializable{
        @SerializedName("hatching_id")
        String hatching_id;
        @SerializedName("hatch_date")
        String hatch_date;
        @SerializedName("age")
        String age;
        @SerializedName("total_bird")
        String total_bird;
        @SerializedName("chick_in")
        String chick_in;
        @SerializedName("transfer_in")
        String transfer_in;
        @SerializedName("transfer_out")
        String transfer_out;
        @SerializedName("mortality")
        String mortality;
        @SerializedName("mortality_total_percent")
        String mortality_total_percent;
        @SerializedName("mortality1")
        Mortality mortality1;
        @SerializedName("mortality2")
        Mortality mortality2;
        @SerializedName("mortality3")
        Mortality mortality3;
        @SerializedName("mortality4")
        Mortality mortality4;
        @SerializedName("mortality5")
        Mortality mortality5;
        @SerializedName("chick_out")
        Mortality chick_out;

        public String getHatching_id() {
            return hatching_id;
        }

        public void setHatching_id(String hatching_id) {
            this.hatching_id = hatching_id;
        }

        public String getHatch_date() {
            return hatch_date;
        }

        public void setHatch_date(String hatch_date) {
            this.hatch_date = hatch_date;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getTotal_bird() {
            return total_bird;
        }

        public void setTotal_bird(String total_bird) {
            this.total_bird = total_bird;
        }

        public String getChick_in() {
            return chick_in;
        }

        public void setChick_in(String chick_in) {
            this.chick_in = chick_in;
        }

        public String getTransfer_in() {
            return transfer_in;
        }

        public void setTransfer_in(String transfer_in) {
            this.transfer_in = transfer_in;
        }

        public String getTransfer_out() {
            return transfer_out;
        }

        public void setTransfer_out(String transfer_out) {
            this.transfer_out = transfer_out;
        }

        public String getMortality() {
            return mortality;
        }

        public void setMortality(String mortality) {
            this.mortality = mortality;
        }

        public String getMortality_total_percent() {
            return mortality_total_percent;
        }

        public void setMortality_total_percent(String mortality_total_percent) {
            this.mortality_total_percent = mortality_total_percent;
        }

        public Mortality getMortality1() {
            return mortality1;
        }

        public void setMortality1(Mortality mortality1) {
            this.mortality1 = mortality1;
        }

        public Mortality getMortality2() {
            return mortality2;
        }

        public void setMortality2(Mortality mortality2) {
            this.mortality2 = mortality2;
        }

        public Mortality getMortality3() {
            return mortality3;
        }

        public void setMortality3(Mortality mortality3) {
            this.mortality3 = mortality3;
        }

        public Mortality getMortality4() {
            return mortality4;
        }

        public void setMortality4(Mortality mortality4) {
            this.mortality4 = mortality4;
        }

        public Mortality getMortality5() {
            return mortality5;
        }

        public void setMortality5(Mortality mortality5) {
            this.mortality5 = mortality5;
        }

        public Mortality getChick_out() {
            return chick_out;
        }

        public void setChick_out(Mortality chick_out) {
            this.chick_out = chick_out;
        }
    }
    public class Mortality implements Serializable{
        @SerializedName("name")
        String name;
        @SerializedName("value")
        String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
