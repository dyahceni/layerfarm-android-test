package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class getFlockModel {

        @SerializedName("nid")
        private String nid;

        @SerializedName("name")
        private String name;

        @SerializedName("location_id")
        private String location_id;

        @SerializedName("type")
        private String type;

        @SerializedName("period")
        private String period;

        @SerializedName("status")
        private String status;

        @SerializedName("capacity")
        private String capacity;


        public String getNid() {
            return nid;
        }

        public void setNid(String nid) {
            this.nid = nid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPeriod() {
            return period;
        }

        public void setPeriod(String period) {
            this.period = period;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCapacity() {
            return capacity;
        }

        public void setCapacity(String capacity) {
            this.capacity = capacity;
        }

}
