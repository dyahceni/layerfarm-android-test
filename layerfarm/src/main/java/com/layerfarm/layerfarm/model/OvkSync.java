package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;

public class OvkSync {

    @SerializedName("id")
    String id;
    @SerializedName("product_name")
    String product_name;
    @SerializedName("vendor")
    String vendor;
    @SerializedName("capacity")
    String capacity;
    @SerializedName("unit")
    String unit;
    @SerializedName("type")
    String type;
    @SerializedName("status")
    String status;
    @SerializedName("rid")
    String rid;
    @SerializedName("delete_status")
    String delete_status;
    @SerializedName("vendor_list")
    LinkedHashMap<String, String> vendor_list;



//    public OvkSync(String product_name, String vendor, String capacity, String unit, String type, String status, String rid) {
//        this.product_name = product_name;
//        this.vendor = vendor;
//        this.capacity = capacity;
//        this.unit = unit;
//        this.type = type;
//        this.status = status;
//        this.rid = rid;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor_id(String vendor) {
        this.vendor = vendor;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDeleteStatus() {
        return delete_status;
    }

    public void setDeleteStatus(String delete_status) {
        this.delete_status = delete_status;
    }

    public LinkedHashMap<String, String> getVendor_list() {
        return vendor_list;
    }

    public void setVendor_list(LinkedHashMap<String, String> vendor_list) {
        this.vendor_list = vendor_list;
    }
}
