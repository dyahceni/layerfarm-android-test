
package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ovklist {

    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("item_nid")
    @Expose
    private String itemNid;
    @SerializedName("ovk_name")
    @Expose
    private String ovkName;
    @SerializedName("capacity_weight")
    @Expose
    private String capacityWeight;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("effective_date")
    @Expose
    private String effective_date;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getItemNid() {
        return itemNid;
    }

    public void setItemNid(String itemNid) {
        this.itemNid = itemNid;
    }

    public String getOvkName() {
        return ovkName;
    }

    public void setOvkName(String ovkName) {
        this.ovkName = ovkName;
    }

    public String getCapacityWeight() {
        return capacityWeight;
    }

    public void setCapacityWeight(String capacityWeight) {
        this.capacityWeight = capacityWeight;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }
}
