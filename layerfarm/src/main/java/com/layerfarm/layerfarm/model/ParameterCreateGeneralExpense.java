package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;


public class ParameterCreateGeneralExpense {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterCreateGeneralExpense(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;

        @SerializedName("date")
        private String b;

        @SerializedName("item")
        private String c;

        @SerializedName("cost")
        private String d;

        @SerializedName("scope")
        private String e;

        @SerializedName("location_nid")
        private String f;

        @SerializedName("vendor")
        private String g;


        public Arguments(String nid, String date, String item, String cost, String scope, String location_nid, String vendor){
            this.a = nid;
            this.b = date;
            this.c = item;
            this.d = cost;
            this.e = scope;
            this.f = location_nid;
            this.g = vendor;
        }
    }
}
