package com.layerfarm.layerfarm.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class UserMember {
    @SerializedName("house")
    private LinkedHashMap<Integer, String> house = new LinkedHashMap<>();
    @SerializedName("users")
    private ArrayList<Users> users = new ArrayList<>();
    @SerializedName("default_location")
    private String default_location = "";

    public LinkedHashMap<Integer, String> getHouse() {
        return house;
    }

    public void setHouse(LinkedHashMap<Integer, String> house) {
        this.house = house;
    }

    public ArrayList<Users> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<Users> users) {
        this.users = users;
    }

    public String getDefault_location() {
        return default_location;
    }

    public void setDefault_location(String default_location) {
        this.default_location = default_location;
    }

    public static class  Users{
        @SerializedName("uid")
        String uid;
        @SerializedName("name")
        String name;
        @SerializedName("pass")
        String pass;
        @SerializedName("mail")
        String mail;
        @SerializedName("status")
        String status;
        @SerializedName("language")
        String language;
        @SerializedName("roles")
        HashMap<Integer, String> roles;
        @SerializedName("field_managed_location")
        JsonElement fieldManagedLocation;


        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public HashMap<Integer, String> getRoles() {
            return roles;
        }

        public void setRoles(HashMap<Integer, String> roles) {
            this.roles = roles;
        }

        public JsonElement getFieldManagedLocation() {
            return fieldManagedLocation;
        }

        public void setFieldManagedLocation(JsonElement fieldManagedLocation) {
            this.fieldManagedLocation = fieldManagedLocation;
        }


    }
    public static class  FieldManagedLocation{
        @SerializedName("und")
        ArrayList<Und> und = new ArrayList<>();

        public ArrayList<Und> getUnd() {
            return und;
        }

        public void setUnd(ArrayList<Und> und) {
            this.und = und;
        }
    }
    public class Und{
        @SerializedName("target_id")
        String target_id;

        public String getTarget_id() {
            return target_id;
        }

        public void setTarget_id(String target_id) {
            this.target_id = target_id;
        }
    }
}
