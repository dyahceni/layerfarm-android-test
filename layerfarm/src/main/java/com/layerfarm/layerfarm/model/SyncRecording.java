package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SyncRecording {
    @SerializedName("module")
    private String module;
    @SerializedName("function_name")
    private String function_name;
    @SerializedName("args")
    private SyncRecordingData args;

    public SyncRecording(String module, String function_name, SyncRecordingData args){
//        Log.d("Layerfarm","args submit : "+args.getHouseId());
        this.module = module;
        this.function_name = function_name;
//        args.setHouseId("1882");
//        args.setRecordingDate("2019-07-19");
        this.args = args;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public SyncRecordingData getArgs() {
        return args;
    }

    public void setArgs(SyncRecordingData args) {
        this.args = args;
    }

    public static class SyncRecordingData{
        @SerializedName ("data")
        ArrayList<DailyRecording> a;

        public ArrayList<DailyRecording> getA() {
            return a;
        }

        public void setA(ArrayList<DailyRecording> a) {
            this.a = a;
        }
    }

}
