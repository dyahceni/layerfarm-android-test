package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class DailyRecordingBodyWeight {
    @SerializedName("recording_date")
    private String a;
    @SerializedName("house_id")
    private String b;
    @SerializedName("body_weight")
    private String[] c = {};

    public String getRecordingDate() {
        return a;
    }

    public void setRecordingDate(String a) {
        this.a = a;
    }

    public String getHouseId() {
        return b;
    }

    public void setHouseId(String b) {
        this.b = b;
    }

    public String[] getBodyWeight() {
        return c;
    }

    public void setBodyWeight(String[] c) {
        this.c = c;
    }
}
