package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class DailyRecordingFeed {
    @SerializedName("feed_id")
    private String feed_id;
    @SerializedName("amount")
    private String amount;

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
