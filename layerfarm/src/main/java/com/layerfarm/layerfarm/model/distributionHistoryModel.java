package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class distributionHistoryModel {

    String id;
    String flock_name;
    String type;
    String number_of_bird;
    String recording_date;
    public distributionHistoryModel(){

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlock_name() {
        return flock_name;
    }

    public void setFlock_name(String flock_name) {
        this.flock_name = flock_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber_of_bird() {
        return number_of_bird;
    }

    public void setNumber_of_bird(String number_of_bird) {
        this.number_of_bird = number_of_bird;
    }

    public String getRecording_date() {
        return recording_date;
    }

    public void setRecording_date(String recording_date) {
        this.recording_date = recording_date;
    }

}
