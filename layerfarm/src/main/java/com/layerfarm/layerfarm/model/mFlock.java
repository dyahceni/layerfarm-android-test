package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class mFlock {

    @SerializedName("nid")
    private String nid;

    @SerializedName("name")
    private String name;

    @SerializedName("location_id")
    private String location_id;

    @SerializedName("location_name")
    private String location_name;

    @SerializedName("type")
    private String type;

    @SerializedName("period")
    private String period;

    @SerializedName("status")
    private String status;

    @SerializedName("capacity")
    private String capacity;

    @SerializedName("effective_date")
    private String effective_date;

    @SerializedName("delete_status")
    private String delete;

//
//    public mFlock(String nid, String name, String location_id, String location_name, String type, String period, String status, String capacity, String delete){
//        this.nid = nid;
//        this.name = name;
//        this.location_id = location_id;
//        this.location_name = location_name;
//        this.type = type;
//        this.period = period;
//        this.status = status;
//        this.capacity = capacity;
//        this.delete = delete;
//    }

    public String getNid() {
        return nid;
    }
    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation_id() {
        return location_id;
    }
    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getCapacity() {
        return capacity;
    }
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }
}
