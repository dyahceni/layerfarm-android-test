package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;


public class ParameterCreateLocation {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterCreateLocation(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;

        @SerializedName("farm_name")
        private String b;

        @SerializedName("farm_address")
        private String c;

        @SerializedName("farm_country")
        private String d;



        public Arguments(String nid, String farm_name, String farm_address, String farm_country){
            this.a = nid;
            this.b = farm_name;
            this.c = farm_address;
            this.d = farm_country;
        }
    }
}
