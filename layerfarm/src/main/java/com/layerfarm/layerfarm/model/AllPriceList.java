package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllPriceList {

    private static AllPriceList staticInstance;

    public static AllPriceList getInstance(){
        if (staticInstance==null){
            staticInstance = new AllPriceList();
        }
        return staticInstance;
    }

    @SerializedName("ovklist")
    @Expose
    private ArrayList<Ovklist> ovklist = null;
    @SerializedName("feedlist")
    @Expose
    private ArrayList<Feedlist> feedlist = null;
    @SerializedName("hatchinglist")
    @Expose
    private ArrayList<Hatchinglist> hatchinglist = null;

    public ArrayList<Ovklist> getOvklist() {
        return ovklist;
    }

    public void setOvklist(ArrayList<Ovklist> ovklist) {
        this.ovklist = ovklist;
    }

    public ArrayList<Feedlist> getFeedlist() {
        return feedlist;
    }

    public void setFeedlist(ArrayList<Feedlist> feedlist) {
        this.feedlist = feedlist;
    }

    public ArrayList<Hatchinglist> getHatchinglist() {
        return hatchinglist;
    }

    public void setHatchinglist(ArrayList<Hatchinglist> hatchinglist) {
        this.hatchinglist = hatchinglist;
    }
}


