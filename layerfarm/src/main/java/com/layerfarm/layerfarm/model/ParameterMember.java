package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ParameterMember {
    @SerializedName("module")
    private String module;
    @SerializedName("function_name")
    private String function_name;
    @SerializedName("args")
    private Users args;

    public ParameterMember(String module, String function_name, Users args){
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public Users getArgs() {
        return args;
    }

    public void setArgs(Users args) {
        this.args = args;
    }

    public static class  Users implements Serializable {
        @SerializedName("uid")
        String a = "";
        @SerializedName("name")
        String b = "";
        @SerializedName("pass")
        String c = "";
        @SerializedName("mail")
        String d ="";
        @SerializedName("status")
        String e ="";
        @SerializedName("role")
        ArrayList<String> f = new ArrayList<>();
        @SerializedName("location")
        ArrayList<String> g = new ArrayList<>();
        @SerializedName("language")
        String h = "";


        public String getUid() {
            return a;
        }

        public void setUid(String uid) {
            this.a = uid;
        }

        public String getName() {
            return b;
        }

        public void setName(String name) {
            this.b = name;
        }

        public String getPass() {
            return c;
        }

        public void setPass(String pass) {
            this.c = pass;
        }

        public String getMail() {
            return d;
        }

        public void setMail(String mail) {
            this.d = mail;
        }

        public String getStatus() {
            return e;
        }

        public void setStatus(String status) {
            this.e = status;
        }


        public ArrayList<String> getRoles() {
            return f;
        }

        public void setRoles(ArrayList<String> roles) {
            this.f = roles;
        }

        public ArrayList<String> getLocation() {
            return g;
        }

        public void setLocation(ArrayList<String> g) {
            this.g = g;
        }

        public String getLanguage(){
            return h;
        }
        public void setLanguage(String h){
            this.h = h;
        }
    }
}
