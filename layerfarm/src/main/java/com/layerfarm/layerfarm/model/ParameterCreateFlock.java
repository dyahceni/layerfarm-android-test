package com.layerfarm.layerfarm.model;
import com.google.gson.annotations.SerializedName;

public class ParameterCreateFlock {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterCreateFlock(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("nid")
        private String a;

        @SerializedName("house_name")
        private String b;

        @SerializedName("farm_nid")
        private String c;

        @SerializedName("house_type")
        private String d;

        @SerializedName("house_period")
        private String e;

        @SerializedName("house_status")
        private String f;

        @SerializedName("house_capacity")
        private String g;

        @SerializedName("effective_date")
        private String h;

        public Arguments(String nid, String house_name, String farm_nid, String house_type, String house_period, String house_status, String house_capacity, String effective_date){
            this.a = nid;
            this.b = house_name;
            this.c = farm_nid;
            this.d = house_type;
            this.e = house_period;
            this.f = house_status;
            this.g = house_capacity;
            this.h = effective_date;
        }
    }
}
