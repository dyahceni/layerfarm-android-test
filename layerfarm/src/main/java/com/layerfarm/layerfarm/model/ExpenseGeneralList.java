package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ExpenseGeneralList {

    @SerializedName("expense_general_list")
    @Expose
    private ArrayList<ExpenseGeneral> expenseGeneralList = null;

    public ArrayList<ExpenseGeneral> getExpenseGeneralList() {
        return expenseGeneralList;
    }

    public void setExpenseGeneralList(ArrayList<ExpenseGeneral> expenseGeneralList) {
        this.expenseGeneralList = expenseGeneralList;
    }
}
