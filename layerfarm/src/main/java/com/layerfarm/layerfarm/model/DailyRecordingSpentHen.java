package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class DailyRecordingSpentHen {
    @SerializedName("hatch_date_move")
    private int hatch_date_move;
    @SerializedName("jumlah_afkir")
    private String jumlah_afkir;
    @SerializedName("afkir_weight")
    private String afkir_weight;

    public int getHatch_date_move() {
        return hatch_date_move;
    }

    public void setHatch_date_move(int hatch_date_move) {
        this.hatch_date_move = hatch_date_move;
    }

    public String getJumlah_afkir() {
        return jumlah_afkir;
    }

    public void setJumlah_afkir(String jumlah_afkir) {
        this.jumlah_afkir = jumlah_afkir;
    }

    public String getAfkir_weight() {
        return afkir_weight;
    }

    public void setAfkir_weight(String afkir_weight) {
        this.afkir_weight = afkir_weight;
    }
}
