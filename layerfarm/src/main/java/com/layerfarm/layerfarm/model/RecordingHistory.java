package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class RecordingHistory {
    @SerializedName("header")
    ArrayList<String> header = new ArrayList<>();
    @SerializedName("rows")
    LinkedHashMap<String, ArrayList<String>> rows = new LinkedHashMap();

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public LinkedHashMap<String, ArrayList<String>> getRows() {
        return rows;
    }

    public void setRows(LinkedHashMap<String, ArrayList<String>> rows) {
        this.rows = rows;
    }
}
