package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

public class Ovk {
    @SerializedName("nid")
    private String nid;
    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;
    @SerializedName("uom")
    private String uom;
    @SerializedName("vendor")
    private String vendor;
    public Ovk(String nid, String name, String type, String uom, String vendor){
        this.nid = nid;
        this.name = name;
        this.type = type;
        this.uom = uom;
        this.vendor = vendor;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
}
