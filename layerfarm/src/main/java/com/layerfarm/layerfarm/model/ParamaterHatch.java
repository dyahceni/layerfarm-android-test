package com.layerfarm.layerfarm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ParamaterHatch {
    private String module;
    private String function_name;
    private Arguments args;

    public ParamaterHatch(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }
    public static class Arguments{
        @SerializedName("nid")
        private String a;

        @SerializedName("hatch_date")
        private String b;

        @SerializedName("strain_nid")
        private String c;

        @SerializedName("distribution")
        private ArrayList<distribution> d;

        @SerializedName("created")
        private String e;

        public Arguments(String nid, String hatch_date, String strain_nid, ArrayList<distribution> distribution, String created){
            this.a = nid;
            this.b = hatch_date;
            this.c = strain_nid;
            this.d = distribution;
            this.e = created;
        }
    }
    public static class distribution{
        @SerializedName("house_nid")
        private String house_nid;
        @SerializedName("number_of_birds")
        private String number_of_birds;
        @SerializedName("start_recording")
        private String start_recording;
//        public distribution(String house_nid, String number_of_birds, String start_recording){
//            this.house_nid = house_nid;
//            this.number_of_birds = number_of_birds;
//            this.start_recording = start_recording;
//        }

        public String getHouse_nid() {
            return house_nid;
        }

        public void setHouse_nid(String house_nid) {
            this.house_nid = house_nid;
        }

        public String getNumber_of_birds() {
            return number_of_birds;
        }

        public void setNumber_of_birds(String number_of_birds) {
            this.number_of_birds = number_of_birds;
        }

        public String getStart_recording() {
            return start_recording;
        }

        public void setStart_recording(String start_recording) {
            this.start_recording = start_recording;
        }
    }
}
