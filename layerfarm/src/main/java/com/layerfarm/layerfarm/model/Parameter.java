package com.layerfarm.layerfarm.model;

/**
 * Created by agungsuyono on 17/02/18.
 *
 */

public class Parameter {
    private String module;
    private String function_name;
    private String[] args;

    public Parameter(String module, String function_name, String[] args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }
}
