package com.layerfarm.layerfarm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper{
    //Declare Database
    private static final String LOGCAT = null;
    public static final String DATABASE_NAME = "layerfarm-mobile.db";
    //public static final String DATABASE_PATH = "/data/data/com.layerfarm.layerfarmmanager/databases/";
    private static final int DATABASE_VERSION = 1;

    // Name of table
    private static final String TABLE_VARIABLES= "variables";
    // All Keys used in table
    private static final String KEY_VARIABLES_NAME = "name";
    private static final String KEY_VARIABLES_VALUE = "value";

    // Name of table
    private static final String TABLE_BIRD_CALCULATION = "bird_calculation";
    // All Keys used in table
    private static final String KEY_BIRD_CALCULATION_ID = "id";
    private static final String KEY_BIRD_CALCULATION_RECORDING_DATE = "recording_date";
    private static final String KEY_BIRD_CALCULATION_REF_ID = "ref_id";
    private static final String KEY_BIRD_CALCULATION_TYPE = "type";
    private static final String KEY_BIRD_CALCULATION_CHICK_IN_ID = "chick_in_id";
    private static final String KEY_BIRD_CALCULATION_LOCATION_ID = "location_id";
    private static final String KEY_BIRD_CALCULATION_FLOCK_ID = "flock_id";
    private static final String KEY_BIRD_CALCULATION_FLOCK_PERIOD = "flock_period";
    private static final String KEY_BIRD_CALCULATION_CHICK_IN = "chick_in";
    private static final String KEY_BIRD_CALCULATION_TRANSFER_IN = "transfer_in";
    private static final String KEY_BIRD_CALCULATION_TRANSFER_OUT = "transfer_out";
    private static final String KEY_BIRD_CALCULATION_MORTALITY = "mortality";
    private static final String KEY_BIRD_CALCULATION_CHICK_OUT= "chick_out";
    private static final String KEY_BIRD_CALCULATION_ADJUSTMENT = "adjustment";
    private static final String KEY_BIRD_CALCULATION_TOTAL_BIRD = "total_bird";


    // Name of table
    private static final String TABLE_HATCH_DATE_TRACKER = "hatch_date_tracker";
    // All Keys used in table
    private static final String KEY_HATCH_DATE_TRACKER_ID = "id";
    private static final String KEY_HATCH_DATE_TRACKER_DATE = "date";
    private static final String KEY_HATCH_DATE_TRACKER_CHICK_IN_ID = "chick_in_id";
    private static final String KEY_HATCH_DATE_TRACKER_FLOCK_ID = "flock_id";
    private static final String KEY_HATCH_DATE_TRACKER_TYPE = "type";
    private static final String KEY_HATCH_DATE_TRACKER_REF_ID = "ref_id";

    // Name of table
    private static final String TABLE_NEXT_RECORDING_DATE = "next_recording_date";
    // All Keys used in table
    private static final String KEY_NEXT_RECORDING_DATE_ID = "id";
    private static final String KEY_NEXT_RECORDING_DATE_FLOCK_ID = "flock_id";
    private static final String KEY_NEXT_RECORDING_DATE_FLOCK_PERIOD = "flock_period";
    private static final String KEY_NEXT_RECORDING_DATE_NEXT_DATE = "next_date";
    private static final String KEY_NEXT_RECORDING_DATE_POPULATION = "population";
    private static final String KEY_NEXT_RECORDING_DATE_REF_ID = "ref_id";


    //Table medication Vaccination
    public static final String TABLE_MEDICATION_VACCINATION = "medication_vaccination";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_VENDOR_ID = "vendor_id";
    public static final String COLUMN_CAPACITY = "capacity";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_RID = "rid";

    //Table egg quality
    public static final String TABLE_EGG_QUALITY = "egg_quality";
    public static final String COLUMN_EGG_ID = "id";
    public static final String COLUMN_EGG_NAME = "name";
    public static final String COLUMN_EGG_MACHINE_NAME = "machine_name";

    //Table Photo
    public static final String COMMA_SEP = ",";
    public static final String TEXT_TYPE = " TEXT";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String NUMERIC_TYPE = " NUMERIC";
    public static final String LONG_TYPE = " LONG";

    public static final String TABLE_NAME_PHOTO = "recording_photo";
    public static final String COLUMN_ID_RECORDING_PHOTO = "recording_id";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATETIME = "datetime";
    public static final String COLUMN_DESCRIPTION = "description";
    //public static final String PRIMARY_KEY = "PRIMARY KEY (" + COLUMN_TITLE + "," + COLUMN_DATETIME + ")";
    public static final String PRIMARY_KEY = "PRIMARY KEY (" + COLUMN_DATETIME + ")";

    private static final String DELETE_TABLE_PHOTO = "DROP TABLE IF EXISTS " + TABLE_NAME_PHOTO;
    private static final String CREATE_TABLE_PHOTO = "CREATE TABLE " + TABLE_NAME_PHOTO + " (" +
            COLUMN_ID_RECORDING_PHOTO + LONG_TYPE + COMMA_SEP +
            COLUMN_PATH + TEXT_TYPE + COMMA_SEP +
            COLUMN_TITLE + TEXT_TYPE + COMMA_SEP +
            COLUMN_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
            COLUMN_DATETIME + NUMERIC_TYPE + COMMA_SEP +
            PRIMARY_KEY +
            " )";

    //Table Body Weight
    public static final String TABLE_BODY_WEIGHT = "body_weight";
    public static final String COLUMN_BODY_WEIGHT_ID = "id";
    public static final String COLUMN_DAILY_RECORDING_ID = "daily_recording_id";
    public static final String COLUMN_BODY_WEIGHT = "body_weight";

    public static final String locationSQL = "CREATE TABLE IF NOT EXISTS location(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, address TEXT, rid INTEGER);";
    public static final String flockSQL = "CREATE TABLE IF NOT EXISTS  flock(id INTEGER PRIMARY KEY, name TEXT, location_id INTEGER, type TEXT, period TEXT, status TEXT, capacity INTEGER, rid INTEGER, last_recording datetime);";
    public static final String feedSQL = "CREATE TABLE IF NOT EXISTS  feed(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, status TEXT, rid INTEGER);";
    public static final String vendorSQL = "CREATE TABLE IF NOT EXISTS  vendor(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, rid INTEGER);";
    public static final String mortality_categorySQL = "CREATE TABLE IF NOT EXISTS  mortality_category(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(45), machine_name VARCHAR(45));";
    public static final String strainSQL = "CREATE TABLE IF NOT EXISTS  strain(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, rid INTEGER);";
    public static final String buyerSQL = "CREATE TABLE IF NOT EXISTS  buyer(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, rid INTEGER, address TEXT);";
    public static final String farm_expired_dateSQL = "CREATE TABLE IF NOT EXISTS  farm_expired_date(id INTEGER PRIMARY KEY AUTOINCREMENT, short_name TEXT, expired datetime);";


    final String medication_vaccinationSQL = "CREATE TABLE IF NOT EXISTS " + TABLE_MEDICATION_VACCINATION + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_PRODUCT_NAME + " TEXT, " +
            COLUMN_VENDOR_ID + " INTEGER, " +
            COLUMN_CAPACITY + " INTEGER, " +
            COLUMN_UNIT + " TEXT, " +
            COLUMN_TYPE + " TEXT, " +
            COLUMN_STATUS + " TEXT, " +
            COLUMN_RID + " INTEGER" +
            " )";

    final String egg_qualitySQL = "CREATE TABLE IF NOT EXISTS " + TABLE_EGG_QUALITY + " (" +
            COLUMN_EGG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_EGG_NAME + " TEXT, " +
            COLUMN_EGG_MACHINE_NAME + " TEXT " +
            " )";


    final String body_weightSQL = "CREATE TABLE IF NOT EXISTS " + TABLE_BODY_WEIGHT + " (" +
            COLUMN_BODY_WEIGHT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_DAILY_RECORDING_ID + " INTEGER, " +
            COLUMN_BODY_WEIGHT + " VARCHAR(255)" +
            " )";


    public static final String chick_in_distributionSQL = "CREATE TABLE IF NOT EXISTS  chick_in_distribution(id INTEGER PRIMARY KEY AUTOINCREMENT, chick_in_id INTEGER, flock_id INTEGER, number_of_bird INTEGER, start_recording DATETIME);";
    public static final String chick_inSQL = "CREATE TABLE IF NOT EXISTS  chick_in(id INTEGER PRIMARY KEY AUTOINCREMENT, chick_in_type TEXT, hatch_date DATETIME, age_in_week INTEGER, age_in_days INTEGER, age_on_date TEXT, strains_id INTEGER, rid INTEGER);";
    public  static final String daily_recordingSQL = "CREATE TABLE IF NOT EXISTS daily_recording(id integer primary key, recording_date datetime, flock_id integer, light_intensity float, light_duration_start varchar(45), light_duration_end varchar(45), " +
            "cage_temperature_1 varchar(45), cage_temperature_2 varchar(45), cage_temperature_3 varchar(45), humidity_1 varchar(45), humidity_2 varchar(45), humidity_3 varchar(45), note varchar(45));";
    public static final String recording_productionSQL = "CREATE TABLE IF NOT EXISTS recording_production(id INTEGER PRIMARY KEY, daily_recording_id integer, egg_quality_id integer, egg_number varchar(45), egg_weight varchar(45));";
    public static final String recording_feed_consumptionSQL = "CREATE TABLE IF NOT EXISTS recording_feed_consumption(id INTEGER PRIMARY KEY, daily_recording_id integer, feed_id integer, feed_amount float);";
    public static final String recording_medication_vaccinationSQL = "CREATE TABLE IF NOT EXISTS recording_medication_vaccination(id INTEGER PRIMARY KEY, daily_recording_id integer, medication_vaccination_id integer, amount_used varchar(45));";
    public static final String recording_mortalitySQL = "CREATE TABLE IF NOT EXISTS recording_mortality(id INTEGER PRIMARY KEY, daily_recording_id integer, chick_in_id integer, mortality_category_id integer, number_of_bird integer);";
    public static final String bird_transferSQL = "CREATE TABLE IF NOT EXISTS bird_transfer(id INTEGER PRIMARY KEY, daily_recording_id integer, chick_in_id integer, to_flock_id integer, number_of_bird integer)";

    public static final String date_flock_emptySQL = "CREATE TABLE IF NOT EXISTS date_flock_empty(id INTEGER PRIMARY KEY, flock_id integer, date datetime)";
    public static final String daily_recording_chick_inSQL = "CREATE TABLE IF NOT EXISTS daily_recording_chick_in(id INTEGER PRIMARY KEY, daily_recording_id integer, chick_in_id integer)";
    public static final String recording_chick_out = "CREATE TABLE IF NOT EXISTS recording_chick_out(id INTEGER PRIMARY KEY, daily_recording_id integer, chick_in_id integer, number_of_bird integer, total_weight varchar)";
    public static final String cache_hatch_date = "CREATE TABLE IF NOT EXISTS cache_hatch_date(id INTEGER PRIMARY KEY, type varchar(45), flock_id integer, chick_in_id integer, from_flock_id integer, timestamp varchar(45), new varchar(45))";
    public static final String cache_total_bird = "CREATE TABLE IF NOT EXISTS cache_total_bird(id INTEGER PRIMARY KEY, daily_recording_id integer, chick_in_id integer, transfer_in integer, " +
            "transfer_out integer, mortality integer, chick_out integer, total_bird integer )";
    //public static final String recording_photo = "CREATE TABLE IF NOT EXISTS recording_photo(id INTEGER PRIMARY KEY, daily_recording_id integer, photo varchar(256), description varchar(45))";
    public static final String cache_mortality = "CREATE TABLE IF NOT EXISTS cache_mortality(id INTEGER PRIMARY KEY AUTOINCREMENT, daily_recording_id varchar(45), value varchar(256))";

    public static final String user_farm = "CREATE TABLE IF NOT EXISTS user_farm(id INTEGER PRIMARY KEY AUTOINCREMENT, email varchar(45), password varchar(45), full_name varchar(256), farm_code varchar(256), farm_name varchar(256), farm_address varchar(256),phone_number varchar(256) ,timezone varchar(256),role varchar(256),is_owner INT, currently_active INT, nid varchar(256))";

    public static final String user_role = "CREATE TABLE IF NOT EXISTS user_role(id INTEGER PRIMARY KEY AUTOINCREMENT, user_farm_id INT, role_id INT)";
    public static final String role = "CREATE TABLE IF NOT EXISTS role(id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(45), machine_name varchar(45))";
    public static final String menu = "CREATE TABLE IF NOT EXISTS menu(id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(45))";
    public static final String role_menu = "CREATE TABLE IF NOT EXISTS role_menu(id INTEGER PRIMARY KEY AUTOINCREMENT, role_id INT, menu_id INT)";
    //Table for purchasing
    String CREATE_PURCHASE_ORDER = "CREATE TABLE purchase_order ( " +
            " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "shop_name TEXT NOT NULL, " +
            "vendor_id TEXT NOT NULL, " +
            "delivered_to TEXT NOT NULL, " +
            "currency TEXT NOT NULL, " +
            "state TEXT NOT NULL, " +
            "invoice_status TEXT NOT NULL, " +
            "amount_untaxed NUMERIC, " +
            "amount_total NUMERIC, " +
            "bill_status TEXT NOT NULL, " +
            "order_date  DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%S','now','localtime')), " +
            "date_planned  DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%S','now','localtime')), " +
            "date_approve  DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%S','now','localtime')), " +
            "deleted BOOLEAN NOT NULL " +
            " );";

    String CREATE_PURCHASE_ORDER_LINE = "CREATE TABLE purchase_order_line ( " +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "shop_id INTEGER NOT NULL, " +
            "item_name TEXT NOT NULL, " +
            "item_id integer NOT NULL, " +
            "item_quantity NUMERIC NOT NULL, " +
            "item_price NUMERIC NOT NULL, " +
            "item_done BOOLEAN NOT NULL, " +
            "item_tax NUMERIC NOT NULL, " +
            "item_sub_total NUMERIC NOT NULL, " +
            "item_total NUMERIC NOT NULL, " +
            "deleted BOOLEAN NOT NULL, " +
            "item_unit TEXT, " +
            "state TEXT, " +
            "item_category TEXT, " +
            "create_date  DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%d %H:%M:%S','now','localtime')), " +
            "schedule_date  DATETIME, " +
            "FOREIGN KEY (shop_id) REFERENCES " +
            "purchase_order (_id) " +
            " );";


    public static final String product_categorySQL = "CREATE TABLE IF NOT EXISTS product_category(id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "parent_left integer,\n" +
            "parent_right integer,\n" +
            "create_uid integer,\n" +
            "name varchar NOT NULL,\n" +
            "write_uid integer,\n" +
            "parent_id integer,\n" +
            "write_date timestamp(6),\n" +
            "create_date timestamp(6),\n" +
            "type varchar,\n" +
            "removal_strategy_id integer)";

    public static final String product_productSQL = "CREATE TABLE IF NOT EXISTS product_product(id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "create_date timestamp(6),\n" +
            "weight numeric,\n" +
            "default_code varchar,\n" +
            "product_tmpl_id integer(4) NOT NULL,\n" +
            "message_last_post timestamp(6),\n" +
            "create_uid integer(4),\n" +
            "write_uid integer(4),\n" +
            "barcode varchar,\n" +
            "volume float(8),\n" +
            "write_date timestamp(6),\n" +
            "active bool)";

    public static final String product_templateSQL = "CREATE TABLE IF NOT EXISTS product_template(id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "warranty float,\n" +
            "list_price numeric,\n" +
            "weight numeric,\n" +
            "sequence integer,\n" +
            "color integer,\n" +
            "write_uid int4,\n" +
            "uom_id integer NOT NULL,\n" +
            "description_purchase text,\n" +
            "default_code varchar,\n" +
            "create_date timestamp(6),\n" +
            "create_uid int4,\n" +
            "sale_ok bool,\n" +
            "purchase_ok bool,\n" +
            "message_last_post timestamp(6),\n" +
            "company_id integer,\n" +
            "uom_po_id integer NOT NULL,\n" +
            "description_sale text,\n" +
            "description text,\n" +
            "volume float,\n" +
            "write_date timestamp(6),\n" +
            "active bool,\n" +
            "categ_id int4 NOT NULL,\n" +
            "name varchar NOT NULL,\n" +
            "rental bool,\n" +
            "type varchar NOT NULL,\n" +
            "tracking varchar NOT NULL,\n" +
            "location_id integer,\n" +
            "description_picking text,\n" +
            "sale_delay float,\n" +
            "warehouse_id integer,\n" +
            "produce_delay float,\n" +
            "sale_line_warn varchar NOT NULL,\n" +
            "track_service varchar,\n" +
            "sale_line_warn_msg text,\n" +
            "invoice_policy varchar,\n" +
            "expense_policy varchar,\n" +
            "purchase_line_warn_msg text,\n" +
            "purchase_method varchar,\n" +
            "purchase_line_warn varchar NOT NULL)";

    public static final String stock_moveSQL = "CREATE TABLE stock_move(\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "origin varchar,\n" +
            "create_date timestamp(6),\n" +
            "restrict_partner_id integer,\n" +
            "product_uom int4 NOT NULL,\n" +
            "price_unit float8,\n" +
            "product_uom_qty numeric NOT NULL,\n" +
            "procure_method varchar NOT NULL,\n" +
            "product_qty numeric,\n" +
            "partner_id int4,\n" +
            "priority varchar,\n" +
            "picking_type_id int4,\n" +
            "location_id int4 NOT NULL,\n" +
            "sequence int4,\n" +
            "company_id int4 NOT NULL,\n" +
            "note text,\n" +
            "state varchar,\n" +
            "ordered_qty numeric,\n" +
            "origin_returned_move_id int4,\n" +
            "product_packaging int4,\n" +
            "restrict_lot_id int4,\n" +
            "date_expected timestamp(6) NOT NULL,\n" +
            "procurement_id int4,\n" +
            "create_uid int4,\n" +
            "warehouse_id int4,\n" +
            "inventory_id int4,\n" +
            "partially_available bool,\n" +
            "propagate bool,\n" +
            "move_dest_id int4,\n" +
            "date timestamp(6) NOT NULL,\n" +
            "scrapped bool,\n" +
            "write_uid int4,\n" +
            "product_id int4 NOT NULL,\n" +
            "push_rule_id int4,\n" +
            "name varchar NOT NULL,\n" +
            "split_from int4,\n" +
            "rule_id int4,\n" +
            "location_dest_id int4 NOT NULL,\n" +
            "write_date timestamp(6),\n" +
            "group_id int4,\n" +
            "picking_id int4,\n" +
            "workorder_id int4,\n" +
            "consume_unbuild_id int4,\n" +
            "is_done bool,\n" +
            "unit_factor float8,\n" +
            "bom_line_id int4,\n" +
            "raw_material_production_id int4,\n" +
            "quantity_done_store numeric,\n" +
            "production_id int4,\n" +
            "operation_id int4,\n" +
            "unbuild_id int4,\n" +
            "to_refund_so bool,\n" +
            "purchase_line_id int4,\n" +
            "invoice_line_id int4)";

    public static final String stock_quantSQL ="CREATE TABLE stock_quant(\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "create_date timestamp(6),\n" +
            "qty float NOT NULL,\n" +
            "propagated_from_id integer,\n" +
            "package_id integer,\n" +
            "cost float,\n" +
            "lot_id integer,\n" +
            "location_id integer NOT NULL,\n" +
            "create_uid integer,\n" +
            "reservation_id integer,\n" +
            "company_id integer NOT NULL,\n" +
            "owner_id integer,\n" +
            "write_date timestamp(6),\n" +
            "write_uid integer,\n" +
            "product_id integer NOT NULL,\n" +
            "packaging_type_id integer,\n" +
            "negative_move_id integer,\n" +
            "in_date timestamp(6)\n" +
            ")";

    public static final String purchase_orderSQL ="CREATE TABLE purchase_order(\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "origin varchar,\n" +
            "create_date timestamp(6),\n" +
            "write_uid integer,\n" +
            "currency_id  integer NOT NULL,\n" +
            "date_order timestamp(6) NOT NULL,\n" +
            "partner_id integer NOT NULL,\n" +
            "dest_address_id integer,\n" +
            "create_uid integer,\n" +
            "amount_untaxed integer,\n" +
            "picking_type_id integer NOT NULL,\n" +
            "message_last_post timestamp(6),\n" +
            "company_id integer NOT NULL,\n" +
            "name varchar NOT NULL,\n" +
            "amount_tax numeric,\n" +
            "state varchar,\n" +
            "date_approve date,\n" +
            "incoterm_id integer,\n" +
            "payment_term_id integer,\n" +
            "write_date timestamp(6),\n" +
            "partner_ref varchar,\n" +
            "fiscal_position_id integer,\n" +
            "amount_total numeric,\n" +
            "invoice_status varchar,\n" +
            "date_planned timestamp(6),\n" +
            "notes text,\n" +
            "group_id integer\n" +
            ")";

    public static final String purchase_order_lineSQL ="CREATE TABLE purchase_order_line(\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "create_date timestamp(6),\n" +
            "product_uom integer NOT NULL,\n" +
            "price_unit numeric NOT NULL,\n" +
            "qty_invoiced numeric,\n" +
            "write_uid integer,\n" +
            "currency_id integer,\n" +
            "product_qty integer NOT NULL,\n" +
            "partner_id integer,\n" +
            "qty_received integer,\n" +
            "create_uid integer,\n" +
            "price_tax integer,\n" +
            "sequence integer,\n" +
            "company_id integer,\n" +
            "state varchar,\n" +
            "account_analytic_id integer,\n" +
            "order_id int4 NOT NULL,\n" +
            "price_subtotal numeric,\n" +
            "write_date timestamp(6),\n" +
            "product_id int4 NOT NULL,\n" +
            "price_total integer,\n" +
            "name text NOT NULL,\n" +
            "date_planned timestamp(6) NOT NULL\n" +
            ")";

    public static final String res_partnerSQL ="CREATE TABLE res_partner (\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "name varchar,\n" +
            "company_id  int4,\n" +
            "comment text,\n" +
            "website varchar,\n" +
            "create_date timestamp(6),\n" +
            "color int4,\n" +
            "active bool,\n" +
            "street varchar,\n" +
            "supplier bool,\n" +
            "city varchar,\n" +
            "display_name varchar,\n" +
            "zip varchar,\n" +
            "title int4,\n" +
            "country_id int4,\n" +
            "commercial_company_name varchar,\n" +
            "parent_id int4,\n" +
            "company_name varchar,\n" +
            "employee bool,\n" +
            "ref varchar,\n" +
            "email varchar,\n" +
            "is_company bool,\n" +
            "function varchar,\n" +
            "lang varchar,\n" +
            "fax varchar,\n" +
            "street2 varchar,\n" +
            "barcode varchar,\n" +
            "phone varchar,\n" +
            "write_date timestamp(6),\n" +
            "date date,\n" +
            "tz varchar,\n" +
            "write_uid int4,\n" +
            "customer bool,\n" +
            "create_uid int4,\n" +
            "credit_limit float8,\n" +
            "user_id int4,\n" +
            "mobile varchar,\n" +
            "type varchar,\n" +
            "partner_share bool,\n" +
            "vat varchar,\n" +
            "state_id int4,\n" +
            "commercial_partner_id int4,\n" +
            "notify_email varchar,\n" +
            "message_last_post timestamp(6),\n" +
            "opt_out bool,\n" +
            "message_bounce int4,\n" +
            "signup_type varchar,\n" +
            "signup_expiration timestamp(6),\n" +
            "signup_token varchar,\n" +
            "picking_warn_msg text,\n" +
            "picking_warn varchar,\n" +
            "team_id int4,\n" +
            "debit_limit numeric,\n" +
            "last_time_entries_checked timestamp(6),\n" +
            "invoice_warn_msg text,\n" +
            "invoice_warn varchar,\n" +
            "sale_warn varchar,\n" +
            "sale_warn_msg text,\n" +
            "purchase_warn varchar,\n" +
            "purchase_warn_msg text\n" +
            ")";
    public static final String stock_pickingSQL ="CREATE TABLE stock_picking (\n" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "origin varchar,\n" +
            "date_done timestamp(6),\n" +
            "write_uid int4,\n" +
            "recompute_pack_op bool,\n" +
            "launch_pack_operations bool,\n" +
            "location_id int4,\n" +
            "priority varchar,\n" +
            "picking_type_id int4,\n" +
            "partner_id int4,\n" +
            "move_type varchar,\n" +
            "message_last_post timestamp(6),\n" +
            "company_id int4 ,\n" +
            "note text,\n" +
            "state varchar,\n" +
            "owner_id int4,\n" +
            "backorder_id int4,\n" +
            "create_uid int4,\n" +
            "min_date timestamp(6),\n" +
            "printed bool,\n" +
            "write_date timestamp(6),\n" +
            "date timestamp(6),\n" +
            "group_id int4,\n" +
            "name TEXT,\n" +
            "create_date timestamp(6),\n" +
            "location_dest_id int4 NOT NULL,\n" +
            "max_date timestamp(6),\n" +
            "invoice_id int4,\n" +
            "done_summary numeric \n" +
            ")";


    public static final String stock_warehouseSQL ="CREATE TABLE stock_warehouse (\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "code varchar(5),\n"+
            "create_date  timestamp(6),\n"+
            "lot_stock_id int4,\n"+
            "wh_pack_stock_loc_id int4,\n"+
            "reception_route_id int4,\n"+
            "pick_type_id int4,\n"+
            "crossdock_route_id int4,\n"+
            "partner_id int4,\n"+
            "create_uid int4,\n"+
            "delivery_route_id int4,\n"+
            "wh_input_stock_loc_id int4,\n"+
            "company_id int4,\n"+
            "reception_steps varchar,\n"+
            "delivery_steps varchar,\n"+
            "view_location_id int4,\n"+
            "wh_qc_stock_loc_id int4,\n"+
            "default_resupply_wh_id int4,\n"+
            "pack_type_id int4,\n"+
            "wh_output_stock_loc_id int4,\n"+
            "write_uid int4,\n"+
            "write_date timestamp(6),\n"+
            "active bool,\n"+
            "mto_pull_id int4,\n"+
            "name varchar,\n"+
            "in_type_id int4,\n"+
            "out_type_id int4,\n"+
            "int_type_id int4,\n"+
            "buy_pull_id int4,\n"+
            "buy_to_resupply bool \n" +
            ")";

    public static final String stock_locationSQL ="CREATE TABLE stock_location (\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "parent_left int4,\n" +
            "parent_right int4,\n" +
            "comment text,\n" +
            "putaway_strategy_id int4,\n" +
            "create_date timestamp(6),\n" +
            "write_date timestamp(6),\n" +
            "write_uid int4,\n" +
            "partner_id int4,\n" +
            "removal_strategy_id int4,\n" +
            "scrap_location bool,\n" +
            "location_id int4,\n" +
            "company_id int4,\n" +
            "complete_name varchar,\n" +
            "usage varchar NOT NULL,\n" +
            "create_uid int4,\n" +
            "barcode varchar,\n" +
            "posz int4,\n" +
            "posx int4,\n" +
            "posy int4,\n" +
            "active bool,\n" +
            "name varchar,\n" +
            "return_location bool,\n" +
            "valuation_in_account_id int4,\n" +
            "valuation_out_account_id int4,\n" +
            "warehouse_id int4\n" +
            ")";

    public static final String stock_picking_typeSQL ="CREATE TABLE stock_picking_type (\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "code varchar NOT NULL,\n" +
            "create_date timestamp(6),\n" +
            "sequence int4,\n" +
            "color int4,\n" +
            "write_uid int4,\n" +
            "use_create_lots bool,\n" +
            "create_uid int4,\n" +
            "default_location_dest_id int4,\n" +
            "show_entire_packs bool,\n" +
            "barcode_nomenclature_id int4,\n" +
            "use_existing_lots bool,\n" +
            "warehouse_id int4,\n" +
            "sequence_id int4,\n" +
            "write_date timestamp(6),\n" +
            "active bool,\n" +
            "name varchar NOT NULL,\n" +
            "return_picking_type_id int4,\n" +
            "default_location_src_id int4\n" +
            ")";


    //untuk mortality (sale)
    public static final String sale_order = "CREATE TABLE IF NOT EXISTS sale_order (id INTEGER PRIMARY KEY AUTOINCREMENT, origin varchar, create_date datetime, write_uid integer, team_id integer, client_order_ref varchar, date_order datetime, partner_id integer, note text, procurement_grpup_id integer, amount_untaxed numeric, message_last_post datetime," +
            "company_id integer, amount_tax numeric, state varchar, pricelist_id integer, project_id integer, create_uid integer, confirmation_date datetime, validity_date datetime, payment_term_id integer, write_date datetime, partner_invoice_id integer, fiscal_position_id integer, amount_total numeric, invoice_status varchar, name integer," +
            "partner_shipping_id integer, user_id integer, picking_policy character, incoterm integer, warehouse_id integer, effective_date date, requested_date datetime, commitment_date datetime)";
    public static final String sale_order_line = "CREATE TABLE IF NOT EXISTS sale_order_line (id INTEGER PRIMARY KEY AUTOINCREMENT, create_date datetime, product_uom integer , price_unit numeric, product_uom_qty numeric, price_subtotal numeric, write_uid integer, currency_id integer," +
            " price_reduce_taxexcl numeric, create_uid integer, price_tax numeric, qty_to_invoice numeric, customer_lead double, layout_category_sequence integer, company_id integer, state character, order_partner_id integer, order_id integer NOT NULL, qty_invoiced numeric," +
            " sequence integer, discount numeric, write_date datetime, price_reduce numeric, qty_delivered numeric, layout_category_id integer, product_id integer, price_reduce_taxinc numeric, price_total numeric, invoice_status character, name text , salesman_id integer," +
            " product_packaging integer, route_id integer)";

    public static  final String stock_pack_operation = "CREATE TABLE IF NOT EXISTS stock_pack_operation (id INTEGER PRIMARY KEY AUTOINCREMENT, create_date datetime, result_package_id integer, write_uid integer, package_id integer, product_qty numeric, location_id integer, create_uid integer, " +
            "ordered_qty numeric, qty_done numeric, owner_id integer, fresh_record boolean, write_date datetime, date datetime, product_id integer, product_uom_id integer, location_dest_id integer, picking_id integer)";


    private static final String CREATE_TABLE_BIRD_CALCULATION = "CREATE TABLE IF NOT EXISTS " + TABLE_BIRD_CALCULATION + " (" +
            KEY_BIRD_CALCULATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_BIRD_CALCULATION_RECORDING_DATE + " VARCHAR(45), " +
            KEY_BIRD_CALCULATION_REF_ID + " INTEGER, " +
            KEY_BIRD_CALCULATION_TYPE + " VARCHAR(45), " +
            KEY_BIRD_CALCULATION_CHICK_IN_ID + " INTEGER, " +
            KEY_BIRD_CALCULATION_LOCATION_ID + " INTEGER, " +
            KEY_BIRD_CALCULATION_FLOCK_ID + " INTEGER, " +
            KEY_BIRD_CALCULATION_FLOCK_PERIOD + " VARCHAR(45)," +
            KEY_BIRD_CALCULATION_CHICK_IN + " INTEGER, " +
            KEY_BIRD_CALCULATION_TRANSFER_IN + " INTEGER, " +
            KEY_BIRD_CALCULATION_TRANSFER_OUT + " INTEGER, " +
            KEY_BIRD_CALCULATION_MORTALITY + " INTEGER, " +
            KEY_BIRD_CALCULATION_CHICK_OUT + " INTEGER, " +
            KEY_BIRD_CALCULATION_ADJUSTMENT + " INTEGER," +
            KEY_BIRD_CALCULATION_TOTAL_BIRD + " INTEGER," +
            "FOREIGN KEY("+ KEY_BIRD_CALCULATION_LOCATION_ID +") REFERENCES location(id)," +
            "FOREIGN KEY("+ KEY_BIRD_CALCULATION_FLOCK_ID +") REFERENCES flock(id)" +
            ")";

    private static final String CREATE_TABLE_HATCH_DATE_TRACKER = "CREATE TABLE IF NOT EXISTS " + TABLE_HATCH_DATE_TRACKER + " (" +
            KEY_HATCH_DATE_TRACKER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_HATCH_DATE_TRACKER_DATE + " VARCHAR(45), " +
            KEY_HATCH_DATE_TRACKER_CHICK_IN_ID + " INTEGER, " +
            KEY_HATCH_DATE_TRACKER_FLOCK_ID + " INTEGER, " +
            KEY_HATCH_DATE_TRACKER_TYPE + " VARCHAR(45), " +
            KEY_HATCH_DATE_TRACKER_REF_ID + " INTEGER, " +
            "FOREIGN KEY("+ KEY_HATCH_DATE_TRACKER_FLOCK_ID +") REFERENCES flock(id)" +
            ")";

    private static final String CREATE_TABLE_NEXT_RECORDING_DATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NEXT_RECORDING_DATE + " (" +
            KEY_NEXT_RECORDING_DATE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_NEXT_RECORDING_DATE_FLOCK_ID + " INTEGER, " +
            KEY_NEXT_RECORDING_DATE_FLOCK_PERIOD + " VARCHAR(45), " +
            KEY_NEXT_RECORDING_DATE_NEXT_DATE + " VARCHAR(45), " +
            KEY_NEXT_RECORDING_DATE_POPULATION + "  INTEGER, " +
            KEY_NEXT_RECORDING_DATE_REF_ID + " INTEGER, " +
            "FOREIGN KEY("+ KEY_NEXT_RECORDING_DATE_FLOCK_ID +") REFERENCES flock(id)" +
            ")";

    private static final String CREATE_TABLE_VARIABLES = "CREATE TABLE IF NOT EXISTS " + TABLE_VARIABLES + " (" +
            KEY_VARIABLES_NAME + " VARCHAR(45), " +
            KEY_VARIABLES_VALUE+ " VARCHAR(45) " +
            ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(LOGCAT,"Created");
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create table
        db.execSQL(locationSQL);
        db.execSQL(flockSQL);
        db.execSQL(egg_qualitySQL);
        db.execSQL(feedSQL);
        db.execSQL(vendorSQL);
        db.execSQL(mortality_categorySQL);
        db.execSQL(strainSQL);
        db.execSQL(medication_vaccinationSQL);
        db.execSQL(chick_inSQL);
        db.execSQL(chick_in_distributionSQL);
        db.execSQL(body_weightSQL);
        db.execSQL(daily_recordingSQL);
        db.execSQL(recording_productionSQL);
        db.execSQL(recording_feed_consumptionSQL);
        db.execSQL(recording_medication_vaccinationSQL);
        db.execSQL(recording_mortalitySQL);
        db.execSQL(bird_transferSQL);
        db.execSQL(cache_hatch_date);
        db.execSQL(cache_total_bird);
        db.execSQL(daily_recording_chick_inSQL);
        db.execSQL(date_flock_emptySQL);
        db.execSQL(recording_chick_out);
        db.execSQL(CREATE_TABLE_PHOTO);
        db.execSQL(cache_mortality);
        db.execSQL(product_categorySQL);
        db.execSQL(product_productSQL);
        db.execSQL(product_templateSQL);
        db.execSQL(stock_moveSQL);
        db.execSQL(stock_quantSQL);
        db.execSQL(CREATE_PURCHASE_ORDER);
        db.execSQL(CREATE_PURCHASE_ORDER_LINE);
        db.execSQL(res_partnerSQL);
        db.execSQL(stock_pickingSQL);
        db.execSQL(stock_warehouseSQL);
        db.execSQL(stock_locationSQL);
        db.execSQL(stock_picking_typeSQL);
        db.execSQL(sale_order);
        db.execSQL(sale_order_line);
        db.execSQL(stock_pack_operation);
        db.execSQL(CREATE_TABLE_BIRD_CALCULATION);
        db.execSQL(CREATE_TABLE_HATCH_DATE_TRACKER);
        db.execSQL(CREATE_TABLE_NEXT_RECORDING_DATE);
        db.execSQL(CREATE_TABLE_VARIABLES);
        db.execSQL(buyerSQL);
        db.execSQL(user_farm);
        db.execSQL(user_role);
        db.execSQL(role);
        db.execSQL(menu);
        db.execSQL(role_menu);
        db.execSQL(farm_expired_dateSQL);

        String insertRole = "INSERT INTO role (name, machine_name) VALUES " +
                "('Administrator','administrator')," +
                "('Farm Manager Grower', 'farm manager grower')," +
                "('Farm Manager Layer', 'farm manager layer')," +
                "('Operator', 'operator')," +
                "('Purchasing', 'purchasing')," +
                "('Sales', 'sales')," +
                "('Nutritionist', 'nutritionist');";
        db.execSQL(insertRole);

        String insertMenu ="INSERT INTO menu (name) VALUES " +
                "('Dashboard')," +
                "('Grower')," +
                "('Layer')," +
                "('Analysis')," +
                "('Chick In')," +
                "('Recording')," +
                "('Sales')," +
                "('Expense')," +
                "('Location')," +
                "('Flock')," +
                "('Member')," +
                "('Strain')," +
                "('Feed')," +
                "('Medivac')," +
                "('Buyer')," +
                "('Vendor')," +
                "('Setting')," +
                "('Farm');";
        db.execSQL(insertMenu);

        String insertRoleMenu = "INSERT INTO role_menu (id, role_id, menu_id) VALUES " +
                " (1,1,1)," +
                " (2,1,2)," +
                " (3,1,3)," +
                " (4,1,4)," +
                " (5,1,5)," +
                " (6,1,6)," +
                " (7,1,7)," +
                " (8,1,8)," +
                " (9,1,9)," +
                " (10,1,10)," +
                " (11,1,11)," +
                " (12,1,12)," +
                " (13,1,13)," +
                " (14,1,14)," +
                " (15,1,15)," +
                " (16,1,16)," +
                " (17,1,17)," +
                " (18,1,18)," +
                " (19,2,2)," +
                " (20,2,4)," +
                " (21,2,5)," +
                " (22,2,6)," +
                " (23,2,10)," +
                " (24,2,13)," +
                " (25,2,14)," +
                " (26,2,18)," +
                " (27,3,3)," +
                " (28,3,4)," +
                " (29,3,5)," +
                " (30,3,6)," +
                " (31,3,10)," +
                " (32,3,13)," +
                " (33,3,14)," +
                " (34,3,18)," +
                " (35,4,4)," +
                " (36,4,5)," +
                " (37,4,6)," +
                " (38,4,18)," +
                " (39,5,8)," +
                " (40,5,16)," +
                " (41,5,18)," +
                " (42,6,7)," +
                " (43,6,15)," +
                " (44,6,18)," +
                " (45,7,2)," +
                " (46,7,3)," +
                " (47,7,4)," +
                " (48,7,18)," +
                " (49,8,1)," +
                " (50,8,2)," +
                " (51,8,3)," +
                " (52,8,18);";
        db.execSQL(insertRoleMenu);

        //add default data for offline mode
        String InsertlocationSQL = "INSERT INTO location (name, address, rid) VALUES " +
                "('Default Location','',0);";
        db.execSQL(InsertlocationSQL);

        String InsertflockSQL = "INSERT INTO flock (name, location_id, type, period, status, capacity, rid) VALUES " +
                "('Default Flock', 1, 'Cage Layer','Layer','Active',1000,0);";
        db.execSQL(InsertflockSQL);

        String InsertStrainSQL = "INSERT INTO strain (name) VALUES " +
                "('Hy-Line Brown');";
        db.execSQL(InsertStrainSQL);

        String InsertfeedSQL = "INSERT INTO feed (name, description, status, rid) VALUES "+
                "('Default Feed', 'Default Feed', 'Active',0);";
        db.execSQL(InsertfeedSQL);

        String medication_vaccinationSQL = "INSERT INTO medication_vaccination (product_name, vendor_id) VALUES "+
                "('Default Medivac','1');";
        db.execSQL(medication_vaccinationSQL);

        String InsertMortalityCategorySQL = "INSERT INTO mortality_category (name, machine_name) VALUES " +
                "('Death (Sick)', 'die_sick'), " +
                "('Death (Mechanic)', 'die_mecanic'), " +
                "('Death (Other)', 'die_others'), " +
                "('Culling (killed)', 'die_selection'), " +
                "('Culling (removed)', 'selection');" ;
//                "('Chick Out', 'jumlah_afkir');";
        db.execSQL(InsertMortalityCategorySQL);

        String InsertEggQualitySQL = "INSERT INTO egg_quality (name, machine_name) VALUES " +
                "('Quality A', 'normal'), " +
                "('Quality B', 'normal2'), " +
                "('Quality C', 'normal3'), " +
                "('Cracked', 'cracked');";
        db.execSQL(InsertEggQualitySQL);

        /*
        String InsertProductCategorySQL = "INSERT INTO product_category (parent_left, parent_right,create_uid, name, write_uid, parent_id, write_date, create_date, type, removal_strategy_id) VALUES " +
                "(null,null,1,'egg',1,null,DATETIME('now','localtime'),DATETIME('now','localtime'),'normal',null)," +
                "(null,null,1,'feed',1,null,DATETIME('now','localtime'),DATETIME('now','localtime'),'normal',null)," +
                "(null,null,1,'medication_vaccination',1,null,DATETIME('now','localtime'),DATETIME('now','localtime'),'normal',null)," +
                "(null,null,1,'strain',1,null,DATETIME('now','localtime'),DATETIME('now','localtime'),'normal',null);";
        db.execSQL(InsertProductCategorySQL);


        String InsertlocationSQL = "INSERT INTO location (name, address, rid) VALUES " +
                "('Location A','Blitar',1)," +
                "('Location B','Blitar',2)," +
                "('Location C','Blitar',3)," +
                "('Location D','Blitar',4);";
        db.execSQL(InsertlocationSQL);


        String InsertDataStocklocationSQL = "INSERT INTO stock_location (create_date, location_id, complete_name,usage) VALUES " +
                "(DATETIME('now','localtime'),null,'Physical Locations','view')," +
                "(DATETIME('now','localtime'),null,'Partner Locations','view')," +
                "(DATETIME('now','localtime'),null,'Virtual Locations','view')," +
                "(DATETIME('now','localtime'),3,'Virtual Locations/Scrapped','inventory')," +
                "(DATETIME('now','localtime'),3,'Virtual Locations/Inventory adjustment','inventory')," +
                "(DATETIME('now','localtime'),3,'Virtual Locations/Procurements','procurement')," +
                "(DATETIME('now','localtime'),3,'Virtual Locations/Production','production')," +
                "(DATETIME('now','localtime'),2,'Partner Locations/Vendors','supplier')," +
                "(DATETIME('now','localtime'),2,'Partner Locations/Customers','customer')," +
                "(DATETIME('now','localtime'),3,'Virtual Locations/Inter Company Transit','transit');";
        db.execSQL(InsertDataStocklocationSQL);

        String InsertEggQualitySQL = "INSERT INTO egg_quality (name) VALUES " +
                "('Quality A'), " +
                "('Quality B'), " +
                "('Quality C'), " +
                "('Cracked');";
        db.execSQL(InsertEggQualitySQL);


        String InsertflockSQL = "INSERT INTO flock (name, location_id, type, period, status, capacity, rid) VALUES " +
                "('Kandang 01', 1, 'Cage Grower','Grower','Active',2000,1)," +
                "('Kandang 02', 1, 'Cage Grower','Grower','Active',2000,2)," +
                "('Kandang 03', 2, 'Cage Grower','Grower','Active',2000,3)," +
                "('Kandang 04', 2, 'Cage Grower','Grower','Active',2000,4)," +
                "('Kandang 05', 2, 'Cage Grower','Grower','Active',2000,5)," +
                "('Kandang 06', 3, 'Cage Grower','Grower','Active',2000,6)," +
                "('Kandang 07', 3, 'Cage Grower','Grower','Active',2000,7)," +
                "('Kandang 08', 3, 'Cage Grower','Grower','Active',2000,8)," +
                "('Kandang 09', 3, 'Cage Grower','Grower','Active',2000,9);";
        db.execSQL(InsertflockSQL);

        String InsertVendorSQL = "INSERT INTO vendor (name, rid) VALUES " +
                "('Kalbe',1), " +
                "('Romindo',2);";
        db.execSQL(InsertVendorSQL);

        String InsertMortalityCategorySQL = "INSERT INTO mortality_category (name) VALUES " +
                "('Death (Sick)'), " +
                "('Death (Mechanic)'), " +
                "('Death (Other)'), " +
                "('Culling (killed)'), " +
                "('Culling (removed)'), " +
                "('Chick Out');";
        db.execSQL(InsertMortalityCategorySQL);

        String InsertStrainSQL = "INSERT INTO strain (name) VALUES " +
                "('Babcock Brown')," +
                "('Babcock White')," +
                "('Bovans Black')," +
                "('Bovans Brown')," +
                "('Bovans White')," +
                "('Dekalb Brown')," +
                "('Dekalb White')," +
                "('Hisex Brown')," +
                "('Hisex White')," +
                "('Hy-Line Brown')," +
                "('Hy-Line CV-22')," +
                "('Hy-Line Silver Brown')," +
                "('Hy-Line Sonia')," +
                "('Hy-Line W-36')," +
                "('ISA Brown')," +
                "('Isa White')," +
                "('Lohmann Brown Classic')," +
                "('Lohmann Brown Lite')," +
                "('Shaver Black')," +
                "('Shaver Brown')," +
                "('Shaver White');";
        db.execSQL(InsertStrainSQL);


        //buatan ceni
        String InsertfeedSQL = "INSERT INTO feed (id, name, description, status, rid) VALUES "+
                "(1, 'Katul', '', 'Active',1),"+
                "(2, 'Jagung', '', 'Active',2),"+
                "(3, 'Beras', '', 'Active',3),"+
                "(4, 'Dedak', '', 'Active',4);";
        db.execSQL(InsertfeedSQL);
        String medication_vaccinationSQL = "INSERT INTO medication_vaccination (id, product_name, vendor_id) VALUES "+
                "(1, 'Acitec', '1'),"+
                "(2, 'Kalvimix','2');";
        db.execSQL(medication_vaccinationSQL);

        String Insert_chick_inSQL = "INSERT INTO chick_in (id, chick_in_type, hatch_date, age_in_week, age_in_days, age_on_date, strains_id, rid) VALUES " +
                "(1,'Hatch Date','2017-10-10','','','',1,1)," +
                "(2,'Hatch Date','2017-10-11','','','',1,1)," +
                "(3,'Hatch Date','2017-10-12','','','',1,1);";
        db.execSQL(Insert_chick_inSQL);

        String Insert_chick_in_distributionSQL = "INSERT INTO chick_in_distribution (id, chick_in_id, flock_id, number_of_bird, start_recording) VALUES " +
                "(1,1,1,100,'2017-10-11')," +
                "(2,2,2,150,'2017-10-14')," +
                "(3,2,3,150,'2017-10-15')," +
                "(4,3,1,200,'2017-10-13');";
        db.execSQL(Insert_chick_in_distributionSQL);

        String Insert_cache_hatch_dateSQL = "INSERT INTO cache_hatch_date(id, type, flock_id, chick_in_id, timestamp, new) VALUES " +
                "(1, 'chick_in', 1, 1, '2017-10-11', '1')," +
                "(2, 'chick_in', 2, 2, '2017-10-14', '1')," +
                "(3, 'chick_in', 3, 2, '2017-10-15', '1')," +
                "(4, 'chick_in', 1, 3, '2017-10-13', '1');";
        db.execSQL(Insert_cache_hatch_dateSQL);

        String Insert_date_flock_emptySQL = "INSERT INTO date_flock_empty (id, flock_id, date) VALUES " +
                "(1,1,'2017-10-10')";
        db.execSQL(Insert_date_flock_emptySQL);
        */

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS location;");
        db.execSQL("DROP TABLE IF EXISTS flock;");
        db.execSQL("DROP TABLE IF EXISTS egg_quality;");
        db.execSQL("DROP TABLE IF EXISTS feed;");
        db.execSQL("DROP TABLE IF EXISTS vendor;");
        db.execSQL("DROP TABLE IF EXISTS mortality_category;");
        db.execSQL("DROP TABLE IF EXISTS strain;");
        db.execSQL("DROP TABLE IF EXISTS medication_vaccination;");
        db.execSQL("DROP TABLE IF EXISTS chick_in;");
        db.execSQL("DROP TABLE IF EXISTS chick_in_distribution;");
        db.execSQL("DROP TABLE IF EXISTS recording_photo;");
        db.execSQL("DROP TABLE IF EXISTS body_weight;");
        db.execSQL("DROP TABLE IF EXISTS daily_recording;");
        db.execSQL("DROP TABLE IF EXISTS recording_production;");
        db.execSQL("DROP TABLE IF EXISTS recording_feed_consumption");
        db.execSQL("DROP TABLE IF EXISTS recording_medication_vaccination");
        db.execSQL("DROP TABLE IF EXISTS recording_mortality");
        db.execSQL("DROP TABLE IF EXISTS bird_transfer");
        db.execSQL("DROP TABLE IF EXISTS date_flock_empty");
        db.execSQL("DROP TABLE IF EXISTS daily_recording_chick_in");
        db.execSQL("DROP TABLE IF EXISTS cache_hatch_date");
        db.execSQL("DROP TABLE IF EXISTS cache_mortality");
        db.execSQL("DROP TABLE IF EXISTS product_category");
        db.execSQL("DROP TABLE IF EXISTS product_product");
        db.execSQL("DROP TABLE IF EXISTS product_template");
        db.execSQL("DROP TABLE IF EXISTS stock_move");
        db.execSQL("DROP TABLE IF EXISTS stock_quant");
        db.execSQL("DROP TABLE IF EXISTS purchase_order");
        db.execSQL("DROP TABLE IF EXISTS purchase_order_line");
        db.execSQL("DROP TABLE IF EXISTS res_partner");
        db.execSQL("DROP TABLE IF EXISTS stock_picking");
        db.execSQL("DROP TABLE IF EXISTS stock_warehouse");
        db.execSQL("DROP TABLE IF EXISTS stock_location");
        db.execSQL("DROP TABLE IF EXISTS stock_picking_type");
        db.execSQL("DROP TABLE IF EXISTS sale_order");
        db.execSQL("DROP TABLE IF EXISTS sale_order_line");
        db.execSQL("DROP TABLE IF EXISTS stock_pack_operation");
        db.execSQL("DROP TABLE IF EXISTS variables");
        db.execSQL("DROP TABLE IF EXISTS buyer");
        db.execSQL("DROP TABLE IF EXISTS user_farm");
        db.execSQL("DROP TABLE IF EXISTS role");
        db.execSQL("DROP TABLE IF EXISTS menu");
        db.execSQL("DROP TABLE IF EXISTS user_role");
        db.execSQL("DROP TABLE IF EXISTS role_menu");
        db.execSQL("DROP TABLE IF EXISTS farm_expired_date");
        onCreate(db);
    }

}
