package com.layerfarm.layerfarm.service;

import com.google.gson.JsonElement;
import com.layerfarm.layerfarm.model.AllPriceList;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailModel;
import com.layerfarm.layerfarm.model.DashboardLayerDetailProductionModel;
import com.layerfarm.layerfarm.model.DashboardLayerFinancial;
import com.layerfarm.layerfarm.model.ExpenseGeneralList;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.FarmNid;
import com.layerfarm.layerfarm.model.FeedPrice;
import com.layerfarm.layerfarm.model.Feedlist;
import com.layerfarm.layerfarm.model.HatchingPrice;
import com.layerfarm.layerfarm.model.Hatchinglist;
import com.layerfarm.layerfarm.model.LayerDataDashboard;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.Node;
import com.layerfarm.layerfarm.model.OvkPrice;
import com.layerfarm.layerfarm.model.Ovklist;
import com.layerfarm.layerfarm.model.ParamaterActivateMember;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.ParamaterHatch;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.ParameterCreateFeed;
import com.layerfarm.layerfarm.model.ParameterCreateFlock;
import com.layerfarm.layerfarm.model.ParameterCreateGeneralExpense;
import com.layerfarm.layerfarm.model.ParameterCreateLocation;
import com.layerfarm.layerfarm.model.ParameterCreateSales;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.ParameterDeleteGeneralExpense;
import com.layerfarm.layerfarm.model.ParameterMember;
import com.layerfarm.layerfarm.model.ParameterRegistrationCreateLayerfarmUser;
import com.layerfarm.layerfarm.model.ParameterSetPriceList;
import com.layerfarm.layerfarm.model.PriceList;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RecordingHistory;
import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.layerfarm.model.ReportByLocation;
import com.layerfarm.layerfarm.model.Sales;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.model.SubmitDailyRecordingBw;
import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.layerfarm.model.SyncEggQuality;
import com.layerfarm.layerfarm.model.SyncFlock;
import com.layerfarm.layerfarm.model.SyncMortality;
import com.layerfarm.layerfarm.model.SyncRecording;
import com.layerfarm.layerfarm.model.UserMember;
import com.layerfarm.layerfarm.model.UserValidate;
import com.layerfarm.layerfarm.model.getFlockModel;
import com.layerfarm.layerfarm.model.getStrainModel;
import com.layerfarm.layerfarm.model.mFeed;
import com.layerfarm.layerfarm.model.FlockStatus;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.SyncOvk;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.StrainsSync;
import com.layerfarm.layerfarm.model.SyncVendor;
import com.layerfarm.layerfarm.model.mFlock;
import com.layerfarm.layerfarm.model.chickinHistory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by agungsuyono on 16/12/17.
 *
 * API Webservice Interface
 */

public interface ApiInterface {

    @GET("services/session/token")
    Call<String> getToken1();
//    Observable<Response<String>> getToken1();

    @Headers("Content-Type: application/json")
    @POST("common/system/connect.json")
    Call<Connect> getConnect(@Header("X-CSRF-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("common/user/login.json")
    Call<Connect> login(@Header("X-CSRF-Token") String token, @Body Login login);

    @GET("services/session/token")
    Call<String> getToken2(@HeaderMap Map<String, String> headers);

//    @GET("services/session/token")
//    Call<String> getToken2c(@Header("Cookie") String sessionIdAndToken);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<LoginStatus> getLoginStatus(@Header("X-CSRF-Token") String token, @Body ParameterAnon parameterAnon);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<UserValidate> getUserValidate(@Header("X-CSRF-Token") String token, @Body ParameterAnon parameterAnon);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> getSendResetPassword(@Header("X-CSRF-Token") String token, @Body ParameterAnon parameterAnon);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> callapi(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
//    Call<FlockStatus> getFlockStatus(@HeaderMap Map<String, String> headers, @Header("X-CSRF-Token") String token, @Body Parameter parameter);
    Call<FlockStatus> getFlockStatus(@Header("X-CSRF-Token") String token, @Body Parameter parameter);


//    // Login without token
//    @Headers("Content-Type: application/json")
//    @POST("common/user/login.json")
//    Call<Connect> loginNoToken(@Body Login login);
//
//    // Get Flock Status using Token Authentication
//    @Headers("Content-Type: application/json")
//    @POST("common/api/callapi.json")
//    Call<FlockStatus> getFlockStatusUsingAuthentication(@Header("Authentication") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<List<Farm>> getFarm(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<List<mFlock>> getFlock_1(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<SyncFlock> getFlock(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<List<mFeed>> getFeed(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    /*@Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<SyncOvk>> getOvk(@Header("X-CSRF-Token") String token, @Body Parameter parameter);*/

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<SyncOvk> getOvk(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<SyncVendor>> getVendor(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> deleteVendor(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> createVendor(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<StrainsSync>> getStrain(@Header("X-CSRF-Token") String token, @Body Parameter parameter);


    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<SyncMortality>> getMortality(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<SyncEggQuality>> getEggQuality(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<SyncBuyer>> getBuyer(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> deleteBuyer(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> createBuyer(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<Farm>> getFarmtest(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> layerfarm_create_rearing_by_hatch_date(@Header("X-CSRF-Token") String token, @Body ParamaterHatch parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> layerfarm_create_rearing_by_age(@Header("X-CSRF-Token") String token, @Body ParamaterAge parameter);

    //dyah
    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<chickinHistory>> getChickinHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<RecordingEntry> getRecordingEntry(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> submitDailyRecording(@Header("X-CSRF-Token") String token, @Body SubmitDailyRecording parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> submitDailyRecordingBw(@Header("X-CSRF-Token") String token, @Body SubmitDailyRecordingBw parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<RecordingHistory> RecordingHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardGrowerModel> DashboardGrower(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardGrowerModel.GrowerPerformanceDetail> DashboardGrowerDetail(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<Sales>> getSales(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> createSales(@Header("X-CSRF-Token") String token, @Body ParameterCreateSales parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> getFarmName(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> getFarmNid(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<UserMember> getMember(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<String>> createMember(@Header("X-CSRF-Token") String token, @Body ParameterMember parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<String>> updatePassword(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> deleteSale(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> createOVK(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> updateEggQuality(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
//    Call<LinkedHashMap<String, ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock>>> getFlockLocationSelection(@Header("X-CSRF-Token") String token, @Body Parameter parameter);
    Call<DashboardGrowerModel.GrowerPerformanceDetail> getFlockLocationSelection(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> syncRecording(@Header("X-CSRF-Token") String token, @Body SyncRecording parameter);


    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> activated_user(@Header("X-CSRF-Token") String token, @Body ParamaterActivateMember parameter);


    //mas nanang
    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ChickinEntry> get_chickin_data_entry(@Header("X-CSRF-Token") String token, @Body Parameter parameter);


    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<Node> layerfarm_registration_create_layerfarm_user(@Header("X-CSRF-Token") String token, @Body ParameterRegistrationCreateLayerfarmUser parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<List<String>> getSringChickinHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ResponseBody> getArrayChickinHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<HashMap<String, chickinHistory>> getMapChickinHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<LinkedHashMap<String, chickinHistory>> getLinkedMapChickinHistory(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<LayerDataDashboard> getDashboardLayerfarm(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> layerfarm_create_general_expense(@Header("X-CSRF-Token") String token, @Body ParameterCreateGeneralExpense parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ExpenseGeneralList> getGeneralExpenseLayerfarm(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> layerfarm_delete_general_expense(@Header("X-CSRF-Token") String token, @Body ParameterDeleteGeneralExpense parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<AllPriceList> getPriceListLayerfarm(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<Ovklist>> getPriceListOvk(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<Feedlist>> getPriceListFeed(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<Hatchinglist>> getPriceListHatching(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<ArrayList<PriceList>> getPriceListItem(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<OvkPrice> layerfarm_set_pricelist_ovk(@Header("X-CSRF-Token") String token, @Body ParameterSetPriceList parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<FeedPrice> layerfarm_set_pricelist_feed(@Header("X-CSRF-Token") String token, @Body ParameterSetPriceList parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<HatchingPrice> layerfarm_set_pricelist_hatching(@Header("X-CSRF-Token") String token, @Body ParameterSetPriceList parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String> create_layerfarm_user(@Header("X-CSRF-Token") String token, @Body ParameterRegistrationCreateLayerfarmUser parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> create_layerfarm_location(@Header("X-CSRF-Token") String token, @Body ParameterCreateLocation parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> create_layerfarm_feed(@Header("X-CSRF-Token") String token, @Body ParameterCreateFeed parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String[]> create_layerfarm_flock(@Header("X-CSRF-Token") String token, @Body ParameterCreateFlock parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String> layerfarm_delete_data_setting(@Header("X-CSRF-Token") String token, @Body ParameterDeleteDataSetting parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardLayerDetailModel> DashboardLayerDetailPerformance(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardLayerDetailModel.LayerPerformanceDetail> DashboardLayerDetailPopulation(@Header("X-CSRF-Token") String token, @Body Parameter parameter);


    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardLayerFinancial> DashboardLayerFinancial(@Header("X-CSRF-Token") String token, @Body Parameter parameter);


    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<DashboardLayerDetailProductionModel> DashboardLayerDetailPerformanceProduction(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<LinkedHashMap<String, Report>> getReport(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<LinkedHashMap<String, ReportByLocation>> getReportLocation(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<List<getFlockModel>> getFlockSpinner(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
        //@GET("get_farm")
    Call<List<getStrainModel>> getStrainSpinner(@Header("X-CSRF-Token") String token, @Body Parameter parameter);

}

