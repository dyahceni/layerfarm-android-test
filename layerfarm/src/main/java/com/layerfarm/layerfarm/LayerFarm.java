package com.layerfarm.layerfarm;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.FarmNid;
import com.layerfarm.layerfarm.model.FlockStatus;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

//import org.jdeferred2.Deferred;
//import org.jdeferred2.DeferredManager;
//import org.jdeferred2.DoneCallback;
//import org.jdeferred2.Promise;
//import org.jdeferred2.impl.DefaultDeferredManager;
//import org.jdeferred2.impl.DeferredObject;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import android.support.v4.util.Pair;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import android.provider.Settings.Secure;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by agungsuyono on 12/12/17.
 *
 * LayerFarm Object
 */

public class LayerFarm {
    //    private String farmName;
//    private String farmURL;
    private String email;
    private String password;
    private String token1;
    private String token2;
    private String baseUrl;
    private LoginStatus loginStatus;
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;

    DatabaseHelper db;
    private String uniqueID;

    private static LayerFarm staticInstance;

    public static LayerFarm getInstance(){
        if (staticInstance==null){
            staticInstance = new LayerFarm();
        }
//        context = context;

        return staticInstance;
    }

    public void Connect(Activity activity, final String farmURL, final String email, final String password) {
        //this.farmName = farmName;
        this.email = email;
        this.password = password;

//        uniqueID = "abcde";

        SharedPreferences variables = activity.getSharedPreferences("variables", MODE_PRIVATE);
        uniqueID = variables.getString("uniqueID", null);
        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
            uniqueID = activity.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty");
            SharedPreferences.Editor editor = variables.edit();
            editor.putString("uniqueID", uniqueID);
            editor.apply();
        }

        baseUrl = farmURL;

//        SharedPreferences variables = context.getSharedPreferences("variables",Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = variables.edit();
//        editor.putString("id", uniqueID);

        Log.d("sss","farm url ="+farmURL);
        Log.d("sss","email ="+email);
        Log.d("sss","passwd ="+password);
        Log.d("sss","base ="+baseUrl);

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();

        callToken1.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                final String token1  = response.body();
                token1  = response.body();
                RetrofitData.getInstance().setToken1(token1);



                // Sebelum melakukan login, cek dulu apakah username yang digunakan utk login ini sudah terdaftar belum di login_log server.
                String module = "layerfarm_android";
                String function_name = "layerfarm_android_get_login_status";
                String[] args = {email, uniqueID};
                String anonymous = "1";

                ParameterAnon parameterAnon = new ParameterAnon(module, function_name, args, anonymous);
                Call<LoginStatus> callLoginStatus = apiInterfaceJson.getLoginStatus(token1, parameterAnon);

                callLoginStatus.enqueue(new Callback<LoginStatus>() {
                    @Override
                    public void onResponse(Call<LoginStatus> callLoginStatus, Response<LoginStatus> response) {
                        loginStatus = response.body();
                        Log.d("Layerfarm", "loginStatus = " + loginStatus);
                        if (!loginStatus.getStatus().equals("cannot_logged_in")) {
                            // Kalau terdeteksi belum login, maka user tsb boleh login selama username dan passwordnya cocok

                            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);

                            callConnect.enqueue(new Callback<Connect>() {
                                @Override
                                public void onResponse(Call<Connect> call, Response<Connect> response) {
                                    connection = response.body();

                                    try{
                                        user = connection.getUser();

                                        Integer uid = user.getUid();

                                        if (uid == 0) {
                                            // Lakukan login terlebih dahulu
                                            Login login = new Login(email, password);
                                            Call<Connect> callLogin = apiInterfaceJson.login(token1, login);

                                            callLogin.enqueue(new Callback<Connect>() {
                                                @Override
                                                public void onResponse(Call<Connect> call, Response<Connect> response) {
                                                    connection = response.body();   // Jika username & password incorrect, response = null
                                                    if (connection != null) {
                                                        user = connection.getUser();
                                                        Log.d("Layerfarm", "uid = " + user.getUid());
                                                        RetrofitData.getInstance().setBaseUrl(farmURL);
                                                        RetrofitData.getInstance().setPassword(password);
                                                        RetrofitData.getInstance().setEmail(email);
                                                        RetrofitData.getInstance().setUser(user);
//                                            RetrofitData.getInstance().setStatus("Success");
                                                        Map<String, String> roles = user.getRoles();
                                                        Log.d("Layerfarm", "roles = " + roles.toString());
                                                        Log.d("Layerfarm", "username = "+ RetrofitData.getInstance().getUser().getName());



                                                        for (Map.Entry<String, String> entry : roles.entrySet())
                                                        {
                                                            Log.d("Layerfarm", entry.getKey() + "/" + entry.getValue());
                                                        }

                                                        Map<String, String> headers = new HashMap<>();
                                                        headers.put(connection.getSessionName(), connection.getSessid());
                                                        Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                                                        callToken2.enqueue(new Callback<String>() {
                                                            @Override
                                                            public void onResponse(Call<String> call, Response<String> response) {
                                                                token2  = response.body();
                                                                RetrofitData.getInstance().setToken2(token2);
                                                                Log.d("Layerfarm", "token2 = " + token2);
//                                                RetrofitData.getInstance().setStatus("Success");
                                                                String module = "layerfarm_android";
                                                                String function_name = "layerfarm_android_get_farm_name";
                                                                String[] args = {};
                                                                Parameter parameter = new Parameter(module, function_name, args);
                                                                ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                                                String token2 = RetrofitData.getInstance().getToken2();
                                                                Call<String[]> callfarmName = apiInterfaceJson.getFarmName(token2, parameter);

                                                                callfarmName.enqueue(new Callback<String[]>() {
                                                                    @Override
                                                                    public void onResponse(Call<String[]> call, Response<String[]> response) {
                                                                        String[] farm_name = response.body();
                                                                        Log.d("Layerfarm","farm_name = "+farm_name[0]);
                                                                        RetrofitData.getInstance().setFarm_name(farm_name[0]);
                                                                        RetrofitData.getInstance().setStatus("Success");


//                                                        String module = "layerfarm_android_unregistered";
//                                                        String function_name = "layerfarm_android_get_farm_nid";
//                                                        String[] args = {"dyah"};
//                                                        Parameter parameter = new Parameter(module, function_name, args);
//                                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                                        String token2 = RetrofitData.getInstance().getToken2();
//                                                        Call<JsonElement> callfarmName = apiInterfaceJson.getFarmNid(token2, parameter);
//
//                                                        callfarmName.enqueue(new Callback<JsonElement>() {
//                                                            @Override
//                                                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                                                                JsonElement farm_name = response.body();
//                                                            }
//                                                            @Override
//                                                            public void onFailure(Call<JsonElement> call, Throwable t) {
//                                                                Log.d("Layerfarm", "farm_nid error!");
//                                                            }
//                                                        });
                                                                    }
                                                                    @Override
                                                                    public void onFailure(Call<String[]> call, Throwable t) {
                                                                        Log.d("Layerfarm", "farm_name error!");
                                                                    }
                                                                });

                                                                if (loginStatus.getStatus().equals("not_logged_in")) {
                                                                    // Set di table 'layerfarm_login_log' bahwa user ini sudah login, sehingga tidak bisa login di device yang lain
                                                                    String module2 = "layerfarm_android";
                                                                    String function_name2 = "layerfarm_android_set_login";
                                                                    String[] args2 = {email, uniqueID};
                                                                    Parameter parameter2 = new Parameter(module2, function_name2, args2);
//                                                                ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//                                                                String token2 = RetrofitData.getInstance().getToken2();
                                                                    Call<String[]> callSetLogin = apiInterfaceJson.callapi(token2, parameter2);

                                                                    callSetLogin.enqueue(new Callback<String[]>() {
                                                                        @Override
                                                                        public void onResponse(Call<String[]> call, Response<String[]> response) {
                                                                            String[] res = response.body();


                                                                        }
                                                                        @Override
                                                                        public void onFailure(Call<String[]> call, Throwable t) {
                                                                            Log.d("Layerfarm", "set login error!");
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<String> call, Throwable t) {
                                                                Log.d("Layerfarm", "processToken2 error!");
                                                                RetrofitData.getInstance().setStatus("processToken2 error!");
                                                            }
                                                        });
                                                    } else {
//                                            if (response.body().equals("Wrong username or password.")){
//                                                // Login failed
                                                        Log.d("Layerfarm", "Wrong username or password.");
                                                        RetrofitData.getInstance().setStatus("Wrong username or password");
//                                            }
                                                        Log.d("zzz","response = "+response.body());
//                                            // Login failed
//                                            Log.d("Layerfarm", "Login failed.");
//                                            RetrofitData.getInstance().setStatus("Error");
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Connect> call, Throwable t) {
                                                    Log.d("Layerfarm", "processLogin error!");
                                                    RetrofitData.getInstance().setStatus("Wrong username or password");
                                                }
                                            });

                                        } else {
                                            Map<String, String> headers = new HashMap<>();
                                            headers.put(connection.getSessionName(), connection.getSessid());
                                            Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                                            callToken2.enqueue(new Callback<String>() {
                                                @Override
                                                public void onResponse(Call<String> call, Response<String> response) {
                                                    token2  = response.body();
                                                    Log.d("Layerfarm", "token2 = " + token2);

                                                }

                                                @Override
                                                public void onFailure(Call<String> call, Throwable t) {
                                                    Log.d("Layerfarm", "processToken2 error!");
                                                    RetrofitData.getInstance().setStatus("processToken2 error");
                                                }
                                            });
                                        }
                                    }catch (Exception e){
                                        Log.d("zzzFailed","Failed");
                                        RetrofitData.getInstance().setStatus("Web maintenance!");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Connect> call, Throwable t) {
                                    Log.d("Layerfarm", "processConnectionStatus error! Connect");
                                    if(t instanceof SocketTimeoutException){
                                        Log.d("Layerfarm", "Socket Time out. Please try again.!");
//                            message = "Socket Time out. Please try again.";
                                        RetrofitData.getInstance().setStatus("processToken2 error");
                                    }
                                }
                            });

                        } else {
                            // Jika sudah ada record login di table 'layerfarm_login_log', berarti dia sudah login dari salah satu device-nya.  Tidak boleh login lebih dari satu device.
                            Log.d("Layerfarm", "This account has been used in other device.");
                            RetrofitData.getInstance().setStatus("This account has been used in other device.");
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginStatus> callLoginStatus, Throwable t) {
                        Log.d("Layerfarm", "LoginStatus failure = " + t.getMessage());
                        RetrofitData.getInstance().setStatus("LoginStatus failure = " + t.getMessage());
                    }
                });






            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
                RetrofitData.getInstance().setStatus("processToken1 error");
                resultConnect = t.getMessage();
                //Log.d("resultConnect", "resultConnect = " + resultConnect);
            }
        });

    }

    public void ConnectServer(Activity activity, final String farmURL, final String email, final String password){
        //this.farmName = farmName;
        this.email = email;
        this.password = password;

//        uniqueID = "abcde";

//        SharedPreferences variables = activity.getSharedPreferences("variables", Context.MODE_PRIVATE);
//        uniqueID = variables.getString("uniqueID", null);
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//            SharedPreferences.Editor editor = variables.edit();
//            editor.putString("uniqueID", uniqueID);
//            editor.apply();
//        }

        baseUrl = farmURL;

//        SharedPreferences variables = context.getSharedPreferences("variables",Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = variables.edit();
//        editor.putString("id", uniqueID);

/*        Log.d("sss","farm url ="+farmURL);
        Log.d("sss","email ="+email);
        Log.d("sss","passwd ="+password);*/

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();

        callToken1.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                final String token1  = response.body();
                token1  = response.body();
                RetrofitData.getInstance().setToken1(token1);

                Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);

                callConnect.enqueue(new Callback<Connect>() {
                    @Override
                    public void onResponse(Call<Connect> call, Response<Connect> response) {
                        connection = response.body();

                        try{
                            user = connection.getUser();

                            Integer uid = user.getUid();

                            if (uid == 0) {
                                // Lakukan login terlebih dahulu
                                Login login = new Login(email, password);
                                Call<Connect> callLogin = apiInterfaceJson.login(token1, login);

                                callLogin.enqueue(new Callback<Connect>() {
                                    @Override
                                    public void onResponse(Call<Connect> call, Response<Connect> response) {
                                        connection = response.body();   // Jika username & password incorrect, response = null
                                        if (connection != null) {
                                            user = connection.getUser();
                                            Log.d("Layerfarm", "uid = " + user.getUid());
                                            RetrofitData.getInstance().setBaseUrl(farmURL);
                                            RetrofitData.getInstance().setPassword(password);
                                            RetrofitData.getInstance().setEmail(email);
                                            RetrofitData.getInstance().setUser(user);
//                                            RetrofitData.getInstance().setStatus("Success");
                                            Map<String, String> roles = user.getRoles();
                                            Log.d("Layerfarm", "roles = " + roles.toString());
                                            Log.d("Layerfarm", "username = "+ RetrofitData.getInstance().getUser().getName());



                                            for (Map.Entry<String, String> entry : roles.entrySet())
                                            {
                                                Log.d("Layerfarm", entry.getKey() + "/" + entry.getValue());
                                            }

                                            Map<String, String> headers = new HashMap<>();
                                            headers.put(connection.getSessionName(), connection.getSessid());
                                            Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                                            callToken2.enqueue(new Callback<String>() {
                                                @Override
                                                public void onResponse(Call<String> call, Response<String> response) {
                                                    token2  = response.body();
                                                    RetrofitData.getInstance().setToken2(token2);
                                                    Log.d("Layerfarm", "token2 = " + token2);
//                                                RetrofitData.getInstance().setStatus("Success");
                                                    String module = "layerfarm_android";
                                                    String function_name = "layerfarm_android_get_farm_name";
                                                    String[] args = {};
                                                    Parameter parameter = new Parameter(module, function_name, args);
                                                    ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                                    String token2 = RetrofitData.getInstance().getToken2();
                                                    Call<String[]> callfarmName = apiInterfaceJson.getFarmName(token2, parameter);

                                                    callfarmName.enqueue(new Callback<String[]>() {
                                                        @Override
                                                        public void onResponse(Call<String[]> call, Response<String[]> response) {
                                                            String[] farm_name = response.body();
                                                            Log.d("Layerfarm","farm_name = "+farm_name[0]);
                                                            RetrofitData.getInstance().setFarm_name(farm_name[0]);
                                                            RetrofitData.getInstance().setStatus("Success");

                                                        }
                                                        @Override
                                                        public void onFailure(Call<String[]> call, Throwable t) {
                                                            Log.d("Layerfarm", "farm_name error!");
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onFailure(Call<String> call, Throwable t) {
                                                    Log.d("Layerfarm", "processToken2 error!");
                                                    RetrofitData.getInstance().setStatus("processToken2 error!");
                                                }
                                            });
                                        } else {
//                                            if (response.body().equals("Wrong username or password.")){
//                                                // Login failed
                                            Log.d("Layerfarm", "Wrong username or password.");
                                            RetrofitData.getInstance().setStatus("Wrong username or password");
//                                            }
                                            Log.d("zzz","response = "+response.body());
//                                            // Login failed
//                                            Log.d("Layerfarm", "Login failed.");
//                                            RetrofitData.getInstance().setStatus("Error");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Connect> call, Throwable t) {
                                        Log.d("Layerfarm", "processLogin error!");
                                        RetrofitData.getInstance().setStatus("Wrong username or password");
                                    }
                                });

                            } else {
                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                                callToken2.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        token2  = response.body();
                                        Log.d("Layerfarm", "token2 = " + token2);

                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        Log.d("Layerfarm", "processToken2 error!");
                                        RetrofitData.getInstance().setStatus("processToken2 error");
                                    }
                                });
                            }
                        }catch (Exception e){
                            Log.d("zzzFailed","Failed");
                            RetrofitData.getInstance().setStatus("Web maintenance!");
                        }
                    }

                    @Override
                    public void onFailure(Call<Connect> call, Throwable t) {
                        Log.d("Layerfarm", "processConnectionStatus error! Connect");
                        if(t instanceof SocketTimeoutException){
                            Log.d("Layerfarm", "Socket Time out. Please try again.!");
//                            message = "Socket Time out. Please try again.";
                            RetrofitData.getInstance().setStatus("processToken2 error");
                        }
                    }
                });








//                // Sebelum melakukan login, cek dulu apakah username yang digunakan utk login ini sudah terdaftar belum di login_log server.
//                String module = "layerfarm_android";
//                String function_name = "layerfarm_android_get_login_status";
//                String[] args = {email, uniqueID};
//                String anonymous = "1";
//
//                ParameterAnon parameterAnon = new ParameterAnon(module, function_name, args, anonymous);
//                Call<LoginStatus> callLoginStatus = apiInterfaceJson.getLoginStatus(token1, parameterAnon);
//
//                callLoginStatus.enqueue(new Callback<LoginStatus>() {
//                    @Override
//                    public void onResponse(Call<LoginStatus> callLoginStatus, Response<LoginStatus> response) {
//                        loginStatus = response.body();
//                        Log.d("Layerfarm", "loginStatus = " + loginStatus);
//                        if (!loginStatus.getStatus().equals("cannot_logged_in")) {
//                            // Kalau terdeteksi belum login, maka user tsb boleh login selama username dan passwordnya cocok
//
//
//
//                        } else {
//                            // Jika sudah ada record login di table 'layerfarm_login_log', berarti dia sudah login dari salah satu device-nya.  Tidak boleh login lebih dari satu device.
//                            Log.d("Layerfarm", "This account has been used in other device.");
//                            RetrofitData.getInstance().setStatus("This account has been used in other device.");
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<LoginStatus> callLoginStatus, Throwable t) {
//                        Log.d("Layerfarm", "LoginStatus failure = " + t.getMessage());
//                    }
//                });






            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
                RetrofitData.getInstance().setStatus("processToken1 error");
                resultConnect = t.getMessage();
                //Log.d("resultConnect", "resultConnect = " + resultConnect);
            }
        });




    }

    /*
    public void CreateMember(final String farmURL, final String farmCode, final String email, final String password){
        //this.farmName = farmName;

        final String passurl = "sapua#@!";
        final String unameurl = "root";
        String code = "";
        if (!farmCode.isEmpty()){
            code = farmCode + ".";
        }
        final String URL = "http://"+code+farmURL+"/";
        this.email = email;
        this.password = password;
        final String baseUrl = "http://"+farmURL+"/";
        Log.d("sss","farm url ="+farmURL);
        Log.d("sss","email ="+email);
        Log.d("sss","passwd ="+password);

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();

        callToken1.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                final String token1  = response.body();
                token1  = response.body();
                RetrofitData.getInstance().setToken1(token1);

                Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);

                callConnect.enqueue(new Callback<Connect>() {
                    @Override
                    public void onResponse(Call<Connect> call, Response<Connect> response) {
                        connection = response.body();

                        user = connection.getUser();

                        Integer uid = user.getUid();

                        if (uid == 0) {
                            // Lakukan login terlebih dahulu
                            Login login = new Login(unameurl, passurl);
                            Call<Connect> callLogin = apiInterfaceJson.login(token1, login);

                            callLogin.enqueue(new Callback<Connect>() {
                                @Override
                                public void onResponse(Call<Connect> call, Response<Connect> response) {
                                    connection = response.body();   // Jika username & password incorrect, response = null
                                    if (connection != null) {
                                        user = connection.getUser();
                                        Log.d("Layerfarm", "uid = " + user.getUid());

                                        RetrofitData.getInstance().setFarm_name(farmCode);
                                        RetrofitData.getInstance().setBaseUrl(baseUrl);
                                        RetrofitData.getInstance().setPassword(password);
                                        RetrofitData.getInstance().setEmail(email);
                                        RetrofitData.getInstance().setUser(user);
                                        Map<String, String> roles = user.getRoles();
                                        Log.d("Layerfarm", "roles = " + roles.toString());
                                        for (Map.Entry<String, String> entry : roles.entrySet())
                                        {
                                            Log.d("Layerfarm", entry.getKey() + "/" + entry.getValue());
                                        }

                                        Map<String, String> headers = new HashMap<>();
                                        headers.put(connection.getSessionName(), connection.getSessid());
                                        Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                                        callToken2.enqueue(new Callback<String>() {
                                            @Override
                                            public void onResponse(Call<String> call, Response<String> response) {
                                                token2  = response.body();
                                                RetrofitData.getInstance().setToken2(token2);
                                                Log.d("Layerfarm", "token2 = " + token2);
                                                //mendapatkan nid dari farm terlebih dahulu
                                                String module = "layerfarm_android_unregistered";
                                                String function_name = "layerfarm_get_farm_nid";
                                                String[] args = {farmCode};
                                                Parameter parameter = new Parameter(module, function_name, args);
                                                ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                                                String token2 = RetrofitData.getInstance().getToken2();
                                                Call<String[]> callfarmName = apiInterfaceJson.getFarmNid(token2, parameter);

                                                callfarmName.enqueue(new Callback<String[]>() {
                                                    @Override
                                                    public void onResponse(Call<String[]> call, Response<String[]> response) {
                                                        String[] farm_nid = response.body();
                                                        RetrofitData.getInstance().setFarmNid(farm_nid[0]);

                                                        Connect(URL,email,password);


//                                                Map<String, String> headers = new HashMap<>();
//                                                headers.put(connection.getSessionName(), connection.getSessid());
//                                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
//
//                                                callToken2.enqueue(new Callback<String>() {
//                                                    @Override
//                                                    public void onResponse(Call<String> call, Response<String> response) {
//                                                        String token2  = response.body();
//                                                        RetrofitData.getInstance().setToken2(token2);
//                                                        Log.d("Layerfarm", "token2 = " + token2);
////                                                RetrofitData.getInstance().setStatus("Success");
//                                                        String module = "layerfarm_android";
//                                                        String function_name = "layerfarm_android_get_farm_name";
//                                                        String[] args = {};
//                                                        Parameter parameter = new Parameter(module, function_name, args);
//                                                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
////                                                        String token2 = RetrofitData.getInstance().getToken2();
//                                                        Call<String[]> callfarmName = apiInterfaceJson.getFarmName(token2, parameter);
//
//                                                        callfarmName.enqueue(new Callback<String[]>() {
//                                                            @Override
//                                                            public void onResponse(Call<String[]> call, Response<String[]> response) {
//                                                                String[] farm_name = response.body();
//                                                                Log.d("Layerfarm","farm_name = "+farm_name[0]);
//                                                                RetrofitData.getInstance().setStatus("Success");
//                                                            }
//                                                            @Override
//                                                            public void onFailure(Call<String[]> call, Throwable t) {
//                                                                Log.d("Layerfarm", "farm_name error!");
//                                                            }
//                                                        });
//                                                    }
//
//                                                    @Override
//                                                    public void onFailure(Call<String> call, Throwable t) {
//                                                        Log.d("Layerfarm", "processToken2 error!");
//                                                        RetrofitData.getInstance().setStatus("processToken2 error!");
//                                                    }
//                                                });
                                                    }
                                                    @Override
                                                    public void onFailure(Call<String[]> call, Throwable t) {
                                                        Log.d("Layerfarm", "farm_nid error!");
                                                    }
                                                });

                                            }

                                            @Override
                                            public void onFailure(Call<String> call, Throwable t) {
                                                Log.d("Layerfarm", "processToken2 error!");
                                                RetrofitData.getInstance().setStatus("processToken2 error!");
                                            }
                                        });



                                    } else {
                                        // Login failed
                                        Log.d("Layerfarm", "Login failed member.");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Connect> call, Throwable t) {
                                    Log.d("Layerfarm", "processLogin error!");
                                }
                            });

                        } else {
                            Map<String, String> headers = new HashMap<>();
                            headers.put(connection.getSessionName(), connection.getSessid());
                            Call<String> callToken2 = apiInterfaceToken.getToken2(headers);

                            callToken2.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    token2  = response.body();
                                    Log.d("Layerfarm", "token2 = " + token2);

                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.d("Layerfarm", "processToken2 error!");
                                }
                            });
                        }



                    }

                    @Override
                    public void onFailure(Call<Connect> call, Throwable t) {
                        Log.d("Layerfarm", "processConnectionStatus error Create Member!");
                    }
                });
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
                resultConnect = t.getMessage();
                //Log.d("resultConnect", "resultConnect = " + resultConnect);
            }
        });
    }
     */

/*    public void isConnect(final String farmURL, final String email, final String password) {
        //this.farmName = farmName;
        this.email = email;
        this.password = password;

        baseUrl = farmURL;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();

        callToken1.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                resultConnect = t.getMessage();
            }
        });

    }*/

    public String getConnect() {
        return resultConnect;
    }


//    @Override
//    protected Boolean doInBackground(Void... voids) {
//        Call<String> callToken1 = apiInterfaceToken.getToken1();
//
//        try {
//            Response<String> response = callToken1.execute();
//            if (response.isSuccessful()) {
//                token1  = response.body();
//                Log.d("Layerfarm", "doInBackground isSuccessful!");
//                return true;
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d("Layerfarm", "doInBackground failed!");
//        }
//
//        return false;
//    }
//
//    @Override
//    protected void onPostExecute(Boolean success) {
//        if (success) {
//            Log.d("Layerfarm", "onPostExecute success!");
//            Toast.makeText(context, "token1 = " + token1, Toast.LENGTH_SHORT).show();
//        } else {
//            Log.d("Layerfarm", "onPostExecute failed!");
//            Toast.makeText(context, "Connection failed", Toast.LENGTH_SHORT).show();
//        }
//    }

    /////
    /// DEBUGGING TOOLS.....
    /////
    // Token 1
    public void processToken1() {

        Call<String> call = apiInterfaceToken.getToken1();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                final String token1  = response.body();
                token1  = response.body();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
            }
        });
    }

    public String getToken1() {
        return token1;
    }

    // Connection
    public void processConnectionStatus() {

        Call<Connect> call = apiInterfaceJson.getConnect(token1);

        call.enqueue(new Callback<Connect>() {
            @Override
            public void onResponse(Call<Connect> call, Response<Connect> response) {
                connection = response.body();

                user = connection.getUser();

                Integer uid = user.getUid();

                Log.d("Layerfarm", "uid = " + uid);

            }

            @Override
            public void onFailure(Call<Connect> call, Throwable t) {
                Log.d("Layerfarm", "processConnectionStatus error!");
            }
        });

    }

    public Connect getConnectionStatus() {
        return connection;
    }

    // Login
    private HttpCookie httpCookie;
    public void processLogin() {

        Login login = new Login(email, password);
        Call<Connect> call = apiInterfaceJson.login(token1, login);

        call.enqueue(new Callback<Connect>() {
            @Override
            public void onResponse(Call<Connect> call, Response<Connect> response) {
                connection = response.body();
                user = connection.getUser();
                Log.d("Layerfarm", "uid = " + user.getUid());

                Map<String, String> roles = user.getRoles();
                Log.d("Layerfarm", "roles = " + roles);

                for (Map.Entry<String, String> entry : roles.entrySet())
                {
                    Log.d("Layerfarm", entry.getKey() + "/" + entry.getValue());
                }

//                httpCookie = new HttpCookie(connectLogin.getSessionName(), connectLogin.getSessid());
            }

            @Override
            public void onFailure(Call<Connect> call, Throwable t) {
                Log.d("Layerfarm", "processLogin error!");
            }
        });
    }

    public Connect getLogin() {
        return connection;
    }

    // token 2
    public void processToken2() {// Setelah berhasil login, dapatkan CSRF token yang disertai dengan cookie session. Perhatikan, csrf token di sini beda dengan yang dihasilkan pada proses 1.

        Map<String, String> headers = new HashMap<>();
        headers.put(connection.getSessionName(), connection.getSessid());
        Call<String> call = apiInterfaceToken.getToken2(headers);

//        String sessionIdAndToken = connectLogin.getSessionName() + "=" + connectLogin.getSessid();
//        Call<String> call = apiInterfaceToken.getToken2c(sessionIdAndToken);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                token2  = response.body();
                Log.d("Layerfarm", "token2 = " + token2);

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken2 error!");
            }
        });
    }

    public String getToken2() {
        return token2;
    }

    private FlockStatus flockStatus;
    public void processFlockStatus(Integer flockNid) {
        String module = "layerfarm";
        String function_name = "layerfarm_get_kandang_status";
        String[] args = {flockNid.toString()};

        Parameter parameter = new Parameter(module, function_name, args);
        Call<FlockStatus> call = apiInterfaceJson.getFlockStatus(token2, parameter);

        call.enqueue(new Callback<FlockStatus>() {
            @Override
            public void onResponse(Call<FlockStatus> call, Response<FlockStatus> response) {
                flockStatus = response.body();
                Log.d("Layerfarm", "flockStatus = " + flockStatus);
            }

            @Override
            public void onFailure(Call<FlockStatus> call, Throwable t) {
                Log.d("Layerfarm", "FlockStatus failure = " + t.getMessage());
            }
        });

    }

    public FlockStatus getFlockStatus() {
        return flockStatus;
    }

//    // Login without Token
//    public void processLoginNoToken() {
//        // Lakukan login terlebih dahulu
//        Retrofit retrofit3 = new Retrofit.Builder()
//                .baseUrl(baseUrl)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        final ApiInterface apiInterface =retrofit3.create(ApiInterface.class);
//
//        Login login = new Login(email, password);
//        Call<Connect> call3 = apiInterface.loginNoToken(login);
//
//        //Call<User> call3 = apiInterface.getUser(token, email, password);
//
//        call3.enqueue(new Callback<Connect>() {
//            @Override
//            public void onResponse(Call<Connect> call, Response<Connect> response) {
//                connectLogin = response.body();
//                user = connectLogin.getUser();
//                Log.d("Layerfarm", "uid = " + user.getUid());
//
//                Map<String, String> roles = user.getRoles();
//                Log.d("Layerfarm", "roles = " + roles);
//
//                for (Map.Entry<String, String> entry : roles.entrySet())
//                {
//                    Log.d("Layerfarm", entry.getKey() + "/" + entry.getValue());
//                }
//
//                //===========
//
//                String token = connectLogin.getToken();
//
////                Retrofit retrofit = new Retrofit.Builder()
////                        .baseUrl(baseUrl)
////                        .addConverterFactory(GsonConverterFactory.create())
////                        .build();
////
////                ApiInterface apiInterfaceA =retrofit.create(ApiInterface.class);
//
//                String module = "layerfarm";
//                String function_name = "layerfarm_get_kandang_status";
//                String[] args = {"12"};
//
//                Parameter parameter = new Parameter(module, function_name, args);
//                    Call<FlockStatus> callA = apiInterface.getFlockStatus(token, parameter);
////                Call<FlockStatus> callA = apiInterface.getFlockStatusUsingAuthentication(token, parameter);
//
//                callA.enqueue(new Callback<FlockStatus>() {
//                    @Override
//                    public void onResponse(Call<FlockStatus> callA, Response<FlockStatus> response) {
//                        flockStatus = response.body();
//                        Log.d("Layerfarm", "flockStatus = " + flockStatus);
//                    }
//
//                    @Override
//                    public void onFailure(Call<FlockStatus> callA, Throwable t) {
//                        Log.d("Layerfarm", "FlockStatus failure = " + t.getMessage());
//                    }
//                });
//
//                //==============
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Connect> call, Throwable t) {
//
//            }
//        });
//    }

}

/*
public class LayerFarm extends AsyncTask<Void, Integer, String> {
    private String farmName;
    private String email;
    private String password;
    private String token1;
    private String token2;
    private String baseUrl;
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;


    private Context context;
    TextView textView;
    ProgressDialog progressDialog;

    public LayerFarm(Context context, TextView textView) {
        this.context = context;
        this.textView = textView;
    }

    @Override
    protected String doInBackground(Void... voids) {
        int i = 0;
        synchronized (this) {
            while (i < 10) {
                try {
                    wait(1500);
                    i++;
                    publishProgress(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return "Download complete....";
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Download in Progress...");
        progressDialog.setMax(10);
        progressDialog.setProgress(0);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        textView.setText(result);
        progressDialog.hide();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        int progress = values[0];
        progressDialog.setProgress(progress);
        textView.setText("Download in Progress...");
    }

    /////
    /// DEBUGGING TOOLS.....
    /////
    public void processToken1() {
    }

    public String getToken1() {
        return token1;
    }

    // Connection
    public void processConnectionStatus() {
    }

    public Connect getConnectionStatus() {
        return connection;
    }

    // Login
    private HttpCookie httpCookie;
    public void processLogin() {
    }

    public Connect getLogin() {
        return connection;
    }

    // token 2
    public void processToken2() {
    }

    public String getToken2() {
        return token2;
    }

    private FlockStatus flockStatus;
    public void processFlockStatus(Integer flockNid) {
    }

    public FlockStatus getFlockStatus() {
        return flockStatus;
    }
}
*/