package com.layerfarm.layerfarm;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.HashMap;

public class LayerfarmHelper {

    DatabaseHelper database ;
    private SQLiteDatabase db;

    private static String DB_PATH = "/data/data/com.layerfarm.layerfarmmanager/databases/";
    private static String DB_NAME = "layerfarm-mobile.db";

    public LayerfarmHelper (Context context){
        database = new DatabaseHelper(context);
    }

    //membuat sambungan ke database
    public void open() throws SQLException {
        db= database.getWritableDatabase();
    }

    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    public String last_chick_in_id(){

        SQLiteDatabase db =  SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        //get last record chickin
        String selectQuery= "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            strlastchickin = cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();
            //Log.d("Layerfarm", "data = " + strlastchickin);
        }

        return strlastchickin;

    }

    public void layerfarm_update_hatch_date_tracker(String flockId){

        SQLiteDatabase db =  SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);

        //get flock
        String selectQuery= "SELECT flock_id FROM hatch_date_tracker Where flock_id = '" + flockId + "' ORDER BY id DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst() && cursor.getCount() > 0) {

        }
    }

    public HashMap<String,String> layerfarm_get_start_recording(String flock_id, String $tgl_tetas){
        this.open();
        HashMap<String, String> value = new HashMap<>();
        String start_recording = "";
        String Query = "SELECT * FROM chick_in_distribution INNER JOIN chick_in on chick_in.id = chick_in_distribution.chick_in_id\n" +
                "WHERE hatch_date = '"+$tgl_tetas+"' AND flock_id = '"+flock_id+"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst()){
            value.put("date",cursor.getString(cursor.getColumnIndex("start_recording")));
            value.put("number_of_bird", cursor.getString(cursor.getColumnIndex("number_of_bird")));
        }
        cursor.close();
        this.close();
        return  value;
    }
    public int layerfarm_get_bird_chick_in(String flockID, String dateRecording, String hatchDate){

        //jika burung sudah didefinisikan di dalam menu chick in
        //String flock_empty = getDateFlockEmpty(flockID,dateRecording);
        this.open();
        int chick_in =0;
        String Query = "SELECT * FROM chick_in_distribution INNER JOIN chick_in on chick_in.id = chick_in_distribution.chick_in_id\n" +
                "WHERE hatch_date = '"+hatchDate+"' AND flock_id = '"+flockID+"' AND start_recording = '"+dateRecording+"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst())
            chick_in  =  cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        cursor.close();
        Log.d("zzzs","ayam chick in ="+chick_in);
        this.close();
        return chick_in;
    }

    //mendapatkan jumlah ayam dari transfer in hari suatu tanggal
    //fungsinya untuk mendata jumlah ayam masuk dari hatch date tertentu sbg penambah jumlah ayam
    public int layerfarm_get_bird_transfer_in(String flockId, String dateRecording, String hatchDate){
        int transfer_in =0;
        this.open();
        //karena bisa saja trenjadi transfer in lebih dari 1 pada hari tersebut
        String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id WHERE daily_recording.recording_date = '"+dateRecording+"' \n" +
                "and bird_transfer.to_flock_id = '"+flockId+"' AND chick_in.hatch_date = '"+hatchDate+"'";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            transfer_in = transfer_in + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        Log.d("zzzs","transfer in today = "+transfer_in);
        this.close();
        return transfer_in;
    }

    //mendapatkan jumlah ayam tranfer out pada hari ini untuk flock tertentu hatch date tertentu
    public int layerfarm_get_bird_transfer_out(String flockId, String dateRecording, String hatchDate){
        int transfer_out = 0;
        this.open();
        String Query= "SELECT * from bird_transfer INNER JOIN daily_recording on daily_recording.id = bird_transfer.daily_recording_id\n" +
                "INNER JOIN chick_in on chick_in.id = bird_transfer.chick_in_id where chick_in.hatch_date = '"+hatchDate+"' \n" +
                "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            transfer_out = transfer_out + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        this.close();
        return transfer_out;
    }

    //mendapatkan mortality untuk tanggal, flock dan hatch date tertentu
    public int layerfarm_get_bird_mortality(String flockId, String dateRecording, String hatchDate){
        int mortality = 0;
        this.open();
        String Query= "SELECT * from recording_mortality INNER JOIN daily_recording on daily_recording.id = recording_mortality.daily_recording_id\n" +
                "INNER JOIN chick_in on chick_in.id = recording_mortality.chick_in_id where chick_in.hatch_date = '"+hatchDate+"'" +
                "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            mortality = mortality + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        this.close();
        return mortality;
    }

    //mendapatkan chick out today
    public int layerfarm_get_bird_chick_out(String flockId, String dateRecording, String hatchDate){
        int chick_out = 0;
        this.open();
        String query = "SELECT number_of_bird FROM recording_chick_out INNER JOIN daily_recording on daily_recording.id = recording_chick_out.daily_recording_id\n" +
                "INNER JOIN chick_in on recording_chick_out.chick_in_id = chick_in.id WHERE chick_in.hatch_date = '"+hatchDate+"' AND\n" +
                "daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst())
            chick_out = cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        this.close();
        return chick_out;
    }

}
