package com.layerfarm.reported.ByLocation;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.layerfarm.model.ReportByLocation;
import com.layerfarm.reported.R;

import java.util.ArrayList;

public class ReportByLocationContentDetail extends Activity {
    Activity context;
    private ViewGroup list_egg, list_egg_weight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_by_age);
        context = this;
        //Get Data
        Bundle extras = getIntent().getExtras();
        ReportByLocation.Production production = (ReportByLocation.Production) extras.getSerializable("production");
        ReportByLocation.Population population = (ReportByLocation.Population) extras.getSerializable("population");
        ReportByLocation.Treatment treatment = (ReportByLocation.Treatment) extras.getSerializable("treatment");
        ArrayList<ReportByLocation.Egg> data_egg = new ArrayList<ReportByLocation.Egg>(production.getEgg().values());
        ArrayList<ReportByLocation.Egg> data_weight = new ArrayList<ReportByLocation.Egg>(production.getWeight().values());
        //Get TextView
        TextView titleBold = (TextView) findViewById(R.id.flockName);
        TextView titleNormal = (TextView) findViewById(R.id.locatioName);
        TextView total_birdView = (TextView) findViewById(R.id.total_bird);
        TextView total_feedView = (TextView) findViewById(R.id.total_feed);
        TextView chick_inView = (TextView) findViewById(R.id.chick_in);
        TextView total_transfer_inView = (TextView) findViewById(R.id.total_transfer_in);
        TextView chick_outView = (TextView) findViewById(R.id.chick_out);
        TextView total_transfer_outView = (TextView) findViewById(R.id.total_transfer_out);
        TextView total_mortalityView = (TextView) findViewById(R.id.total_mortality);
        TextView mortality_cumulativeView = (TextView) findViewById(R.id.mortality_cumulative);
        TextView deaths_sickView = (TextView) findViewById(R.id.deaths_sick);
        TextView deaths_mechanicView = (TextView) findViewById(R.id.deaths_mechanic);
        TextView deaths_otherView = (TextView) findViewById(R.id.deaths_other);
        TextView deaths_killedView = (TextView) findViewById(R.id.deaths_killed);
        TextView deaths_removedView = (TextView) findViewById(R.id.deaths_removed);
        TextView deaths_sickName = (TextView) findViewById(R.id.textView10);
        TextView deaths_mechanicName = (TextView) findViewById(R.id.textView11);
        TextView deaths_otherName = (TextView) findViewById(R.id.textView12);
        TextView deaths_killedName = (TextView) findViewById(R.id.textView13);
        TextView deaths_removedName = (TextView) findViewById(R.id.textView14);
        list_egg = (ViewGroup) findViewById(R.id.egg_detail_row);
        list_egg_weight = (ViewGroup) findViewById(R.id.weight_detail_row);
        TextView hen_day = (TextView) findViewById(R.id.hen_day);
        TextView fcr = (TextView) findViewById(R.id.fcr);
        TextView hen_housed = (TextView) findViewById(R.id.hen_housed);
        TextView egg_weight = (TextView) findViewById(R.id.egg_weight);
        TextView medication = (TextView) findViewById(R.id.medication);
        TextView body_weights = (TextView) findViewById(R.id.body_weights);
        TextView feed = (TextView) findViewById(R.id.feed);
        TextView feed_intake = (TextView) findViewById(R.id.feed_intake);
        //Set Text
        titleNormal.setText(population.getHatch_date());
        titleBold.setText(population.getAge());
        total_birdView.setText(population.getTotal_bird());
        total_feedView.setText(treatment.getTotal_feed());
        chick_inView.setText(population.getChick_in());
        total_transfer_inView.setText(population.getTransfer_in());
        chick_outView.setText(population.getChick_out().getValue());
        total_transfer_outView.setText(population.getTransfer_out());
        total_mortalityView.setText(population.getMortality());
        mortality_cumulativeView.setText(population.getMortality_total_percent());
        deaths_sickView.setText(population.getMortality1().getValue());
        deaths_mechanicView.setText(population.getMortality2().getValue());
        deaths_otherView.setText(population.getMortality3().getValue());
        deaths_killedView.setText(population.getMortality4().getValue());
        deaths_removedView.setText(population.getMortality5().getValue());
        deaths_sickName.setText(population.getMortality1().getName());
        deaths_mechanicName.setText(population.getMortality2().getName());
        deaths_otherName.setText(population.getMortality3().getName());
        deaths_killedName.setText(population.getMortality4().getName());
        deaths_removedName.setText(population.getMortality5().getName());
        for (int i =0; i< data_egg.size(); i++){
            ViewListEgg(data_egg.get(i));
        }
        for (int a =0; a< data_weight.size(); a++){
            ViewListWeightEgg(data_weight.get(a));
        }
        hen_day.setText(production.getHenday());
        fcr.setText(production.getFcr());
        hen_housed.setText(production.getHenhoused());
        egg_weight.setText(production.getEgg_weight());
        medication.setText(treatment.getMedivac());
        body_weights.setText(treatment.getBw());
        feed.setText(treatment.getFeed());
        feed_intake.setText(treatment.getFeed_intake());
    }
    public void ViewListEgg(ReportByLocation.Egg egg){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View egg_view = inflater.inflate(R.layout.egg_detail_row, null);
        TextView egg_name = (TextView) egg_view.findViewById(R.id.textViewDetail);
        TextView value = (TextView) egg_view.findViewById(R.id.value);
        egg_name.setText(egg.getName());
        value.setText(egg.getValue());
        list_egg.addView(egg_view);
    }
    public void ViewListWeightEgg(ReportByLocation.Egg egg){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View egg_view = inflater.inflate(R.layout.egg_detail_row, null);
        TextView egg_name = (TextView) egg_view.findViewById(R.id.textViewDetail);
        TextView value = (TextView) egg_view.findViewById(R.id.value);
        egg_name.setText(egg.getName());
        value.setText(egg.getValue());
        list_egg_weight.addView(egg_view);
    }
}
