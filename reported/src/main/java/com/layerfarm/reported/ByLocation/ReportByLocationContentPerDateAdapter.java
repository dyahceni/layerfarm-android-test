package com.layerfarm.reported.ByLocation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.ReportByLocation;
import com.layerfarm.reported.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ReportByLocationContentPerDateAdapter extends RecyclerView.Adapter<ReportByLocationContentPerDateAdapter.ViewHolder> {
    Activity context;
    String countryList[];
    int flags[];
    private ViewGroup listPopulation, listTreatment, listAgeTotal, listProduction, list_egg, list_egg_weight;
    LayoutInflater inflter;
    private HashMap<String, ReportByLocation> recording_population_items;
    int index;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> date = new ArrayList<>();
    ArrayList<ReportByLocation> data_report = new ArrayList<>();

    public ReportByLocationContentPerDateAdapter(HashMap<String, ReportByLocation> inputData, int index, Activity content){
        recording_population_items = inputData;
        this.index = index;
        context = content;
        date = new ArrayList<String>(recording_population_items.keySet());
        data_report = new ArrayList<ReportByLocation>(recording_population_items.values());
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView flock_name;
        boolean opened1, opened2, opened3;
        public RecyclerView content_per_date;
        LinearLayout view1, view2, view_production;
        public TextView dot;
        public TextView date_record;
        public TextView sumPopulation;
        public TextView sumProduction;
        public TextView sumTreatment;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        //public CardView card;
        public ViewHolder(final View v) {
            super(v);
            date_record = (TextView) v.findViewById(R.id.dateRecord);
            listAgeTotal = (ViewGroup) v.findViewById(R.id.listAgeTotal);
//            content_per_date = v.findViewById(R.id.content_list);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Context context = v.getContext();
//                    Intent intent = new Intent(context, TreatmentListViewActivity.class);
//                    context.startActivity(intent);
//                }
//            });
        }
    }
    @NonNull
    @Override
    public ReportByLocationContentPerDateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // membuat view baru
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.age_content_list_row, viewGroup, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
//        ReportByAgeContentPerDateAdapter.ViewHolder vh = new ReportByAgeContentPerDateAdapter.ViewHolder(v);
        return new ReportByLocationContentPerDateAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.date_record.setText(date.get(i));
        ArrayList<String> hatch_date = new ArrayList<String>(data_report.get(i).getAges().keySet());
        ArrayList<ReportByLocation.Age> ages = new ArrayList<ReportByLocation.Age>(data_report.get(i).getAges().values());
        for (int m =0; m< ages.size(); m++){
            ViewListAgeTotal(ages.get(m).getPopulation(),ages.get(m).getProduction(), ages.get(m).getTreatment(), ages.get(m).getAge());
        }

    }
    public void ViewListAgeTotal(final ReportByLocation.Population population, final ReportByLocation.Production production, final ReportByLocation.Treatment treatment, String age){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View eggsInf = inflater.inflate(R.layout.treatment_age_list_row, null);
        TextView populationTotal = (TextView) eggsInf.findViewById(R.id.populationTotal);
        TextView ageDate = (TextView) eggsInf.findViewById(R.id.ageDate);
        populationTotal.setText(population.getTotal_bird());
        ageDate.setText(age);
        eggsInf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, ReportByLocationContentDetail.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("population", (Serializable) population);
                bundle.putSerializable("production", (Serializable) production);
                bundle.putSerializable("treatment", (Serializable) treatment);
                i.putExtras(bundle);
                context.startActivity(i);
            }
        });
        listAgeTotal.addView(eggsInf);
    }
    @Override
    public int getItemCount() {
        return data_report.size();
    }
}
