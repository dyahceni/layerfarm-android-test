package com.layerfarm.reported.ByFlock;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.reported.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportByFlockDateMonth extends RecyclerView.Adapter<ReportByFlockDateMonth.ViewHolder> {
    Activity context;
    String countryList[];
    int flags[];
    LayoutInflater inflter;
    private HashMap<String, Report> recording_population_items;
    int index;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    ArrayList<String> date = new ArrayList<>();
    ArrayList<Report> data_report = new ArrayList<>();
    public ReportByFlockDateMonth(HashMap<String, Report> inputData, int index, Activity content){
        recording_population_items = inputData;
        this.index = index;
        context = content;
        date = new ArrayList<String>(inputData.keySet());
        data_report = new ArrayList<>(inputData.values());
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView flock_name;
        public RecyclerView content_per_date;
        public TextView flock_type;
        public TextView dot;
        public TextView valuePercentage;
        public TextView capacity;
        public TextView number_bird;
        public TextView utilization;
        //public CardView card;
        public ViewHolder(final View v) {
            super(v);
            //card = (CardView) v.findViewById(R.id.cv_flock);
//            flock_name = (TextView) v.findViewById(R.id.flockName);
            content_per_date = v.findViewById(R.id.content_list);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Context context = v.getContext();
//                    Intent intent = new Intent(context, TreatmentListViewActivity.class);
//                    context.startActivity(intent);
//                }
//            });
        }
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // membuat view baru
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.treatment_list_row, viewGroup, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
//        DateHistoryAdapter.ViewHolder vh = new DateHistoryAdapter.ViewHolder(v);
        return new ReportByFlockDateMonth.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportByFlockDateMonth.ViewHolder viewHolder, int i) {
        //final String flockname = recording_population_items.get(i);
//        viewHolder.flock_name.setText("satu");
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                viewHolder.content_per_date.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );

        adapter = new ReportByFlockPerDateAdapter(recording_population_items, 2, context);
        viewHolder.content_per_date.setAdapter(adapter);
        viewHolder.content_per_date.setLayoutManager(layoutManager);
        viewHolder.content_per_date.setRecycledViewPool(viewPool);
    }

    @Override
    public int getItemCount() {
        return date.size();
    }
}
