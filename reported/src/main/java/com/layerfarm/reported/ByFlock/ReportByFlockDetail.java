package com.layerfarm.reported.ByFlock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.layerfarm.reported.R;
import com.layerfarm.reported.ByLocation.ReportByLocationActivity;

public class ReportByFlockDetail extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.treatment_age_detail);
        //Get Data
        Bundle extras = getIntent().getExtras();
        final String hatch_date = extras.getString("hatch_date");
        final String age = extras.getString("age");
        final String total_bird = extras.getString("total_bird");
        final String chick_in = extras.getString("chick_in");
        final String transfer_in = extras.getString("transfer_in");
        final String transfer_out = extras.getString("transfer_out");
        final String mortality = extras.getString("mortality");
        final String mortality1 = extras.getString("mortality1");
        final String mortality2 = extras.getString("mortality2");
        final String mortality3 = extras.getString("mortality3");
        final String mortality4 = extras.getString("mortality4");
        final String mortality5 = extras.getString("mortality5");
        final String mortality1Name = extras.getString("mortality1Name");
        final String mortality2Name = extras.getString("mortality2Name");
        final String mortality3Name = extras.getString("mortality3Name");
        final String mortality4Name = extras.getString("mortality4Name");
        final String mortality5Name = extras.getString("mortality5Name");
        final String chick_out = extras.getString("chick_out");
        //Get Text View
        TextView titleBold = (TextView) findViewById(R.id.flockName);
        TextView titleNormal = (TextView) findViewById(R.id.locatioName);
        TextView total_birdView = (TextView) findViewById(R.id.total_bird);
        TextView chick_inView = (TextView) findViewById(R.id.chick_in);
        TextView total_transfer_inView = (TextView) findViewById(R.id.total_transfer_in);
        TextView chick_outView = (TextView) findViewById(R.id.chick_out);
        TextView total_transfer_outView = (TextView) findViewById(R.id.total_transfer_out);
        TextView total_mortalityView = (TextView) findViewById(R.id.total_mortality);
        TextView mortality_cumulativeView = (TextView) findViewById(R.id.mortality_cumulative);
        TextView deaths_sickView = (TextView) findViewById(R.id.deaths_sick);
        TextView deaths_mechanicView = (TextView) findViewById(R.id.deaths_mechanic);
        TextView deaths_otherView = (TextView) findViewById(R.id.deaths_other);
        TextView deaths_killedView = (TextView) findViewById(R.id.deaths_killed);
        TextView deaths_removedView = (TextView) findViewById(R.id.deaths_removed);
        TextView deaths_sickName = (TextView) findViewById(R.id.textView10);
        TextView deaths_mechanicName = (TextView) findViewById(R.id.textView11);
        TextView deaths_otherName = (TextView) findViewById(R.id.textView12);
        TextView deaths_killedName = (TextView) findViewById(R.id.textView13);
        TextView deaths_removedName = (TextView) findViewById(R.id.textView14);
        //Set Text
        titleNormal.setText(hatch_date);
        titleBold.setText(age);
        total_birdView.setText(total_bird);
        chick_inView.setText(chick_in);
        total_transfer_inView.setText(transfer_in);
        chick_outView.setText(chick_out);
        total_transfer_outView.setText(transfer_out);
        total_mortalityView.setText(mortality);
        mortality_cumulativeView.setText(mortality1+" %");
        deaths_sickView.setText(mortality1);
        deaths_mechanicView.setText(mortality2);
        deaths_otherView.setText(mortality3);
        deaths_killedView.setText(mortality4);
        deaths_removedView.setText(mortality5);
        deaths_sickName.setText(mortality1Name);
        deaths_mechanicName.setText(mortality2Name);
        deaths_otherName.setText(mortality3Name);
        deaths_killedName.setText(mortality4Name);
        deaths_removedName.setText(mortality5Name);
        deaths_removedName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, ReportByLocationActivity.class);
                context.startActivity(i);
            }
        });
    }
}
