package com.layerfarm.reported.ByFlock;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.reported.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportByFlockActivity extends Activity {
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    LinkedHashMap<String, Report> data;
    ArrayList<String> date = new ArrayList<>();
    ArrayList<Report> data_report = new ArrayList<>();
    RecyclerView rvItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.treatment_content_list);
        TextView flock_name = (TextView) findViewById(R.id.flockName);
        flock_name.setText("satu");
        getDetailPopulation();
        rvItem = findViewById(R.id.date_history);
        layoutManager = new LinearLayoutManager(this);


        rvItem.setLayoutManager(layoutManager);

    }

    public void getDetailPopulation(){
        final ProgressDialog loading = ProgressDialog.show(this, "Load Data", "Please wait...", true, false);
        String module = "table_report";
        String function_name = "table_report_get_data_flock";
        String[] args={"99803", "2019-09-13", "2019-10-07"};

        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<LinkedHashMap<String, Report>> call = apiInterfaceJson.getReport(token2, parameter);

        call.enqueue(new Callback<LinkedHashMap<String, Report>>() {
            @Override
            public void onResponse(Call<LinkedHashMap<String, Report>> call, Response<LinkedHashMap<String, Report>> response) {
                data = response.body();
                date = new ArrayList<String>(data.keySet());
                data_report = new ArrayList<>(data.values());
                adapter = new ReportByFlockDateMonth(data, 2, ReportByFlockActivity.this);
                rvItem.setAdapter(adapter);

                adapter.notifyDataSetChanged();
                for (int i=0; i< data.size(); i++){
//                    Log.d("Layerfarm", "key = "+data)
                }
//                DashboardGrowerModel.GrowerPerformanceDetailFlockData data = dashboardLayerDetail.getFlock_data();
//                Log.d("zzz","log= "+dashboardLayerDetail.toString());
//                DashboardGrowerModel.getInstance().setLocation_flock(dashboardLayerDetail.getLocation_flock());
//                DashboardGrowerModel.getInstance().setFlock_data(dashboardLayerDetail.getFlock_data());
                //setupViewPager(viewPager);
                loading.dismiss();

            }

            @Override
            public void onFailure(Call<LinkedHashMap<String, Report>> call, Throwable t) {
                String alert = t.getMessage();
                loading.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }

}
