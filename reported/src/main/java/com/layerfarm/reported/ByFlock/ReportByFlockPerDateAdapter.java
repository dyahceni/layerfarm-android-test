package com.layerfarm.reported.ByFlock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.Report;
import com.layerfarm.reported.Adapter.MedicineAdapter;
import com.layerfarm.reported.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportByFlockPerDateAdapter extends RecyclerView.Adapter<ReportByFlockPerDateAdapter.ViewHolder> {
    Activity context;
    String countryList[];
    int flags[];
    private ViewGroup listPopulation, listTreatment, listAgeTotal, listProduction, list_egg, list_egg_weight;

    boolean open_egg, open_weight;
    LayoutInflater inflter;
    private HashMap<String, Report> recording_population_items;
    int index;
    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapter_feed;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> date = new ArrayList<>();
    ArrayList<Report> data_report = new ArrayList<>();

    public ReportByFlockPerDateAdapter(HashMap<String, Report> inputData, int index, Activity content){
        recording_population_items = inputData;
        this.index = index;
        context = content;
        date = new ArrayList<String>(recording_population_items.keySet());
        data_report = new ArrayList<>(recording_population_items.values());
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        boolean opened1, opened2, opened3;
        LinearLayout view1, view2, view_production;
        public TextView date_record;
        public TextView sumPopulation;
        public TextView sumProduction;
        public TextView sumTreatment;
        public TextView henday;
        public TextView fcr;
        //public CardView card;
        public ViewHolder(final View v) {
            super(v);
            listPopulation = (ViewGroup) v.findViewById(R.id.listPopulation);
            listTreatment = (ViewGroup) v.findViewById(R.id.listTreatment);
            listAgeTotal = (ViewGroup) v.findViewById(R.id.listAgeTotal);
            listProduction = (ViewGroup) v.findViewById(R.id.listProduction);

            date_record = (TextView) v.findViewById(R.id.dateRecord);
            sumPopulation = (TextView) v.findViewById(R.id.sumPopulation);
//            sumProduction = (TextView) v.findViewById(R.id.sumProduction);
            henday = (TextView)v.findViewById(R.id.henday);
            fcr = (TextView) v.findViewById(R.id.fcr);
            sumTreatment = (TextView) v.findViewById(R.id.sumTreatment);

            opened1 = true;
            opened2 = true;
            opened3 = true;
            view1 = v.findViewById(R.id.view1);
            view1.setVisibility(View.GONE);
            v.findViewById(R.id.linierPopulation).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!opened1){
                        view1.setVisibility(View.GONE);
                    } else {
                        view1.setVisibility(View.VISIBLE);
                    }
                    opened1 = !opened1;
                }
            });
            view2 = v.findViewById(R.id.view2);
            view2.setVisibility(View.GONE);
            v.findViewById(R.id.linierTreatmen).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!opened2){
                        view2.setVisibility(View.GONE);
                    } else {
                        view2.setVisibility(View.VISIBLE);
                    }
                    opened2 = !opened2;
                }
            });

            view_production = v.findViewById(R.id.view3);
            view_production.setVisibility(View.GONE);
            v.findViewById(R.id.linierProduction).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!opened3){
                        view_production.setVisibility(View.GONE);
                    }
                    else {
                        view_production.setVisibility(View.VISIBLE);
                    }
                    opened3 = !opened3;
                }
            });
        }
    }
    @NonNull
    @Override
    public ReportByFlockPerDateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // membuat view baru
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.treatment_content_list_row, viewGroup, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
//        ContentPerDateAdapter.ViewHolder vh = new ContentPerDateAdapter.ViewHolder(v);
        return new ReportByFlockPerDateAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.date_record.setText(date.get(i));
        viewHolder.sumPopulation.setText(data_report.get(i).getPopulation().getTotal_bird());
        viewHolder.henday.setText(data_report.get(i).getProduction().getHenday());
        viewHolder.fcr.setText(data_report.get(i).getProduction().getFcr());
        viewHolder.sumTreatment.setText(data_report.get(i).getTreatment().getFeed_intake());
        ViewListPopulation(data_report.get(i).getPopulation());
        ViewListProduction(data_report.get(i).getProduction());
        ViewListTreatment(data_report.get(i).getTreatment());

        ArrayList<String> hatch_date = new ArrayList<String>(data_report.get(i).getAge().keySet());
        ArrayList<Report.Age> age = new ArrayList<Report.Age>(data_report.get(i).getAge().values());

        for (int m =0; m< age.size(); m++){
            ViewListAgeTotal(age.get(m));
        }

    }
    public void ViewListPopulation(Report.Population data){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View populationView = inflater.inflate(R.layout.treatment_population_list_row, null);
        TextView chick_in = (TextView)populationView.findViewById(R.id.chickIn);
        TextView total_mortality = (TextView)populationView.findViewById(R.id.totalMortality);
        TextView total_population = (TextView)populationView.findViewById(R.id.totalPopulation);

        chick_in.setText(data.getChick_in());
        total_mortality.setText(data.getMortality());
        total_population.setText(data.getTotal_bird());
        listPopulation.addView(populationView);
    }
    public void ViewListProduction(Report.Production data){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View production_view = inflater.inflate(R.layout.treatment_production_list_row, null);
        TextView production_egg = (TextView)production_view.findViewById(R.id.production_egg);
        TextView production_weight = (TextView)production_view.findViewById(R.id.production_weight);
        TextView henday = (TextView) production_view.findViewById(R.id.henday);
        TextView henhoused = (TextView) production_view.findViewById(R.id.hen_housed);
        TextView fcr = (TextView) production_view.findViewById(R.id.fcr);
        TextView egg_weight = (TextView) production_view.findViewById(R.id.egg_weight);
        ViewGroup production_egg_row = (ViewGroup) production_view.findViewById(R.id.production_egg_row);
        ViewGroup production_weight_row = (ViewGroup) production_view.findViewById(R.id.production_weight_row);
        final LinearLayout view_production_egg = (LinearLayout) production_view.findViewById(R.id.view_production_egg);
        final LinearLayout view_production_weight = (LinearLayout) production_view.findViewById(R.id.view_production_weight);
        list_egg = (LinearLayout) production_view.findViewById(R.id.production_egg_view);
        list_egg_weight = (LinearLayout) production_view.findViewById(R.id.production_weight_view);

        production_egg.setText(data.getTotal_egg());
        production_weight.setText(data.getTotal_weight());
        henday.setText(data.getHenday());
        henhoused.setText(data.getHenhoused());
        fcr.setText(data.getFcr());
        egg_weight.setText(data.getEgg_weight());

        for (int i =0; i< data.getEgg().size(); i++){
            ViewListEgg(data.getEgg().get(i));
        }
        for (int a =0; a< data.getWeight().size(); a++){
            ViewListWeightEgg(data.getWeight().get(a));
        }

        open_egg = true;
        open_weight = true;
        view_production_egg.setVisibility(View.GONE);
        production_egg_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Layerfarm","Click "+open_egg);

                if (!open_egg){
                    Log.d("Layerfarm","not egg "+open_egg);
                    view_production_egg.setVisibility(View.GONE);

                }
                else {
                    Log.d("Layerfarm","egg "+open_egg);
                    view_production_egg.setVisibility(View.VISIBLE);
                }
                open_egg = !open_egg;
            }
        });
        view_production_weight.setVisibility(View.GONE);
        production_weight_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Layerfarm","Click = "+open_weight);
                if (!open_weight){
                    view_production_weight.setVisibility(View.GONE);
                }
                else {
                    view_production_weight.setVisibility(View.VISIBLE);
                }
                open_weight = !open_weight;
            }
        });

        listProduction.addView(production_view);
    }
    public void ViewListEgg(Report.Egg egg){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View egg_view = inflater.inflate(R.layout.treatment_egg_list_row, null);
        TextView egg_name = (TextView) egg_view.findViewById(R.id.egg_name);
        TextView value = (TextView) egg_view.findViewById(R.id.value);
        egg_name.setText(egg.getDisplay_name());
        value.setText(egg.getQty());
        list_egg.addView(egg_view);
    }
    public void ViewListWeightEgg(Report.Egg egg){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View egg_view = inflater.inflate(R.layout.treatment_egg_list_row, null);
        TextView egg_name = (TextView) egg_view.findViewById(R.id.egg_name);
        TextView value = (TextView) egg_view.findViewById(R.id.value);
        egg_name.setText(egg.getDisplay_name());
        value.setText(egg.getQty());
        list_egg_weight.addView(egg_view);
    }
    public void ViewListTreatment(Report.Treatment data){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View treatmentView = inflater.inflate(R.layout.treatment_treatment_list_row, null);
        // data to populate the RecyclerView with
        String[] items = {"10000000000000000000", "200000000000000000000000", "30000000000000000000", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48"};
        String[] item = data.getMedivac().split("\\|");
        String[] items_feed = data.getFeed().split("\\|");
        // set up the RecyclerView
        RecyclerView recyclerViewmed = (RecyclerView) treatmentView.findViewById(R.id.medicine_items);
        RecyclerView recyclerViewfeed = (RecyclerView) treatmentView.findViewById(R.id.feed_items);
        TextView bodyweight = (TextView) treatmentView.findViewById(R.id.totalBodyWeight);
//        TextView feed = (TextView) treatmentView.findViewById(R.id.totalFeed);
        TextView feed_intake = (TextView) treatmentView.findViewById(R.id.feedInTake);
        int numberOfColumns = 1;
        recyclerViewmed.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
        recyclerViewfeed.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
        adapter = new MedicineAdapter(context, item);
        adapter_feed = new MedicineAdapter(context, items_feed);
//        adapter.setClickListener(this);
        recyclerViewmed.setAdapter(adapter);
        recyclerViewfeed.setAdapter(adapter_feed);
        bodyweight.setText(data.getBw());
//        feed.setText(data.getFeed());
        feed_intake.setText(data.getFeed_intake());
        listTreatment.addView(treatmentView);
    }
    public void ViewListAgeTotal(Report.Age data){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View eggsInf = inflater.inflate(R.layout.treatment_age_list_row, null);
        TextView populationTotal = (TextView) eggsInf.findViewById(R.id.populationTotal);
        TextView ageDate = (TextView) eggsInf.findViewById(R.id.ageDate);
        populationTotal.setText(data.getTotal_bird());
        ageDate.setText(data.getAge());
        
        final String hatch_date = data.getHatch_date();
        final String age = data.getAge();
        final String total_bird = data.getTotal_bird();
        final String chick_in = data.getChick_in();
        final String transfer_in = data.getTransfer_in();
        final String transfer_out = data.getTransfer_out();
        final String mortality = data.getMortality();
        final Report.Mortality mortality1 = data.getMortality1();
        final Report.Mortality mortality2 = data.getMortality2();
        final Report.Mortality mortality3 = data.getMortality3();
        final Report.Mortality mortality4 = data.getMortality4();
        final Report.Mortality mortality5 = data.getMortality5();
        final Report.Mortality chick_out = data.getChick_out();
        eggsInf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, ReportByFlockDetail.class);
                i.putExtra("hatch_date", hatch_date);
                i.putExtra("age", age);
                i.putExtra("total_bird",total_bird);
                i.putExtra("chick_in",chick_in);
                i.putExtra("transfer_in",transfer_in);
                i.putExtra("transfer_out",transfer_out);
                i.putExtra("mortality",mortality);
                i.putExtra("mortality1",mortality1.getValue());
                i.putExtra("mortality2",mortality2.getValue());
                i.putExtra("mortality3",mortality3.getValue());
                i.putExtra("mortality4",mortality4.getValue());
                i.putExtra("mortality5",mortality5.getValue());
                i.putExtra("mortality1Name",mortality1.getName());
                i.putExtra("mortality2Name",mortality2.getName());
                i.putExtra("mortality3Name",mortality3.getName());
                i.putExtra("mortality4Name",mortality4.getName());
                i.putExtra("mortality5Name",mortality5.getName());
                i.putExtra("chick_out",chick_out.getName());
                context.startActivity(i);
            }
        });
        listAgeTotal.addView(eggsInf);
    }
    @Override
    public int getItemCount() {
        return data_report.size();
    }
}
