package com.layerfarm.setting;

import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParameterMember;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.adddata.CreateUserActivity;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserActivationUnitTest {
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String token2;
    private String baseUrl = "http://"+"nmcgyj"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    String nodes = "";
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private BillingClient billingClient;
    List<SkuDetails> mySkuDetailsList;

    @Before
    public void ConnectServer(){
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            System.out.println(tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                System.out.println(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            if (connection != null) {
                                user = connection.getUser();
//                                Log.d("Layerfarm", "uid = " + user.getUid());
                                System.out.println("uid = "+user.getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            System.out.println("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                System.out.println("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public Boolean CreateUser(){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_user";
        ParameterMember.Users args = new ParameterMember.Users();
        ArrayList<String> role = new ArrayList<String>();
        role.add("authenticated user");
        role.add("administrator");
        ArrayList<String> location = new ArrayList<>();
        location.add("22142");
        location.add("22150");

        args.setUid("");
        args.setStatus("0");
        args.setName("Ceni");
        args.setMail("dyahceni1010@gmail.com");
        args.setPass("112233");
        args.setRoles(role);
        args.setLanguage("id");
        args.setLocation(location);

        ParameterMember parameter = new ParameterMember(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ArrayList<String>> call = apiInterfaceJson.createMember(token2, parameter);
        try {
            ArrayList<String> response = call.execute().body();
            return true;
        }catch (Exception e){
            System.out.println("error "+e);
            return false;
        }
    }


//    void querySkuList(String sku) {
//        List skuList = new ArrayList<>();
//        skuList.add(sku);
///*        skuList.add("android.test.purchased");  // prepared by Google
//        skuList.add("android.test.canceled");
//        skuList.add("android.test.refunded");
//        skuList.add("android.test.item_unavailable");*/
//        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
//        //params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
//        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
//        billingClient.querySkuDetailsAsync(params.build(),
//                new SkuDetailsResponseListener() {
//                    @Override
//                    public void onSkuDetailsResponse(BillingResult billingResult,
//                                                     List<SkuDetails> skuDetailsList) {
//                        // Process the result.
//                        StringBuffer resultStr = new StringBuffer("");
//                        int responseCode = billingResult.getResponseCode();
//                        if (responseCode == BillingClient.BillingResponseCode.OK) {
//                            mySkuDetailsList = skuDetailsList;
//                            Log.d("zzz", "mySkuDetailList = "+mySkuDetailsList);
//                            if (skuDetailsList != null) {
//                                for (Object item : skuDetailsList) {
//                                    SkuDetails skuDetails = (SkuDetails) item;
//                                    String sku = skuDetails.getSku();
//                                    String price = skuDetails.getPrice();
//                                    resultStr.append("Sku=" + sku + " Price=" + price + "\n");
//                                }
//                            } else {
//                                resultStr.append("No Sku");
//                            }
//                            Log.d("resultStr"," "+ resultStr);
//                            //textView1.setText(resultStr);
//                        } else {
//                            showResponseCode(responseCode);
//                        }
//                    }
//                });
//    }
//
//
//    void startPurchase(String sku) {
//        Log.d("sku", "sku= " + sku);
//        SkuDetails skuDetails = getSkuDetails(sku);
//        Log.d("sku","sku detail = "+skuDetails);
//        if (skuDetails != null) {
//            BillingFlowParams params = BillingFlowParams.newBuilder()
//                    .setSkuDetails(skuDetails)
//                    .setReplaceSkusProrationMode(BillingFlowParams.ProrationMode.IMMEDIATE_WITH_TIME_PRORATION)
//                    .build();
////            int responseCode = billingClient.launchBillingFlow(params);
//            BillingResult billingResult =  BillingClient.BillingResponse.OK;
//            showResponseCode(billingResult.getResponseCode());
//        }
////        queryPurchaseHistory();
////        ActivatedMember();
//    }
//    void Upgrade(String new_sku, String old_sku) {
//        Log.d("sku", "sku upgrade= " + new_sku);
//        Log.d("sku", "sku old= " + old_sku);
//        SkuDetails newskuDetails = getSkuDetails(new_sku);
//        Log.d("sku","sku upgrade detail = "+newskuDetails);
//        if (newskuDetails != null) {
//
//            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
//                    .setSkuDetails(newskuDetails)
//                    .setOldSku(old_sku)
//                    .setReplaceSkusProrationMode(BillingFlowParams.ProrationMode.IMMEDIATE_WITH_TIME_PRORATION)
//                    .build();
////            int responseCode = billingClient.launchBillingFlow(flowParams);
////            BillingFlowParams params = BillingFlowParams.newBuilder()
////                    .setOldSku(old_sku)
////                    .setReplaceSkusProrationMode(IMMEDIATE_AND_CHARGE_PRORATED_PRICE)
////                    .build();
////            int responseCode = billingClient.launchBillingFlow(params);
//            BillingResult billingResult = billingClient.launchBillingFlow(RegisterMemberAccount.this, flowParams);
//            showResponseCode(billingResult.getResponseCode());
//        }
////        ActivatedMember();
//    }
//
//
//    SkuDetails getSkuDetails(String sku) {
//        SkuDetails skuDetails = null;
//        if(mySkuDetailsList==null){
//            //textView1.setText("Exec [Get Skus] first");
//            Log.d("resultStr", "Exec [Get Skus] first");
//        }else {
//            for (SkuDetails sd : mySkuDetailsList) {
//                if (sd.getSku().equals(sku)) skuDetails = sd;
//            }
//            if (skuDetails == null) {
//                //textView1.setText(sku + " is not found");
//                Log.d("resultStr", " "+ sku + " is not found");
//            }
//        }
//        return skuDetails;
//    }
//
//    String handlePurchase(Purchase purchase) {
//        String stateStr = "error";
//        int purchaseState = purchase.getPurchaseState();
//        if (purchaseState == Purchase.PurchaseState.PURCHASED) {
//            // Grant entitlement to the user.
//            Log.d("zzz","purchased handle purchased");
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date purchaseDate = new Date(purchase.getPurchaseTime());
//            String date = sdf.format(purchaseDate);
//            expired_date = date;
//            ActivatedMember(date);
//            stateStr = "purchased";
//            // Acknowledge the purchase if it hasn't already been acknowledged.
////            ActivatedMember();
//            if (!purchase.isAcknowledged()) {
//                AcknowledgePurchaseParams acknowledgePurchaseParams =
//                        AcknowledgePurchaseParams.newBuilder()
//                                .setPurchaseToken(purchase.getPurchaseToken())
//                                .build();
//                billingClient.acknowledgePurchase(acknowledgePurchaseParams, this);
//            }
//        }else if(purchaseState == Purchase.PurchaseState.PENDING){
//            stateStr = "pending";
//        }else if(purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE){
//            stateStr = "unspecified state";
//        }
//        return stateStr;
//    }
//
//
//    @Override
//    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
//        int responseCode = billingResult.getResponseCode();
//        if(responseCode != BillingClient.BillingResponseCode.OK) {
//            showResponseCode(responseCode);
//        }
//    }
//
//
//    void queryOwned(){
//        member_items = new ArrayList<>();
//        StringBuffer resultStr = new StringBuffer("");
//        Purchase.PurchasesResult purchasesResult
//                = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
//        int responseCode = purchasesResult.getResponseCode ();
//        if(responseCode== BillingClient.BillingResponseCode.OK){
//            resultStr.append("Query Success\n");
//            List<Purchase> purchases = purchasesResult.getPurchasesList();
//            if(purchases.isEmpty()){
//                resultStr.append("Owned Nothing");
//            } else {
//                for (Purchase purchase : purchases) {
//                    resultStr.append(purchase.getSku()).append("\n");
//                    resultStr.append(purchase.getPurchaseState()).append("\n");
//                    resultStr.append(purchase.isAutoRenewing()).append("\n");
//                    resultStr.append(purchase.getPurchaseTime()).append("\n");
//                    if (purchase.isAutoRenewing()){
//                        if (!purchase.getSku().equals("lfm1.0alfa_1")) {
//                            member_items.add(purchase.getSku());
//                        }
//                    }
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                    Date purchaseDate = new Date(purchase.getPurchaseTime());
//                    String date = sdf.format(purchaseDate);
//                    expired_date = date;
//                    Log.d("zzz","purchase date = " +date);
//                }
//            }
//
//            //textView1.setText(resultStr);
//            Log.d("resultStr", " "+ resultStr);
//        }else{
//            showResponseCode(responseCode);
//        }
//    }
//
//
//    void queryPurchaseHistory() {
//        billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS,
//                new PurchaseHistoryResponseListener() {
//                    @Override
//                    public void onPurchaseHistoryResponse(BillingResult billingResult,
//                                                          List<PurchaseHistoryRecord> purchasesList) {
//                        int responseCode = billingResult.getResponseCode();
//                        if (responseCode == BillingClient.BillingResponseCode.OK) {
//                            if (purchasesList == null || purchasesList.size() == 0) {
//                                //textView1.setText("No History");
//                                Log.d("No History", " "+ "No History");
//                            } else {
//                                for (PurchaseHistoryRecord purchase : purchasesList) {
//                                    // Process the result.
//                                    //textView1.setText("Purchase History=" + purchase.toString() + "\n");
//                                    Log.d("No History", " "+ purchase.toString());
//                                }
//                            }
//                        } else {
//                            showResponseCode(responseCode);
//                        }
//                    }
//                });
//    }
//
//    void showResponseCode(int responseCode){
//        switch(responseCode){
//            case BillingClient.BillingResponseCode.OK:
//                // textView1.setText("OK");
//                Log.d("resultStr", " "+ "OK");
//                break;
//            case BillingClient.BillingResponseCode.USER_CANCELED:
//                //textView1.setText("USER_CANCELED");
//                Log.d("resultStr", " "+ "USER CANCELLED");
//                break;
//            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
//                //textView1.setText("SERVICE_UNAVAILABLE");
//                Log.d("resultStr", " "+ "SERVICE_UNAVAILABLE");
//                break;
//            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
//                Log.d("resultStr", " "+ "BILLING_UNAVAILABLE");
//                //textView1.setText("BILLING_UNAVAILABLE");
//                break;
//            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
//                Log.d("resultStr", " "+ "ITEM_UNAVAILABLE");
//                //textView1.setText("ITEM_UNAVAILABLE");
//                break;
//            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
//                Log.d("resultStr", " "+ "DEVELOPER_ERROR");
//                //textView1.setText("DEVELOPER_ERROR");
//                break;
//            case BillingClient.BillingResponseCode.ERROR:
//                Log.d("resultStr", " "+ "ERROR");
//                //textView1.setText("ERROR");
//                break;
//            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
//                Log.d("resultStr", " "+ "ITEM_ALREADY_OWNED");
//                //textView1.setText("ITEM_ALREADY_OWNED");
//                break;
//            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
//                Log.d("resultStr", " "+ "ITEM_NOT_OWNED");
//                //textView1.setText("ITEM_NOT_OWNED");
//                break;
//            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
//                Log.d("resultStr", " "+ "SERVICE_DISCONNECTED");
//                //textView1.setText("SERVICE_DISCONNECTED");
//                break;
//            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
//                Log.d("resultStr", " "+ "FEATURE_NOT_SUPPORTED");
//                //textView1.setText("FEATURE_NOT_SUPPORTED");
//                break;
//        }
//    }
//
//    @Override
//    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
//        StringBuffer resultStr = new StringBuffer("");
//        int billingResultCode = billingResult.getResponseCode();
//        if (billingResultCode == BillingClient.BillingResponseCode.OK
//                && purchases != null) {
//            for (Purchase purchase : purchases) {
//
//                String state = handlePurchase(purchase);
//                String sku = purchase.getSku();
//                resultStr.append(sku).append("\n");
//                resultStr.append(" State=").append(state).append("\n");
//            }
//            //textView1.setText(resultStr);
//            queryOwned();
//            Log.d("resultStr", " "+ resultStr);
//        } else {
//            // Handle error codes.
//            showResponseCode(billingResultCode);
//        }
//    }
}
