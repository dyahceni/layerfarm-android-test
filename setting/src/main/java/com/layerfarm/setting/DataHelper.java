package com.layerfarm.setting;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.layerfarm.layerfarm.DatabaseHelper;



public class DataHelper  {
    //inisialisasi kelas helper
    private DatabaseHelper database;
    private SQLiteDatabase db;

    //Table medication Vaccination
    public static final String TABLE_MEDICATION_VACCINATION = "medication_vaccination";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_VENDOR_NAME = "vendor_name";
    public static final String COLUMN_CAPACITY = "capacity";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_RID = "rid";
    //Table Body Weight
    public static final String TABLE_BODY_WEIGHT = "body_weight";
    public static final String COLUMN_BODY_WEIGHT_ID = "id";
    public static final String COLUMN_DAILY_RECORDING_ID = "daily_recording_id";
    public static final String COLUMN_BODY_WEIGHT = "body_weight";

    //Table egg quality
    public static final String TABLE_EGG_QUALITY = "egg_quality";
    public static final String COLUMN_EGG_ID = "id";
    public static final String COLUMN_EGG_NAME = "name";
    //Table Photo
    public static final String COMMA_SEP = ",";
    public static final String TEXT_TYPE = " TEXT";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String NUMERIC_TYPE = " NUMERIC";
    public static final String LONG_TYPE = " LONG";

    public static final String TABLE_NAME_PHOTO = "recording_photo";
    public static final String COLUMN_ID_RECORDING_PHOTO = "recording_id";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATETIME = "datetime";
    public static final String COLUMN_DESCRIPTION = "description";
    //public static final String PRIMARY_KEY = "PRIMARY KEY (" + COLUMN_TITLE + "," + COLUMN_DATETIME + ")";
    public static final String PRIMARY_KEY = "PRIMARY KEY (" + COLUMN_DATETIME + ")";


    //DBHelper diinstantiasi pada constructor
    public DataHelper(Context context)
    {

        database = new DatabaseHelper(context);
    }

    //membuat sambungan ke database
    public void open() throws SQLException {
        db= database.getWritableDatabase();
    }

    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    //getting all strain from database
    public List<String> getAllStrain()
    {
        this.open();
        List<String> strainlist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=database.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT name FROM strain",null);
        if(cursor.moveToFirst())
        {
            do {
                strainlist.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        this.close();
        return strainlist;
    }

    //getting all location from database
    public List<String> getAllLocation()
    {
        this.open();
        List<String> locationlist=new ArrayList<>();
        //get readable database
        Cursor cursor=db.rawQuery("SELECT * FROM location",null);
        if(cursor.moveToFirst())
        {
            do {
                locationlist.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        this.close();
        return locationlist;
    }

    //getting all location from database
    public List<String> getAllFlock()
    {
        this.open();
        List<String> flockList=new ArrayList<>();
        //get readable database
        Cursor cursor=db.rawQuery("SELECT name FROM flock",null);
        if(cursor.moveToFirst())
        {
            do {
                flockList.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        this.close();
        return flockList;
    }

    //getting all location from database
    public List<String> getAllVendor()
    {
        this.open();
        List<String> vendorList=new ArrayList<>();
        //get readable database
        Cursor cursor=db.rawQuery("SELECT name FROM vendor",null);
        if(cursor.moveToFirst())
        {
            do {
                vendorList.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        this.close();
        return vendorList;
    }

    public Cursor getData(){
        this.open();
        String query = "SELECT * FROM feed";
        Cursor data = db.rawQuery(query, null);
        this.close();
        return data;
    }


    public ArrayList<HashMap<String, String>> getAllMedicationVaccination() {
        this.open();
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM " + TABLE_MEDICATION_VACCINATION;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_ID, cursor.getString(0));
                map.put(COLUMN_PRODUCT_NAME, cursor.getString(1));
                map.put(COLUMN_VENDOR_NAME, cursor.getString(2));
                map.put(COLUMN_CAPACITY, cursor.getString(3));
                map.put(COLUMN_UNIT, cursor.getString(4));
                map.put(COLUMN_TYPE, cursor.getString(5));
                map.put(COLUMN_STATUS, cursor.getString(6));
                map.put(COLUMN_RID, cursor.getString(7));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        Log.e("select sqlite ", "" + wordList);
        this.close();
        return wordList;
    }

    public void insertMedicationVaccination(int id, String product_name, int vendor_id, int capacity, String unit, String type, String status, int rid) {
        this.open();
        String queryValues = "INSERT INTO " + TABLE_MEDICATION_VACCINATION + " (product_name, vendor_id, capacity, unit, type, status,rid) " +
                "VALUES ('" + product_name + "', '" + vendor_id + "', '" + capacity + "' ,'" + unit + "', '" + type + "' , '" + status + "' , '" + rid + "'  )";

        Log.e("insert sqlite ", "" + queryValues);
        db.execSQL(queryValues);
        this.close();
    }

    public void updateMedicationVaccination(int id, String product_name, int vendor_id, int capacity, String unit, String type, String status, int rid) {
        this.open();
        String updateQuery = "UPDATE " + TABLE_MEDICATION_VACCINATION + " SET "
                + COLUMN_PRODUCT_NAME + "='" + product_name + "', "
                + COLUMN_VENDOR_NAME + "='" + vendor_id + "', "
                + COLUMN_CAPACITY + "='" + capacity + "', "
                + COLUMN_UNIT + "='" + unit + "', "
                + COLUMN_TYPE + "='" + type + "', "
                + COLUMN_STATUS + "='" + status + "', "
                + COLUMN_RID + "='" + rid + "'"
                + " WHERE " + COLUMN_ID + "=" + "'" + id + "'";
        Log.e("update sqlite ", updateQuery);
        db.execSQL(updateQuery);
        this.close();
    }

    public void deleteMedicationVaccination(int id) {
        this.open();
        String updateQuery = "DELETE FROM " + TABLE_MEDICATION_VACCINATION + " WHERE " + COLUMN_ID + "=" + "'" + id + "'";
        Log.e("delete sqlite ", updateQuery);
        db.execSQL(updateQuery);
        this.close();
    }

    public void deleteBodyWeight(int id) {
        this.open();

        String updateQuery = "DELETE FROM " + TABLE_BODY_WEIGHT + " WHERE " + COLUMN_ID + "=" + "'" + id + "'";
        Log.e("delete sqlite ", updateQuery);
        db.execSQL(updateQuery);
        this.close();
    }

    public void insertBodyWeight(int daily_recording_id, String body_weight) {
        this.open();
        String queryValues = "INSERT INTO " + TABLE_BODY_WEIGHT + " (daily_recording_id, body_weight) " +
                "VALUES ('" + daily_recording_id + "', '" + body_weight + "')";

        Log.e("insert sqlite ", "" + queryValues);
        db.execSQL(queryValues);
        this.close();

    }

    public boolean insertBodyWeightData(String daily_recording_id, String body_weight) {
        this.open();
        ContentValues contentValues = new ContentValues();

        contentValues.put("daily_recording_id", daily_recording_id);
        contentValues.put("body_weight", body_weight);
        db.insert(TABLE_BODY_WEIGHT, null, contentValues);
        this.close(); //Add this here!
        return true;
    }

    public ArrayList<HashMap<String, String>> getAllBodyWeight() {
        this.open();
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM " + TABLE_BODY_WEIGHT;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_ID, cursor.getString(0));
                map.put(COLUMN_DAILY_RECORDING_ID, cursor.getString(1));
                map.put(COLUMN_BODY_WEIGHT, cursor.getString(2));
                wordList.add(map);
            } while (cursor.moveToNext());
        }

        Log.e("select sqlite ", "" + wordList);

        this.close();
        return wordList;
    }

    public void deleteEggQuality(int id) {
        this.open();
        String updateQuery = "DELETE FROM " + TABLE_EGG_QUALITY + " WHERE " + COLUMN_EGG_ID + "=" + "'" + id + "'";
        Log.e("delete sqlite ", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteFeed(int id) {
        this.open();
        String updateQuery = "DELETE FROM feed WHERE id  =" + "'" + id + "'";
        Log.e("delete feed ", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteFlock(int id) {
        this.open();
        String updateQuery = "DELETE FROM flock WHERE id  =" + "'" + id + "'";
        Log.e("delete flock ", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteLocation(int id) {
        this.open();

        String updateQuery = "DELETE FROM location WHERE id  =" + "'" + id + "'";
        Log.e("delete location ", updateQuery);
        db.execSQL(updateQuery);
        this.close();
    }

    public void deleteMortality(int id) {
        this.open();

        String updateQuery = "DELETE FROM mortality_category WHERE id  =" + "'" + id + "'";
        Log.e("delete mortality ", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteStrain(int id) {
        this.open();

        String updateQuery = "DELETE FROM strain WHERE id  =" + "'" + id + "'";
        Log.e("delete strain ", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteVendor(int id) {
        this.open();

        String updateQuery = "DELETE FROM vendor WHERE id  =" + "'" + id + "'";
        Log.e("delete vendor", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public void deleteBuyer(int id) {
        this.open();

        String updateQuery = "DELETE FROM buyer WHERE id  =" + "'" + id + "'";
        Log.e("delete buyer", updateQuery);
        db.execSQL(updateQuery);
        db.close();
    }

    public ArrayList<HashMap<String, String>> getAllEggQuality() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM " + TABLE_EGG_QUALITY;
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_EGG_ID, cursor.getString(0));
                map.put(COLUMN_EGG_NAME, cursor.getString(1));
                map.put("display_name", cursor.getString(2));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        Log.e("select sqlite ", "" + wordList);
        db.close();
        return wordList;
    }

    public ArrayList<HashMap<String, String>> getAllFeed()
    {
        ArrayList<HashMap<String, String>> feedList;
        feedList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM feed";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("description", cursor.getString(2));
                map.put("status", cursor.getString(3));
                map.put("rid", cursor.getString(4));
                feedList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return feedList;
    }

    public ArrayList<HashMap<String, String>> getAllFlockList()
    {
        ArrayList<HashMap<String, String>> FlockList;
        FlockList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM flock";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("location_id", cursor.getString(2));
                map.put("type", cursor.getString(3));
                map.put("period", cursor.getString(4));
                map.put("status", cursor.getString(5));
                map.put("capacity", cursor.getString(6));
                map.put("rid", cursor.getString(7));
                FlockList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return FlockList;
    }

    public ArrayList<HashMap<String, String>> getAllLocationList()
    {
        ArrayList<HashMap<String, String>> LocationList;
        LocationList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM location order by name asc";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("address", cursor.getString(2));
                map.put("rid", cursor.getString(3));
                LocationList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return LocationList;
    }

    public ArrayList<HashMap<String, String>> getAllMortality()
    {
        ArrayList<HashMap<String, String>> MortalityList;
        MortalityList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM mortality_category";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                MortalityList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return MortalityList;
    }

    public ArrayList<HashMap<String, String>> getAllStrainList()
    {
        ArrayList<HashMap<String, String>> StrainList;
        StrainList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM strain";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                StrainList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return StrainList;
    }

    public ArrayList<HashMap<String, String>> getAllVendorList()
    {
        ArrayList<HashMap<String, String>> VendorList;
        VendorList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM vendor";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("rid", cursor.getString(2));
                VendorList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return VendorList;
    }
    public ArrayList<HashMap<String, String>> getAllBuyerList()
    {
        ArrayList<HashMap<String, String>> VendorList;
        VendorList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM buyer";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("rid", cursor.getString(2));
                map.put("address", cursor.getString(3));
                VendorList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return VendorList;
    }

    //Odoo Query
    //Insert Product to product_template, product_product

    public void insertOdooProduct(String activityName, String nameProduct) {
        this.open();

        //Fill on odoo schema
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);

        //Get category id
        String selectQueryFlock = "SELECT id FROM product_category WHERE name = '" + activityName + "' LIMIT 1";
        Cursor cursorFlok = db.rawQuery(selectQueryFlock, null);
        String strCategory = "";
        if (cursorFlok.moveToFirst())
            strCategory = cursorFlok.getString(cursorFlok.getColumnIndex("id"));
        cursorFlok.close();

        String insertProduct_templateSQL = "INSERT INTO product_template \n" +
                "(warranty," +
                "list_price," +
                "weight, " +
                "sequence, " +
                "color, " +
                "write_uid, " +
                "uom_id," +
                "description_purchase, " +
                "default_code, " +
                "create_date, " +
                "create_uid, " +
                "sale_ok, " +
                "purchase_ok, " +
                "message_last_post," +
                "company_id," +
                "uom_po_id," +
                "description_sale," +
                "description, " +
                "volume, " +
                "write_date," +
                "active," +
                "categ_id, " +
                "name," +
                "rental," +
                "type," +
                "tracking," +
                "location_id," +
                "description_picking," +
                "sale_delay," +
                "warehouse_id," +
                "produce_delay," +
                "sale_line_warn," +
                "track_service," +
                "sale_line_warn_msg," +
                "invoice_policy," +
                "expense_policy," +
                "purchase_line_warn_msg," +
                "purchase_method," +
                "purchase_line_warn) VALUES \n" +
                "(?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?);";
        db.execSQL(insertProduct_templateSQL, new String[]{"0", "0", "0", "1", null, "1", "1", null, null, today, "1", "t", "t", null, "1", "1", null, null, "0", today, "t", strCategory, nameProduct, "f", "consu", "none", null, null, "0", null, "0", "no-message", "manual", null, "order", "no", null, "receive", "no-message"});
        Log.e("insert sqlite ", "" + insertProduct_templateSQL);

        //get last record chickin
        String selectQuery = "SELECT id FROM product_template ORDER BY id DESC LIMIT 1";
        Cursor cursor2 = db.rawQuery(selectQuery, null);
        String strlastProductTemplate = "";
        if (cursor2.moveToFirst())
            strlastProductTemplate = cursor2.getString(cursor2.getColumnIndex("id"));
        cursor2.close();

        //Save to chickin distribution
        String insertDistributionSQL = "INSERT INTO product_product \n" +
                "(create_date, weight, default_code, product_tmpl_id, message_last_post, create_uid, write_uid, barcode, volume, write_date, active) VALUES \n" +
                "(?,  ?, ? ,?,?, ?,  ?, ? ,?,?, ?);";
        db.execSQL(insertDistributionSQL, new String[]{today, "0", null, strlastProductTemplate, null, "1", "1", null, "0", today, "t"});

        db.close();
    }


    //Insert Product to product_template, product_product
    public void insertOdooProductChickin(String nameProduct, String hatchDate, String chickQty, String flockId, String locationId, String flockChickinId, String strainId) {
        this.open();

        //Fill on odoo schema
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);


        String insertstock_moveSQL = "INSERT INTO stock_move \n" +
                "(id," +
                "origin," +
                "create_date," +
                "restrict_partner_id," +
                "product_uom," +
                "price_unit," +
                "product_uom_qty," +
                "procure_method," +
                "product_qty," +
                "partner_id," +
                "priority," +
                "picking_type_id," +
                "location_id," +
                "sequence," +
                "company_id," +
                "note," +
                "state," +
                "ordered_qty," +
                "origin_returned_move_id," +
                "product_packaging," +
                "restrict_lot_id," +
                "date_expected," +
                "procurement_id," +
                "create_uid," +
                "warehouse_id," +
                "inventory_id," +
                "partially_available," +
                "propagate," +
                "move_dest_id," +
                "date," +
                "scrapped," +
                "write_uid," +
                "product_id," +
                "push_rule_id," +
                "name," +
                "split_from," +
                "rule_id," +
                "location_dest_id," +
                "write_date," +
                "group_id," +
                "picking_id," +
                "workorder_id," +
                "consume_unbuild_id," +
                "is_done," +
                "unit_factor," +
                "bom_line_id," +
                "raw_material_production_id," +
                "quantity_done_store," +
                "production_id," +
                "operation_id," +
                "unbuild_id," +
                "to_refund_so," +
                "purchase_line_id," +
                "invoice_line_id) VALUES \n" +
                "(?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        db.execSQL(insertstock_moveSQL, new String[]{null,hatchDate,today, null, "1", "0", chickQty, "make_to_stock", chickQty, "8", "1", "4", flockId, "10", "1", null, "done", chickQty, null, null, null, today, "2", "1", locationId, null, "f", "t", null, today, "f", "1", strainId, null, nameProduct, null, "1", flockChickinId, today, "8", "10", null, null, "t", null, null, null, null, null, null, null, "f", null, null});

        Log.e("insert sqlite ", "" + insertstock_moveSQL);

        String insertstock_quantSQL = "INSERT INTO stock_quant \n" +
                "(id," +
                "create_date," +
                "qty," +
                "propagated_from_id," +
                "package_id," +
                "cost," +
                "lot_id," +
                "location_id," +
                "create_uid," +
                "reservation_id," +
                "company_id," +
                "owner_id," +
                "write_date," +
                "write_uid," +
                "product_id," +
                "packaging_type_id," +
                "negative_move_id," +
                "in_date)  VALUES \n" +
                "(?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?, ?, ? ,?,?);";
        db.execSQL(insertstock_quantSQL, new String[]{null, today, chickQty, null, null, "1", null, flockId, "1", null, "1", null, today, "1", strainId, null, null, today});
        Log.e("insert sqlite ", "" + insertstock_quantSQL);

        db.close();
    }


    //Insert PO to Purchase_Order, Purchase_Order_Line
    public void insertPurchaseOrder(String po_number, String vendor, String currency, String warehouse) {
        this.open();

        //Get flock id
        String selectQueryFlock= "SELECT * FROM flock WHERE name = '"+ warehouse + "' LIMIT 1";
        Cursor cursorFlock = db.rawQuery(selectQueryFlock, null);
        String strFlock = "";
        if(cursorFlock.moveToFirst())
            strFlock  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
        cursorFlock.close();

        //Get vendor id
        String selectQueryVendor= "SELECT * FROM vendor WHERE name = '"+ vendor + "' LIMIT 1";
        Cursor cursorVendor = db.rawQuery(selectQueryVendor, null);
        String strVendor = "";
        if(cursorVendor.moveToFirst())
            strVendor  =  cursorVendor.getString(cursorVendor.getColumnIndex("id"));
        cursorVendor.close();

        //Fill on odoo schema
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);


        String insertpurchase_orderSQL = "INSERT INTO purchase_order \n" +
                "(origin," +
                "create_date," +
                "write_uid," +
                "currency_id," +
                "date_order," +
                "partner_id," +
                "dest_address_id ," +
                "create_uid," +
                "amount_untaxed," +
                "picking_type_id," +
                "message_last_post," +
                "company_id," +
                "name," +
                "amount_tax," +
                "state," +
                "date_approve," +
                "incoterm_id," +
                "payment_term_id," +
                "write_date," +
                "partner_ref," +
                "fiscal_position_id," +
                "amount_total," +
                "invoice_status," +
                "date_planned," +
                "notes," +
                "group_id)  VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        db.execSQL(insertpurchase_orderSQL, new String[]{null,today,"1", currency, today, strVendor, null, "1", "0", strFlock, "1", "4", po_number, "0", "purchase", today, null, null, today, null, null, "0", "to invoice",today, null, null});
        Log.e("insert sqlite ", "" + insertpurchase_orderSQL);

        db.close();
    }

    //Insert PO to Purchase_Order, Purchase_Order_Line
    public void insertPurchaseOrderLine(String price_unit,String product_qty, String product_name, String order_id) {
        this.open();

        Integer tax = (Integer.parseInt(price_unit) * 10)/100;
        Integer subtotal = Integer.parseInt(price_unit) * Integer.parseInt(product_qty);
        Integer total = tax + subtotal;

        String price_tax = String.valueOf(tax);
        String price_subtotal = String.valueOf(subtotal);
        String price_total = String.valueOf(total);
        //Fill on odoo schema
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);


        String insertpurchase_order_lineSQL = "INSERT INTO purchase_order_line \n" +
                "(create_date ," +
                "product_uom," +
                "price_unit," +
                "qty_invoiced," +
                "write_uid," +
                "currency_id," +
                "product_qty," +
                "partner_id," +
                "qty_received," +
                "create_uid," +
                "price_tax," +
                "sequence," +
                "company_id," +
                "state," +
                "account_analytic_id," +
                "order_id," +
                "price_subtotal," +
                "write_date," +
                "product_id," +
                "price_total," +
                "name," +
                "date_planned) VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        db.execSQL(insertpurchase_order_lineSQL, new String[]{today,"1", price_unit, "0","1",null, product_qty, null, product_qty, "1", price_tax, "10", "1","purchase",null,order_id, price_subtotal,today, "1", price_total, product_name, today });
        Log.e("insert sqlite ", "" + insertpurchase_order_lineSQL);

        db.close();
    }

    //getting autonumber
    public String getAutoNumberPO()
    {
        //get autonumber
        this.open();
        String autoNumberPO = "";
        String selectQueryAuto = "SELECT MAX(substr(shop_name,6)) AS nomor FROM shops";
        Cursor cursorAuto = db.rawQuery(selectQueryAuto, null);
        while (cursorAuto.moveToNext()) {
            if (cursorAuto.moveToFirst() == false) {
                autoNumberPO = "PO000001";
            } else {
                cursorAuto.moveToLast();
                int auto_id = cursorAuto.getInt(0) + 1;
                String no = String.valueOf(auto_id);
                int NomorJual = no.length();
                for (int j = 0; j < 6 - NomorJual; j++) {
                    no = "0" + no;
                }
                autoNumberPO = "PO" + no;
            }
        }
        this.close();
        return autoNumberPO;
    }

    public String getAutoNumberDelivery()
    {
        //get autonumber
        this.open();
        String autoNumberde = "";
        String selectQueryAuto = "SELECT MAX(substr(name,10)) AS nomor FROM stock_picking";
        Cursor cursorAuto = db.rawQuery(selectQueryAuto, null);
        while (cursorAuto.moveToNext()) {
            if (cursorAuto.moveToFirst() == false) {
                autoNumberde = "WH/IN/000001";
            } else {
                cursorAuto.moveToLast();
                int auto_id = cursorAuto.getInt(0) + 1;
                String no = String.valueOf(auto_id);
                int NomorJual = no.length();
                for (int j = 0; j < 6 - NomorJual; j++) {
                    no = "0" + no;
                }
                autoNumberde = "WH/IN/" + no;
            }
        }
        this.close();
        return autoNumberde;
    }




    public void insertRes_partner(String name) {
        this.open();
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);


        String insert_rest_partnerSQL = "INSERT INTO res_partner \n" +
                "(name," +
                "company_id ," +
                "comment," +
                "website," +
                "create_date," +
                "color," +
                "active," +
                "street," +
                "supplier," +
                "city," +
                "display_name," +
                "zip," +
                "title," +
                "country_id," +
                "commercial_company_name," +
                "parent_id," +
                "company_name," +
                "employee," +
                "ref," +
                "email," +
                "is_company," +
                "function," +
                "lang," +
                "fax," +
                "street2," +
                "barcode," +
                "phone," +
                "write_date," +
                "date," +
                "tz," +
                "write_uid," +
                "customer," +
                "create_uid," +
                "credit_limit," +
                "user_id," +
                "mobile," +
                "type," +
                "partner_share," +
                "vat," +
                "state_id," +
                "commercial_partner_id," +
                "notify_email," +
                "message_last_post," +
                "opt_out," +
                "message_bounce," +
                "signup_type," +
                "signup_expiration," +
                "signup_token," +
                "picking_warn_msg," +
                "picking_warn," +
                "team_id," +
                "debit_limit," +
                "last_time_entries_checked," +
                "invoice_warn_msg," +
                "invoice_warn," +
                "sale_warn," +
                "sale_warn_msg," +
                "purchase_warn," +
                "purchase_warn_msg) VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        db.execSQL(insert_rest_partnerSQL, new String[]{name,"1", null, null, today, null, null, null, null, null, null, null,null,null,null, null,null, null, null, null, null , null, null, null, null, null, null, null,null,null,null, null,null, null, null, null, null ,null, null, null, null, null, null, null, null,null,null,null, null,null, null, null, null, null});
        Log.e("insert sqlite ", "" + insert_rest_partnerSQL);

        db.close();
    }

    /*
    public void insertstock_picking(String name) {
        SQLiteDatabase database = this.getWritableDatabase();

        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String today = formatter.format(todayDate);


        String insert_rest_partnerSQL = "INSERT INTO stock_warehouse \n" +
                "(code," +
                "create_date," +
                "lot_stock_id," +
                "wh_pack_stock_loc_id," +
                "reception_route_id," +
                "pick_type_id," +
                "crossdock_route_id," +
                "partner_id," +
                "create_uid," +
                "delivery_route_id," +
                "wh_input_stock_loc_id," +
                "company_id," +
                "reception_steps," +
                "delivery_steps," +
                "view_location_id," +
                "wh_qc_stock_loc_id," +
                "default_resupply_wh_id," +
                "pack_type_id," +
                "wh_output_stock_loc_id," +
                "write_uid," +
                "write_date," +
                "active," +
                "mto_pull_id," +
                "name," +
                "in_type_id," +
                "out_type_id," +
                "int_type_id," +
                "buy_pull_id," +
                "buy_to_resupply) VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        String code = name.toUpperCase();
        String kode = code.replaceAll("\\s+", "");

        database.execSQL(insert_rest_partnerSQL, new String[]{kode,today, null, null, null, null, null, null, null, null, null, null,null,null,null, null,null, null, null, null, null , null, name, null, null, null, null, null,null});

        String insert_stocklocationASQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationASQL, new String[]{today,"1", "Physical Locations/" + kode, "view", kode});


        //get warehouse id
        String selectQueryPurchase = "SELECT id FROM stock_location ORDER BY id DESC LIMIT 1";
        Cursor cursor = database.rawQuery(selectQueryPurchase, null);
        String strloc = "";
        if (cursor.moveToFirst())
            strloc = cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();


        //insert to stock location

        String insert_stocklocationPackingSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationPackingSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Packing Zone","internal","Packing Zone"});

        String insert_stocklocationInputSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationInputSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Input","internal","Input"});

        String insert_stocklocationQualitySQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationQualitySQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Quality Control","internal","Quality Control"});

        String insert_stocklocationStockSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationStockSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Stock","internal","Stock"});

        String insert_stocklocationOutputSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        database.execSQL(insert_stocklocationOutputSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Output","internal","Output"});


        String insert_stocklocationSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(DATETIME('now','localtime'),strloc,'Physical Locations/'+ kode +'/Packing Zone','internal','Packing Zone')," +
                "(DATETIME('now','localtime'),strloc,'Physical Locations/'+ kode +'/Input','internal','Input')," +
                "(DATETIME('now','localtime'),strloc,'Physical Locations/'+ kode +'/Quality Control','internal','Quality Control')," +
                "(DATETIME('now','localtime'),strloc,'Physical Locations/'+ kode +'/Stock','internal','Stock')," +
                "(DATETIME('now','localtime'),strloc,'Physical Locations/'+ kode +'/Output','internal','Output');";

        database.execSQL(insert_stocklocationSQL, new String[]{kode,today, null, null});

        Log.e("insert sqlite ", "" + insert_rest_partnerSQL);


        database.close();
    }

    */

    public void insertStock_warehouse(String name) {
        this.open();
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
        String today = formatter.format(todayDate);


        String insert_rest_partnerSQL = "INSERT INTO stock_warehouse \n" +
                "(code," +
                "create_date," +
                "lot_stock_id," +
                "wh_pack_stock_loc_id," +
                "reception_route_id," +
                "pick_type_id," +
                "crossdock_route_id," +
                "partner_id," +
                "create_uid," +
                "delivery_route_id," +
                "wh_input_stock_loc_id," +
                "company_id," +
                "reception_steps," +
                "delivery_steps," +
                "view_location_id," +
                "wh_qc_stock_loc_id," +
                "default_resupply_wh_id," +
                "pack_type_id," +
                "wh_output_stock_loc_id," +
                "write_uid," +
                "write_date," +
                "active," +
                "mto_pull_id," +
                "name," +
                "in_type_id," +
                "out_type_id," +
                "int_type_id," +
                "buy_pull_id," +
                "buy_to_resupply) VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        String code = name.toUpperCase();
        String kode = code.replaceAll("\\s+", "");

        db.execSQL(insert_rest_partnerSQL, new String[]{kode,today, null, null, null, null, null, null, null, null, null, null,null,null,null, null,null, null, null, null, null , null, null, name, null, null, null, null,null});

        String insert_stocklocationASQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        db.execSQL(insert_stocklocationASQL, new String[]{today,"1", "Physical Locations/" + kode, "view", kode});


        //get warehouse id
        String selectQueryPurchase = "SELECT id FROM stock_warehouse ORDER BY id DESC LIMIT 1";
        Cursor cursorWarehouse = db.rawQuery(selectQueryPurchase, null);
        String strloc = "";
        if (cursorWarehouse.moveToFirst())
            strloc = cursorWarehouse.getString(cursorWarehouse.getColumnIndex("id"));
        cursorWarehouse.close();


        //insert to stock location
        String insert_stocklocationPackingSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        db.execSQL(insert_stocklocationPackingSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Packing Zone","internal","Packing Zone"});

        String insert_stocklocationInputSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        db.execSQL(insert_stocklocationInputSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Input","internal","Input"});

        String insert_stocklocationQualitySQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        db.execSQL(insert_stocklocationQualitySQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Quality Control","internal","Quality Control"});

        String insert_stocklocationStockSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";

        db.execSQL(insert_stocklocationStockSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Stock","internal","Stock"});

        String insert_stocklocationOutputSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";
        db.execSQL(insert_stocklocationOutputSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Output","internal","Output"});

        String insert_stocklocationFarmSQL = "INSERT INTO stock_location \n" +
                "(create_date, location_id, complete_name,usage,name) VALUES \n" +
                "(?,?,?,?,?);";
        db.execSQL(insert_stocklocationFarmSQL, new String[]{today,strloc,"Physical Locations/"+ kode +"/Farm","internal","Farm"});


        //Get id for stock and ongoing
        String selectStockPickingTypeStock = "SELECT id FROM stock_location WHERE complete_name LIKE '%/" + kode +"/Stock' ORDER BY id DESC LIMIT 1";
        Cursor cursorTypeStock = db.rawQuery(selectStockPickingTypeStock, null);
        String strTypeStock = "";
        if(cursorTypeStock != null && cursorTypeStock.moveToFirst()) {
            strTypeStock = cursorTypeStock.getString(cursorTypeStock.getColumnIndex("id"));
            cursorTypeStock.close();
            Log.e("strTypeStock ", "" + strTypeStock);
        }

        String selectStockPickingTypePackingZone = "SELECT id FROM stock_location WHERE complete_name LIKE '%" + kode +"/Packing Zone' ORDER BY id DESC LIMIT 1";
        Cursor cursorTypePackingZone = db.rawQuery(selectStockPickingTypePackingZone, null);
        String strTypePackingZone = "";

        if(cursorTypePackingZone != null && cursorTypePackingZone.moveToFirst()) {
            strTypePackingZone = cursorTypePackingZone.getString(cursorTypePackingZone.getColumnIndex("id"));
            cursorTypePackingZone.close();
            Log.e("strTypeStock ", "" + strTypeStock);
        }

        //insert to stock picking type
        String insert_stockPickingReceiptsSQL = "INSERT INTO stock_picking_type \n" +
                "(code, create_date,default_location_dest_id,warehouse_id, name, default_location_src_id) VALUES \n" +
                "(?,?,?,?,?,?);";
        db.execSQL(insert_stockPickingReceiptsSQL, new String[]{"incoming",today,strTypeStock,strloc,"Receipts", null});


        String insert_stockPickingPackSQL = "INSERT INTO stock_picking_type \n" +
                "(code, create_date,default_location_dest_id,warehouse_id, name, default_location_src_id) VALUES \n" +
                "(?,?,?,?,?,?);";
        db.execSQL(insert_stockPickingPackSQL, new String[]{"internal",today,strTypeStock,strloc,"Pack", strTypePackingZone});


        String insert_stockPickingPickSQL = "INSERT INTO stock_picking_type \n" +
                "(code, create_date,default_location_dest_id,warehouse_id, name, default_location_src_id) VALUES \n" +
                "(?,?,?,?,?,?);";
        db.execSQL(insert_stockPickingPickSQL, new String[]{"internal",today,strTypePackingZone,strloc,"Pick", strTypeStock});


        String insert_stockPickingDeliveryOrdersSQL = "INSERT INTO stock_picking_type \n" +
                "(code, create_date,default_location_dest_id,warehouse_id, name, default_location_src_id) VALUES \n" +
                "(?,?,?,?,?,?);";
        db.execSQL(insert_stockPickingDeliveryOrdersSQL, new String[]{"outgoing",today,null,strloc,"Delivery Orders", strTypeStock});


        String insert_stockPickingInternalTransferSQL = "INSERT INTO stock_picking_type \n" +
                "(code, create_date,default_location_dest_id,warehouse_id, name, default_location_src_id) VALUES \n" +
                "(?,?,?,?,?,?);";
        db.execSQL(insert_stockPickingInternalTransferSQL, new String[]{"internal",today,strTypeStock,strloc,"Internal Transfers", strTypeStock});


        db.close();
    }



    public void insertStock_PickingData(String po_name, String location) {
        this.open();
        String deliveryNumber = getAutoNumberDelivery();
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String today = formatter.format(todayDate);


        String code = location.toUpperCase();
        String kode = code.replaceAll("\\s+", "");

        String insert_rest_partnerSQL = "INSERT INTO stock_picking \n" +
                "(origin," +
                "date_done," +
                "location_id," +
                "picking_type_id," +
                "partner_id," +
                "move_type," +
                "state," +
                "backorder_id," +
                "name," +
                "location_dest_id) VALUES \n" +
                "(?,?,?,?,?,?,?,?,?,?);";

        db.execSQL(insert_rest_partnerSQL, new String[]{po_name,today, "8","1", null, "direct","done",null,deliveryNumber,"15"});

        db.close();
    }

    public ArrayList<HashMap<String, String>> getAllChickinList()
    {
        ArrayList<HashMap<String, String>> ChickinList;
        ChickinList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM chick_in";
        this.open();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("chick_in_type", cursor.getString(1));
                map.put("hatch_date", cursor.getString(2));
                ChickinList.add(map);
            } while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return ChickinList;
    }

}