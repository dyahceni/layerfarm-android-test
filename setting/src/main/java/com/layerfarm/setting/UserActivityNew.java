package com.layerfarm.setting;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.layerfarm.setting.adapter.AdapterUserNew;
import com.layerfarm.setting.adddata.CreateUserActivity;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterMember;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.UserMember;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivityNew extends AppCompatActivity {
    Toolbar toolbar ;
    TextView toolbar_title;
    ListView listView;
    LinearLayout add_member;
    List<UserMember.Users> itemList = new ArrayList<UserMember.Users>();
    ProgressDialog progressDoalog;
    UserMember member = new UserMember();
    ArrayList<UserMember.Users> users = new ArrayList<>();
    LinkedHashMap<Integer, String> house = new LinkedHashMap<>();
    String default_location = "";
    AdapterUserNew adapter;
    LinearLayout linearLayout;
    EditText search;
    FloatingActionButton fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_user_new);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(R.string.member);

        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (getSettingBoolean(UserActivity.this, "showads", true)) {
//                    if (mHelper != null) {
//                        purchaseRemoveAds();
//                    }
//                }
//                Intent intent = new Intent(UserActivity.this, CreateUserActivity.class);
                LinkedHashMap<Integer, String> location = house;
                Intent intent = new Intent(UserActivityNew.this, CreateUserActivity.class);
//                intent.putExtra("user", (Serializable) users);
                intent.putExtra("location", (Serializable) location);
                intent.putExtra("default_location", (Serializable) default_location);
                startActivity(intent);
            }
        });

        listView = (ListView) findViewById(R.id.list_view_user);
        add_member = (LinearLayout) findViewById(R.id.add_member);
        add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkedHashMap<Integer, String> location = house;
                Intent intent = new Intent(UserActivityNew.this, CreateUserActivity.class);
//                intent.putExtra("user", (Serializable) users);
                intent.putExtra("location", (Serializable) location);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserMember.FieldManagedLocation item;
                ParameterMember.Users users = new ParameterMember.Users();
                UserMember.Users items = (UserMember.Users)adapter.getItem(position);
                JsonElement data = items.getFieldManagedLocation();
                item = new UserMember.FieldManagedLocation();
                try{
                    if(data.isJsonArray()){
                        //Array
                        //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is array");
                        item = new UserMember.FieldManagedLocation();
                    } else {
                        //Object
                        //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is object");
                        Type type = new TypeToken<UserMember.FieldManagedLocation>() {
                        }.getType();
                        item = new Gson().fromJson(data, type);
                    }
                }catch (Exception e){}
                ArrayList<String> loc = new ArrayList<>();
                if (!item.getUnd().isEmpty()){
                    for (int i =0; i< item.getUnd().size(); i++) {
                        loc.add(item.getUnd().get(i).getTarget_id());
                    }
                }
                users.setUid(items.getUid());
                users.setStatus(items.getStatus());
                users.setRoles(new ArrayList<String>(items.getRoles().values()));
                users.setPass(items.getPass());
                users.setMail(items.getMail());
                users.setName(items.getName());
                users.setLanguage(items.getLanguage());
                users.setLocation(loc);
                LinkedHashMap<Integer, String> location = house;
                Intent intent = new Intent(UserActivityNew.this, CreateUserActivity.class);
                intent.putExtra("user", (Serializable) users);
                intent.putExtra("location", (Serializable) location);
                startActivity(intent);
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                UserActivityNew.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        Sync();
    }
    @Override
    protected void onResume() {
        super.onResume();
        member = new UserMember();
        itemList.clear();
        Sync();
//        itemList.clear();
//        getAllData();
    }
    public void Activate(View view) {
        Intent intent = new Intent(UserActivityNew.this, RegisterMemberAccount.class);
        startActivity(intent);
    }
    public void close(View view){
        finish();
    }
    public void Sync(){
        itemList.clear();
        progressDoalog = new ProgressDialog(UserActivityNew.this);
//        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        member = new UserMember();
        String module = "layerfarm_android";
        String function_name = "layerfarm_get_user";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<UserMember> call = apiInterfaceJson.getMember(token2, parameter);

        call.enqueue(new Callback<UserMember>() {

            @Override
            public void onResponse(Call<UserMember> call, Response<UserMember> response) {
                member = response.body();

                Log.d("Layerfarm", "data = "+member.getHouse());
                member.getUsers().remove("root");
                users = member.getUsers();
                house = member.getHouse();
                default_location = member.getDefault_location();
//                for (int i =0; i< users.size() ; i++){
//                    UserMember.Users user = new UserMember.Users();
//                    user.setFieldManagedLocation(users.get(i).getFieldManagedLocation());
//                    user.setLanguage(users.get(i).getLanguage());
//                    user.setMail(users.get(i).getMail());
//                    user.setName(users.get(i).getName());
//                    user.setPass(users.get(i).getPass());
//                    user.setRoles(users.get(i).getRoles());
//                    user.setStatus(users.get(i).getStatus());
//                    user.setUid(users.get(i).getUid());
//                    Log.d("zzz"," name = "+users.get(i).getName());
////                    list_all.add(user);
////                    if (users.get(i).getStatus().equalsIgnoreCase("1")){
////                        list_active.add(user);
////                    }
////                    else
////                        list_inactive.add(user);
//                }
//                if (users.size() > items){
//                    fab.setVisibility(View.GONE);
//                }
//                all.setHouse(house);
//                all.setUsers(list_all);
//                active.setHouse(house);
//                active.setUsers(list_active);
//                inactive.setHouse(house);
//                inactive.setUsers(list_inactive);

                itemList = member.getUsers();
                adapter = new AdapterUserNew(UserActivityNew.this, member);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.d("zzz","Response OK");
                progressDoalog.dismiss();
//                mTabLayout.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.VISIBLE);
                search.setText("");

            }

            @Override
            public void onFailure(Call<UserMember> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                progressDoalog.dismiss();
                finish();
            }
        });
    }
}
