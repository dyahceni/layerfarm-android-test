package com.layerfarm.setting.adddata;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import com.layerfarm.setting.DateWheel.DatePickerPopWin;
import com.layerfarm.layerfarm.model.ParameterCreateFlock;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateFlockActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText flockNameEditText, capacityEditText, effective_dateEditText;
    private Spinner allLocation;
    RadioGroup radio_group_type,radio_group_period,radio_status;
    RadioButton radio_floor,radio_cage,radio_free_range,radio_grower,radio_layer,radio_active,radio_inactive;
    String type_flock = null;
    String flock_period = null;
    String flock_status = null;
    private String id,name,location_id,capacity,rid;
    HashMap<String, String> list_location = new LinkedHashMap<>();
    HashMap<String, String> location_map = new LinkedHashMap<>();
    List<String> lables;
    String[] status = {"Active", "Inactive"};
    String[] type = {"Cage", "Floor", "Free Range"};
    String[] period = {"Grower", "Layer"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_flock);


        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        //Spinner Location
        allLocation=(Spinner)findViewById(R.id.spLocation);
        flockNameEditText = (EditText) findViewById(R.id.flockNameEditText);
        effective_dateEditText = (EditText) findViewById(R.id.effective_date);
        capacityEditText = (EditText) findViewById(R.id.capacityEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        radio_group_type = (RadioGroup) findViewById(R.id.radio_group_type);
        radio_group_period = (RadioGroup) findViewById(R.id.radio_group_period);
        radio_status = (RadioGroup) findViewById(R.id.radio_status);

        radio_floor = (RadioButton) findViewById(R.id.radio_floor);
        radio_cage = (RadioButton) findViewById(R.id.radio_cage);
        radio_free_range = (RadioButton) findViewById(R.id.radio_free_range);
        radio_grower = (RadioButton) findViewById(R.id.radio_grower);
        radio_layer = (RadioButton) findViewById(R.id.radio_layer);
        radio_active = (RadioButton) findViewById(R.id.radio_active);
        radio_inactive = (RadioButton) findViewById(R.id.radio_inactive);

        radio_active.setChecked(true);

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            list_location = ListLocation.getInstance().getList_location();
        }
//        list_location = ((FlockActivity) getApplication().getApplicationContext()).getList_location();
        if (!list_location.isEmpty() && list_location != null){
            location_map = list_location;
        }
//        loadFlockSpinnerData();
        Collection<String> lables_col = location_map.values();
        lables = new ArrayList<String>(lables_col);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        allLocation.setAdapter(dataAdapter);
        registerEvents();

        effective_dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(CreateFlockActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        effective_dateEditText.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(CreateFlockActivity.this);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (flockNameEditText.getText().toString().isEmpty()) {
                    flockNameEditText.setError("Please fill the data");
                    valid = false;
                }else if (capacityEditText.getText().toString().isEmpty()){
                    capacityEditText.setError("Please fill the data");
                    valid = false;
                }else if(radio_group_type.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();
                }else if(radio_group_period.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();
                }else if(radio_status.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();
                }
                else if(effective_dateEditText.getText().toString().isEmpty()){
                    effective_dateEditText.setError("Please fill the data!");
                }
                else {
                    //saveFlock();
                    create_layerfarm_flock();
                }
            }
        });
    }

    private void loadFlockSpinnerData(){
        // database handler
        DataHelper db = new DataHelper(getApplicationContext());

        // Spinner Drop down elements
        List<String> lables = db.getAllLocation();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        allLocation.setAdapter(dataAdapter);
    }

    private void registerEvents() {
        radio_group_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_floor) {
                    type_flock = "Floor";
                } else if (checkedId == R.id.radio_cage) {
                    type_flock = "Cage";
                }else if (checkedId == R.id.radio_free_range) {
                    type_flock = "Free Range";
                }
            }

        });


        radio_group_period.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_grower) {
                    flock_period = "Grower";
                } else if (checkedId == R.id.radio_layer) {
                    flock_period = "Layer";
                }
            }

        });

        radio_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    flock_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    flock_status = "Inactive";
                }
            }

        });


    }


    private void create_layerfarm_flock(){
        //Get location id
        String location = allLocation.getSelectedItem().toString();
        String strLocation = getKey(location_map, location);
        // TODO Auto-generated method stub

        EditText a = (EditText) findViewById(R.id.flockNameEditText);
        String flock_name = (String)(a.getText().toString());

        EditText b = (EditText) findViewById(R.id.capacityEditText);
        String flock_capacity = (String)(b.getText().toString());

        int radioButtonID_status = radio_status.getCheckedRadioButtonId();
        View radioButton_status = radio_status.findViewById(radioButtonID_status);
//        Log.d("zzz","radio "+radioButton);
        int idx_status = radio_status.indexOfChild(radioButton_status);
        RadioButton r_status = (RadioButton) radio_status.getChildAt(idx_status);
//        String selectedtext_status = r_status.getText().toString();
        String selectedtext_status = status[idx_status];
        String effctv_date = effective_dateEditText.getText().toString();

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_flock";

        ParameterCreateFlock.Arguments args = new ParameterCreateFlock.Arguments("",flock_name,strLocation,type_flock,flock_period,selectedtext_status,flock_capacity, effctv_date);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateFlock parameter = new ParameterCreateFlock(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_flock(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                //SubmitDialog();
                Toast.makeText(getApplicationContext(), "Create Flock/House Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
                Toast.makeText(getApplicationContext(), "Create Flock/House Successfully", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}