package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import com.layerfarm.layerfarm.DatabaseHelper;


public class CreateEqqQualityActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper db;
    Button saveButton;
    EditText eqqNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_eqq_quality);


        dbHelper = new DataHelper(this);
        db = new DatabaseHelper(this);
        eqqNameEditText = (EditText) findViewById(R.id.eqqNameEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            String activityName = getIntent().getStringExtra("egg");
        //Toast.makeText(CreateEqqQualityActivity.this, "nama activity = " + getIntent().getStringExtra("EqqQualityActivity"), Toast.LENGTH_SHORT).show();

            @Override
            public void onClick(View arg0) {

                boolean valid=true;
                if (eqqNameEditText.getText().toString().isEmpty()) {
                    eqqNameEditText.setError("Please fill the data");
                    valid = false;
                }
                    else{
                        // TODO Auto-generated method stub
                        SQLiteDatabase dbase = db.getWritableDatabase();
                        dbase.execSQL("insert into egg_quality(name) values('" +
                                eqqNameEditText.getText().toString() + "')");

                    dbHelper.insertOdooProduct(activityName,eqqNameEditText.getText().toString());

                    Toast.makeText(getApplicationContext(), "Create Egg Quality Name Successfully", Toast.LENGTH_LONG).show();
                        //EqqQualityActivity.ma.RefreshListEqq();
                        finish();
                    }
            }
        });
    }

}