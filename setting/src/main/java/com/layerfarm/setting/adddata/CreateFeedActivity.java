package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.ParameterCreateFeed;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.layerfarm.layerfarm.DatabaseHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFeedActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper db;
    Button saveButton;
    EditText feedNameEditText, descriptionEditText;
    private RadioGroup radioGroup;
    private RadioButton activeRadioButton, inactiveRadioButton;
//    String status = null;
    String [] status = {"Active", "Inacative"};
    String feed_status = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_feed);

        String activityName = getIntent().getStringExtra("feed");
        Toast.makeText(CreateFeedActivity.this, "nama activity = " + getIntent().getStringExtra("EqqQualityActivity"), Toast.LENGTH_SHORT).show();


        dbHelper = new DataHelper(this);
        db = new DatabaseHelper(this);
        radioGroup = (RadioGroup) findViewById(R.id.radio_status);
        activeRadioButton = (RadioButton) findViewById(R.id.radio_active);
        inactiveRadioButton = (RadioButton) findViewById(R.id.radio_inactive);

        activeRadioButton.setChecked(true);

        feedNameEditText = (EditText) findViewById(R.id.feedNameEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        registerEvents();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                boolean valid=true;
                if (feedNameEditText.getText().toString().isEmpty()) {
                    feedNameEditText.setError("Please fill the data");
                    valid = false;
                }else if (descriptionEditText.getText().toString().isEmpty()){
                    descriptionEditText.setError("Please fill the data");
                    valid = false;
                }else if(radioGroup.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();
                }
                else {
                    // TODO Auto-generated method stub

                    create_layerfarm_feed();
                  /*  SQLiteDatabase dbase = db.getWritableDatabase();
                    dbase.execSQL("insert into feed(name,description,status) values('" +
                            feedNameEditText.getText().toString() + "','" +
                            descriptionEditText.getText().toString() + "','" +
                            status + "')");

                    dbHelper.insertOdooProduct("feed",feedNameEditText.getText().toString());*/

//                    Toast.makeText(getApplicationContext(), "Create Location Successfully", Toast.LENGTH_LONG).show();
//                    //FeedActivity.ma.RefreshListFeed();
//                    finish();
                }
            }
        });


    }

    private void registerEvents() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    feed_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    feed_status = "Inactive";
                }
            }

        });

    }

    private void create_layerfarm_feed(){

        EditText a = (EditText) findViewById(R.id.feedNameEditText);
        String feed_name = (String)(a.getText().toString());

        EditText b = (EditText) findViewById(R.id.descriptionEditText);
        String farm_desc = (String)(b.getText().toString());

        int radioButtonID_status = radioGroup.getCheckedRadioButtonId();
        View radioButton_status = radioGroup.findViewById(radioButtonID_status);
//        Log.d("zzz","radio "+radioButton);
        int idx_status = radioGroup.indexOfChild(radioButton_status);
        String selectedtext_status = status[idx_status];

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_feed";
        Log.d("zzz","status = "+status);
        Log.d("zzz","status = "+selectedtext_status);

        ParameterCreateFeed.Arguments args = new ParameterCreateFeed.Arguments("", feed_name,farm_desc, selectedtext_status);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateFeed parameter = new ParameterCreateFeed(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_feed(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                //SubmitDialog();
                Toast.makeText(getApplicationContext(), "Create Feed Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
                Toast.makeText(getApplicationContext(), "Create Feed Successfully", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
}