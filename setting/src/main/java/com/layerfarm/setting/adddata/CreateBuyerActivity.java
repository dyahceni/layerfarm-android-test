package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateBuyerActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText buyerNameEditText, buyerAddressEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_buyer);

//        dbHelper = new DataHelper(this);
//        dbase = new DatabaseHelper(this);
        buyerNameEditText = (EditText) findViewById(R.id.buyerNameEditText);
        buyerAddressEditText = (EditText) findViewById(R.id.buyerAddressEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (buyerNameEditText.getText().toString().isEmpty()) {
                    buyerNameEditText.setError("Please fill the data");
                    valid = false;
                }else {
                    // TODO Auto-generated method stub
                    create("",buyerNameEditText.getText().toString(),buyerAddressEditText.getText().toString(),"");
//                    SQLiteDatabase db = dbase.getWritableDatabase();
//                    db.execSQL("insert into buyer(name) values('" +
//                            buyerNameEditText.getText().toString() + "')");
//
//                    dbHelper.insertRes_partner(buyerNameEditText.getText().toString());
//                    Toast.makeText(getApplicationContext(), "Create Buyer Successfully", Toast.LENGTH_LONG).show();
                    //VendorActivity.ma.RefreshListVendor();

                }
            }
        });
    }
    public void create(String rid, final String buyer_name, String address, final String idx){
        String module = "selling_price";
        String function_name = "selling_price_create_buyer";
        String[] args = {rid, buyer_name, address};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createBuyer(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] buyerCreate = response.body();

//                SQLiteDatabase db = dbase.getWritableDatabase();
//                db.execSQL("insert into buyer(name) values('" +
//                        buyer_name + "')");;
//                dbHelper.insertRes_partner(buyer_name);
                Toast.makeText(getApplicationContext(), "Create Buyer Successfully "+buyerCreate, Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Create Buyer Failed", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}