package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import com.layerfarm.layerfarm.DatabaseHelper;


public class CreateMortalityActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText mortalityNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_mortality);

        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        mortalityNameEditText = (EditText) findViewById(R.id.mortalityNameEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (mortalityNameEditText.getText().toString().isEmpty()) {
                    mortalityNameEditText.setError("Please fill the data");
                    valid = false;
                }else {
                    // TODO Auto-generated method stub
                    SQLiteDatabase db = dbase.getWritableDatabase();
                    db.execSQL("insert into mortality_category(name) values('" +
                            mortalityNameEditText.getText().toString() + "')");
                    Toast.makeText(getApplicationContext(), "Create Mortality Category Successfully", Toast.LENGTH_LONG).show();
                    //MortalityActivity.ma.RefreshListMortality();
                    finish();
                }
            }
        });
    }

}