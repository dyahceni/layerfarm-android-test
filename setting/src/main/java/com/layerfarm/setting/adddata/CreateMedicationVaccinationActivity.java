package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateMedicationVaccinationActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText productNameEditText, capacityEditText;
    private Spinner allVendor, allUnit;
    RadioGroup radio_group_type,radio_status;
    RadioButton radio_medicine,radio_vaccine,radio_vitamin,radio_probiotic,radio_disinfectan,radio_active,radio_inactive;
    String type = null;
    String med_status = null;
    String[] unit={"Vial","Litre","Mililitre","Kg","Gram" ,"Miligram","Bottle","Dozen"};
    HashMap<String, String> vendor_map;
    String[] status = {"Active", "Inactive"};
    List<String> lables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_medication_vaccination);
        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);

        //Spinner Location
        allVendor=(Spinner)findViewById(R.id.spVendor);

        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
        capacityEditText = (EditText) findViewById(R.id.capacityEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        radio_group_type = (RadioGroup) findViewById(R.id.radio_group_type);
        radio_status = (RadioGroup) findViewById(R.id.radio_status);
        radio_group_type.clearCheck();
//        radio_status.clearCheck();

        radio_medicine = (RadioButton) findViewById(R.id.radio_medicine);
        radio_vaccine = (RadioButton) findViewById(R.id.radio_vaccine);
        radio_vitamin = (RadioButton) findViewById(R.id.radio_vitamin);
        radio_probiotic = (RadioButton) findViewById(R.id.radio_probiotic);
        radio_disinfectan = (RadioButton) findViewById(R.id.radio_disinfectan);
        radio_active = (RadioButton) findViewById(R.id.radio_active);
        radio_inactive = (RadioButton) findViewById(R.id.radio_inactive);
        saveButton = (Button) findViewById(R.id.saveButton);

        radio_active.setChecked(true);

        Bundle extras = getIntent().getExtras();
        HashMap<String, String> vendor_list = (HashMap<String, String>)extras.getSerializable("vendor_list");
        if (!vendor_list.isEmpty()){
            vendor_map = ListLocation.getInstance().getList_location();
            Log.d("zzz","create vendor list "+vendor_map.toString());
        }
        Collection<String> lables_col = vendor_map.values();

        lables = new ArrayList<String>(lables_col);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        allVendor.setAdapter(dataAdapter);



        //Load Vendor data spinner
//        loadVendorSpinnerData();
        registerEvents();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (productNameEditText.getText().toString().isEmpty()) {
                    productNameEditText.setError("Please fill the data");
                    valid = false;
                }else if (capacityEditText.getText().toString().isEmpty()){
                    capacityEditText.setError("Please fill the data");
                    valid = false;
                }else if(radio_group_type.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select type", Toast.LENGTH_SHORT).show();
                }else if(radio_status.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();

                }
                else {
                    // TODO Auto-generated method stub

//                    saveMedication();
                    update_layerfarm_ovk("");
//                    dbHelper.insertOdooProduct("medication_vaccination",productNameEditText.getText().toString());
                }
                /*
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("insert into location(name,address) values('" +
                        locationNameEditText.getText().toString()+"','"+
                        locationAddressEditText.getText().toString() + "')");
                Toast.makeText(getApplicationContext(), "Create Location Successfully", Toast.LENGTH_LONG).show();
                LocationActivity.ma.RefreshListLocation();
                finish();
                */
            }
        });

        //Spinner kind of chickin
        allUnit=(Spinner)findViewById(R.id.spUnit);
        // inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, unit);
        // mengeset Array Adapter tersebut ke Spinner
        allUnit.setAdapter(adapter);

        // mengeset listener untuk mengetahui saat item dipilih
        allUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                    Toast.makeText(CreateMedicationVaccinationActivity.this, "Selected "+ adapter.getItem(i) +" Mode", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });
    }

    private void loadVendorSpinnerData(){
        // database handler
        DataHelper db = new DataHelper(getApplicationContext());

        // Spinner Drop down elements
        List<String> lables = db.getAllVendor();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        allVendor.setAdapter(dataAdapter);
    }

    private void registerEvents() {
        radio_group_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_medicine) {
                    type = "Medicine";
                } else if (checkedId == R.id.radio_vaccine) {
                    type = "Vaccine";
                }else if (checkedId == R.id.radio_vitamin) {
                    type = "Vitamin";
                }else if (checkedId == R.id.radio_probiotic) {
                    type = "Probiotic";
                }else if (checkedId == R.id.radio_disinfectan) {
                    type = "Disinfectant";
                }
            }

        });


        radio_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    med_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    med_status = "Inactive";
                } else{
                    med_status = "Not Defined";
                }
            }

        });


    }
    private void update_layerfarm_ovk(String nid){
        int radioButtonID = radio_group_type.getCheckedRadioButtonId();
        View radioButton = radio_group_type.findViewById(radioButtonID);
        Log.d("zzz","radio "+radioButton);
        int idx = radio_group_type.indexOfChild(radioButton);
        RadioButton r = (RadioButton) radio_group_type.getChildAt(idx);
        String selectedtext = r.getText().toString();

        int radioButtonID_status = radio_status.getCheckedRadioButtonId();
        View radioButton_status = radio_status.findViewById(radioButtonID_status);
//        Log.d("zzz","radio "+radioButton);
        int idx_status = radio_status.indexOfChild(radioButton_status);
        RadioButton r_status = (RadioButton) radio_status.getChildAt(idx_status);
        String selectedtext_status = status[idx_status];


        //Get location id
        String rid = nid;
        String product = productNameEditText.getText().toString();
        String vendor = allVendor.getSelectedItem().toString();
        String vendor_nid = getKey(vendor_map, vendor);
        String capacity = capacityEditText.getText().toString();
        String unit = allUnit.getSelectedItem().toString().toLowerCase();
        String types = selectedtext;
        String state = selectedtext_status;
        // TODO Auto-generated method stub
//        SQLiteDatabase db = dbase.getWritableDatabase();
//
//        String selectQueryLocation= "SELECT * FROM vendor WHERE name = '"+ vendor + "' LIMIT 1";
//        Cursor cursorLocation = db.rawQuery(selectQueryLocation, null);
//        String vendor_id = "";
//        if(cursorLocation.moveToFirst())
//            vendor_id  =  cursorLocation.getString(cursorLocation.getColumnIndex("rid"));
//        cursorLocation.close();

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_ovk";

        String[] args = {rid, product, vendor_nid, capacity, unit, types, state};

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Parameter parameter = new Parameter(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.createOVK(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] nid = response.body();
                Log.d("message","create OVK nid : "+nid[0]);
                finish();
//                saveMedication(nid[0]);
                //SubmitDialog();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                //String alert = t.getMessage();
                Log.d("Layerfarm", "Update failure = " + t.getMessage());
                Toast.makeText(CreateMedicationVaccinationActivity.this, "failure",Toast.LENGTH_LONG).show();
                //WarningDialog();
            }
        });
    }
    private void saveMedication(String rid){
        //Get vendor id
        String vendor = allVendor.getSelectedItem().toString();
        String unit = allUnit.getSelectedItem().toString();
        // TODO Auto-generated method stub
        SQLiteDatabase db = dbase.getWritableDatabase();

        String selectQueryVendor= "SELECT id FROM vendor WHERE name = '"+ vendor + "' LIMIT 1";
        Cursor cursorVendor = db.rawQuery(selectQueryVendor, null);
        String strVendor = "";
        if(cursorVendor.moveToFirst())
            strVendor  =  cursorVendor.getString(cursorVendor.getColumnIndex("id"));
        cursorVendor.close();


        db.execSQL("insert into medication_vaccination(product_name,vendor_id, capacity, unit, type, status, rid) values('" +
                productNameEditText.getText().toString()+"','"+
                strVendor +"','"+
                capacityEditText.getText().toString()+"','"+
                unit +"','"+
                type +"','"+
                status +"','"+
                rid + "')");
        Toast.makeText(getApplicationContext(), "Create Medication Successfully", Toast.LENGTH_LONG).show();
        finish();
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

}