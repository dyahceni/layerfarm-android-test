package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateVendorActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText vendorNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_vendor);

//        dbHelper = new DataHelper(this);
//        dbase = new DatabaseHelper(this);
        vendorNameEditText = (EditText) findViewById(R.id.vendorNameEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (vendorNameEditText.getText().toString().isEmpty()) {
                    vendorNameEditText.setError("Please fill the data");
                    valid = false;
                }else {
                    // TODO Auto-generated method stub
                    create(vendorNameEditText.getText().toString());

//                    Toast.makeText(getApplicationContext(), "Create Vendor Successfully", Toast.LENGTH_LONG).show();
                    //VendorActivity.ma.RefreshListVendor();

                }
            }
        });
    }

    public void create(String vendor_name){
        String module = "ovk";
        String function_name = "ovk_create_vendor";
        String[] args = {"", vendor_name};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createVendor(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] vendorCreate = response.body();

//                SQLiteDatabase db = dbase.getWritableDatabase();
//                db.execSQL("insert into vendor(name) values('" +
//                        vendorNameEditText.getText().toString() + "')");
//
//                dbHelper.insertRes_partner(vendorNameEditText.getText().toString());
                Toast.makeText(getApplicationContext(), "Create Vendor Successfully "+vendorCreate, Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Create Vendor Failed", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}