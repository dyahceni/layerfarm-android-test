package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;


public class CreateStrainActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText strainNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_strain);

        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        strainNameEditText = (EditText) findViewById(R.id.strainNameEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (strainNameEditText.getText().toString().isEmpty()) {
                    strainNameEditText.setError("Please fill the data");
                    valid = false;
                }else {
                    // TODO Auto-generated method stub
                    SQLiteDatabase db = dbase.getWritableDatabase();
                    db.execSQL("insert into strain(name) values('" +
                            strainNameEditText.getText().toString() + "')");
                    dbHelper.insertOdooProduct("strain",strainNameEditText.getText().toString());
                    Toast.makeText(getApplicationContext(), "Create Strain Successfully", Toast.LENGTH_LONG).show();
                    //StrainActivity.ma.RefreshListStrain();
                    finish();
                }
            }
        });
    }

}