package com.layerfarm.setting.adddata;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.ParameterMember;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserActivity extends AppCompatActivity {
    TextInputLayout username, email, password, password_confirmation;
    Toolbar toolbar ;
    TextView toolbar_title;
    RadioGroup rg_state, rg_role, rg_language;
    RadioButton btn_state, btn_role, btn_language;
    Button save;
    String state;
    CheckBox operator, executive, farm_coordinator, purchasing, nutritionist, administrator;
    ArrayList<String> role = new ArrayList<>();
    LinearLayout ll;
    int create = 0;

    ParameterMember.Users users;
    HashMap<Integer, String> loc;
    String default_location;
    LinkedHashMap<Integer, String> location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(R.string.add_member);

//        ScrollView sv = new ScrollView(this);
        ll = findViewById(R.id.linear_create_user);
//        ll.setOrientation(LinearLayout.VERTICAL);
//        sv.addView(ll);

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        password_confirmation = findViewById(R.id.password_confirmation);

//        rg_state = (RadioGroup) findViewById(R.id.state);

        administrator = (CheckBox) findViewById(R.id.administrator);
        operator = (CheckBox) findViewById(R.id.operator);
        executive = (CheckBox) findViewById(R.id.executive);
        farm_coordinator = (CheckBox) findViewById(R.id.farm_coordinator);
        purchasing = (CheckBox) findViewById(R.id.purchasing);
        nutritionist = (CheckBox) findViewById(R.id.nutritionist);

//        rg_role = (RadioGroup) findViewById(R.id.role);
        rg_language = (RadioGroup) findViewById(R.id.language);
        save = (Button) findViewById(R.id.save);

        Intent i = getIntent();
        users = (ParameterMember.Users) i.getSerializableExtra("user");
        loc = (HashMap<Integer, String>) i.getSerializableExtra("location");
        default_location = (String) i.getSerializableExtra("default_location");
        location = new LinkedHashMap<>(loc);

//        Log.d("zzz","location "+location.size());
        if (location != null && location.size() >0){
//            Log.d("zzz","location isi");
            Set set2 = location.entrySet();
            Iterator iterator2 = set2.iterator();
            int fildno =0;
            while(iterator2.hasNext()) {
                Map.Entry me2 = (Map.Entry)iterator2.next();
                CheckBox ch = new CheckBox(this);
                ch.setText(me2.getValue().toString());
//                System.out.print(me2.getKey() + ": ");
//                System.out.println(me2.getValue());
                ll.addView(ch, ll.getChildCount()-1);
                ch.setId(fildno);
                ch.setTag(((Integer)me2.getKey()));
//                Log.d("zzz","check box = "+ch.getId());
//                Log.d("zzz","check tag = "+ch.getTag());
                fildno++;
            }

        }
        //berarti update
        if (users != null){
            username.getEditText().setText(users.getName());
            username.getEditText().setEnabled(false);
            email.getEditText().setText(users.getMail());
            role = users.getRoles();
            for (int a =0; a< role.size(); a++){
                Log.d("zzz","role"+role.get(a));
                switch (role.get(a)){
                    case "administrator":
                        administrator.setChecked(true);
                        break;
                    case "operator":
                        operator.setChecked(true);
                        break;
                    case "executive":
                        executive.setChecked(true);
                        break;
                    case "farm coordinator":
                        farm_coordinator.setChecked(true);
                        break;
                    case "purchasing":
                        purchasing.setChecked(true);
                        break;
                    case "nutritionist":
                        nutritionist.setChecked(true);
                        break;
                }
            }
//            if (users.getStatus().equalsIgnoreCase("1")){
//                ((RadioButton)rg_state.getChildAt(0)).setChecked(true);
//            }
//            else
//                ((RadioButton)rg_state.getChildAt(1)).setChecked(true);
//            Log.d("zzz","UID = "+users.getUid());
            if (users.getLanguage().equalsIgnoreCase("en")){
                ((RadioButton)rg_language.getChildAt(0)).setChecked(true);
            }
            if (users.getLanguage().equalsIgnoreCase("id")){
                ((RadioButton)rg_language.getChildAt(1)).setChecked(true);
            }
            if (users.getLocation() != null && users.getLocation().size() > 0){
//                for (int x = 0; x< users.getLocation().size(); x++){
//                    Log.d("zzz","location isinya = "+users.getLocation().get(x));
//                }

//                Set set2 = users.getLocation().entrySet();
//                Iterator iterator2 = set2.iterator();
                for(int c = 0; c< users.getLocation().size(); c++){
//                    Log.d("zzz","update target id = "+users.getLocation().get(c));
                    for(int b=0; b<ll.getChildCount(); b++) {

                        View nextChild = ll.getChildAt(b);

                        if(nextChild instanceof CheckBox)
                        {

                            CheckBox check = (CheckBox) nextChild;
//                            Log.d("zzz","update check box tag= "+check.getTag());
//                            Log.d("zzz","update check box ID= "+check.getId());
                            if (check.getTag() != null) {
                                if (((Integer) check.getTag()).equals(Integer.parseInt(users.getLocation().get(c)))) {
                                    check.setChecked(true);
//                                    Log.d("zzz", "samaa " + check.getTag() + " " + users.getLocation().get(c));
                                }
                            }
//                        if (check.isChecked()) {
//                            AllCheckbox.add(check.getText().toString());
//                        }
                        }

                    }
                }

            }
        }
        //berarti create
        else {
            Log.d("zzz", "user === null");
            create = 1;
            if (default_location.equalsIgnoreCase("en")){
                ((RadioButton)rg_language.getChildAt(0)).setChecked(true);
            }
            if (default_location.equalsIgnoreCase("id")){
                ((RadioButton)rg_language.getChildAt(1)).setChecked(true);
            }
        }







//        int id_role = rg_role.getCheckedRadioButtonId();
//        switch (id_role){
//            case 1:
//                role.add("administrator");
//                break;
//            case 2:
//                role.add("operator");
//                break;
//            case 3:
//                role.add("executive");
//                break;
//            case 4:
//                role.add("farm coordinator");
//                break;
//            case 5:
//                role.add("purchasing");
//                break;
//            case 6:
//                role.add("nutritionist");
//                break;
//        }
//        String add = "nutritionist";
//        role.add(add);
//        Log.d("sss","role = "+role.get(0));
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (create == 0){
                    if (administrator.isChecked()){
                        Log.d("zzz","prev_mail = "+users.getMail());
                        Log.d("zzz","cur_mail = "+email.getEditText().getText().toString());
                        //jika ada perubahan email
                        if (!users.getMail().equals(email.getEditText().getText().toString())){
                            Submit();

                            AskOption();
                        }
                        else {
                            Submit();
                        }
                    }

                }
                else {
                    Submit();
                }


            }
        });

    }
    private void AskOption() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.pop_up_edit_email, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Warning!!");

        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(CreateUserActivity.this,"YES",Toast.LENGTH_SHORT).show();
//                Submit();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/account/subscriptions?package=com.layerfarm.layerfarmmanager"));
                startActivity(browserIntent);
            }
        });
        dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(CreateUserActivity.this, "NO", Toast.LENGTH_LONG).show();
            }
        });
        dialog.show();
    }

    public void Submit(){
        String lang = "";
        int id_lang = rg_language.getCheckedRadioButtonId();
        View radioLanguage = rg_language.findViewById(id_lang);
        int childLang = rg_language.indexOfChild(radioLanguage);
        Log.d("zzz","language = "+id_lang );
        Log.d("zzz","child language = "+childLang );
        if (childLang == 0){
            lang = "en";
        }
        else if (childLang == 1){
            lang = "id";
        }
        role.clear();
        role.add("authenticated user");
        if (administrator.isChecked()){
            role.add("administrator");
        }
        if (operator.isChecked()){
            role.add("operator");
        }
        if (executive.isChecked()){
            role.add("executive");
        }
        if (farm_coordinator.isChecked()){
            role.add("farm coordinator");
        }
        if (purchasing.isChecked()){
            role.add("purchasing");
        }
        if (nutritionist.isChecked()){
            role.add("nutritionist");
        }

        ArrayList<String> location_check = new ArrayList<>();
        for(int b=0; b<ll.getChildCount(); b++) {

            View nextChild = ll.getChildAt(b);

            if(nextChild instanceof CheckBox)
//                    Log.d("zzz","test masuk insteadof");
            {

//                        try {
                CheckBox check = (CheckBox) nextChild;
//                            Log.d("zzz","click check box tag= "+check.getTag());
//                            Log.d("zzz","click check box ID= "+check.getId());
//                            Log.d("zzz","is selected= "+check.isChecked());
//                            if (check.getTag() != null) {
//                            if (((Integer) check.getTag()).equals(Integer.parseInt(users.getLocation().get(c)))) {
//                                check.setChecked(true);
//                                Log.d("zzz", "samaa " + check.getTag() + " " + users.getLocation().get(c));
//                            }
//                        if(check.)
                if (check.isChecked()){
//                                    Log.d("zzz","is check "+check.getTag().toString());
                    location_check.add(check.getTag().toString());
                }
//                            }
//                        }catch (Exception e){}

//                        if (check.isChecked()) {
//                            AllCheckbox.add(check.getText().toString());
//                        }
            }

        }

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_user";
        ParameterMember.Users args = new ParameterMember.Users();
        if(create == 1){
            args.setUid("");
            args.setStatus("0");
        }
        else {
            args.setUid(users.getUid());
            args.setStatus(users.getStatus());
        }

        args.setName(username.getEditText().getText().toString());
        args.setMail(email.getEditText().getText().toString());
        args.setPass(password.getEditText().getText().toString());
//                args.setStatus("0");
        args.setRoles(role);
        args.setLanguage(lang);
        args.setLocation(location_check);
        for (int i =0; i< role.size(); i++){
            Log.d("zzz","role = "+ role.get(i));
        }
//                Log.d("zzz","state = "+state);

        ParameterMember parameter = new ParameterMember(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<ArrayList<String>> call = apiInterfaceJson.createMember(token2, parameter);

        call.enqueue(new Callback<ArrayList<String>>() {

            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {
                ArrayList<String> member = response.body();
                Toast.makeText(CreateUserActivity.this, "Success", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(CreateUserActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void close(View view){
        finish();
    }
}
