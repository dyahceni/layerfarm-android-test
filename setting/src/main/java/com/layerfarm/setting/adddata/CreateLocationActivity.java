package com.layerfarm.setting.adddata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.ParameterCreateLocation;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;
import com.layerfarm.layerfarm.DatabaseHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateLocationActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText locationNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_location);


        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        locationNameEditText = (EditText) findViewById(R.id.locationNameEditText);
//        locationAddressEditText = (EditText) findViewById(R.id.locationAddressEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid = true;
                if (locationNameEditText.getText().toString().isEmpty()) {
                    locationNameEditText.setError("Please fill the data");
                    valid = false;
//                } else if (locationAddressEditText.getText().toString().isEmpty()) {
//                    locationAddressEditText.setError("Please fill the data");
//                    valid = false;
                } else {
                    // TODO Auto-generated method stub
                   /* SQLiteDatabase db = dbase.getWritableDatabase();
                    db.execSQL("insert into location(name,address) values('" +
                            locationNameEditText.getText().toString() + "','" +
                            locationAddressEditText.getText().toString() + "')");
                    dbHelper.insertStock_warehouse(locationNameEditText.getText().toString());*/
                    create_layerfarm_location();
                    //Toast.makeText(getApplicationContext(), "Create Location Successfully", Toast.LENGTH_LONG).show();
                    //LocationActivity.ma.RefreshListLocation();
                    //finish();
                }
            }
        });
    }

    public void create_layerfarm_location() {

        EditText a = (EditText) findViewById(R.id.locationNameEditText);
        String farm_name = (String)(a.getText().toString());


        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_farm";

        ParameterCreateLocation.Arguments args = new ParameterCreateLocation.Arguments("", farm_name, "", "");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateLocation parameter = new ParameterCreateLocation(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_location(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                //SubmitDialog();
               Toast.makeText(getApplicationContext(), "Create Farm/Location Successfully", Toast.LENGTH_LONG).show();
               finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
                Toast.makeText(getApplicationContext(), "Create Farm/Location Successfully", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
}