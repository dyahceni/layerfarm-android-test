package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterStrain;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.StrainsSync;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.Strain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StrainActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    Toolbar toolbar ;
    TextView toolbar_title;
    List<Strain> itemList = new ArrayList<Strain>();
    AdapterStrain adapter;
    DatabaseHelper dbase;
    DataHelper SQLite = new DataHelper(this);

    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "name";
    public static final String TAG_PRODUCT_RID = "rid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strain);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.strain));

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_strain);

//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(StrainActivity.this, CreateStrainActivity.class);
//                startActivity(intent);
//            }
//        });

        adapter = new AdapterStrain(StrainActivity.this, itemList);
        listView.setAdapter(adapter);

        // long press listview to show edit and delete
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(final AdapterView<?> parent, View view,
//                                    final int position, long id) {
//                // TODO Auto-generated method stub
//                final String idx = itemList.get(position).getId();
//                final String name = itemList.get(position).getName();
//                final String rid = itemList.get(position).getRid();
//
//                final CharSequence[] dialogitem = {"Edit", "Delete"};
//                dialog = new AlertDialog.Builder(StrainActivity.this);
//                dialog.setCancelable(true);
//                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        // TODO Auto-generated method stub
//                        switch (which) {
//                            case 0:
//                                Intent intent = new Intent(StrainActivity.this, UpdateStrain.class);
//                                intent.putExtra(TAG_ID, idx);
//                                intent.putExtra(TAG_PRODUCT_NAME, name);
//                                intent.putExtra(TAG_PRODUCT_RID, rid);
//                                startActivity(intent);
//                                break;
//                            case 1:
//                                AlertDialog.Builder builder=new AlertDialog.Builder(StrainActivity.this); //Home is name of the activity
//                                builder.setMessage("Do you want to delete this data?");
//                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int id) {
//
//                                        SQLite.deleteStrain(Integer.parseInt(idx));
//                                        //SQLite.deleteMedicationVaccination(idx);
//                                        itemList.clear();
//                                        getAllData();
//
//
//                                    }
//                                });
//
//                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//
//                                AlertDialog alert=builder.create();
//                                alert.show();
//
//
//                                break;
//                        }
//                    }
//                }).show();
//                //return false;
//            }
//        });

//        getAllData();

    }


    private void getAllData() {
        itemList.clear();
//        ArrayList<HashMap<String, String>> row = SQLite.getAllStrainList();
//        itemList.clear();
//        for (int i = 0; i < row.size(); i++) {
//            String id = row.get(i).get(TAG_ID);
//            String product_name = row.get(i).get(TAG_PRODUCT_NAME);
//            String rid = row.get(i).get(TAG_PRODUCT_RID);
//
//            Strain data = new Strain();
//
//            data.setId(id);
//            data.setName(product_name);
//
//
//            itemList.add(data);
//        }
//
//        adapter.notifyDataSetChanged();

        final ProgressDialog progressDoalog = new ProgressDialog(StrainActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_strain";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        //Coding mbak Diah
        /*ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<StrainsSync>> call = apiInterfaceJson.getStrain(token2, parameter);

        call.enqueue(new Callback<List<StrainsSync>>() {
            @Override
            public void onResponse(Call<List<StrainsSync>> call, Response<List<StrainsSync>> response) {

                ArrayList<HashMap<String, String>> row = SQLite.getAllStrainList();
                List<String> data = new ArrayList<>();
                for (int i = 0; i < row.size(); i++) {
                    String name = row.get(i).get("name").toLowerCase();
                    String rid = row.get(i).get("rid");
                    Log.d("Layerfarm","data ="+name);
                    data.add(name);
                }
                List<StrainsSync> strainsSyncList = response.body();
                for (int i =0; i< strainsSyncList.size() ; i++){
                    HashMap<String, String> map = new HashMap<String, String>();
                    if (!strainsSyncList.get(i).getName().isEmpty()) {
                        map.put("key", strainsSyncList.get(i).getName().toLowerCase());
                        map.put("name", strainsSyncList.get(i).getName());
                        map.put("rid", strainsSyncList.get(i).getRid());
                        InsertDataStrain(map, data);
                    }
                }

                Toast.makeText(getApplicationContext(), "Create Vendor Successfully", Toast.LENGTH_LONG).show();
                getAllData();
                getAllData();
                Toast.makeText(getApplicationContext(), "Strain Synchronisizing sucessfully ", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(Call<List<StrainsSync>> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });*/

//        final SQLiteDatabase db = dbase.getWritableDatabase();

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<StrainsSync>> call = apiInterfaceJson.getStrain(token2, parameter);

        call.enqueue(new Callback<List<StrainsSync>>() {
            @Override
            public void onResponse(Call<List<StrainsSync>> call, Response<List<StrainsSync>> response) {
                List<StrainsSync> strainSyncList = response.body();

                if(strainSyncList!=null && strainSyncList.size()>0) {

//                    db.delete("STRAIN", null, null);
                    for (int i = 0; i < strainSyncList.size(); i++) {
                        Strain list = new Strain();
                        list.setName(strainSyncList.get(i).getName());
                        list.setRid(strainSyncList.get(i).getRid());
                        itemList.add(list);
                        //Log.d("ssss", "name = " + farmSyncList.get(i).getNid());
//                        Integer number = Integer.parseInt(strainSyncList.get(i).getRid());
//                        //Log.d("ssss", "" + number.toString());
//                        //Cursor result = db.rawQuery("SELECT rid FROM STRAIN WHERE rid='" + strainSyncList.get(i).getRid()+"'", null);
//                        //if (result.getCount() == 0) {
//                        db.execSQL("insert into strain(name,rid) values('" +
//                                strainSyncList.get(i).getName() + "','" +
//                                number+ "')");
//
//                        //}else{
//                        db.execSQL("UPDATE strain SET name = '"+strainSyncList.get(i).getName()+"' WHERE rid = "+ number);
                        //}
                    }
                }

//                itemList.clear();
                adapter.notifyDataSetChanged();
//                getAllData();
                listView.getAdapter().getCount();
                Toast.makeText(getApplicationContext(), "Total Sync of Location are:" + listView.getAdapter().getCount() , Toast.LENGTH_LONG).show();
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<List<StrainsSync>> call, Throwable t) {
                String alert = t.getMessage();
                Toast.makeText(getApplicationContext(), "Failed to show strain list" , Toast.LENGTH_LONG).show();
                progressDoalog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        itemList.clear();
        getAllData();
//        sync();
    }

    public void sync(View v){
        getAllData();

    }
    public void InsertDataStrain(HashMap<String, String> map , List<String> data){
        SQLiteDatabase db = dbase.getWritableDatabase();
        String key = map.get("key");

        if (!data.contains(key)) {
            Log.d("Layerfarm","noo");
            Log.d("Layerfarm","key ="+key);
            db.execSQL("insert into strain(name,rid) values('" +
                    map.get("name") +
                    map.get("rid") + "')");

        }
        else {
            Log.d("Layerfarm", "adaa");

        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
    public void close(View view){
        finish();
    }
}
