package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterLocation;
import com.layerfarm.setting.adddata.CreateLocationActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.updatedata.UpdateLocation;

import java.util.ArrayList;
import java.util.List;

//retrofit
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationActivity extends AppCompatActivity {

    private String TAG = SyncLayerfarmActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    public LayerFarm layerFarm;
    ListView listView;
    AlertDialog.Builder dialog;
    List<Farm> itemList = new ArrayList<Farm>();
    List<Farm> active = new ArrayList<>();
    List<Farm> inactive = new ArrayList<>();
    List<Farm> all = new ArrayList<>();
    AdapterLocation adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper dbase;

    Toolbar toolbar ;
    TextView toolbar_title;
    ProgressBar progressBar;
    EditText search;
    LinearLayout linearLayout;
    ProgressDialog progressDoalog;
    TabLayout mTabLayout;


    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "name";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_RID = "rid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main_location);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.location));
        progressBar = (ProgressBar) findViewById(R.id.progress_circular);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_location);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LocationActivity.this, CreateLocationActivity.class);
                startActivity(intent);
            }
        });
        mTabLayout = (TabLayout)findViewById(R.id.tabs);
        adapter = new AdapterLocation(LocationActivity.this, itemList);
        listView.setAdapter(adapter);
//        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                Farm item = (Farm) adapter.getItem(position);
                final String idx = item.getNid();
                final String name = item.getName();
                final String address = item.getAddress();
                final String rid = item.getNid();
                final String delete_status = item.getDelete_status();

                if (delete_status.equals("0")){
                    final CharSequence[] dialogitem = {"Edit", "Delete"};
                    dialog = new AlertDialog.Builder(LocationActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(LocationActivity.this, UpdateLocation.class);
                                    intent.putExtra("id", idx);
                                    intent.putExtra("name", name);
                                    intent.putExtra("address", address);
                                    intent.putExtra("rid", rid);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    AlertDialog.Builder builder=new AlertDialog.Builder(LocationActivity.this); //Home is name of the activity
                                    builder.setMessage("Do you want to delete this data?");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {

                                            deleteLocation(rid);
                                            //SQLite.deleteLocation(Integer.parseInt(idx));
                                            //SQLite.deleteMedicationVaccination(idx);


                                        }
                                    });

                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                                    AlertDialog alert=builder.create();
                                    alert.show();


                                    break;
                            }
                        }
                    }).show();
                }
                else {
                    final CharSequence[] dialogitem = {"Edit"};
                    dialog = new AlertDialog.Builder(LocationActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(LocationActivity.this, UpdateLocation.class);
                                    intent.putExtra("id", idx);
                                    intent.putExtra("name", name);
                                    intent.putExtra("address", address);
                                    intent.putExtra("rid", rid);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                }


                //return false;
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                LocationActivity.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        getAllData();

    }
    private void onTabTapped(int position) {
        switch (position) {

            case 0:
                itemList.clear();
                search.setText("");
                itemList.addAll(active);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                itemList.clear();
                search.setText("");
                itemList.addAll(inactive);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                itemList.clear();
                itemList.addAll(all);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }

//
//    private void getAllData() {
//        ArrayList<HashMap<String, String>> row = SQLite.getAllLocationList();
//
//        for (int i = 0; i < row.size(); i++) {
//            String id = row.get(i).get(TAG_ID);
//            String product_name = row.get(i).get(TAG_PRODUCT_NAME);
//            String address = row.get(i).get(TAG_ADDRESS);
//            String rid = row.get(i).get(TAG_RID);
//
//
//            Farm data = new Farm();
//
//            data.setNid(id);
//            data.setName(product_name);
//            data.setAddress(address);
//            data.setRid(rid);
//
//
//            itemList.add(data);
//        }
//
//        adapter.notifyDataSetChanged();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        //getAllData();
        getLocationData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void sync_location(View v){
        getLocationData();


    }

    private void getLocationData(){
//        onTabTapped(0);
        progressDoalog = new ProgressDialog(LocationActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_farm";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        all.clear();
        inactive.clear();
        active.clear();
        itemList.clear();
        final SQLiteDatabase db = dbase.getWritableDatabase();

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<Farm>> call = apiInterfaceJson.getFarm(token2, parameter);

        call.enqueue(new Callback<List<Farm>>() {
            @Override
            public void onResponse(Call<List<Farm>> call, Response<List<Farm>> response) {
                List<Farm> farmSyncList = response.body();

                if(farmSyncList!=null && farmSyncList.size()>0) {
//                    db.delete("LOCATION", null, null);
                    for (int i = 0; i < farmSyncList.size(); i++) {
                        Farm data = new Farm();

                        data.setName(farmSyncList.get(i).getName());
//                        data.setAddress(address);
                        data.setNid(farmSyncList.get(i).getNid());
                        data.setAddress(farmSyncList.get(i).getAddress());
                        data.setDelete_status(farmSyncList.get(i).getDelete_status());
                        all.add(data);
                        active.add(data);


//                        //Log.d("ssss", "name = " + farmSyncList.get(i).getNid());
//                        Integer number = Integer.parseInt(farmSyncList.get(i).getNid());
//                        //Log.d("ssss", "" + number.toString());
//                        //Cursor result = db.rawQuery("SELECT NAME FROM LOCATION WHERE NAME='" + farmSyncList.get(i).getName()+"'", null);
//                        //if (result.getCount() == 0) {
//                        db.execSQL("insert into location(name, address, rid) values('" +
//                                farmSyncList.get(i).getName() + "','" +
//                                "" + "','" +
//                                number+ "')");

                        //}else{
                        //    db.execSQL("UPDATE location SET name = '"+farmSyncList.get(i).getName()+"' WHERE rid = "+ number);
                        //}
                    }
                }

                itemList.addAll(all);
                mTabLayout.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                search.setText("");
                linearLayout.setVisibility(View.VISIBLE);
//                getAllData();
                progressDoalog.dismiss();
                listView.getAdapter().getCount();
                Toast.makeText(getApplicationContext(), "Total Sync of Location are:" + listView.getAdapter().getCount() , Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Farm>> call, Throwable t) {
                String alert = t.getMessage();
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Failed to load data" + t.getMessage() , Toast.LENGTH_LONG).show();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
      /*          AlertDialog.Builder builder = new AlertDialog.Builder(LocationActivity.this);
                builder.setTitle("Connection Alert")
                        .setIcon(R.drawable.error)
                        .setMessage(alert + "Please check connection server!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                              //  Toast.makeText(LocationActivity.this,"Selected Option: YES",Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                      *//*  .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                             }
                        });*//*
                //Creating dialog box
                AlertDialog dialog  = builder.create();
                dialog.show();*/

            }
        });
    }

    private void deleteLocation(String nid){
        String module = "layerfarm_android";
        String function_name = "delete_setting_data";

        ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(nid,"location");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

        Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                //if (response != null && response.isSuccessful()) {
                String message = response.body();
                //Log.d("message","get_expense_general_respons : "+message);
                if (message.equalsIgnoreCase("fail")){
                    Toast.makeText(getApplicationContext(), "Location can' delete, location used on other transaction" , Toast.LENGTH_LONG).show();
                }

                if (message.equalsIgnoreCase("ok")){
                    Toast.makeText(getApplicationContext(), "Location deleted" , Toast.LENGTH_LONG).show();
                    itemList.clear();

                    getLocationData();
                }

                //}

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail location deleted" , Toast.LENGTH_LONG).show();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void close(View view){
        finish();
    }
}
