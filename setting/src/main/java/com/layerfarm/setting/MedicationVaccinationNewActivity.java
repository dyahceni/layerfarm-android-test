package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterMedicationVaccination;
import com.layerfarm.setting.adddata.CreateMedicationVaccinationActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.OvkSync;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.SyncOvk;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.model.MedicationVaccination;
import com.layerfarm.setting.updatedata.UpdateMedicationVaccination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MedicationVaccinationNewActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    List<MedicationVaccination> itemList = new ArrayList<MedicationVaccination>();
    List<MedicationVaccination> active = new ArrayList<>();
    List<MedicationVaccination> inactive = new ArrayList<>();
    List<MedicationVaccination> all = new ArrayList<>();
    AdapterMedicationVaccination adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper db;

    Toolbar toolbar ;
    TextView toolbar_title;
    ProgressBar progressBar;
    EditText search;
    LinearLayout linearLayout;
    ProgressDialog progressDoalog;
    TabLayout mTabLayout;

    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "product_name";
    public static final String TAG_VENDOR_NAME = "vendor_name";
    public static final String TAG_CAPACITY = "capacity";
    public static final String TAG_UNIT = "unit";
    public static final String TAG_TYPE = "type";
    public static final String TAG_STATUS = "status";
    public static final String TAG_RID = "rid";
    public static final String TAG_DELETE_STATUS = "delete_status";
    LinkedHashMap<String, String> vendor_list = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_vaccination_new);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.medication_vaccination));
        progressBar = (ProgressBar) findViewById(R.id.progress_circular);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);


        SQLite = new DataHelper(getApplicationContext());
        db = new DatabaseHelper(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_medication_vaccination);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MedicationVaccinationNewActivity.this, CreateMedicationVaccinationActivity.class);
                intent.putExtra("vendor_list", vendor_list);
                startActivity(intent);
            }
        });

        mTabLayout = (TabLayout)findViewById(R.id.tabs);

        adapter = new AdapterMedicationVaccination(MedicationVaccinationNewActivity.this, itemList);
        listView.setAdapter(adapter);
//        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                MedicationVaccination medicationmodel = (MedicationVaccination)adapter.getItem(position);
                final String idx = medicationmodel.getId();
                final String product_name = medicationmodel.getProduct_name();
                final String vendor_id = medicationmodel.getVendor_id();
                final String capacity = medicationmodel.getCapacity();
                final String unit = medicationmodel.getUnit();
                final String type = medicationmodel.getType();
                final String status = medicationmodel.getStatus();
                final String rid = medicationmodel.getRid();
                final String delete_status = medicationmodel.getDeleteStatus();
                final LinkedHashMap<String, String> vendor_list= medicationmodel.getVendor_list();
                Log.d("medvac", " "+idx+"-"+product_name+"-"+vendor_id+"-"+capacity+"-"+unit+"-"+type+"-"+status+"-"+rid);


                if (delete_status.equals("0")){
                    final CharSequence[] dialogitem = {"Edit", "Delete"};
                    dialog = new AlertDialog.Builder(MedicationVaccinationNewActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(MedicationVaccinationNewActivity.this, UpdateMedicationVaccination.class);
                                    intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_PRODUCT_NAME, product_name);
                                    intent.putExtra(TAG_VENDOR_NAME, vendor_id);
                                    intent.putExtra(TAG_CAPACITY, capacity);
                                    intent.putExtra(TAG_UNIT, unit);
                                    intent.putExtra(TAG_TYPE, type);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_RID, rid);
                                    intent.putExtra("vendor_list", vendor_list);
                                    startActivity(intent);
                                    break;
                                case 1:
//                                SQLite.deleteMedicationVaccination(Integer.parseInt(idx));
                                    deleteMedicineVaccination(rid);
                                    itemList.clear();
                                    sync();
//                                getAllData();


                                    break;
                            }
                        }
                    }).show();
                } else {
                    final CharSequence[] dialogitem = {"Edit"};
                    dialog = new AlertDialog.Builder(MedicationVaccinationNewActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(MedicationVaccinationNewActivity.this, UpdateMedicationVaccination.class);
                                    intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_PRODUCT_NAME, product_name);
                                    intent.putExtra(TAG_VENDOR_NAME, vendor_id);
                                    intent.putExtra(TAG_CAPACITY, capacity);
                                    intent.putExtra(TAG_UNIT, unit);
                                    intent.putExtra(TAG_TYPE, type);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_RID, rid);
                                    intent.putExtra("vendor_list", vendor_list);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                }

                //return false;
            }
        });
//        sync();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                MedicationVaccinationNewActivity.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        getAllData();

    }

    private void onTabTapped(int position) {
        switch (position) {

            case 0:
                itemList.clear();
                search.setText("");
                itemList.addAll(active);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                itemList.clear();
                search.setText("");
                itemList.addAll(inactive);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                itemList.clear();
                itemList.addAll(all);
                search.setText("");
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    private void deleteMedicineVaccination(String nid){
        final ProgressDialog progress = new ProgressDialog(MedicationVaccinationNewActivity.this);
//                                progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        String module = "layerfarm_android";
        String function_name = "delete_setting_data";

        ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(nid,"ovk");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

        Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                //if (response != null && response.isSuccessful()) {
                String message = response.body();
                //Log.d("message","get_expense_general_respons : "+message);
                if (message.equalsIgnoreCase("fail")){
                    Toast.makeText(getApplicationContext(), "Medicine or Vaccine can't delete, Medicine or Vaccine used on other transaction" , Toast.LENGTH_LONG).show();
                }

                if (message.equalsIgnoreCase("ok")){
                    Toast.makeText(getApplicationContext(), "Medicine or Vaccine deleted" , Toast.LENGTH_LONG).show();
                }

                progress.dismiss();
                //}

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Medicine or Vaccine failed" , Toast.LENGTH_LONG).show();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    private void getAllData() {
        ArrayList<HashMap<String, String>> row = SQLite.getAllMedicationVaccination();
        itemList.clear();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get(TAG_ID);
            String product_name = row.get(i).get(TAG_PRODUCT_NAME);
            String vendor_id = row.get(i).get(TAG_VENDOR_NAME);
            String capacity = row.get(i).get(TAG_CAPACITY);
            String unit = row.get(i).get(TAG_UNIT);
            String type = row.get(i).get(TAG_TYPE);
            String status = row.get(i).get(TAG_STATUS);
            String rid = row.get(i).get(TAG_RID);
            String delete_status = row.get(i).get(TAG_DELETE_STATUS);


            MedicationVaccination data = new MedicationVaccination();

            data.setId(id);
            data.setProduct_name(product_name);
            data.setVendor_id(vendor_id);
            data.setCapacity(capacity);
            data.setUnit(unit);
            data.setType(type);
            data.setStatus(status);
            data.setRid(rid);
            data.setDeleteStatus(delete_status);


            itemList.add(data);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
//        search.setText("");
        sync();
//        getAllData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void sync(){

        progressDoalog = new ProgressDialog(MedicationVaccinationNewActivity.this);
        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        all.clear();
        inactive.clear();
        active.clear();
        itemList.clear();
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_ovk";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<SyncOvk> call = apiInterfaceJson.getOvk(token2, parameter);

        call.enqueue(new Callback<SyncOvk>() {
            @Override
            public void onResponse(Call<SyncOvk> call, Response<SyncOvk> response) {
//                SyncOvk data = response.body();
                List<OvkSync> ovkSyncList = response.body().getList_ovk();
                if (response.body().getVendor_list() != null && response.body().getVendor_list().size() > 0) {
                    vendor_list = response.body().getVendor_list();
                    ListLocation.getInstance().setList_location(vendor_list);
                    Log.d("zzz","list vendor"+vendor_list.toString());
                    onTabTapped(0);
                }
//                HashMap<String, String> all_ovk = new HashMap<>();

//                ArrayList<HashMap<String, String>> row = SQLite.getAllMedicationVaccination();
//                List<String> active_list = new ArrayList<>();
//                List<String> inactive_list = new ArrayList<>();
//                ArrayList<String> all_ovk = new ArrayList<>();
//
//                for (int i = 0; i < row.size(); i++) {
//                    String id = row.get(i).get(TAG_ID);
//                    String product_name = row.get(i).get(TAG_PRODUCT_NAME);
//                    String vendor_id = row.get(i).get(TAG_VENDOR_NAME);
//                    String capacity = row.get(i).get(TAG_CAPACITY);
//                    String unit = row.get(i).get(TAG_UNIT);
//                    String type = row.get(i).get(TAG_TYPE);
//                    String status = row.get(i).get(TAG_STATUS);
//                    String rid = row.get(i).get(TAG_RID);
//                    String key = rid+"#"+product_name.toLowerCase()+"-"+unit.toLowerCase();
//                    String keys = product_name.toLowerCase()+"-"+unit.toLowerCase();
//                    if (status.equals("Active")){
//                        active_list.add(keys);
//                    }
//                    else{
//                        inactive_list.add(keys);
//                    }
//                    all_ovk.add(key);
//                }

//                List<String> ovkSync = new ArrayList<>();
//                Log.d("Layerfarm", "flockStatus = " + ovkSyncList.size());
                if(ovkSyncList!=null && ovkSyncList.size()>0) {
                    for (int i = 0; i < ovkSyncList.size(); i++) {
//                    HashMap<String, String> map = new HashMap<>();
//                    Log.d("ov key", "key :"+ovkSyncList.get(i).getProduct_name().toLowerCase());
//                    Log.d("ov key", "unit : "+ovkSyncList.get(i).getUnit());
//                    String key;
//                    if (ovkSyncList.get(i).getUnit()==null){
//                        key = ovkSyncList.get(i).getProduct_name().toLowerCase()+"-";
//                    }
//                    else
//                        key = ovkSyncList.get(i).getProduct_name().toLowerCase()+"-"+ovkSyncList.get(i).getUnit().toLowerCase();
                        MedicationVaccination data = new MedicationVaccination();

//                    data.setId(id);
                        data.setProduct_name(ovkSyncList.get(i).getProduct_name());
                        data.setVendor_id(ovkSyncList.get(i).getVendor());
                        data.setCapacity(ovkSyncList.get(i).getCapacity());
                        data.setUnit(ovkSyncList.get(i).getUnit());
                        data.setType(ovkSyncList.get(i).getType());
                        data.setStatus(ovkSyncList.get(i).getStatus());
                        data.setRid(ovkSyncList.get(i).getRid());
                        data.setDeleteStatus(ovkSyncList.get(i).getDeleteStatus());
                        data.setVendor_list(vendor_list);
                        all.add(data);
                        Log.d("zzz","status = "+ovkSyncList.get(i).getStatus());
                        if (ovkSyncList.get(i).getStatus().equalsIgnoreCase("active")) {
                            active.add(data);
                        } else
                            inactive.add(data);

//                    itemList.add(data);
//                    Log.d("xxx","vendor "+ovkSyncList.get(i).getVendor_list().toString());

//                    map.put("key", key);
//                    map.put("name",ovkSyncList.get(i).getProduct_name());
//                    map.put("vendor", ovkSyncList.get(i).getVendor());
//                    map.put("capacity", ovkSyncList.get(i).getCapacity());
//                    map.put("unit", ovkSyncList.get(i).getUnit());
//                    map.put("type", ovkSyncList.get(i).getType());
//                    map.put("status", ovkSyncList.get(i).getStatus());
//                    map.put("rid",ovkSyncList.get(i).getRid());
//                    InsertDataOVK(active_list,inactive_list,map);
//                    ovkSync.add(key);
//                    Log.d("Layerfarm","name "+i+"="+ovkSyncList.get(i).getProduct_name());
                    }
//                SQLiteDatabase dbase = db.getWritableDatabase();
//                for (int i=0; i<all_ovk.size();i++){
//                    String[] separated = all_ovk.get(i).split("#");
//                    String id = separated[0];
//                    if(!ovkSync.contains(separated[1])){
//                        String updateQuery = "UPDATE medication_vaccination SET status = '" + "Inactive" + "' WHERE id =" + "'" + id + "'";
//                        Log.e("update sqlite ", updateQuery);
//                        dbase.execSQL(updateQuery);
//                        //db.close();
//                    }
//                }
                }
                itemList.addAll(active);
                mTabLayout.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                search.setText("");
                linearLayout.setVisibility(View.VISIBLE);
//                getAllData();
                progressDoalog.dismiss();
//                Toast.makeText(getApplicationContext(), "Medication and Vaccination Successfully ", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(Call<SyncOvk> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });

    }

    public void InsertDataOVK(List<String> active, List<String> inactive, HashMap<String, String> map){
        SQLiteDatabase dbase = db.getWritableDatabase();
        String key = map.get("key");
        String name = map.get("name");
        String vendor = map.get("vendor");
        String capacity = map.get("capacity");
        String unit = map.get("unit");
        String type = map.get("type");
        String status = map.get("status");
        String rid = map.get("rid");



//        Log.d("Layerfarm","key = "+key);
        //if (status.equals("Active")){
        //jika tidak ada di dalam array active maka cari didalam array inactive
        if ( !active.contains(key)) {
            //jika tidak ada didalam inactive, maka buat product baru
            if (!inactive.contains(key)){
//                    Log.d("Layerfarm","not inactive");
                dbase.execSQL("insert into medication_vaccination(product_name,vendor_id,capacity,unit,type,status,rid) values('" +
                        name+"','"+
                        vendor+"','"+
                        capacity +"','"+
                        unit +"','"+
                        type +"','"+
                        status +"','"+
                        rid +"')");
            }
            //jika terdapat di array inactive, maka update status menjadi aktif
            else {
//                    Log.d("Layerfarm","inactive");
                //Get ovk id
                String selectQuery= "SELECT id FROM medication_vaccination WHERE product_name = '"+ name + "' AND unit = '"+unit+"' LIMIT 1";
                Cursor cursor = dbase.rawQuery(selectQuery, null);
                String ID = "";
                if(cursor.moveToFirst())
                    ID  =  cursor.getString(cursor.getColumnIndex("id"));
                cursor.close();
                String updateQuery = "UPDATE medication_vaccination SET status = '" + status + "' WHERE id =" + "'" + ID + "'";
                Log.e("update sqlite ", updateQuery);
                dbase.execSQL(updateQuery);
                //db.close();

            }

        }
        else {
//                Log.d("Layerfarm","actived");
            //Get ovk id
            String selectQuery= "SELECT id FROM medication_vaccination WHERE product_name = '"+ name + "' AND unit = '"+unit+"' LIMIT 1";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String ID = "";
            if(cursor.moveToFirst())
                ID  =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();
            String updateQuery = "UPDATE medication_vaccination SET status = '" + status + "' WHERE id =" + "'" + ID + "'";
            Log.e("update sqlite ", updateQuery);
            dbase.execSQL(updateQuery);
            //db.close();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void close(View view){
        finish();
    }
}
