package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterFlock;
import com.layerfarm.setting.adddata.CreateFlockActivity;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.SyncFlock;
import com.layerfarm.layerfarm.model.mFlock;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.updatedata.UpdateFlock;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlockActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    public LayerFarm layerFarm;
    ListView listView;
    AlertDialog.Builder dialog;
    List<mFlock> itemList = new ArrayList<mFlock>();
    AdapterFlock adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper dbase;

    List<mFlock> active = new ArrayList<>();
    List<mFlock> inactive = new ArrayList<>();
    List<mFlock> all = new ArrayList<>();

    Toolbar toolbar ;
    TextView toolbar_title;
    ProgressBar progressBar;
    EditText search;
    LinearLayout linearLayout;
    ProgressDialog progressDoalog;
    TabLayout mTabLayout;

    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_LOCATION = "location_id";
    public static final String TAG_TYPE = "type";
    public static final String TAG_PERIOD = "period";
    public static final String TAG_STATUS = "status";
    public static final String TAG_CAPACITY = "capacity";
    public static final String TAG_RID = "rid";
    LinkedHashMap<String, String> list_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flock);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main_flock);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.flock));
        progressBar = (ProgressBar) findViewById(R.id.progress_circular);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_flock);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list_location != null){
                    Intent intent = new Intent(FlockActivity.this, CreateFlockActivity.class);
                    intent.putExtra("list_location", list_location);
                    startActivity(intent);
                }else {
                    AskOption();
                }
            }
        });
        mTabLayout = (TabLayout)findViewById(R.id.tabs);
        adapter = new AdapterFlock(FlockActivity.this, itemList);
        listView.setAdapter(adapter);
        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                mFlock item = (mFlock) adapter.getItem(position);
//                final String idx = item.getId();
                final String name = item.getName();
                final String loc_id = item.getLocation_id();
                final String type = item.getType();
                final String period = item.getPeriod();
                final String status = item.getStatus();
                final String capacity = item.getCapacity();
                final String rid = item.getNid();
                final String delete = item.getDelete();
                final String effective_date = item.getEffective_date();
                final LinkedHashMap<String, String> list_loc = list_location;
                if (delete.equals("0")){
                    final CharSequence[] dialogitem = {"Edit", "Delete"};
                    dialog = new AlertDialog.Builder(FlockActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(FlockActivity.this, UpdateFlock.class);
//                                intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_NAME, name);
                                    intent.putExtra(TAG_LOCATION, loc_id);
                                    intent.putExtra(TAG_TYPE, type);
                                    intent.putExtra(TAG_PERIOD, period);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_CAPACITY, capacity);
                                    intent.putExtra(TAG_RID, rid);
                                    intent.putExtra("effective_date", effective_date);
                                    intent.putExtra("list_location", list_loc);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    AlertDialog.Builder builder=new AlertDialog.Builder(FlockActivity.this); //Home is name of the activity
                                        builder.setMessage("Do you want to delete this data?");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {

                                                deleteFlock(rid);
                                            }
                                        });

                                        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                    AlertDialog alert=builder.create();
                                    alert.show();


                                    break;
                            }
                        }
                    }).show();
                }
                else {
                    final CharSequence[] dialogitem = {"Edit"};
//                if (delete == "1"){
//                    dialogitem.
//                }
                    dialog = new AlertDialog.Builder(FlockActivity.this);
                    dialog.setCancelable(true);
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(FlockActivity.this, UpdateFlock.class);
//                                intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_NAME, name);
                                    intent.putExtra(TAG_LOCATION, loc_id);
                                    intent.putExtra(TAG_TYPE, type);
                                    intent.putExtra(TAG_PERIOD, period);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_CAPACITY, capacity);
                                    intent.putExtra(TAG_RID, rid);
                                    intent.putExtra("effective_date", effective_date);
                                    intent.putExtra("list_location", list_loc);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                }


                //return false;
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                FlockActivity.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        getAllData();

    }

    public LinkedHashMap<String, String> getList_location(){
        return list_location;
    }
    private void onTabTapped(int position) {
        switch (position) {

            case 0:
                itemList.clear();
                search.setText("");
                itemList.addAll(active);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                itemList.clear();
                search.setText("");
                itemList.addAll(inactive);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                itemList.clear();
                itemList.addAll(all);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please fill data location")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return myQuittingDialogBox;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllFlock();
    }


    public void sync_flock(View v){
        getAllFlock();

    }

    private void getAllFlock(){
//        itemList.clear();
        onTabTapped(0);
        progressDoalog = new ProgressDialog(FlockActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_flock";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        all.clear();
        inactive.clear();
        active.clear();
        itemList.clear();
        Log.d("zzz","clear all");
//        final SQLiteDatabase db = dbase.getWritableDatabase();

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<SyncFlock> call = apiInterfaceJson.getFlock(token2, parameter);

        call.enqueue(new Callback<SyncFlock>() {
            @Override
            public void onResponse(Call<SyncFlock> call, Response<SyncFlock> response) {
                SyncFlock response_data = response.body();
                List<mFlock> flockSyncList = response_data.getList_flock();
                if (response_data.getList_location() != null && response_data.getList_location().size() >0){
                    list_location = response_data.getList_location();
                    ListLocation.getInstance().setList_location(list_location);
                }
//                db.delete("FLOCK", null, null);
                if(flockSyncList!=null && flockSyncList.size()>0) {
                    for (int i = 0; i < flockSyncList.size(); i++) {
                        mFlock data = new mFlock();
//                        data.setId(id);
                        data.setName(flockSyncList.get(i).getName());
                        data.setLocation_id(flockSyncList.get(i).getLocation_id());
                        data.setLocation_name(flockSyncList.get(i).getLocation_name());
                        data.setType(flockSyncList.get(i).getType());
                        data.setPeriod(flockSyncList.get(i).getPeriod());
                        data.setStatus(flockSyncList.get(i).getStatus());
                        data.setCapacity(flockSyncList.get(i).getCapacity());
                        data.setNid(flockSyncList.get(i).getNid());
                        data.setEffective_date(flockSyncList.get(i).getEffective_date());
                        data.setDelete(flockSyncList.get(i).getDelete());
                        all.add(data);
                        if (flockSyncList.get(i).getStatus().equalsIgnoreCase("active")){
                            active.add(data);
                        }
                        else
                            inactive.add(data);
                    }
                }

                itemList.addAll(active);
                mTabLayout.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                search.setText("");
                linearLayout.setVisibility(View.VISIBLE);
//                getAllData();
                progressDoalog.dismiss();
                onTabTapped(0);
                listView.getAdapter().getCount();
                Toast.makeText(getApplicationContext(), "Total Sync of flock are:" + listView.getAdapter().getCount() , Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(Call<SyncFlock> call, Throwable t) {
                 Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Failed to load data" , Toast.LENGTH_LONG).show();
                progressDoalog.dismiss();

            }
        });
    }

    private void deleteFlock(String nid){
        String module = "layerfarm_android";
        String function_name = "delete_setting_data";

        ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(nid,"flock");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

        Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                //if (response != null && response.isSuccessful()) {
                String message = response.body();
                //Log.d("message","get_expense_general_respons : "+message);
                if (message.equalsIgnoreCase("fail")){
                    Toast.makeText(getApplicationContext(), "Flock can' delete, flock used on other transaction" , Toast.LENGTH_LONG).show();
                }

                if (message.equalsIgnoreCase("ok")){
                    Toast.makeText(getApplicationContext(), "Flock deleted" , Toast.LENGTH_LONG).show();
                    itemList.clear();

                    getAllFlock();
                }

                //}

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail Flock deleted" , Toast.LENGTH_LONG).show();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void close(View view){
        finish();
    }
}
