package com.layerfarm.setting.model;
import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("nid")
    String id;

    @SerializedName("name")
    String name;
    String address;
    String rid;

    public Location(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
