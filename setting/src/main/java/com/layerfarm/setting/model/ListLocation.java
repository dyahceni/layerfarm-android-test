package com.layerfarm.setting.model;

import java.util.HashMap;

public class ListLocation {
    private static ListLocation staticInstance;
    public static ListLocation getInstance(){
        if (staticInstance==null){
            staticInstance = new ListLocation();
        }
        return staticInstance;

    }
    private HashMap<String, String> list_location;

    public HashMap<String, String> getList_location() {
        return list_location;
    }

    public void setList_location(HashMap<String, String> list_location) {
        this.list_location = list_location;
    }
}
