package com.layerfarm.setting.model;

public class Feed {
    String id;
    String name;
    String description;
    String status;
    String rid;
    String delete_status;

     public Feed(){
     }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDeleteStatus() {
        return delete_status;
    }

    public void setDeleteStatus(String delete_status) {
        this.delete_status = delete_status;
    }
}
