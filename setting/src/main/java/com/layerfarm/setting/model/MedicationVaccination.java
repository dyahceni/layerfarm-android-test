package com.layerfarm.setting.model;

import java.util.LinkedHashMap;

public class MedicationVaccination {

    String id;
    String product_name;
    String vendor_id;
    String capacity;
    String unit;
    String type;
    String status;
    String rid;
    String delete_status;
    LinkedHashMap vendor_list;



    public MedicationVaccination() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public LinkedHashMap getVendor_list() {
        return vendor_list;
    }

    public void setVendor_list(LinkedHashMap vendor_list) {
        this.vendor_list = vendor_list;
    }

    public String getDeleteStatus() {
        return delete_status;
    }

    public void setDeleteStatus(String delete_status) {
        this.delete_status = delete_status;
    }
}
