package com.layerfarm.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterMortality;
import com.layerfarm.setting.adddata.CreateMortalityActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SyncMortality;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.Mortality;
import com.layerfarm.setting.updatedata.UpdateMortality;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MortalityActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    List<Mortality> itemList = new ArrayList<Mortality>();
    AdapterMortality adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper dbase;

    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "name";

    Toolbar toolbar ;
    TextView toolbar_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mortality);

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.mortality_category));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_mortality);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MortalityActivity.this, CreateMortalityActivity.class);
                startActivity(intent);
            }
        });

        adapter = new AdapterMortality(MortalityActivity.this, itemList);
        listView.setAdapter(adapter);

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                final String idx = itemList.get(position).getId();
                final String name = itemList.get(position).getName();

                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(MortalityActivity.this);
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(MortalityActivity.this, UpdateMortality.class);
                                intent.putExtra(TAG_ID, idx);
                                intent.putExtra(TAG_PRODUCT_NAME, name);
                                startActivity(intent);
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(MortalityActivity.this); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        SQLite.deleteMortality(Integer.parseInt(idx));
                                        //SQLite.deleteMedicationVaccination(idx);
                                        itemList.clear();
                                        getAllData();
                                    }
                                });
                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                                AlertDialog alert=builder.create();
                                alert.show();


                                break;
                        }
                    }
                }).show();
                //return false;
            }
        });

        getAllData();

    }


    private void getAllData() {
        ArrayList<HashMap<String, String>> row = SQLite.getAllMortality();
        itemList.clear();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get(TAG_ID);
            String product_name = row.get(i).get(TAG_PRODUCT_NAME);


            Mortality data = new Mortality();

            data.setId(id);
            data.setName(product_name);


            itemList.add(data);
        }

        adapter.notifyDataSetChanged();
    }

    public void sync(View v){
        String module = "layerfarm";
        String function_name = "layerfarm_get_available_mortality";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<SyncMortality>> call = apiInterfaceJson.getMortality(token2, parameter);

        call.enqueue(new Callback<List<SyncMortality>>() {
            @Override
            public void onResponse(Call<List<SyncMortality>> call, Response<List<SyncMortality>> response) {
                List<SyncMortality> mortalitySyncList = response.body();
//                HashMap<String, String> all_ovk = new HashMap<>();

                ArrayList<HashMap<String, String>> row = SQLite.getAllMortality();
                List<String> data = new ArrayList<>();
                for (int i = 0; i < row.size(); i++) {
                    String name = row.get(i).get("name").toLowerCase();
                    data.add(name);
                }
                for (int i =0; i< mortalitySyncList.size() ; i++){
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("key", mortalitySyncList.get(i).getHuman_name().toLowerCase());
                    map.put("name", mortalitySyncList.get(i).getHuman_name());
                    map.put("machine_name", mortalitySyncList.get(i).getOld_name());
//
                    InsertDataMortality(map, data);
                }

                Toast.makeText(getApplicationContext(), "Create Mortality Successfully", Toast.LENGTH_LONG).show();
                getAllData();
            }
            @Override
            public void onFailure(Call<List<SyncMortality>> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });

    }

    public void InsertDataMortality(HashMap<String, String> map , List<String> data){
        SQLiteDatabase db = dbase.getWritableDatabase();
        String key = map.get("key");
        if (!data.contains(key)) {
            Log.d("Layerfarm","noo");
            db.execSQL("insert into mortality_category(name, machine_name) values('" +
                    map.get("name")+"', '"+
                    map.get("machine_name") + "')");

        }
        else {
            Log.d("Layerfarm", "adaa");

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
        getAllData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void close(View view){
        finish();
    }
}
