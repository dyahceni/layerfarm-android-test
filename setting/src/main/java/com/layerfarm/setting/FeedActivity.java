package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterFeed;
import com.layerfarm.setting.adddata.CreateFeedActivity;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.mFeed;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.Feed;
import com.layerfarm.setting.updatedata.UpdateFeed;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedActivity extends AppCompatActivity {

    private String TAG = SyncLayerfarmActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    public LayerFarm layerFarm;
    DatabaseHelper dbase;
    ListView listView;
    AlertDialog.Builder dialog;
    List<Feed> itemList = new ArrayList<Feed>();
    List<Feed> active = new ArrayList<>();
    List<Feed> inactive = new ArrayList<>();
    List<Feed> all = new ArrayList<>();
    AdapterFeed adapter;
    DataHelper SQLite = new DataHelper(this);

    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_STATUS = "status";
    public static final String TAG_RID = "rid";
    public static final String TAG_DELETE_STATUS = "delete_status";

    EditText search;
    LinearLayout linearLayout;
    ProgressDialog progressDoalog;
    TabLayout mTabLayout;

    Toolbar toolbar ;
    TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed);

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main_feed);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.feed));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_feed);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FeedActivity.this, CreateFeedActivity.class);
                intent.putExtra("feed","feed");
                startActivity(intent);
            }
        });

        mTabLayout = (TabLayout)findViewById(R.id.tabs);
        adapter = new AdapterFeed(FeedActivity.this, itemList);
        listView.setAdapter(adapter);

//        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                           final int position, long id) {
                Feed item = (Feed) adapter.getItem(position);
                // TODO Auto-generated method stub
                final String idx = item.getId();
                final String name = item.getName();
                final String desc = item.getDescription();
                final String status = item.getStatus();
                final String rid = item.getRid();
                final String delete_status = item.getDeleteStatus();


                dialog = new AlertDialog.Builder(FeedActivity.this);
                dialog.setCancelable(true);
                if (delete_status.equals("0")){
                    final CharSequence[] dialogitem = {"Edit", "Delete"};
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(FeedActivity.this, UpdateFeed.class);
                                    intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_NAME, name);
                                    intent.putExtra(TAG_DESCRIPTION, desc);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_RID, rid);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    AlertDialog.Builder builder=new AlertDialog.Builder(FeedActivity.this); //Home is name of the activity
                                    builder.setMessage("Do you want to delete this data?");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            deleteFeed(rid);
                                            //SQLite.deleteFeed(Integer.parseInt(idx));
                                            //SQLite.deleteMedicationVaccination(idx);


                                        }
                                    });

                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                                    AlertDialog alert=builder.create();
                                    alert.show();
                                    break;
                            }
                        }
                    }).show();
                } else {
                    final CharSequence[] dialogitem = {"Edit"};
                    dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(FeedActivity.this, UpdateFeed.class);
                                    intent.putExtra(TAG_ID, idx);
                                    intent.putExtra(TAG_NAME, name);
                                    intent.putExtra(TAG_DESCRIPTION, desc);
                                    intent.putExtra(TAG_STATUS, status);
                                    intent.putExtra(TAG_RID, rid);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                }
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                FeedActivity.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //getAllData();
//        getDataFeed();

    }
    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                itemList.clear();
                search.setText("");
                itemList.addAll(active);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                itemList.clear();
                search.setText("");
                itemList.addAll(inactive);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                itemList.clear();
                itemList.addAll(all);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }

    private void getAllData() {
        List<HashMap<String, String>> row = SQLite.getAllFeed();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get(TAG_ID);
            String name = row.get(i).get(TAG_NAME);
            String desc = row.get(i).get(TAG_DESCRIPTION);
            String status = row.get(i).get(TAG_STATUS);
            String rid = row.get(i).get(TAG_RID);
            String delete_status = row.get(i).get(TAG_DELETE_STATUS);


            Feed data = new Feed();


            data.setId(id);
            data.setName(name);
            data.setDescription(desc);
            data.setStatus(status);
            data.setRid(rid);


            itemList.add(data);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        itemList.clear();

        //getAllData();
        getDataFeed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

   /* private class GetFeed extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(FeedActivity.this);
            pDialog.setMessage("Syncronizing start please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            layerFarm.processToken1();
            layerFarm.getToken1();
            layerFarm.processToken2();
            layerFarm.getToken2();
            //layerFarm.getFarmData();
            layerFarm.processGetFeed();
            List<com.layerfarm.layerfarm.model.Feed> feed = layerFarm.getFeedResult();
            SQLiteDatabase db = dbase.getWritableDatabase();
            if(feed!=null && feed.size()>0) {
                for (int i = 0; i < feed.size(); i++) {
                    Log.d("ssss", "name = " + feed.get(i).getName());
                    Cursor result = db.rawQuery("SELECT NAME FROM FEED WHERE NAME='" + feed.get(i).getName()+"'", null);
                    if (result.getCount() == 0) {
                        db.execSQL("insert into feed(name, description, status) values('" +
                                feed.get(i).getName() + "','" +
                                "" + "','" +
                                "" + "')");

                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            //finish();
            itemList.clear();
            getAllData();
            listView.getAdapter().getCount();
            Toast.makeText(getApplicationContext(), "Total Sync of Feed are:" + listView.getAdapter().getCount() , Toast.LENGTH_LONG).show();
        }
    }*/

    public void sync_feed(View v){

//        itemList.clear();
        getDataFeed();

    }


    private void getDataFeed(){
        all.clear();
        inactive.clear();
        active.clear();
        itemList.clear();

        progressDoalog = new ProgressDialog(FeedActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_feed";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);

        final SQLiteDatabase db = dbase.getWritableDatabase();

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<mFeed>> call = apiInterfaceJson.getFeed(token2, parameter);

        call.enqueue(new Callback<List<mFeed>>() {
            @Override
            public void onResponse(Call<List<mFeed>> call, Response<List<mFeed>> response) {
                List<mFeed> feedSyncList = response.body();
//                db.delete("FEED", null, null);
                if(feedSyncList!=null && feedSyncList.size()>0) {
                    for (int i = 0; i < feedSyncList.size(); i++) {
                        Feed data = new Feed();
                        data.setName(feedSyncList.get(i).getName());
                        data.setDescription(feedSyncList.get(i).getDescription());
                        data.setStatus(feedSyncList.get(i).getStatus());
                        data.setRid(feedSyncList.get(i).getNid());
                        data.setDeleteStatus(feedSyncList.get(i).getDeleteStatus());
                        all.add(data);
//                        if (!feedSyncList.get(i).getStatus().isEmpty()) {
                            if (feedSyncList.get(i).getStatus().equalsIgnoreCase("active")) {
                                active.add(data);
                            } else
                                inactive.add(data);
//                        }
                        Log.d("ssss", "nid = " + feedSyncList.get(i).getNid());
                        Log.d("ssss", "name = " + feedSyncList.get(i).getName());
                        Log.d("ssss", "status = " + feedSyncList.get(i).getStatus());
                        Log.d("ssss", "description = " + feedSyncList.get(i).getDescription());
                        Log.d("ssss", "delete_status = " + feedSyncList.get(i).getDeleteStatus());

                        //Cursor result = db.rawQuery("SELECT NAME FROM FEED WHERE NAME='" + feedSyncList.get(i).getName()+"'", null);
                        //if (result.getCount() == 0) {
//                        db.execSQL("insert into feed(name, status, description,rid) values('" +
//                                feedSyncList.get(i).getName() + "','" +
//                                feedSyncList.get(i).getStatus() + "','" +
//                                feedSyncList.get(i).getDescription() + "','" +
//                                feedSyncList.get(i).getNid() + "')");
                        //}
                    }
                }
                itemList.addAll(active);


                mTabLayout.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                search.setText("");
                linearLayout.setVisibility(View.VISIBLE);
//                getAllData();
                progressDoalog.dismiss();
                onTabTapped(0);
//                itemList.clear();
//                getAllData();
//                listView.getAdapter().getCount();
//                Toast.makeText(getApplicationContext(), "Total Sync of feed are:" + listView.getAdapter().getCount() , Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(Call<List<mFeed>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Failed to load data" + t.getMessage() , Toast.LENGTH_LONG).show();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());

            }
        });
    }

    private void deleteFeed(String nid){
        String module = "layerfarm_android";
        String function_name = "delete_setting_data";

        ParameterDeleteDataSetting.Arguments args = new ParameterDeleteDataSetting.Arguments(nid,"feed");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterDeleteDataSetting parameter = new ParameterDeleteDataSetting(module, function_name, args);

        Call<String> call = apiInterfaceJson.layerfarm_delete_data_setting(token2, parameter);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                //if (response != null && response.isSuccessful()) {
                    String message = response.body();
                    //Log.d("message","get_expense_general_respons : "+message);
                if (message.equalsIgnoreCase("fail")){
                    Toast.makeText(getApplicationContext(), "Feed can' delete, feed used on other transaction" , Toast.LENGTH_LONG).show();
                }

                if (message.equalsIgnoreCase("ok")){
                    Toast.makeText(getApplicationContext(), "Feed deleted" , Toast.LENGTH_LONG).show();
                    itemList.clear();
                    all.clear();
                    inactive.clear();
                    active.clear();
                    getDataFeed();
                }

                //}

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail Feed deleted" , Toast.LENGTH_LONG).show();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void close(View view){
        finish();
    }
}
