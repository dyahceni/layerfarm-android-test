package com.layerfarm.setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterVendor;
import com.layerfarm.setting.adddata.CreateVendorActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SyncVendor;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.Vendor;
import com.layerfarm.setting.updatedata.UpdateVendor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    List<Vendor> itemList = new ArrayList<Vendor>();
    AdapterVendor adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper dbase;

    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "name";
    public static final String TAG_RID = "rid";

    Toolbar toolbar ;
    TextView toolbar_title;
    List<SyncVendor> vendorSyncList = new ArrayList<>();
    ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(R.string.vendor);

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_vendor);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VendorActivity.this, CreateVendorActivity.class);
                startActivity(intent);
            }
        });

        adapter = new AdapterVendor(VendorActivity.this, itemList);
        listView.setAdapter(adapter);

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                final String idx = itemList.get(position).getId();
                final String name = itemList.get(position).getName();
                final String rid = itemList.get(position).getRid();

                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(VendorActivity.this);
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(VendorActivity.this, UpdateVendor.class);
                                intent.putExtra(TAG_ID, idx);
                                intent.putExtra(TAG_PRODUCT_NAME, name);
                                intent.putExtra(TAG_RID, rid);
                                startActivity(intent);
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(VendorActivity.this); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        delete(rid, name, idx);
                                        //SQLite.deleteMedicationVaccination(idx);



                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();


                                break;
                        }
                    }
                }).show();
                //return false;
            }
        });

//        getAllData();

    }


    private void getAllData() {
        ArrayList<HashMap<String, String>> row = SQLite.getAllVendorList();
        itemList.clear();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get("id");
            String product_name = row.get(i).get("name");
            String rid = row.get(i).get("rid");
            Vendor data = new Vendor();
            data.setId(id);
            data.setName(product_name);
            data.setRid(rid);
            itemList.add(data);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemList.clear();
//        getAllData();
        sync();
    }

    public void sync(){
        progressDoalog = new ProgressDialog(VendorActivity.this);
        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        String module = "ovk";
        String function_name = "ovk_get_vendor";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<SyncVendor>> call = apiInterfaceJson.getVendor(token2, parameter);

        call.enqueue(new Callback<List<SyncVendor>>() {

            @Override
            public void onResponse(Call<List<SyncVendor>> call, Response<List<SyncVendor>> response) {
                ArrayList<HashMap<String, String>> row = SQLite.getAllVendorList();
                List<String> data = new ArrayList<>();
                for (int i = 0; i < row.size(); i++) {

                    String name = row.get(i).get("name").toLowerCase();
                    data.add(name);
                }
                vendorSyncList = response.body();
                for (int i =0; i< vendorSyncList.size() ; i++){
                    Vendor vendor = new Vendor();
                    vendor.setName(vendorSyncList.get(i).getName());
                    vendor.setRid(vendorSyncList.get(i).getRid());
                    itemList.add(vendor);
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    map.put("key", vendorSyncList.get(i).getName().toLowerCase());
//                    map.put("name", vendorSyncList.get(i).getName());
//                    map.put("rid", vendorSyncList.get(i).getRid());
////
//                    InsertDataVendor(map, data);
                }
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Create Vendor Successfully", Toast.LENGTH_LONG).show();
                progressDoalog.dismiss();
//                getAllData();

            }

            @Override
            public void onFailure(Call<List<SyncVendor>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Filed to load data", Toast.LENGTH_LONG).show();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void delete(String rid, String vendor_name, final String idx){
        String module = "ovk";
        String function_name = "ovk_delete_vendor";
        String[] args = {rid};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.deleteVendor(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] vendorDelete = response.body();
//                SQLite.deleteVendor(Integer.parseInt(idx));
                itemList.clear();
                sync();
                Toast.makeText(getApplicationContext(), "Delete Vendor Successfully", Toast.LENGTH_LONG).show();
//                getAllData();
            }
            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Delete Vendor Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void InsertDataVendor(HashMap<String, String> map , List<String> data){
        SQLiteDatabase db = dbase.getWritableDatabase();
        String key = map.get("key");
        if (!data.contains(key)) {
            Log.d("Layerfarm","noo");
            db.execSQL("insert into vendor(name,rid) values('" +
                    map.get("name") + "','" +
                    map.get("rid") + "')");

        }
        else {
            Log.d("Layerfarm", "adaa");

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void close(View view){
        finish();
    }
}
