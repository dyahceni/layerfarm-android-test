package com.layerfarm.setting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.layerfarm.setting.adapter.AdapterUser;
import com.layerfarm.setting.adddata.CreateUserActivity;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterMember;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.UserMember;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity implements PurchasesUpdatedListener, AcknowledgePurchaseResponseListener {
    ListView listView;
    Toolbar toolbar ;
    TextView toolbar_title;
    List<UserMember.Users> itemList = new ArrayList<UserMember.Users>();
    ArrayList<UserMember.Users> list_all = new ArrayList<UserMember.Users>();
    ArrayList<UserMember.Users> list_active = new ArrayList<UserMember.Users>();
    ArrayList<UserMember.Users> list_inactive = new ArrayList<UserMember.Users>();
    AdapterUser adapter;
    UserMember member = new UserMember();
    UserMember all = new UserMember();
    UserMember active = new UserMember();
    UserMember inactive = new UserMember();
    ProgressDialog progressDoalog;
    ProgressBar progressBar;
    EditText search;
    LinearLayout linearLayout;
    TabLayout mTabLayout;
    ArrayList<UserMember.Users> users = new ArrayList<>();
    LinkedHashMap<Integer, String> house = new LinkedHashMap<>();
    FloatingActionButton fab;

    private Boolean isAlreadyPurchase;
    private BillingClient billingClient;
    ArrayList<String> member_items = new ArrayList<>();
    int items = 0;

    //billing
    private String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlpBRn+ek2E7Zb/HfPM4Z4rx3TIFo92h7XNPDMjfRDx7cBqEaJNo0WLe/b+9YNkHGD6D5rtD0d4Gta5KhEfzSRbQiEbqSQzlzJGGsl9uSqbLE4k8WS2apTgukU9n5pCdapeAp0613CmM7LoQjEDnknwVEOukCZjPExEPgLOU2rpq0vGibYaDeTsw3TGZboIIs/IP19Q4EmwmfZ0NtqWd/EndAwGRRax2fvZwp6Hq7OTIRTa/dAKriuABUQtOA272vj9ASrEG88UCAX4AycuBQeXr3IrMgg1XvvEZdxM7l3C5PfmtH+wroetcQ01SFrWEVqm7NUB4cJBgGn15aNhi1wwIDAQAB";
    private String SKU_REMOVE_ADS = "lfm1.0alfa_member";
    private int RC_REQUEST = 115;
    private String payload = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_user);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(R.string.member);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout_search);
        search = (EditText) findViewById(R.id.search);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (getSettingBoolean(UserActivity.this, "showads", true)) {
//                    if (mHelper != null) {
//                        purchaseRemoveAds();
//                    }
//                }
//                Intent intent = new Intent(UserActivity.this, CreateUserActivity.class);
                LinkedHashMap<Integer, String> location = house;
                Intent intent = new Intent(UserActivity.this, CreateUserActivity.class);
//                intent.putExtra("user", (Serializable) users);
                intent.putExtra("location", (Serializable) location);
                startActivity(intent);
            }
        });
        mTabLayout = (TabLayout)findViewById(R.id.tabs);

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        listView = (ListView) findViewById(R.id.list_view_user);
        onTabTapped(0);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserMember.FieldManagedLocation item;
                ParameterMember.Users users = new ParameterMember.Users();
                UserMember.Users items = (UserMember.Users)adapter.getItem(position);
                JsonElement data = items.getFieldManagedLocation();
                item = new UserMember.FieldManagedLocation();
                try{
                    if(data.isJsonArray()){
                        //Array
    //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is array");
                        item = new UserMember.FieldManagedLocation();
                    } else {
                        //Object
    //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is object");
                        Type type = new TypeToken<UserMember.FieldManagedLocation>() {
                        }.getType();
                        item = new Gson().fromJson(data, type);
                    }
                }catch (Exception e){}
                ArrayList<String> loc = new ArrayList<>();
                if (!item.getUnd().isEmpty()){
                    for (int i =0; i< item.getUnd().size(); i++) {
                        loc.add(item.getUnd().get(i).getTarget_id());
                    }
                }
                users.setUid(items.getUid());
                users.setStatus(items.getStatus());
                users.setRoles(new ArrayList<String>(items.getRoles().values()));
                users.setPass(items.getPass());
                users.setMail(items.getMail());
                users.setName(items.getName());
                users.setLanguage(items.getLanguage());
                users.setLocation(loc);
                LinkedHashMap<Integer, String> location = house;
                Intent intent = new Intent(UserActivity.this, CreateUserActivity.class);
                intent.putExtra("user", (Serializable) users);
                intent.putExtra("location", (Serializable) location);
                startActivity(intent);
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                Log.d("zzz","text "+s);
                UserActivity.this.adapter.getFilter().filter(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                itemList.clear();
                itemList.addAll(list_all);
                adapter = new AdapterUser(UserActivity.this, all, items);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                itemList.clear();
//                search.setText("");
                itemList.addAll(list_active);
                adapter = new AdapterUser(UserActivity.this, active, items);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                itemList.clear();
//                search.setText("");
                itemList.addAll(list_inactive);
                adapter = new AdapterUser(UserActivity.this, inactive, items);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        member = new UserMember();
        itemList.clear();
        Sync();
//        itemList.clear();
//        getAllData();
    }

    //    private void getAllData() {
//        ArrayList<HashMap<String, String>> row = SQLite.getAllBuyerList();
//        itemList.clear();
//        for (int i = 0; i < row.size(); i++) {
//            String id = row.get(i).get("id");
//            String name = row.get(i).get("name");
//            String rid = row.get(i).get("rid");
//            String address = row.get(i).get("address");
//
//            SyncBuyer data = new SyncBuyer(id, name, rid, address);
//            itemList.add(data);
//        }
//
//        adapter.notifyDataSetChanged();
//    }
    public void Sync(){
        itemList.clear();
        progressDoalog = new ProgressDialog(UserActivity.this);
        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        member = new UserMember();
        String module = "layerfarm_android";
        String function_name = "layerfarm_get_user";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<UserMember> call = apiInterfaceJson.getMember(token2, parameter);

        call.enqueue(new Callback<UserMember>() {

            @Override
            public void onResponse(Call<UserMember> call, Response<UserMember> response) {
                member = response.body();

                billingClient = BillingClient.newBuilder(UserActivity.this)
                        .setListener(UserActivity.this).enablePendingPurchases().build();
                billingClient.startConnection(new BillingClientStateListener() {
                    @Override
                    public void onBillingSetupFinished(BillingResult billingResult) {
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            // The BillingClient is ready. You can query purchases here.
                            Toast.makeText(UserActivity.this, "Setup Blling OK", Toast.LENGTH_SHORT).show();
                            queryOwned();
                            String[] value = new String[0];

                            if (member_items.size() > 0){
                                if (member_items.get(0).equals("lfm1.0alfa_member_05")){
                                    items = 5;
                                }
                                if (member_items.get(0).equals("lfm1.0alfa_member_10")){
                                    items = 10;
                                }
                                if (member_items.get(0).equals("lfm1.0alfa_member_15")){
                                    items = 15;
                                }
                                if (member_items.get(0).equals("lfm1.0alfa_member_20")){
                                    items = 20;
                                }
                            }
//                    adapter= new ArrayAdapter<String>(RegisterMemberAccount.this, android.R.layout.simple_list_item_1);
//                    listView.setAdapter(new ArrayAdapter(RegisterMemberAccount.this, android.R.layout.simple_list_item_1, value));
                        } else {
                            showResponseCode(responseCode);
                        }
                    }

                    @Override
                    public void onBillingServiceDisconnected() {
                        // Try to restart the connection on the next request to
                        // Google Play by calling the startConnection() method.
                        Toast.makeText(UserActivity.this, "Billing Service Disconnect", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.d("Layerfarm", "data = "+member.getHouse());
                users = member.getUsers();
                house = member.getHouse();
                for (int i =0; i< users.size() ; i++){
                    UserMember.Users user = new UserMember.Users();
                    user.setFieldManagedLocation(users.get(i).getFieldManagedLocation());
                    user.setLanguage(users.get(i).getLanguage());
                    user.setMail(users.get(i).getMail());
                    user.setName(users.get(i).getName());
                    user.setPass(users.get(i).getPass());
                    user.setRoles(users.get(i).getRoles());
                    user.setStatus(users.get(i).getStatus());
                    user.setUid(users.get(i).getUid());
                    Log.d("zzz"," name = "+users.get(i).getName());
                    list_all.add(user);
                    if (users.get(i).getStatus().equalsIgnoreCase("1")){
                        list_active.add(user);
                    }
                    else
                        list_inactive.add(user);
                }
//                if (users.size() > items){
//                    fab.setVisibility(View.GONE);
//                }
                all.setHouse(house);
                all.setUsers(list_all);
                active.setHouse(house);
                active.setUsers(list_active);
                inactive.setHouse(house);
                inactive.setUsers(list_inactive);

                itemList = member.getUsers();
                adapter = new AdapterUser(UserActivity.this, member, items);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mTabLayout.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.VISIBLE);
                search.setText("");
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<UserMember> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                progressDoalog.dismiss();
                finish();
            }
        });
    }

    public static void saveSetting(Context ctx, String key, Object save) {
        SharedPreferences gameSettings = ctx.getSharedPreferences("apppro", 0);
        SharedPreferences.Editor prefEditor = gameSettings.edit();
        if (save instanceof Boolean) {
            prefEditor.putBoolean(key, (Boolean) save);
        } else if (save instanceof String) {
            prefEditor.putString(key, (String) save);
        }
        prefEditor.commit();
    }
    public static Boolean getSettingBoolean(Context ctx, String key, Boolean defValueStr) {
        SharedPreferences settings = ctx.getSharedPreferences("apppro", 0);
        Boolean cekSetting = settings.getBoolean(key, defValueStr);
        return cekSetting;
    }

    void queryOwned(){
        StringBuffer resultStr = new StringBuffer("");
        Purchase.PurchasesResult purchasesResult
                = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
        int responseCode = purchasesResult.getResponseCode ();
        if(responseCode== BillingClient.BillingResponseCode.OK){
            resultStr.append("Query Success\n");
            List<Purchase> purchases = purchasesResult.getPurchasesList();
            if(purchases.isEmpty()){
                resultStr.append("Owned Nothing");
            } else {
                for (Purchase purchase : purchases) {
                    resultStr.append(purchase.getSku()).append("\n");
                    resultStr.append(purchase.getPurchaseState()).append("\n");
                    resultStr.append(purchase.isAutoRenewing()).append("\n");
                    if (purchase.isAutoRenewing()){
                        if (!purchase.getSku().equals("lfm1.0alfa_1")) {
                            member_items.add(purchase.getSku());
                        }
                    }
                }

            }
            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        }else{
            showResponseCode(responseCode);
        }
    }

    String handlePurchase(Purchase purchase) {
        String stateStr = "error";
        int purchaseState = purchase.getPurchaseState();
        if (purchaseState == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.
            stateStr = "purchased";
            // Acknowledge the purchase if it hasn't already been acknowledged.
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, this);
            }
        }else if(purchaseState == Purchase.PurchaseState.PENDING){
            stateStr = "pending";
        }else if(purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE){
            stateStr = "unspecified state";
        }
        return stateStr;
    }

    @Override
    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
        int responseCode = billingResult.getResponseCode();
        if(responseCode != BillingClient.BillingResponseCode.OK) {
            showResponseCode(responseCode);
        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<com.android.billingclient.api.Purchase> purchases) {
        StringBuffer resultStr = new StringBuffer("");
        int billingResultCode = billingResult.getResponseCode();
        if (billingResultCode == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                String state = handlePurchase(purchase);
                String sku = purchase.getSku();
                resultStr.append(sku).append("\n");
                resultStr.append(" State=").append(state).append("\n");
            }
            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        } else {
            // Handle error codes.
            showResponseCode(billingResultCode);
        }
    }

    void showResponseCode(int responseCode){
        switch(responseCode){
            case BillingClient.BillingResponseCode.OK:
                // textView1.setText("OK");
                Log.d("resultStr", " "+ "OK");
                break;
            case BillingClient.BillingResponseCode.USER_CANCELED:
                //textView1.setText("USER_CANCELED");
                Log.d("resultStr", " "+ "USER CANCELLED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                //textView1.setText("SERVICE_UNAVAILABLE");
                Log.d("resultStr", " "+ "SERVICE_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                Log.d("resultStr", " "+ "BILLING_UNAVAILABLE");
                //textView1.setText("BILLING_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                Log.d("resultStr", " "+ "ITEM_UNAVAILABLE");
                //textView1.setText("ITEM_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                Log.d("resultStr", " "+ "DEVELOPER_ERROR");
                //textView1.setText("DEVELOPER_ERROR");
                break;
            case BillingClient.BillingResponseCode.ERROR:
                Log.d("resultStr", " "+ "ERROR");
                //textView1.setText("ERROR");
                break;
            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                Log.d("resultStr", " "+ "ITEM_ALREADY_OWNED");
                //textView1.setText("ITEM_ALREADY_OWNED");
                break;
            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                Log.d("resultStr", " "+ "ITEM_NOT_OWNED");
                //textView1.setText("ITEM_NOT_OWNED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                Log.d("resultStr", " "+ "SERVICE_DISCONNECTED");
                //textView1.setText("SERVICE_DISCONNECTED");
                break;
            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                Log.d("resultStr", " "+ "FEATURE_NOT_SUPPORTED");
                //textView1.setText("FEATURE_NOT_SUPPORTED");
                break;
        }
    }
    public void close(View view){
        finish();
    }
}
