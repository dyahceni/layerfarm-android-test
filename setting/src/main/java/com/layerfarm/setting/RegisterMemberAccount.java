package com.layerfarm.setting;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//billing client API
import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.layerfarm.setting.adapter.AdapterListActive;
import com.layerfarm.setting.adapter.AdapterListInactive;
import com.layerfarm.layerfarm.model.ParamaterActivateMember;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.UserMember;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterMemberAccount extends AppCompatActivity implements PurchasesUpdatedListener, AcknowledgePurchaseResponseListener {

    //billing api
    private BillingClient billingClient;
    List<SkuDetails> mySkuDetailsList;
    private final String PURCHASE_ID = "lfm1.0alfa_1";
    ListView listView, lv_inactive;
    ArrayList<String> member_items = new ArrayList<>();
    AdapterListActive adapter_active;
//    ArrayAdapter<String> adapter_inactive;
    AdapterListInactive adapter_inactive;
    TextView member_joined;
    List<UserMember.Users> itemList = new ArrayList<UserMember.Users>();
    String expired_date;
//    ArrayList<UserMember.Users> list_all = new ArrayList<UserMember.Users>();
//    ArrayList<UserMember.Users> list_active = new ArrayList<UserMember.Users>();
//    ArrayList<UserMember.Users> list_inactive = new ArrayList<UserMember.Users>();
    UserMember member = new UserMember();
    UserMember all = new UserMember();
//    UserMember active = new UserMember();
//    UserMember inactive = new UserMember();
    ProgressDialog progressDoalog;
    ArrayList<UserMember.Users> users = new ArrayList<>();
    ArrayList<UserMember.Users> list_all = new ArrayList<>();
    int items = 0;
    HashMap<Integer, String> active_original = new HashMap<>();
    HashMap<Integer, String> inactive_original = new HashMap<>();
    ArrayList<String> active = new ArrayList<>();
    ArrayList<String> inactive = new ArrayList<>();
    ArrayList<String> submit = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_subscription);
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText(R.string.activation);
        Sync();
        listView = (ListView) findViewById(R.id.list_view_user);
        lv_inactive = (ListView) findViewById(R.id.inactive_user);
        member_joined = (TextView) findViewById(R.id.member_joined);
        billingClient = BillingClient.newBuilder(this)
                .setListener(this).enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                int responseCode = billingResult.getResponseCode();
                if (responseCode == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    Toast.makeText(RegisterMemberAccount.this, "Setup Blling OK", Toast.LENGTH_SHORT).show();
                    queryOwned();
                    if (!member_items.isEmpty()){
                        String[] value = new String[0];
                        value = member_items.get(0).split("_");
                        member_joined.setText(value[1]);
                    }





//                    adapter= new ArrayAdapter<String>(RegisterMemberAccount.this, android.R.layout.simple_list_item_1);
//                    listView.setAdapter(new ArrayAdapter(RegisterMemberAccount.this, android.R.layout.simple_list_item_1, value));
                } else {
                    showResponseCode(responseCode);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Toast.makeText(RegisterMemberAccount.this, "Billing Service Disconnect", Toast.LENGTH_SHORT).show();
            }
        });
//        queryOwned();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("zzz","click "+position);
                inactive.add((String) adapter_active.getItem(position));
                active.remove(position);
                adapter_active.notifyDataSetChanged();
                adapter_inactive.notifyDataSetChanged();
//                new ArrayAdapter(RegisterMemberAccount.this, android.R.layout.simple_list_item_1, inactive).notifyDataSetChanged();
            }
        });
        lv_inactive.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("zzz","click "+position);
                active.add((String) adapter_inactive.getItem(position));
                inactive.remove(position);
                adapter_active.notifyDataSetChanged();
                adapter_inactive.notifyDataSetChanged();
//                new ArrayAdapter(RegisterMemberAccount.this, android.R.layout.simple_list_item_1, inactive).notifyDataSetChanged();
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void close(View view){
        finish();
    }

    public void Sync(){
        itemList.clear();
        active.clear();
        inactive.clear();
        active_original = new HashMap<>();
        inactive_original = new HashMap<>();
        progressDoalog = new ProgressDialog(RegisterMemberAccount.this);
        progressDoalog.setMax(100);
//        progressDoalog.setMessage("Its loading....");
//        progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();
        member = new UserMember();
        String module = "layerfarm_android";
        String function_name = "layerfarm_get_user";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<UserMember> call = apiInterfaceJson.getMember(token2, parameter);

        call.enqueue(new Callback<UserMember>() {

            @Override
            public void onResponse(Call<UserMember> call, Response<UserMember> response) {
                member = response.body();

                Log.d("Layerfarm", "data = "+member.getHouse());
                users = member.getUsers();
//                house = member.getHouse();
                for (int i =0; i< users.size() ; i++){
                    UserMember.Users user = new UserMember.Users();
                    user.setFieldManagedLocation(users.get(i).getFieldManagedLocation());
                    user.setLanguage(users.get(i).getLanguage());
                    user.setMail(users.get(i).getMail());
                    user.setName(users.get(i).getName());
                    user.setPass(users.get(i).getPass());
                    user.setRoles(users.get(i).getRoles());
                    user.setStatus(users.get(i).getStatus());
                    user.setUid(users.get(i).getUid());
                    Log.d("zzz"," name = "+users.get(i).getName());
                    list_all.add(user);
                    if (users.get(i).getStatus().equalsIgnoreCase("1")){
                        active.add(users.get(i).getName());
                        active_original.put(i,users.get(i).getName());
                    }
                    else {
                        inactive.add(users.get(i).getName());
                        inactive_original.put(i,users.get(i).getName());
                    }
                }
//                for (int i =0; i< active_original.size(); i++){
                    Log.d("zzz","active original = "+active_original.values());
//                }
//                if (users.size() > items){
//                    fab.setVisibility(View.GONE);
//                }
//                all.setHouse(house);
//                all.setUsers(list_all);
//                active.setHouse(house);
//                active.setUsers(list_active);
//                inactive.setHouse(house);
//                inactive.setUsers(list_inactive);

                adapter_active= new AdapterListActive(RegisterMemberAccount.this, active, active_original);
//                adapter_inactive = new ArrayAdapter<String>(RegisterMemberAccount.this, android.R.layout.simple_list_item_1, inactive);
                adapter_inactive = new AdapterListInactive(RegisterMemberAccount.this, inactive, inactive_original);
                    listView.setAdapter(adapter_active);
                    lv_inactive.setAdapter(adapter_inactive);
                itemList = member.getUsers();
//                adapter = new AdapterUser(RegisterMemberAccount.this, member, items);
//                listView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                mTabLayout.setVisibility(View.VISIBLE);
//                linearLayout.setVisibility(View.VISIBLE);
//                search.setText("");
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<UserMember> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                progressDoalog.dismiss();
                finish();
            }
        });
    }

    public void increase(View v){
        int val = Integer.parseInt(member_joined.getText().toString());
        val = val + 5;
        member_joined.setText(String.valueOf(val));
    }
    public void decrease(View v){
        int val = Integer.parseInt(member_joined.getText().toString());
        if (val != 0) {
            val = val - 5;
        }
        else {
            Toast.makeText(this, "0 member join", Toast.LENGTH_SHORT).show();
        }
        member_joined.setText(String.valueOf(val));
    }
    public void Submit(View v){
//        ActivatedMember();
        queryOwned();
        String member_val = String.valueOf(listView.getAdapter().getCount());
        String sku = "member_"+member_val;
        Log.d("zzz","sku Submit = "+sku);
        querySkuList(sku);
        submit = active;
        Log.d("zzz","member items ="+member_items.toString());
        Log.d("zzz","member items size ="+member_items.size());
        for (int i =0; i< member_items.size(); i++){
            Log.d("zzz","member items value = "+member_items.get(i));
        }

//        queryPurchaseHistory();
        if (member_items.size() > 0){
            Log.d("zzz","member_items = "+member_items);
            Upgrade(sku,member_items.get(0));
        }
        else {
            startPurchase(sku);
        }


    }
    public void Cancel(View v){
        queryOwned();
        String sku = member_items.get(0);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/account/subscriptions?package=com.layerfarm.layerfarmmanager&sku="+sku));
        startActivity(browserIntent);
    }
    public void ActivatedMember(String exp_date){
        progressDoalog = new ProgressDialog(RegisterMemberAccount.this);
        progressDoalog.setMax(100);

        HashMap<String, String> value = new HashMap<>();
        for (int i=0; i < active.size(); i++){
            value.put(active.get(i), "1");
        }
        for (int a=0; a < inactive.size(); a++){
            value.put(inactive.get(a), "0");
        }


        String module = "layerfarm_android";
        String function_name = "layerfarm_android_set_user_activate";
        ParamaterActivateMember.ActivateMember args = new ParamaterActivateMember.ActivateMember(value, exp_date);


        ParamaterActivateMember parameter = new ParamaterActivateMember(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.activated_user(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
//                String[] member = response.body();
//                Log.d("zzz", member.toString());
                progressDoalog.dismiss();
                Sync();
//                submit.remove(0);
////                submit.notifyAll();
//                if (submit.size()>0){
//                    Log.d("zzz","activated = "+submit.get(0));
////                    ActivatedMember();
//                }
//                else {
//                    Toast.makeText(RegisterMemberAccount.this, "Success", Toast.LENGTH_SHORT).show();
//                }
//                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                progressDoalog.dismiss();
                Toast.makeText(RegisterMemberAccount.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        billingClient.endConnection();
        super.onDestroy();
    }


    void querySkuList(String sku) {
        List skuList = new ArrayList<>();
        skuList.add(sku);
/*        skuList.add("android.test.purchased");  // prepared by Google
        skuList.add("android.test.canceled");
        skuList.add("android.test.refunded");
        skuList.add("android.test.item_unavailable");*/
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        //params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        StringBuffer resultStr = new StringBuffer("");
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            mySkuDetailsList = skuDetailsList;
                            Log.d("zzz", "mySkuDetailList = "+mySkuDetailsList);
                            if (skuDetailsList != null) {
                                for (Object item : skuDetailsList) {
                                    SkuDetails skuDetails = (SkuDetails) item;
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();
                                    resultStr.append("Sku=" + sku + " Price=" + price + "\n");
                                }
                            } else {
                                resultStr.append("No Sku");
                            }
                            Log.d("resultStr"," "+ resultStr);
                            //textView1.setText(resultStr);
                        } else {
                            showResponseCode(responseCode);
                        }
                    }
                });
    }


    void startPurchase(String sku) {
        Log.d("sku", "sku= " + sku);
        SkuDetails skuDetails = getSkuDetails(sku);
        Log.d("sku","sku detail = "+skuDetails);
        if (skuDetails != null) {
            BillingFlowParams params = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetails)
                    .setReplaceSkusProrationMode(BillingFlowParams.ProrationMode.IMMEDIATE_WITH_TIME_PRORATION)
                    .build();
//            int responseCode = billingClient.launchBillingFlow(params);
            BillingResult billingResult = billingClient.launchBillingFlow(RegisterMemberAccount.this, params);
            showResponseCode(billingResult.getResponseCode());
        }
//        queryPurchaseHistory();
//        ActivatedMember();
    }
    void Upgrade(String new_sku, String old_sku) {
        Log.d("sku", "sku upgrade= " + new_sku);
        Log.d("sku", "sku old= " + old_sku);
        SkuDetails newskuDetails = getSkuDetails(new_sku);
        Log.d("sku","sku upgrade detail = "+newskuDetails);
        if (newskuDetails != null) {

            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(newskuDetails)
                    .setOldSku(old_sku)
                    .setReplaceSkusProrationMode(BillingFlowParams.ProrationMode.IMMEDIATE_WITH_TIME_PRORATION)
                    .build();
//            int responseCode = billingClient.launchBillingFlow(flowParams);
//            BillingFlowParams params = BillingFlowParams.newBuilder()
//                    .setOldSku(old_sku)
//                    .setReplaceSkusProrationMode(IMMEDIATE_AND_CHARGE_PRORATED_PRICE)
//                    .build();
//            int responseCode = billingClient.launchBillingFlow(params);
            BillingResult billingResult = billingClient.launchBillingFlow(RegisterMemberAccount.this, flowParams);
            showResponseCode(billingResult.getResponseCode());
        }
//        ActivatedMember();
    }


    SkuDetails getSkuDetails(String sku) {
        SkuDetails skuDetails = null;
        if(mySkuDetailsList==null){
            //textView1.setText("Exec [Get Skus] first");
            Log.d("resultStr", "Exec [Get Skus] first");
        }else {
            for (SkuDetails sd : mySkuDetailsList) {
                if (sd.getSku().equals(sku)) skuDetails = sd;
            }
            if (skuDetails == null) {
                //textView1.setText(sku + " is not found");
                Log.d("resultStr", " "+ sku + " is not found");
            }
        }
        return skuDetails;
    }

    String handlePurchase(Purchase purchase) {
        String stateStr = "error";
        int purchaseState = purchase.getPurchaseState();
        if (purchaseState == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.
            Log.d("zzz","purchased handle purchased");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date purchaseDate = new Date(purchase.getPurchaseTime());
            String date = sdf.format(purchaseDate);
            expired_date = date;
            ActivatedMember(date);
            stateStr = "purchased";
            // Acknowledge the purchase if it hasn't already been acknowledged.
//            ActivatedMember();
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, this);
            }
        }else if(purchaseState == Purchase.PurchaseState.PENDING){
            stateStr = "pending";
        }else if(purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE){
            stateStr = "unspecified state";
        }
        return stateStr;
    }


    @Override
    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
        int responseCode = billingResult.getResponseCode();
        if(responseCode != BillingClient.BillingResponseCode.OK) {
            showResponseCode(responseCode);
        }
    }


    void queryOwned(){
        member_items = new ArrayList<>();
        StringBuffer resultStr = new StringBuffer("");
        Purchase.PurchasesResult purchasesResult
                = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
        int responseCode = purchasesResult.getResponseCode ();
        if(responseCode== BillingClient.BillingResponseCode.OK){
            resultStr.append("Query Success\n");
            List<Purchase> purchases = purchasesResult.getPurchasesList();
            if(purchases.isEmpty()){
                resultStr.append("Owned Nothing");
            } else {
                for (Purchase purchase : purchases) {
                    resultStr.append(purchase.getSku()).append("\n");
                    resultStr.append(purchase.getPurchaseState()).append("\n");
                    resultStr.append(purchase.isAutoRenewing()).append("\n");
                    resultStr.append(purchase.getPurchaseTime()).append("\n");
                    if (purchase.isAutoRenewing()){
                        if (!purchase.getSku().equals("lfm1.0alfa_1")) {
                            member_items.add(purchase.getSku());
                        }
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date purchaseDate = new Date(purchase.getPurchaseTime());
                    String date = sdf.format(purchaseDate);
                    expired_date = date;
                    Log.d("zzz","purchase date = " +date);
                }
            }

            //textView1.setText(resultStr);
            Log.d("resultStr", " "+ resultStr);
        }else{
            showResponseCode(responseCode);
        }
    }


    void queryPurchaseHistory() {
        billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS,
                new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(BillingResult billingResult,
                                                          List<PurchaseHistoryRecord> purchasesList) {
                        int responseCode = billingResult.getResponseCode();
                        if (responseCode == BillingClient.BillingResponseCode.OK) {
                            if (purchasesList == null || purchasesList.size() == 0) {
                                //textView1.setText("No History");
                                Log.d("No History", " "+ "No History");
                            } else {
                                for (PurchaseHistoryRecord purchase : purchasesList) {
                                    // Process the result.
                                    //textView1.setText("Purchase History=" + purchase.toString() + "\n");
                                    Log.d("No History", " "+ purchase.toString());
                                }
                            }
                        } else {
                            showResponseCode(responseCode);
                        }
                    }
                });
    }

    void showResponseCode(int responseCode){
        switch(responseCode){
            case BillingClient.BillingResponseCode.OK:
                // textView1.setText("OK");
                Log.d("resultStr", " "+ "OK");
                break;
            case BillingClient.BillingResponseCode.USER_CANCELED:
                //textView1.setText("USER_CANCELED");
                Log.d("resultStr", " "+ "USER CANCELLED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                //textView1.setText("SERVICE_UNAVAILABLE");
                Log.d("resultStr", " "+ "SERVICE_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                Log.d("resultStr", " "+ "BILLING_UNAVAILABLE");
                //textView1.setText("BILLING_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                Log.d("resultStr", " "+ "ITEM_UNAVAILABLE");
                //textView1.setText("ITEM_UNAVAILABLE");
                break;
            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                Log.d("resultStr", " "+ "DEVELOPER_ERROR");
                //textView1.setText("DEVELOPER_ERROR");
                break;
            case BillingClient.BillingResponseCode.ERROR:
                Log.d("resultStr", " "+ "ERROR");
                //textView1.setText("ERROR");
                break;
            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                Log.d("resultStr", " "+ "ITEM_ALREADY_OWNED");
                //textView1.setText("ITEM_ALREADY_OWNED");
                break;
            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                Log.d("resultStr", " "+ "ITEM_NOT_OWNED");
                //textView1.setText("ITEM_NOT_OWNED");
                break;
            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                Log.d("resultStr", " "+ "SERVICE_DISCONNECTED");
                //textView1.setText("SERVICE_DISCONNECTED");
                break;
            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                Log.d("resultStr", " "+ "FEATURE_NOT_SUPPORTED");
                //textView1.setText("FEATURE_NOT_SUPPORTED");
                break;
        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        StringBuffer resultStr = new StringBuffer("");
        int billingResultCode = billingResult.getResponseCode();
        if (billingResultCode == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {

                String state = handlePurchase(purchase);
                String sku = purchase.getSku();
                resultStr.append(sku).append("\n");
                resultStr.append(" State=").append(state).append("\n");
            }
            //textView1.setText(resultStr);
            queryOwned();
            Log.d("resultStr", " "+ resultStr);
        } else {
            // Handle error codes.
            showResponseCode(billingResultCode);
        }
    }
}
