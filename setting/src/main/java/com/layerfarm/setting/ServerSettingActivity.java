package com.layerfarm.setting;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;

import java.util.ArrayList;
import java.util.Map;

public class ServerSettingActivity extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button saveButton;
    EditText codeEditText, usernameEditText, passwordEditText;
    ProgressBar progressBar;
    String url, code, uname, passwd;

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_setting);

        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        codeEditText = (EditText) findViewById(R.id.codeEditText);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        saveButton = (Button) findViewById(R.id.saveButton);
        progressBar = (ProgressBar) findViewById(R.id.proggressBar);

        final SQLiteDatabase db = dbase.getReadableDatabase();
        String selectQuery = "SELECT value FROM variables";
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            ArrayList<String> values = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    String value = cursor.getString(cursor.getColumnIndex("value"));
                    values.add(value);
                } while (cursor.moveToNext());
            }
            cursor.close();

            if (values.isEmpty()){
                Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();

            }else {
                codeEditText.setText(values.get(0));
                usernameEditText.setText(values.get(1));
                passwordEditText.setText(values.get(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid = true;
                if (codeEditText.getText().toString().isEmpty()) {
                    codeEditText.setError("Please fill the data");
                    valid = false;
                } else if (usernameEditText.getText().toString().isEmpty()) {
                    usernameEditText.setError("Please fill the data");
                    valid = false;
                } else if (passwordEditText.getText().toString().isEmpty()) {
                    passwordEditText.setError("Please fill the data");
                    valid = false;
                } else {
                    connect_server();
//                    Cursor result = db.rawQuery("SELECT value FROM variables", null);
//                    if (result.getCount() == 0) {
//                        // TODO Auto-generated method stub
//
//                        String[] _VARIABLES_INSERT = {"INSERT INTO variables (name, value) VALUES ('code', '"+codeEditText.getText().toString()+"')","INSERT INTO VARIABLE (name, value) VALUES ('username', '"+usernameEditText.getText().toString()+"')", "INSERT INTO VARIABLE (name, value) VALUES ('password', '"+passwordEditText.getText().toString()+"')"};
//                        for(String s : _VARIABLES_INSERT) {
//                            db.execSQL(s);
//                        }
//                        Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();
//                        connect_server();
//                        finish();
//
//                    }else {
//
//                        // TODO Auto-generated method stub
//
//                        String[] _VARIABLES_UPDATE = {"UPDATE variables SET value = '"+codeEditText.getText().toString()+"' WHERE name = 'code'", "UPDATE variable SET value = '"+usernameEditText.getText().toString()+"' WHERE name = 'username'", "UPDATE variable SET value = '"+passwordEditText.getText().toString()+"' WHERE name = 'password'"};
//                        for(String s : _VARIABLES_UPDATE) {
//                            db.execSQL(s);
//                        }
//                        Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();
//                        connect_server();
//                        finish();
//                    }


                }
            }
        });
    }

    public void closeApplication() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void connect_server(){

//        //get new layerfarm class
//        SQLiteDatabase dbSQL = dbase.getReadableDatabase();
//        String selectQuery = "SELECT value FROM variables";
//        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
//        ArrayList<String> values = new ArrayList<>();
//        if (cursor.moveToFirst()) {
//            do {
//                String value = cursor.getString(cursor.getColumnIndex("value"));
//                values.add(value);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        if (values.isEmpty()){
//            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
//
//        }else {
        code = codeEditText.getText().toString();
        uname = usernameEditText.getText().toString();
        passwd = passwordEditText.getText().toString();
        LayerFarm.getInstance().Connect(this, "http://"+url+"/", uname, passwd);
        RetrofitData.getInstance().setStatus("");
        Start();
//        }
    }
    private void set_variabl(){
        SQLiteDatabase db = dbase.getReadableDatabase();
        Cursor result = db.rawQuery("SELECT value FROM variables", null);
        if (result.getCount() == 0) {
            // TODO Auto-generated method stub

            String[] _VARIABLES_INSERT = {"INSERT INTO variables (name, value) VALUES ('code', '"+codeEditText.getText().toString()+"')","INSERT INTO variables (name, value) VALUES ('username', '"+usernameEditText.getText().toString()+"')", "INSERT INTO variables (name, value) VALUES ('password', '"+passwordEditText.getText().toString()+"')"};
            for(String s : _VARIABLES_INSERT) {
                db.execSQL(s);
            }
            Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();
/*            MainActivity a = MainActivity.getInstance();
            a.getLayerFarmDashboard();*/


        }else {

            // TODO Auto-generated method stub

            String[] _VARIABLE_UPDATE = {"UPDATE variables SET value = '"+codeEditText.getText().toString()+"' WHERE name = 'code'", "UPDATE variables SET value = '"+usernameEditText.getText().toString()+"' WHERE name = 'username'", "UPDATE variables SET value = '"+passwordEditText.getText().toString()+"' WHERE name = 'password'"};
            for(String s : _VARIABLE_UPDATE) {
                db.execSQL(s);
            }
            Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();
        }
    }
    public void Start(){
        progressBar.setVisibility(View.VISIBLE);
        mRun.run();
    }
    public void Stop(){
        progressBar.setVisibility(View.GONE);
        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            Map<String, String> role = RetrofitData.getInstance().getUser().getRoles();
            if (email == uname && passwd == passwd){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(ServerSettingActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        InsertUser();
                        set_variabl();
                        Stop();
                        //finish();
                        restart();
                    } else{
                        Toast.makeText(ServerSettingActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Stop();
                        finish();
                    }
                }
            }

        }
    };
    public void InsertUser(){
        SQLiteDatabase db = dbase.getReadableDatabase();
        User data_user = RetrofitData.getInstance().getUser();
        String farm_name = RetrofitData.getInstance().getFarm_name();
        ContentValues user = new ContentValues();
        //daily_values.put("note", note);
        user.put("email", data_user.getMail());
        user.put("password", passwordEditText.getText().toString());
        user.put("full_name", usernameEditText.getText().toString());
        user.put("farm_code", codeEditText.getText().toString());
        user.put("farm_name",farm_name);
        user.put("farm_adddress","");
        Map<String, String> role = data_user.getRoles();
        role.remove("2");
        if (role.containsValue("administrator")){
            user.put("is_owner","1");
        }
        else
            user.put("is_owner","0");
        ArrayList<String> roles = new ArrayList<>(role.values());
        String[] arr = roles.toArray(new String[0]);
        Log.d("zzz","array = "+arr);
        arr.toString();
        String str_roles = "";
        String query = "SELECT * FROM role WHERE machine_name IN (" + makePlaceholders(arr.length) + ")";
        Cursor cursor = db.rawQuery(query, arr);
        String[] role_name = new String[arr.length];
        String[] role_id = new String[arr.length];
        for (int i = 0; i< cursor.getCount(); i++){
            cursor.moveToPosition(i);
            role_name[i] = cursor.getString(cursor.getColumnIndex("name"));
            role_id[i] = cursor.getString(cursor.getColumnIndex("id"));
        }

        for (int i =0; i < role_name.length; i++){
            if (i!= 0){
                str_roles +=",";
            }
            str_roles = str_roles+role_name[i]+" ";
        }

//        user.put("role", roles.toString());
        user.put("role", str_roles);


        //mengeksekusi perintah sql insert data
        //yang mengembalikan sebuah insert ID
        long user_id = db.insert("user_farm",null,user);
        for (int i =0; i< roles.size(); i++){
            str_roles = roles.get(i)+" ";
            ContentValues cv_roles = new ContentValues();
            cv_roles.put("user_farm_id", user_id);
            cv_roles.put("role_id",role_id[i] );
            long user_role_id = db.insert("user_role", null, cv_roles);
        }
    }

    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }


    public void restart(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        ActivityCompat.finishAfterTransition(ServerSettingActivity.this);
    }
}
