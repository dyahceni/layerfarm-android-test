package com.layerfarm.setting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SettingMainActivity extends AppCompatActivity {

    public static final String TAG = SettingMainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_main);
    }

}
