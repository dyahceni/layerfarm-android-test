package com.layerfarm.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.adapter.AdapterBuyer;
import com.layerfarm.setting.adddata.CreateBuyerActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.updatedata.UpdateBuyer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyerActivity extends AppCompatActivity {

    ListView listView;
    AlertDialog.Builder dialog;
    List<SyncBuyer> itemList = new ArrayList<SyncBuyer>();
    AdapterBuyer adapter;
    DataHelper SQLite = new DataHelper(this);
    DatabaseHelper dbase;

    public static final String TAG_ID = "id";
    public static final String TAG_PRODUCT_NAME = "name";
    public static final String TAG_RID = "rid";


    Toolbar toolbar ;
    TextView toolbar_title;
    List<SyncBuyer> buyerSyncList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer);

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText(getResources().getString(R.string.buyer));

        SQLite = new DataHelper(getApplicationContext());
        dbase = new DatabaseHelper(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        listView = (ListView) findViewById(R.id.list_view_buyer);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BuyerActivity.this, CreateBuyerActivity.class);
                startActivity(intent);
            }
        });

        adapter = new AdapterBuyer(BuyerActivity.this, itemList);
        listView.setAdapter(adapter);

        // long press listview to show edit and delete
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                final String idx = itemList.get(position).getId();
                final String name = itemList.get(position).getName();
                final String address = itemList.get(position).getAddress();
                final String rid = itemList.get(position).getRid();

                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(BuyerActivity.this);
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(BuyerActivity.this, UpdateBuyer.class);
                                intent.putExtra(TAG_ID, idx);
                                intent.putExtra(TAG_PRODUCT_NAME, name);
                                intent.putExtra("address",address);
                                intent.putExtra("rid", rid);
                                startActivity(intent);
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(BuyerActivity.this); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        delete(rid,name,idx);
                                        //SQLite.deleteMedicationVaccination(idx);



                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();


                                break;
                        }
                    }
                }).show();
                //return false;
            }
        });

//        getAllData();

    }


    private void getAllData() {
        ArrayList<HashMap<String, String>> row = SQLite.getAllBuyerList();
        itemList.clear();
        for (int i = 0; i < row.size(); i++) {
            String id = row.get(i).get("id");
            String name = row.get(i).get("name");
            String rid = row.get(i).get("rid");
            String address = row.get(i).get("address");

            SyncBuyer data = new SyncBuyer(id, name, rid, address);
            itemList.add(data);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        itemList.clear();
        synchronize();
//        getAllData();
    }

    public void sync(View v){
        synchronize();
    }
    public void synchronize(){
        String module = "selling_price";
        String function_name = "selling_price_get_buyer";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<List<SyncBuyer>> call = apiInterfaceJson.getBuyer(token2, parameter);

        call.enqueue(new Callback<List<SyncBuyer>>() {

            @Override
            public void onResponse(Call<List<SyncBuyer>> call, Response<List<SyncBuyer>> response) {
//                ArrayList<HashMap<String, String>> row = SQLite.getAllBuyerList();
//                List<String> data = new ArrayList<>();
//                for (int i = 0; i < row.size(); i++) {
//                    String name = row.get(i).get("name").toLowerCase();
//                    data.add(name);
//                }
                buyerSyncList = response.body();
                for (int i =0; i< buyerSyncList.size() ; i++){
                    String name = buyerSyncList.get(i).getName();
                    String rid = buyerSyncList.get(i).getRid();
                    String address = buyerSyncList.get(i).getAddress();

                    SyncBuyer data = new SyncBuyer("", name, rid, address);
                    itemList.add(data);
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    map.put("key", buyerSyncList.get(i).getName());
//                    map.put("name", buyerSyncList.get(i).getName());
//                    map.put("rid", buyerSyncList.get(i).getRid());
//                    map.put("address",buyerSyncList.get(i).getAddress());
//                    InsertDataBuyer(map, data);
                }
                Toast.makeText(getApplicationContext(), "Create Buyer Successfully", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
//                getAllData();

            }

            @Override
            public void onFailure(Call<List<SyncBuyer>> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
            }
        });
    }
    public void delete(String rid, String vendor_name, final String idx){
        String module = "selling_price";
        String function_name = "selling_price_delete_buyer";
        String[] args = {rid};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.deleteBuyer(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] vendorDelete = response.body();
//                SQLite.deleteBuyer(Integer.parseInt(idx));
                Toast.makeText(getApplicationContext(), "Delete Buyer Successfully", Toast.LENGTH_LONG).show();
                itemList.clear();
                synchronize();
            }
            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Delete Buyer Failed", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void InsertDataBuyer(HashMap<String, String> map , List<String> data){
        SQLiteDatabase db = dbase.getWritableDatabase();
        String key = map.get("key");
        if (!data.contains(key)) {
            Log.d("Layerfarm","noo");
            db.execSQL("insert into buyer(name,rid,address) values('" +
                    map.get("name") + "','" +
                    map.get("rid") + "','"+
                    map.get("address")+"')");

        }
        else {
            Log.d("Layerfarm", "adaa");

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void close(View view){
        finish();
    }
}
