package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.List;


public class AdapterLocation extends BaseAdapter implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Farm> items;
    private List<Farm>originalData = null;
    private List<Farm>filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public AdapterLocation(Activity activity, List<Farm> items) {
        this.activity = activity;
        this.items = items;
        this.originalData = items;
        this.filteredData = items;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int location) {
        return filteredData.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_location, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView address = (TextView) convertView.findViewById(R.id.address);
        TextView rid = (TextView) convertView.findViewById(R.id.rid);

        Farm locationmodel = filteredData.get(position);

        //id.setText(locationmodel.getId());
        name.setText(locationmodel.getName());
        address.setText("Address :"+locationmodel.getAddress());
        //rid.setText(locationmodel.getRid());

        return convertView;
    }
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            Log.d("zzz","constraint "+ filterString);
            FilterResults results = new FilterResults();

            final List<Farm> list = originalData;

            int count = list.size();
            final ArrayList<Farm> nlist = new ArrayList<Farm>(count);

            String filterableString ;
//            MedicationVaccination test;

            for (int i = 0; i < count; i++) {
                Farm test;
                test = list.get(i);
                filterableString = list.get(i).getName().toLowerCase();
//                Log.d("zzz","filter "+filterableString);
                if (filterableString.toLowerCase().contains(filterString)) {
                    Log.d("zzz","add "+filterableString);
                    Log.d("zzz","test "+test.getName().toLowerCase());

                    nlist.add(test);
                }
                Log.d("zzz","status :"+list.get(i).getDelete_status());
            }
            for (int i =0; i< nlist.size(); i++){
                Log.d("zzz","nlist "+nlist.get(i).getName());
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Farm>) results.values;
            for (int i =0; i< filteredData.size(); i++){
                Log.d("zzz","filter data = "+filteredData.get(i).getName());
            }
            notifyDataSetChanged();
        }

    }
}
