package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.layerfarm.layerfarm.model.UserMember;
import com.layerfarm.setting.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class AdapterUser extends BaseAdapter implements Filterable {
    private Activity activity;
    private UserMember items;
    private LayoutInflater inflater;
    UserMember.FieldManagedLocation item;
    private UserMember originalData = null;
    private UserMember filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();
    HashMap<Integer, String> house;
    int items_qty;
    public AdapterUser(Activity activity, UserMember items, int items_qty){
        this.activity = activity;
        this.items = items;
        this.items_qty = items_qty;
        this.originalData = items;
        this.filteredData = items;
    }
    @Override
    public int getCount() {
        return filteredData.getUsers().size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.getUsers().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        item = new UserMember.FieldManagedLocation();
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_user, null);
        house = filteredData.getHouse();
//        Object json = new JSONTokener(data).nextValue();
        JsonElement data = filteredData.getUsers().get(position).getFieldManagedLocation();
//        Log.d("zzz","data = "+data);
        try{
            if(data.isJsonArray()){
                //Array
    //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is array");
                item = new UserMember.FieldManagedLocation();
            } else {
                //Object
    //            Log.d("Layer","name = "+items.getUsers().get(position).getName()+"is object");
                Type type = new TypeToken<UserMember.FieldManagedLocation>() {
                }.getType();
                item = new Gson().fromJson(data, type);
            }
        }catch (Exception e){}

//        Log.d("adapteruser", "item = "+item.getUnd());
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView role = (TextView) convertView.findViewById(R.id.role);
        TextView access = (TextView) convertView.findViewById(R.id.access);

        name.setText(filteredData.getUsers().get(position).getName());
        ArrayList<String> roles = new ArrayList<>(filteredData.getUsers().get(position).getRoles().values());
        String tx_role = "";
        for (int i=0; i< roles.size(); i++){
            String[] chr_role = roles.get(i).split(" ");
            tx_role += "- ";
            for (int a =0; a< chr_role.length; a++){

                tx_role += chr_role[a].substring(0,1).toUpperCase();
                tx_role += chr_role[a].substring(1).toLowerCase();
                tx_role += " ";
            }

        }
        role.setText(tx_role);
        String akses = "";
//        Log.d("adminuser","name = "+items.getUsers().get(position).getName());
        if (!item.getUnd().isEmpty()){
//            Log.d("adapteruser", "notnull");
            for (int i = 0; i< item.getUnd().size(); i++){
                akses += "- ";
//                String nid = items.getUsers().get(position).getFieldManagedHouse().getUnd().get(i).getTarget_id();
                String nid = item.getUnd().get(i).getTarget_id();
                String house_akses = house.get(Integer.parseInt(nid));
//                Log.d("adapteruser","nid = "+nid);
//                Log.d("adapteruser","akse = "+house_akses);
                akses += house_akses;
                akses += "\n";
            }
        }
        access.setText(akses);
//        Log.d("zzzz","position = "+position);
        Log.d("zzzz","items_qty ="+items_qty);
        if (items_qty != 0){
            if (position > items_qty){
                Log.d("zzzz","changed position = "+position);
                convertView.setBackgroundColor(ContextCompat.getColor(activity, R.color.red));
            }
            else{
                convertView.setBackgroundColor(ContextCompat.getColor(activity, R.color.background_material_light));
            }

        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
//            Log.d("zzz","constraint "+ filterString);
            FilterResults results = new FilterResults();

            final UserMember user = originalData;
            ArrayList<UserMember.Users> list = originalData.getUsers();

            int count = list.size();
            final ArrayList<UserMember.Users> nlist = new ArrayList<UserMember.Users>(count);

            String filterableString ;
//            MedicationVaccination test;

            for (int i = 0; i < count; i++) {
                UserMember.Users test;
                test = list.get(i);
                filterableString = list.get(i).getName().toLowerCase();
//                Log.d("zzz","filter "+filterableString);
                if (filterableString.toLowerCase().contains(filterString)) {
//                    Log.d("zzz","add "+filterableString);
//                    Log.d("zzz","test "+test.getName().toLowerCase());
                    nlist.add(test);
                }
            }
//            for (int i =0; i< nlist.size(); i++){
//                Log.d("zzz","nlist "+nlist.get(i).getName());
//            }
            UserMember value = new UserMember();
            value.setHouse(user.getHouse());
            value.setUsers(nlist);

            results.values = value;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (UserMember) results.values;
            for (int i =0; i< filteredData.getUsers().size(); i++){
//                Log.d("zzz","filter data = "+filteredData.getUsers().get(i).getName());
            }
            notifyDataSetChanged();
        }

    }
}
