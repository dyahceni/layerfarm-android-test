package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.setting.model.Mortality;
import com.layerfarm.setting.R;

import java.util.List;


public class AdapterMortality extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Mortality> items;

    public AdapterMortality(Activity activity, List<Mortality> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_mortality, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView product_name = (TextView) convertView.findViewById(R.id.name);

        Mortality mortalitymodel = items.get(position);

        //id.setText(mortalitymodel.getId());
        product_name.setText(mortalitymodel.getName());

        return convertView;
    }
}
