package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.setting.model.Vendor;
import com.layerfarm.setting.R;

import java.util.List;


public class AdapterVendor extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Vendor> items;

    public AdapterVendor(Activity activity, List<Vendor> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_vendor, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView rid = (TextView) convertView.findViewById(R.id.rid);

        Vendor vendormodel = items.get(position);

        //id.setText(vendormodel.getId());
        name.setText(vendormodel.getName());
        //rid.setText(vendormodel.getRid());

        return convertView;
    }
}
