package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterListActive extends BaseAdapter {

    private Activity activity;
    private ArrayList<String> items;
    private LayoutInflater inflater;
    private HashMap<Integer, String> original;

    public AdapterListActive(Activity activity, ArrayList<String> items, HashMap<Integer, String> original){
        this.activity = activity;
        this.items = items;
        this.original = original;
//        this.items_qty = items_qty;
//        this.originalData = items;
//        this.filteredData = items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_active, null);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        ImageView check = (ImageView) convertView.findViewById(R.id.img_check);
        name.setText(items.get(position));
        if (original.containsValue(items.get(position))){
            check.setVisibility(View.VISIBLE);
        }
        else
            check.setVisibility(View.GONE);
        return convertView;
    }
}
