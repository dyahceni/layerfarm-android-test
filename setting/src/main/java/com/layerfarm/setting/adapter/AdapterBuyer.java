package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.setting.R;

import java.util.List;


public class AdapterBuyer extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<SyncBuyer> items;

    public AdapterBuyer(Activity activity, List<SyncBuyer> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_buyer, null);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView address = (TextView) convertView.findViewById(R.id.address);

        SyncBuyer vendormodel = items.get(position);

        //id.setText(vendormodel.getId());
        name.setText(vendormodel.getName());
        address.setText(vendormodel.getAddress());
        //rid.setText(vendormodel.getRid());

        return convertView;
    }
}
