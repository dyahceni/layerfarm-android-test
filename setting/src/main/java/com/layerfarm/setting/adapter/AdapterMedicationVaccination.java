package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.layerfarm.setting.R;
import com.layerfarm.setting.model.MedicationVaccination;

import java.util.ArrayList;
import java.util.List;


public class AdapterMedicationVaccination extends BaseAdapter implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<MedicationVaccination> items;
    private List<MedicationVaccination>originalData = null;
    private List<MedicationVaccination>filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public AdapterMedicationVaccination(Activity activity, List<MedicationVaccination> items) {
        this.originalData = items;
        this.filteredData = items;
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int location) {
        return filteredData.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_medication, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView vendor_id = (TextView) convertView.findViewById(R.id.vendor);
        TextView capacity = (TextView) convertView.findViewById(R.id.capacity);
        TextView unit = (TextView) convertView.findViewById(R.id.unit);
        TextView type = (TextView) convertView.findViewById(R.id.type);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView rid = (TextView) convertView.findViewById(R.id.rid);

        MedicationVaccination medicationmodel = filteredData.get(position);

        //id.setText(medicationmodel.getId());
        name.setText(medicationmodel.getProduct_name());
        //vendor_id.setText(medicationmodel.getVendor_id());
        capacity.setText("Capacity "+medicationmodel.getCapacity());
        unit.setText("Unit :"+medicationmodel.getUnit());
        type.setText("Type :"+medicationmodel.getType());
        status.setText("Status :"+medicationmodel.getStatus());
        //rid.setText(medicationmodel.getRid());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            Log.d("zzz","constraint "+ filterString);
            FilterResults results = new FilterResults();

            final List<MedicationVaccination> list = originalData;

            int count = list.size();
            final ArrayList<MedicationVaccination> nlist = new ArrayList<MedicationVaccination>(count);

            String filterableString ;
//            MedicationVaccination test;

            for (int i = 0; i < count; i++) {
                MedicationVaccination test;
                test = list.get(i);
                filterableString = list.get(i).getProduct_name().toLowerCase();
//                Log.d("zzz","filter "+filterableString);
                if (filterableString.toLowerCase().contains(filterString)) {
                    Log.d("zzz","add "+filterableString);
                    Log.d("zzz","test "+test.getProduct_name().toLowerCase());
                    nlist.add(test);
                }
            }
            for (int i =0; i< nlist.size(); i++){
                Log.d("zzz","nlist "+nlist.get(i).getProduct_name());
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<MedicationVaccination>) results.values;
            for (int i =0; i< filteredData.size(); i++){
                Log.d("zzz","filter data = "+filteredData.get(i).getProduct_name());
            }
            notifyDataSetChanged();
        }

    }
}
