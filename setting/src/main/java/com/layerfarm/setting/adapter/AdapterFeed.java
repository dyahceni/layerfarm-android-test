package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.layerfarm.setting.model.Feed;
import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.List;


public class AdapterFeed extends BaseAdapter implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Feed> items;
    private List<Feed>originalData = new ArrayList<>();
    private List<Feed>filteredData = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public AdapterFeed(Activity activity, List<Feed> items) {
        this.activity = activity;
        this.items = items;
        this.originalData = items;
        this.filteredData = items;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_feed, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView product_name = (TextView) convertView.findViewById(R.id.name);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView rid = (TextView) convertView.findViewById(R.id.rid);

        Feed feedmodel = filteredData.get(position);

        //id.setText(feedmodel.getId());
        product_name.setText(feedmodel.getName());
        description.setText("Description: " +feedmodel.getDescription());
        status.setText("Status: "+feedmodel.getStatus());
        //rid.setText(feedmodel.getRid());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            Log.d("zzz","constraint "+ filterString);
            FilterResults results = new FilterResults();

            final List<Feed> list = originalData;

            int count = list.size();
            final ArrayList<Feed> nlist = new ArrayList<Feed>(count);

            String filterableString ;
//            MedicationVaccination test;

            for (int i = 0; i < count; i++) {
                Feed test;
                test = list.get(i);
                if(!list.get(i).getName().isEmpty()) {
                    filterableString = list.get(i).getName().toLowerCase();
//                Log.d("zzz","filter "+filterableString);
                    if (filterableString.toLowerCase().contains(filterString)) {
                        Log.d("zzz", "add " + filterableString);
                        Log.d("zzz", "test " + test.getName().toLowerCase());
                        nlist.add(test);
                    }
                }
            }
            for (int i =0; i< nlist.size(); i++){
                Log.d("zzz","nlist "+nlist.get(i).getName());
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Feed>) results.values;
            try{
                for (int i =0; i< filteredData.size(); i++){
                    Log.d("zzz","filter data = "+filteredData.get(i).getName());
                }
                notifyDataSetChanged();
            }catch (Exception e){

            }

        }

    }
}
