package com.layerfarm.setting.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.setting.model.Strain;
import com.layerfarm.setting.R;

import java.util.List;


public class AdapterStrain extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Strain> items;

    public AdapterStrain(Activity activity, List<Strain> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_strain, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView name = (TextView) convertView.findViewById(R.id.name);

        Strain strainmodel = items.get(position);

        //id.setText(strainmodel.getId());
        name.setText(strainmodel.getName());

        return convertView;
    }
}
