package com.layerfarm.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class UserRegistration extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);

    }
    public void register_member_account(View view) {
        Intent intent = new Intent(UserRegistration.this, RegisterMemberAccount.class);
        startActivity(intent);
    }
    public void show_member_account(View v){
        Intent intent = new Intent(UserRegistration.this, UserActivityNew.class);
        startActivity(intent);
    }
}
