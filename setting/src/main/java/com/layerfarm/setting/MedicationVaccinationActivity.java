package com.layerfarm.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.layerfarm.setting.adddata.CreateMedicationVaccinationActivity;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.setting.updatedata.UpdateMedicationVaccination;

public class MedicationVaccinationActivity extends AppCompatActivity {

    String[] ListMedication;
    ListView ListView01;
    Menu menu;
    protected Cursor cursor;
    DataHelper dbcenter;
    DatabaseHelper dbase;
    public static MedicationVaccinationActivity ma;

    public static final String TAG = MedicationVaccinationActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_vaccination);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MedicationVaccinationActivity.this, CreateMedicationVaccinationActivity.class));
            }
        });

        ma = this;
        dbcenter = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        RefreshListMedication();
    }


     public void RefreshListMedication() {
         SQLiteDatabase db = dbase.getReadableDatabase();
         cursor = db.rawQuery("SELECT * FROM medication_vaccination",null);
         ListMedication = new String[cursor.getCount()];
         cursor.moveToFirst();
         for (int cc=0; cc < cursor.getCount(); cc++){
             cursor.moveToPosition(cc);
             ListMedication[cc] = cursor.getString(1).toString();
         }
         ListView01 = (ListView)findViewById(R.id.listViewMedication);
         ListView01.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, ListMedication));
         ListView01.setSelected(true);

         //Listener Data Click
         ListView01.setOnItemClickListener(new AdapterView.OnItemClickListener() {


             public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                 final String selection = ListMedication[arg2]; //.getItemAtPosition(arg2).toString();
                 final CharSequence[] dialogitem = {"Update Medication Vaccination", "Delete Medication Vaccination"};
                 AlertDialog.Builder builder = new AlertDialog.Builder(MedicationVaccinationActivity.this);
                 builder.setTitle("Please Choose Operation");
                 builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int item) {
                         switch(item){
                             case 0 :
                                 Intent in = new Intent(getApplicationContext(), UpdateMedicationVaccination.class);
                                 in.putExtra("product_name", selection);
                                 startActivity(in);
                                 break;
                             case 1 :
                                 SQLiteDatabase db = dbase.getWritableDatabase();
                                 db.execSQL("delete from medication_vaccination where product_name = '"+selection+"'");
                                 RefreshListMedication();
                                 break;
                         }
                     }
                 });
                 builder.create().show();
             }});
         ((ArrayAdapter)ListView01.getAdapter()).notifyDataSetInvalidated();

     }

}
