package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateEqqQuality extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    DatabaseHelper dbase;
    Button updateButton;
    EditText EqqNameEditText,idEqqEditText;
    String idx, egg_name, machine_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_eqq_quality);

        dbHelper = new DataHelper(this);
        dbase = new DatabaseHelper(this);
        idEqqEditText = (EditText) findViewById(R.id.idEqqEditText);
        EqqNameEditText = (EditText) findViewById(R.id.eqqNameEditText);
        Bundle i = getIntent().getExtras();
        idx = i.getString("id");
        egg_name = i.getString("name");
        machine_name = i.getString("machine_name");
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        cursor = db.rawQuery("SELECT * FROM egg_quality WHERE name = '" +
//                getIntent().getStringExtra("name") + "'",null);
//        cursor.moveToFirst();
//        if (cursor.getCount()>0)
//        {
//            cursor.moveToPosition(0);
            idEqqEditText.setText(idx);
            EqqNameEditText.setText(egg_name);
//        }

        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                update();

            }
        });

    }
    public void update(){
        String module = "layerfarm";
        String function_name = "layerfarm_set_available_quality";
        String[] args = {machine_name, EqqNameEditText.getText().toString()};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.updateEggQuality(token2, parameter);

        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[]resp = response.body();
//                HashMap<String, String> all_ovk = new HashMap<>();

                String message = resp[0];
                //Log.d("message","get_expense_general_respons : "+message);
                if (message.equalsIgnoreCase("fail")){
                    Toast.makeText(getApplicationContext(), "Egg can't updated" , Toast.LENGTH_LONG).show();
                }

                if (message.equalsIgnoreCase("ok")){
                    Toast.makeText(getApplicationContext(), "Egg updated" , Toast.LENGTH_LONG).show();
                    // TODO Auto-generated method stub
                    SQLiteDatabase db = dbase.getWritableDatabase();
                    db.execSQL("update egg_quality set name='"+
                            EqqNameEditText.getText().toString() +"' where id='" +
                            idEqqEditText.getText().toString()+"'");
                    Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                    //EqqQualityActivity.ma.RefreshListEqq();

                }
                finish();
            }
            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                finish();
            }
        });

    }

}