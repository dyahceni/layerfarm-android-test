package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateVendor extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idVendorEditText,vendorNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vendor);

//        dbHelper = new DatabaseHelper(this);
        idVendorEditText = (EditText) findViewById(R.id.idVendorEditText);
        vendorNameEditText = (EditText) findViewById(R.id.vendorNameEditText);

        Bundle extras = getIntent().getExtras();
        final String idx = extras.getString("id");
        final String rid = extras.getString("rid");
        final String name = extras.getString("name");

        idVendorEditText.setText(idx);
        vendorNameEditText.setText(name);

        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                update(rid, vendorNameEditText.getText().toString(), idx);
                //VendorActivity.ma.RefreshListVendor();

            }
        });

    }
    public void update(String rid, String vendor_name, final String idx){
        String module = "ovk";
        String function_name = "ovk_create_vendor";
        String[] args = {rid, vendor_name};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createVendor(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] vendorCreate = response.body();

//                SQLiteDatabase db = dbHelper.getWritableDatabase();
//                db.execSQL("update vendor set name='"+
//                        vendorNameEditText.getText().toString() +"' where id='" +
//                        idVendorEditText.getText().toString()+"'");

                Toast.makeText(getApplicationContext(), "Update Vendor Successfully "+vendorCreate, Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Update Vendor Failed", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}