package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateBuyer extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idBuyerEditText,buyerNameEditText, buyerAddressEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_buyer);

//        dbHelper = new DatabaseHelper(this);
        idBuyerEditText = (EditText) findViewById(R.id.idBuyerEditText);
        buyerNameEditText = (EditText) findViewById(R.id.buyerNameEditText);
        buyerAddressEditText = (EditText) findViewById(R.id.buyerAddressEditText);

        Bundle extras = getIntent().getExtras();
        final String idx = extras.getString("id");
        final String rid = extras.getString("rid");
        final String name = extras.getString("name");
        final String address = extras.getString("address");
        idBuyerEditText.setText(idx);
        buyerNameEditText.setText(name);
        buyerAddressEditText.setText(address);

//        SQLiteDatabase db = dbHelper.getReadableDatabase();
//        cursor = db.rawQuery("SELECT * FROM buyer WHERE name = '" +
//                getIntent().getStringExtra("name") + "'",null);
//        cursor.moveToFirst();
//        if (cursor.getCount()>0)
//        {
//            cursor.moveToPosition(0);
//            idBuyerEditText.setText(cursor.getString(0).toString());
//            buyerNameEditText.setText(cursor.getString(1).toString());
//        }

        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                update(rid, buyerNameEditText.getText().toString(), buyerAddressEditText.getText().toString(), idx);
//                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                //VendorActivity.ma.RefreshListVendor();

            }
        });

    }
    public void update(String rid, final String buyer_name, String address, final String idx){
        String module = "selling_price";
        String function_name = "selling_price_create_buyer";
        String[] args = {rid, buyer_name, address};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createBuyer(token2, parameter);

        call.enqueue(new Callback<String[]>() {

            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                String[] buyerCreate = response.body();

//                SQLiteDatabase db = dbHelper.getWritableDatabase();
//                db.execSQL("update buyer set name='"+
//                        buyer_name +"' where id='" +
//                        idx+"'");

                Toast.makeText(getApplicationContext(), "Update Buyer Successfully "+buyerCreate, Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Update Buyer Failed", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}