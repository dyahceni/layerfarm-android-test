package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.setting.R;

public class UpdateMortality extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idMortalityEditText, mortalityNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mortality);

        dbHelper = new DatabaseHelper(this);
        idMortalityEditText = (EditText) findViewById(R.id.idMortalityEditText);
        mortalityNameEditText = (EditText) findViewById(R.id.mortalityNameEditText);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM mortality_category WHERE name = '" +
                getIntent().getStringExtra("name") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            idMortalityEditText.setText(cursor.getString(0).toString());
            mortalityNameEditText.setText(cursor.getString(1).toString());
        }

        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update mortality_category set name='"+
                        mortalityNameEditText.getText().toString() +"' where id='" +
                        idMortalityEditText.getText().toString()+"'");
                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                //MortalityActivity.ma.RefreshListMortality();
                finish();
            }
        });

    }

}