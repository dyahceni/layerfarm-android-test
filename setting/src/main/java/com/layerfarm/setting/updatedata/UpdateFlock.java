package com.layerfarm.setting.updatedata;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.setting.DateWheel.DatePickerPopWin;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.ParameterCreateFlock;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateFlock extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idFlockEditText,flockNameEditText, capacityEditText, effective_dateEditText;
    private Spinner allLocation;
    RadioGroup radio_group_type,radio_group_period,radio_status;
    RadioButton radio_floor,radio_cage,radio_free_range,radio_grower,radio_layer,radio_active,radio_inactive;
    String type_flock = null;
    String flock_period = null;
    String flock_status = null;
    private String id,name,location_id,capacity,rid, effective_date;
    HashMap<String, String> list_location;
    HashMap<String, String> location_map = new HashMap<>();
    List<String> lables;
    String[] status = {"Active", "Inactive"};
    String[] type = {"Cage", "Floor", "Free Range"};
    String[] period = {"Grower", "Layer"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_flock);


        dbHelper = new DatabaseHelper(this);
        //Spinner Location
        allLocation = (Spinner) findViewById(R.id.spLocation);

        idFlockEditText = (EditText) findViewById(R.id.idFlockEditText);
        flockNameEditText = (EditText) findViewById(R.id.flockNameEditText);
        effective_dateEditText = (EditText) findViewById(R.id.effective_date);
        capacityEditText = (EditText) findViewById(R.id.capacityEditText);
        updateButton = (Button) findViewById(R.id.updateButton);

        radio_group_type = (RadioGroup) findViewById(R.id.radio_group_type);
        radio_group_period = (RadioGroup) findViewById(R.id.radio_group_period);
        radio_status = (RadioGroup) findViewById(R.id.radio_status);

        radio_floor = (RadioButton) findViewById(R.id.radio_floor);
        radio_cage = (RadioButton) findViewById(R.id.radio_cage);
        radio_free_range = (RadioButton) findViewById(R.id.radio_free_range);
        radio_grower = (RadioButton) findViewById(R.id.radio_grower);
        radio_layer = (RadioButton) findViewById(R.id.radio_layer);
        radio_active = (RadioButton) findViewById(R.id.radio_active);
        radio_inactive = (RadioButton) findViewById(R.id.radio_inactive);

        /**
         * Kita cek apakah ada Bundle atau tidak
         */
        if (getIntent().getExtras() != null) {
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
//            id = bundle.getString("id");
            name = bundle.getString("name");
            location_id = bundle.getString("location_id");
            type_flock = bundle.getString("type");
            flock_period = bundle.getString("period");
            flock_status = bundle.getString("status");
            capacity = bundle.getString("capacity");
            rid = bundle.getString("rid");
            effective_date = bundle.getString("effective_date");
            list_location = (HashMap<String, String>)bundle.getSerializable("list_location");
        } else {
            /**
             * Apabila Bundle tidak ada, ambil dari Intent
             */
//            id = getIntent().getStringExtra("id");
            name = getIntent().getStringExtra("name");
            location_id = getIntent().getStringExtra("location_id");
            type_flock = getIntent().getStringExtra("type");
            flock_period = getIntent().getStringExtra("period");
            flock_status = getIntent().getStringExtra("status");
            capacity = getIntent().getStringExtra("capacity");
            rid = getIntent().getStringExtra("rid");
            effective_date = getIntent().getStringExtra("effective_date");
            list_location = (HashMap<String, String>)getIntent().getSerializableExtra("list_location");
        }
        if (!list_location.isEmpty() && list_location != null){
            location_map = ListLocation.getInstance().getList_location();
        }
        Collection<String> lables_col = location_map.values();
        lables = new ArrayList<String>(lables_col);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        allLocation.setAdapter(dataAdapter);
//        loadFlockSpinnerData();
        registerEvents();

            idFlockEditText.setText(rid);
            flockNameEditText.setText(name);
            effective_dateEditText.setText(effective_date);
            //allLocation.setSelection(cursor.getInt((2))- 1);
            allLocation.setSelection(getIndex(allLocation,location_map.get(location_id)));

            //Type
            String type = type_flock;
            //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            if(type.equalsIgnoreCase("Floor")){
                radio_floor.setChecked(true);
                radio_cage.setChecked(false);
                radio_free_range.setChecked(false);
            }else if(type.equalsIgnoreCase("Cage")){
                radio_floor.setChecked(false);
                radio_cage.setChecked(true);
                radio_free_range.setChecked(false);
            }else if(type.equalsIgnoreCase("Free Range")){
                radio_floor.setChecked(false);
                radio_cage.setChecked(false);
                radio_free_range.setChecked(true);
            }

            //Period
            String period = flock_period;
            //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            if(period.equalsIgnoreCase("Grower")){
                radio_grower.setChecked(true);
                radio_layer.setChecked(false);
            }else if(period.equalsIgnoreCase("Layer")){
                radio_grower.setChecked(false);
                radio_layer.setChecked(true);
            }

            //Status
            String status = flock_status;
            //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            if(status.equalsIgnoreCase("Active")){
                radio_active.setChecked(true);
                radio_inactive.setChecked(false);
            }else if(status.equalsIgnoreCase("Inactive")){
                radio_active.setChecked(false);
                radio_inactive.setChecked(true);
            }

            capacityEditText.setText(capacity);

        effective_dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(UpdateFlock.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        effective_dateEditText.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(UpdateFlock.this);
            }
        });


        updateButton = (Button) findViewById(R.id.updateButton);


        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                create_layerfarm_flock(rid);
            }
        });

    }

    private void loadFlockSpinnerData(){
        // database handler
        DataHelper db = new DataHelper(getApplicationContext());
        // Spinner Drop down elements
        List<String> lables = db.getAllLocation();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        allLocation.setAdapter(dataAdapter);
    }

    private void registerEvents() {
        radio_group_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_floor) {
                    type_flock = "Floor";
                } else if (checkedId == R.id.radio_cage) {
                    type_flock = "Cage";
                }else if (checkedId == R.id.radio_free_range) {
                    type_flock = "Free Range";
                }
            }

        });


        radio_group_period.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_grower) {
                    flock_period = "Grower";
                } else if (checkedId == R.id.radio_layer) {
                    flock_period = "Layer";
                }
            }

        });

        radio_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    flock_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    flock_status = "Inactive";
                }
            }

        });


    }


    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }

    private void create_layerfarm_flock(String nid){
        //Get location id
        String location = allLocation.getSelectedItem().toString();
        String strLocation = getKey(location_map, location);

        EditText a = (EditText) findViewById(R.id.flockNameEditText);
        String flock_name = (String)(a.getText().toString());

        EditText b = (EditText) findViewById(R.id.capacityEditText);
        String flock_capacity = (String)(b.getText().toString());

        String effective_date = effective_dateEditText.getText().toString();

        int radioButtonID_status = radio_status.getCheckedRadioButtonId();
        View radioButton_status = radio_status.findViewById(radioButtonID_status);
        int idx_status = radio_status.indexOfChild(radioButton_status);
        String selectedtext_status = status[idx_status];

        int radioButtonID_period = radio_group_period.getCheckedRadioButtonId();
        View radioButton_period = radio_group_period.findViewById(radioButtonID_period);
        int idx_period = radio_group_period.indexOfChild(radioButton_period);
        String selectedPeriod = period[idx_period];

        int radioButtonID_type = radio_group_type.getCheckedRadioButtonId();
        View radioButton_type = radio_group_type.findViewById(radioButtonID_type);
        int idx_type = radio_group_type.indexOfChild(radioButton_type);
        String selectedType = type[idx_type];

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_flock";

        ParameterCreateFlock.Arguments args = new ParameterCreateFlock.Arguments(nid,flock_name,strLocation,selectedType,selectedPeriod,selectedtext_status,flock_capacity, effective_date);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateFlock parameter = new ParameterCreateFlock(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_flock(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                finish();
                //SubmitDialog();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Data Update Failed", Toast.LENGTH_LONG).show();
                finish();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
            }
        });
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}