package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.ParameterCreateFeed;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateFeed extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idFeedEditText, feedNameEditText, descriptionEditText;
    private RadioGroup radioGroup;
    private RadioButton activeRadioButton, inactiveRadioButton;
    private String id, name, description, feed_status, rid;
    String[] status = {"Active", "Inactive"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_feed);

        dbHelper = new DatabaseHelper (this);
        idFeedEditText = (EditText) findViewById(R.id.idFeedEditText);
        feedNameEditText = (EditText) findViewById(R.id.feedNameEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        radioGroup = (RadioGroup) findViewById(R.id.radio_status);
        activeRadioButton = (RadioButton) findViewById(R.id.radio_active);
        inactiveRadioButton = (RadioButton) findViewById(R.id.radio_inactive);

        /**
         * Kita cek apakah ada Bundle atau tidak
         */
        if(getIntent().getExtras()!=null){
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            id = bundle.getString("id");
            name = bundle.getString("name");
            description = bundle.getString("description");
            feed_status = bundle.getString("status");
            rid = bundle.getString("rid");
        }else{
            /**
             * Apabila Bundle tidak ada, ambil dari Intent
             */
            id = getIntent().getStringExtra("id");
            name = getIntent().getStringExtra("name");
            description = getIntent().getStringExtra("description");
            feed_status = getIntent().getStringExtra("status");
            rid = getIntent().getStringExtra("rid");
        }

         Log.d("intent","id "+id);
        Log.d("intent","name "+name);
        Log.d("intent","description "+description);
        Log.d("intent","status "+feed_status);
        Log.d("intent","rid "+rid);
        registerEvents();

/*        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM feed WHERE name = '" +
                getIntent().getStringExtra("name") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);*/
            idFeedEditText.setText(rid);
            feedNameEditText.setText(name);
            descriptionEditText.setText(description);
            //String status = cursor.getString(3).toString();
            //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            if(feed_status.equalsIgnoreCase("Active")){
                activeRadioButton.setChecked(true);
                inactiveRadioButton.setChecked(false);
            }else if(feed_status.equalsIgnoreCase("Inactive")){
                inactiveRadioButton.setChecked(true);
                activeRadioButton.setChecked(false);
            }
     //   }


        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                boolean valid=true;
                if (feedNameEditText.getText().toString().isEmpty()) {
                    feedNameEditText.setError("Please fill the data");
                    valid = false;
                }else if (descriptionEditText.getText().toString().isEmpty()){
                    descriptionEditText.setError("Please fill the data");
                    valid = false;
                }else if(radioGroup.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select data", Toast.LENGTH_SHORT).show();
                } else {
                    create_layerfarm_feed(rid);
               /* SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update feed set name='"+
                        feedNameEditText.getText().toString() +"', description='" +
                        descriptionEditText.getText().toString() +"', status='" +
                        status +"' where id='" +
                        idFeedEditText.getText().toString()+"'");*/
                }
            }
        });

    }

    private void registerEvents() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    feed_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    feed_status = "Inactive";
                }
            }

        });

    }

    private void create_layerfarm_feed(String nid){

        EditText a = (EditText) findViewById(R.id.feedNameEditText);
        String feed_name = (String)(a.getText().toString());

        EditText b = (EditText) findViewById(R.id.descriptionEditText);
        String farm_desc = (String)(b.getText().toString());

        int radioButtonID_status = radioGroup.getCheckedRadioButtonId();
        View radioButton_status = radioGroup.findViewById(radioButtonID_status);
//        Log.d("zzz","radio "+radioButton);
        int idx_status = radioGroup.indexOfChild(radioButton_status);
        RadioButton r_status = (RadioButton) radioGroup.getChildAt(idx_status);
        String selectedtext_status = status[idx_status];

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_feed";

        ParameterCreateFeed.Arguments args = new ParameterCreateFeed.Arguments(nid, feed_name,farm_desc, selectedtext_status);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateFeed parameter = new ParameterCreateFeed(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_feed(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                //SubmitDialog();
                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                //FeedActivity.ma.RefreshListFeed();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Data Update Failed", Toast.LENGTH_LONG).show();
                finish();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
            }
        });
    }

}