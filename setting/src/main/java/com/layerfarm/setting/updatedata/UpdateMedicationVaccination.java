package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.model.ListLocation;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.setting.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMedicationVaccination extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idProductEditText, productNameEditText, capacityEditText;
    private Spinner allVendor, allUnit;
    RadioGroup radio_group_type,radio_status;
    RadioButton radio_medicine,radio_vaccine,radio_vitamin,radio_probiotic,radio_disinfectan,radio_active,radio_inactive;
//    HashMap<String, String> vendor_list;
    String med_type = null;
    String med_status = null;
    String[] unit_name={"Vial","Liter","Mililiter","Kg","Gram" ,"Miligram","Bottle","Dozen", "Ds"};
    String[] type = {"Medicine", "Vaccine", "Vitamin", "Probiotic", "Disinfectant"};
    String[] status = {"Active","Inactive"};
    List<String> lables;
    HashMap<String, String> vendor_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_medication_vaccination);

        dbHelper = new DatabaseHelper(this);

        //Spinner Location
        allVendor=(Spinner)findViewById(R.id.spVendor);
        allUnit=(Spinner)findViewById(R.id.spUnit);
        idProductEditText = (EditText) findViewById(R.id.idProductEditText);
        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
        capacityEditText = (EditText) findViewById(R.id.capacityEditText);
        updateButton = (Button) findViewById(R.id.updateButton);

        radio_group_type = (RadioGroup) findViewById(R.id.radio_group_type);
        radio_status = (RadioGroup) findViewById(R.id.radio_status);

        radio_medicine = (RadioButton) findViewById(R.id.radio_medicine);
        radio_vaccine = (RadioButton) findViewById(R.id.radio_vaccine);
        radio_vitamin = (RadioButton) findViewById(R.id.radio_vitamin);
        radio_probiotic = (RadioButton) findViewById(R.id.radio_probiotic);
        radio_disinfectan = (RadioButton) findViewById(R.id.radio_disinfectan);
        radio_active = (RadioButton) findViewById(R.id.radio_active);
        radio_inactive = (RadioButton) findViewById(R.id.radio_inactive);

        Bundle extras = getIntent().getExtras();
        final String idx = extras.getString("id");
        final String product_name = extras.getString("product_name");
        final String vendor_name = extras.getString("vendor_name");
        final String capacity = extras.getString("capacity");
        final String unit = extras.getString("unit");
        final String type = extras.getString("type");
        final String status = extras.getString("status");
        final String rid = extras.getString("rid");
        final HashMap<String, String> vendor_list = (HashMap<String, String>)extras.getSerializable("vendor_list");
        if (!vendor_list.isEmpty()){
            vendor_map = ListLocation.getInstance().getList_location();
        }

        //Load Vendor data spinner
//        loadVendorSpinnerData();
        Collection<String> lables_col = vendor_map.values();

        lables = new ArrayList<String>(lables_col);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        allVendor.setAdapter(dataAdapter);

        registerEvents();

        //Load spinner unit
        //Spinner kind of chickin
        allUnit=(Spinner)findViewById(R.id.spUnit);
        // inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, unit_name);
        // mengeset Array Adapter tersebut ke Spinner
        allUnit.setAdapter(adapter);

        idProductEditText.setText(idx);
        productNameEditText.setText(product_name);
        allVendor.setSelection(getIndex(allVendor,vendor_name));
        capacityEditText.setText(capacity);
        Log.d("zzz","unit "+unit.toString()+" index "+ Integer.toString(getIndex(allUnit, unit)));
        //Unit
        allUnit.setSelection(getIndex(allUnit, unit));

        //Type
        //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
//        Log.d("zzz","type "+type.isEmpty());
        if(!type.isEmpty() || type != null) {
            if (type.equalsIgnoreCase("Medicine")) {
                radio_medicine.setChecked(true);
                radio_vaccine.setChecked(false);
                radio_vitamin.setChecked(false);
                radio_probiotic.setChecked(false);
                radio_disinfectan.setChecked(false);
            } else if (type.equalsIgnoreCase("Vaccine")) {
                radio_medicine.setChecked(false);
                radio_vaccine.setChecked(true);
                radio_vitamin.setChecked(false);
                radio_probiotic.setChecked(false);
                radio_disinfectan.setChecked(false);
            } else if (type.equalsIgnoreCase("Vitamin")) {
                radio_medicine.setChecked(false);
                radio_vaccine.setChecked(false);
                radio_vitamin.setChecked(true);
                radio_probiotic.setChecked(false);
                radio_disinfectan.setChecked(false);
            } else if (type.equalsIgnoreCase("Probiotic")) {
                radio_medicine.setChecked(false);
                radio_vaccine.setChecked(false);
                radio_vitamin.setChecked(false);
                radio_probiotic.setChecked(true);
                radio_disinfectan.setChecked(false);
            } else if (type.equalsIgnoreCase("Disinfectant")) {
                radio_medicine.setChecked(false);
                radio_vaccine.setChecked(false);
                radio_vitamin.setChecked(false);
                radio_probiotic.setChecked(false);
                radio_disinfectan.setChecked(true);
            }
        }


        //Status
        //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
        if(status.equalsIgnoreCase("Active")){
            radio_active.setChecked(true);
            radio_inactive.setChecked(false);
        }else if(status.equalsIgnoreCase("Inactive")){
            radio_active.setChecked(false);
            radio_inactive.setChecked(true);
        }

        updateButton = (Button) findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                boolean valid=true;
                if (productNameEditText.getText().toString().isEmpty()) {
                    productNameEditText.setError("Please fill the data");
                    valid = false;
                }else if (capacityEditText.getText().toString().isEmpty()){
                    capacityEditText.setError("Please fill the data");
                    valid = false;
                }else if(radio_group_type.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select type", Toast.LENGTH_SHORT).show();
                }else if(radio_status.getCheckedRadioButtonId()==-1)
                {
                    Toast.makeText(getApplicationContext(), "Please select status", Toast.LENGTH_SHORT).show();

                }
                else {
                    update_layerfarm_ovk(rid);
                }
            }
        });




        // mengeset listener untuk mengetahui saat item dipilih
        allUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                //Toast.makeText(CreateMedicationVaccinationActivity.this, "Selected "+ adapter.getItem(i) +" Mode", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });

    }

    private void loadVendorSpinnerData(){
        // database handler
        DataHelper db = new DataHelper(getApplicationContext());

        // Spinner Drop down elements
        lables = db.getAllVendor();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        allVendor.setAdapter(dataAdapter);
    }

    private void registerEvents() {
        radio_group_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_medicine) {
                    med_type = "Medicine";
                } else if (checkedId == R.id.radio_vaccine) {
                    med_type = "Vaccine";
                }else if (checkedId == R.id.radio_vitamin) {
                    med_type = "Vitamin";
                }else if (checkedId == R.id.radio_probiotic) {
                    med_type = "Probiotic";
                }else if (checkedId == R.id.radio_disinfectan) {
                    med_type = "Disinfectant";
                }
            }

        });


        radio_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_active) {
                    med_status = "Active";
                } else if (checkedId == R.id.radio_inactive) {
                    med_status = "Inactive";
                }
            }

        });


    }
    private void update_layerfarm_ovk(String nid){
        int radioButtonID = radio_group_type.getCheckedRadioButtonId();
        View radioButton = radio_group_type.findViewById(radioButtonID);
        Log.d("zzz","radio "+radioButton);
        int idx = radio_group_type.indexOfChild(radioButton);
        String selectedtext = type[idx];
        //Get location id
        String rid = nid;
        String product = productNameEditText.getText().toString();
        String vendor = allVendor.getSelectedItem().toString();
        String capacity = capacityEditText.getText().toString();
        String unit = allUnit.getSelectedItem().toString().toLowerCase();
        String types = selectedtext;
//        String state = status;
        String vendor_id = getKey(vendor_map, vendor);

        int radioButtonID_status = radio_status.getCheckedRadioButtonId();
        View radioButton_status = radio_status.findViewById(radioButtonID_status);
//        Log.d("zzz","radio "+radioButton);
        int idx_status = radio_status.indexOfChild(radioButton_status);
//        RadioButton r_status = (RadioButton) radio_status.getChildAt(idx_status);
        String state = status[idx_status];


//        String selectQueryLocation= "SELECT * FROM vendor WHERE name = '"+ vendor + "' LIMIT 1";
//        Cursor cursorLocation = db.rawQuery(selectQueryLocation, null);
//        String vendor_id = "";
//        if(cursorLocation.moveToFirst())
//            vendor_id  =  cursorLocation.getString(cursorLocation.getColumnIndex("rid"));
//        cursorLocation.close();

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_ovk";

        String[] args = {rid, product, vendor_id, capacity, unit, types, state};

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Parameter parameter = new Parameter(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.createOVK(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] nid = response.body();
//                updateMedication(nid[0]);
                Log.d("message","create OVK nid : "+nid[0]);
                Toast.makeText(UpdateMedicationVaccination.this, "Success",Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                //String alert = t.getMessage();
                Log.d("Layerfarm", "Update failure = " + t.getMessage());
                Toast.makeText(UpdateMedicationVaccination.this, "failure",Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
    private void updateMedication(String rid){
        //Get vendor id
        String vendor = allVendor.getSelectedItem().toString();
        String unit = allUnit.getSelectedItem().toString();
        // TODO Auto-generated method stub
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selectQueryVendor= "SELECT id FROM vendor WHERE name = '"+ vendor + "' LIMIT 1";
        Cursor cursorVendor = db.rawQuery(selectQueryVendor, null);
        String strVendor = "";
//        int rid = 0;
        if(cursorVendor.moveToFirst())
            strVendor  =  cursorVendor.getString(cursorVendor.getColumnIndex("id"));
        cursorVendor.close();


        db.execSQL("update medication_vaccination set product_name='"+
                productNameEditText.getText().toString() +"', vendor_id='" +
                strVendor  +"', capacity ='" +
                capacityEditText.getText().toString() +"', unit ='" +
                unit +"', type='" +
                type +"', status='" +
                status +"', rid='" +
                rid +"' where id='" +
                idProductEditText.getText().toString()+"'");
        Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
        //MedicationVaccinationActivity.ma.RefreshListMedication();
        finish();
    }

    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }
}