package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.model.ParameterCreateLocation;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.setting.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateLocation extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText locationNameEditText, locationAddressEditText,idLocationEditText;
    private String id, name, address,rid, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_location);
        dbHelper = new DatabaseHelper(this);
        idLocationEditText = (EditText) findViewById(R.id.idLocationEditText);
        locationNameEditText = (EditText) findViewById(R.id.locationNameEditText);
        locationAddressEditText = (EditText) findViewById(R.id.locationAddressEditText);

        /**
         * Kita cek apakah ada Bundle atau tidak
         */
        if(getIntent().getExtras()!=null){
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            id = bundle.getString("id");
            name = bundle.getString("name");
            address = bundle.getString("address");
            rid = bundle.getString("rid");
        }else{
            /**
             * Apabila Bundle tidak ada, ambil dari Intent
             */
            id = getIntent().getStringExtra("id");
            name = getIntent().getStringExtra("name");
            address = getIntent().getStringExtra("address");
            rid = getIntent().getStringExtra("rid");
        }

       /* Log.d("intent","id"+id);
        Log.d("intent","name"+name);
        Log.d("intent","address"+address);
        Log.d("intent","rid"+rid);*/

        idLocationEditText.setText(rid);
        locationNameEditText.setText(name);


       /* SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM location WHERE name = '" +
                getIntent().getStringExtra("name") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            idLocationEditText.setText(cursor.getString(0).toString());
            locationNameEditText.setText(cursor.getString(1).toString());
            locationAddressEditText.setText(cursor.getString(2).toString());
        }
*/
        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                create_layerfarm_location(rid);
                /*SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update location set name='"+
                        locationNameEditText.getText().toString() +"', address='" +
                        locationAddressEditText.getText().toString()+"' where id='" +
                        idLocationEditText.getText().toString()+"'");*/

            }
        });

    }

    public void create_layerfarm_location(String nid) {

        EditText a = (EditText) findViewById(R.id.locationNameEditText);
        String farm_name = (String)(a.getText().toString());


        String module = "layerfarm_android";
        String function_name = "layerfarm_android_create_farm";

        ParameterCreateLocation.Arguments args = new ParameterCreateLocation.Arguments(nid, farm_name, "", "");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterCreateLocation parameter = new ParameterCreateLocation(module, function_name, args);

        Call<String[]> call = apiInterfaceJson.create_layerfarm_location(token2, parameter);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] get_expense_general_respons = response.body();
                //Log.d("message","create_layerfarm_location : "+get_expense_general_respons.toString());
                //SubmitDialog();
                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Data Update Failed", Toast.LENGTH_LONG).show();
                finish();
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
            }
        });
    }
}