package com.layerfarm.setting.updatedata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.setting.R;

public class UpdateStrain extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button updateButton;
    EditText idStrainEditText, strainNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_strain);

        dbHelper = new DatabaseHelper(this);
        idStrainEditText = (EditText) findViewById(R.id.idStrainEditText);
        strainNameEditText = (EditText) findViewById(R.id.strainNameEditText);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM strain WHERE name = '" +
                getIntent().getStringExtra("name") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            idStrainEditText.setText(cursor.getString(0).toString());
            strainNameEditText.setText(cursor.getString(1).toString());
        }

        updateButton = (Button) findViewById(R.id.updateButton);
        // daftarkan even onClick pada btnSimpan
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update strain set name='"+
                        strainNameEditText.getText().toString() +"'where id='" +
                        idStrainEditText.getText().toString()+"'");
                Toast.makeText(getApplicationContext(), "Data Update Successfully", Toast.LENGTH_LONG).show();
                //StrainActivity.ma.RefreshListStrain();
                finish();
            }
        });

    }

}