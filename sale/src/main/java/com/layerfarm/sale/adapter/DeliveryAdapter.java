package com.layerfarm.sale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.layerfarm.sale.R;
import com.layerfarm.sale.model.StockPackOperation;
import com.layerfarm.sale.model.StockPicking;

import java.util.ArrayList;

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.ViewHolder> {
    private ArrayList<StockPackOperation> _retData;

    ArrayList<StockPackOperation> data;
    public DeliveryAdapter(ArrayList<StockPackOperation> items) {
        this.data = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView items_name;
        TextView todo;
        TextView done;
        CardView cv_delivery;
        ViewGroup delete;
        public ViewHolder(View itemView) {
            super(itemView);
            items_name = (TextView) itemView.findViewById(R.id.items_name);
            todo = (TextView) itemView.findViewById(R.id.todo);
            done = (TextView) itemView.findViewById(R.id.done);
            cv_delivery = (CardView) itemView.findViewById(R.id.cv_delivery_items);
            delete = (ViewGroup) itemView.findViewById(R.id.delete);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_delivery_items, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final String name = data.get(position).getProduct_id();
        final String ordered_qty = data.get(position).getOrdered_qty();
        final String qty_done = data.get(position).getQty_done();
        holder.items_name.setText(name);
        holder.todo.setText(ordered_qty);
        holder.done.setText(qty_done);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public void removeAt(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }
    public ArrayList<StockPackOperation> retrieveData()
    {
        return _retData;
    }
}
