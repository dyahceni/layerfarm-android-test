package com.layerfarm.sale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.sale.Delivery;
import com.layerfarm.sale.R;
import com.layerfarm.sale.model.StockPicking;

import java.util.ArrayList;
import java.util.List;

public class DeliveryListAdapter extends RecyclerView.Adapter<DeliveryListAdapter.ViewHolder> {
    ArrayList<StockPicking> data;
    public DeliveryListAdapter(ArrayList<StockPicking> items) {
        this.data = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView delivery_name;
        TextView status;
        CardView cv_list_delivery;
        public ViewHolder(View itemView) {
            super(itemView);
            delivery_name = (TextView) itemView.findViewById(R.id.delivery_name);
            status = (TextView) itemView.findViewById(R.id.status);
            cv_list_delivery = (CardView) itemView.findViewById(R.id.cv_list_delivery);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_list_delivery, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final String name = data.get(position).getName();
        final String state = data.get(position).getState();
        holder.delivery_name.setText(name);
        holder.status.setText(state);
        holder.cv_list_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent i = new Intent(context, Delivery.class);
                i.putExtra("name", name);
                i.putExtra("state", state);
                context.startActivity(i);
            }
        });
    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}
