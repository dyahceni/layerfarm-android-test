package com.layerfarm.sale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.sale.CreateSaleOrder;
import com.layerfarm.sale.R;
import com.layerfarm.sale.model.SaleOrder;

import java.util.ArrayList;

public class SaleOrderListAdapter extends RecyclerView.Adapter<SaleOrderListAdapter.ViewHolder> {
    ArrayList<SaleOrder> data;
    public SaleOrderListAdapter(ArrayList<SaleOrder> items){
        data = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView SO_name;
        TextView customer;
        TextView order_date;
        TextView total;
        CardView sale_order;
        public ViewHolder(View itemView) {
            super(itemView);
            SO_name = (TextView) itemView.findViewById(R.id.sale_name);
            customer = (TextView) itemView.findViewById(R.id.sale_customer);
            order_date = (TextView) itemView.findViewById(R.id.sale_order_date);
            total = (TextView) itemView.findViewById(R.id.sale_total);
            sale_order = (CardView) itemView.findViewById(R.id.cv_sale_order);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_sale_order_list, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SaleOrderListAdapter.ViewHolder holder, int position) {
        final String name = data.get(position).getOrder_number();
        String order_date = data.get(position).getOrder_date();
        String customer = data.get(position).getCustomer();
        String total = data.get(position).getTotal();
        final String status = data.get(position).getStatus();

        String content = "Order Date : "+order_date+"\n"+
                "Customer : "+customer+"\n"+
                "Total : "+total+"\n"+
                "Status : "+status;
        holder.SO_name.setText(name);
        holder.customer.setText(customer);
        holder.order_date.setText(order_date);
        holder.total.setText(total);
        holder.sale_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, CreateSaleOrder.class);
                i.putExtra("name", name);
                i.putExtra("state",status);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
