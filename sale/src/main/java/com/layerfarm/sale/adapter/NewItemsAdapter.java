package com.layerfarm.sale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.sale.NewItemsActivity;
import com.layerfarm.sale.R;
import com.layerfarm.sale.model.SaleOrderLine;

import java.util.ArrayList;

public class NewItemsAdapter extends RecyclerView.Adapter<NewItemsAdapter.ViewHolder> {
    ArrayList<SaleOrderLine> data;
    String status;
    public NewItemsAdapter(ArrayList<SaleOrderLine> items, String state){
        data = items;
        status = state;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView product_name;
        TextView quantity;
        TextView unit_price;
        TextView subtotal;
        CardView cv_product_items;
        public ViewHolder(View itemView) {
            super(itemView);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            unit_price = (TextView) itemView.findViewById(R.id.unit_price);
            subtotal = (TextView) itemView.findViewById(R.id.subtotal);
            cv_product_items = (CardView) itemView.findViewById(R.id.cv_product_items);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_list_product_items, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NewItemsAdapter.ViewHolder holder, final int position) {
        final String name = data.get(position).getName();
        final String qty = data.get(position).getQuantity();
        final String price = data.get(position).getUnit_price();
        final String uom = data.get(position).getUoM();
        final String subtotal = data.get(position).getSubtotal();
        final String tax = data.get(position).getTaxes();

        holder.product_name.setText(name);
        holder.quantity.setText(qty+" "+uom);
        holder.unit_price.setText(price+"/"+uom);
        holder.subtotal.setText(subtotal);
        holder.cv_product_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, NewItemsActivity.class);
                i.putExtra("state",status);
                i.putExtra("product", name);
                i.putExtra("quantity", qty);
                i.putExtra("UoM", uom);
                i.putExtra("unit_price", price);
                i.putExtra("taxes", tax);
                i.putExtra("subtotal", subtotal);
                i.putExtra("position",position );
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return data.size();
    }
}
