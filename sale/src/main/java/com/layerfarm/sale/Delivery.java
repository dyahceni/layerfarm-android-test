package com.layerfarm.sale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.sale.adapter.DeliveryAdapter;
import com.layerfarm.sale.function.SaleHelper;
import com.layerfarm.sale.model.StockPackOperation;
import com.layerfarm.sale.model.StockPicking;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class Delivery extends AppCompatActivity {
    TextView document_name, partner, warehouse, order_date, source_document;
    Button validate;
    Toolbar toolbar ;
    TextView toolbar_save, toolbar_title;

    StockPicking stock_picking;
    ArrayList<StockPackOperation> stock_pack_operation;

    RecyclerView rv_delivery_items;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private SQLiteDatabase database;
    DatabaseHelper db;
    SaleHelper saleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery);

        db  = new DatabaseHelper(this);
        saleHelper = new SaleHelper(this);
        saleHelper.open();

        partner = (TextView)findViewById(R.id.partner);
        warehouse = (TextView)findViewById(R.id.warehouse);
        source_document = (TextView)findViewById(R.id.source_document);
        order_date = (TextView) findViewById(R.id.order_date);
        validate = (Button)findViewById(R.id.validate);
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_save = (TextView) toolbar.findViewById(R.id.save);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);


        rv_delivery_items = (RecyclerView) findViewById(R.id.rv_delivery_item);

        rv_delivery_items.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_delivery_items.setLayoutManager(layoutManager);

        Bundle extras = getIntent().getExtras();
        String name = extras.getString("name");
        String state = extras.getString("state");

        toolbar_save.setText(state);
        toolbar_save.setEnabled(false);
        toolbar_save.setClickable(false);

        toolbar_title.setText(name);

        stock_picking = saleHelper.getStockPicking(name);
        stock_pack_operation = saleHelper.getStockPackOperation(name);
        adapter = new DeliveryAdapter(stock_pack_operation);
        adapter.notifyDataSetChanged();
        rv_delivery_items.setAdapter(adapter);

        partner.setText(stock_picking.getPartner());
        order_date.setText(stock_picking.getMin_date());
        warehouse.setText(stock_picking.getPicking_type_id());
        source_document.setText(stock_picking.getOrigin());
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }
    public void Back(View v) {
        finish();
    }
    public void validate(){

        Toast.makeText(this,"Validate",Toast.LENGTH_LONG).show();
        getContent();
    }
    public void getContent(){
        ArrayList<StockPackOperation> _data = new ArrayList<>();
        int flag = 0;
        int childCount = rv_delivery_items.getChildCount();
        //Log.d("hasil", "child_count : "+childCount);
        String showallPrompt = "";
        showallPrompt += "chilcCount :" +childCount+"\n\n";
        Toast.makeText(this,showallPrompt,Toast.LENGTH_LONG).show();

        for (int c =0; c<childCount;c++){
            StockPackOperation _stock_ = new StockPackOperation();
            View childView = rv_delivery_items.getChildAt(c);
            TextView name = (TextView)(childView.findViewById(R.id.items_name));
            TextView ordered_qty = (TextView) (childView.findViewById(R.id.todo));
            EditText qty_done = (EditText) (childView.findViewById(R.id.done));
            float todo = Float.parseFloat(ordered_qty.getText().toString());
            float done = Float.parseFloat(qty_done.getText().toString());
            _stock_.setProduct_id(name.getText().toString());
            _stock_.setOrdered_qty(ordered_qty.getText().toString());
            _stock_.setQty_done(qty_done.getText().toString());

            if (todo != done){
                flag = 1;
                ArrayList<StockPackOperation> data = new ArrayList<>();
                StockPackOperation stock = new StockPackOperation();
                stock.setProduct_id(name.getText().toString());
                float value = todo - done;
                if (value < 0)
                    value = value * -1;
                stock.setOrdered_qty(Float.toString(value));
                data.add(stock);
                database = db.getReadableDatabase();
                String selectQuery= "SELECT * FROM stock_picking ORDER BY _id DESC";
                Cursor cursor = database.rawQuery(selectQuery, null);
                long ID = 0;
                if(cursor.moveToFirst())
                    ID  =  cursor.getLong(cursor.getColumnIndex("_id"));
                cursor.close();
                String src = "WH/OUT/000"+(ID+1);
                long stock_id = 0;
                long stock_cancel =0;
                //jika ada selisih diantara mereka
                if (done<todo) {
                    stock_id = createStockPicking(src, stock_picking.getOrigin(), Integer.parseInt(stock_picking.getLocation_id()), "cancel", stock_picking.getMin_date(), stock_picking.getPartner());
                }
                else
                    stock_id = createStockPicking(src, stock_picking.getOrigin(), Integer.parseInt(stock_picking.getLocation_id()), "assign", stock_picking.getMin_date(), stock_picking.getPartner());
                createStockPackOperation(stock_id, data, stock_picking.getMin_date(), Integer.parseInt(stock_picking.getLocation_id()) );
            }
            _data.add(_stock_);
            UpdateStockPackOperation(_data,document_name.getText().toString());
        }
    }
    public void createStockPackOperation(long id, ArrayList<StockPackOperation> saleitems, String date, long location_id){
        database = db.getWritableDatabase();
        for (int i=0; i< saleitems.size(); i++){
            String name = saleitems.get(i).getProduct_id();
            String quantity = saleitems.get(i).getOrdered_qty();
            ContentValues content = new ContentValues();
            content.put("product_qty", quantity);
            content.put("qty_done", quantity);
            content.put("ordered_qty", quantity);
            content.put("product_id", name);
            content.put("picking_id", id);
            content.put("create_date",date);
            content.put("location_id",location_id);
            content.put("location_dest_id", "9");
            long insert = database.insert("stock_pack_operation", null, content);
        }
    }
    public long createStockPicking(String name, String origin, long location_id, String state, String date, String partner){
        database = db.getWritableDatabase();
        Log.d("stock_picking","origin = "+origin);
        Log.d("stock_picking","location_id = "+location_id);
        Log.d("stock_picking","state = "+state);
        Log.d("stock_picking","date = "+date);

        ContentValues values = new ContentValues();
        values.put("origin", origin);
        values.put("location_id", location_id);
        values.put("state", state);
        values.put("write_date", date);
        values.put("name", name);
        values.put("location_dest_id","9");
        values.put("partner_id", partner);
        values.put("picking_type_id", stock_picking.getPicking_type_id());
        values.put("min_date",date);
        long stock = database.insert("stock_picking", null, values);
        return stock;
    }
    public void UpdateStockPackOperation(ArrayList<StockPackOperation> data, String name){
        database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM stock_picking WHERE name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("_id"));
        cursor.close();
        //update dulu menjadi confirm
        ContentValues update_picking = new ContentValues();
        update_picking.put("state", "confirm");
        String Filter = "_id =" + ID;
        long updateRecording = database.update("stock_picking", update_picking, Filter, null);

        //update quantity masing2 items
        for (int i =0; i< data.size(); i++){
            String product = data.get(i).getProduct_id();
            database = db.getReadableDatabase();
            String select= "SELECT * FROM stock_pack_operation WHERE picking_id = '"+ID+"' AND product_id = '"+product+"'";
            Cursor cursorItems = database.rawQuery(select, null);
            long ID_items = 0;
            if(cursorItems.moveToFirst())
                ID_items  =  cursorItems.getLong(cursorItems.getColumnIndex("id"));
            cursorItems.close();
            //update dulu menjadi confirm
            ContentValues update_stock_op = new ContentValues();
            update_stock_op.put("product_qty", data.get(i).getOrdered_qty());
            update_stock_op.put("ordered_qty", data.get(i).getOrdered_qty());
            update_stock_op.put("qty_done", data.get(i).getQty_done());
            String Filter_stock_op = "id =" + ID_items;
            long update_stock_operation = database.update("stock_pack_operation", update_stock_op, Filter_stock_op, null);
        }
    }

}
