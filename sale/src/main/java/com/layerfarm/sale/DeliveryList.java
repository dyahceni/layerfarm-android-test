package com.layerfarm.sale;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.layerfarm.sale.adapter.DeliveryListAdapter;
import com.layerfarm.sale.model.StockPicking;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class DeliveryList extends AppCompatActivity {
    private SQLiteDatabase database;
    DatabaseHelper db;

    RecyclerView rv_list_delivery;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    String bundle_origin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_delivery);
        db = new DatabaseHelper(this);

        Bundle extras = getIntent().getExtras();
        bundle_origin = extras.getString("origin");
        Log.d("origin", "origin = "+bundle_origin);

        rv_list_delivery = (RecyclerView) findViewById(R.id.delivery_list);
        rv_list_delivery.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_list_delivery.setLayoutManager(layoutManager);


    }
    public ArrayList<StockPicking> getDocument(String origin){
        ArrayList<StockPicking> deliver = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM stock_picking where origin = '"+origin+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            StockPicking stock_picking = new StockPicking();
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String state = cursor.getString(cursor.getColumnIndex("state"));
            stock_picking.setName(name);
            stock_picking.setState(state);
            deliver.add(stock_picking);
        }
        return deliver;
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapter = new DeliveryListAdapter(getDocument(bundle_origin));
        adapter.notifyDataSetChanged();
        rv_list_delivery.setAdapter(adapter);
    }

}
