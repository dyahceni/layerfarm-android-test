package com.layerfarm.sale;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.layerfarm.sale.adapter.DraftSaleListAdapter;
import com.layerfarm.sale.adapter.SaleOrderListAdapter;
import com.layerfarm.sale.model.SaleOrder;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;
import java.util.ArrayList;

public class SalesOrderMainActivity extends AppCompatActivity {
    private SQLiteDatabase database;
    DatabaseHelper db;

    private RecyclerView rvSaleList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order_main);
        db = new DatabaseHelper(this);
        rvSaleList = (RecyclerView) findViewById(R.id.sale_content);
        rvSaleList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvSaleList.setLayoutManager(layoutManager);

        adapter = new SaleOrderListAdapter(getDocument());
        adapter.notifyDataSetChanged();
        rvSaleList.setAdapter(adapter);

    }
    public ArrayList<SaleOrder> getDocument(){
        ArrayList<SaleOrder> saleOrder = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM sale_order where state = '"+"confirm"+"' ORDER BY id ASC";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            SaleOrder sale = new SaleOrder();
            sale.setOrder_number(cursor.getString(cursor.getColumnIndex("name")));
            sale.setOrder_date(cursor.getString(cursor.getColumnIndex("date_order")));
            sale.setCustomer(cursor.getString(cursor.getColumnIndex("partner_id")));
            sale.setTotal(cursor.getString(cursor.getColumnIndex("amount_total")));
            sale.setStatus(cursor.getString(cursor.getColumnIndex("state")));
            saleOrder.add(sale);
        }
        return saleOrder;
    }
}
