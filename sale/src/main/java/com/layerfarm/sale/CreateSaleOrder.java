package com.layerfarm.sale;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.Fragment;

import com.layerfarm.sale.BottomClass.CurrencyBottomClass;
import com.layerfarm.sale.activity.CustomerActivity;
import com.layerfarm.sale.activity.ProductActivity;
import com.layerfarm.sale.activity.WarehouseActivity;
import com.layerfarm.sale.adapter.NewItemsAdapter;
import com.layerfarm.sale.function.SaleHelper;
import com.layerfarm.sale.model.ProductItemsManager;
import com.layerfarm.sale.model.SaleOrder;
import com.layerfarm.sale.model.SaleOrderLine;
import com.layerfarm.sale.model.Sales;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.layerfarm.setting.DataHelper;

public class CreateSaleOrder extends AppCompatActivity {
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    EditText currency, date_order, customer_name, warehouse_name;
    TextView total;
    RecyclerView rvProductItems;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<SaleOrderLine> saleOrderLines = new ArrayList<>();
    Toolbar toolbar ;
    TextView toolbar_save, toolbar_title;
    String state = "new";
    String bundle_name, bundle_state;

    Button button_delivery, button_invoice, button_confirm, button_delete;
    RelativeLayout new_items;
    long sale_order_id = 0;

    private SQLiteDatabase database;
    DatabaseHelper db;
    SaleHelper saleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_sale_order);
        db  = new DatabaseHelper(this);
        saleHelper = new SaleHelper(this);
        saleHelper.open();

        currency = (EditText) findViewById(R.id.currency);
        date_order = (EditText) findViewById(R.id.date_order);
        customer_name = (EditText)findViewById(R.id.customer_name);
        warehouse_name = (EditText) findViewById(R.id.warehouse_name);
        total = (TextView) findViewById(R.id.total);
        //setup toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar_save = (TextView) toolbar.findViewById(R.id.save);
        toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        button_delivery = (Button) findViewById(R.id.button_delivery);
        button_invoice = (Button) findViewById(R.id.button_invoice);
        button_confirm = (Button) findViewById(R.id.button_confirm);
        button_delete = (Button) findViewById(R.id.button_delete);


        rvProductItems = (RecyclerView) findViewById(R.id.items);
        rvProductItems.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvProductItems.setLayoutManager(layoutManager);

        currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CurrencyBottomClass bottom_currency = new CurrencyBottomClass();
                bottom_currency.show(getSupportFragmentManager(),"Currency");

                bottom_currency.setOnBottomSheetClickListener(new CurrencyBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        currency.setText(text);
                        bottom_currency.dismiss();
                    }
                });
            }

        });

        customer_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateSaleOrder.this, CustomerActivity.class);
                startActivity(intent);
            }
        });
        warehouse_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateSaleOrder.this, WarehouseActivity.class);
                startActivity(intent);
            }
        });

        date_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CreateSaleOrder.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                date_order.setText(date);
            }
        };

        new_items = (RelativeLayout) findViewById(R.id.new_items);

        new_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateSaleOrder.this, NewItemsActivity.class);
                i.putExtra("state",state);
                i.putExtra("product", "");
                i.putExtra("quantity", "");
                i.putExtra("UoM", "");
                i.putExtra("unit_price", "");
                i.putExtra("taxes", "");
                i.putExtra("subtotal", "");
                i.putExtra("position","");
                startActivity(i);
            }
        });
        Bundle extras = getIntent().getExtras();
        bundle_name = extras.getString("name");
        bundle_state = extras.getString("state");


        Log.d("sale","name = "+bundle_name);
        Log.d("sale","state = "+bundle_state);
        //jika nama dokument tidak kosong
        if (!bundle_name.equals("")){
            //jika state adalah confirm
            if (!bundle_state.equals("draft")){
                setConfirmDesignPage();
                state = "confirm";
            }
            else{
                state = "draft";
                setDraftDesignPage();
            }

            //ArrayList<SaleOrder> sale_order = saleHelper.getSaleOrderData(bundle_name);
            SaleOrder sale_order = saleHelper.getSaleOrder(bundle_name);
            long id = saleHelper.getSaleOrderID(bundle_name);
            //Log.d("sale","ID = "+id);
            ArrayList<SaleOrderLine> sale_order_line = saleHelper.getSaleOrderLine(id);
            ProductItemsManager.getInstance().setSaleOrderLines(sale_order_line);
            toolbar_title.setText(bundle_name);
            date_order.setText(sale_order.getOrder_date());
            Log.d("sale","customer_name = "+sale_order.getCustomer());
            CustomerActivity.getInstance().setProduct(sale_order.getCustomer());
            WarehouseActivity.getInstance().setProduct(sale_order.getWarehouse());
            currency.setText(sale_order.getPricelist());
            //customer_name.setText(sale_order.get(0).getCustomer());
            //state adalah confirm

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        customer_name.setText(CustomerActivity.getInstance().getProduct());
        warehouse_name.setText(WarehouseActivity.getInstance().getProduct());
        saleOrderLines = ProductItemsManager.getInstance().getSaleOrderLines();
        adapter = new NewItemsAdapter(saleOrderLines, state);
        adapter.notifyDataSetChanged();
        rvProductItems.setAdapter(adapter);
        getTotal();
    }
    @Override
    public void onBackPressed() {
        finish();
        ArrayList <SaleOrderLine> new_SOL= new ArrayList<>();
        ProductItemsManager.getInstance().setSaleOrderLines(new_SOL);
        CustomerActivity.getInstance().setProduct("");
        WarehouseActivity.getInstance().setProduct("");
    }

    public void Back(View v) {
        // does something very interesting
        finish();
        ArrayList <SaleOrderLine> new_SOL= new ArrayList<>();
        ProductItemsManager.getInstance().setSaleOrderLines(new_SOL);
    }
    public void setConfirmDesignPage(){
        button_delivery.setText(String.valueOf(getPicking(bundle_name))+" DELIVERY");
        button_delivery.setVisibility(View.VISIBLE);
        button_invoice.setVisibility(View.VISIBLE);
        button_confirm.setVisibility(View.GONE);
        button_delete.setVisibility(View.GONE);
        //set black order
        date_order.setFocusable(false);
        date_order.setEnabled(false);
        date_order.setClickable(false);
        date_order.setTextColor(Color.BLACK);

        //set currency
        currency.setFocusable(false);
        currency.setEnabled(false);
        currency.setClickable(false);
        currency.setTextColor(Color.BLACK);

        //set customer_name
        customer_name.setFocusable(false);
        customer_name.setEnabled(false);
        customer_name.setClickable(false);
        customer_name.setTextColor(Color.BLACK);

        //set warehouse_name
        warehouse_name.setFocusable(false);
        warehouse_name.setEnabled(false);
        warehouse_name.setClickable(false);
        warehouse_name.setTextColor(Color.BLACK);
    }
    public void setDraftDesignPage(){
        toolbar_save.setVisibility(View.INVISIBLE);
        button_confirm.setVisibility(View.VISIBLE);
        button_delete.setVisibility(View.VISIBLE);
    }
    public void save(View v){
        Toast.makeText(this,"Save was clicked!!",Toast.LENGTH_LONG).show();
        Save("draft");
        setDraftDesignPage();
    }
    public void confirm(View v){
        Confirm();
        setConfirmDesignPage();
    }
    public void delivery(View v){
        Intent intent = new Intent(CreateSaleOrder.this, DeliveryList.class);
        intent.putExtra("origin", bundle_name);
        startActivity(intent);
    }
    public void getTotal(){
        float total_price = 0;
        for(int a =0; a<saleOrderLines.size(); a++){
            Log.d("saleorderline","name = "+saleOrderLines.get(a).getName());
            String tx_subtotal = saleOrderLines.get(a).getSubtotal();
            Log.d("saleorderline","subtotal = "+tx_subtotal);
            if (tx_subtotal != null){
                float subs = Float.parseFloat(tx_subtotal);
                total_price = total_price+ subs;
            }
        }
        total.setText(Float.toString(total_price));
    }

    public void Save(String state){
        long id = saleHelper.createSaleOrder(total.getText().toString(),date_order.getText().toString(), customer_name.getText().toString(), state, currency.getText().toString(), warehouse_name.getText().toString());
        sale_order_id = id;
        saleHelper.createSaleOrderLine(id,saleOrderLines,"draft");
        String code_txt = "SO/000"+id;
        toolbar_title.setText(code_txt);
    }

    public void Confirm(){
        /*ArrayList<Sales> sales = getTable();
        ArrayList<SaleOrderLine> saleItems = new ArrayList<>();
        String total = new String();
        for (int i =0; i< sales.size(); i++){
            total = sales.get(i).getTotal();
            saleItems = sales.get(i).getSaleItems();
        }
        //jika belum di save, maka save terlebih dahulu
        if (save_flag == 0){
            Save("confirm");
        }*/
        long id = updateSaleOrder(toolbar_title.getText().toString());
        updateSaleOrderLine(id);
        long location_id = saleHelper.getWarehouseID(warehouse_name.getText().toString());
        String partner = customer_name.getText().toString();
        long stock_id = createStockPicking(toolbar_title.getText().toString(), location_id, "assign", date_order.getText().toString(),partner);
        createStockPackOperation(stock_id, saleOrderLines, date_order.getText().toString(), location_id );
    }



    public void createStockPackOperation(long id, ArrayList<SaleOrderLine> saleitems, String date, long location_id){
        database = db.getWritableDatabase();
        for (int i=0; i< saleitems.size(); i++){
            String name = saleitems.get(i).getName();
            String quantity = saleitems.get(i).getQuantity();
            String unit_price = saleitems.get(i).getUnit_price();
            String total = saleitems.get(i).getSubtotal();

            ContentValues content = new ContentValues();
            content.put("product_qty", quantity);
            content.put("ordered_qty", quantity);
            content.put("product_id", name);
            content.put("picking_id", id);
            content.put("create_date",date);
            content.put("location_id",location_id);
            content.put("location_dest_id", "9");
            long insert = database.insert("stock_pack_operation", null, content);
        }
    }
    public long createStockPicking(String origin, long location_id, String state, String date, String partner){
        Log.d("stock_picking","origin = "+origin);
        Log.d("stock_picking","location_id = "+location_id);
        Log.d("stock_picking","state = "+state);
        Log.d("stock_picking","date = "+date);

        database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM stock_picking ORDER BY _id DESC";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("_id"));
        cursor.close();
        String name = "WH/OUT/000"+(ID+1);

        ContentValues stock_picking = new ContentValues();
        stock_picking.put("origin", origin);
        stock_picking.put("location_id", location_id);
        stock_picking.put("state", state);
        stock_picking.put("write_date", date);
        stock_picking.put("name", name);
        stock_picking.put("location_dest_id","9");
        stock_picking.put("partner_id", partner);
        stock_picking.put("picking_type_id", warehouse_name.getText().toString());
        stock_picking.put("min_date",date);
        long stock = database.insert("stock_picking", null, stock_picking);
        return stock;
    }

    public long updateSaleOrder(String name){
        database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM sale_order where name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("id"));
        cursor.close();
        String Filter = "id=" + ID;
        //memasukkan ke content values
        ContentValues update = new ContentValues();
        //masukkan data sesuai dengan kolom pada database
        update.put("state", "confirm");
        update.put("amount_total", total.getText().toString());
        update.put("name", name);
        update.put("date_order", date_order.getText().toString());
        update.put("partner_id", customer_name.getText().toString());
        update.put("invoice_status", "confirm");
        update.put("pricelist_id",currency.getText().toString());
        update.put("warehouse_id", warehouse_name.getText().toString());

        //update query
        long update_saleorder = database.update("sale_order", update, Filter, null);
        return ID;
    }
    public void updateSaleOrderLine(long id){
        database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM sale_order_line WHERE order_id = '"+id+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        for (int i =0; i< cursor.getCount(); i++){
            long ID = cursor.getLong(cursor.getColumnIndex("id"));
            String unit_price = saleOrderLines.get(i).getUnit_price();
            String quantity = saleOrderLines.get(i).getQuantity();
            String subtotal = saleOrderLines.get(i).getSubtotal();
            String price_total = subtotal;
            String product_uom = saleOrderLines.get(i).getUoM();
            String price_tax = saleOrderLines.get(i).getTaxes();
            String name = saleOrderLines.get(i).getName();


            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("state", "confirm");
            update.put("price_unit", unit_price);
            update.put("product_uom_qty", quantity);
            update.put("price_subtotal", subtotal);
            update.put("order_id", id);
            update.put("product_id", 1);
            update.put("price_total", price_total);
            update.put("product_uom", product_uom);
            update.put("price_tax", price_tax);
            update.put("name", name);

            //update query
            long update_saleorderline = database.update("sale_order_line", update, Filter, null);
        }
    }
    public int getPicking(String origin){
        int value =0;
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM stock_picking WHERE origin = '"+origin +"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        value = cursor.getCount();
        Log.d("doc","origin = "+origin);
        Log.d("doc","picking = "+value);
        return value;
    }
    public ArrayList<String> getWarehouseName(){
        ArrayList<String> warehousename = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT name FROM stock_warehouse ORDER BY name ASC";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String name = cursor.getString(cursor.getColumnIndex("name"));
            warehousename.add(name);
        }
        return warehousename;
    }
    public String getDate(){
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);
        return today;
    }


}
