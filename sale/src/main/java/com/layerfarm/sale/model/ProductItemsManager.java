package com.layerfarm.sale.model;

import java.util.ArrayList;

public class ProductItemsManager {
    private static ProductItemsManager staticInstance;

    public static ProductItemsManager getInstance(){
        if (staticInstance==null){
            staticInstance = new ProductItemsManager();
        }

        return staticInstance;
    }
    //di inisialisasi disini
    public ArrayList<SaleOrderLine> saleOrderLines = new ArrayList<>();
    public SaleOrderLine saleOrderLine = new SaleOrderLine();
    public Sales sales = new Sales();

    public void setSaleOrderLine(SaleOrderLine saleorderLine) {
        this.saleOrderLine = saleorderLine;
    }

    public SaleOrderLine getSaleOrderLine() {
        return saleOrderLine;
    }

    public ArrayList<SaleOrderLine> getSaleOrderLines() {
        return saleOrderLines;
    }

    public void setSaleOrderLines(ArrayList<SaleOrderLine> saleOrderLines) {
        this.saleOrderLines = saleOrderLines;
    }

    public void setSales(Sales sales){
        this.sales = sales;
    }
    public Sales getSales(){
        return sales;
    }
}
