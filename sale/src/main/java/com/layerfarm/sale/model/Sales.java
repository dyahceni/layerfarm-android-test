package com.layerfarm.sale.model;

import java.util.ArrayList;

public class Sales {
    ArrayList<SaleOrderLine> saleItems;
    String total ;

    public ArrayList<SaleOrderLine> getSaleItems() {
        return saleItems;
    }

    public void setSaleItems(ArrayList<SaleOrderLine> saleItems) {
        this.saleItems = saleItems;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
