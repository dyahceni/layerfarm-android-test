package com.layerfarm.sale.model;

public class SaleOrderLine {
    String name;
    String quantity;
    String unit_price;
    String subtotal;
    String UoM;
    String taxes;

    public String getUoM() {
        return UoM;
    }

    public void setUoM(String uoM) {
        UoM = uoM;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getName() {
        return name;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public String getSubtotal() {
        return subtotal;
    }
}
