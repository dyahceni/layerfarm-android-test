package com.layerfarm.sale.model;

public class StockPicking {
    String name;
    String state;
    String partner;
    String origin;
    String location_id;
    String location_name;
    String picking_type_id;
    String backorder_id;
    String min_date;
    String write_date;
    String location_dest_id;
    String location_dest_name;
    String stock_picking_id;

    public String getStock_picking_id() {
        return stock_picking_id;
    }

    public void setStock_picking_id(String stock_picking_id) {
        this.stock_picking_id = stock_picking_id;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getPicking_type_id() {
        return picking_type_id;
    }

    public void setPicking_type_id(String picking_type_id) {
        this.picking_type_id = picking_type_id;
    }

    public String getBackorder_id() {
        return backorder_id;
    }

    public void setBackorder_id(String backorder_id) {
        this.backorder_id = backorder_id;
    }

    public String getMin_date() {
        return min_date;
    }

    public void setMin_date(String min_date) {
        this.min_date = min_date;
    }

    public String getWrite_date() {
        return write_date;
    }

    public void setWrite_date(String write_date) {
        this.write_date = write_date;
    }

    public String getLocation_dest_id() {
        return location_dest_id;
    }

    public void setLocation_dest_id(String location_dest_id) {
        this.location_dest_id = location_dest_id;
    }

    public String getLocation_dest_name() {
        return location_dest_name;
    }

    public void setLocation_dest_name(String location_dest_name) {
        this.location_dest_name = location_dest_name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
