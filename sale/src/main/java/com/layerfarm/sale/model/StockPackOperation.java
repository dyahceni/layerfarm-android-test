package com.layerfarm.sale.model;

public class StockPackOperation {
    String ordered_qty;
    String qty_done;
    String product_id;


    public String getOrdered_qty() {
        return ordered_qty;
    }

    public void setOrdered_qty(String ordered_qty) {
        this.ordered_qty = ordered_qty;
    }

    public String getQty_done() {
        return qty_done;
    }

    public void setQty_done(String qty_done) {
        this.qty_done = qty_done;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }




}
