package com.layerfarm.sale;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.sale.BottomClass.UoMBottomClass;
import com.layerfarm.sale.activity.ProductActivity;
import com.layerfarm.sale.model.ProductItemsManager;
import com.layerfarm.sale.model.SaleOrderLine;

public class NewItemsActivity extends AppCompatActivity {
    EditText ed_product, quantity, UoM, unit_price, taxes;
    TextView subtotal;
    String product_items;
    Button save ;

    String bundle_state, bundle_product, bundle_quantity, bundle_UoM, bundle_unit_price,bundle_taxes, bundle_subtotal;
    int bundle_posistion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_items);
        ed_product = (EditText) findViewById(R.id.product);
        quantity = (EditText) findViewById(R.id.quantity);
        UoM = (EditText) findViewById(R.id.UoM);
        unit_price = (EditText) findViewById(R.id.unit_price);
        taxes = (EditText) findViewById(R.id.taxes);
        subtotal = (TextView) findViewById(R.id.subtotal);
//        save = (Button) findViewById(R.id.save);
        Bundle extras = getIntent().getExtras();

        bundle_state = extras.getString("state");
        bundle_product = extras.getString("product");
        bundle_quantity = extras.getString("quantity");
        bundle_UoM = extras.getString("UoM");
        bundle_unit_price = extras.getString("unit_price");
        bundle_taxes = extras.getString("taxes");
        bundle_subtotal = extras.getString("subtotal");
        bundle_posistion = extras.getInt("position");
        Log.d("bundle","bundle_product = "+bundle_product);
        Log.d("bundle","bundle_quantity = "+bundle_quantity);
        Log.d("bundle","bundle_UoM = "+bundle_UoM);
        Log.d("bundle","bundle_unit_price = "+bundle_unit_price);
        Log.d("bundle","bundle_taxes = "+bundle_taxes);
        Log.d("bundle","bundle_subtotal = "+bundle_subtotal);
        Log.d("bundle","bundle_position = "+bundle_posistion);
        Log.d("bundle","getproduct = "+ProductActivity.getInstance().getProduct());

        if (!bundle_product.equals("")){
            ed_product.setText(bundle_product);
            quantity.setText(bundle_quantity);
            UoM.setText(bundle_UoM);
            taxes.setText(bundle_taxes);
            unit_price.setText(bundle_unit_price);
            subtotal.setText(bundle_subtotal);
        }
        ed_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewItemsActivity.this, ProductActivity.class);
                startActivity(intent);
            }
        });
        UoM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UoMBottomClass bottom_UoM = new UoMBottomClass();
                bottom_UoM.show(getSupportFragmentManager(),"UoM");

                bottom_UoM.setOnBottomSheetClickListener(new UoMBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        UoM.setText(text);
                        bottom_UoM.dismiss();
                    }
                });
            }
        });
        quantity.addTextChangedListener(new GenericTextWatcher(quantity));
        unit_price.addTextChangedListener(new GenericTextWatcher(unit_price));
        if (bundle_state.equals("confirm")){
            setDesaignConfirm();
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (ProductActivity.getInstance().getProduct()==""||ProductActivity.getInstance().getProduct()==null){
            ed_product.setText(bundle_product);
        }
        else
            ed_product.setText(ProductActivity.getInstance().getProduct());
    }
    public void save(View v){
        Toast.makeText(this,"Save was clicked!!",Toast.LENGTH_LONG).show();
        if (bundle_state.equals("new")){
            ProductActivity.getInstance().setProduct("");
            SaleOrderLine saleOrderLine = new SaleOrderLine();
            saleOrderLine.setName(ed_product.getText().toString());
            saleOrderLine.setQuantity(quantity.getText().toString());
            saleOrderLine.setUnit_price(unit_price.getText().toString());
            saleOrderLine.setSubtotal(subtotal.getText().toString());
            saleOrderLine.setTaxes(taxes.getText().toString());
            saleOrderLine.setUoM(UoM.getText().toString());
            ProductItemsManager.getInstance().getSaleOrderLines().add(saleOrderLine);
        }
        else{
            ProductActivity.getInstance().setProduct("");
            SaleOrderLine saleOrderLine = new SaleOrderLine();
            saleOrderLine.setName(ed_product.getText().toString());
            saleOrderLine.setQuantity(quantity.getText().toString());
            saleOrderLine.setUnit_price(unit_price.getText().toString());
            saleOrderLine.setSubtotal(subtotal.getText().toString());
            saleOrderLine.setTaxes(taxes.getText().toString());
            saleOrderLine.setUoM(UoM.getText().toString());
            ProductItemsManager.getInstance().getSaleOrderLines().set(bundle_posistion,saleOrderLine);
        }
        finish();
    }
    public void setDesaignConfirm(){
        //set currency
        ed_product.setFocusable(false);
        ed_product.setEnabled(false);
        ed_product.setClickable(false);
        ed_product.setTextColor(Color.BLACK);

        UoM.setFocusable(false);
        UoM.setEnabled(false);
        UoM.setClickable(false);
        UoM.setTextColor(Color.BLACK);
    }
    public void Back(View v) {
        // does something very interesting
        finish();
        ProductActivity.getInstance().setProduct("");
    }
    private class GenericTextWatcher implements TextWatcher {

        private View view;
        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.quantity) {
                if((!quantity.getText().toString().equals("")) && (!unit_price.getText().toString().equals(""))){
                    //int val = Integer.parseInt(qty.getText().toString()) * Integer.parseInt(price.getText().toString());
                    float val = Float.parseFloat(unit_price.getText().toString())*Float.parseFloat(quantity.getText().toString());
                    subtotal.setText(Float.toString(val));
                }

            } else if (i == R.id.unit_price) {
                if((!quantity.getText().toString().equals("")) && (!unit_price.getText().toString().equals(""))){
                    //int val = Integer.parseInt(qty.getText().toString()) * 2;
                    float val = Float.parseFloat(unit_price.getText().toString())*Float.parseFloat(quantity.getText().toString());
                    subtotal.setText(Float.toString(val));
                }
                /*Toast.makeText(getApplicationContext(), "value was "+index+" unitpeice",
                        Toast.LENGTH_LONG).show();*/

            }
        }
    }
}
