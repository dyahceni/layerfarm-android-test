package com.layerfarm.sale.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.layerfarm.sale.R;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class WarehouseActivity extends AppCompatActivity {
    private static WarehouseActivity staticInstance;
    EditText search;
    ListView warehouse_list;

    // Listview Data
    String warehouses[] = {"Kandang 01", "Kandang 02", "Kandang 03", "Kandang 04", "Kandang 05",
            "Kandang 06", "Kandang 07", "Kandang 08", "Kandang 09", "Kandang 10", "Kandang 11"};

    // Listview Adapter
    ArrayAdapter<String> adapter;

    private SQLiteDatabase database;
    DatabaseHelper db;

    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    public static WarehouseActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new WarehouseActivity();
        }
        return staticInstance;
    }
    private static String warehouse_items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse);
        search = (EditText) findViewById(R.id.search);
        db  = new DatabaseHelper(this);
        warehouse_list = (ListView) findViewById(R.id.warehouse_items);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getWarehouseName());
        warehouse_list.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                WarehouseActivity.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        warehouse_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = warehouse_list.getItemAtPosition(position).toString();
                setProduct(s);
                finish();
            }
        });
    }
    public ArrayList<String> getWarehouseName(){
        ArrayList<String> warehousename = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT name FROM stock_warehouse ORDER BY name ASC";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String name = cursor.getString(cursor.getColumnIndex("name"));
            warehousename.add(name);
        }
        return warehousename;
    }
    public void setProduct(String warehouse_items){
        this.warehouse_items = warehouse_items;
    }
    public String getProduct(){
        return warehouse_items;
    }
}
