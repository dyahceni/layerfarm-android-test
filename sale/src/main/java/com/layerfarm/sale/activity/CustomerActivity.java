package com.layerfarm.sale.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.layerfarm.sale.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerActivity extends Activity implements AdapterView.OnItemClickListener {
    private EditText mEditText;
    //private InteractionListener mListener;
    private static CustomerActivity staticInstance;
    EditText search;
    ListView customer_list;

    // Listview Data
    String customer[] = {"Aan Rohmana", "Budi Cahaya", "Cv.Agro SM", "Ibu Ellsa Soeyono"};

    // Listview Adapter
    ArrayAdapter<String> adapter;

/* using fragment
    public CustomerActivity() {
        // Required empty public constructor
    }

    public void setOnCustomerClickListener(InteractionListener l) {
        mListener = l;
    }

    public static CustomerActivity newInstance() {
        CustomerActivity fragment = new CustomerActivity();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_customer, container, false);
        search = (EditText) view.findViewById(R.id.search);
        customer_list = (ListView) view.findViewById(R.id.customer_items);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, customer);
        customer_list.setAdapter(adapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                CustomerActivity.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        customer_list.setOnItemClickListener(this);
        return view;
    }

    public void sentString() {
        mListener.onFragmentInteraction(mEditText.getText().toString());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String s = customer_list.getItemAtPosition(position).toString();
        mListener.onFragmentInteraction(s);
    }

    public interface InteractionListener {
        void onFragmentInteraction(String string);
    }
*/

    public static CustomerActivity getInstance(){
        if (staticInstance == null){
            staticInstance = new CustomerActivity();
        }
        return staticInstance;
    }
    private static String customer_items;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String s = customer_list.getItemAtPosition(position).toString();
        setProduct(s);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        search = (EditText) findViewById(R.id.search);
        customer_list = (ListView) findViewById(R.id.customer_items);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, customer);
        customer_list.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                CustomerActivity.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        customer_list.setOnItemClickListener(this);
    }
    public void setProduct(String customer_items){
        this.customer_items = customer_items;
    }
    public String getProduct(){
        return customer_items;
    }


}
