package com.layerfarm.sale.BottomClass;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.layerfarm.sale.R;

import java.util.ArrayList;

public class CurrencyBottomClass extends BottomSheetDialogFragment implements AdapterView.OnItemClickListener {
    private BottomSheetListener mListener;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list = new ArrayList<>();
    private String[] currency={
            "IDR","USD","SGD","INR","EUR","JPY"
    };

    public void setOnBottomSheetClickListener(BottomSheetListener l) {
        mListener = l;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onButtonClicked(currency[position]);
        dismiss();
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_currency, container, false);
        listView = (ListView) v.findViewById(R.id.currency);
        arrayAdapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,currency);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        return v;

    }


}
