package com.layerfarm.sale.function;

import android.content.ContentValues;
import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.layerfarm.sale.model.SaleOrder;
import com.layerfarm.sale.model.SaleOrderLine;
import com.layerfarm.sale.model.StockPackOperation;
import com.layerfarm.sale.model.StockPicking;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

public class SaleHelper {
    DatabaseHelper db;
    private SQLiteDatabase database;
    public SaleHelper(Context context){
        db = new DatabaseHelper(context);
    }
    //membuat sambungan ke database
    public void open() throws SQLException {
        database= db.getWritableDatabase();

    }
    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    public SaleOrder getSaleOrder(String name){
        SaleOrder sale = new SaleOrder();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM sale_order WHERE name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            sale.setOrder_number(cursor.getString(cursor.getColumnIndex("name")));
            sale.setOrder_date(cursor.getString(cursor.getColumnIndex("date_order")));
            sale.setCustomer(cursor.getString(cursor.getColumnIndex("partner_id")));
            sale.setTotal(cursor.getString(cursor.getColumnIndex("amount_total")));
            sale.setStatus(cursor.getString(cursor.getColumnIndex("state")));
            sale.setPricelist(cursor.getString(cursor.getColumnIndex("pricelist_id")));
            sale.setWarehouse(cursor.getString(cursor.getColumnIndex("warehouse_id")));
        }
        return sale;
    }
    public ArrayList<SaleOrderLine> getSaleOrderLine(long id){
        ArrayList<SaleOrderLine> sale_order_line = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM sale_order_line WHERE order_id = '"+id +"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            SaleOrderLine sale_line = new SaleOrderLine();
            sale_line.setUnit_price(cursor.getString(cursor.getColumnIndex("price_unit")));
            sale_line.setQuantity(cursor.getString(cursor.getColumnIndex("product_uom_qty")));
            sale_line.setSubtotal(cursor.getString(cursor.getColumnIndex("price_subtotal")));
            sale_line.setName(cursor.getString(cursor.getColumnIndex("name")));
            sale_line.setUoM(cursor.getString(cursor.getColumnIndex("product_uom")));
            sale_line.setTaxes(cursor.getString(cursor.getColumnIndex("price_tax")));
            sale_order_line.add(sale_line);
        }
        return sale_order_line;
    }
    public long getSaleOrderID(String name){
        long id = 0;
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM sale_order WHERE name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            id = cursor.getLong(cursor.getColumnIndex("id"));
        }
        return id;
    }
    public long getWarehouseID(String name){
        long id = 0;
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM stock_warehouse WHERE name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            id = cursor.getLong(cursor.getColumnIndex("id"));
        }
        return id;
    }
    public StockPicking getStockPicking(String name){
        StockPicking picking = new StockPicking();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT origin, stock_picking.partner_id, state, backorder_id, min_date, stock_picking.write_date, \n" +
                "location_dest_id, stock_picking.name, stock_warehouse.name, stock_picking.location_id, picking_type_id, complete_name, stock_picking._id \n" +
                "FROM stock_picking INNER JOIN stock_warehouse on stock_picking.location_id = stock_warehouse.id \n" +
                "INNER JOIN stock_location on stock_picking.location_dest_id = stock_location.id WHERE stock_picking.name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){

            String origin = cursor.getString(cursor.getColumnIndex("origin"));
            String partner_id = cursor.getString(1);
            String state= cursor.getString(cursor.getColumnIndex("state"));
            String backorder_id= cursor.getString(cursor.getColumnIndex("backorder_id"));
            String min_date= cursor.getString(cursor.getColumnIndex("min_date"));
            String write_date= cursor.getString(cursor.getColumnIndex("write_date"));
            String location_dest_id= cursor.getString(cursor.getColumnIndex("location_dest_id"));
            String document_name = cursor.getString(cursor.getColumnIndex("name"));
            String location_name= cursor.getString(8);
            String location_id= cursor.getString(9);
            String picking_type_id = cursor.getString(cursor.getColumnIndex("picking_type_id"));
            String location_dest_name = cursor.getString(cursor.getColumnIndex("complete_name"));
            String id = cursor.getString(12);

            picking.setOrigin(origin);
            picking.setPartner(partner_id);
            picking.setState(state);
            picking.setBackorder_id(backorder_id);
            picking.setMin_date(min_date);
            picking.setWrite_date(write_date);
            picking.setLocation_dest_id(location_dest_id);
            picking.setName(document_name);
            picking.setLocation_name(location_name);
            picking.setLocation_id(location_id);
            picking.setPicking_type_id(picking_type_id);
            picking.setLocation_dest_name(location_dest_name);
            picking.setStock_picking_id(id);
        }
        return picking;
    }
    public ArrayList<StockPackOperation> getStockPackOperation(String name){
        ArrayList<StockPackOperation> stock_pack_operation = new ArrayList<>();
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT ordered_qty, qty_done, product_id FROM stock_pack_operation INNER JOIN stock_picking on stock_pack_operation.picking_id = stock_picking._id \n" +
                "WHERE name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            StockPackOperation stock_pack = new StockPackOperation();
            stock_pack.setOrdered_qty(cursor.getString(cursor.getColumnIndex("ordered_qty")));
            stock_pack.setQty_done(cursor.getString(cursor.getColumnIndex("qty_done")));
            stock_pack.setProduct_id(cursor.getString(cursor.getColumnIndex("product_id")));
            stock_pack_operation.add(stock_pack);
        }
        return stock_pack_operation;
    }
    //fungsi digunakan untuk create di table Sale Order
    public long createSaleOrder(String total, String date_order, String partner_id, String invoice_status, String pricelist, String warehouse){
        database = db.getReadableDatabase();
        String selectQuery= "SELECT id FROM sale_order ORDER BY id DESC LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("id"));
        cursor.close();
        String name = "SO/000"+(ID+1);
        database = db.getWritableDatabase();
        //RECORDING MedicineVaccination
        ContentValues sale_order = new ContentValues();
        sale_order.put("amount_total", total);
        sale_order.put("name", name);
        sale_order.put("date_order", date_order);
        sale_order.put("partner_id", partner_id);
        sale_order.put("invoice_status", invoice_status);
        sale_order.put("pricelist_id",pricelist);
        sale_order.put("warehouse_id", warehouse);
        sale_order.put("state","draft");
        long insertSO = database.insert("sale_order", null, sale_order);
        return insertSO;
    }

    //fungsi digunakan untuk create di table Sale Order Line
    public void createSaleOrderLine(long id, ArrayList<SaleOrderLine> saleitems, String state){
        database = db.getWritableDatabase();
        for (int i=0; i< saleitems.size(); i++){
            String name = saleitems.get(i).getName();
            String quantity = saleitems.get(i).getQuantity();
            String unit_price = saleitems.get(i).getUnit_price();
            String total = saleitems.get(i).getSubtotal();
            String uom = saleitems.get(i).getUoM();
            String tax = saleitems.get(i).getTaxes();

            ContentValues sale_order = new ContentValues();
            sale_order.put("price_unit", unit_price);
            sale_order.put("product_uom_qty", quantity);
            sale_order.put("price_subtotal", total);
            sale_order.put("order_id", id);
            sale_order.put("product_id", 1);
            sale_order.put("price_total", total);
            sale_order.put("product_uom", uom);
            sale_order.put("price_tax", tax);
            sale_order.put("name", name);
            sale_order.put("state",state);
            long insertSO = database.insert("sale_order_line", null, sale_order);
        }
    }
    //fungsi digunakan untuk create di table Sale Order Line
    public void createSaleOrderLineRecording(long id, SaleOrderLine saleitems, String state){
        database = db.getWritableDatabase();

        String name = saleitems.getName();
        String quantity = saleitems.getQuantity();
        String unit_price = saleitems.getUnit_price();
        String total = saleitems.getSubtotal();
        String uom = saleitems.getUoM();
        String tax = saleitems.getTaxes();

        ContentValues sale_order = new ContentValues();
        sale_order.put("price_unit", unit_price);
        sale_order.put("product_uom_qty", quantity);
        sale_order.put("price_subtotal", total);
        sale_order.put("order_id", id);
        sale_order.put("product_id", 1);
        sale_order.put("price_total", total);
        sale_order.put("product_uom", uom);
        sale_order.put("price_tax", tax);
        sale_order.put("name", name);
        sale_order.put("state",state);
        long insertSO = database.insert("sale_order_line", null, sale_order);

    }
    /*
    public String getPickingTypeName(String id){
        SQLiteDatabase database = db.getReadableDatabase();
        String selectQuery= "SELECT origin, stock_picking.partner_id, state, backorder_id, min_date, stock_picking.write_date, \n" +
                "location_dest_id, stock_picking.name, stock_warehouse.name, stock_picking.location_id, picking_type_id \n" +
                "FROM stock_picking INNER JOIN stock_warehouse on stock_picking.location_id = stock_warehouse.id \n" +
                "INNER JOIN stock_location on stock_picking.location_dest_id = stock_location.id WHERE stock_picking.name = '"+name+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){

        }
    }*/
}
