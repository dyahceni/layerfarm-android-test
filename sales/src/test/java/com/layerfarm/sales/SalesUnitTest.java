package com.layerfarm.sales;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.ParameterCreateSales;
import com.layerfarm.layerfarm.model.ParameterDeleteDataSetting;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.Sales;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import okhttp3.JavaNetCookieJar;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.matchers.text.ValuePrinter.print;

public class SalesUnitTest {
    //    private Handler mHandler = mock(Handler.class);
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String token2;
    private String baseUrl = "http://"+"nmcgyj"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    String nodes = "";
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void ConnectServer(){
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            print("tokenn1 = "+tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                print(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            if (connection != null) {
                                user = connection.getUser();
//                                Log.d("Layerfarm", "uid = " + user.getUid());
                                System.out.println("uid = "+user.getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            print("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                print("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            print("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public void Test(){
        boolean create = Create_Sales();
        boolean edit = false, delete = false;
        if (create){
            edit = Edit_Sales();
        }
        if (edit){
            delete = Delete_Sales();
        }
        System.out.println("Create Sales: "+create);
        System.out.println("Edit  Sales: "+edit);
        System.out.println("Delete  Sales: "+delete);
        if (!create){
            assertFalse(true);
        }
        else if (!edit){
            assertFalse(true);
        }
        else if (!delete){
            assertFalse(true);
        }
        else {
            assertTrue(true);
        }
    }

    public Boolean Create_Sales(){
        String module = "selling_price";
        String function_name = "selling_price_create_sale";
        ArrayList<String> farm = new ArrayList<>();
        farm.add("1869");


        Sales args = new Sales();
        args.setDate("2020-09-09");
        args.setBuyer("1875");
        args.setProduct("egg");
        args.setQuality("normal");
        args.setPrice("15000");
        args.setQuantity("2");
        args.setFarm(farm);
        ParameterCreateSales parameter = new ParameterCreateSales(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createSales(token2, parameter);

        try {
            String[] create = call.execute().body();
            nodes = create[0];
            print("recording = "+create[0]);
            return  true;
        }catch (Exception e){
            print("error = "+e);
            return false;
        }
//        call.enqueue(new Callback<String[]>() {
//            @Override
//            public void onResponse(Call<String[]> call, Response<String[]> response) {
//                String[] recording = response.body();
//                print("recording = "+recording);
//            }
//
//            @Override
//            public void onFailure(Call<String[]> call, Throwable t) {
//                String alert = t.getMessage();
//                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
////                finish();
//            }
//        });
    }

    public Boolean Edit_Sales(){
        String module = "selling_price";
        String function_name = "selling_price_create_sale";
        ArrayList<String> farm = new ArrayList<>();
        farm.add("1869");


        Sales args = new Sales();
        args.setDate("2020-09-09");
        args.setBuyer("1875");
        args.setProduct("egg");
        args.setQuality("normal");
        args.setPrice("15000");
        args.setQuantity("5");
        args.setFarm(farm);
        ParameterCreateSales parameter = new ParameterCreateSales(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createSales(token2, parameter);

        try {
            String[] create = call.execute().body();
            nodes = create[0];
            print("recording = "+create[0]);
            return  true;
        }catch (Exception e){
            print("error = "+e);
            return false;
        }
    }


    public Boolean Delete_Sales(){
        String module = "selling_price";
        String function_name = "selling_price_delete_sale";
        String[] args = {nodes};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.deleteSale(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        try {
            String[] delete = call.execute().body();
            System.out.println("delete "+delete);
            return true;
        }catch (Exception e){
            System.out.println("error "+e);
            return false;
        }
//        call.enqueue(new Callback<String[]>() {
//            @Override
//            public void onResponse(Call<String[]> call, Response<String[]> response) {
//                String[] recording = response.body();
//            }
//
//            @Override
//            public void onFailure(Call<String[]> call, Throwable t) {
//                String alert = t.getMessage();
//                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
////                finish();
//            }
//        });
    }

}
