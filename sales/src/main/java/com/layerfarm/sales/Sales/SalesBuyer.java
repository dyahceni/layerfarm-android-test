package com.layerfarm.sales.Sales;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.sales.Helper.SalesHelper;
import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.layerfarm.sales.Sales.SalesCreate;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SalesBuyer extends Activity implements AdapterView.OnItemClickListener{
    private static SalesBuyer staticInstance;
    EditText search;
    ListView buyer_list;


    // Listview Adapter
    ArrayAdapter<String> adapter;
    private static String buyer_items;
    List<SyncBuyer> buyerSyncList = new ArrayList<>();
    ArrayList<String> buyer = new ArrayList<>();
    HashMap<String, String> hashMapbuyer = new HashMap<>();

    public static SalesBuyer getInstance(){
        if (staticInstance == null){
            staticInstance = new SalesBuyer();
        }
        return staticInstance;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String s = buyer_list.getItemAtPosition(position).toString();
        setBuyer(s);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_buyer);
        SalesHelper salesHelper = new SalesHelper(this);
        salesHelper.open();
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText("Buyer");
        search = (EditText) findViewById(R.id.search);
        buyer_list = (ListView) findViewById(R.id.list_buyer);
        HashMap<String, String> hashMapbuyer = SalesModel.getInstance().getList_buyer();
        Log.d("buyer", "buyer3 = "+hashMapbuyer.values());
        String[] buyer =  hashMapbuyer.values().toArray(new String[0]);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, buyer);
        buyer_list.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                SalesBuyer.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        buyer_list.setOnItemClickListener(this);
    }
    public void setBuyer(String buyer_items){
        this.buyer_items = buyer_items;
    }
    public String getBuyer(){
        return buyer_items;
    }
    public void close(View view){
        finish();
    }
}
