package com.layerfarm.sales.Sales;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.layerfarm.sales.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SalesProductBottomClass extends BottomSheetDialogFragment implements AdapterView.OnItemClickListener  {
    private BottomSheetListener mListener;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list = new ArrayList<>();
    String[] product_title;
    String[] product_key;
    HashMap<String, String> product;

    public void setOnBottomSheetClickListener(BottomSheetListener l) {
        mListener = l;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        product = new HashMap<>();
        product.put(product_key[position],product_title[position]);
        mListener.onButtonClicked(product_title[position]);
        dismiss();
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_sales_product_bottom, container, false);
        listView = (ListView) v.findViewById(R.id.product);
        product_title = getResources().getStringArray(R.array.sales_product);
        product_key = getResources().getStringArray(R.array.sales_product_key);

        arrayAdapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,product_title);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        return v;
    }
}
