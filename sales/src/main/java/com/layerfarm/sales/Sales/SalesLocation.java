package com.layerfarm.sales.Sales;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.layerfarm.sales.Helper.SalesHelper;
import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.R;

import java.util.HashMap;

public class SalesLocation extends Activity implements AdapterView.OnItemClickListener{
    private static SalesLocation staticInstance;
    EditText search;
    ListView location_list;


    // Listview Adapter
    ArrayAdapter<String> adapter;
    private static String location_items;

    public static SalesLocation getInstance(){
        if (staticInstance == null){
            staticInstance = new SalesLocation();
        }
        return staticInstance;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String s = location_list.getItemAtPosition(position).toString();
        setLocation(s);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_location);
        SalesHelper salesHelper = new SalesHelper(this);
        salesHelper.open();

        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText("Farm");

        search = (EditText) findViewById(R.id.search);
        location_list = (ListView) findViewById(R.id.list_location);
        HashMap<String, String> hashMaplocation = SalesModel.getInstance().getList_location();
        String[] location =  hashMaplocation.values().toArray(new String[0]);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, location);
        location_list.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                SalesLocation.this.adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        location_list.setOnItemClickListener(this);
    }
    public void setLocation(String buyer_items){
        this.location_items = buyer_items;
    }
    public String getLocation(){
        return location_items;
    }
    public void close(View view){
        finish();
    }
}
