package com.layerfarm.sales.Sales;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.R;

import java.util.ArrayList;

public class SalesQualityBottomClass extends BottomSheetDialogFragment implements AdapterView.OnItemClickListener {
    private BottomSheetListener mListener;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list = new ArrayList<>();
    private String[] quality;

    public void setOnBottomSheetClickListener(BottomSheetListener l) {
        mListener = l;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onButtonClicked(quality[position]);
        dismiss();
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_sales_quality_bottom, container, false);
        listView = (ListView) v.findViewById(R.id.quality);
        quality = SalesModel.getInstance().getList_quality().values().toArray(new String[0]);
        arrayAdapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,quality);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        return v;
    }
}
