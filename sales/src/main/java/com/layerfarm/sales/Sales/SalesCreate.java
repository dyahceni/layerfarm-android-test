package com.layerfarm.sales.Sales;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
//import android.widget.DatePicker;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterCreateSales;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.Sales;
import com.layerfarm.layerfarm.model.SyncBuyer;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.sales.DateWheel.DatePickerPopWin;
import com.layerfarm.sales.Helper.SalesHelper;
import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.R;
import com.layerfarm.sales.SalesActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.layerfarm.sales.DateWheel.DatePickerPopWin;

public class SalesCreate extends AppCompatActivity {
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    EditText selling_date, buyer, product, quality, cost, quantity, farm;
    Button btn_confirm;
    String nid = "";
    HashMap<String, String> productMap;
    HashMap<String, String> qualityMap;
    HashMap<String, String> buyerMap;
    HashMap<String, String> farmMap;
    List<SyncBuyer> buyerSyncList = new ArrayList<>();
    LinearLayout quality_linear_layout;
    LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_create);
        setupUI(findViewById(R.id.parent));

//        SalesHelper salesHelper = new SalesHelper(this);
//        salesHelper.open();

        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText("Create Sales");
        ll = findViewById(R.id.linear_create_user);
        Button btn_delete = (Button) findViewById(R.id.btn_delete);

        selling_date = (EditText)findViewById(R.id.selling_date);
        buyer = (EditText)findViewById(R.id.buyer);
        product = (EditText)findViewById(R.id.product);
        quality = (EditText) findViewById(R.id.quality);
        cost = (EditText) findViewById(R.id.cost);
        quantity = (EditText) findViewById(R.id.quantity);
//        farm = (EditText) findViewById(R.id.farm);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        quality_linear_layout = (LinearLayout) findViewById(R.id.quality_layout);
        Intent i = getIntent();
        Sales sales = (Sales) i.getSerializableExtra("sales");

        selling_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Calendar cal = Calendar.getInstance();
//                int year = cal.get(Calendar.YEAR);
//                int month = cal.get(Calendar.MONTH);
//                int day = cal.get(Calendar.DAY_OF_MONTH);
//
//                DatePickerDialog dialog = new DatePickerDialog(
//                        SalesCreate.this,
//                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
//                        mDateSetListener,
//                        year,month,day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
//                dialog.show();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(SalesCreate.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
//                        month = month + 1;
                        selling_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(SalesCreate.this);
            }
        });
//        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                month = month + 1;
//                String date = year + "-" + month + "-" + dayOfMonth;
//                selling_date.setText(date);
//            }
//        };
        buyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalesCreate.this, SalesBuyer.class);
                startActivity(intent);
            }
        });
        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SalesProductBottomClass bottom_product = new SalesProductBottomClass();
                bottom_product.show(getSupportFragmentManager(),"Product");

                bottom_product.setOnBottomSheetClickListener(new SalesProductBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        product.setText(text);
                        bottom_product.dismiss();
                    }
                });
            }
        });
        product.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("Egg")){
                    quality_linear_layout.setVisibility(View.VISIBLE);
                }
                else
                    quality_linear_layout.setVisibility(View.GONE);
            }
        });
        quality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SalesQualityBottomClass bottom_quality = new SalesQualityBottomClass();
                bottom_quality.show(getSupportFragmentManager(),"Quality");

                bottom_quality.setOnBottomSheetClickListener(new SalesQualityBottomClass.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(String text) {
                        quality.setText(text);
                        bottom_quality.dismiss();
                    }
                });
            }
        });
//        farm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(SalesCreate.this, SalesLocation.class);
//                startActivity(intent);
//            }
//        });

        //hasmap untuk product
        String[] product_title = getResources().getStringArray(R.array.sales_product);
        String[] product_key = getResources().getStringArray(R.array.sales_product_key);
        productMap = new HashMap<String, String>();
        for (int b =0; b< product_key.length; b++){
            productMap.put(product_key[b], product_title[b]);
        }
        qualityMap = SalesModel.getInstance().getList_quality();
        buyerMap = SalesModel.getInstance().getList_buyer();
        farmMap = SalesModel.getInstance().getList_location();

        if (farmMap != null && farmMap.size() >0){
//            Log.d("zzz","location isi");
            Set set2 = farmMap.entrySet();
            Iterator iterator2 = set2.iterator();
            int fildno =0;
            while(iterator2.hasNext()) {
                Map.Entry me2 = (Map.Entry)iterator2.next();
                CheckBox ch = new CheckBox(this);
                ch.setText(me2.getValue().toString());
//                System.out.print(me2.getKey() + ": ");
//                System.out.println(me2.getValue());
                ll.addView(ch, ll.getChildCount()-1);
                ch.setId(fildno);
                ch.setTag((me2.getKey()));
//                Log.d("zzz","check box = "+ch.getId());
//                Log.d("zzz","check tag = "+ch.getTag());
                fildno++;
            }

        }


        if (!sales.getNid().isEmpty() && sales.getNid() != null){
            Log.d("sales","nid = "+sales.getNid());
            btn_delete.setVisibility(View.VISIBLE);
            nid = sales.getNid();
        }

        if (!sales.getDate().isEmpty() && sales.getDate() != null){
            selling_date.setText(sales.getDate());
        }
        if (!sales.getBuyer().isEmpty() && sales.getBuyer() != null){
            String buyer_name = buyerMap.get(sales.getBuyer());
            if(buyer_name != null)
                SalesBuyer.getInstance().setBuyer(buyer_name);
            else
                SalesBuyer.getInstance().setBuyer("");
//            buyer.setText(sales.getBuyer());
        }
        try {
            if (!sales.getProduct().isEmpty() && sales.getProduct() != null){
                String product_name = productMap.get(sales.getProduct());
                if(product_name != null)
                    product.setText(product_name);
                else
                    product.setText("");
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Error, can't find product ",Toast.LENGTH_LONG).show();
        }


        try {
            if (!sales.getQuality().isEmpty() && sales.getQuality() != null){
                String quality_name = qualityMap.get(sales.getQuality());
                if(quality_name != null)
                    quality.setText(quality_name);
                else
                    quality.setText("");
            }
        }catch (Exception e ){
            Toast.makeText(getApplicationContext(),"Error Quality can't find",Toast.LENGTH_LONG).show();
        }

        if (sales.getPrice() != null){
            cost.setText(sales.getPrice());
        }

        if (sales.getQuantity()!= null){
            quantity.setText(sales.getQuantity());
        }
        if (sales.getFarm() != null){
//            String farm_name = farmMap.get(sales.getFarm());
//            if (farm_name != null){
//                SalesLocation.getInstance().setLocation(farm_name);
//            }
//            else
//                SalesLocation.getInstance().setLocation("");

            for(int c = 0; c< sales.getFarm().size(); c++){
//                    Log.d("zzz","update target id = "+users.getLocation().get(c));
                for(int b=0; b<ll.getChildCount(); b++) {

                    View nextChild = ll.getChildAt(b);

                    if(nextChild instanceof CheckBox)
                    {

                        CheckBox check = (CheckBox) nextChild;
//                            Log.d("zzz","update check box tag= "+check.getTag());
//                            Log.d("zzz","update check box ID= "+check.getId());
                        if (check.getTag() != null) {
                            if ((check.getTag()).equals((sales.getFarm().get(c)))) {
                                check.setChecked(true);
//                                    Log.d("zzz", "samaa " + check.getTag() + " " + users.getLocation().get(c));
                            }
                        }
//                        if (check.isChecked()) {
//                            AllCheckbox.add(check.getText().toString());
//                        }
                    }

                }
            }
        }

    }
    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(SalesCreate.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        buyer.setText(SalesBuyer.getInstance().getBuyer());
//        farm.setText(SalesLocation.getInstance().getLocation());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SalesBuyer.getInstance().setBuyer("");
        SalesLocation.getInstance().setLocation("");
    }
    public void Sync(){
        ProgressDialog progressDoalog = new ProgressDialog(this);
//        progressDoalog.setMax(100);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

    }

    public void Confirm(View v){
        String date = selling_date.getText().toString();
        String data_buyer = getKey(buyerMap, buyer.getText().toString());
        String data_product = getKey(productMap, product.getText().toString());
        String data_quality ="";
        if(!quality.getText().toString().isEmpty()){
            data_quality = getKey(qualityMap, quality.getText().toString());
        }
        else
            data_quality = "";
        Log.d("zzz","data quality ="+data_quality);
        if(data_quality == null){
            data_quality = "";
        }
        String data_cost = cost.getText().toString();
        String data_quantity = quantity.getText().toString();
        ArrayList<String> location_check = new ArrayList<>();
        for(int b=0; b<ll.getChildCount(); b++) {

            View nextChild = ll.getChildAt(b);

            if(nextChild instanceof CheckBox)
            {
                CheckBox check = (CheckBox) nextChild;
                if (check.isChecked()){
                    location_check.add(check.getTag().toString());
                }

            }

        }
//        String data_farm = getKey(farmMap, farm.getText().toString());

        Log.d("zzz"," date: "+date+" buyer: "+data_buyer+" product: "+data_product+" quality: "+data_quality+
                " cost:"+data_cost+" quantity: "+data_quantity);

        String module = "selling_price";
        String function_name = "selling_price_create_sale";


        Sales args = new Sales();
        args.setDate(date);
        args.setBuyer(data_buyer);
        args.setProduct(data_product);
        args.setQuality(data_quality);
        args.setPrice(data_cost);
        args.setQuantity(data_quantity);
        args.setFarm(location_check);
        ParameterCreateSales parameter = new ParameterCreateSales(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.createSales(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] recording = response.body();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                finish();
            }
        });

    }
    public void Delete(View v){
        String module = "selling_price";
        String function_name = "selling_price_delete_sale";
        String[] args = {nid};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.deleteSale(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] recording = response.body();
                finish();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                finish();
            }
        });
    }
    public void close(View view){
        finish();
    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
