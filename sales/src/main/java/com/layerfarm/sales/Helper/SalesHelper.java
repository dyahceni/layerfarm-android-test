package com.layerfarm.sales.Helper;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.sales.R;

import java.util.HashMap;

public class SalesHelper {
    DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    public SalesHelper(Context context){

        dbHelper = new DatabaseHelper(context);
    }
    //membuat sambungan ke database
    public void open() throws SQLException {
        database= dbHelper.getWritableDatabase();

    }

    //menutup sambungan ke database
    public void close(){

        dbHelper.close();
    }

    public HashMap<String, String> getBuyer(){
        HashMap<String, String> buyer = new HashMap<>();
        SQLiteDatabase dbase = dbHelper.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM buyer",null);

        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            buyer.put(cursor.getString(cursor.getColumnIndex("rid")),cursor.getString(cursor.getColumnIndex("name")));
        }
        return buyer;
    }
    public HashMap<String, String> getQuality(){
        HashMap<String, String> quality = new HashMap<>();
        SQLiteDatabase dbase = dbHelper.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM egg_quality",null);

        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            quality.put(cursor.getString(cursor.getColumnIndex("machine_name")),cursor.getString(cursor.getColumnIndex("name")));
        }
        return quality;
    }
    public HashMap<String, String> getLocation(){
        HashMap<String, String> location = new HashMap<>();
        SQLiteDatabase dbase = dbHelper.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM location",null);

        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            location.put(cursor.getString(cursor.getColumnIndex("rid")),cursor.getString(cursor.getColumnIndex("name")));
        }
        return location;
    }



}
