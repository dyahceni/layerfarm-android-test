package com.layerfarm.sales.Model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class SalesModel {
    private static SalesModel staticInstance;

    public static SalesModel getInstance(){
        if (staticInstance==null){
            staticInstance = new SalesModel();
        }

        return staticInstance;
    }
    @SerializedName("list_buyer")
    HashMap<String, String> list_buyer = new HashMap<>();

    @SerializedName("list_location")
    HashMap<String, String> list_location = new HashMap<>();

    @SerializedName("list_quality")
    HashMap<String, String> list_quality = new HashMap<>();

    public HashMap<String, String> getList_buyer() {
        return list_buyer;
    }

    public void setList_buyer(HashMap<String, String> list_buyer) {
        this.list_buyer = list_buyer;
    }

    public HashMap<String, String> getList_location() {
        return list_location;
    }

    public void setList_location(HashMap<String, String> list_location) {
        this.list_location = list_location;
    }

    public HashMap<String, String> getList_quality() {
        return list_quality;
    }

    public void setList_quality(HashMap<String, String> list_quality) {
        this.list_quality = list_quality;
    }
}
