package com.layerfarm.sales.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.layerfarm.layerfarm.model.Sales;
import com.layerfarm.sales.Model.SalesModel;
import com.layerfarm.sales.R;
import com.layerfarm.sales.Sales.SalesCreate;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.ViewHolder> {
    ArrayList<Sales> data;
    public SalesAdapter(ArrayList<Sales> items){
        data = items;
        Log.d("zzz","Sales adapter");
//        status = status;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tx_date;
        TextView tx_buyer;
        TextView tx_product;
        TextView tx_quality;
        TextView tx_quantity;
        TextView tx_price;
        CardView cv_sales;
        public ViewHolder(View itemView) {
            super(itemView);
            tx_date = (TextView) itemView.findViewById(R.id.date);
            tx_buyer = (TextView) itemView.findViewById(R.id.buyer);
            tx_product = (TextView) itemView.findViewById(R.id.product);
            tx_quality = (TextView) itemView.findViewById(R.id.quality);
            tx_quantity = (TextView) itemView.findViewById(R.id.quantity);
            tx_price = (TextView) itemView.findViewById(R.id.price);
            cv_sales = (CardView) itemView.findViewById(R.id.cv_sales);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_sales_rv_sales, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String date = data.get(position).getDate();
        String buyer = data.get(position).getBuyerName();
        String product = data.get(position).getProduct();

        String quality = SalesModel.getInstance().getList_quality().get(data.get(position).getQuality());
        String quantity = data.get(position).getQuantity();
        String price = data.get(position).getPrice();


        holder.tx_date.setText(date);
        holder.tx_buyer.setText(buyer);
        holder.tx_product.setText(product);
        holder.tx_quality.setText(quality);
        holder.tx_quantity.setText(quantity);
        holder.tx_price.setText(price);
        holder.cv_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Sales sale = data.get(position);
                Context context = view.getContext();
                Intent i = new Intent(context, SalesCreate.class);
                i.putExtra("sales", (Serializable) sale);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

