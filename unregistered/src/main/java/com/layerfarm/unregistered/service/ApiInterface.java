package com.layerfarm.unregistered.service;


import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.unregistered.model.Login;
import com.layerfarm.unregistered.model.ParameterRegistrationCreateLayerfarmUser;
import com.layerfarm.unregistered.model.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by agungsuyono on 16/12/17.
 *
 * API Webservice Interface
 */

public interface ApiInterface {

    @GET("services/session/token")
    Call<String> getToken1();
//    Observable<Response<String>> getToken1();

    @Headers("Content-Type: application/json")
    @POST("common/system/connect.json")
    Call<Connect> getConnect(@Header("X-CSRF-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("common/user/login.json")
    Call<Connect> login(@Header("X-CSRF-Token") String token, @Body Login login);

    @GET("services/session/token")
    Call<String> getToken2(@HeaderMap Map<String, String> headers);

//    @GET("services/session/token")
//    Call<String> getToken2c(@Header("Cookie") String sessionIdAndToken);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<Node> layerfarm_registration_create_layerfarm_user(@Header("X-CSRF-Token") String token, @Body ParameterRegistrationCreateLayerfarmUser parameter);

    @Headers("Content-Type: application/json")
    @POST("retrofit/retrofit/callapi.json")
    Call<String> create_layerfarm_user(@Header("X-CSRF-Token") String token, @Body ParameterRegistrationCreateLayerfarmUser parameter);

}

