package com.layerfarm.unregistered.model;

public class SpentHen {
    String chick_in;
    String totalBird;
    String totalWeight;
    public SpentHen(){

    }

    public String getChick_in() {
        return chick_in;
    }
    public void setChick_in(String chick_in) {
        this.chick_in = chick_in;
    }
    public String getTotalBird() {
        return totalBird;
    }

    public void setTotalBird(String totalBird) {
        this.totalBird = totalBird;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }
}
