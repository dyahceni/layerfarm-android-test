package com.layerfarm.unregistered.model;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.unregistered.Pinger;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.service.ApiInterface;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.unregistered.R;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationUserMainActivity extends AppCompatActivity {

    EditText eDFullname, eDEmail, eDFarmName, eDFarmAddress, eDPhone;
    CheckBox chxAggree;
    public String url,username,password;
    Spinner mSpinner;
    ArrayAdapter<String> idAdapter;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);


    //Get server information

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SQLite = new DataHelper(getApplicationContext());

        setContentView(R.layout.activity_activation_user_main);
        overridePendingTransition(R.anim.bottom_to_up, R.anim.up_to_bottom);
        mSpinner = (Spinner)findViewById(R.id.availableID);
        eDFullname = (EditText) findViewById(R.id.input_full_name);
        eDEmail= (EditText) findViewById(R.id.input_email);
        eDFarmName= (EditText) findViewById(R.id.input_farm_name);
        eDFarmAddress= (EditText) findViewById(R.id.input_farm_address);
        eDPhone= (EditText) findViewById(R.id.input_phone);
        chxAggree = (CheckBox) findViewById(R.id.cb_click_activation) ;
        //Get server information from local database
        getServerInformation();

        ArrayList<String> timezones = new ArrayList<>(Arrays.asList(TimeZone.getAvailableIDs()));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, timezones);
        mSpinner.setAdapter(adapter);

        TimeZone tz=TimeZone.getDefault();
        mSpinner.setSelection(getIndex(mSpinner,tz.getID()));
        Toast.makeText(ActivationUserMainActivity.this, "Time Zone : " + tz.getDisplayName(false,TimeZone.SHORT) + " \n TimeZone Id: "+ tz.getID(), Toast.LENGTH_SHORT).show();



       /* String[] idArray = TimeZone.getAvailableIDs();
        idAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, idArray);
        idAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(idAdapter);*/


    }

    private void getServerInformation(){
        //Load Server Info
        SQLite = new DataHelper(getApplicationContext());
        //get url information
        SQLiteDatabase dbSQL = db.getReadableDatabase();
        String selectQuery = "SELECT value FROM variables";
        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
        ArrayList< String > values = new ArrayList < >();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String value = cursor.getString(cursor.getColumnIndex("value"));
                values.add(value);
            } while ( cursor . moveToNext ());
        }
        cursor.close();
        if (values != null && !values.isEmpty()) {
            url = values.get(0);
        }
    }

    public void activate_user(View view) {
        boolean valid=true;
        if (eDFullname.getText().toString().isEmpty() || eDEmail.getText().toString().isEmpty()
                || eDFarmName.getText().toString().isEmpty() || eDFarmAddress.getText().toString().isEmpty()
                || eDPhone.getText().toString().isEmpty() || chxAggree.isChecked()== false) {
            AskOption();

        }
        else {
            //create_layerfarm_general_expense();
            //saveDataGeneralExpense();
            //LayerFarm.getInstance().Connect("http://" + url + "/", username, password);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Activate...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want activate account?");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.ic_plus);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                    new AsyncTask<String,
                            Void,
                            Boolean>() {
                        @Override
                        protected Boolean doInBackground(String... destinations) {
                            try {
                                Pinger pinger = new Pinger();
                                for (String destination : destinations)
                                    if (pinger.ping(destination, 60)) return true;
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            //Check Internet Connection
                            if (result == true) {
                                create_layerfarm_registration_create_layerfarm_user();
                               // create_layerfarm_user();

                            } else {
                                //Internet Failure
                                Toast.makeText(ActivationUserMainActivity.this, "Internet Connection Error", Toast.LENGTH_SHORT).show();
                                //addChickinDistributionAge();
                                finish();
                            }

                        }
                    }.execute(url);

                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                    finish();
                }
            });

            // Showing Alert Message
            alertDialog.show();
            //ChickInMainActivity refresh= new ChickInMainActivity();
            //refresh.Submit();

        }
    }

    public void create_layerfarm_registration_create_layerfarm_user() {

        EditText a = (EditText)(findViewById(R.id.input_full_name));
        final String full_name = (String)(a.getText().toString());

        EditText b = (EditText)(findViewById(R.id.input_email));
        final String email = (String)(b.getText().toString());

        EditText c = (EditText)(findViewById(R.id.input_farm_name));
        final String farm_name = (String)(c.getText().toString());

        EditText d = (EditText)(findViewById(R.id.input_farm_address));
        final String farm_address = (String)(d.getText().toString());

        EditText e = (EditText)(findViewById(R.id.input_phone));
        final String phone = (String)(e.getText().toString());

        Spinner f = (Spinner)(findViewById(R.id.availableID));
        final String timezone = (f.getSelectedItem().toString()).trim();
        //Generate uniqe code
        final String key = generateUniqeCode();

        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_create_user";

        ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments(full_name, email,farm_name,key,"",farm_address,phone, timezone,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

        Call<Node> call = apiInterfaceJson.layerfarm_registration_create_layerfarm_user(token2, parameter);

        //Progress
        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(ActivationUserMainActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Activation user on progress ....");
        progressDoalog.setTitle("Activation");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();

        call.enqueue(new Callback<Node>() {
            @Override
            public void onResponse(Call<Node> call, Response<Node> response) {
                //String[] get_expense_general_respons = response.body();
                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                Node node = response.body();
                Log.d("Layerfarm", "node = " + node);

                if (response.isSuccessful() && node != null) {
                    /*SQLite = new DataHelper(getApplication());
                    SQLiteDatabase dbase = db.getWritableDatabase();
                    String insertSQL = "INSERT INTO user_farm \n" + "(email, full_name, farm_code, farm_name, farm_adddress, phone_number, timezone, role, is_owner, currently_active)VALUES \n" + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    dbase.execSQL(insertSQL, new String[]{email, full_name, key, farm_name, farm_address, phone, "", "0", "0", "0"});
*/
                    SubmitDialog();
                    progressDoalog.dismiss();
                    Log.v("this", "Yes!");
                }
            }

            @Override
            public void onFailure(Call<Node> call, Throwable t) {
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();
                SubmitDialog();
                progressDoalog.dismiss();
                Log.v("this", "Yes!");
            }
        });
                    SQLite = new DataHelper(getApplication());
                    SQLiteDatabase dbase = db.getWritableDatabase();
                    String insertSQL = "INSERT INTO user_farm \n" + "(email, full_name, farm_code, farm_name, farm_adddress, phone_number, timezone, role, is_owner, currently_active)VALUES \n" + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    dbase.execSQL(insertSQL, new String[]{email, full_name, key, farm_name, farm_address, phone, "", "0", "0", "0"});

    }

    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Activate Success")
                .setMessage("Your account was activated")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivationUserMainActivity.this.finish();
                    }
                })
                .show();

        //ExpenseMainActivity.ma.finish();

        return myQuittingDialogBox;

    }

    private AlertDialog WarningDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Upload Error")
                .setMessage("General Expense Upload Error, Please Try Again")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivationUserMainActivity.this.finish();
                    }
                })
                .show();

        return myQuittingDialogBox;

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please fill all data")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return myQuittingDialogBox;
    }

    private static String generateUniqeCode() {
        int length = 6;
        String DATA_FOR_RANDOM_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        SecureRandom random = new SecureRandom();
        if (length < 1) throw new IllegalArgumentException();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            // debug
            sb.append(rndChar);
        }




       /* String DATA_FOR_RANDOM_STRING_NUMBER = NUMBER;
        SecureRandom random_number = new SecureRandom();
        if (lengthNumber < 1) throw new IllegalArgumentException();
        StringBuilder sb_number = new StringBuilder(length);
        for (int i = 0; i < lengthNumber; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAtNumber = random_number.nextInt(DATA_FOR_RANDOM_STRING_NUMBER.length());
            char rndCharNumber = DATA_FOR_RANDOM_STRING_NUMBER.charAt(rndCharAtNumber);
            // debug
            sb_number.append(rndCharNumber);
        }
        String key = sb.toString() + sb_number.toString();*/
        //Log.d("message","key : "+sb.toString());
        return sb.toString();
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }

    public void create_layerfarm_user() {

        EditText a = (EditText)(findViewById(R.id.input_full_name));
        final String full_name = (String)(a.getText().toString());

        EditText b = (EditText)(findViewById(R.id.input_email));
        final String email = (String)(b.getText().toString());

        EditText c = (EditText)(findViewById(R.id.input_farm_name));
        final String farm_name = (String)(c.getText().toString());

        EditText d = (EditText)(findViewById(R.id.input_farm_address));
        final String farm_address = (String)(d.getText().toString());

        EditText e = (EditText)(findViewById(R.id.input_phone));
        final String phone = (String)(e.getText().toString());

        Spinner f = (Spinner)(findViewById(R.id.availableID));
        final String timezone = (f.getSelectedItem().toString()).trim();
        //Generate uniqe code
        final String key = generateUniqeCode();

        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_create_user";

        ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments(full_name, email,farm_name,key,"",farm_address,phone, timezone,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

        Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

        //Progress
        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(ActivationUserMainActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Activation user on progress ....");
        progressDoalog.setTitle("Activation");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //String[] get_expense_general_respons = response.body();
                //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                String node = response.body();
                Log.d("Layerfarm", "node = " + node);

                if (response.isSuccessful() && node != null) {
                    /*SQLite = new DataHelper(getApplication());
                    SQLiteDatabase dbase = db.getWritableDatabase();
                    String insertSQL = "INSERT INTO user_farm \n" + "(email, full_name, farm_code, farm_name, farm_adddress, phone_number, timezone, role, is_owner, currently_active)VALUES \n" + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                    dbase.execSQL(insertSQL, new String[]{email, full_name, key, farm_name, farm_address, phone, "", "0", "0", "0"});
*/
                    SubmitDialog();
                    progressDoalog.dismiss();
                    Log.v("this", "Yes!");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //String alert = t.getMessage();
                //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                WarningDialog();

            }
        });

    }
}
