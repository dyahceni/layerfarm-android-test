package com.layerfarm.unregistered.model.Mortality;

import java.util.ArrayList;

public class MortalityData {
    String hatch_date;
    ArrayList<Mortality> mortality;
    public MortalityData(){

    }

    public String getHatch_date() {
        return hatch_date;
    }

    public void setHatch_date(String hatch_date) {
        this.hatch_date = hatch_date;
    }

    public ArrayList<Mortality> getMortality() {
        return mortality;
    }

    public void setMortality(ArrayList<Mortality> mortality) {
        this.mortality = mortality;
    }
}
