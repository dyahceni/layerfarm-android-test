package com.layerfarm.unregistered.model;

import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.unregistered.service.ApiInterface;

public class RetrofitData {
    private static RetrofitData staticInstance;

    public static RetrofitData getInstance(){
        if (staticInstance==null){
            staticInstance = new RetrofitData();
        }

        return staticInstance;
    }
    public String email;
    public String password;
    public String token1;
    public String token2;
    public String baseUrl;
    public User user;
    public Connect connection;

    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;

    public ApiInterface getApiInterfaceToken() {
        return apiInterfaceToken;
    }

    public void setApiInterfaceToken(ApiInterface apiInterfaceToken) {
        this.apiInterfaceToken = apiInterfaceToken;
    }

    public ApiInterface getApiInterfaceJson() {
        return apiInterfaceJson;
    }

    public void setApiInterfaceJson(ApiInterface apiInterfaceJson) {
        this.apiInterfaceJson = apiInterfaceJson;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken1() {
        return token1;
    }

    public void setToken1(String token1) {
        this.token1 = token1;
    }

    public String getToken2() {
        return token2;
    }

    public void setToken2(String token2) {
        this.token2 = token2;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Connect getConnection() {
        return connection;
    }

    public void setConnection(Connect connection) {
        this.connection = connection;
    }


}
