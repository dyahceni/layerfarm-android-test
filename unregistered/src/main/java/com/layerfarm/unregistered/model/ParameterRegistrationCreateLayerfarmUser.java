package com.layerfarm.unregistered.model;
import com.google.gson.annotations.SerializedName;


public class ParameterRegistrationCreateLayerfarmUser {

    private String module;
    private String function_name;
    private Arguments args;

    public ParameterRegistrationCreateLayerfarmUser(String module, String function_name, Arguments args) {
        this.module = module;
        this.function_name = function_name;
        this.args = args;
    }

    public static class Arguments{

        @SerializedName("name")
        private String a;

        @SerializedName("email")
        private String b;

        @SerializedName("farmname")
        private String c;

        @SerializedName("farmnameshort")
        private String d;

        @SerializedName("population")
        private String e;

        @SerializedName("address")
        private String f;

        @SerializedName("phonenumber")
        private String g;

        @SerializedName("timezone")
        private String h;

        @SerializedName("expectation")
        private String i;


        public Arguments(String name, String email, String farmname, String farmnameshort, String population, String address, String phonenumber , String timezone, String expectation){
            this.a = name;
            this.b = email;
            this.c = farmname;
            this.d = farmnameshort;
            this.e = population;
            this.f = address;
            this.g = phonenumber;
            this.h = timezone;
            this.i = expectation;
        }
    }
}
