package com.layerfarm.unregistered.model;

public class Feed {
    String name;
    String value;
    public Feed(){

    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
