package com.layerfarm.unregistered.model;

public class BodyWeight {

    private String id,daily_recording_id,body_weight;


    public BodyWeight() {
    }



    public BodyWeight(String id, String daily_recording_id, String body_weight) {
        this.id = id;
        this.daily_recording_id = daily_recording_id;
        this.body_weight = body_weight;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDaily_recording_id() {
        return daily_recording_id;
    }

    public void setDaily_recording_id(String daily_recording_id) {
        this.daily_recording_id = daily_recording_id;
    }

    public String getBody_weight() {
        return body_weight;
    }

    public void setBody_weight(String body_weight) {
        this.body_weight = body_weight;
    }


}
