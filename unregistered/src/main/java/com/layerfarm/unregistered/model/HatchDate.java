package com.layerfarm.unregistered.model;

public class HatchDate {
    String hatchDate;
    String strain ;
    public HatchDate(){

    }

    public void setHatchDate(String hatchDate) {
        this.hatchDate = hatchDate;
    }

    public void setStrain(String strain) {
        this.strain = strain;
    }

    public String getHatchDate() {
        return hatchDate;
    }

    public String getStrain() {
        return strain;
    }
}
