package com.layerfarm.unregistered.model;

import com.google.gson.annotations.SerializedName;

public class Node {
    @SerializedName("nid")
    public String nid;

    public Node(String nid){
        this.nid = nid;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }
}
