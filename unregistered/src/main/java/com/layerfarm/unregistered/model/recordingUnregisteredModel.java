package com.layerfarm.unregistered.model;

import java.util.ArrayList;

public class recordingUnregisteredModel {

    String id;
    String recording_date;
    String age;
    ArrayList name_egg;
    ArrayList value_eggs;
    ArrayList value_weight;
    String name_mortality;
    String value_mortality;
    String total_bird;
    String flock_id;

    public recordingUnregisteredModel(){

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecording_date() {
        return recording_date;
    }

    public void setRecording_date(String recording_date) {
        this.recording_date = recording_date;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public ArrayList getName_egg() {
        return name_egg;
    }

    public void setName_egg(ArrayList name_egg) {
        this.name_egg = name_egg;
    }

    public ArrayList getValue_eggs() {
        return value_eggs;
    }

    public void setValue_eggs(ArrayList value_eggs) {
        this.value_eggs = value_eggs;
    }

    public ArrayList getValue_weight() {
        return value_weight;
    }

    public void setValue_weight(ArrayList value_weight) {
        this.value_weight = value_weight;
    }

    public String getName_mortality() {
        return name_mortality;
    }

    public void setName_mortality(String name_mortality) {
        this.name_mortality = name_mortality;
    }

    public String getValue_mortality() {
        return value_mortality;
    }

    public void setValue_mortality(String value_mortality) {
        this.value_mortality = value_mortality;
    }

    public String getTotal_bird() {
        return total_bird;
    }

    public void setTotal_bird(String total_bird) {
        this.total_bird = total_bird;
    }

    public String getFlock_id() {
        return flock_id;
    }

    public void setFlock_id(String flock_id) {
        this.flock_id = flock_id;
    }
}
