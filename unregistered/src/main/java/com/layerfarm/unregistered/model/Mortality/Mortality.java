package com.layerfarm.unregistered.model.Mortality;

public class Mortality {
    String name;
    String value;
    public Mortality(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
