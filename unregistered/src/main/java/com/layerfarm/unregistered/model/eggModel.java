package com.layerfarm.unregistered.model;

public class eggModel {

    String qualityEgg;
    String valueEgg;
    String valueWeight;

    public eggModel(){

    }

    public String getQualityEgg() {
        return qualityEgg;
    }

    public void setQualityEgg(String qualityEgg) {
        this.qualityEgg = qualityEgg;
    }

    public String getValueEgg() {
        return valueEgg;
    }

    public void setValueEgg(String valueEgg) {
        this.valueEgg = valueEgg;
    }

    public String getValueWeight() {
        return valueWeight;
    }

    public void setValueWeight(String valueWeight) {
        this.valueWeight = valueWeight;
    }
}
