package com.layerfarm.unregistered.model;

import java.util.ArrayList;

public class History {
    String date;
    String data;
    ArrayList<Flock> flock_data;

    public History(){}

    public ArrayList<Flock> getFlock_data() {
        return flock_data;
    }

    public void setFlock_data(ArrayList<Flock> flock_data) {
        this.flock_data = flock_data;
    }

    public String getData() {
        return data;
    }

    public String getDate() {
        return date;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
