package com.layerfarm.unregistered.adapterUnregistered;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.recording.adapter.FlockAdapter;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.recording.fragment.MainFragment;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.model.recordingUnregisteredModel;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.model.eggModel;
import com.layerfarm.unregistered.recordingUnregistered.AddUnRecordingMainActivity;
import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.recordingUnregistered.EditUnRecordingMainActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class UnFlockAdapter extends RecyclerView.Adapter<UnFlockAdapter.ViewHolder> {
    private ArrayList<recordingUnregisteredModel> flock_items;
    private String loc_id;
    private String loc_name;
    private String id_flock;
    private String name_flock;
    private String type;
    private String period;
    private String recordingDate, status;
    Context context;
    helperUnregistered help;
    private DBDataSourceUnregistered dataSource;
    long ID;
    AlertDialog.Builder dialog;
    private SQLiteDatabase database;
    DatabaseHelper db;
    long last_recording_id;
    int last_recording_id2;

    public UnFlockAdapter(ArrayList<recordingUnregisteredModel> inputData){
        flock_items = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mortality;
        public TextView textLastRecording;
        public TextView totalbird;
        public CardView card;
        public TextView valuehatchDate, egg,textView2, textView3 ;
        public ViewGroup leftLinear, rightLinear, parentLinear;
        public LinearLayout linearContent;
        public TextView lastRecordingId;


        public ViewHolder(View v) {
            super(v);
            linearContent = (LinearLayout) v.findViewById(R.id.id_layout);
            lastRecordingId = (TextView) v.findViewById(R.id.id_recording);
            textLastRecording = (TextView) v.findViewById(R.id.textLastRecording);
            mortality = (TextView) v.findViewById(R.id.valueMortality);
            totalbird = (TextView) v.findViewById(R.id.valueTotalBird);
            valuehatchDate = (TextView) v.findViewById(R.id.valueHatchDate);
            egg = (TextView) v.findViewById(R.id.lastRecording);
            textView2 = (TextView) v.findViewById(R.id.textView2);
            textView3 = (TextView) v.findViewById(R.id.textView3);
           // jenis = (TextView) v.findViewById(com.layerfarm.recording.R.id.jenis);
//            card = (CardView) v.findViewById(R.id.Prueba3);
           parentLinear = (ViewGroup) v.findViewById(R.id.parent_layout);


            db = new DatabaseHelper(v.getContext());
            database = db.getWritableDatabase();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.unregistered_recording_list_row, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);

        //db = new DatabaseHelper(context);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        final String recording_id = flock_items.get(position).getId();
        final String mortality = flock_items.get(position).getValue_mortality();
        final String total_bird = flock_items.get(position).getTotal_bird();
        final String lastRecording =  flock_items.get(position).getRecording_date();
        final String hatch_date =  flock_items.get(position).getAge();
        final String flockid =  flock_items.get(position).getFlock_id();

        //String age = help.getBirdAgeByHatchDate(hatch_date,lastRecording);

        long bedaHari=0;
        long hari =0;
        try {
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            Date tglAwal = (Date) date.parse(hatch_date);
            Date tglAkhir = (Date) date.parse(lastRecording);

            bedaHari = Math.abs(tglAkhir.getTime() - tglAwal.getTime());
            hari = TimeUnit.MILLISECONDS.toDays(bedaHari);
            //Log.d("zzzs","beda hari: "+hari);
        }
        catch (Exception e){}

        long minggu = hari/7;
        long sisa = hari%7;
        String hasil = " "+minggu+" week(s) "+sisa+" day(s)";

        Log.d("lastRecording","lastRecording:"+lastRecording);
        Log.d("mortality","flock:"+mortality);
        Log.d("total_bird","total_bird:"+total_bird);
        Log.d("flock_id","flock_id:"+flockid);

        //query telur
        ArrayList<eggModel> eggarray = new ArrayList<>();
        String Query= "SELECT * from daily_recording INNER JOIN recording_production on daily_recording.id = recording_production.daily_recording_id\n" +
                "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                "WHERE daily_recording.recording_date = '"+lastRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String name = "";
        String weight = "";
        String egg = "";
        cursor.moveToFirst();


        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            eggModel eggnum = new eggModel();
            name = cursor.getString(19);
            weight = cursor.getString(17);
            egg = cursor.getString(16);
            eggnum.setQualityEgg(name);
            eggnum.setValueWeight(weight);
            eggnum.setValueEgg(egg);
            eggarray.add(eggnum);
        }

        String value = "";
        String value1 = "";
        String value2 = "";
        for (int a = 0; a<eggarray.size(); a++){
            Log.d("nama","nama:"+eggarray.get(a).getQualityEgg());
            Log.d("jumlah","kg:"+eggarray.get(a).getValueWeight());
            Log.d("jumlah","telur:"+eggarray.get(a).getValueEgg());

           // value += eggarray.get(a).getQualityEgg()+"\n";
            value += eggarray.get(a).getQualityEgg()+"\n";
            value1 += eggarray.get(a).getValueWeight()+"\n";
            value2 += eggarray.get(a).getValueEgg()+"\n";
            //AddEggLayout(holder, eggarray.get(a).getQualityEgg(),eggarray.get(a).getValueWeight() ,eggarray.get(a).getQualityEgg());
        }

         /*  Log.d("flock_id","flock_type:"+flock_type);
        Log.d("flock_id","flock_period:"+flock_period);
        Log.d("flock_id","location_id:"+location_id);
        Log.d("flock_id","location_name:"+location_name);*/
         holder.lastRecordingId.setText(recording_id);
        holder.textLastRecording.setText(lastRecording);
        holder.mortality.setText(mortality);
        holder.totalbird.setText(total_bird);
        holder.valuehatchDate.setText("[" + hatch_date +"]: " + hasil);
        holder.egg.setText(value);
        holder.textView2.setText(value1);
        holder.textView3.setText(value2);


        holder.linearContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //mFragment.onItemLongClicked(position);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:

                                database = db.getWritableDatabase();
                                String Query2= "SELECT * from daily_recording order by id desc limit 1";
                                Cursor cursor2 = database.rawQuery(Query2,null);
                                try {
                                    while (cursor2.moveToNext()) {
                                        last_recording_id2 = Integer.parseInt(cursor2.getString(0));

                                    }
                                } finally {
                                    cursor2.close();
                                }
                                Log.d("last_recording_id2", ""+last_recording_id2);
                                Log.d("recording_id", ""+recording_id);
                                if (Long.parseLong(recording_id) == last_recording_id2 ){
                                    SQLiteDatabase dbase = db.getReadableDatabase();
                                    Cursor cursor= null;
                                    cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+flockid+"'",null);
                                    cursor.moveToFirst();
                                    for (int i=0; i < cursor.getCount(); i++){
                                        cursor.moveToPosition(i);

                                        id_flock = cursor.getString(cursor.getColumnIndex("id"));
                                        name_flock = cursor.getString(cursor.getColumnIndex("name"));
                                        loc_id = cursor.getString(cursor.getColumnIndex("location_id"));
                                        type = cursor.getString(cursor.getColumnIndex("type"));
                                        period = cursor.getString(cursor.getColumnIndex("period"));
                                    }
                                    status = "update";
                                    //recordingDate = help.getStartRecordingDate(id_flock);
                                    String lastDate = getDateLastRecording(id_flock);

                                    Log.d("lastDate", "lastDate= "+lastDate);
                                    Log.d("zzzs", "flock_id= "+id_flock);
                                    Log.d("zzzs","flock_name= "+name_flock);
                                    Log.d("zzzs", "flock_type= "+type);
                                    Log.d("zzzs","flock_period= "+period);
                                    Log.d("zzzs","status= "+status);
                                    Log.d("zzzs","recordingDate= "+lastDate);

                                    Context context = view.getContext();
                                    Intent i = new Intent(context, EditUnRecordingMainActivity.class);
                                    i.putExtra("recording_id",recording_id);
                                    i.putExtra("DATE",lastDate);
                                    i.putExtra("id_flock",id_flock);
                                    i.putExtra("name_flock",name_flock);
                                    i.putExtra("type",type);
                                    i.putExtra("period",period);
                                    i.putExtra("status",status);
                                    context.startActivity(i);

                                }else{
                                    showWarning(view);
                                }


                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                            database = db.getWritableDatabase();
                                            String Query2= "SELECT * from daily_recording order by id desc limit 1";
                                            Cursor cursor2 = database.rawQuery(Query2,null);
                                            try {
                                                while (cursor2.moveToNext()) {
                                                    last_recording_id2 = Integer.parseInt(cursor2.getString(0));

                                                }
                                            } finally {
                                                cursor2.close();
                                            }
                                        Log.d("last_recording_id2", ""+last_recording_id2);
                                        Log.d("recording_id", ""+recording_id);
                                        if (Long.parseLong(recording_id) == last_recording_id2 ){

                                            database.delete("daily_recording", "id" +  "=" + recording_id, null);
                                            database.delete("cache_mortality", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("cache_total_bird", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("daily_recording_chick_in", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("recording_feed_consumption", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("recording_medication_vaccination", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("recording_mortality", "daily_recording_id" +  "=" + recording_id, null);
                                            database.delete("recording_production", "daily_recording_id" +  "=" + recording_id, null);

                                            database = db.getWritableDatabase();
                                            String Query= "SELECT * from daily_recording order by id desc limit 1";
                                            Cursor cursor = database.rawQuery(Query,null);
                                            try {
                                                while (cursor.moveToNext()) {
                                                    recordingDate = cursor.getString(1);
                                                    id_flock = cursor.getString(2);
                                                }
                                            } finally {
                                                cursor.close();
                                            }

                                            Log.d("eee","recordingDate" + recordingDate);
                                            Log.d("eee","id_flock" +id_flock);
                                            ContentValues data=new ContentValues();
                                            data.put("last_recording",recordingDate);
                                            database.update("flock", data, "id=" + id_flock, null);

                                            showInformation(view);
                                        }else{
                                            showWarning(view);
                                        }

                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();
                                break;
                        }
                    }
                }).show();
                return true;
            }

        });
        /*holder.linearContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                database = db.getWritableDatabase();
                String Query2= "SELECT * from daily_recording order by id desc limit 1";
                Cursor cursor2 = database.rawQuery(Query2,null);
                Log.d("cursor2", ""+cursor2);
                try {
                    while (cursor2.moveToNext()) {
                        String last_recording_id2 = String.valueOf(cursor2.getString(0));
                        Log.d("last_recording_id", ""+last_recording_id2);
                    }
                } finally {
                    cursor2.close();
                }

                Log.d("recording_id", ""+recording_id);
                // TODO Auto-generated method stub

            }
        });*/
    }
    @Override
    public int getItemCount() {
        return flock_items.size();
    }

    public void AddEggLayout(ViewHolder holder, String type, String weight, String egg){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mortalityInf = inflater.inflate(R.layout.egg_result_row, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView eggType = (TextView) mortalityInf.findViewById(R.id.egg);
        TextView eggWeight = (TextView) mortalityInf.findViewById(R.id.weight);
        TextView eggNumber = (TextView) mortalityInf.findViewById(R.id.egg_number);
        eggType.setText(type);
        eggWeight.setText(weight);
        eggNumber.setText(egg);

        holder.parentLinear.addView(mortalityInf);
    }

   public void showWarning(final View view){
       // setup the alert builder
       AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
       builder.setTitle("Information");
       builder.setMessage("Data can't be delete because used other recording");
       // add a button
       builder.setPositiveButton("OK", null);
       // create and show the alert dialog
       AlertDialog dialog = builder.create();
       dialog.show();
   }

    public void showInformation(final View view){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle("Information");
        builder.setMessage("OK");
        // add a button
        builder.setPositiveButton("OK", null);
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public String getDateLastRecording(String flockId){
        //mencari tanggal last recording yg baru saja di update oleh flock
        String Query= "select * from flock where id = '"+flockId+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String lastRecording = "";
        if(cursor.moveToFirst())
            lastRecording  =  cursor.getString(cursor.getColumnIndex("last_recording"));
        cursor.close();
        Log.d("zzzs","last recording = "+lastRecording);

        if (lastRecording==null){
            Log.d("zzzs","getDateLastRecording last recording");
        }
        else
            Log.d("zzzs","getDateLastRecording isi last recording "+lastRecording);

        return lastRecording;
    }
}
