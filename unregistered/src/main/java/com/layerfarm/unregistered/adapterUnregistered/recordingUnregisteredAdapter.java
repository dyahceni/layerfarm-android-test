package com.layerfarm.unregistered.adapterUnregistered;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.chickin.model.chickinModel;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.chickinUnregistered.EditChickinUnregisteredMainActivity;
import com.layerfarm.unregistered.chickinUnregistered.UnregisteredChickinMainActivity;

import java.util.List;


public class recordingUnregisteredAdapter extends RecyclerView.Adapter<recordingUnregisteredAdapter.MyViewHolder> {

    private Context context;
    private List<chickinModel> notesList;
    AlertDialog.Builder dialog;
    DatabaseHelper db;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHatcDate;
        public TextView tvNumberOfBird;
        public TextView tvRecordingDate;
        public TextView tvStrainName;
        public TextView tvId;

        public MyViewHolder(View view) {
            super(view);
            tvHatcDate = view.findViewById(R.id.tvHatchDateUn);
            tvNumberOfBird = view.findViewById(R.id.tvNumberBirdUn);
            tvRecordingDate = view.findViewById(R.id.tvDateRecordingUm);
            tvStrainName = view.findViewById(R.id.tvStrainNameUn);
            tvId = view.findViewById(R.id.tvchickinIdUn);

        }
    }


    public recordingUnregisteredAdapter(Context context, List<chickinModel> notesList) {
        this.context = context;
        this.notesList = notesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_unregistered_chickin, parent, false);
        db = new DatabaseHelper(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final chickinModel student = notesList.get(position);

        holder.tvHatcDate.setText("Hatch Date = " + student.getHatch_date());
        holder.tvId.setText(student.getId());
        holder.tvNumberOfBird.setText("Number of Bird = " +student.getNumber_of_bird() + " Birds");
        holder.tvRecordingDate.setText("Date Recording = " +student.getRecording_date());
        holder.tvStrainName.setText("Name Strain = " +student.getStrains_id());
       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, student.getHatch_date(), Toast.LENGTH_SHORT).show();
            }
        });*/

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //mFragment.onItemLongClicked(position);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                              Intent mIntent = new Intent(view.getContext(), EditChickinUnregisteredMainActivity.class);
                                mIntent.putExtra("nid", notesList.get(position).getId());
                                mIntent.putExtra("chick_in_type", notesList.get(position).getChick_in_type());
                                mIntent.putExtra("hatch_date", notesList.get(position).getHatch_date());
                                mIntent.putExtra("age_in_week", notesList.get(position).getAge_in_week());
                                mIntent.putExtra("age_in_days", notesList.get(position).getAge_in_days());
                                mIntent.putExtra("age_on_date", notesList.get(position).getAge_on_date());
                                mIntent.putExtra("strains_id", notesList.get(position).getStrains_id());
                                mIntent.putExtra("rid", notesList.get(position).getRid());
                                mIntent.putExtra("number_of_bird", notesList.get(position).getNumber_of_bird());
                                mIntent.putExtra("recording_date", notesList.get(position).getRecording_date());
                                view.getContext().startActivity(mIntent);
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                        SQLiteDatabase dbase = db.getReadableDatabase();
                                        dbase.delete("chick_in_distribution", "chick_in_id = ?", new String[] {notesList.get(position).getId()});
                                        dbase.delete("chick_in", "id = ?", new String[] {notesList.get(position).getId()});
                                            Toast.makeText(context,"Data Deleted",Toast.LENGTH_LONG).show();

                                            //((Activity)context).finish();
                                            ((UnregisteredChickinMainActivity)context).Submit();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();
                                break;
                        }
                    }
                }).show();
                return true;
            }

        });
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }
}
/*public class chickinUnregisteredAdapter  extends RecyclerView.Adapter<chickinUnregisteredAdapter.ExpViewHolder> {
    Context c;
    android.app.AlertDialog.Builder dialog;
    private ArrayList<chickinModel> dataList;

    public chickinUnregisteredAdapter(ArrayList<chickinModel> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ExpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rv_unregistered_chickin, parent, false);
        return new ExpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ExpViewHolder holder, final int position) {

        holder.txtHatchDate.setText(dataList.get(position).getHatch_date());
        holder.txtNumberBird.setText(dataList.get(position).getNumber_of_bird());
        holder.txtDate.setText(dataList.get(position).getRecording_date());


*//*

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), EditGeneralExpenseActivity.class);
                mIntent.putExtra("nid", dataList.get(position).getNid());
                mIntent.putExtra("date", dataList.get(position).getDate());
                mIntent.putExtra("vedor", dataList.get(position).getVendor());
                view.getContext().startActivity(mIntent);
            }
        });
*//*


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                //mFragment.onItemLongClicked(position);
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(view.getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
*//*                                Intent mIntent = new Intent(view.getContext(), EditGeneralExpenseActivity.class);
                                mIntent.putExtra("nid", dataList.get(position).getNid());
                                mIntent.putExtra("date", dataList.get(position).getDate());
                                mIntent.putExtra("scope", dataList.get(position).getScope());
                                mIntent.putExtra("vendor", dataList.get(position).getVendor());
                                mIntent.putExtra("item", dataList.get(position).getItem());
                                mIntent.putExtra("cost", dataList.get(position).getCost());
                                view.getContext().startActivity(mIntent);*//*
                                break;
                            case 1:
                                AlertDialog.Builder builder=new AlertDialog.Builder(view.getContext()); //Home is name of the activity
                                builder.setMessage("Do you want to delete this data?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {


                                    }
                                });

                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert=builder.create();
                                alert.show();
                                break;
                        }
                    }
                }).show();
                return true;
            }

        });

    }

    @Override
    public int getItemCount() {
        return (dataList == null) ? 0 : dataList.size();
    }

    class ExpViewHolder extends RecyclerView.ViewHolder {
        TextView txtNid, txtdate, txtVendor, txtScope, txtLocation, txtItem, txtCost, txtHatchDate, txtNumberBird, txtDate;
        ImageView imageScope;
        ExpViewHolder(View itemView) {
            super(itemView);
            txtHatchDate = (TextView) itemView.findViewById(R.id.tvHatchDateUn);
            txtNumberBird = (TextView) itemView.findViewById(R.id.tvNumberBirdUn);
            txtDate = (TextView) itemView.findViewById(R.id.tvDateRecordingUm);
*//*            txtVendor = (TextView) itemView.findViewById(R.id.tvVendor);
            //txtScope = (TextView) itemView.findViewById(R.id.tv_scope);
            //txtLocation = (TextView) itemView.findViewById(R.id.tVNid);
            txtItem = (TextView) itemView.findViewById(R.id.tvItem);
            txtCost = (TextView) itemView.findViewById(R.id.tvCost);
            imageScope = (ImageView) itemView.findViewById(R.id.imageView);*//*
        }
    }
}*/
