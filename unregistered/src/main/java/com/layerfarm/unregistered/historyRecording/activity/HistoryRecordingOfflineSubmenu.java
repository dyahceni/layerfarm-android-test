package com.layerfarm.unregistered.historyRecording.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.chickinUnregistered.UnregisteredChickinMainActivity;
import com.layerfarm.unregistered.createAccountFarmMainActivity;
import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.historyRecording.adapter.HistoryAdapter;
import com.layerfarm.unregistered.model.Flock;
import com.layerfarm.unregistered.model.HatchDate;
import com.layerfarm.unregistered.model.History;
import com.layerfarm.unregistered.model.recordingUnregisteredModel;
import com.layerfarm.unregistered.recordingUnregistered.AddRecordingUnregisteredActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryRecordingOfflineSubmenu extends Activity {
    String flock_id;
    String flock_name;
    String flock_type;
    String flock_period;
    String location_id;
    String location_name;
    String status;
    String recordingDate;
    long ID;
    TextView location, flock;

    helperUnregistered help;
    DatabaseHelper dbase;
    private DBDataSourceUnregistered dataSource;

    ArrayList<String> data;
    ArrayList<Flock> flock_data = new ArrayList<>();
    ArrayList <recordingUnregisteredModel> flock_items;
    private RecyclerView rvHistory;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_submenu_offline);

        help = new helperUnregistered(this);
        help.open();
        dbase = new DatabaseHelper(this);
        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();
        final SQLiteDatabase db = dbase.getReadableDatabase();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //show fab or hide fab based recording date
        //get last record chickin
        String selectQuery = "SELECT id FROM chick_in";
        Cursor cursorChickin = db.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if (cursorChickin.moveToFirst())
            strlastchickin = cursorChickin.getString(cursorChickin.getColumnIndex("id"));
        cursorChickin.close();
        //Log.d("last chickin" , ""+ strlastchickin);
        if (strlastchickin.equals("")) {
            fab.hide();
            showDialog();

        } else {

            String hatch_date_exist = "";
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = sdf.format(new Date());
                //get last record chickin
                Cursor cursorlast = db.rawQuery("SELECT * FROM daily_recording order by recording_date desc limit 1", null);
                if (cursorlast.moveToFirst())
                    hatch_date_exist = cursorlast.getString(1);
                cursorlast.close();
                Log.d("hatch_date_exist",""+hatch_date_exist);
                Log.d("date_check",""+currentDate);
                if (hatch_date_exist != null) {
                    Date date_recording = sdf.parse(hatch_date_exist);
                    Date date_check_today = sdf.parse(currentDate);
                    Log.d("date_recording", "" + date_recording);
                    Log.d("date_check_today", "" + date_check_today);
                    if (date_recording.after(date_check_today) || date_recording.equals(date_check_today)) {
                        fab.hide();
                        //fab.show();
                    } else {
                        fab.show();
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        //mencari flock dengan id location tersebut
        Cursor cursor= null;
        cursor = db.rawQuery("SELECT * FROM flock",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            flock_id = cursor.getString(cursor.getColumnIndex("id"));
            flock_name = cursor.getString(cursor.getColumnIndex("name"));
            location_id = cursor.getString(cursor.getColumnIndex("location_id"));
            flock_type = cursor.getString(cursor.getColumnIndex("type"));
            flock_period = cursor.getString(cursor.getColumnIndex("period"));
            //flock_status = cursor.getString(cursor.getColumnIndex("status"));
        }
        location_name = "Default Flock";



       /* Bundle extras = getIntent().getExtras();
        flock_id = extras.getString("id_flock");
        flock_name = extras.getString("name_flock");
        flock_type = extras.getString("type");
        flock_period = extras.getString("period");
        location_id = extras.getString("loc_id");
        location_name = extras.getString("loc_name");*/

        location = (TextView) findViewById(R.id.location_title);
        flock = (TextView) findViewById(R.id.flock_title);

        //location.setText(location_name);
        //flock.setText(flock_name);

        rvHistory = (RecyclerView) findViewById(R.id.content);
        rvHistory.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(layoutManager);

        Insert();

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
//        mSwipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                //Insert();
                Log.d("zzzs","HISTORY LAGI REFRESH");
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String location = "Default Flock";
                //mendapatkan id dari location tersebut
                //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
                //dan hanya memberikan 1 buah output dengan index 0
                //String loc_id = dataSource.getLocation(location).get(0);

                //mencari flock dengan id location tersebut
                flock_items = new ArrayList();
                Cursor cursor= null;
                cursor = db.rawQuery("SELECT * FROM flock",null);
                cursor.moveToFirst();
                for (int i=0; i < cursor.getCount(); i++){
                    cursor.moveToPosition(i);

                    flock_id  = cursor.getString(cursor.getColumnIndex("id"));
                    flock_name = cursor.getString(cursor.getColumnIndex("name"));
                    location_id = cursor.getString(cursor.getColumnIndex("location_id"));
                    flock_type =cursor.getString(cursor.getColumnIndex("type"));
                    flock_period = cursor.getString(cursor.getColumnIndex("period"));
                    status = cursor.getString(cursor.getColumnIndex("status"));
                    location_name = location;
                }

                recordingDate = help.getStartRecordingDate(flock_id);
                //Log.d("recordingDatezzzz", "" + recordingDate);
                ID = dataSource.getRecordingID(flock_id, recordingDate);
                //Log.d("IDzzzz", "" + ID);

                if (recordingDate != null && ID == 0){
                    status = "new";
                } else{
                    status = "update";
                }
                //Log.d("status", "" + status);

                Context context = view.getContext();
                Intent i = new Intent(context, AddRecordingUnregisteredActivity.class);
                i.putExtra("DATE",recordingDate);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status",status);

               /* Log.d("zzzs","ARGUMENT FRAGMENT");
                Log.d("zzzs", "recordingDate= "+recordingDate);
                Log.d("zzzs", "flock_id= "+flock_id);
                Log.d("zzzs","flock_name= "+flock_name);
                Log.d("zzzs", "flock_type= "+flock_type);
                Log.d("zzzs","flock_period= "+flock_period);
                Log.d("zzzs","location_name= "+location_name);
                Log.d("zzzs","location_id= "+location_id);
                Log.d("zzzs","status= "+status);*/

                context.startActivity(i);

            }
        });
    }
    public String[][] getRecordingProduction(String id){

        SQLiteDatabase db = dbase.getReadableDatabase();
        String Query= "SELECT * from daily_recording INNER JOIN recording_production on daily_recording.id = recording_production.daily_recording_id\n" +
                "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                "where daily_recording.id = '"+id+"'";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        String[][] production = new String[cursor.getCount()][3];
        //index 0 merupakan nama dari egg quality
        //index 1 jumlah berdasarkan butiran telur
        //index 2 jumlah berdasarkan berat telur
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            production[i][0] = cursor.getString(cursor.getColumnIndex("name"));
            production[i][1] = cursor.getString(cursor.getColumnIndex("egg_number"));
            production[i][2] = cursor.getString(cursor.getColumnIndex("egg_weight"));
        }
        return production;
    }
    public void Insert(){
        ArrayList<History> history = new ArrayList<>();

        Flock data_flock =new Flock();
        data_flock.setFlock_id(flock_id);
        data_flock.setFlock_name(flock_name);
        data_flock.setType(flock_type);
        data_flock.setPeriod(flock_period);
        data_flock.setLocation_id(location_id);
        data_flock.setLocation_name(location_name);
        flock_data.add(data_flock);

        SQLiteDatabase db = dbase.getReadableDatabase();
        String Query= "SELECT * from daily_recording where flock_id = '"+flock_id+"' ORDER BY recording_date DESC";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            data = new ArrayList<>();
            History hist = new History();
            //ArrayList<String> data = new ArrayList<>();
            cursor.moveToPosition(i);
            String value = new String();
            String recording_id = cursor.getString(cursor.getColumnIndex("id"));
            String dateRecording = cursor.getString(cursor.getColumnIndex("recording_date"));
            //Log.d("history","date: "+ dateRecording);

            hist.setDate(dateRecording);
            hist.setFlock_data(flock_data);

            String production[][] = getRecordingProduction(recording_id);
            for (int a=0; a<production.length; a++){
                //Log.d("history",production[a][0]+" eggs: "+production[a][1]);
                value += production[a][0]+" eggs: "+production[a][1]+"\n";
                //Log.d("history",production[a][0]+" weight: "+production[a][2]);
                value += production[a][0]+" weight: "+production[a][2]+"\n";
            }
            //Log.d("history","TRANSFER OUT");
          /*  value += "TRANSFER OUT \n";
            ArrayList<TransferOut> tf_out = help.getTransferOutHatchDatebyDate(flock_id,dateRecording);
            for (int b =0; b< tf_out.size(); b++) {
                //Log.d("history", "transfer out: " + "[" + tf_out.get(b).getHatchDate() + "]: " + tf_out.get(b).getQty());
                value += "[" + tf_out.get(b).getHatchDate() + "]: " + tf_out.get(b).getQty()+"\n";
            }

            //Log.d("history", "TRANSFER IN");
            value += "TRANSFER IN \n";
            ArrayList<TransferIn> tf_in= help.getTransferInHatchDatebyDate(flock_id,dateRecording);
            for (int b =0; b< tf_in.size(); b++) {
                //Log.d("history", "transfer in: " + "[" + tf_in.get(b).getHatchDate() + "]: " + tf_in.get(b).getQty());
                value += "[" + tf_in.get(b).getHatchDate() + "]: " + tf_in.get(b).getQty()+"\n";
            }*/
            //Log.d("history", "MORTALITY");

            //Strat from here
            ArrayList<HatchDate> hatchDate = help.getHatchDate(flock_id,dateRecording);
            int total_mortality =0;

            value += "MORTALITY \n";
            for (int b = 0; b < hatchDate.size(); b++){
                //int total_bird = help.getTotalBirdByHatchDate(hatchDate.get(b).getHatchDate(),dateRecording);
                int mortality = help.getMortalityToday(flock_id,dateRecording,hatchDate.get(b).getHatchDate());
                //Log.d("history", "mortality: "+ "["+hatchDate.get(b).getHatchDate()+"]: "+total_mortality);
                value += "["+hatchDate.get(b).getHatchDate()+"]: "+mortality +"\n";
            }
/*
            value += "SPENT HEN \n";
            for (int b = 0; b < hatchDate.size(); b++){
                //int total_bird = help.getTotalBirdByHatchDate(hatchDate.get(b).getHatchDate(),dateRecording);
                int spentHen = help.getSpentHenToday(flock_id,dateRecording,hatchDate.get(b).getHatchDate());
                //Log.d("history", "mortality: "+ "["+hatchDate.get(b).getHatchDate()+"]: "+total_mortality);
                value += "["+hatchDate.get(b).getHatchDate()+"]: "+spentHen +"\n";
            }*/

            //kurang yg total bird
            //Log.d("history","TOTAL BIRD: ");
            value += "TOTAL BIRD \n";
            int jumlah =0;
            for (int b = 0; b < hatchDate.size(); b++){
                int nilai = help.getTotal(flock_id,dateRecording,hatchDate.get(b).getHatchDate()) - help.getMortalityToday(flock_id,dateRecording, hatchDate.get(b).getHatchDate())-
                        help.getBirdTransferOutToday(flock_id, dateRecording, hatchDate.get(b).getHatchDate())- help.getSpentHenToday(flock_id, dateRecording, hatchDate.get(b).getHatchDate());
                //Log.d("history","total: "+ "["+hatchDate.get(b).getHatchDate()+"]: "+nilai );)
                value += "["+hatchDate.get(b).getHatchDate()+"]: "+nilai+"\n";
                jumlah += nilai;
            }
            //Log.d("history","JUMLAH TOTAL BIRD: "+jumlah);
            value += "JUMLAH TOTAL BIRD: "+jumlah;
            data.add(value);
            hist.setData(value);
            history.add(hist);
        }

        Log.d("history", "size data: "+history.size());
        for (int i = 0; i<history.size(); i++){
            Log.d("history", history.get(i).getDate());
            String Data = history.get(i).getData();
            Log.d("history","data = "+Data);
        }
        adapter = new HistoryAdapter(history);
        adapter.notifyDataSetChanged();
        rvHistory.setAdapter(adapter);
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Data Not Found");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Are you want to entry hatching/chickin data ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        startActivity(new Intent(HistoryRecordingOfflineSubmenu.this, UnregisteredChickinMainActivity.class));
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        HistoryRecordingOfflineSubmenu.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }


    public void activate(View view) {
        /*Intent intent = new Intent(this, createAccountFarmMainActivity.class);
        startActivity(intent);*/

        // check local data server
        SQLiteDatabase db = dbase.getReadableDatabase();
        String count = "SELECT count(*) FROM farm_expired_date";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount > 0){
            Intent intent = new Intent();
            intent.setClassName("com.layerfarm.layerfarmmanager", "com.layerfarm.layerfarmmanager.renewalGoogleBillingActivity");
            startActivity(intent);
        } else{
            Intent intent = new Intent(this, createAccountFarmMainActivity.class);
            startActivity(intent);
        }
    }

    public void backRecording_1(View view) {
        finish();
    }

    public void backRecording_2(View view) {
        finish();
    }
}
