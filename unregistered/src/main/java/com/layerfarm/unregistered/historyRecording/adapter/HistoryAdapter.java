package com.layerfarm.unregistered.historyRecording.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.historyRecording.fragment.MainFragment;
import com.layerfarm.unregistered.model.Flock;
import com.layerfarm.unregistered.model.History;
import com.layerfarm.unregistered.model.eggModel;
import com.layerfarm.unregistered.model.recordingUnregisteredModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    ArrayList<History> data;
    private SQLiteDatabase database;
    DatabaseHelper db;
    public HistoryAdapter(ArrayList<History> items){
        data = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txHistory;
        TextView txDate;
        CardView card;

        public TextView mortality;
        public TextView textLastRecording;
        public TextView totalbird;
        public TextView valuehatchDate, egg,textView2, textView3 ;
        public ViewGroup leftLinear, rightLinear, parentLinear;
        public TextView lastRecordingId;



        public ViewHolder(View itemView) {
            super(itemView);
            txHistory = (TextView) itemView.findViewById(R.id.history_rv);
            card = (CardView) itemView.findViewById(R.id.date_history);
            txDate = (TextView) itemView.findViewById(R.id.history_date);

            lastRecordingId = (TextView) itemView.findViewById(R.id.id_recording);
            textLastRecording = (TextView) itemView.findViewById(R.id.textLastRecording);
            mortality = (TextView) itemView.findViewById(R.id.valueMortality);
            totalbird = (TextView) itemView.findViewById(R.id.valueTotalBird);
            valuehatchDate = (TextView) itemView.findViewById(R.id.valueHatchDate);
            egg = (TextView) itemView.findViewById(R.id.lastRecording);
            textView2 = (TextView) itemView.findViewById(R.id.textView2);
            textView3 = (TextView) itemView.findViewById(R.id.textView3);

            db = new DatabaseHelper(itemView.getContext());
            database = db.getWritableDatabase();
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_rv_offline, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String date = data.get(position).getDate();
        ArrayList<Flock> flock_data = data.get(position).getFlock_data();
        final String text = data.get(position).getData();
        final String location_name = flock_data.get(0).getLocation_name();
        final String location_id = flock_data.get(0).getLocation_id();
        final String flock_id = flock_data.get(0).getFlock_id();
        final String flock_name = flock_data.get(0).getFlock_name();
        final String flock_type = flock_data.get(0).getType();
        final String flock_period = flock_data.get(0).getPeriod();
        final String status = "edit";

        //codingan saya
        //mencari flock dengan id location tersebut
        ArrayList <recordingUnregisteredModel> flock_items;
        flock_items = new ArrayList();
        String loc_id = "1";
        Cursor cursor= null;
        cursor = database.rawQuery("select flock.id as flock_id, chick_in.hatch_date,daily_recording_chick_in.chick_in_id, daily_recording.id as daily_recording_id, daily_recording.recording_date, cache_total_bird.mortality, cache_total_bird.total_bird  from daily_recording\n" +
                "inner join cache_total_bird on cache_total_bird.daily_recording_id = daily_recording.id\n" +
                "inner join daily_recording_chick_in on daily_recording_chick_in.daily_recording_id = daily_recording.id\n" +
                "INNER join chick_in on chick_in.id = daily_recording_chick_in.chick_in_id\n" +
                "INNER JOIN flock on flock.id = daily_recording.flock_id  where daily_recording.flock_id = '"+loc_id+"' order by daily_recording.recording_date desc ",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            recordingUnregisteredModel flock = new recordingUnregisteredModel();
            flock.setId(cursor.getString(cursor.getColumnIndex("daily_recording_id")));
            flock.setFlock_id(cursor.getString(cursor.getColumnIndex("flock_id")));
            flock.setValue_mortality(cursor.getString(cursor.getColumnIndex("mortality")));
            flock.setTotal_bird(cursor.getString(cursor.getColumnIndex("total_bird")));
            flock.setRecording_date(cursor.getString(cursor.getColumnIndex("recording_date")));
            flock.setAge(cursor.getString(cursor.getColumnIndex("hatch_date")));
            flock_items.add(flock);

        }

        final String recording_id = flock_items.get(position).getId();
        final String mortality = flock_items.get(position).getValue_mortality();
        final String total_bird = flock_items.get(position).getTotal_bird();
        final String lastRecording =  flock_items.get(position).getRecording_date();
        final String hatch_date =  flock_items.get(position).getAge();
        final String flockid =  flock_items.get(position).getFlock_id();
        long bedaHari=0;
        long hari =0;
        try {
            DateFormat date2 = new SimpleDateFormat("yyyy-MM-dd");
            Date tglAwal = (Date) date2.parse(hatch_date);
            Date tglAkhir = (Date) date2.parse(lastRecording);

            bedaHari = Math.abs(tglAkhir.getTime() - tglAwal.getTime());
            hari = TimeUnit.MILLISECONDS.toDays(bedaHari);
            //Log.d("zzzs","beda hari: "+hari);
        }
        catch (Exception e){}

        long minggu = hari/7;
        long sisa = hari%7;
        String hasil = " "+minggu+" week(s) "+sisa+" day(s)";

        Log.d("lastRecording","lastRecording:"+lastRecording);
        Log.d("mortality","flock:"+mortality);
        Log.d("total_bird","total_bird:"+total_bird);
        Log.d("flock_id","flock_id:"+flockid);

        //query telur
        ArrayList<eggModel> eggarray = new ArrayList<>();
        String Query= "SELECT * from daily_recording INNER JOIN recording_production on daily_recording.id = recording_production.daily_recording_id\n" +
                "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                "WHERE daily_recording.recording_date = '"+lastRecording+"'";
        Cursor cursor1 = database.rawQuery(Query, null);
        String name = "";
        String weight = "";
        String egg = "";
        cursor.moveToFirst();


        for (int i=0; i < cursor1.getCount(); i++) {
            cursor1.moveToPosition(i);
            eggModel eggnum = new eggModel();
            name = cursor1.getString(19);
            weight = cursor1.getString(17);
            egg = cursor1.getString(16);
            eggnum.setQualityEgg(name);
            eggnum.setValueWeight(weight);
            eggnum.setValueEgg(egg);
            eggarray.add(eggnum);
        }

        String value = "";
        String value1 = "";
        String value2 = "";
        for (int a = 0; a<eggarray.size(); a++){
            Log.d("nama","nama:"+eggarray.get(a).getQualityEgg());
            Log.d("jumlah","kg:"+eggarray.get(a).getValueWeight());
            Log.d("jumlah","telur:"+eggarray.get(a).getValueEgg());

            // value += eggarray.get(a).getQualityEgg()+"\n";
            value += eggarray.get(a).getQualityEgg()+"\n";
            value1 += eggarray.get(a).getValueWeight()+"\n";
            value2 += eggarray.get(a).getValueEgg()+"\n";
            //AddEggLayout(holder, eggarray.get(a).getQualityEgg(),eggarray.get(a).getValueWeight() ,eggarray.get(a).getQualityEgg());
        }

        holder.lastRecordingId.setText(recording_id);
        holder.textLastRecording.setText(lastRecording);
        holder.mortality.setText(mortality);
        holder.totalbird.setText(total_bird);
        holder.valuehatchDate.setText("[" + hatch_date +"]: " + hasil);
        holder.egg.setText(value);
        holder.textView2.setText(value1);
        holder.textView3.setText(value2);


        holder.txDate.setText(date);
        holder.txHistory.setText(text);


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, MainFragment.class);
                i.putExtra("DATE", date);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status",status);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
