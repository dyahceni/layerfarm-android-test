package com.layerfarm.unregistered.historyRecording.activity;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.historyRecording.adapter.FlockAdapter;
import com.layerfarm.unregistered.model.Flock;

import java.util.ArrayList;


public class RecordingOfflineMainActivity extends Activity {
    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSourceUnregistered dataSource;
    ArrayList<Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording_main_offline);

        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();

        db = new DatabaseHelper(this);

        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               Submit();
           }
           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

        rvFlock = (RecyclerView) findViewById(R.id.rv_flock);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);


    }

    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Flock flock = new Flock();
            flock.setFlock_id(cursor.getString(cursor.getColumnIndex("id")));
            flock.setFlock_name(cursor.getString(cursor.getColumnIndex("name")));
            flock.setLocation_id(cursor.getString(cursor.getColumnIndex("location_id")));
            flock.setType(cursor.getString(cursor.getColumnIndex("type")));
            flock.setPeriod(cursor.getString(cursor.getColumnIndex("period")));
            flock.setStatus(cursor.getString(cursor.getColumnIndex("status")));
            flock.setLocation_name(location);
            flock_items.add(flock);
        }
        adapter = new FlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
}
