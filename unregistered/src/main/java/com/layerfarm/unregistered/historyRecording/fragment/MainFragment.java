package com.layerfarm.unregistered.historyRecording.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.helperUnregistered;


public class MainFragment extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    helperUnregistered help;
    private DBDataSourceUnregistered dataSource;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, recordingDate;
    long ID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_offline_main);

        help = new helperUnregistered(this);
        help.open();
        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();

        final RelativeLayout rootView = (RelativeLayout) findViewById(R.id.layout_recording);

        Bundle extras = getIntent().getExtras();
        recordingDate = extras.getString("DATE");
        status = extras.getString("status");
        flock_id = extras.getString("id_flock");
        flock_name = extras.getString("name_flock");
        flock_type = extras.getString("type");
        flock_period = extras.getString("period");
        location_id = extras.getString("loc_id");
        location_name = extras.getString("loc_name");

        Log.d("zzzs","FRAGMENT MAIN");
        Log.d("zzzs", "recordingDate= "+recordingDate);
        Log.d("zzzs", "flock_id= "+flock_id);
        Log.d("zzzs","flock_name= "+flock_name);
        Log.d("zzzs", "flock_type= "+flock_type);
        Log.d("zzzs","flock_period= "+flock_period);
        Log.d("zzzs","location_name= "+location_name);
        Log.d("zzzs","location_id= "+location_id);
        Log.d("zzzs","status= "+status);
        //recording untuk menu recording
        if (recordingDate == null && status == null){
            Log.d("zzzs","recordingdate == null");
            recordingDate = help.getStartRecordingDate(flock_id);
            Log.d("zzzs", "recordingDated = " + recordingDate);
            ID = dataSource.getRecordingID(flock_id, recordingDate);
            if (ID == 0) {
                status = "new";
            } else
                status = "update";
        }
        //recording entry untuk menu history
        //else {

        //}

        // kita set default nya Home Fragment
        loadFragment(new RecordingFragment());



        // inisialisasi BottomNavigaionView
/*        final BottomNavigationView bottomNavigationView = findViewById(R.id.btm_nav);
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.getMenu().findItem(R.id.action_clipboard).setChecked(true);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();
                Log.d("keyboard", "screenHeight = " + screenHeight);
                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("keyboard", "keypadHeight = " + keypadHeight);
                //int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
                //Log.d("keyboard", "heightDiff = " + heightDiff);
                if (keypadHeight > screenHeight * 0.15) {
                    Log.d("keyboard", "keyboard opened");
                    bottomNavigationView.animate()
                            .translationY(bottomNavigationView.getHeight()).setDuration(1000).start();
                    bottomNavigationView.setVisibility(View.GONE);
                } else {
                    Log.d("keyboard", "keyboard closed");
                    bottomNavigationView.setVisibility(View.VISIBLE);
                    bottomNavigationView.animate()
                            .translationY(0).setDuration(300).start();
                }
            }
        });*/
        TextView date = (TextView) findViewById(R.id.recording_date);
        date.setText(recordingDate);
    }
    @Override
    public void onBackPressed() {

        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setIcon(R.drawable.camera)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","load fragment");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_test, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        int i1 = menuItem.getItemId();
        if (i1 == R.id.action_clipboard){
            fragment = new RecordingFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","ARGUMENT FRAGMENT");
            Log.d("zzzs", "recordingDate= "+recordingDate);
            Log.d("zzzs", "flock_id= "+flock_id);
            Log.d("zzzs","flock_name= "+flock_name);
            Log.d("zzzs", "flock_type= "+flock_type);
            Log.d("zzzs","flock_period= "+flock_period);
            Log.d("zzzs","location_name= "+location_name);
            Log.d("zzzs","location_id= "+location_id);
            Log.d("zzzs","status= "+status);
            Toast.makeText(getApplicationContext(), "Recording clicked", Toast.LENGTH_SHORT).show();
        }
        return loadFragment(fragment);
    }
}