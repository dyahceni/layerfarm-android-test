package com.layerfarm.unregistered;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.layerfarm.unregistered.intro.IntroActivity;
import com.layerfarm.unregistered.intro.MainIntroActivity;

import java.util.ArrayList;
import java.util.List;

public class createAccountFarmMainActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_farm_main);

    }

    public void create_farm(View view) {
        startActivity(new Intent(createAccountFarmMainActivity.this, ActivationUserMainActivity.class));
    }
    public void create_partner(View view){
        startActivity(new Intent(createAccountFarmMainActivity.this, AppAddPartner.class));
    }

    public void BackCreateFarm(View view) {
        finish();
    }
}
