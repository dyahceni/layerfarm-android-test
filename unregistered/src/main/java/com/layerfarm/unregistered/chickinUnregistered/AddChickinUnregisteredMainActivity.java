package com.layerfarm.unregistered.chickinUnregistered;

import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.layerfarm.unregistered.DateWheel.DatePickerPopWin;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.R;


import java.text.SimpleDateFormat;
import java.util.Date;

public class AddChickinUnregisteredMainActivity extends AppCompatActivity {

    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener, mDateSetListeneronDate, mDateSetListenerHatch, mDateSetListenerAge;
    DatabaseHelper SQLite = new DatabaseHelper(this);
    public String[] strain_value;
    private Spinner spStrain;
    private Spinner spChickin;
    private String[] chickinmode = {"Hatch Date","Age"};
    private EditText mEdtMasked, rec_date_age, mEdtMaskedHatch, rec_date_hatch, hatch_date, totalBirdAge, totalBirdHatch,inWeek,inDays;
    private String strdailyId ="";
    private TextView descChickin;
    private String hatchtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_chickin_unregistered_main);
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabsChickinUnregistered);

        //get value from xml
        mEdtMasked = (EditText) findViewById(R.id.on_date);
        mEdtMaskedHatch = (EditText) findViewById(R.id.hatchDate);
        rec_date_age = (EditText) findViewById(R.id.start_recording_date_age);
        rec_date_hatch = (EditText) findViewById(R.id.start_recording_date_hatch);
        totalBirdAge = (EditText) findViewById(R.id.edBirdAge);
        totalBirdHatch = (EditText) findViewById(R.id.edBirdHatch);
        hatch_date  = (EditText) findViewById(R.id.hatchDate);
        spStrain = (Spinner)findViewById(R.id.spStrain);
        descChickin = (TextView) findViewById(R.id.txtDescriptionType);

        inWeek= (EditText) findViewById(R.id.inWeek);
        inDays= (EditText) findViewById(R.id.inDays);

        totalBirdHatch.clearFocus();
        mEdtMaskedHatch.requestFocus();
        SQLite = new DatabaseHelper(getApplicationContext());

        deleteRecording();
        //Load spLocation value
        strainName();
        //chickinMode();


        /*// inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, chickinmode);
        // mengeset Array Adapter tersebut ke Spinner
        spChickin.setAdapter(adapter);
        // mengeset listener untuk mengetahui saat item dipilih
        spChickin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)

                if (adapterView.getSelectedItem().toString() == "Age")
                {
                    //Toast.makeText(adapterView.getContext(), adapterView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                    CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                    card_view_hatch.setVisibility(View.GONE);

                    CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                    card_view_age.setVisibility(View.VISIBLE);

                    Toast.makeText(AddChickinUnregisteredMainActivity.this, "Selected "+ adapter.getItem(i) +" Mode", Toast.LENGTH_SHORT).show();

                }

                else
                {
                    CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                    card_view_hatch.setVisibility(View.VISIBLE);

                    CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                    card_view_age.setVisibility(View.GONE);

                    Toast.makeText(AddChickinUnregisteredMainActivity.this,  "Selected "+ adapter.getItem(i) +" Mode", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });
*/

        //set Date
        mEdtMaskedHatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        AddChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(AddChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        mEdtMaskedHatch.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(AddChickinUnregisteredMainActivity.this);
            }
        });
        /*mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                mEdtMaskedHatch.setText(date);
            }
        };*/


        //set Date
        mEdtMasked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        AddChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListeneronDate,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(AddChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        mEdtMasked.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(AddChickinUnregisteredMainActivity.this);

            }
        });
        /*mDateSetListeneronDate = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                mEdtMasked.setText(date);
            }
        };*/

        //set Date
        rec_date_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        AddChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerAge,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(AddChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        rec_date_age.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(AddChickinUnregisteredMainActivity.this);

            }
        });
       /* mDateSetListenerAge = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                rec_date_age.setText(date);
            }
        };*/

        //set Date
        rec_date_hatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        AddChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerHatch,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(AddChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        rec_date_hatch.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(AddChickinUnregisteredMainActivity.this);

            }
        });
       /* mDateSetListenerHatch = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                rec_date_hatch.setText(date);
            }
        };*/


        onTabTapped(0);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });


        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.hatchDate));
                EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_hatch));

                String hatch_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_hatch = sdf.parse(hatch_date_check);
                    if (date_recording.before(date_hatch)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };

        rec_date_hatch.addTextChangedListener(watcher);

        TextWatcher watcherAge = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.on_date));
                EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_age));

                String on_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_on_date = sdf.parse(on_date_check);
                    if (date_recording.before(date_on_date)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };

        rec_date_age.addTextChangedListener(watcherAge);
    }

    private void onTabTapped(int position) {

        CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
        CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
        switch (position) {
            case 0:
                mEdtMaskedHatch.requestFocus();

                hatchtype = "Hatch Date";
                descChickin.setText("Choose this option if you know the date when the birds are hatched");
                card_view_hatch.setVisibility(View.VISIBLE);
                card_view_age.setVisibility(View.GONE);
                Toast.makeText(AddChickinUnregisteredMainActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                hatchtype = "Age";
                mEdtMasked.requestFocus();
                descChickin.setText("Choose this option if you don’t know when the birds are hatched, but you know the age of the birds.");
                card_view_hatch.setVisibility(View.GONE);
                card_view_age.setVisibility(View.VISIBLE);
                Toast.makeText(AddChickinUnregisteredMainActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            default:

        }

    }
    //ambil data taruh spinner
    public void strainName(){
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM strain",null);
        strain_value = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            strain_value[i] = cursor.getString(1).toString();
        }
        spStrain = (Spinner) findViewById(R.id.spStrain);
        spStrain.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, strain_value));
        spStrain.setSelected(true);
    }

    //ambil data taruh spinner
/*    public void chickinMode(){
        spChickin = (Spinner) findViewById(R.id.chickinMode);
        spChickin.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, chickinmode));
        spChickin.setSelected(true);
    }*/


    public void addChickinUnregistered(View view) {
        //Spinner spnLocale = (Spinner)findViewById(R.id.chickinMode);
        if (hatchtype.equals("Age")) {
            addChickinAge();
        } else {
            addChickinHatch();
        }
    }

    private void addChickinHatch() {
        //String chickinMode = spChickin.getSelectedItem().toString();
        String chickinMode = "Hatch Date";
        String hatchDate = hatch_date.getText().toString().trim();
        String strainHatch = spStrain.getSelectedItem().toString();

        EditText b = (EditText)findViewById(R.id.start_recording_date_hatch);
        String start_recording = (String) (b.getText().toString());
        EditText d = (EditText) findViewById(R.id.edBirdHatch);
        String total_bird = (String) (d.getText().toString());
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        // TODO Auto-generated method stub
        //Get Strain id
        String selectQueryStrain= "SELECT id FROM strain WHERE name = '"+ strainHatch + "' LIMIT 1";
        Cursor cursorStrain = dbase.rawQuery(selectQueryStrain, null);
        String strStrain = "";
        if(cursorStrain.moveToFirst())
            strStrain  =  cursorStrain.getString(cursorStrain.getColumnIndex("id"));
        cursorStrain.close();
        //Toast.makeText(ChickinEntryActivity.this, "Strain ID "+ strStrain , Toast.LENGTH_SHORT).show();

        //Get flock id
        String selectQueryFlock= "SELECT * FROM flock LIMIT 1";
        Cursor cursorFlock = dbase.rawQuery(selectQueryFlock, null);
        String strFlock = "";
        if(cursorFlock.moveToFirst())
            strFlock  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
        cursorFlock.close();
        //Toast.makeText(ChickinEntryActivity.this, "ID Flock "+ strFlock , Toast.LENGTH_SHORT).show();

        String rid_value ="1";
        //insert to chicin table
        String insertSQL = "INSERT INTO chick_in \n" +
                "(chick_in_type, hatch_date, strains_id, rid)VALUES \n" +
                "(?, ?, ?, ?);";
        dbase.execSQL(insertSQL, new String[]{chickinMode, hatchDate,strStrain,rid_value});

        //get last record chickin
        String selectQuery = "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if (cursor.moveToFirst())
            strlastchickin = cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();

        String insertDistributionSQL = "INSERT INTO chick_in_distribution \n" +
                "(chick_in_id, flock_id, number_of_bird, start_recording) VALUES \n" +
                "(?,  ?, ? ,?);";
        dbase.execSQL(insertDistributionSQL, new String[]{strlastchickin, strFlock, total_bird, start_recording});

        String insertChanceSQL = "INSERT INTO cache_hatch_date \n" +
                "(type, flock_id, chick_in_id, timestamp,new) VALUES \n" +
                "(?, ? ,?, ?, ?);";
        dbase.execSQL(insertChanceSQL, new String[]{"chick_in", strFlock,strlastchickin, start_recording, "1"});

        AddChickinUnregisteredMainActivity.this.finish();

        /*
        try {


            Toast.makeText(ChickinEntryActivity.this, "Save Data Successfully", Toast.LENGTH_SHORT).show();
        } catch (SQLiteException e) {
            Toast.makeText(ChickinEntryActivity.this, "Save Data Error", Toast.LENGTH_SHORT).show();
        }
        */
        //finish();
    }


    private void addChickinAge() {

        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        //String chickinMode = spChickin.getSelectedItem().toString();
        String chickinMode = "Age";
        String strain = spStrain.getSelectedItem().toString();
        String onDate = mEdtMasked.getText().toString().trim();
        String inWeekAge = inWeek.getText().toString().trim();
        String inDaysAge = inDays.getText().toString().trim();

        EditText b = (EditText) findViewById(R.id.start_recording_date_age);
        String start_recording = (String) (b.getText().toString());
        EditText d = (EditText) findViewById(R.id.edBirdAge);
        String total_bird = (String) (d.getText().toString());


        // TODO Auto-generated method stub

        //Get Strain id
        String selectQueryStrain= "SELECT id FROM strain WHERE name = '"+ strain + "' LIMIT 1";
        Cursor cursorStrain = dbase.rawQuery(selectQueryStrain, null);
        String strStrain = "";
        if(cursorStrain.moveToFirst())
            strStrain  =  cursorStrain.getString(cursorStrain.getColumnIndex("id"));
        cursorStrain.close();
        //Toast.makeText(ChickinEntryActivity.this, "Selected "+ strStrain +" Mode", Toast.LENGTH_SHORT).show();

        String rid_value ="1";
        //insert to chicin table
        String insertSQL = "INSERT INTO chick_in \n" +
                "(chick_in_type,hatch_date, age_in_week, age_in_days, age_on_date, strains_id, rid)VALUES \n" +
                "(?, ?, ?, ?, ? , ?, ?);";
        dbase.execSQL(insertSQL, new String[]{chickinMode, onDate, inWeekAge, inDaysAge, onDate, strStrain, rid_value});


        try {

            //Get flock id
            String selectQueryFlock = "SELECT id FROM flock LIMIT 1";
            Cursor cursorFlok = dbase.rawQuery(selectQueryFlock, null);
            String strFlock = "";
            if (cursorFlok.moveToFirst())
                strFlock = cursorFlok.getString(cursorFlok.getColumnIndex("id"));
            cursorFlok.close();
            //Toast.makeText(ChickinEntryActivity.this, "flock id " + strFlock, Toast.LENGTH_SHORT).show();

            //get last record chickin
            String selectQuery = "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String strlastchickin = "";
            if (cursor.moveToFirst())
                strlastchickin = cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();

            //Save to chickin distribution
            String insertDistributionSQL = "INSERT INTO chick_in_distribution \n" +
                    "(chick_in_id, flock_id, number_of_bird, start_recording) VALUES \n" +
                    "(?,  ?, ? ,?);";
            dbase.execSQL(insertDistributionSQL, new String[]{strlastchickin, strFlock, total_bird, start_recording});


            String insertChanceSQL = "INSERT INTO cache_hatch_date \n" +
                    "(type, flock_id, chick_in_id, timestamp,new) VALUES \n" +
                    "(?, ? ,?, ?, ?);";
            dbase.execSQL(insertChanceSQL, new String[]{"chick_in", strFlock,strlastchickin, start_recording, "1"});

            Toast.makeText(AddChickinUnregisteredMainActivity.this, "Save Data Successfully", Toast.LENGTH_SHORT).show();
        AddChickinUnregisteredMainActivity.this.finish();
        } catch (SQLiteException e) {
            Toast.makeText(AddChickinUnregisteredMainActivity.this, "Save Data Error", Toast.LENGTH_SHORT).show();
        }
        //finish();
    }

    private void deleteRecording(){
        SQLiteDatabase dbase = SQLite.getReadableDatabase();

        //mencari tanggal kosong terakhir di tabel date_flock_empty
        String Query= "SELECT * FROM date_flock_empty ORDER BY date DESC LIMIT 1";
        Cursor cursor = dbase.rawQuery(Query, null);
        String tgl_kosong_terakhir = "";
        if(cursor.moveToFirst())
            tgl_kosong_terakhir  =  cursor.getString(cursor.getColumnIndex("date"));
        cursor.close();
        Log.d("tgl_terakhir" , "= " + tgl_kosong_terakhir);
        //mencari jumlah date dari daily recording di bawah tgl kosong terakhir
        int idCount = 0;
        String selectQueryDailyRecording = "SELECT * FROM daily_recording where recording_date <= '"+tgl_kosong_terakhir+"'";
        Cursor cursorDailyRecording = dbase.rawQuery(selectQueryDailyRecording, null);
        if (cursorDailyRecording.moveToFirst() && cursorDailyRecording.getCount() > 0) {
            idCount = cursorDailyRecording.getCount();
            cursorDailyRecording.close();
        }
        Log.d("idCount" , "= " + idCount);
        String[] arrayID ;
        if (idCount > 0 && tgl_kosong_terakhir!= null){
            for(int x = 0; x < idCount; x++){
                //get last record chickin
                String selectLastChickinQuery = "SELECT * FROM daily_recording order by id desc";
                Cursor cursorLastChickin = dbase.rawQuery(selectLastChickinQuery, null);
                if (cursorLastChickin.moveToFirst()) {
                    strdailyId = cursorLastChickin.getString(cursorLastChickin.getColumnIndex("id"));
                    cursorLastChickin.close();
                    Log.d("id", "" + strdailyId);
                }
                //delete data other table
                delete_from_recording_entry(strdailyId);
            }
        }
    }

    public void delete_from_recording_entry(String dailyid){
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        String query_production = "DELETE FROM recording_production WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(query_production);
        String query_feed = "DELETE FROM recording_feed_consumption WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(query_feed);
        String query_ovk = "DELETE FROM recording_medication_vaccination WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(query_ovk);
        String query_mortality = "DELETE FROM recording_mortality WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(query_mortality);
        String query_chick_in = "DELETE FROM daily_recording_chick_in WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(query_chick_in);
        String cache_total_bird = "DELETE FROM cache_total_bird WHERE daily_recording_id = '"+dailyid+"'";
        dbase.execSQL(cache_total_bird);

        //from nanang
        String cache_hatch_date = "DELETE FROM cache_hatch_date";
        dbase.execSQL(cache_hatch_date);

        String cache_mortality = "DELETE FROM cache_mortality";
        dbase.execSQL(cache_mortality);

        String cache_total_birdSQL = "DELETE FROM cache_total_bird";
        dbase.execSQL(cache_total_birdSQL);

        /*String daily_recording = "DELETE FROM daily_recording";
        dbase.execSQL(daily_recording);*/

        dbase.delete("daily_recording","id=?",new String[]{dailyid});

        String chick_in_distribution = "DELETE FROM chick_in_distribution";
        dbase.execSQL(chick_in_distribution);

        String chick_in_SQL = "DELETE FROM chick_in";
        dbase.execSQL(chick_in_SQL);
    }



    public void Back_1(View view) {
        finish();
    }

    public void Back_2(View view) {
        finish();
    }
}
