package com.layerfarm.unregistered.chickinUnregistered;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.chickin.model.chickinModel;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.adapterUnregistered.chickinUnregisteredAdapter;
import com.layerfarm.unregistered.createAccountFarmMainActivity;


import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class UnregisteredChickinMainActivity extends AppCompatActivity {

    DatabaseHelper SQLite = new DatabaseHelper(this);
    private List<chickinModel> chickin_items = new ArrayList<chickinModel>();
    private RecyclerView recyclerView;
    private chickinUnregisteredAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unregistered_chickin_main);
        FloatingActionButton fab = (FloatingActionButton) findViewById(com.layerfarm.setting.R.id.fab);
        recyclerView = (RecyclerView) findViewById(R.id.rv_chickin_unregistered);
        SQLite = new DatabaseHelper(getApplicationContext());
        Submit();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UnregisteredChickinMainActivity.this, AddChickinUnregisteredMainActivity.class);
                startActivity(intent);
            }
        });


    }

    public void Submit(){
        SQLite = new DatabaseHelper(getApplicationContext());
        chickin_items = new ArrayList();
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT chick_in.id as nid, chick_in.hatch_date as hatchDate, chick_in.age_in_week as age_in_week, chick_in.age_in_days as age_in_days, chick_in.age_on_date as age_on_date, location.name as location_name, flock.name, flock.type, chick_in_distribution.number_of_bird, chick_in_distribution.start_recording as start_recording, strain.name as strain_name, chick_in.chick_in_type from chick_in, location, flock, strain, chick_in_distribution\n" +
                "where chick_in_distribution.chick_in_id = chick_in.id\n" +
                "and strain.id = chick_in.strains_id\n" +
                "and flock.location_id = location.id",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            chickinModel flock = new chickinModel();
            //flock.setId(cursor.getString(cursor.getColumnIndex("id")));
            flock.setId(cursor.getString(cursor.getColumnIndex("nid")));
            flock.setStrains_id(cursor.getString(cursor.getColumnIndex("strain_name")));
            flock.setHatch_date(cursor.getString(cursor.getColumnIndex("hatchDate")));
            flock.setRecording_date(cursor.getString(cursor.getColumnIndex("start_recording")));
            flock.setNumber_of_bird(cursor.getString(cursor.getColumnIndex("number_of_bird")));
            flock.setChick_in_type(cursor.getString(cursor.getColumnIndex("chick_in_type")));
            flock.setAge_in_week(cursor.getString(cursor.getColumnIndex("age_in_week")));
            flock.setAge_in_days(cursor.getString(cursor.getColumnIndex("age_in_days")));
            flock.setAge_on_date(cursor.getString(cursor.getColumnIndex("age_on_date")));
            //flock.setStatus(cursor.getString(cursor.getColumnIndex("status")));
            //flock.setLocation_name(location);
            chickin_items.add(flock);
        }
        Log.e("result",""+ chickin_items);
        adapter = new chickinUnregisteredAdapter(this, chickin_items);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Submit();
    }

    public void activate_user_page(View view) {
        Intent intent = new Intent(this, createAccountFarmMainActivity.class);
        startActivity(intent);

       /* Session prefManager = new Session(getApplicationContext());
        prefManager.setFirstTimeLaunch(true);
        startActivity(new Intent(UnregisteredChickinMainActivity.this, WelcomeActivity.class));
        finish();*/
    }



    public void activate_chickin(View view) {

        // check local data server
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        String count = "SELECT count(*) FROM farm_expired_date";
        Cursor mcursor = dbase.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount > 0){
            Intent intent = new Intent();
            intent.setClassName("com.layerfarm.layerfarmmanager", "com.layerfarm.layerfarmmanager.renewalGoogleBillingActivity");
            startActivity(intent);
        } else{
            Intent intent = new Intent(this, createAccountFarmMainActivity.class);
            startActivity(intent);
        }
    }

    public void back_1(View view) {
        finish();
    }

    public void back_2(View view) {
        finish();
    }
}
