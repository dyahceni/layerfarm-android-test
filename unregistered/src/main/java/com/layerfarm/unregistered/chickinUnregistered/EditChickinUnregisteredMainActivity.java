package com.layerfarm.unregistered.chickinUnregistered;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.setting.DateWheel.DatePickerPopWin;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EditChickinUnregisteredMainActivity extends AppCompatActivity {
    private android.app.DatePickerDialog.OnDateSetListener mDateSetListener, mDateSetListeneronDate, mDateSetListenerHatch, mDateSetListenerAge;

    DatabaseHelper SQLite = new DatabaseHelper(this);
    EditText edIdChickinUn, inWeek, inDays, on_date, start_recording_date_age, edBirdAge, hatchDate, start_recording_date_hatch, edBirdHatch;
    Spinner chickinMode;
    String nid,
    chick_in_type,
    hatch_date,
    age_in_week,
    age_in_days,
    age_on_date,
    strains_id,
    rid,
    number_of_bird,
    recording_date;
    String[] type= {"Hatch Date","Age"};
    private String strdailyId ="";
    private TextView descChickin;
    private String hatchtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_chickin_unregistered_main);
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabsEditChickinUnregistered);

        SQLite = new DatabaseHelper(getApplicationContext());

        edIdChickinUn = (EditText) findViewById(R.id.edIdChickinUn) ;
        inWeek  = (EditText) findViewById(R.id.inWeek) ;
        inDays  = (EditText) findViewById(R.id.inDays) ;
        on_date  = (EditText) findViewById(R.id.on_date) ;
        start_recording_date_age  = (EditText) findViewById(R.id.start_recording_date_age) ;
        edBirdAge  = (EditText) findViewById(R.id.edBirdAge) ;
        hatchDate  = (EditText) findViewById(R.id.hatchDate) ;
        start_recording_date_hatch  = (EditText) findViewById(R.id.start_recording_date_hatch) ;
        edBirdHatch  = (EditText) findViewById(R.id.edBirdHatch) ;
        descChickin = (TextView) findViewById(R.id.txtDescriptionType);
/*        chickinMode = (Spinner) findViewById(R.id.chickinMode);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, type);
        // mengeset Array Adapter tersebut ke Spinner
        chickinMode.setAdapter(adapter);*/


        Intent mIntent = getIntent();
        if(getIntent().getExtras()!=null)
        {
            // Get Extras
            nid = mIntent.getStringExtra("nid");
            chick_in_type = mIntent.getStringExtra("chick_in_type");
            hatch_date = mIntent.getStringExtra("hatch_date");
            age_in_week = mIntent.getStringExtra("age_in_week");
            age_in_days = mIntent.getStringExtra("age_in_days");
            age_on_date = mIntent.getStringExtra("age_on_date");
            strains_id = mIntent.getStringExtra("strains_id");
            rid = mIntent.getStringExtra("rid");
            number_of_bird = mIntent.getStringExtra("number_of_bird");
            recording_date = mIntent.getStringExtra("recording_date");



            edIdChickinUn.setText(nid);


            Log.d("hatch type",""+ chick_in_type);

            Log.d("age_in_week",""+ age_in_week);
            Log.d("age_in_days",""+ age_in_days);
            Log.d("age_on_date",""+ age_on_date);

            if (chick_in_type.equals("Age")) {

                mTabLayout.getTabAt(1).select();
                onTabTapped(1);

                inWeek.setText(age_in_week);
                inDays.setText(age_in_days);  ;
                on_date.setText(age_on_date);  ;
                start_recording_date_age.setText(recording_date);
                edBirdAge.setText(number_of_bird); ;

           /*     //Toast.makeText(adapterView.getContext(), adapterView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.GONE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.VISIBLE);

                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + chick_in_type + " Mode", Toast.LENGTH_SHORT).show();
*/
            }

            else {
                mTabLayout.getTabAt(0).select();
                onTabTapped(0);

                hatchDate.setText(hatch_date);  ;
                start_recording_date_hatch.setText(recording_date);
                edBirdHatch.setText(number_of_bird);

                /*CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.VISIBLE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.GONE);

                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + chick_in_type + " Mode", Toast.LENGTH_SHORT).show();
           */ }
        }

        // End of intens

        //Start Spinner select
        // mengeset Array Adapter tersebut ke Spinner
        //chickinMode.setAdapter(adapter);

 /*       chickinMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {@Override
        public void onItemSelected(AdapterView < ?>adapterView, View view, int i, long l) {
            // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)

            if (adapterView.getSelectedItem().toString().equals("Age")) {
                //Toast.makeText(adapterView.getContext(), adapterView.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.GONE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.VISIBLE);

                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + adapter.getItem(i) + " Mode", Toast.LENGTH_SHORT).show();

            }

            else {
                CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
                card_view_hatch.setVisibility(View.VISIBLE);

                CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
                card_view_age.setVisibility(View.GONE);

                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + adapter.getItem(i) + " Mode", Toast.LENGTH_SHORT).show();

            }

        }

            @Override
            public void onNothingSelected(AdapterView < ?>adapterView) {

            }
        });
*/

        //set Date
        on_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        EditChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        on_date.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(EditChickinUnregisteredMainActivity.this);

            }
        });
/*        mDateSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                on_date.setText(date);
            }
        };*/


        //set Date
        start_recording_date_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        EditChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListeneronDate,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        start_recording_date_age.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(EditChickinUnregisteredMainActivity.this);

            }
        });
/*        mDateSetListeneronDate = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                start_recording_date_age.setText(date);
            }
        };*/

        //set Date
        hatchDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        EditChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerAge,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        hatchDate.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(EditChickinUnregisteredMainActivity.this);

            }
        });
 /*       mDateSetListenerAge = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                hatchDate.setText(date);
            }
        };*/

        //set Date
        start_recording_date_hatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(
                        EditChickinUnregisteredMainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerHatch,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
                dialog.show();*/
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(EditChickinUnregisteredMainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        //Toast.makeText(ChickinEntryActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
                        start_recording_date_hatch.setText(dateDesc);
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose(currentDateandTime) // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(EditChickinUnregisteredMainActivity.this);

            }
        });
 /*       mDateSetListenerHatch = new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                start_recording_date_hatch.setText(date);
            }
        };*/

        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.hatchDate));
                EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_hatch));

                String hatch_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_hatch = sdf.parse(hatch_date_check);
                    if (date_recording.before(date_hatch)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };

        start_recording_date_hatch.addTextChangedListener(watcher);

        TextWatcher watcherAge = new TextWatcher() {
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                boolean isValid = true;

                EditText mDateEntryField = (EditText)(findViewById(R.id.on_date));
                EditText mEditTextDate = (EditText)(findViewById(R.id.start_recording_date_age));

                String on_date_check = (String)(mDateEntryField.getText().toString());
                String start_recording = (String)(mEditTextDate.getText().toString());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date_recording = sdf.parse(start_recording);
                    Date date_on_date = sdf.parse(on_date_check);
                    if (date_recording.before(date_on_date)) {
                        isValid = false;

                    } else {
                        isValid = true;

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                }

                if (!isValid) {
                    mEditTextDate.setError("Recording date can't smaller than hatch date");
                    mEditTextDate.setFocusable(true);
                } else {
                    mEditTextDate.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        };

        start_recording_date_age.addTextChangedListener(watcherAge);

    }

    private void onTabTapped(int position) {

        CardView card_view_hatch = (CardView) findViewById(R.id.card_view_hatch_date);
        CardView card_view_age = (CardView) findViewById(R.id.card_view_age);
        switch (position) {
            case 0:
                hatchtype = "Hatch Date";
                descChickin.setText("Choose this option if you know the date when the birds are hatched");
                card_view_hatch.setVisibility(View.VISIBLE);
                card_view_age.setVisibility(View.GONE);
                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                hatchtype = "Age";
                descChickin.setText("Choose this option if you don’t know when the birds are hatched, but you know the age of the birds.");
                card_view_hatch.setVisibility(View.GONE);
                card_view_age.setVisibility(View.VISIBLE);
                Toast.makeText(EditChickinUnregisteredMainActivity.this, "Selected " + hatchtype + " Mode", Toast.LENGTH_SHORT).show();
                break;
            default:

        }

    }

    public void editChickinUnregistered(View view) {
        //Spinner spnLocale = (Spinner)findViewById(R.id.chickinMode);
        //if (spnLocale.getSelectedItem().toString().equals("Age")) {
        Log.e("hatchtype",""+ hatchtype);
        if (hatchtype.equals("Age")) {
            editChickinAge(nid);
        } else {
            editChickinHatch(nid);
        }
    }



/*        public boolean updateDataHatch(String id,String chick_in_type,String hatch_date,String strains_id, String rid) {
            SQLiteDatabase dbase = SQLite.getReadableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id",id);
            contentValues.put("chick_in_type",chick_in_type);
            contentValues.put("hatch_date",hatch_date);
            contentValues.put("strains_id",strains_id);
            contentValues.put("rid",rid);
            dbase.update("chick_in",contentValues,"id = ?",new String[] {id});
            return true;
        }*/




    private void editChickinHatch(String id) {
        Log.e("xx",""+ id);
        Log.e("xx",""+ nid);

        try {
        //Spinner a = (Spinner)findViewById(R.id.chickinMode);
        //String hatchtype = a.getSelectedItem().toString();
            String hatch_type = "Hatch Date";
        EditText b = (EditText)findViewById(R.id.hatchDate);
        String hatch_date = (String)(b.getText().toString());
        EditText c = (EditText)findViewById(R.id.start_recording_date_hatch);
        String start_recording = (String)(c.getText().toString());
        EditText d = (EditText)findViewById(R.id.edBirdHatch);
        String total_bird = (String)(d.getText().toString());



        String cid = getLastChickinId();
        String fid = getFlockId();
        String sid = getStrainId();
        String rid = "0";


        Log.e("xx",""+ hatch_type);
        Log.e("xx",""+ hatch_date);
        Log.e("xx",""+ start_recording);
        Log.e("xx",""+ total_bird);
            Log.e("xx",""+ cid);
            Log.e("xx",""+ fid);
            Log.e("xx",""+ sid);
            Log.e("xx",""+ rid);

        //update Chickin
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id",id);
        contentValues.put("chick_in_type",hatch_type);
        contentValues.put("hatch_date",hatch_date);
        contentValues.put("strains_id",sid);
        contentValues.put("rid",rid);
        dbase.update("chick_in",contentValues,"id = ?",new String[] {id});


        //update Chickin Distribution
        ContentValues contentValuesDist = new ContentValues();
        contentValuesDist.put("flock_id",fid);
        contentValuesDist.put("number_of_bird",total_bird);
        contentValuesDist.put("start_recording",start_recording);
        dbase.update("chick_in_distribution",contentValuesDist,"chick_in_id = ?",new String[] {id});

        Toast.makeText(EditChickinUnregisteredMainActivity.this,"Data Updated",Toast.LENGTH_LONG).show();
        //    finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void editChickinAge(String id) {
        Log.e("id",""+ id);
        Log.e("nid",""+ nid);


        try {
            //Spinner a = (Spinner)findViewById(R.id.chickinMode);
            //String hatchtype = a.getSelectedItem().toString();
            String hatch_type = "Age";
            EditText b = (EditText)findViewById(R.id.inWeek);
            String in_week = (String)(b.getText().toString());
            EditText c = (EditText)findViewById(R.id.inDays);
            String in_days = (String)(c.getText().toString());
            EditText d = (EditText)findViewById(R.id.on_date);
            String on_date = (String)(d.getText().toString());

            EditText e = (EditText)findViewById(R.id.start_recording_date_age);
            String start_recording = (String)(e.getText().toString());
            EditText f = (EditText)findViewById(R.id.edBirdAge);
            String total_bird = (String)(f.getText().toString());

            String cid = getLastChickinId();
            String fid = getFlockId();
            String sid = getStrainId();
            String rid = "0";


            //update Chickin
            SQLiteDatabase dbase = SQLite.getReadableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id",id);
            contentValues.put("chick_in_type",hatch_type);
            contentValues.put("hatch_date",on_date);
            contentValues.put("age_in_week",in_week);
            contentValues.put("age_in_days",in_days);
            contentValues.put("age_on_date",on_date);
            contentValues.put("strains_id",sid);
            contentValues.put("rid",rid);
            dbase.update("chick_in",contentValues,"id = ?",new String[] {id});


            //update Chickin Distribution
            ContentValues contentValuesDist = new ContentValues();
            contentValuesDist.put("flock_id",fid);
            contentValuesDist.put("number_of_bird",total_bird);
            contentValuesDist.put("start_recording",start_recording);
            dbase.update("chick_in_distribution",contentValuesDist,"chick_in_id = ?",new String[] {id});

            Toast.makeText(EditChickinUnregisteredMainActivity.this,"Data Updated",Toast.LENGTH_LONG).show();
            EditChickinUnregisteredMainActivity.this.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public String getStrainId (){
        //Get Strain id
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        String selectQueryStrain= "SELECT id FROM strain LIMIT 1";
        Cursor cursorStrain = dbase.rawQuery(selectQueryStrain, null);
        String strStrain = "";
        if(cursorStrain.moveToFirst())
            strStrain  =  cursorStrain.getString(cursorStrain.getColumnIndex("id"));
        cursorStrain.close();
        return strStrain;
    }


    public String getFlockId(){
        //Get flock id
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        String selectQueryFlock= "SELECT * FROM flock LIMIT 1";
        Cursor cursorFlock = dbase.rawQuery(selectQueryFlock, null);
        String flridFlock = "";
        if(cursorFlock.moveToFirst())
            flridFlock  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
        cursorFlock.close();
        return flridFlock;
    }

    public String getLastChickinId(){
        //get last record chickin
        SQLiteDatabase dbase = SQLite.getReadableDatabase();
        String selectQuery = "SELECT id FROM chick_in ORDER BY id DESC LIMIT 1";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if (cursor.moveToFirst()) strlastchickin = cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        return strlastchickin;
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


    public void Back(View view) {finish();
    }
}
