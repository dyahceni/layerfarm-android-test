package com.layerfarm.unregistered.intro;

public class ScreenItem {

    String Title,Description, Description_One, Description_Two ;
    int ScreenImg;

    public ScreenItem(String title, String description, String description_one, String description_two, int screenImg) {
        Title = title;
        Description = description;
        Description_One = description_one;
        Description_Two = description_two;
        ScreenImg = screenImg;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setScreenImg(int screenImg) {
        ScreenImg = screenImg;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getDescription_One() {
        return Description_One;
    }

    public void setDescription_One(String description_One) {
        Description_One = description_One;
    }

    public String getDescription_Two() {
        return Description_Two;
    }

    public void setDescription_Two(String description_Two) {
        Description_Two = description_Two;
    }

    public int getScreenImg() {
        return ScreenImg;
    }
}
