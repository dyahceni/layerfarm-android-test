package com.layerfarm.unregistered.intro;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.unregistered.AppAddPartner;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.intro.IntroActivity;

import java.util.ArrayList;


public class SplashScreenActivity extends Activity {

    DatabaseHelper db;
    String code, uname, passwd, url, urlserver, usernameserver, passwordserver;
    private Handler mHandler = new Handler();
    ProgressBar loading;
    LinearLayout time_out_layout;
    LinearLayout main_layout;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        db = new DatabaseHelper(this);
        loading = (ProgressBar)findViewById(R.id.progress_bar);

        time_out_layout = (LinearLayout) findViewById(R.id.time_out_layout);
        Button try_again = (Button) findViewById(R.id.try_again);
        try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("zzzz", "Click try again");
            }
        });
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        String code = variables.getString("code", null);
        String uname = variables.getString("username", null);
        String passwd = variables.getString("password", null);

        if(code == null && uname == null && passwd == null){
            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
//            purchase_state = 0;
//            changeDashboard();
            startActivity(new Intent(this, IntroActivity.class));
            finish();
        }
        else {
            Log.d("xxx", "url = " + url);
            Log.d("xxx", "uname = " + uname);
            Log.d("xxx", "passwd = " + passwd);

            LayerFarm.getInstance().Connect(this, "http://"+code+"."+ Env.DOMAIN+"/", uname, passwd);
            RetrofitData.getInstance().setStatus("");
            Start();
        }

        Log.d("zzz","code = "+code+" username = "+uname+" password = "+passwd);

//        SQLiteDatabase dbSQL = db.getReadableDatabase();
//        String selectQuery = "SELECT value FROM variables";
//        Cursor cursor = dbSQL.rawQuery(selectQuery, null);
//        ArrayList<String> values = new ArrayList<>();
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                String value = cursor.getString(cursor.getColumnIndex("value"));
//                values.add(value);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//
//        if (values.isEmpty()){
//            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
////            purchase_state = 0;
////            changeDashboard();
//            startActivity(new Intent(this, IntroActivity.class));
//            finish();
//        } else {
//            SharedPreferences.Editor editor = variables.edit();
//            editor.putString("code", values.get(0));
//            editor.putString("username", values.get(1));
//            editor.putString("password", values.get(2));
//            editor.apply();
//
//            code = values.get(0);
//            uname = values.get(1);
//            passwd = values.get(2);
//
//            Log.d("xxx", "url = " + url);
//            Log.d("xxx", "uname = " + uname);
//            Log.d("xxx", "passwd = " + passwd);
//
//            LayerFarm.getInstance(SplashScreenActivity.this).Connect("http://"+code+"."+ Env.DOMAIN+"/", uname, passwd);
//            RetrofitData.getInstance().setStatus("");
//            Start();
//
//        }

        /*
        code = values.get(0);
        uname = values.get(1);
        passwd = values.get(2);

        if (code.isEmpty() && uname.isEmpty() && passwd.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, IntroActivity.class));
            finish();
        } else {
            LayerFarm.getInstance().Connect("http://"+code+"."+ Env.DOMAIN+"/", uname, passwd);
            RetrofitData.getInstance().setStatus("");
            Start();
        }

         */


//        int secondsDelayed = 1;
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                startActivity(new Intent(SplashScreenActivity.this, IntroActivity.class));
//                finish();
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            }
//        }, secondsDelayed * 300);
    }

    public void Start(){
        //progressBar.setVisibility(View.VISIBLE);

        mRun.run();
    }
    public void Stop(){
        //progressBar.setVisibility(View.GONE);

        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            Map<String, String> role = RetrofitData.getInstance().getUser().getRoles();
            Log.d("zzzz","try to connect website");
            Log.d("zzzz","status = "+RetrofitData.getInstance().getStatus());
            Log.d("zzzz","uname = "+uname+" passwd = "+passwd);
            Log.d("zzzz","email = "+email+" passwd = "+passwd);
//            if (email == uname && passwd == passwd){
            Log.d("zzzz","email sama semua");
            if (RetrofitData.getInstance().getStatus() != "") {
                Log.d("zzzz","status != null");
                if (RetrofitData.getInstance().getStatus() == "Success") {
                    //loadingCheckfarm.dismiss();
                    loading.setVisibility(View.GONE);
                    String name = RetrofitData.getInstance().getFarm_name();
                    String url = RetrofitData.getInstance().getBaseUrl();
//                    user_name.setText(RetrofitData.getInstance().getUser().getName());
//                    farm_name.setText(name);
//                    farm_code.setText(url);
//                    farm_role.setText(RetrofitData.getInstance().getUser().getRoles().toString());
                    Log.d("zzz","url = "+url+" role = "+RetrofitData.getInstance().getUser().getRoles());
                    //Toast.makeText(MainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
//                        loadingCheckfarm.dismiss();
//                        getMenu();
//                        loading = ProgressDialog.show(MainActivity.this, "Syncronize Data", "Please wait...", true, false);

                    //delay to prevent CSRF Error
//                    int secs = 2; // Delay in seconds
//                    delay.delay(secs, new delay.DelayCallback() {
//                        public void afterDelay() {
//                            // Do something after delay
//
//                            getLayerFarmDashboard();
//                        }
//                    });

                    Stop();
                    restart();
                } else{
//                        loading.dismiss();
//                        connection.setText("Connection Timeout");
                    Toast.makeText(SplashScreenActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                    Log.d("zzz","Failed dashboard");
                    loading.setVisibility(View.GONE);
//                        Fragment fragment = new TimeOutFragment();
//                        displaySelectedFragment(fragment, "TimeOutFragment");
                    if (RetrofitData.getInstance().getStatus().equals("Wrong username or password")){
                        AskOption();
                    }
                    if (RetrofitData.getInstance().getStatus().equals("This account has been used in other device.")){
                        Intent i = new Intent(getApplicationContext(), AppAddPartner.class);
                        startActivity(i);
                    }
                    else {
                        main_layout.setVisibility(View.GONE);
                        time_out_layout.setVisibility(View.VISIBLE);
                    }
                    Stop();
                }
            }
//            }

        }
    };

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Data Response Empty, Please Try Again!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void restart(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        ActivityCompat.finishAfterTransition(SplashScreenActivity.this);
    }
}
