package com.layerfarm.unregistered;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResetSuccess extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_success);

        TextView message = (TextView) findViewById(R.id.message);
        String email = (getIntent().getStringExtra("email"));
        String resetS_Success = getResources().getString(R.string.reset_success);
        message.setText(resetS_Success + " "+email);
        Button ok = (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AppAddPartner.class);
                startActivity(i);
                finish();
            }
        });

    }
}
