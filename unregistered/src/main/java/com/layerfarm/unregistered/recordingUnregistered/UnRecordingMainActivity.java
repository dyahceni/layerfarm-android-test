package com.layerfarm.unregistered.recordingUnregistered;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.unregistered.chickinUnregistered.UnregisteredChickinMainActivity;
import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.model.recordingUnregisteredModel;
import com.layerfarm.unregistered.ActivationUserMainActivity;
import com.layerfarm.unregistered.Session;
import com.layerfarm.unregistered.WelcomeActivity;
import com.layerfarm.unregistered.adapterUnregistered.UnFlockAdapter;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.R;
import com.layerfarm.unregistered.createAccountFarmMainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class UnRecordingMainActivity extends AppCompatActivity {

    private  ArrayList recording_history = new ArrayList();
    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSourceUnregistered dataSource;
    ArrayList <recordingUnregisteredModel> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    helperUnregistered help;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, recordingDate;
    long ID;
    String last_recording_date_date, date_today;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_recording_main);

        help = new helperUnregistered(this);
        help.open();

        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();
        db = new DatabaseHelper(this);
        SQLiteDatabase dbase = db.getWritableDatabase();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        //get last record chickin
        String selectQuery = "SELECT id FROM chick_in";
        Cursor cursorChickin = dbase.rawQuery(selectQuery, null);
        String strlastchickin = "";
        if (cursorChickin.moveToFirst())
            strlastchickin = cursorChickin.getString(cursorChickin.getColumnIndex("id"));
        cursorChickin.close();
        //Log.d("last chickin" , ""+ strlastchickin);
        if (strlastchickin.equals("")) {
            fab.hide();
            showDialog();

        } else {

        String hatch_date_exist = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = sdf.format(new Date());




            //get last record chickin

            Cursor cursor = dbase.rawQuery("SELECT * FROM daily_recording order by recording_date desc limit 1", null);
            if (cursor.moveToFirst())
                hatch_date_exist = cursor.getString(1);
            cursor.close();

            //Log.d("hatch_date_exist",""+hatch_date_exist);
            //Log.d("date_check",""+currentDate);

    if (hatch_date_exist != null) {
        Date date_recording = sdf.parse(hatch_date_exist);
        Date date_check_today = sdf.parse(currentDate);
        //Log.d("date_recording", "" + date_recording);
        //Log.d("date_check_today", "" + date_check_today);
        if (date_recording.after(date_check_today) || date_recording.equals(date_check_today)) {

            //fab.setVisibility(View.GONE);
            fab.hide();
            ///fab.show();
        } else {

            //fab.setVisibility(View.VISIBLE);
            fab.show();
        }
    }

        } catch(Exception e) {
            e.printStackTrace();
        }


        }

        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);

       /* spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        rvFlock = (RecyclerView) findViewById(R.id.rv_flock);

        Submit();
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* String location = spn_location.getSelectedItem().toString();
                //mendapatkan id dari location tersebut
                //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
                //dan hanya memberikan 1 buah output dengan index 0
                String loc_id = dataSource.getLocation(location).get(0);

                //mencari flock dengan id location tersebut
                flock_items = new ArrayList();
                SQLiteDatabase dbase = db.getReadableDatabase();
                Cursor cursor= null;
                cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
                cursor.moveToFirst();
                for (int i=0; i < cursor.getCount(); i++){
                    cursor.moveToPosition(i);

                    flock_id  = cursor.getString(cursor.getColumnIndex("id"));
                    flock_name = cursor.getString(cursor.getColumnIndex("name"));
                    location_id = cursor.getString(cursor.getColumnIndex("location_id"));
                    flock_type =cursor.getString(cursor.getColumnIndex("type"));
                    flock_period = cursor.getString(cursor.getColumnIndex("period"));
                    status = cursor.getString(cursor.getColumnIndex("status"));
                    recording_date = cursor.getString(cursor.getColumnIndex("last_recording"));
                    location_name = location;
                }

                String status_flock = "new";
                Context context = view.getContext();
                Intent i = new Intent(context, AddUnRecordingMainActivity.class);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("recording_date",recording_date);
                i.putExtra("status",status_flock);
                context.startActivity(i);*/
                //Log.d("flock_id", "" + flock_id);

                String location = spn_location.getSelectedItem().toString();
                //mendapatkan id dari location tersebut
                //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
                //dan hanya memberikan 1 buah output dengan index 0
                String loc_id = dataSource.getLocation(location).get(0);

                //mencari flock dengan id location tersebut
                flock_items = new ArrayList();
                SQLiteDatabase dbase = db.getReadableDatabase();
                Cursor cursor= null;
                cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
                cursor.moveToFirst();
                for (int i=0; i < cursor.getCount(); i++){
                    cursor.moveToPosition(i);

                    flock_id  = cursor.getString(cursor.getColumnIndex("id"));
                    flock_name = cursor.getString(cursor.getColumnIndex("name"));
                    location_id = cursor.getString(cursor.getColumnIndex("location_id"));
                    flock_type =cursor.getString(cursor.getColumnIndex("type"));
                    flock_period = cursor.getString(cursor.getColumnIndex("period"));
                    status = cursor.getString(cursor.getColumnIndex("status"));
                    location_name = location;
                }

                recordingDate = help.getStartRecordingDate(flock_id);
                //Log.d("recordingDatezzzz", "" + recordingDate);
                ID = dataSource.getRecordingID(flock_id, recordingDate);
                //Log.d("IDzzzz", "" + ID);

                if (recordingDate != null && ID == 0){
                    status = "new";
                } else{
                    status = "update";
                }
                //Log.d("status", "" + status);

                Context context = view.getContext();
                Intent i = new Intent(context, AddRecordingUnregisteredActivity.class);
                //Intent i = new Intent(context, AddUnRecordingMainActivity.class);
                i.putExtra("DATE",recordingDate);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status",status);

               /* Log.d("zzzs","ARGUMENT FRAGMENT");
                Log.d("zzzs", "recordingDate= "+recordingDate);
                Log.d("zzzs", "flock_id= "+flock_id);
                Log.d("zzzs","flock_name= "+flock_name);
                Log.d("zzzs", "flock_type= "+flock_type);
                Log.d("zzzs","flock_period= "+flock_period);
                Log.d("zzzs","location_name= "+location_name);
                Log.d("zzzs","location_id= "+location_id);
                Log.d("zzzs","status= "+status);*/

                context.startActivity(i);
                //Log.d("flock_id", "" + flock_id);
            }
        });
    }

    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("select flock.id as flock_id, chick_in.hatch_date,daily_recording_chick_in.chick_in_id, daily_recording.id as daily_recording_id, daily_recording.recording_date, cache_total_bird.mortality, cache_total_bird.total_bird  from daily_recording\n" +
                "inner join cache_total_bird on cache_total_bird.daily_recording_id = daily_recording.id\n" +
                "inner join daily_recording_chick_in on daily_recording_chick_in.daily_recording_id = daily_recording.id\n" +
                "INNER join chick_in on chick_in.id = daily_recording_chick_in.chick_in_id\n" +
                "INNER JOIN flock on flock.id = daily_recording.flock_id  where daily_recording.flock_id = '"+loc_id+"' order by daily_recording.recording_date desc ",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            recordingUnregisteredModel flock = new recordingUnregisteredModel();
            flock.setId(cursor.getString(cursor.getColumnIndex("daily_recording_id")));
            flock.setFlock_id(cursor.getString(cursor.getColumnIndex("flock_id")));
            flock.setValue_mortality(cursor.getString(cursor.getColumnIndex("mortality")));
            flock.setTotal_bird(cursor.getString(cursor.getColumnIndex("total_bird")));
            flock.setRecording_date(cursor.getString(cursor.getColumnIndex("recording_date")));
            flock.setAge(cursor.getString(cursor.getColumnIndex("hatch_date")));
            flock_items.add(flock);

        }

        //Log.d("flock_items"," = "+flock_items);
        adapter = new UnFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Submit();
    }

    public void activate_user_page(View view) {
        Intent intent = new Intent(this, ActivationUserMainActivity.class);
        startActivity(intent);

/*        Session prefManager = new Session(getApplicationContext());
        prefManager.setFirstTimeLaunch(true);
        startActivity(new Intent(UnRecordingMainActivity.this, WelcomeActivity.class));
        finish();*/
    }

    public void back(View view) {
        finish();
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Data Not Found");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Are you want to entry hatching/chickin data ?")
                .setIcon(R.drawable.error)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        startActivity(new Intent(UnRecordingMainActivity.this,UnregisteredChickinMainActivity.class));
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //MainActivity.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    public void activate(View view) {
        Intent intent = new Intent(this, createAccountFarmMainActivity.class);
        startActivity(intent);
    }
}
