package com.layerfarm.unregistered.recordingUnregistered;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.model.EggValue;
import com.layerfarm.unregistered.model.Feed;
import com.layerfarm.unregistered.model.HatchDate;
import com.layerfarm.unregistered.model.Medicine;
import com.layerfarm.unregistered.model.Mortality.Mortality;
import com.layerfarm.unregistered.model.Mortality.MortalityData;
import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class EditUnRecordingMainActivity extends AppCompatActivity {

    private Button submit;
    private EditText edNote,edMortality, total_feed;
    private TextView txMortality, tx_recording_date, tx_house_profile, tx_add_more_feed, tx_add_more_ovk;
    private ViewGroup more_feed, more_medicine, mortalityView, eggsView, profileView, mortalityItemsView;
    private BottomNavigationView bottomNavigationView;

    //inisialisasi array list untuk masing2 pengelompokan di recording
    private ArrayList<Feed> ListFeed;
    private ArrayList<Medicine> ListMedicine;
    private ArrayList<EggValue> ListEggs;
    private ArrayList<MortalityData> ListMortality;
    private ArrayList<String> eggName, mortalityCategory;

    private Spinner feed_type;
    private Spinner medication_name;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, id_daily_recording, recordingDate;
    private ProgressBar pgsBar;
    ArrayList<String> feed_value, ovk_value;
    ArrayList<HatchDate> hatchDate;

    //inisialisasi kontroller/ Data Source
    DatabaseHelper db;
    helperUnregistered help;
    private DBDataSourceUnregistered dataSource;
    private int i = 0;
    private Handler hdlr = new Handler();
    View view;
    long ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_un_recording_main);
        //Script recording
        edNote = (EditText) findViewById(R.id.Note);
        tx_house_profile = (TextView) findViewById(R.id.House);
        pgsBar = (ProgressBar) findViewById(R.id.pBar);

        submit = (Button) findViewById(R.id.Submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pgsBar.setVisibility(v.VISIBLE);
                CheckBeforeSubmit();
                pgsBar.setVisibility(View.GONE);
                Log.d("zzzs","akhir thread");

            }
        });

        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();

        help = new helperUnregistered(this);
        help.open();
        db = new DatabaseHelper(this);

        Bundle extras = getIntent().getExtras();
        id_daily_recording = extras.getString("recording_id");
        flock_id = extras.getString("id_flock");
        flock_name = extras.getString("name_flock");
        flock_type = extras.getString("type");
        flock_period = extras.getString("period");
        location_id = extras.getString("loc_id");
        location_name = extras.getString("loc_name");
        recordingDate = extras.getString("recording_date");

        if (recordingDate == null) {
            Log.d("zzzs", "recordingdate == null");
            recordingDate = help.getStartRecordingDate(flock_id);
            Log.d("zzzs", "recordingDated = " + recordingDate);
            ID = dataSource.getRecordingID(flock_id, recordingDate);
            if (ID == 0) {
                status = "new";
            } else
                status = "update";
        }

/*        Log.d("xx", "recording_id= "+id_daily_recording);
        Log.d("xx", "recordingDate= "+recordingDate);
        Log.d("xx", "flock_id= "+flock_id);
        Log.d("xx","flock_name= "+flock_name);
        Log.d("xx", "flock_type= "+flock_type);
        Log.d("xx","flock_period= "+flock_period);
        Log.d("xx","location_name= "+location_name);
        Log.d("xx","location_id= "+location_id);
        Log.d("xx","status= "+status);*/

        //deklarasi view group
        more_feed = (ViewGroup)findViewById(R.id.more_feed);
        more_medicine = (ViewGroup) findViewById(R.id.more_medicine);
        profileView = (ViewGroup) findViewById(R.id.bird_profile);
        mortalityView = (ViewGroup) findViewById(R.id.mortality);
        eggsView = (ViewGroup) findViewById(R.id.eggs);

        //eggName adalah jenis jenis telur
        eggName = dataSource.getEggName();
        //mendapatkan hatch date untuk flock ini
        hatchDate = help.getHatchDate(flock_id,recordingDate);

        //mendapatkan kategori kematian dari ayam
        mortalityCategory = dataSource.getMortalityCategory();

        //fungsi memasukkan feed ke dalam array yg kemudian digunakan di spinner
        feed_value = dataSource.getFeedName();
        //fungsi memasukkan medicine ke dalam array yg kemudian digunakan di spinner
        ovk_value = dataSource.getMedicineName();

        tx_add_more_feed = (TextView) findViewById(R.id.add_more_feed);
        tx_add_more_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("zzzs","add feed ditekan");
                AddMoreFeed();
            }
        });

        tx_add_more_ovk = (TextView) findViewById(R.id.add_more_ovk);
        tx_add_more_ovk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("zzzs","add ovk ditekan");
                AddMoreMedicine();
            }
        });



        //menampilkan box eggs
        for (int i =0; i< eggName.size();i++){
            ViewEggsValue(eggName.get(i));
        }

        //menampilkan mortality ayam
        for (int i =0; i< hatchDate.size();i++){
            Log.d("zzzs","hatch date: "+hatchDate.get(i).getHatchDate());
            Log.d("zzzs","strain: "+hatchDate.get(i).getStrain());
            ViewMortality(hatchDate.get(i).getHatchDate(), hatchDate.get(i).getStrain());
        }
        //menampilkan profile untuk masing- masing
        tx_house_profile.setText("House : "+location_name+" - "+flock_name+" - "+flock_type+" "+flock_period);
        for (int i =0; i< hatchDate.size();i++){
            //Log.d("zzzs","hatch date: "+hatchDate.get(i).getHatchDate());
            //Log.d("zzzs","strain: "+hatchDate.get(i).getStrain());
            ViewHouseProfile(hatchDate.get(i).getHatchDate(),hatchDate.get(i).getStrain());
            //ViewMortality(hatchDate.get(i).getHatchDate(), hatchDate.get(i).getStrain());
        }

        //cek apakah sudah pernah melakukan recording ataukah belum
        //jika baru pertama kali melakukan recording
        final long ID = dataSource.getRecordingID(flock_id,recordingDate);
        Log.d("zzzs","ID recording = " +ID);
        if (ID == 0){
            Log.d("zzzs","pertama kali recording");
            ViewFeed();
            ViewMedicine();
        }
        else {

            ViewFeed();
            //ViewMedicine();

           Log.d("zzzs", "sudah pernah recording = " + ID);
            //mengambil quality telur
            ArrayList<EggValue> eggValue = getEggProduction(ID, eggName);
            Log.d("zzzs", "isi egg value = " + eggValue.size());
            setEggProduction(eggValue);


            //mengambil feeds
            ArrayList<Feed> feeds = getFeedConsumption(ID);
            if (feeds.size() > 0)
                setFeedConstumption(feeds);
            else
                ViewFeed();

             /*
            //mengambil ovk
            ArrayList<Medicine> medicines = getOVKConsumption(ID);
            if (medicines.size() > 0)
                setOVKConsumption(medicines);
            else
                ViewMedicine();

*/

            //mengambil mortality
            ArrayList<MortalityData> mortalityData = getRecordingMortality(ID, hatchDate);
            setRecordingMortality(mortalityData);

            //set note
            setNote(ID);
        }
        
    }

    public void CheckBeforeSubmit(){
        String notif = "";
        if (getListMortality().size()!=0){
            ArrayList<MortalityData> MortalityData = getListMortality();
            for (int b =0; b< MortalityData.size(); b++){
                MortalityData mortalityData = MortalityData.get(b);
                String hatchdate = mortalityData.getHatch_date();
                int bird_qty = help.getTotal(flock_id,recordingDate,hatchdate);
                int sum =0;
                for (int a = 0; a<mortalityData.getMortality().size(); a++){
                    Mortality mor = MortalityData.get(b).getMortality().get(a);
                    String value = mor.getValue();
                    int qty= Integer.parseInt(value);
                    sum += qty;
                }
                int total = bird_qty - help.getBirdTransferOutToday(flock_id,recordingDate,hatchdate)- help.getSpentHenToday(flock_id,recordingDate,hatchdate)
                        - sum;
                if (total < 0){
                    notif += "["+hatchdate+"] melebihi kapasitas ayam \n";
                }
            }
        }
        if (notif != ""){
            AskOption(notif);
        }
        else{
            Submit();
        }
    }
    public void Submit(){

        insert(Long.parseLong(id_daily_recording));
        //long ID = dataSource.getRecordingID(flock_id, recordingDate);

        //jika ID = 0, dan status new berarti recording baru pertama kali di record
   /*     if (ID == 0) {
            //if (status.equals("new") || status.equals("update")) {
            //if (status.equals("new")) {
            // if (ID == 0) {
            Log.d("zzzs", "baru pertama kali recording, New");
            //jika index masih 0 berarti belum pernah melakukan recording sehingga perlu untuk membuat row recording terlebih dahulu
            dataSource.create_recording(recordingDate, flock_id);
            //jika sudah di record, maka ambil index nya untuk melakukan recording di menu lainnya
            long id = dataSource.getRecordingID(flock_id, recordingDate);
            ID = id;
            insert(id);
            *//*    } else {
                    Log.d("zzzs", "menu lain sudah melakukan recording, tapi state masih new berarti belum pernah recording. Update");
                    //bisa jadi kemungkinan adalah dia melakukan edit kembali
                    //1. menghapus semua data yang ada di dalam tabel,
                    dataSource.delete_from_recording_entry(ID);
                    insert(ID);
                }
            }*//*
            //jika sudah ada yg recording terlebih dahulu dari menu yg lain
           *//* else {
                Log.d("zzzs", "Update menu recording");
                //jika sudah pernah melakukan recording, maka data2 yang yg sebelumnya akan dihapus dan di update dengan data yang baru
                dataSource.delete_from_recording_entry(ID);
                insert(ID);
            }*//*
            //dataSource.update_New_cache_hatch_date(flock_id, recordingDate);
            //dataSource.count_totalBird_flock(flock_id, recordingDate, ID);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate =mdformat.format(calendar.getTime());

            //dataSource.createLastRecording(flock_id, strDate);
           // dataSource.createLastRecording(flock_id, recordingDate);
        }
        //status edit
        else {
            Log.d("zzzs", "Edit menu recording");
            //jika sudah pernah melakukan recording, maka data2 yang yg sebelumnya akan dihapus dan di update dengan data yang baru
            dataSource.delete_from_recording_entry(ID);
            insert(ID);
            dataSource.count_totalBird_flock(flock_id,recordingDate,ID);
            for (int i =0; i< hatchDate.size();i++){
                dataSource.update_edit_cache_total_bird(flock_id,recordingDate,hatchDate.get(i).getHatchDate());
            }
        }*/

        SubmitDialog();
    }

    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage(notif)
                .setIcon(R.drawable.camera)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Recording Success")
                .setMessage("Recording berhasil ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }

    public void AddMoreFeed() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View feedView = inflater.inflate(R.layout.more_feed, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        Spinner feed_type = (Spinner)(feedView.findViewById(R.id.feed_type));
        feed_type.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, feed_value));
        feed_type.setSelected(true);
        more_feed.addView(feedView,more_feed.getChildCount()-2);
    }
    public void AddMoreMedicine(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View medicineView = inflater.inflate(R.layout.more_medicine,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
        medication_name.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, ovk_value));
        medication_name.setSelected(true);
        more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
    }

    public void ViewMortality(String hatch, String strain){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mortalityInf = inflater.inflate(R.layout.mortality_un, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView Hatch = (TextView) mortalityInf.findViewById(R.id.hatch_date);
        Hatch.setText("Hatching "+ hatch +"| "+strain);
        mortalityItemsView = (ViewGroup) mortalityInf.findViewById(R.id.mortality_items);
        //membuat nama textview di mortalitycategory
        for (int i =0; i<mortalityCategory.size(); i++){
            ViewMortalityItems(mortalityCategory.get(i));
        }
        mortalityView.addView(mortalityInf);
    }
    public void ViewMortalityItems(String name){
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mortalityInf = inflater.inflate(R.layout.mortality_items_un, null);

        TextView mortality_name = (TextView) (mortalityInf.findViewById(R.id.mortality_name));
        mortality_name.setText(name);
        mortalityItemsView.addView(mortalityInf);
    }
    public void ViewFeed(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View feedView = inflater.inflate(R.layout.more_feed_un, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        Spinner feed_type = (Spinner)(feedView.findViewById(R.id.feed_type));
        feed_type.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, feed_value));
        feed_type.setSelected(true);
        more_feed.addView(feedView,more_feed.getChildCount()-2);
    }
    public void ViewMedicine(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View medicineView = inflater.inflate(R.layout.more_medicine_un,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
        medication_name.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, ovk_value));
        medication_name.setSelected(true);
        more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
    }
    public void ViewEggsValue(String name){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View eggsInf = inflater.inflate(R.layout.egg_quality_view_un, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView eggsName = (TextView) eggsInf.findViewById(R.id.eggs_name);
        TextView weightName = (TextView) eggsInf.findViewById(R.id.weight_name);
        eggsName.setText(name+" (eggs)");
        weightName.setText(name+" (kg)");
        eggsView.addView(eggsInf);
    }
    public void ViewHouseProfile(String hatchDate, String strain){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View house = inflater.inflate(R.layout.house_profile_un, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        int total = help.getTotal(flock_id,recordingDate,hatchDate);
        Log.d("zzzs","total = "+total);
        String age = help.getBirdAgeByHatchDate(hatchDate,recordingDate);
        TextView hatch = (TextView) house.findViewById(R.id.hatching);
        TextView total_bird = (TextView) house.findViewById(R.id.total_bird);
        TextView bird_age = (TextView) house.findViewById(R.id.bird_age);
        hatch.setText("["+hatchDate+"]");
        total_bird.setText("Total Bird Hatching | "+strain+": "+total);
        bird_age.setText("Age Bird Hatching | "+strain+": "+age);
        profileView.addView(house);
    }

    //untuk mendapatkan data dari eggs box
    public ArrayList<EggValue> getListEggs() {
        int childCount = eggsView.getChildCount();
        //Log.d("hasil", "child_count : "+childCount);
        ListEggs = new ArrayList<>();
        String showallPrompt = "";
        showallPrompt += "chilcCount :" +childCount+"\n\n";
        for (int c =0; c<childCount;c++){
            EggValue eggs = new EggValue();
            View childView = eggsView.getChildAt(c);
            TextView txname = (TextView)(childView.findViewById(R.id.eggs_name));
            String egg_name = txname.getText().toString();
            String[] name = egg_name.split(" \\(eggs");
            EditText qty_eggs = (EditText)(childView.findViewById(R.id.quality_eggs));
            String total_eggs = (String)(qty_eggs.getText().toString());
            EditText qty_weight = (EditText) (childView.findViewById(R.id.quality_weight));
            String weight = (String)(qty_weight.getText().toString());
            //Log.d("zzzs","stat not empt "+(!total_eggs.isEmpty()));
            //Log.d("zzzs","stat not kosong "+(total_eggs != ""));
            //Log.d("zzzs","stat not null "+(total_eggs != null));
            if (!total_eggs.isEmpty() || !weight.isEmpty()) {
                eggs.setName(name[0]);
                eggs.setValue_eggs(total_eggs);
                eggs.setValue_weight(weight);
                showallPrompt += c + " : " + name[0] + " - " + total_eggs + " - " + weight + " \n";
                ListEggs.add(eggs);
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListEggs;
    }

    //untuk mendapatkan data dari feed box
    public ArrayList<Feed> getListFeed() {
        ListFeed = new ArrayList<>();
        String showallPrompt = "";
        int childCount = more_feed.getChildCount()-2;
        showallPrompt += "chilcCount :" +childCount+"\n\n";
        for (int c =0; c<childCount;c++){
            Feed feed = new Feed();
            View childView = more_feed.getChildAt(c);
            EditText total_feed = (EditText)(childView.findViewById(R.id.total_feed));
            String total = (String)(total_feed.getText().toString());
            Spinner feed_type = (Spinner)(childView.findViewById(R.id.feed_type));
            String type = feed_type.getSelectedItem().toString();
            if(!total.isEmpty()){
                feed.setName(type);
                feed.setValue(total);
                showallPrompt += c + ":"+type+" - "+total+" \n";
                ListFeed.add(feed);
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListFeed;
    }

    //untuk mendapatkan data dari box medicine
    public ArrayList<Medicine> getListMedicine() {
        ListMedicine = new ArrayList<>();
        String showallPrompt = "";
        int childCount = more_medicine.getChildCount()-2;
        showallPrompt += "chilcCount :" +childCount+"\n\n";
        for (int c =0; c<childCount;c++){
            Medicine medicine = new Medicine();
            View childView = more_medicine.getChildAt(c);
            EditText total_medicine = (EditText)(childView.findViewById(R.id.total_medicine));
            String total = (String)(total_medicine.getText().toString());
            Spinner medication_name = (Spinner)(childView.findViewById(R.id.medication_name));
            String name = medication_name.getSelectedItem().toString();
            if (!total.isEmpty()){
                medicine.setName(name);
                medicine.setValue(total);
                showallPrompt += c + ":"+name+" - "+total+" \n";
                ListMedicine.add(medicine);
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListMedicine;
    }

    //untuk mendapatkan data dari mortality box
    public ArrayList<MortalityData> getListMortality() {
        ArrayList<MortalityData> ListMortality = new ArrayList<>();
        String showallPrompt = "";
        int childCount = mortalityView.getChildCount();
        Log.d("hasil", "count mortality: "+childCount);
        for (int c =0; c<childCount;c++){
            View childView = mortalityView.getChildAt(c);
            ArrayList<Mortality> List = new ArrayList<>();
            TextView hatch = (TextView) (childView.findViewById(R.id.hatch_date));
            String[] split_txt = hatch.getText().toString().split(" ");
            String[] hatch_date = split_txt[1].split("\\|");
            Log.d("hasil","hatch "+hatch_date[0]);
            int mortalitychild = mortalityItemsView.getChildCount();
            Log.d("hasil","items "+mortalitychild);
            ViewGroup child = childView.findViewById(R.id.mortality_items);
            for (int i =0; i< child.getChildCount(); i++){
                View childMortality = child.getChildAt(i);
                Mortality mor = new Mortality();
                TextView txMortality = (TextView) (childMortality.findViewById(R.id.mortality_name));
                EditText edMortality = (EditText) (childMortality.findViewById(R.id.mortality_value));
                Log.d("hasil","edmortality "+edMortality.getText().toString());
                Log.d("hasil","txmortality "+txMortality.getText().toString());
                //jika edit text tidak kosong
                if (!edMortality.getText().toString().matches("")) {
                    mor.setValue(edMortality.getText().toString());
                    mor.setName(txMortality.getText().toString());
                    showallPrompt += i + ":" + edMortality.getText().toString() + " - " + txMortality.getText().toString() + " \n";
                    Log.d("hasil", txMortality.getText().toString()+" : "+edMortality.getText().toString());
                    List.add(mor);
                }
            }
            if(List.size()!=0){
                MortalityData mortData = new MortalityData();
                mortData.setHatch_date(hatch_date[0]);
                mortData.setMortality(List);
                ListMortality.add(mortData);
                Log.d("hasil", "add to list");
            }
        }
        for (int b =0; b< ListMortality.size(); b++){
            Log.d("hasil", "hatch_date: "+ListMortality.get(b).getHatch_date());
            for (int a = 0; a<ListMortality.get(b).getMortality().size(); a++){
                Mortality mor = ListMortality.get(b).getMortality().get(a);
                Log.d("hasil", "name: "+mor.getName());
                Log.d("hasil", "value"+mor.getValue());
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListMortality;
    }


    public void  setNote(long id){
        SQLiteDatabase database;
        database= db.getWritableDatabase();
        String selectQuery= "SELECT * FROM daily_recording WHERE id = '"+id+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String note = "";
        if(cursor.moveToFirst()) {
            note = cursor.getString(cursor.getColumnIndex("note"));
        }
        cursor.close();
        edNote.setText(note);

    }
    //mendapatkan produksi telur yang telah di recording sebelumnya
    public ArrayList<EggValue> getEggProduction(long recording_id, ArrayList<String> eggs){
        ArrayList<EggValue> value = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        for (int i =0; i < eggs.size(); i++){
            EggValue eggValue = new EggValue();
            String selectQuery= "SELECT * FROM recording_production INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id \n" +
                    "where daily_recording_id = '"+recording_id+"' and name = '"+eggs.get(i)+"'";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String egg_number = "";
            String egg_weight = "";
            long ID = 0;
            if(cursor.moveToFirst()) {
                ID = cursor.getLong(cursor.getColumnIndex("id"));
                egg_number = cursor.getString(cursor.getColumnIndex("egg_number"));
                egg_weight = cursor.getString(cursor.getColumnIndex("egg_weight"));
                Log.d("zzzs","dicatat masuk if");
            }
            Log.d("zzzs", eggs.get(i)+" = "+ egg_number);
            eggValue.setId(ID);
            eggValue.setName(eggs.get(i));
            eggValue.setValue_eggs(egg_number);
            eggValue.setValue_weight(egg_weight);
            value.add(eggValue);
            cursor.close();
        }
        return value;
    }
    //set nilai kedalam edit text produksi telur
    public void setEggProduction(ArrayList<EggValue> eggs){
        int childCount = eggsView.getChildCount();
        for (int c =0; c<childCount;c++){
            View childView = eggsView.getChildAt(c);
            EditText qty_eggs = (EditText)(childView.findViewById(R.id.quality_eggs));
            EditText qty_weight = (EditText) (childView.findViewById(R.id.quality_weight));
            qty_eggs.setText(eggs.get(c).getValue_eggs());
            qty_weight.setText(eggs.get(c).getValue_weight());
        }
    }

    //mendapatkan pakan digunakan yang telah di recording
    public ArrayList<Feed> getFeedConsumption(long recording_id){
        ArrayList<Feed> feeds = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM recording_feed_consumption INNER JOIN feed on recording_feed_consumption.feed_id = feed.id \n" +
                "WHERE daily_recording_id = '"+recording_id+"'";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String name = "";
        String amount = "";
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            Feed pakan = new Feed();
            name = cursor.getString(cursor.getColumnIndex("name"));
            amount = cursor.getString(cursor.getColumnIndex("feed_amount"));
            pakan.setName(name);
            pakan.setValue(amount);
            feeds.add(pakan);
        }
        return feeds;
    }
    //set nilai kedalam edit text konsumsi pakan
    public void setFeedConstumption(ArrayList<Feed> feeds){
        for (int i =0; i< feeds.size(); i++){
            LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View feedView = inflater.inflate(R.layout.more_feed, null);
            // Add the new row before the add field button.
            //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
            Spinner feed_type = (Spinner)(feedView.findViewById(R.id.feed_type));
            EditText amount = (EditText)(feedView.findViewById(R.id.total_feed));
            ArrayAdapter myAdapt = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, feed_value);
            feed_type.setAdapter(myAdapt);
            feed_type.setSelected(true);
            int position =myAdapt.getPosition(feeds.get(i).getName());
            feed_type.setSelection(position);
            amount.setText(feeds.get(i).getValue());
            more_feed.addView(feedView,more_feed.getChildCount()-2);
        }
    }

    //mendapatkan ovk yang telah digunakan di recording
    public ArrayList<Medicine> getOVKConsumption(long recording_id){
        ArrayList<Medicine> medicines = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM recording_medication_vaccination INNER JOIN medication_vaccination \n" +
                "on medication_vaccination.id = recording_medication_vaccination.medication_vaccination_id \n" +
                "WHERE daily_recording_id = '"+recording_id+"'";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String name = "";
        String amount = "";
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            Medicine obat = new Medicine();
            name = cursor.getString(cursor.getColumnIndex("product_name"));
            amount = cursor.getString(cursor.getColumnIndex("amount_used"));
            obat.setName(name);
            obat.setValue(amount);
            medicines.add(obat);
        }
        return medicines;
    }
    //set nilai kedalam edit text penggunaan obat2an
    public void setOVKConsumption(ArrayList<Medicine> ovk){
        for (int i =0; i< ovk.size(); i++){
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View medicineView = inflater.inflate(R.layout.more_medicine,null);
            // Add the new row before the add field button.
            //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
            Spinner medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
            EditText total_medicine = (EditText)(medicineView.findViewById(R.id.total_medicine));
            ArrayAdapter myAdapt = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, ovk_value);
            medication_name.setAdapter(myAdapt);
            medication_name.setSelected(true);
            int position =myAdapt.getPosition(ovk.get(i).getName());
            medication_name.setSelection(position);
            total_medicine.setText(ovk.get(i).getValue());
            more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
        }
    }

    //mendapatkan nilai mortality untuk masing2 hatch date
    public ArrayList<MortalityData> getRecordingMortality(long recording_id, ArrayList<HatchDate> hatch){
        ArrayList<MortalityData> mortalityData = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        for (int a =0; a < hatch.size(); a++){
            MortalityData data = new MortalityData();
            data.setHatch_date(hatch.get(a).getHatchDate());
            String selectQuery= "SELECT * FROM recording_mortality INNER JOIN chick_in on recording_mortality.chick_in_id = chick_in.id INNER JOIN\n" +
                    "mortality_category on recording_mortality.mortality_category_id = mortality_category.id\n" +
                    "WHERE recording_mortality.daily_recording_id = '"+recording_id+"' AND chick_in.hatch_date = '"+hatch.get(a).getHatchDate()+"'";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String name = "";
            String amount = "";
            ArrayList<Mortality> mortalities = new ArrayList<>();
            for (int i=0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Mortality mor = new Mortality();
                name = cursor.getString(cursor.getColumnIndex("name"));
                amount = cursor.getString(cursor.getColumnIndex("number_of_bird"));
                mor.setName(name);
                mor.setValue(amount);
                mortalities.add(mor);
            }
            data.setMortality(mortalities);
            mortalityData.add(data);
        }
        return mortalityData;
    }
    //set nilai kedalam edit text kematian
    public void setRecordingMortality(ArrayList<MortalityData> mortality){
        Log.d("zzzs","mortality.size() = "+mortality.size());
        int childCount = mortalityView.getChildCount();
        Log.d("zzzs","childCount = "+childCount);
        //ArrayList<Mortality> mortality_items = mortality.get(2).getMortality();
        //Log.d("zzzs","mortality_items.size() = "+mortality_items.size());
        for (int c =0; c<childCount;c++){
            View childView = mortalityView.getChildAt(c);
            TextView hatch = (TextView) (childView.findViewById(R.id.hatch_date));
            String[] split_txt = hatch.getText().toString().split(" ");
            String[] hatch_date = split_txt[1].split("\\|");
            int mortalitychild = mortalityItemsView.getChildCount();
            ViewGroup child = childView.findViewById(R.id.mortality_items);
            ArrayList<Mortality> mortality_items = mortality.get(c).getMortality();
            Log.d("zzzs","mortality_items.size() = "+mortality_items.size());

            for (int i =0; i< child.getChildCount(); i++){
                for (int a = 0; a<mortality_items.size(); a++){
                    View childMortality = child.getChildAt(i);
                    String name = mortality_items.get(a).getName();
                    String amount = mortality_items.get(a).getValue();
                    TextView txMortality = (TextView) (childMortality.findViewById(R.id.mortality_name));
                    EditText edMortality = (EditText) (childMortality.findViewById(R.id.mortality_value));
                    if (txMortality.getText().toString().equals(name)){
                        edMortality.setText(amount);
                    }
                }
            }
        }
    }
/*    public void insert(long id){
        String note = null;
        ArrayList<EggValue> list_egg = getListEggs();
        ArrayList<MortalityData> list_mortality = getListMortality();
        ArrayList<Feed> list_feed = getListFeed();
        ArrayList<Medicine> list_medicine = getListMedicine();
        if(edNote.getText()!=null){
            //memasukkan kedalam group recording
            note = edNote.getText().toString();
        }
        if (!note.isEmpty()) {
            dataSource.create_note(id, note);
        }
        if (hatchDate.size() != 0){
            dataSource.createDaily_recording_chick_in(hatchDate,id);
        }
        if (list_feed.size()!=0){
            dataSource.createFeed(list_feed,id);
        }
        if (list_medicine.size()!=0){
            dataSource.createMedicine(list_medicine,id);
        }
        if (list_egg.size()!=0){
            dataSource.createEggValue(list_egg,id);
        }
        if (list_mortality.size()!=0){
            dataSource.createMortality(list_mortality, id);
        }
        //membuat di table cache
        for (int i =0; i< hatchDate.size(); i++){
            dataSource.create_cache_total_bird(flock_id,recordingDate,id,hatchDate.get(i).getHatchDate());
        }
        dataSource.createCacheMortality(id);
    }*/

    public void insert(long id_daily_recording){
        String note = null;
        ArrayList<EggValue> list_egg = getListEggs();
        ArrayList<MortalityData> list_mortality = getListMortality();
        ArrayList<Feed> list_feed = getListFeed();
        ArrayList<Medicine> list_medicine = getListMedicine();


/*        if(edNote.getText()!=null){
            //memasukkan kedalam group recording
            note = edNote.getText().toString();
        }
        if (!note.isEmpty()) {
            dataSource.create_note(id_daily_recording, note);
        }
        if (hatchDate.size() != 0){
            dataSource.createDaily_recording_chick_in(hatchDate,id_daily_recording);
        }*/
        if (list_feed.size()!=0){
            updateFeed(list_feed,id_daily_recording);
        }
        if (list_medicine.size()!=0){
            updateMedicine(list_medicine,id_daily_recording);
        }
          if (list_egg.size()!=0){
            updateEggValue(list_egg,id_daily_recording);
        }
        if (list_mortality.size()!=0){
            updateMortality(list_mortality, id_daily_recording);
        }
        //membuat di table cache
        for (int i =0; i< hatchDate.size(); i++){
            update_cache_total_bird(flock_id,recordingDate,id_daily_recording,hatchDate.get(i).getHatchDate());
        }
        updateCacheMortality(id_daily_recording);


    }

    public void updateDaily_recording_chick_in(ArrayList<HatchDate> hatch, long id){
        SQLiteDatabase database = db.getReadableDatabase();
        for (int i =0; i< hatch.size(); i++){

            //Get chick_in id
            String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch.get(i).getHatchDate() + "' LIMIT 1";
            Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
            String chick_in_ID = "";
            if(cursorChickIn.moveToFirst())
                chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
            cursorChickIn.close();

            //RECORDING chick_in
            ContentValues recording_production = new ContentValues();
            recording_production.put("daily_recording_id", id);
            recording_production.put("chick_in_id", chick_in_ID);
            long insertFeed = database.insert("daily_recording_chick_in", null, recording_production);
        }
    }

    public void updateFeed(ArrayList<Feed> Feed, long id_daily_recording){
        SQLiteDatabase database = db.getReadableDatabase();
        for (int c =0; c<Feed.size();c++){
           Feed feed = Feed.get(c);
            String name = feed.getName();
            String value = feed.getValue();

            //Get feed id
            String selectQueryFeed= "SELECT id FROM feed WHERE name = '"+ name + "' LIMIT 1";
            Cursor cursorFeed = database.rawQuery(selectQueryFeed, null);
            String FeedID = "";
            if(cursorFeed.moveToFirst())
                FeedID  =  cursorFeed.getString(cursorFeed.getColumnIndex("id"));
            cursorFeed.close();

            //RECORDING FEED
            ContentValues recording_feed = new ContentValues();
            //recording_feed.put("daily_recording_id", id);
            //recording_feed.put("feed_id", FeedID);
            recording_feed.put("feed_amount", value);
            //long insertFeed = database.insert("recording_feed_consumption", null, recording_feed);
            database.update("recording_feed_consumption", recording_feed, "daily_recording_id = '" + id_daily_recording + "' and feed_id = '" + FeedID +"'",null);
        }
    }

    public void updateMedicine(ArrayList<Medicine> Medicine, long id_daily_recording) {
        SQLiteDatabase database = db.getReadableDatabase();
        for (int c = 0; c < Medicine.size(); c++) {
            Medicine medicine = Medicine.get(c);
            String name = medicine.getName();
            String value = medicine.getValue();

            //Get Medicine vaccination id
            String selectQueryMV = "SELECT id FROM medication_vaccination WHERE product_name = '" + name + "' LIMIT 1";
            Cursor cursorMV = database.rawQuery(selectQueryMV, null);
            String MVID = "";
            if (cursorMV.moveToFirst())
                MVID = cursorMV.getString(cursorMV.getColumnIndex("id"));
            cursorMV.close();

            //RECORDING MedicineVaccination
            ContentValues recording_medicine = new ContentValues();
            //recording_medicine.put("daily_recording_id", id);
            //recording_medicine.put("medication_vaccination_id", MVID);
            recording_medicine.put("amount_used", value);
            //long insertFeed = database.insert("recording_medication_vaccination", null, recording_medicine);
            database.update("recording_medication_vaccination", recording_medicine, "daily_recording_id = '" + id_daily_recording + "' and medication_vaccination_id = '" + MVID + "'", null);

        }
    }

        public void updateEggValue(ArrayList<EggValue> eggValues, long id_daily_recording){
            SQLiteDatabase database = db.getReadableDatabase();
            for (int i =0; i<eggValues.size(); i++){
                EggValue eggs = eggValues.get(i);
                String name = eggs.getName();
                String value_eggs = eggs.getValue_eggs();
                String value_weight = eggs.getValue_weight();
                //Log.d("zzzs","egg quality_name: "+name+"hola");
                //Get egg_quality id
                String selectQueryEggQuality= "SELECT id FROM egg_quality WHERE name = '"+ name + "' LIMIT 1";
                Cursor cursorEggQuality = database.rawQuery(selectQueryEggQuality, null);
                String egg_quality_ID = "";
                if(cursorEggQuality.moveToFirst())
                    egg_quality_ID  =  cursorEggQuality.getString(cursorEggQuality.getColumnIndex("id"));
                cursorEggQuality.close();
                //Log.d("zzzs","id quality: "+egg_quality_ID);

                //RECORDING Production
                ContentValues recording_production = new ContentValues();
                //recording_production.put("daily_recording_id", id);
                //recording_production.put("egg_quality_id", egg_quality_ID);
                recording_production.put("egg_number", value_eggs);
                recording_production.put("egg_weight", value_weight);
                //long insertFeed = database.insert("recording_production", null, recording_production);
                database.update("recording_production", recording_production, "daily_recording_id = '" + id_daily_recording + "' and egg_quality_id = '" + egg_quality_ID + "'", null);

            }
        }

    public void updateMortality(ArrayList<MortalityData> MortalityData , long id_daily_recording){
        SQLiteDatabase database = db.getReadableDatabase();
        int total = 0;
        for (int b =0; b< MortalityData.size(); b++){
            MortalityData mortalityData = MortalityData.get(b);
            String hatch_date = mortalityData.getHatch_date();
            Log.d("mor","hatch date ="+hatch_date);

            //Get chick_in id
            String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch_date + "' LIMIT 1";
            Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
            String chick_in_ID = "";
            if(cursorChickIn.moveToFirst())
                chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
            cursorChickIn.close();
            Log.d("mor","chick_in_ID ="+chick_in_ID);

            for (int a = 0; a<mortalityData.getMortality().size(); a++){
                Mortality mor = MortalityData.get(b).getMortality().get(a);
                String name = mor.getName();
                String value = mor.getValue();
                total = Integer.parseInt(value)+total;
                //Get mortality_category id
                String selectQueryMortalityCategory= "SELECT id FROM mortality_category WHERE name = '"+ name + "' LIMIT 1";
                Cursor cursorMortalityCategory = database.rawQuery(selectQueryMortalityCategory, null);
                String MortalityCategory_ID = "";
                if(cursorMortalityCategory.moveToFirst())
                    MortalityCategory_ID  =  cursorMortalityCategory.getString(cursorMortalityCategory.getColumnIndex("id"));
                cursorMortalityCategory.close();

                Log.d("mor","MortalityCategory_ID ="+MortalityCategory_ID);
                Log.d("mor","value ="+MortalityCategory_ID);


                //RECORDING Mortality
                ContentValues recording_mortality = new ContentValues();
                //recording_mortality.put("daily_recording_id", id);
                recording_mortality.put("chick_in_id", chick_in_ID);
                //recording_mortality.put("mortality_category_id", MortalityCategory_ID);
                recording_mortality.put("number_of_bird", value);
                //long insertMortality = database.insert("recording_mortality", null, recording_mortality);
                database.update("recording_mortality", recording_mortality, "daily_recording_id = '" + id_daily_recording + "' and mortality_category_id ='"+MortalityCategory_ID + "'", null);
            }
        }
    }

    public void updateCacheMortality(long id_daily_recording){
        Log.d("id","id = "+id_daily_recording);
        SQLiteDatabase database = db.getReadableDatabase();
        String value = "";
        //mendapatkan jumlah mortality dari recording id
        String selectQueryMortality= "SELECT * FROM recording_mortality WHERE daily_recording_id = '"+ id_daily_recording + "'";
        Cursor cursorMortality = database.rawQuery(selectQueryMortality, null);
        int total =0;
        for (int i = 0; i<cursorMortality.getCount(); i++){
            cursorMortality.moveToPosition(i);
            total= total+ cursorMortality.getInt(cursorMortality.getColumnIndex("number_of_bird"));
        }
        value += "total_"+total+"/";

        Cursor cursor = database.rawQuery("SELECT * FROM mortality_category",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            long mortalityID = cursor.getInt(cursor.getColumnIndex("id"));
            String selectMortalityCategory = "SELECT * FROM recording_mortality WHERE daily_recording_id = '" + id_daily_recording + "' AND mortality_category_id = '" + mortalityID + "'";
            Cursor cursorMortalityCategory = database.rawQuery(selectMortalityCategory, null);
            int nilai = 0;
            for (int j = 0; j < cursorMortalityCategory.getCount(); j++) {
                cursorMortalityCategory.moveToPosition(j);
                nilai = nilai + cursorMortalityCategory.getInt(cursorMortalityCategory.getColumnIndex("number_of_bird"));
            }
            value += mortalityID + "_" + nilai + "/";
        }
        Log.d("zzzs","nilai = "+value);
/*
        //cek apakah cacahe dengan tanggal recording dan flock ini sudah ada di table cache_mortality?
        String selectQueryCacheMortality= "SELECT id FROM cache_mortality WHERE daily_recording_id = '"+ id + "' LIMIT 1";
        Cursor cursorCacheMortality = database.rawQuery(selectQueryCacheMortality, null);
        long ID = 0;
        if(cursorCacheMortality.moveToFirst())
            ID  =  cursorCacheMortality.getLong(cursorCacheMortality.getColumnIndex("id"));
        cursorCacheMortality.close();
        //jika ID =0, berarti belum ada di cache
        if (ID != 0){*/
            ContentValues cache_mortality = new ContentValues();
            //daily_values.put("note", note);
            //cache_mortality.put("daily_recording_id", id);
            cache_mortality.put("value", value);

            //mengeksekusi perintah sql insert data
            //yang mengembalikan sebuah insert ID
            database.update("cache_mortality",cache_mortality,"daily_recording_id = '" + id_daily_recording +"'", null);
       // }
    }

    public void update_cache_total_bird(String flockID, String dateRecording, long id_daily_recording, String hatch_date){
        SQLiteDatabase database = db.getReadableDatabase();
        Log.d("y","flockID = "+flockID);
        Log.d("y","dateRecording = "+dateRecording);
        Log.d("y","id_daily_recording = "+id_daily_recording);
        Log.d("y","hatch_date = "+hatch_date);

        //get last recording
        String selectQuerylastRecording= "SELECT recording_date FROM daily_recording\n" +
                "ORDER BY recording_date DESC LIMIT 1";
        Cursor cursorlastRecording = database.rawQuery(selectQuerylastRecording, null);
        String lastRecording ="";
        for (int i = 0; i<cursorlastRecording.getCount(); i++){
            cursorlastRecording.moveToPosition(i);
            lastRecording = cursorlastRecording.getString(0);
        }

        String selectQueryMortality= "SELECT * FROM recording_mortality WHERE daily_recording_id = '"+ id_daily_recording + "'";
        Cursor cursorMortality = database.rawQuery(selectQueryMortality, null);
        int mortality =0;
        for (int i = 0; i<cursorMortality.getCount(); i++){
            cursorMortality.moveToPosition(i);
            mortality= mortality+ cursorMortality.getInt(cursorMortality.getColumnIndex("number_of_bird"));
        }

        Log.d("y","mortality = "+mortality);

        //GET TOTAL BIRD BEFORE
        String selectQueryBird= "SELECT daily_recording.recording_date, cache_total_bird.total_bird FROM daily_recording\n" +
                "INNER JOIN cache_total_bird ON cache_total_bird.daily_recording_id = daily_recording.ID\n" +
                "WHERE daily_recording.recording_date < '"+lastRecording+"' ORDER BY daily_recording.recording_date DESC LIMIT 1";
        Cursor cursorBird = database.rawQuery(selectQueryBird, null);
        int bird =0;
        for (int i = 0; i<cursorBird.getCount(); i++){
            cursorBird.moveToPosition(i);
            bird = cursorBird.getInt(cursorBird.getColumnIndex("total_bird"));
        }
        Log.d("y","bird = "+bird);
        int total_bird = bird - mortality;

/*        SQLiteDatabase database = db.getReadableDatabase();
        int transfer_in = help.getBirdTransferInToday(flockID,dateRecording,hatch_date);
        int transfer_out = help.getBirdTransferOutToday(flockID, dateRecording, hatch_date);
        int mortality = help.getMortalityToday(flockID, dateRecording, hatch_date);
        int spent_hen = help.getSpentHenToday(flockID, dateRecording, hatch_date);

       //total adalah pengurangan antara jumlah ayam-transfer out-mortality
        int total = dataSource.count_total_bird_by_hatch_date(flockID,dateRecording,hatch_date);
*//*
        //mendapatkan chick_in ID
        long chick_inID = getChickInID(hatch_date);
        *//**//*
        Log.d("zzzs","recording date = "+dateRecording);
        Log.d("zzzs","recording id = "+daily_recordingID);
        Log.d("zzzs","hatch date = "+hatch_date);
        *//**//*
        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT cache_total_bird.id FROM cache_total_bird INNER JOIN chick_in on chick_in.id = cache_total_bird.chick_in_id " +
                "WHERE daily_recording_id = '"+daily_recordingID+"' and hatch_date= '"+hatch_date+"' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        Log.d("zzzs","ID = "+ID);
        //jika belum ada recording untuk recording ID dan hatch date tersebut maka dilakukan insert
        if (ID.isEmpty()){
            Log.d("zzzs","insert cache_total_bird");
            ContentValues cache_total_bird = new ContentValues();
            cache_total_bird.put("daily_recording_id", daily_recordingID);
            cache_total_bird.put("chick_in_id", chick_inID);
            cache_total_bird.put("transfer_in", transfer_in);
            cache_total_bird.put("transfer_out", transfer_out);
            cache_total_bird.put("mortality", mortality);
            cache_total_bird.put("chick_out", spent_hen);
            cache_total_bird.put("total_bird", total);
            long insertCache = database.insert("cache_total_bird", null, cache_total_bird);
        }
        //jika sudah pernah melakukan recording
        else {*/

        //Get chick_in id
        String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch_date + "' LIMIT 1";
        Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
        String chick_in_ID = "";
        if(cursorChickIn.moveToFirst())
            chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
        cursorChickIn.close();

            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "daily_recording_id =" + id_daily_recording;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            //update.put("daily_recording_id", id_daily_recording);
            update.put("chick_in_id", chick_in_ID);
            update.put("mortality", mortality);
            update.put("total_bird", total_bird);
            //update query
            database.update("cache_total_bird", update, Filter, null);

    }

    public void update_cache_total_bird_NEW(long id_daily_recording, String hatch_date){

        SQLiteDatabase database = db.getReadableDatabase();
        //Get chick_in id
        String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch_date + "' LIMIT 1";
        Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
        String chick_in_ID = "";
        if(cursorChickIn.moveToFirst())
            chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
        cursorChickIn.close();

        Log.d("zzzs","update cache_total_bird");
        Log.d("chick_in_ID","chick_in_ID");
        /*//ambil id
        String Filter = "daily_recording_id =" + id_daily_recording;
        //memasukkan ke content values
        ContentValues update = new ContentValues();
        //masukkan data sesuai dengan kolom pada database
        //update.put("daily_recording_id", id_daily_recording);
        update.put("chick_in_id", chick_in_ID);
        update.put("total_bird", total);
        //update query
        database.update("cache_total_bird", update, Filter, null);*/

    }
}
