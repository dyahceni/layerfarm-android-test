package com.layerfarm.unregistered.recordingUnregistered;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.layerfarm.unregistered.helperUnregistered;
import com.layerfarm.unregistered.adapterUnregistered.UnFlockAdapter;
import com.layerfarm.unregistered.DBDataSourceUnregistered;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.unregistered.R;

import java.util.ArrayList;

public class UnregisteredRecordingMainActivity extends AppCompatActivity {

    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSourceUnregistered dataSource;
    ArrayList <Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    helperUnregistered help;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, recordingDate;
    long ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unregistered_recording_main);

        help = new helperUnregistered(this);
        help.open();

        dataSource =  new DBDataSourceUnregistered(this);
        dataSource.open();
        db = new DatabaseHelper(this);
        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
        spn_location.setSelected(true);
       /* spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Submit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        rvFlock = (RecyclerView) findViewById(R.id.rv_flock);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(com.layerfarm.setting.R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String location = spn_location.getSelectedItem().toString();
                //mendapatkan id dari location tersebut
                //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
                //dan hanya memberikan 1 buah output dengan index 0
                String loc_id = dataSource.getLocation(location).get(0);

                //mencari flock dengan id location tersebut
                flock_items = new ArrayList();
                SQLiteDatabase dbase = db.getReadableDatabase();
                Cursor cursor= null;
                cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
                cursor.moveToFirst();
                for (int i=0; i < cursor.getCount(); i++){
                    cursor.moveToPosition(i);

                    flock_id  = cursor.getString(cursor.getColumnIndex("id"));
                    flock_name = cursor.getString(cursor.getColumnIndex("name"));
                    location_id = cursor.getString(cursor.getColumnIndex("location_id"));
                    flock_type =cursor.getString(cursor.getColumnIndex("type"));
                    flock_period = cursor.getString(cursor.getColumnIndex("period"));
                    status = cursor.getString(cursor.getColumnIndex("status"));
                    location_name = location;
                }

                recordingDate = help.getStartRecordingDate(flock_id);
                //Log.d("recordingDatezzzz", "" + recordingDate);
                ID = dataSource.getRecordingID(flock_id, recordingDate);
                //Log.d("IDzzzz", "" + ID);

                if (recordingDate != null && ID == 0){
                        status = "new";
                    } else{
                        status = "update";
                     }
                //Log.d("status", "" + status);

                Context context = view.getContext();
                Intent i = new Intent(context, AddRecordingUnregisteredActivity.class);
                 i.putExtra("DATE",recordingDate);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status",status);

               /* Log.d("zzzs","ARGUMENT FRAGMENT");
                Log.d("zzzs", "recordingDate= "+recordingDate);
                Log.d("zzzs", "flock_id= "+flock_id);
                Log.d("zzzs","flock_name= "+flock_name);
                Log.d("zzzs", "flock_type= "+flock_type);
                Log.d("zzzs","flock_period= "+flock_period);
                Log.d("zzzs","location_name= "+location_name);
                Log.d("zzzs","location_id= "+location_id);
                Log.d("zzzs","status= "+status);*/

                context.startActivity(i);
                //Log.d("flock_id", "" + flock_id);
            }
        });
    }

    public void Submit(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        String loc_id = dataSource.getLocation(location).get(0);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Flock flock = new Flock();
            flock.setFlock_id(cursor.getString(cursor.getColumnIndex("id")));
            flock.setFlock_name(cursor.getString(cursor.getColumnIndex("name")));
            flock.setLocation_id(cursor.getString(cursor.getColumnIndex("location_id")));
            flock.setType(cursor.getString(cursor.getColumnIndex("type")));
            flock.setPeriod(cursor.getString(cursor.getColumnIndex("period")));
            flock.setStatus(cursor.getString(cursor.getColumnIndex("status")));
            flock.setLocation_name(location);
            flock_items.add(flock);
        }
        //adapter = new UnFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
}
