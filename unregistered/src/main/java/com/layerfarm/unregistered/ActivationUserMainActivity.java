package com.layerfarm.unregistered;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingFeed;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterRegistrationCreateLayerfarmUser;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.model.mFeed;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.chickin.model.chickinDistributionModelAge;
import com.layerfarm.chickin.model.chickinDistributionModelHatch;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.unregistered.intro.IntroActivity;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationUserMainActivity extends AppCompatActivity {

    ProgressDialog loading, progressDialog, loading_create_farm;
    private String nid="";
    private String created ="";
    private ArrayList <chickinDistributionModelHatch> ListchickinDistributionHatch;
    private ArrayList <chickinDistributionModelAge> ListchickinDistributionAge;
    private int nombr = 0;
    int progress =0;
    private String key;


    //billing api
    private BillingClient billingClient;
    List<SkuDetails> mySkuDetailsList;

    EditText eDFullname, eDEmail, eDFarmName, eDFarmAddress, eDPhone;
    CheckBox chxAggree;
    Button activateButton;
    public String url,username,password,_username_retrofit,_password_retrofit;
    Spinner mSpinner;
    ArrayAdapter<String> idAdapter;
    DatabaseHelper db = new DatabaseHelper(this);
    DataHelper SQLite = new DataHelper(this);
    private final String PURCHASE_ID = "lfm1.0alfa_1";
    //private final String PURCHASE_ID = "lfm1.0alfa_member";
    private DBDataSourceUnregistered dataSource;
    private String cb_status;
    private Handler mHandler = new Handler();
    private Handler mHandlerCheckFarm = new Handler();
    private Handler mHandlerConnectNewFarm = new Handler();
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SQLite = new DataHelper(getApplicationContext());

        setContentView(R.layout.activity_activation_user_main);

        Toolbar toolbarTop = (Toolbar) findViewById(com.layerfarm.recording.R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(com.layerfarm.recording.R.id.title);
        mTitle.setText("Create Farm");

        overridePendingTransition(R.anim.bottom_to_up, R.anim.up_to_bottom);
        mSpinner = (Spinner)findViewById(R.id.availableID);
        eDFullname = (EditText) findViewById(R.id.input_full_name);
        eDEmail= (EditText) findViewById(R.id.input_email);
        eDFarmName= (EditText) findViewById(R.id.input_farm_name);
        eDFarmAddress= (EditText) findViewById(R.id.input_farm_address);
        eDPhone= (EditText) findViewById(R.id.input_phone);
        chxAggree = (CheckBox) findViewById(R.id.cb_click_activation) ;
        //Get server information from local database
        //getServerInformation();

        activateButton = (Button)findViewById(R.id.activateButton) ;
        activateButton.setEnabled(false);
        activateButton.setBackgroundColor(Color.parseColor("#ceced2"));

        connect_server();

        ArrayList<String> timezones = new ArrayList<>(Arrays.asList(TimeZone.getAvailableIDs()));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, timezones);
        mSpinner.setAdapter(adapter);

        TimeZone tz=TimeZone.getDefault();
        mSpinner.setSelection(getIndex(mSpinner,tz.getID()));
        Toast.makeText(ActivationUserMainActivity.this, "Time Zone : " + tz.getDisplayName(false,TimeZone.SHORT) + " \n TimeZone Id: "+ tz.getID(), Toast.LENGTH_SHORT).show();

        chxAggree.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(chxAggree.isChecked())
                {
                    activateButton.setEnabled(true);
                    activateButton.setBackgroundColor(Color.parseColor("#008577"));
                }
                else
                {
                    activateButton.setEnabled(false);
                    activateButton.setBackgroundColor(Color.parseColor("#ceced2"));
                }

            }

        });
    }

    public void close(View view){
        finish();
    }


    public void activate_user(View view) {

        if (eDFullname.getText().toString().isEmpty()) {
            eDFullname.setError("Please fill the data");
        }else if (eDEmail.getText().toString().isEmpty()){
            eDEmail.setError("Please fill the data");
        }else if (!isValidEmail(eDEmail.getText().toString())){
            eDEmail.setError("Email invalid");
        }else if (eDFarmName.getText().toString().isEmpty()){
            eDFarmName.setError("Please fill the data");
        }else if (eDFarmAddress.getText().toString().isEmpty()){
            eDFarmAddress.setError("Please fill the data");
        }else if (eDPhone.getText().toString().isEmpty()){
            eDPhone.setError("Please fill the data");
        }else if (!validCellPhone(eDPhone.getText().toString())){
            eDPhone.setError("Invalid phone number");
        } else {
            // editText is not empty
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
            alertDialog.setTitle("Confirm Activate...");
            // Setting Dialog Message
            alertDialog.setMessage("Process may need long time?");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.ic_plus);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                    String module = "layerfarm_android_unregistered";
                    String function_name = "layerfarm_check_email";

                    ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments("", eDEmail.getText().toString(),"","","","","", "","");

                    ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                    String token2 = RetrofitData.getInstance().getToken2();
                    ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

                    Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            //String[] get_expense_general_respons = response.body();
                            //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                            if (response.isSuccessful()) {
                                String farm_nid = response.body();
                                Log.d("Layerfarm", "node = " + farm_nid);
                                if (farm_nid.equals("valid_email")){

                                    //---- Billing init, comment jika tidak pakai billing
                                    /*querySkuList();
                                    startPurchase("lfm1.0alfa_1");
                                    queryOwned();*/
                                    //------ end

                                    //bypass billing
                                    create_layerfarm_user();
                                    //delay to prevent CSRF Error
                                    int secs = 10; // Delay in seconds
                                    delayFarm.delay(secs, new delayFarm.DelayCallback() {
                                        public void afterDelay() {
                                            // Do something after delay
                                            StartCheckFarm();
                                        }
                                    });


                                }else{
                                    AskEmailError();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                /*String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());*/
                        }
                    });
                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event

                }
            });
            alertDialog.show();
        }


    }
    public void DialogChangedPassword(){
        dialog = new AlertDialog.Builder(ActivationUserMainActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_changed_password, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Ubah Password");

        final EditText username = (EditText)dialogView.findViewById(R.id.username);
        final EditText password = (EditText)dialogView.findViewById(R.id.password);
        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String module = "layerfarm_android";
                String function_name = "layerfarm_android_update_password";
                String[] args = {username.getText().toString(), password.getText().toString()};
//                if(!users.getUid().isEmpty()){
//                    args.setUid("");
//                }

//                args[0] = username.getText().toString();
////                args.setMail(email.getText().toString());
//                args[1] =(password.getText().toString());
//                args.setStatus(state);
//                args.setRoles(role);
//                args.setLanguage(lang);
//                args.setLocation(location_check);
//                for (int i =0; i< role.size(); i++){
//                    Log.d("zzz","role = "+ role.get(i));
//                }
//                Log.d("zzz","state = "+state);

                Parameter parameter = new Parameter(module, function_name, args);
                ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                String token2 = RetrofitData.getInstance().getToken2();
                Call<ArrayList<String>> call = apiInterfaceJson.updatePassword(token2, parameter);

                call.enqueue(new Callback<ArrayList<String>>() {

                    @Override
                    public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {
                        ArrayList<String> member = response.body();
                        Toast.makeText(ActivationUserMainActivity.this, "Success update password", Toast.LENGTH_SHORT).show();
                        restart();
                        //finish();
                    }

                    @Override
                    public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                        Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                        Toast.makeText(ActivationUserMainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });

//                nama    = txt_nama.getText().toString();
//                usia    = txt_usia.getText().toString();
//                alamat  = txt_alamat.getText().toString();
//                status = txt_status.getText().toString();
//
//                txt_hasil.setText("Nama : " + nama + "\n" + "Usia : " + usia + "\n" + "Alamat : " + alamat + "\n" + "Status : " + status);
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Activate Success")
                .setMessage("Your account was activated")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivationUserMainActivity.this.finish();
                    }
                })
                .show();

        //ExpenseMainActivity.ma.finish();

        return myQuittingDialogBox;

    }

    private AlertDialog MigrationDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Done")
                .setMessage("Your farm created successfully")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //ActivationUserMainActivity.this.finish();
//                        connect_server_new_farm();

                        restart();
                    }
                })
                .show();

        //ExpenseMainActivity.ma.finish();

        return myQuittingDialogBox;

    }

    private AlertDialog WarningDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Upload Error")
                .setMessage("General Expense Upload Error, Please Try Again")
                //.setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivationUserMainActivity.this.finish();
                    }
                })
                .show();

        return myQuittingDialogBox;

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Please fill all data")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return myQuittingDialogBox;
    }

    public boolean validCellPhone(CharSequence target) {
        if (target == null || target.length() < 6 || target.length() > 20) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    private AlertDialog AskEmailError() {
        AlertDialog emailDialogBox = new AlertDialog
                .Builder(this)
                .setTitle("Warning!!")
                .setMessage("Email has been used")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {@Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                }).show();
        return emailDialogBox;
    }

    private static String generateUniqeCode() {
        int length = 6;
        String DATA_FOR_RANDOM_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        SecureRandom random = new SecureRandom();
        if (length < 1) throw new IllegalArgumentException();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            // debug
            sb.append(rndChar);
        }




       /* String DATA_FOR_RANDOM_STRING_NUMBER = NUMBER;
        SecureRandom random_number = new SecureRandom();
        if (lengthNumber < 1) throw new IllegalArgumentException();
        StringBuilder sb_number = new StringBuilder(length);
        for (int i = 0; i < lengthNumber; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAtNumber = random_number.nextInt(DATA_FOR_RANDOM_STRING_NUMBER.length());
            char rndCharNumber = DATA_FOR_RANDOM_STRING_NUMBER.charAt(rndCharAtNumber);
            // debug
            sb_number.append(rndCharNumber);
        }
        String key = sb.toString() + sb_number.toString();*/
        //Log.d("message","key : "+sb.toString());
        return sb.toString();
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }

    public void create_layerfarm_user() {

        loading_create_farm = ProgressDialog.show(this, "Create farm...", "Please wait...", true, false);

        EditText a = (EditText )(findViewById(R.id.input_full_name));
        final String full_name = (String)(a.getText().toString());

        EditText b = (EditText )(findViewById(R.id.input_email));
        final String email = (String)(b.getText().toString());

        EditText c = (EditText )(findViewById(R.id.input_farm_name));
        final String farm_name = (String)(c.getText().toString());

        EditText d = (EditText )(findViewById(R.id.input_farm_address));
        final String farm_address = (String)(d.getText().toString());

        EditText e = (EditText)(findViewById(R.id.input_phone));
        final String phone = (String)(e.getText().toString());

        Spinner f = (Spinner)(findViewById(R.id.availableID));
        final String timezone = (f.getSelectedItem().toString()).trim();
        //Generate uniqe code
        final String url_name = generateUniqeCode().toLowerCase();
        key = url_name;

        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_create_farm";

        ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments(full_name, email,farm_name,url_name,"",farm_address,phone, timezone,"");

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

        Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String farm_nid = response.body();
                    Log.d("Layerfarm", "node = " + farm_nid);

                    Log.v("this", "Yes!");


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                //WarningDialog();

            }
        });

    }

    private void set_variabl(String url_key){
        String url_address = url_key;
        String url_username = "admin";
        String url_password = "admin!@#";
        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = variables.edit();
        editor.putString("code", url_key);
        editor.putString("username", url_username);
        editor.putString("password", url_password);
        editor.apply();
        Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();

    }

    private void set_date_expired(String url_key){

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
        String strDate = mdformat.format(calendar.getTime());

        calendar.add(Calendar.MONTH, 1);  // number of days to add
        String end_date = mdformat.format(calendar.getTime());


        String url_address = url_key;
        String url_expired_date = end_date;

        SQLite = new DataHelper(getApplication());
        SQLiteDatabase dbase = db.getWritableDatabase();
        Cursor result = dbase.rawQuery("SELECT id FROM farm_expired_date", null);
        if (result.getCount() == 0) {
            // TODO Auto-generated method stub

            ContentValues x = new ContentValues();
            x.put("short_name", url_address);
            x.put("expired", url_expired_date);
            dbase.insert("farm_expired_date",null,x);

            //Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();

        }else {
            // TODO Auto-generated method stub
            String strSQL = "UPDATE farm_expired_date SET expired = " + url_expired_date +" WHERE short_name = "+ url_address;
            dbase.execSQL(strSQL);
            Toast.makeText(getApplicationContext(), "Renewal Successfully", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onDestroy() {
//        billingClient.endConnection();
        super.onDestroy();
    }

    public void restart(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        ActivityCompat.finishAfterTransition(ActivationUserMainActivity.this);
    }

    private void connect_server(){
        loading = ProgressDialog.show(ActivationUserMainActivity.this, null, "Please wait...", true, false);
        url = Env.DOMAIN;
        username = "root";
        password = "sapua#@!";
        LayerFarm.getInstance().ConnectServer(this, "http://"+url+"/", username, password);
        RetrofitData.getInstance().setStatus("");
        Start();

    }

    public void Start(){
        mRun.run();
    }
    public void Stop(){
        loading.dismiss();
        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
            if (email == username && passwd == password){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(ActivationUserMainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Stop();
                    } else{
                        Stop();
                        finish();
                    }
                }
            }

        }
    };

    private void dropDataServer(){
        final SQLiteDatabase dbase = db.getReadableDatabase();
        //dbase.delete("Feed", null, null);
        dbase.delete("variables", null, null);
    }

    public void StartCheckFarm(){
        mRunCheckFarm.run();
    }

    public void StopCheckFarm()
    {
        mHandlerCheckFarm.removeCallbacks(mRunCheckFarm);
    }

    private Runnable mRunCheckFarm = new Runnable() {
        @Override
        public void run() {
            mHandlerCheckFarm.postDelayed(mRunCheckFarm,5000);

            String module = "layerfarm_android_unregistered";
            String function_name = "layerfarm_check_farm";

            ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments("", "","",key,"","","", "","");

            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
            String token2 = RetrofitData.getInstance().getToken2();
            ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

            Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //String[] get_expense_general_respons = response.body();
                    //Log.d("message","get_expense_general_respons : "+get_expense_general_respons.toString());
                    if (response.isSuccessful()) {
                        String exist_farm = response.body();
                        Log.d("xxxxxx", "exist_farm = " + exist_farm);

                        if (exist_farm.equals("exist_farm")){
                            Toast.makeText(ActivationUserMainActivity.this, "Farm created" , Toast.LENGTH_SHORT).show();
                            Log.d("zzz","farm created");
                            StopCheckFarm();

                            set_variabl(key);

                            connect_server_new_farm();
                            //loading_create_farm.dismiss();
//                            connect_server_new_farm();



                            //Log.d("farm", "farm found = " + farm_nid);

                            //DialogChangedPassword();
                            //SubmitDialog();
                            //loading.dismiss();
                            //restart();

                        }
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                /*String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());*/
                    StopCheckFarm();
                }
            });
        }
    };


    private void connect_server_new_farm(){
        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        String code = variables.getString("code", null);
        String uname = variables.getString("username", null);
        String password = variables.getString("password", null);

        if(code == null && uname == null && password == null){
            Toast.makeText(getApplicationContext(), "No server setting data", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, IntroActivity.class));
            finish();
        }
        else {

            LayerFarm.getInstance().Connect(this, "http://"+code+"."+ Env.DOMAIN+"/", uname, password);
            RetrofitData.getInstance().setStatus("");
            Start_connect_new_farm();
        }

    }


    public void Start_connect_new_farm(){
        mRunConnectNewFarm.run();
    }
    public void Stop_connect_new_farm(){
        loading_create_farm.dismiss();
        mHandlerConnectNewFarm.removeCallbacks(mRunConnectNewFarm);
    }
    private Runnable mRunConnectNewFarm = new Runnable() {
        @Override
        public void run() {
            mHandlerConnectNewFarm.postDelayed(mRunConnectNewFarm,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            if (email == _username_retrofit && passwd == _password_retrofit){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        InsertUser();
                        loading_create_farm.dismiss();
                        Toast.makeText(ActivationUserMainActivity.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        MigrationDialog();
                        Stop_connect_new_farm();

//                        create_layerfarm_location();
                    } else{
                        Stop_connect_new_farm();
                        finish();
                    }
                }
//            }

        }
    };
    public void InsertUser(){
        User data_user = RetrofitData.getInstance().getUser();
        String farm_name = RetrofitData.getInstance().getFarm_name();
        ContentValues user = new ContentValues();
        //daily_values.put("note", note);
        user.put("email", data_user.getMail());
        user.put("password", RetrofitData.getInstance().getPassword());
        user.put("full_name", RetrofitData.getInstance().getUser().getName());

//        if (!code.isEmpty()){
//            url = code+"."+url;
//        }
        user.put("farm_code", url);
        user.put("farm_name",farm_name);
        user.put("farm_address","");
//        FarmNid nid = RetrofitData.getInstance().getFarmNid().values();
        user.put("nid", RetrofitData.getInstance().getFarmNid());
        Map<String, String> role = data_user.getRoles();
        role.remove("2");
        ArrayList<String> roles = new ArrayList<>(role.values());
        String[] arr = roles.toArray(new String[0]);
        if (role.containsValue("administrator")){
            user.put("is_owner","1");
        }
        else
            user.put("is_owner","0");
        Log.d("zzz","array = "+arr);
        arr.toString();
        Log.d("zzzz","toString = "+arr.toString());
        String str_roles = "";
        SQLiteDatabase dbase = db.getWritableDatabase();
        String query = "SELECT * FROM role WHERE machine_name IN (" + makePlaceholders(arr.length) + ")";
        Cursor cursor = dbase.rawQuery(query, arr);
        String[] role_name = new String[arr.length];
        String[] role_id = new String[arr.length];
        for (int i = 0; i< cursor.getCount(); i++){
            cursor.moveToPosition(i);
            role_name[i] = cursor.getString(cursor.getColumnIndex("name"));
            role_id[i] = cursor.getString(cursor.getColumnIndex("id"));
        }

        for (int i =0; i < role_name.length; i++){
            if (i!= 0){
                str_roles +=",";
            }
            str_roles = str_roles+role_name[i]+" ";
        }

//        user.put("role", roles.toString());
        user.put("role", str_roles);


        //mengeksekusi perintah sql insert data
        //yang mengembalikan sebuah insert ID
        long user_id = dbase.insert("user_farm",null,user);
        for (int i =0; i< roles.size(); i++){
            str_roles = roles.get(i)+" ";
            ContentValues cv_roles = new ContentValues();
            cv_roles.put("user_farm_id", user_id);
            cv_roles.put("role_id",role_id[i] );
            long user_role_id = dbase.insert("user_role", null, cv_roles);
        }
    }
    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    public void BackActivationFarm_1(View view) {
        ActivationUserMainActivity.this.finish();
    }

    public void BackActivationFarm_2(View view) {
        ActivationUserMainActivity.this.finish();
    }


}
