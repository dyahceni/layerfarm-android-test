package com.layerfarm.unregistered;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.model.UserValidate;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ResetPassword extends Activity {
    EditText farm_code, username_email;
    Button submit;

    private String email;
    private String password;
    private String token1;
    private String token2;
    private String baseUrl;
    String name;
    String code;
    private LoginStatus loginStatus;
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);

        farm_code = (EditText) findViewById(R.id.farm_code);
        username_email = (EditText) findViewById(R.id.email_username);

        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coonnect();
            }
        });
    }

    public void Coonnect() {
        code = farm_code.getText().toString();
        name = username_email.getText().toString();
        baseUrl = "http://" + code + "." + Env.DOMAIN + "/";
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();

        callToken1.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                final String token1  = response.body();
                token1 = response.body();
                RetrofitData.getInstance().setToken1(token1);


                // Sebelum melakukan login, cek dulu apakah username yang digunakan utk login ini sudah terdaftar belum di login_log server.
                String module = "layerfarm_android";
                String function_name = "layerfarm_android_user_validate";
                String[] args = {name};
                String anonymous = "1";

                ParameterAnon parameterAnon = new ParameterAnon(module, function_name, args, anonymous);
                Call<UserValidate> callLoginStatus = apiInterfaceJson.getUserValidate(token1, parameterAnon);

                callLoginStatus.enqueue(new Callback<UserValidate>() {
                    @Override
                    public void onResponse(Call<UserValidate> callLoginStatus, Response<UserValidate> response) {
                        final UserValidate loginStatus = response.body();
                        Log.d("Layerfarm", "LoginStatus success = "+loginStatus);
                        if (!loginStatus.getStatus().equals("user_not_exist")) {
                            String module = "layerfarm_android";
                            String function_name = "layerfarm_android_send_email_reset_password";
                            String[] args = {loginStatus.getEmail()};
                            String anonymous = "1";

                            ParameterAnon parameterAnon = new ParameterAnon(module, function_name, args, anonymous);
                            Call<String[]> callsendReset = apiInterfaceJson.getSendResetPassword(token1, parameterAnon);
                            callsendReset.enqueue(new Callback<String[]>() {
                                @Override
                                public void onResponse(Call<String[]> callsendReset, Response<String[]> response) {
                                    String[] result = response.body();
                                    Log.d("Layerfarm", "reset password success = "+result);
                                    if (result[0].equals("true")){
                                        Intent intent;
                                        intent = new Intent(getApplicationContext(), ResetSuccess.class);
                                        intent.putExtra("email", loginStatus.getEmail());
                                        startActivity(intent);
                                    }

                                }

                                @Override
                                public void onFailure(Call<String[]> callsendReset, Throwable t) {
                                    Log.d("Layerfarm", "reset password failure = " + t.getMessage());
                                }
                            });

                        }
                        else {
                            Intent intent;
                            intent = new Intent(getApplicationContext(), ResetFailed.class);
                            intent.putExtra("email", name);
                            startActivity(intent);
                        }

                    }

                    @Override
                    public void onFailure(Call<UserValidate> callLoginStatus, Throwable t) {
                        Log.d("Layerfarm", "LoginStatus failure = " + t.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Layerfarm", "processToken1 error: " + t.getMessage()); // SocketTimeoutException
                //LayerFarm(farmName, getEmail(), getPassword());
                RetrofitData.getInstance().setStatus("processToken1 error");
                resultConnect = t.getMessage();
                //Log.d("resultConnect", "resultConnect = " + resultConnect);
            }

        });
    }
}
