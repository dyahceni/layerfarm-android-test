package com.layerfarm.unregistered;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.DatabaseHelper;
import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterRegistrationCreateLayerfarmUser;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppAddPartner extends Activity {
    EditText username, farm_code, farm_password, farm_url;
    TextView reset_password;
    ProgressBar progressBar;
    Button add_account;
    DatabaseHelper dbase;
    SQLiteDatabase db;

    String url, uname, passwd, code ="";

    private Handler mHandler = new Handler();
    private Handler mHandlerCheckFarmStatus = new Handler();
    private Handler mHandlerServer = new Handler();
    private String urlserver,usernameserver, passwordserver;
    ProgressDialog loadingCheckfarm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_add_partner_unregistered);

        dbase = new DatabaseHelper(this);
        db = dbase.getReadableDatabase();

        Toolbar toolbarTop = (Toolbar) findViewById(com.layerfarm.recording.R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(com.layerfarm.recording.R.id.title);
        mTitle.setText("Add Connection");

        progressBar = (ProgressBar) findViewById(R.id.proggressBar);
        username = (EditText) findViewById(R.id.farm_username);
        farm_code = (EditText) findViewById(R.id.farm_code);
        farm_password = (EditText) findViewById(R.id.farm_password);
//        farm_url = (EditText) findViewById(R.id.url);
        add_account = (Button) findViewById(R.id.add_account);
        reset_password = (TextView) findViewById(R.id.reset_password);
        reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(getApplicationContext(), ResetPassword.class);
                startActivity(intent);
            }
        });
        add_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valid = true;
                if (username.getText().toString().isEmpty()) {
                    username.setError("Please fill the data");
                    valid = false;
                }
                else if (farm_code.getText().toString().isEmpty()) {
                    farm_code.setError("Please fill the data");
                    valid = false;
                } else if (farm_password.getText().toString().isEmpty()) {
                    farm_password.setError("Please fill the data");
                    valid = false;
                } else {
                    code = farm_code.getText().toString();
                    uname = username.getText().toString();
                    passwd = farm_password.getText().toString();
                    final String baseUrl = "http://"+code+"."+Env.DOMAIN+"/";
                    LayerFarm.getInstance().Connect(AppAddPartner.this, baseUrl, uname, passwd);
                    RetrofitData.getInstance().setStatus("");
                    Start();
                }
            }
        });



    }
    public void close(View view){
        finish();
    }
    private void connect_server(){
        code = farm_code.getText().toString();
        uname = username.getText().toString();
        passwd = farm_password.getText().toString();
        final String baseUrl = "http://"+code+"."+Env.DOMAIN+"/";

        String module = "layerfarm_android_unregistered";
        String function_name = "layerfarm_android_get_farm_nid";
        String[] args = {code};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> callfarmName = apiInterfaceJson.getFarmNid(token2, parameter);

        callfarmName.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] farm_nid = response.body();
                RetrofitData.getInstance().setFarmNid(farm_nid[0]);

                LayerFarm.getInstance().Connect(AppAddPartner.this, baseUrl, uname, passwd);
                RetrofitData.getInstance().setStatus("");
                Start();
            }
            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "farm_nid error = "+t.getMessage());
                Log.d("Layerfarm", "farm_nid error!");
                loadingCheckfarm.dismiss();
            }
        });

//        }
    }
    private void set_variabl(){
        String url_address = code;
        String url_username = username.getText().toString();
        String url_password = farm_password.getText().toString();
        SharedPreferences variables = this.getSharedPreferences("variables", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = variables.edit();
        editor.putString("code", url_address);
        editor.putString("username", url_username);
        editor.putString("password", url_password);
        editor.apply();
        Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();

//        Cursor result = db.rawQuery("SELECT value FROM variables", null);
//        if (result.getCount() == 0) {
//            // TODO Auto-generated method stub
////            if (!code.isEmpty()){
////                url = code+"."+url;
////            }
//            String[] _VARIABLE_INSERT = {"INSERT INTO variables (name, value) VALUES ('code', '"+code+"')","INSERT INTO variables (name, value) VALUES ('username', '"+username.getText().toString()+"')", "INSERT INTO variables (name, value) VALUES ('password', '"+farm_password.getText().toString()+"')"};
//            for(String s : _VARIABLE_INSERT) {
//                db.execSQL(s);
//            }
//            Toast.makeText(getApplicationContext(), "Setting Server Successfully", Toast.LENGTH_LONG).show();
//
//
//        }else {
//
//            // TODO Auto-generated method stub
//
//            String[] _VARIABLE_UPDATE = {"UPDATE variables SET value = '"+code+"' WHERE name = 'code'", "UPDATE variables SET value = '"+username.getText().toString()+"' WHERE name = 'username'", "UPDATE variables SET value = '"+farm_password.getText().toString()+"' WHERE name = 'password'"};
//            for(String s : _VARIABLE_UPDATE) {
//                db.execSQL(s);
//            }
//            Toast.makeText(getApplicationContext(), "Update Setting Server Successfully", Toast.LENGTH_LONG).show();
//        }
    }
    public void Start(){
        progressBar.setVisibility(View.VISIBLE);
        mRun.run();
    }
    public void Stop(){
        progressBar.setVisibility(View.GONE);
        mHandler.removeCallbacks(mRun);
    }
    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRun,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
//            Map<String, String> role = RetrofitData.getInstance().getUser().getRoles();
//            if (email == uname && passwd == passwd){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(AppAddPartner.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        InsertUser();
                        set_variabl();
                        Stop();
                        //finish();
                        restart();
                    } else{
                        Toast.makeText(AppAddPartner.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Stop();
//                        loadingCheckfarm.dismiss();
                        if (RetrofitData.getInstance().getStatus().equals("Wrong username or password")){
                            AskOption();
                        }
                        //finish();
//                        restart();
                    }
                }
//            }

        }
    };

    public void InsertUser(){
        User data_user = RetrofitData.getInstance().getUser();
        String farm_name = RetrofitData.getInstance().getFarm_name();
        ContentValues user = new ContentValues();
        //daily_values.put("note", note);
        user.put("email", data_user.getMail());
        user.put("password", passwd);
        user.put("full_name", uname);

//        if (!code.isEmpty()){
//            url = code+"."+url;
//        }
        user.put("farm_code", code);
        user.put("farm_name",farm_name);
        user.put("farm_address","");
//        FarmNid nid = RetrofitData.getInstance().getFarmNid().values();
        user.put("nid", RetrofitData.getInstance().getFarmNid());
        Map<String, String> role = data_user.getRoles();
        role.remove("2");
        ArrayList<String> roles = new ArrayList<>(role.values());
        String[] arr = roles.toArray(new String[0]);
        if (role.containsValue("administrator")){
            user.put("is_owner","1");
        }
        else
            user.put("is_owner","0");
        Log.d("zzz","array = "+arr);
        arr.toString();
        Log.d("zzzz","toString = "+arr.toString());
        String str_roles = "";
        String query = "SELECT * FROM role WHERE machine_name IN (" + makePlaceholders(arr.length) + ")";
        Cursor cursor = db.rawQuery(query, arr);
        String[] role_name = new String[arr.length];
        String[] role_id = new String[arr.length];
        for (int i = 0; i< cursor.getCount(); i++){
            cursor.moveToPosition(i);
            role_name[i] = cursor.getString(cursor.getColumnIndex("name"));
            role_id[i] = cursor.getString(cursor.getColumnIndex("id"));
        }

        for (int i =0; i < role_name.length; i++){
            if (i!= 0){
                str_roles +=",";
            }
            str_roles = str_roles+role_name[i]+" ";
        }

//        user.put("role", roles.toString());
        user.put("role", str_roles);


        //mengeksekusi perintah sql insert data
        //yang mengembalikan sebuah insert ID
        long user_id = db.insert("user_farm",null,user);
        for (int i =0; i< roles.size(); i++){
            str_roles = roles.get(i)+" ";
            ContentValues cv_roles = new ContentValues();
            cv_roles.put("user_farm_id", user_id);
            cv_roles.put("role_id",role_id[i] );
            long user_role_id = db.insert("user_role", null, cv_roles);
        }
    }
    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }


    //Codingan Nanang
    public void restart(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        ActivityCompat.finishAfterTransition(AppAddPartner.this);
    }


    public void StartCheckFarmStatus(){
        mRunCheckFarmStatus.run();
    }

    public void StopCheckFarmStatus(){

        mHandlerCheckFarmStatus.removeCallbacks(mRunCheckFarmStatus);
    }

    private Runnable mRunCheckFarmStatus = new Runnable() {
        @Override
        public void run() {
            mHandlerCheckFarmStatus.postDelayed(mRunCheckFarmStatus,5000);

            EditText a = (EditText )(findViewById(R.id.url));
            final String ed_url = (String)(a.getText().toString());
            final String farm_name = ed_url.split("\\.")[0];

            String module = "layerfarm_android_unregistered";
            String function_name = "layerfarm_check_farm_status";

            ParameterRegistrationCreateLayerfarmUser.Arguments args = new ParameterRegistrationCreateLayerfarmUser.Arguments("", "","",farm_name,"","","", "","");

            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
            String token2 = RetrofitData.getInstance().getToken2();
            ParameterRegistrationCreateLayerfarmUser parameter = new ParameterRegistrationCreateLayerfarmUser(module, function_name, args);

            Call<String> call = apiInterfaceJson.create_layerfarm_user(token2, parameter);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        String farm_status = response.body();
                        if (farm_status != null) {
                            Log.d("Layerfarm_status", "Layerfarm_status = " + farm_status);
                            String status = farm_status.split("\\/")[0];
                            Log.d("xxx", "Layerfarm_status = " + status);
                            if (status.equals("1")) {
                                StopCheckFarmStatus();
//                                loadingCheckfarm.dismiss();
                                Toast.makeText(AppAddPartner.this, "Farm Active", Toast.LENGTH_SHORT).show();
                                connect_server();

                            } else {
                                StopCheckFarmStatus();
//                                loadingCheckfarm.dismiss();
                                //Toast.makeText(AppAddPartner.this, "Farm InActive", Toast.LENGTH_SHORT).show();
                                AskOption();

                            }
                        }
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    String alert = t.getMessage();
                    Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                    StopCheckFarmStatus();
                }
            });
        }
    };

    /*
    private void connect_server_farm(){
        //Log.d("key", "key = " + key);
        loadingCheckfarm = ProgressDialog.show(AppAddPartner.this, "Checking farm", "Please wait...", true, false);
//        urlserver = "edufren.com";
        urlserver = Env.DOMAIN;
        usernameserver = "root";
        passwordserver = "sapua#@!";
        LayerFarm.getInstance().Connect(this, "http://"+urlserver+"/", usernameserver, passwordserver);
        RetrofitData.getInstance().setStatus("");
        Start_Connect();

    }
     */

    public void Start_Connect(){
        mRunServer.run();
    }
    public void Stop_connect(){

        mHandlerServer.removeCallbacks(mRunServer);
    }
    private Runnable mRunServer = new Runnable() {
        @Override
        public void run() {
            mHandlerServer.postDelayed(mRunServer,5000);
            String email = RetrofitData.getInstance().getEmail();
            String passwd = RetrofitData.getInstance().getPassword();
            if (email == usernameserver && passwd == passwordserver){
                if (RetrofitData.getInstance().getStatus() != "") {
                    if (RetrofitData.getInstance().getStatus() == "Success") {
                        Toast.makeText(AppAddPartner.this, RetrofitData.getInstance().getStatus(), Toast.LENGTH_SHORT).show();
                        Stop_connect();
//                        loadingCheckfarm.dismiss();
                        Toast.makeText(AppAddPartner.this, "Farm Active", Toast.LENGTH_SHORT).show();
                        connect_server();
//                       StartCheckFarmStatus();
                    } else{
                        Stop_connect();
//                        loadingCheckfarm.dismiss();
                    }
                }
            }

        }
    };


    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Log In Failed!")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
}
