package com.layerfarm.recording;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.Env;
import com.layerfarm.layerfarm.LayerFarm;
import com.layerfarm.layerfarm.model.ChickinEntry;
import com.layerfarm.layerfarm.model.Connect;
import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingBodyWeight;
import com.layerfarm.layerfarm.model.DailyRecordingFeed;
import com.layerfarm.layerfarm.model.DailyRecordingOvk;
import com.layerfarm.layerfarm.model.DailyRecordingSpentHen;
import com.layerfarm.layerfarm.model.DailyRecordingTransfer;
import com.layerfarm.layerfarm.model.ExpenseGeneral;
import com.layerfarm.layerfarm.model.ExpenseGeneralList;
import com.layerfarm.layerfarm.model.Login;
import com.layerfarm.layerfarm.model.LoginStatus;
import com.layerfarm.layerfarm.model.ParamaterAge;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.ParameterAnon;
import com.layerfarm.layerfarm.model.ParameterCreateGeneralExpense;
import com.layerfarm.layerfarm.model.ParameterDeleteGeneralExpense;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RecordingHistory;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.model.SubmitDailyRecordingBw;
import com.layerfarm.layerfarm.model.User;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.recording.model.Mortality.MortalityData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.text.ValuePrinter;
//import org.mockito.internal.matchers.text.ValuePrinter;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import okhttp3.JavaNetCookieJar;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.matchers.text.ValuePrinter.print;

public class RecordingUnitTest {
    //    private Handler mHandler = mock(Handler.class);
    private String email =  "root";
    private String password = "sapua#@!";
    private String token1;
    private String nid;
    private String token2;
    private String baseUrl = "http://"+"nmcgyj"+"."+ Env.DOMAIN+"/";
    private LoginStatus loginStatus;
    private User user;
    private Connect connection;
    private ApiInterface apiInterfaceToken;
    private ApiInterface apiInterfaceJson;
    private String resultConnect;
    private String uniqueID;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void ConnectServer(){
//        if (TextUtils.isEmpty(uniqueID)) {
//            uniqueID = UUID.randomUUID().toString();
//        }

        // Cookies handling supaya session-nya bisa dimaintain:
        // https://stackoverflow.com/questions/48090485/how-to-retrieve-cookie-from-response-retrofit-okhttp
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        CookieHandler cookieHandler = new CookieManager();

        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.addNetworkInterceptor(interceptor);
        okhttpBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofitToken = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceToken = retrofitToken.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceToken(apiInterfaceToken);

        Retrofit retrofitJson = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterfaceJson = retrofitJson.create(ApiInterface.class);
        RetrofitData.getInstance().setApiInterfaceJson(apiInterfaceJson);

        // Ditest token-nya dulu, bisa dapat atau tidak
        Call<String> callToken1 = apiInterfaceToken.getToken1();
        try {
            String tokenn1 = callToken1.execute().body();
            ValuePrinter.print("tokenn1 = "+tokenn1);
            Call<Connect> callConnect = apiInterfaceJson.getConnect(token1);
            try {
                connection = callConnect.execute().body();
                ValuePrinter.print(connection);
                try {
                    user = connection.getUser();

                    Integer uid = user.getUid();

                    if (uid == 0) {
                        Login login = new Login(email, password);
                        Call<Connect> callLogin = apiInterfaceJson.login(token1, login);
                        try {
                            connection = callLogin.execute().body();   // Jika username & password incorrect, response = null
                            System.out.println("uid = "+connection.getUser().getUid());
                            if (connection != null) {
                                user = connection.getUser();
                                System.out.println("uid = "+connection.getUser().getUid());
                                RetrofitData.getInstance().setBaseUrl(baseUrl);
                                RetrofitData.getInstance().setPassword(password);
                                RetrofitData.getInstance().setEmail(email);
                                RetrofitData.getInstance().setUser(user);
                                assertTrue(true);

                                Map<String, String> headers = new HashMap<>();
                                headers.put(connection.getSessionName(), connection.getSessid());
                                Call<String> callToken2 = apiInterfaceToken.getToken2(headers);
                                try {
                                    token2  = callToken2.execute().body();
                                    RetrofitData.getInstance().setToken2(token2);
//                                    return true;
                                }catch (Exception e){
//                                    return false;
                                }

                            }

                        }catch (Exception e){
                            ValuePrinter.print("connection error = "+e);
                            assertFalse(true);
//                            return false;
                        }
                    }
                }catch (Exception e){}
            }catch (Exception e){
                ValuePrinter.print("connection error = "+e);
                assertFalse(true);
//                return false;
            }
//            Assert.assertEquals("tokenn1 = "+tokenn1, tokenn1);
        } catch (IOException e) {
            e.printStackTrace();
            ValuePrinter.print("tokenn1 error = "+e);
            assertFalse(true);
//            return false;

//            Assert.assertEquals("tokenn1 error = "+e, e);
        }
    }

    @Test
    public void getData() {
        System.out.println("Get Data");
//        if (ConnectServer()){
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_recording_per_house";
        String[] args;
        args = new String[]{"163376", "edit"};
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<RecordingEntry> call = apiInterfaceJson.getRecordingEntry(token2, parameter);
        try {
            RecordingEntry recording = call.execute().body();
            System.out.println("Recording date: "+recording.getProfil_kandang().getRecording_date());

        }
        catch (Exception e){
            assertFalse(true);
        }

    }

    public ArrayList<DailyRecordingOvk> getListMedicine() {
        ArrayList<DailyRecordingOvk> ListMedicine = new ArrayList<>();
        DailyRecordingOvk _ovk = new DailyRecordingOvk();
        _ovk.setOvk_id("8236");
        _ovk.setAmount("1");
        ListMedicine.add(_ovk);
        return ListMedicine;
    }

    public ArrayList<DailyRecordingFeed> getListFeed() {
        ArrayList<DailyRecordingFeed> ListFeed = new ArrayList<>();
        DailyRecordingFeed _feed = new DailyRecordingFeed();
        _feed.setFeed_id("158650");
        _feed.setAmount("1");
        ListFeed.add(_feed);
        return ListFeed;
    }

    public ArrayList<LinkedHashMap<String, String>> getListMortality() {
        ArrayList<LinkedHashMap<String, String>> list_mortality = new ArrayList<>();
        LinkedHashMap<String, String> list_items = new LinkedHashMap<>();
        list_items.put("die_sick","1");
        list_items.put("hatch_date_move", "163316");
        list_mortality.add(list_items);
        return list_mortality;
    }

    @Test
    public void test_recording_entry() {
        boolean create = create_entry();
        System.out.println("Create Recording Entry: "+create);
        if (!create){
            assertFalse(true);
        }
    }

    private boolean create_entry(){
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";
        //Data
        final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        String note = "test unit";
        args.setHouseId("163303");
        args.setRecordingDate("2020-07-10");
        args.setOvk(getListMedicine());
        args.setFeed(getListFeed());
        args.setMortality(getListMortality());
        args.setNote(note);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ temp);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    public ArrayList<DailyRecordingSpentHen> getListSpentHent() {
        ArrayList<DailyRecordingSpentHen> ListSpentHent = new ArrayList<>();
        DailyRecordingSpentHen spent_hent = new DailyRecordingSpentHen();
        spent_hent.setHatch_date_move(163348);
        spent_hent.setJumlah_afkir("2");
        spent_hent.setAfkir_weight("7");
        ListSpentHent.add(spent_hent);
        return ListSpentHent;
    }

    public ArrayList<DailyRecordingTransfer> getListTransfer(){
        ArrayList<DailyRecordingTransfer> ListTransfer = new ArrayList<>();
        DailyRecordingTransfer transfer = new DailyRecordingTransfer();
        transfer.setHatch_date_move("163348");
        transfer.setTo_house("139952");
        transfer.setNumber_hen_move("2");
        ListTransfer.add(transfer);
        return ListTransfer;
    }

    @Test
    public void test_recording_transfer() {
        boolean create = create_transfer();
        System.out.println("Create Recording Trasnfer: "+create);
        if (!create){
            assertFalse(true);
        }
    }

    private boolean create_transfer(){
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";
        //Data
        DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        ArrayList<DailyRecordingSpentHen> spentHens = getListSpentHent();
        ArrayList<DailyRecordingTransfer> transfers = getListTransfer();
        args.setHouseId("163303");
        args.setRecordingDate("2020-07-10");
        args.setAfkir(spentHens);
        args.setPindahKandang(transfers);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


        ApiInterface apiInterfaceJson;
        apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);

        try {
            String[] node = call.execute().body();
            int temp = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ temp);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    @Test
    public void test_recording_environment() {
        boolean create = create_environment();
        System.out.println("Create Recording Trasnfer: "+create);
        if (!create){
            assertFalse(true);
        }
    }

    private boolean create_environment(){
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";
        //Data
        DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        ArrayList<String> light_duration = new ArrayList<>();
        light_duration.add("00:30");
        light_duration.add("01:00");
        ArrayList<String> temp = new ArrayList<>();
        temp.add("0");
        temp.add("3");
        temp.add("5");
        ArrayList<String> humid = new ArrayList<>();
        humid.add("32");
        humid.add("132");
        humid.add("12");
        args.setHouseId("163303");
        args.setRecordingDate("2020-07-10");
        args.setLightIntensity("1");
        args.setLightDuration(light_duration);
        args.setTemp(temp);
        args.setHumid(humid);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


        ApiInterface apiInterfaceJson;
        apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);

        try {
            String[] node = call.execute().body();
            int tempo = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ tempo);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }

    @Test
    public void test_recording_body_weight() {
        boolean create = create_body_weight();
        System.out.println("Create Recording Trasnfer: "+create);
        if (!create){
            assertFalse(true);
        }
    }

    private boolean create_body_weight(){
        String module = "bw_recording";
        String function_name = "bw_recording_save_body_weight";
        //Data
        final DailyRecordingBodyWeight args = DailyRecordingManager.getInstance().getBodyWeight();
        String data = "21 21";
        final String[] bw = data.split(" ");
        args.setRecordingDate("2020-07-10");
        args.setHouseId("163303");
        args.setBodyWeight(bw);

        DailyRecordingManager.getInstance().setBodyWeight(args);
        SubmitDailyRecordingBw parameter = new SubmitDailyRecordingBw(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> callbw = apiInterfaceJson.submitDailyRecordingBw(token2, parameter);

        try {
            String[] node = callbw.execute().body();
            int tempo = Integer.parseInt(node[0]);
            nid = node[0];
            System.out.println("node = "+ tempo);
            return true;
        }
        catch (Exception e){
            System.out.println("error = "+e);
            return false;
        }
    }
}
