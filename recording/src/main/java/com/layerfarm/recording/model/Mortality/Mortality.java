package com.layerfarm.recording.model.Mortality;

import com.google.gson.annotations.SerializedName;

public class Mortality {
    @SerializedName("name")
    String name;
    @SerializedName("value")
    String value;
    public Mortality(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
