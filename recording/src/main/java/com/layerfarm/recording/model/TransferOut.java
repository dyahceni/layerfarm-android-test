package com.layerfarm.recording.model;

public class TransferOut {
    String hatchDate;
    String strain;
    String qty;
    public TransferOut(){

    }

    public String getStrain() {
        return strain;
    }

    public String getHatchDate() {
        return hatchDate;
    }

    public String getQty() {
        return qty;
    }

    public void setStrain(String strain) {
        this.strain = strain;
    }

    public void setHatchDate(String hatchDate) {
        this.hatchDate = hatchDate;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
