package com.layerfarm.recording.model;

import com.google.gson.annotations.SerializedName;
import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingBodyWeight;

public class DailyRecordingManager {
    private static DailyRecordingManager staticInstance;

    public static DailyRecordingManager getInstance(){
        if (staticInstance==null){
            staticInstance = new DailyRecordingManager();
        }

        return staticInstance;
    }
    @SerializedName("daily_recording")
    public DailyRecording dailyRecording = new DailyRecording();
    @SerializedName("body_weight")
    public DailyRecordingBodyWeight bodyWeight = new DailyRecordingBodyWeight();

    public DailyRecording getDailyRecording() {
        return dailyRecording;
    }

    public void setDailyRecording(DailyRecording dailyRecording) {
        this.dailyRecording = dailyRecording;
    }

    public DailyRecordingBodyWeight getBodyWeight() {
        return bodyWeight;
    }

    public void setBodyWeight(DailyRecordingBodyWeight bodyWeight) {
        this.bodyWeight = bodyWeight;
    }
}
