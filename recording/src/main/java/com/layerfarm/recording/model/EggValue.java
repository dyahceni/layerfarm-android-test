package com.layerfarm.recording.model;

public class EggValue {
    long id;
    String name;
    String value_eggs;
    String value_weight;
    public EggValue(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue_eggs() {
        return value_eggs;
    }

    public void setValue_eggs(String value) {
        this.value_eggs = value;
    }

    public String getValue_weight() {
        return value_weight;
    }

    public void setValue_weight(String value_weight) {
        this.value_weight = value_weight;
    }
}
