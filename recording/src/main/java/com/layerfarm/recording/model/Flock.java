package com.layerfarm.recording.model;

public class Flock {
    String flock_name;
    String flock_id;
    String location_name;
    String location_id;
    String type;
    String period;
    String status;
     public Flock(){

     }

    public String getFlock_id() {
        return flock_id;
    }
    public String getFlock_name() {
        return flock_name;
    }
    public String getLocation_id() {
        return location_id;
    }
    public String getLocation_name() {
        return location_name;
    }
    public String getPeriod() {
        return period;
    }
    public String getStatus() {
        return status;
    }
    public String getType() {
        return type;
    }

    public void setFlock_id(String flock_id) {
        this.flock_id = flock_id;
    }
    public void setFlock_name(String flock_name) {
        this.flock_name = flock_name;
    }
    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setType(String type) {
        this.type = type;
    }
}
