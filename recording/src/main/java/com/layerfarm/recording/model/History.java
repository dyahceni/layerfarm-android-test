package com.layerfarm.recording.model;

import java.util.ArrayList;

public class History {
    String date;
    String data;
    ArrayList<Flock> flock_data;
    String recording_id;
    public History(){}

    public String getRecording_id() {
        return recording_id;
    }

    public void setRecording_id(String recording_id) {
        this.recording_id = recording_id;
    }

    public ArrayList<Flock> getFlock_data() {
        return flock_data;
    }

    public void setFlock_data(ArrayList<Flock> flock_data) {
        this.flock_data = flock_data;
    }

    public String getData() {
        return data;
    }

    public String getDate() {
        return date;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
