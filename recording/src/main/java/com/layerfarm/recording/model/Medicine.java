package com.layerfarm.recording.model;

public class Medicine {
    String name;
    String value;
    public Medicine(){

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
