package com.layerfarm.recording.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Log;

import java.security.acl.LastOwnerException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.IllegalFormatCodePointException;
import java.util.concurrent.TimeUnit;

import com.layerfarm.recording.model.HatchDate;
import com.layerfarm.recording.model.TransferIn;
import com.layerfarm.recording.model.TransferOut;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

public class helper {
    DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    public helper(Context context){

        dbHelper = new DatabaseHelper(context);
    }
    //membuat sambungan ke database
    public void open() throws SQLException {
        database= dbHelper.getWritableDatabase();

    }

    //menutup sambungan ke database
    public void close(){

        dbHelper.close();
    }

    public String getDateFlockEmpty(String flockId , String dateRecording){
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);
        if(dateRecording==null||dateRecording.isEmpty()||dateRecording.equals("")){
            dateRecording = today;
        }

        //mencari tanggal kosong terakhir di tabel date_flock_empty
        String Query= "SELECT * FROM date_flock_empty WHERE flock_id = '"+ flockId + "' AND date < '"+dateRecording+"' ORDER BY date DESC LIMIT 1";
        Cursor cursor = database.rawQuery(Query, null);
        String tgl_kosong_terakhir = "";
        if(cursor.moveToFirst())
            tgl_kosong_terakhir  =  cursor.getString(cursor.getColumnIndex("date"));
        cursor.close();
        return tgl_kosong_terakhir;
    }

    public ArrayList<HatchDate> getHatchDate(String flockId, String dateRecording){
        ArrayList<HatchDate> ListHatchDate = new ArrayList<>();
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String today = formatter.format(todayDate);
        if(dateRecording==null||dateRecording.isEmpty()||dateRecording.equals("")){
            dateRecording = today;
            //Log.d("zzz","ini null");
        }
        String flockEmpty = getDateFlockEmpty(flockId, dateRecording);
        Log.d("zzzs","flock empty = "+flockEmpty);

        /*
        String Query = "SELECT DISTINCT hatch_date, name from cache_hatch_date INNER JOIN chick_in on cache_hatch_date.chick_in_id = chick_in.id INNER JOIN \n" +
                "strain on chick_in.strains_id = strain.id where flock_id ='"+flockId+"' AND \n" +
                "state = 1 AND date <= '"+dateRecording+"'";*/
        String Query = "";
        if (flockEmpty.isEmpty()){
            Query = "SELECT DISTINCT hatch_date, name from cache_hatch_date INNER JOIN chick_in on cache_hatch_date.chick_in_id = chick_in.id INNER JOIN \n" +
                    "strain on chick_in.strains_id = strain.id where flock_id ='"+flockId+"' AND timestamp <= '"+dateRecording+"' ORDER BY hatch_date ASC";
        }
        else {
            Query = "SELECT DISTINCT hatch_date, name from cache_hatch_date INNER JOIN chick_in on cache_hatch_date.chick_in_id = chick_in.id INNER JOIN \n" +
                    "strain on chick_in.strains_id = strain.id where flock_id ='"+flockId+"' AND timestamp >= '"+flockEmpty+"' AND timestamp <= '"+dateRecording+"' ORDER BY hatch_date ASC";
        }
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int cc=0; cc < cursor.getCount(); cc++){
            cursor.moveToPosition(cc);
            HatchDate chicken = new HatchDate();
            String hatch_date = cursor.getString(cursor.getColumnIndex("hatch_date"));
            String strain = cursor.getString(cursor.getColumnIndex("name"));
            int total = getTotal(flockId, dateRecording, hatch_date);
            if (total != 0){
                chicken.setHatchDate(hatch_date);
                chicken.setStrain(strain);
                Log.d("zzzs","hatch date isi = "+hatch_date);
                ListHatchDate.add(chicken);
            }
        }
        cursor.close();
        return ListHatchDate;
    }

    //mencari start recording untuk chick in tertentu
    public String getStartRecording(String flockId, String hatchDate){
        //mencari id dari chickin dengan paramater hatchDate
        String Query= "SELECT id FROM chick_in where hatch_date = '"+hatchDate+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String chickinID = "";
        if(cursor.moveToFirst())
            chickinID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();

        //mencari start recording dengan paramater chick_in_id dan floclk_id
        String QueryDistribution= "SELECT * FROM chick_in_distribution where chick_in_id ='"+chickinID+"' and flock_id = '"+flockId+"'";
        Cursor cursorDistribution = database.rawQuery(QueryDistribution, null);
        String startrecording = "";
        if(cursorDistribution.moveToFirst())
            startrecording  =  cursorDistribution.getString(cursorDistribution.getColumnIndex("start_recording"));
        cursorDistribution.close();

        return startrecording;
    }

    public String getDateLastRecording(String flockId){
        //mencari tanggal last recording yg baru saja di update oleh flock
        String Query= "select * from flock where id = '"+flockId+"'";
        Cursor cursor = database.rawQuery(Query, null);
        String lastRecording = "";
        if(cursor.moveToFirst())
            lastRecording  =  cursor.getString(cursor.getColumnIndex("last_recording"));
        cursor.close();
        Log.d("zzzs","last recording = "+lastRecording);

        if (lastRecording==null){
            Log.d("zzzs","getDateLastRecording last recording");
        }
        else
            Log.d("zzzs","getDateLastRecording isi last recording "+lastRecording);

        return lastRecording;
    }

    public String getStartRecordingFlock(String flockID, String lastDate){
        //mencari start recording dengan nilai new = 1
        if (lastDate == null){
            lastDate = "";
        }
        String Query = "SELECT * from cache_hatch_date WHERE flock_id = '"+flockID+"' AND timestamp >= '"+lastDate+"' AND new = 1 ORDER by timestamp ASC LIMIT 1";
        Cursor cursor = database.rawQuery(Query, null);
        String startdate = "";
        if(cursor.moveToFirst())
            startdate  =  cursor.getString(cursor.getColumnIndex("timestamp"));
        cursor.close();
        return startdate;
    }

    public String cekBeforeDate(String flockId, String last_date, String flock_empty){
        // jika flock empty kosong maka kemungkinan adalah recording lanjutan yg pertama
        //pada saat melakukan pengecekan tanggal sebelumnya status yang digunakan hanya new saja,
        //karena bisa jadi pada saat melakukan edit transfer, nilai diubah menjadi 0 yang berarti state berubah menjadi 0
        //padahal recording harus diupdate karena nilai tersebut.
        String date ="";
        //if (flock_empty.isEmpty() || flock_empty == ""){
            String Query= "SELECT * FROM cache_hatch_date where flock_id = '"+flockId+"' and timestamp <= '"+last_date+"' and new = 1 ORDER BY timestamp ASC LIMIT 1";
            Cursor cursor = database.rawQuery(Query, null);
            if(cursor.moveToFirst())
                date  =  cursor.getString(cursor.getColumnIndex("timestamp"));
            cursor.close();
            Log.d("zzzs","cek before flock_empty");
        //}
        /*else {
            String Query= "SELECT * FROM cache_hatch_date where flock_id = '"+flockId+"' and date <= '"+last_date+"' and date > '"+flock_empty+"' " +
                    "and new = 1 ORDER BY date ASC LIMIT 1";
            Cursor cursor = database.rawQuery(Query, null);
            if(cursor.moveToFirst())
                date  =  cursor.getString(cursor.getColumnIndex("date"));
            cursor.close();
            Log.d("zzzs","cek before flock_isi");
        }*/
        Log.d("zzzs","cek before ="+date);
        return date;
    }

    //tanggal mulai recording
    public String getStartRecordingDate(String flockId){
        //cek apakah sudah pernah melakukan recording
        String lastDate = getDateLastRecording(flockId);

        //mendapatkan tanggal kosong untuk flock tersebut
        String flock_empty = getDateFlockEmpty(flockId,"");

        String startdate = "";
        Log.d("zzzs", "start recording --- last Date = "+lastDate);
        Log.d("zzzs", "start recording --- flock empty = "+flock_empty);

        //jika last date kosong, berarti belum pernah melakukan recording, dan ini merupakan recording pertama, maka diambil dari
        //start recording chick in
        if (lastDate == null){
            if (flock_empty.isEmpty()){
                String Query= "SELECT * FROM cache_hatch_date where flock_id = '"+flockId+"' and new = 1 ORDER BY timestamp ASC LIMIT 1";
                Cursor cursor = database.rawQuery(Query, null);
                startdate = "";
                if(cursor.moveToFirst())
                    startdate  =  cursor.getString(cursor.getColumnIndex("timestamp"));
                cursor.close();
                Log.d("zzzs","lastDate flock is empty");
            }
            else {
                String Query= "SELECT * FROM cache_hatch_date where flock_id = '"+flockId+"' and new = 1 and timestamp > '"+flock_empty+"' ORDER BY timestamp ASC LIMIT 1";
                Cursor cursor = database.rawQuery(Query, null);
                startdate = "";
                if(cursor.moveToFirst())
                    startdate  =  cursor.getString(cursor.getColumnIndex("timestamp"));
                cursor.close();
                Log.d("zzzs","lastDate flock is fill");
            }
        }
        //sudah pernah melakukan recording, di cek di cache hatch date jika masih ada hatch date untuk tanggal > last date
        //jika terdapat tanggal hatch date untuk hari tersebut maka akan akan mengecek ada yg baru atau tidak, jika ada yg baru maka
        //date adalah tanggal yg baru tersebut

        //jika sudah pernah melakukan recording, maka akan terdapat last date
        else{
            //jika sudah pernah melakukan recording kemungkinan ada tanggal flock kosong


            //1. cek date sebelumnya apakah ada nilai new yang baru, nilai new ini merepresentasikan adanya chick_in baru atau transfer baru
            // ditanggal sebelum recording, sehingga recording harus kembali ke tanggal tersebut
            // otomatis nilai dari last recording teah terisi karena sudah pernah melakukan recording, dan juga kemungkinan sudah ada
            // data flock kosong
            String cek = cekBeforeDate(flockId,lastDate,flock_empty);
            //jika ada pembaharuan di tanggal sebelum tanggal recording saat ini
            //cek akan memiliki nilai berupa tanggal pembaharuan tersebut

            if (!cek.isEmpty()){
                startdate = cek;
                Log.d("zzzs","cek tidak kosong = ");
            }
            //jika tidak terjadi pembaharuan di tanggal sebelumnya maka start date adalah h+1 dr last date
            else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(lastDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                String output = sdf1.format(c.getTime());
                startdate = output;
                Log.d("zzzs","date plus satu");
                if (getHatchDate(flockId,startdate).size()==0){
                    //mendapatkan tanggal pertama untuk flock ini yang memiliki nilai new = 1
                    String Start = getStartRecordingFlock(flockId, lastDate);
                    startdate = Start;
                }
            }
        }
        Log.d("zzzs","start date = "+startdate);
        return startdate;
    }

    //jumlah semua ayam awal mula berdasarkan hatch date
    /*public int getFirstBirdbyHatchDate(String hatchDate){
        String Query= "select * from chick_in_distribution INNER join chick_in on chick_in.id = chick_in_distribution.chick_in_id " +
                "where chick_in.hatch_date = '"+hatchDate+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        int totalBird = 0;
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        return totalBird;
    }*/

    //jumlah semua ayam by hatch date sampai dengan date
    /*public int getCurrentBirdbyHatchDate(String hatchDate, String dateRecording){
        String Query= "SELECT * from chick_in INNER JOIN  daily_recording_chick_in on chick_in.id = daily_recording_chick_in.chick_in_id\n" +
                "INNER JOIN daily_recording on daily_recording.id = daily_recording_chick_in.daily_recording_id\n" +
                "INNER JOIN recording_mortality on daily_recording.id= recording_mortality.daily_recording_id\n" +
                "and daily_recording_chick_in.chick_in_id = recording_mortality.chick_in_id\n" +
                "where chick_in.hatch_date = '"+hatchDate+"' and daily_recording.recording_date  <= '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        int totalBird = 0;
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        return totalBird;
    }*/

    //total bird by hatch date ini tanpa transfer, karena transfer berarti kan pindah jadi ayam masih ada, hanya pindah
    //total semua ayam di tangggal hatch date tersebut, tidak terpatok flock
    /*public int getTotalBirdByHatchDate(String hatchDate, String dateRecording){
        int total;
        total = getFirstBirdbyHatchDate(hatchDate) - getCurrentBirdbyHatchDate(hatchDate, dateRecording);
        return total;
    }*/

    //mendapatkan jumalah ayam transfer out dari awal recording sampai dengan "date"
    //berdasarkan flock dan hatch date
    /*public int getBirdTransferOut_tillToday(String flockId, String dateRecording, String hatchDate){
        int totalBird =0;
        String tgl_kosong = getDateFlockEmpty(flockId,dateRecording);
        if (!tgl_kosong.isEmpty()){
            String Query= "SELECT * from daily_recording INNER JOIN daily_recording_chick_in on \n" +
                    "daily_recording.id = daily_recording_chick_in.daily_recording_id\n" +
                    "INNER JOIN chick_in on chick_in.id = daily_recording_chick_in.chick_in_id\n" +
                    "INNER join bird_transfer on bird_transfer.daily_recording_id = daily_recording.id AND bird_transfer.chick_in_id = chick_in.id\n" +
                    "where chick_in.hatch_date = '"+hatchDate+"' and daily_recording.flock_id = '"+flockId+"' \n" +
                    "and daily_recording.recording_date > '"+tgl_kosong+"' and daily_recording.recording_date <= '"+dateRecording+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
            }
        }
        else {
            String Query= "SELECT * from daily_recording INNER JOIN daily_recording_chick_in on daily_recording_chick_in.id = daily_recording.id INNER JOIN\n" +
                    "chick_in on daily_recording_chick_in.chick_in_id = chick_in.id INNER JOIN bird_transfer on bird_transfer.daily_recording_id = daily_recording.id\n" +
                    "WHERE chick_in.hatch_date = '"+hatchDate+"' and daily_recording.flock_id = '"+flockId+"'" +
                    "and daily_recording.recording_date <= '"+dateRecording+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
            }
        }
        return totalBird;
    }*/



    //mendapatkan jumlah ayam transfer in dari awal recording sampai dengan "date"
    //berdasarkan flock dan hatch date
    //jumlah ayam yang di pindahkan masuk flock pd hatch date yang bermacam- macam
    /*public ArrayList<TransferIn> getTransferInHatchDate(String flockId, String dateRecording){
        ArrayList<TransferIn> ListTransferIn = new ArrayList<>();
        String flock_empty = getDateFlockEmpty(flockId,dateRecording);
        if (!flock_empty.isEmpty()){
            String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                    "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id INNER JOIN strain on strain.id = chick_in.strains_id " +
                    "WHERE daily_recording.recording_date > '"+flock_empty+"' and daily_recording.recording_date <= '"+dateRecording+"' " +
                    "and bird_transfer.to_flock_id = '"+flockId+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                TransferIn transfer = new TransferIn();
                transfer.setHatchDate(cursor.getString(cursor.getColumnIndex("hatch_date")));
                transfer.setQty(cursor.getString(cursor.getColumnIndex("number_of_bird")));
                transfer.setStrain(cursor.getString(cursor.getColumnIndex("name")));
                ListTransferIn.add(transfer);
            }
        }
        else{
            String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                    "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id INNER JOIN strain on strain.id = chick_in.strains_id " +
                    "WHERE daily_recording.recording_date <= '"+dateRecording+"' and bird_transfer.to_flock_id = '"+flockId+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                TransferIn transfer = new TransferIn();
                transfer.setHatchDate(cursor.getString(cursor.getColumnIndex("hatch_date")));
                transfer.setQty(cursor.getString(cursor.getColumnIndex("number_of_bird")));
                transfer.setStrain(cursor.getString(cursor.getColumnIndex("name")));
                ListTransferIn.add(transfer);
            }
        }
        return ListTransferIn;
    }*/

    //mendapatkan transfer In pada tanggal ditentukan (sbg isi kolom transfer In pada history)
    public ArrayList<TransferIn> getTransferInHatchDatebyDate(String flockId, String dateRecording){
        ArrayList<TransferIn> ListTransferIn = new ArrayList<>();
        String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id INNER JOIN strain on strain.id = chick_in.strains_id " +
                "WHERE daily_recording.recording_date = '"+dateRecording+"' and bird_transfer.to_flock_id = '"+flockId+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            TransferIn transfer = new TransferIn();
            transfer.setHatchDate(cursor.getString(cursor.getColumnIndex("hatch_date")));
            transfer.setQty(cursor.getString(cursor.getColumnIndex("number_of_bird")));
            transfer.setStrain(cursor.getString(cursor.getColumnIndex("name")));
            ListTransferIn.add(transfer);
        }
        return ListTransferIn;
    }

    public ArrayList<TransferOut> getTransferOutHatchDatebyDate(String flockId, String dateRecording){
        ArrayList<TransferOut> ListTransferOut = new ArrayList<>();
        String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id INNER JOIN strain on strain.id = chick_in.strains_id " +
                "WHERE daily_recording.recording_date = '"+dateRecording+"' and daily_recording.flock_id = '"+flockId+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            TransferOut transfer = new TransferOut();
            transfer.setHatchDate(cursor.getString(cursor.getColumnIndex("hatch_date")));
            transfer.setQty(cursor.getString(cursor.getColumnIndex("number_of_bird")));
            transfer.setStrain(cursor.getString(cursor.getColumnIndex("name")));
            ListTransferOut.add(transfer);
        }
        return ListTransferOut;

    }

    //mendapatkan jumlah ayam dari transfer in hari suatu tanggal
    //fungsinya untuk mendata jumlah ayam masuk dari hatch date tertentu sbg penambah jumlah ayam
    public int getBirdTransferInToday(String flockId, String dateRecording, String hatchDate){
        int totalBird =0;
        String flock_empty = getDateFlockEmpty(flockId,dateRecording);
        //karena bisa saja trenjadi transfer in lebih dari 1 pada hari tersebut
        String Query= "SELECT * from daily_recording INNER JOIN bird_transfer on daily_recording.id = bird_transfer.daily_recording_id \n" +
                "INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id WHERE daily_recording.recording_date = '"+dateRecording+"' \n" +
                "and bird_transfer.to_flock_id = '"+flockId+"' AND chick_in.hatch_date = '"+hatchDate+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        Log.d("zzzs","transfer in today = "+totalBird);
        return totalBird;
    }

    //mendapatkan jumlah ayam tranfer out pada hari ini untuk flock tertentu hatch date tertentu
    public int getBirdTransferOutToday(String flockId, String dateRecording, String hatchDate){
        int totalBird = 0;
        String Query= "SELECT * from bird_transfer INNER JOIN daily_recording on daily_recording.id = bird_transfer.daily_recording_id\n" +
                "INNER JOIN chick_in on chick_in.id = bird_transfer.chick_in_id where chick_in.hatch_date = '"+hatchDate+"' \n" +
                "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        return totalBird;
    }

    public int getTotalDistributionBird(String flockID, String dateRecording, String hatchDate){
        //jika burung sudah didefinisikan di dalam menu chick in
        //String flock_empty = getDateFlockEmpty(flockID,dateRecording);
        int bird =0;

        String Query = "SELECT * FROM chick_in_distribution INNER JOIN chick_in on chick_in.id = chick_in_distribution.chick_in_id\n" +
                "WHERE hatch_date = '"+hatchDate+"' AND flock_id = '"+flockID+"' AND start_recording = '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        if(cursor.moveToFirst())
            bird  =  cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        cursor.close();

        Log.d("zzzs","ayam distribusi ="+bird);

        return bird;
    }

    //mendapatkan mortality untuk tanggal, flock dan hatch date tertentu
    public int getMortalityToday(String flockId, String dateRecording, String hatchDate){
        int totalBird = 0;
        String Query= "SELECT * from recording_mortality INNER JOIN daily_recording on daily_recording.id = recording_mortality.daily_recording_id\n" +
                "INNER JOIN chick_in on chick_in.id = recording_mortality.chick_in_id where chick_in.hatch_date = '"+hatchDate+"'" +
                "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        }
        return totalBird;
    }

    //mendapatkan spent hen today
    public int getSpentHenToday(String flockId, String dateRecording, String hatchDate){
        int totalBird = 0;
        String query = "SELECT number_of_bird FROM recording_chick_out INNER JOIN daily_recording on daily_recording.id = recording_chick_out.daily_recording_id\n" +
                "INNER JOIN chick_in on recording_chick_out.chick_in_id = chick_in.id WHERE chick_in.hatch_date = '"+hatchDate+"' AND\n" +
                "daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst())
            totalBird = cursor.getInt(cursor.getColumnIndex("number_of_bird"));
        return totalBird;
    }
    /*public int getMortality_tillToday(String flockId, String dateRecording, String hatchDate){
        int totalBird =0;
        String tgl_kosong = getDateFlockEmpty(flockId,dateRecording);
        if (!tgl_kosong.isEmpty()){
            String Query= "SELECT * from recording_mortality INNER JOIN daily_recording on daily_recording.id = recording_mortality.daily_recording_id\n" +
                    "INNER JOIN chick_in on chick_in.id = recording_mortality.chick_in_id where chick_in.hatch_date = '"+hatchDate+"'" +
                    "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date <= '"+dateRecording+"' AND " +
                    "daily_recording.recording_date > '"+tgl_kosong+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
            }
        }
        else {
            String Query= "SELECT * from recording_mortality INNER JOIN daily_recording on daily_recording.id = recording_mortality.daily_recording_id\n" +
                    "INNER JOIN chick_in on chick_in.id = recording_mortality.chick_in_id where chick_in.hatch_date = '"+hatchDate+"'" +
                    "and daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date <= '"+dateRecording+"'";
            Cursor cursor = database.rawQuery(Query, null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                totalBird = totalBird + cursor.getInt(cursor.getColumnIndex("number_of_bird"));
            }
        }
        return totalBird;
    }*/

    /*public int PengurangHatchDate(String flockID, String dateRecording, String hatchDate){
        int pengurang = 0;
        pengurang = getBirdTransferOutToday(flockID,dateRecording,hatchDate)+getMortalityToday(flockID,dateRecording,hatchDate);
        return pengurang;
    }*/

    public int getTotalBird_onCache(String flockId, String dateRecording, String hatchDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateRecording));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String day_before = sdf1.format(c.getTime());
        int total =0;
        //mencari recording dengan tanggal tersebut
        String Query= "SELECT * FROM cache_total_bird INNER JOIN daily_recording on cache_total_bird.daily_recording_id = daily_recording.id \n" +
                "INNER JOIN chick_in on cache_total_bird.chick_in_id = chick_in.id WHERE \n" +
                "daily_recording.flock_id = '"+flockId+"' AND daily_recording.recording_date = '"+day_before+"' AND chick_in.hatch_date = '"+hatchDate+"' ";
        Cursor cursor = database.rawQuery(Query, null);
        if(cursor.moveToFirst())
            total  =  cursor.getInt(cursor.getColumnIndex("total_bird"));
        cursor.close();
        Log.d("zzzs","cache bird = "+total);
        return total;
    }

    //mendapatkan total bird untuk tanggal tersebut
    public int getTotal(String flockId, String dateRecording, String hatchDate){
        /*
        int awal = getTotalDistributionBird(flockId, dateRecording,hatchDate);
        int pengurang = getBirdTransferOut_tillToday(flockId,dateRecording,hatchDate) + getMortality_tillToday(flockId,dateRecording,hatchDate);
        int total = awal - pengurang;*/

        //melakukan pengecekan di dalam tabel cache_total_bird untuk mendapatkan jumlah ayam per hatch date
        //int bird =0;
        //1. cek di tabel cache_total_bird h-1
        int bird_on_cache = getTotalBird_onCache(flockId,dateRecording,hatchDate);
        //bird dari distribusi, jika kemungkinan ada distribusi pada hari tersebut
        int bird_distribution = getTotalDistributionBird(flockId,dateRecording,hatchDate);
        /*
        //bird on cache sama dengan 0 atau tidak ada data maka kemungkinan adalah pertama kali melakukan recording maka ambil dari
        //cache_hatch_date
        if (bird_on_cache != 0){
            bird = bird_on_cache;
            Log.d("zzzs", "bird cache nggak kosong");
        }
        else{
            //mengecek pada distribusi chick in
            Log.d("zzzs", "bird cache kosong");
            int awal = getTotalDistributionBird(flockId,dateRecording,hatchDate);
            bird = awal;
        }
        //jika tidak terdefinisi di dalam chick in ditribution berarti transfer in
        /*
        if (bird==0){
            bird = getBirdTransferInToday(flockId,dateRecording,hatchDate);
            Log.d("history","bird kosong: "+bird);
        }*/

        //2. mendapatkan transfer in dengan hatch date tersebut
        int transfer_in = getBirdTransferInToday(flockId,dateRecording,hatchDate);

        int total = bird_on_cache + bird_distribution + transfer_in;

        return total;
    }

    //mendapatkan usia dari ayam
    public  String getBirdAgeByHatchDate(String hatchDate, String dateRecording){
        long bedaHari=0;
        long hari =0;
        try {
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            Date tglAwal = (Date) date.parse(hatchDate);
            Date tglAkhir = (Date) date.parse(dateRecording);

            bedaHari = Math.abs(tglAkhir.getTime() - tglAwal.getTime());
            hari = TimeUnit.MILLISECONDS.toDays(bedaHari);
            //Log.d("zzzs","beda hari: "+hari);
        }
        catch (Exception e){}

        long minggu = hari/7;
        long sisa = hari%7;
        String hasil = "hari: "+minggu+" week(s) "+sisa+" day(s)";
        return hasil;
    }
    public String gethatchDateMaxBird(String flockId){
        String flock_empty = getDateFlockEmpty(flockId,"");
        String Query = "";
        if (flock_empty.isEmpty()) {
            Query = "SELECT chick_in.hatch_date as dateMax, MAX(chick_in_distribution.number_of_bird) FROM chick_in\n" +
                    "INNER JOIN chick_in_distribution on chick_in_distribution.chick_in_id = chick_in.id \n" +
                    "WHERE flock_id = '" + flockId + "' and chick_in_distribution.number_of_bird > 0";
        }
        else {
            Query = "SELECT chick_in.hatch_date as dateMax, MAX(chick_in_distribution.number_of_bird) FROM chick_in\n" +
                    "INNER JOIN chick_in_distribution on chick_in_distribution.chick_in_id = chick_in.id \n" +
                    "WHERE flock_id = '" + flockId + "' and chick_in_distribution.number_of_bird > 0 AND " +
                    "chick_in_distribution.start_recording > '"+flock_empty+"'";
        }
        Cursor cursor = database.rawQuery(Query, null);
        String hatchDateMax = "";
        if(cursor.moveToFirst())
            hatchDateMax  =  cursor.getString(cursor.getColumnIndex("dateMax"));
        cursor.close();
        Log.d("zzzs","hatchDate = "+hatchDateMax);
        return hatchDateMax;
    }
    public ArrayList<Integer> ageXData (String flockId, String recordingDate){
        ArrayList<Integer> ageXData = new ArrayList<>();
        String tgl_terbanyak = gethatchDateMaxBird(flockId);

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String inputString2 = date;
        if (tgl_terbanyak == null){
            tgl_terbanyak = inputString2;
        }
        if (recordingDate == null){
            recordingDate = tgl_terbanyak;
        }

        try {
            Date date1 = myFormat.parse(tgl_terbanyak);
            Date date2 = myFormat.parse(recordingDate);
            long diff = date2.getTime() - date1.getTime();
            long ageChickin = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            Log.d("zzzs","hatchDate = "+ageChickin);
            Integer daysHatch = (int) ageChickin;

            for (int i = 1; i <= daysHatch; i++) {
                ageXData.add(i);
                System.out.println(ageXData.get(i - 1));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ageXData;
    }

    //mendapatkan jumlah total ayam untuk tiap tanggal recording, per flock
    public int getTotalBird(String flockId, String dateRecording){
        String Query = "SELECT total_bird FROM cache_total_bird INNER JOIN daily_recording on daily_recording.id = cache_total_bird.daily_recording_id\n" +
                "WHERE flock_id = '"+flockId+"' AND recording_date = '"+dateRecording+"'";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        int total = 0;
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            total += cursor.getInt(cursor.getColumnIndex("total_bird"));
        }
        return total;
    }


}
