package com.layerfarm.recording.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static final String db_name = "layerfarm-mobile.db";
    private static final int db_version = 1;

    public static final String recording_productionSQL = "CREATE TABLE IF NOT EXISTS recording_production(id INTEGER PRIMARY KEY, daily_recording_id integer, " +
            "egg_quality_id integer, egg_number varchar(45), egg_weight varchar(45));";


    public DBHelper(Context context) {

        super(context, db_name, null, db_version);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(recording_productionSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(),"Upgrading database from version"+oldVersion+"to"
        +newVersion+", which will destroy all old data");
        db.execSQL(recording_productionSQL);
        //onCreate(db);
    }
}
