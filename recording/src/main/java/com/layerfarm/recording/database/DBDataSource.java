package com.layerfarm.recording.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingFeed;
import com.layerfarm.layerfarm.model.DailyRecordingOvk;
import com.layerfarm.layerfarm.model.DailyRecordingSpentHen;
import com.layerfarm.layerfarm.model.DailyRecordingTransfer;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.EggValue;
import com.layerfarm.recording.model.Feed;
import com.layerfarm.recording.model.HatchDate;
import com.layerfarm.recording.model.Medicine;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.recording.model.Mortality.MortalityData;
import com.layerfarm.recording.model.SpentHen;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBDataSource {
    //inisialisasi SQLite Database
    private SQLiteDatabase database;

    //inisialisasi kelas helper
    private DatabaseHelper db;
    helper help;

    //mengambil semua nama kolom
    private String[] allColumns = {"id",
            "recording_date", "flock_id",
            "light_intensity", "light_duration_start", "light_duration_end",
            "cage_temperature_1", "cage_temperature_2", "cage_temperature_3",
            "humidity_1", "humidity_2", "humidity_3", "note"};

    //DBHelper diinstantiasi pada constructor
    public DBDataSource(Context context)
    {

        db = new DatabaseHelper(context);
        help = new helper(context);
        help.open();
    }

    //membuat sambungan ke database
    public void open() throws SQLException {
        database= db.getWritableDatabase();
    }

    //menutup sambungan ke database
    public void close(){

        db.close();
    }

    //methhod untuk create/ insert ke database
    public void create_recording(String recording_date, String flockId, DailyRecording dailyRecording){
        long ID = getRecordingID(flockId, recording_date);
        if (ID == 0){
            createDailyRecording(recording_date, flockId);
            save_recording(recording_date, flockId, dailyRecording);
            Log.d("Layerfarm","ID belum ada");
        }else {
            delete_from_recording_entry(ID);
            save_recording(recording_date, flockId, dailyRecording);
            Log.d("Layerfarm","ID sudah ada");
        }


    }
    public void save_recording(String recording_date, String flockId, DailyRecording dailyRecording){
        long recording_ID = getRecordingID(flockId, recording_date);
        createEggValue(dailyRecording, recording_ID);
        createFeed(dailyRecording, recording_ID);
        createMedicine(dailyRecording, recording_ID);
        createTransfer(dailyRecording, recording_ID);
        createSpentHen(dailyRecording, recording_ID);
        //createMortality(dailyRecording, recording_ID);


    }
    public void create_note(long id, String note){
        ContentValues notes = new ContentValues();
        notes.put("note", note);

        String Filter = "id =" + id;
        //update query
        long updateRecording = database.update("daily_recording", notes, Filter, null);
    }
    public void createLastRecording (String flockID, String dateRecording){
        //String selectQuery= "SELECT id FROM last_recording WHERE flock_id = '"+ flockID + "' LIMIT 1";
        String selectQuery= "SELECT * FROM flock WHERE id = '"+ flockID + "' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String lastdate = "";
        if(cursor.moveToFirst())
            lastdate  =  cursor.getString(cursor.getColumnIndex("last_recording"));
        cursor.close();

        String Filter = "id=" + flockID;
        //memasukkan ke content values
        ContentValues update = new ContentValues();
        //masukkan data sesuai dengan kolom pada database
        update.put("last_recording", dateRecording);
        //update query
        long updatelast = database.update("flock", update, Filter, null);

    }

    //methhod untuk create/ insert ke database
    public void createDailyRecording(String recording_date, String flockId){
        //membuat content value untuk memasangkan data dengan nama di kolom database
        ContentValues daily_values = new ContentValues();
        //daily_values.put("note", note);
        daily_values.put("recording_date", recording_date);
        daily_values.put("flock_id", flockId);
        Log.d("zzzs","flock id create recording"+flockId);

        //mengeksekusi perintah sql insert data
        //yang mengembalikan sebuah insert ID
        long recording_ID = database.insert("daily_recording",null,daily_values);
    }
    public void createEggValue(DailyRecording dailyRecording, long id){
        HashMap<String, String> production = dailyRecording.getProduction();
        for (Map.Entry<String, String> entry : production.entrySet()) {
            //RECORDING Production
            ContentValues recording_production = new ContentValues();
            recording_production.put("daily_recording_id", id);
            recording_production.put("egg_quality_id", entry.getKey());
            recording_production.put("egg_number", entry.getValue());
            long insertFeed = database.insert("recording_production", null, recording_production);
        }
    }
    public void createFeed(DailyRecording dailyRecording, long id){
        ArrayList<DailyRecordingFeed> Feed = dailyRecording.getFeed();
        for (int c =0; c<Feed.size();c++){
            DailyRecordingFeed feed = new DailyRecordingFeed();
            String feed_nid = Feed.get(c).getFeed_id();
            String value = Feed.get(c).getAmount();

            //RECORDING FEED
            ContentValues recording_feed = new ContentValues();
            recording_feed.put("daily_recording_id", id);
            recording_feed.put("feed_id", feed_nid);
            recording_feed.put("feed_amount", value);
            long insertFeed = database.insert("recording_feed_consumption", null, recording_feed);

        }
    }
    public void createMedicine(DailyRecording dailyRecording, long id){
        ArrayList<DailyRecordingOvk> Medicine = dailyRecording.getOvk();
        for (int c =0; c<Medicine.size();c++){
            DailyRecordingOvk medicine = new DailyRecordingOvk();
            String ovk_nid = Medicine.get(c).getOvk_id();
            String value = Medicine.get(c).getAmount();

            //RECORDING MedicineVaccination
            ContentValues recording_medicine = new ContentValues();
            recording_medicine.put("daily_recording_id", id);
            recording_medicine.put("medication_vaccination_id", ovk_nid);
            recording_medicine.put("amount_used", value);
            long insertFeed = database.insert("recording_medication_vaccination", null, recording_medicine);
        }
    }
    public void createTransfer(DailyRecording dailyRecording, long id){
        ArrayList<DailyRecordingTransfer> ListTransfer = dailyRecording.getPindahKandang();
        for (int i =0; i< ListTransfer.size(); i++){
            //RECORDING TRANSFER
            ContentValues recording_transfer = new ContentValues();
            recording_transfer.put("daily_recording_id", id);
            recording_transfer.put("chick_in_id", ListTransfer.get(i).getHatch_date_move());
            recording_transfer.put("to_flock_id", ListTransfer.get(i).getTo_house());
            recording_transfer.put("number_of_bird", ListTransfer.get(i).getNumber_hen_move());
            long insertBirdTansfer = database.insert("bird_transfer", null, recording_transfer);
        }
    }
    public void createSpentHen(DailyRecording dailyRecording, long id){
        ArrayList<DailyRecordingSpentHen> SpentHen = dailyRecording.getAfkir();
        for (int c =0; c<SpentHen.size();c++){
            DailyRecordingSpentHen spent_hent = SpentHen.get(c);
            int chick_in = spent_hent.getHatch_date_move();
            String total_bird = spent_hent.getJumlah_afkir();
            String total_weight = spent_hent.getAfkir_weight();

            //RECORDING Spent Hen
            ContentValues recording_spentHen = new ContentValues();
            recording_spentHen.put("daily_recording_id", id);
            recording_spentHen.put("chick_in_id", chick_in);
            recording_spentHen.put("number_of_bird", total_bird);
            recording_spentHen.put("total_weight", total_weight);
            long insert = database.insert("recording_chick_out", null, recording_spentHen);
        }
    }
    public void createDaily_recording_chick_in(ArrayList<HatchDate> hatch, long id){
        for (int i =0; i< hatch.size(); i++){
            //Get chick_in id
            String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch.get(i).getHatchDate() + "' LIMIT 1";
            Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
            String chick_in_ID = "";
            if(cursorChickIn.moveToFirst())
                chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
            cursorChickIn.close();

            //RECORDING chick_in
            ContentValues recording_production = new ContentValues();
            recording_production.put("daily_recording_id", id);
            recording_production.put("chick_in_id", chick_in_ID);
            long insertFeed = database.insert("daily_recording_chick_in", null, recording_production);
        }
    }

    public void createMortality(ArrayList<MortalityData> MortalityData , long id){
        int total = 0;
        for (int b =0; b< MortalityData.size(); b++){
            com.layerfarm.recording.model.Mortality.MortalityData mortalityData = MortalityData.get(b);
            String hatch_date = mortalityData.getHatch_date();
            Log.d("zzzs","hatch date ="+hatch_date);

            //Get chick_in id
            String selectQueryChickIn= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch_date + "' LIMIT 1";
            Cursor cursorChickIn = database.rawQuery(selectQueryChickIn, null);
            String chick_in_ID = "";
            if(cursorChickIn.moveToFirst())
                chick_in_ID  =  cursorChickIn.getString(cursorChickIn.getColumnIndex("id"));
            cursorChickIn.close();


            for (int a = 0; a<mortalityData.getMortality().size(); a++){
                Mortality mor = MortalityData.get(b).getMortality().get(a);
                String name = mor.getName();
                String value = mor.getValue();
                total = Integer.parseInt(value)+total;
                //Get mortality_category id
                String selectQueryMortalityCategory= "SELECT id FROM mortality_category WHERE name = '"+ name + "' LIMIT 1";
                Cursor cursorMortalityCategory = database.rawQuery(selectQueryMortalityCategory, null);
                String MortalityCategory_ID = "";
                if(cursorMortalityCategory.moveToFirst())
                    MortalityCategory_ID  =  cursorMortalityCategory.getString(cursorMortalityCategory.getColumnIndex("id"));
                cursorMortalityCategory.close();

                //RECORDING Mortality
                ContentValues recording_mortality = new ContentValues();
                recording_mortality.put("daily_recording_id", id);
                recording_mortality.put("chick_in_id", chick_in_ID);
                recording_mortality.put("mortality_category_id", MortalityCategory_ID);
                recording_mortality.put("number_of_bird", value);
                long insertMortality = database.insert("recording_mortality", null, recording_mortality);
            }
        }
    }
    public void createCacheMortality(long id){
        String value = "";
        //mendapatkan jumlah mortality dari recording id
        String selectQueryMortality= "SELECT * FROM recording_mortality WHERE daily_recording_id = '"+ id + "'";
        Cursor cursorMortality = database.rawQuery(selectQueryMortality, null);
        int total =0;
        for (int i = 0; i<cursorMortality.getCount(); i++){
            cursorMortality.moveToPosition(i);
            total= total+ cursorMortality.getInt(cursorMortality.getColumnIndex("number_of_bird"));
        }
        value += "total_"+total+"/";

        Cursor cursor = database.rawQuery("SELECT * FROM mortality_category",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            long mortalityID = cursor.getInt(cursor.getColumnIndex("id"));
            String selectMortalityCategory = "SELECT * FROM recording_mortality WHERE daily_recording_id = '" + id + "' AND mortality_category_id = '" + mortalityID + "'";
            Cursor cursorMortalityCategory = database.rawQuery(selectMortalityCategory, null);
            int nilai = 0;
            for (int j = 0; j < cursorMortalityCategory.getCount(); j++) {
                cursorMortalityCategory.moveToPosition(j);
                nilai = nilai + cursorMortalityCategory.getInt(cursorMortalityCategory.getColumnIndex("number_of_bird"));
            }
            value += mortalityID + "_" + nilai + "/";
        }
        Log.d("zzzs","nilai = "+value);

        //cek apakah cacahe dengan tanggal recording dan flock ini sudah ada di table cache_mortality?
        String selectQueryCacheMortality= "SELECT id FROM cache_mortality WHERE daily_recording_id = '"+ id + "' LIMIT 1";
        Cursor cursorCacheMortality = database.rawQuery(selectQueryCacheMortality, null);
        long ID = 0;
        if(cursorCacheMortality.moveToFirst())
            ID  =  cursorCacheMortality.getLong(cursorCacheMortality.getColumnIndex("id"));
        cursorCacheMortality.close();
        //jika ID =0, berarti belum ada di cache
        if (ID == 0){
            ContentValues cache_mortality = new ContentValues();
            //daily_values.put("note", note);
            cache_mortality.put("daily_recording_id", id);
            cache_mortality.put("value", value);

            //mengeksekusi perintah sql insert data
            //yang mengembalikan sebuah insert ID
            long insertID = database.insert("cache_mortality",null,cache_mortality);
        }
    }
    public void createEggValue(ArrayList<EggValue> eggValues, long id){
        for (int i =0; i<eggValues.size(); i++){
            EggValue eggs = eggValues.get(i);
            String name = eggs.getName();
            String value_eggs = eggs.getValue_eggs();
            String value_weight = eggs.getValue_weight();
            Log.d("zzzs","egg quality_name: "+name+"hola");
            //Get egg_quality id
            String selectQueryEggQuality= "SELECT id FROM egg_quality WHERE name = '"+ name + "' LIMIT 1";
            Cursor cursorEggQuality = database.rawQuery(selectQueryEggQuality, null);
            String egg_quality_ID = "";
            if(cursorEggQuality.moveToFirst())
                egg_quality_ID  =  cursorEggQuality.getString(cursorEggQuality.getColumnIndex("id"));
            cursorEggQuality.close();
            Log.d("zzzs","id quality: "+egg_quality_ID);

            //RECORDING Production
            ContentValues recording_production = new ContentValues();
            recording_production.put("daily_recording_id", id);
            recording_production.put("egg_quality_id", egg_quality_ID);
            recording_production.put("egg_number", value_eggs);
            recording_production.put("egg_weight", value_weight);
            long insertFeed = database.insert("recording_production", null, recording_production);

        }
    }

    public void createDateFlockEmpty(String flockId, String dateRecording){
        String selectQuery= "SELECT * FROM date_flock_empty WHERE flock_id = '"+flockId+"' AND date = '"+dateRecording+"' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        if (ID.isEmpty()){
            //RECORDING Production
            ContentValues recording_production = new ContentValues();
            recording_production.put("flock_id", flockId);
            recording_production.put("date", dateRecording);
            long insertFlockEmpty = database.insert("date_flock_empty", null, recording_production);
        }
    }
    public void deleteDateFlockEmpty(String flockId, String dateRecording){
        String selectQuery= "SELECT * FROM date_flock_empty WHERE flock_id = '"+flockId+"' AND date = '"+dateRecording+"' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        if (!ID.isEmpty()){
            String query_transfer = "DELETE FROM date_flock_empty WHERE id = '"+ID+"'";
            database.execSQL(query_transfer);
        }
    }
    public void create_cache_total_bird(String flockID, String dateRecording, long daily_recordingID, String hatch_date){
        int transfer_in = help.getBirdTransferInToday(flockID,dateRecording,hatch_date);
        int transfer_out = help.getBirdTransferOutToday(flockID, dateRecording, hatch_date);
        int mortality = help.getMortalityToday(flockID, dateRecording, hatch_date);
        int spent_hen = help.getSpentHenToday(flockID, dateRecording, hatch_date);

        //total adalah pengurangan antara jumlah ayam-transfer out-mortality
        int total = count_total_bird_by_hatch_date(flockID,dateRecording,hatch_date);

        //mendapatkan chick_in ID
        long chick_inID = getChickInID(hatch_date);
        /*
        Log.d("zzzs","recording date = "+dateRecording);
        Log.d("zzzs","recording id = "+daily_recordingID);
        Log.d("zzzs","hatch date = "+hatch_date);
        */
        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT cache_total_bird.id FROM cache_total_bird INNER JOIN chick_in on chick_in.id = cache_total_bird.chick_in_id " +
                "WHERE daily_recording_id = '"+daily_recordingID+"' and hatch_date= '"+hatch_date+"' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        Log.d("zzzs","ID = "+ID);
        //jika belum ada recording untuk recording ID dan hatch date tersebut maka dilakukan insert
        if (ID.isEmpty()){
            Log.d("zzzs","insert cache_total_bird");
            ContentValues cache_total_bird = new ContentValues();
            cache_total_bird.put("daily_recording_id", daily_recordingID);
            cache_total_bird.put("chick_in_id", chick_inID);
            cache_total_bird.put("transfer_in", transfer_in);
            cache_total_bird.put("transfer_out", transfer_out);
            cache_total_bird.put("mortality", mortality);
            cache_total_bird.put("chick_out", spent_hen);
            cache_total_bird.put("total_bird", total);
            long insertCache = database.insert("cache_total_bird", null, cache_total_bird);
        }
        //jika sudah pernah melakukan recording
        else {
            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("daily_recording_id", daily_recordingID);
            update.put("chick_in_id", chick_inID);
            update.put("transfer_in", transfer_in);
            update.put("transfer_out", transfer_out);
            update.put("mortality", mortality);
            update.put("chick_out", spent_hen);
            update.put("total_bird", total);
            //update query
            long updateCache = database.update("cache_total_bird", update, Filter, null);
            Log.d("zzzs","id = "+ID);
            Log.d("zzzs","daily_rec_id = "+daily_recordingID);
            Log.d("zzzs","chick in id= "+chick_inID);
            Log.d("zzzs","transfer in = "+transfer_in);
            Log.d("zzzs","transfer out = "+transfer_out);
            Log.d("zzzs","mortality = "+mortality);
            Log.d("zzzs","chick out = "+spent_hen);
            Log.d("zzzs","total_bird = "+total);
        }
    }
    public void create_sale_order_line(long sale_orderID, int value, int harga){
        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT * FROM sale_order_line WHERE order_id = '"+sale_orderID+"' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        Log.d("zzzs","ID = "+ID);
        int total = harga * value;
        //jika belum ada recording untuk recording ID tersebut
        if (ID.isEmpty()){
            ContentValues sale_order = new ContentValues();
            sale_order.put("order_id", sale_orderID);
            sale_order.put("price_unit", harga);
            sale_order.put("product_uom_qty", value);
            sale_order.put("price_subtotal", total);
            sale_order.put("product_id", "1");
            sale_order.put("price_total", total);
            sale_order.put("name", "Ayam");
            long insert = database.insert("sale_order_line", null, sale_order);
        }
        //jika sudah pernah melakukan recording
        else {
            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("order_id", sale_orderID);
            update.put("price_unit", harga);
            update.put("product_uom_qty", value);
            update.put("price_subtotal", total);
            update.put("product_id", "1");
            update.put("price_total", total);
            update.put("name", "Ayam");
            //update query
            long updateCache = database.update("sale_order_line", update, Filter, null);
        }
    }
    public long create_sale_order(long daily_recordingID, int total, String flockID){
        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT * FROM sale_order WHERE origin = '"+daily_recordingID+"' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        Log.d("zzzs","ID = "+ID);
        //jika belum ada recording untuk recording ID tersebut
        long SO_id = 0;
        if (ID.isEmpty()){
            ContentValues insert = new ContentValues();
            insert.put("amount_total", total);
            insert.put("name", daily_recordingID);
            insert.put("warehouse_id", flockID);
            long Insert = database.insert("sale_order", null, insert);
            SO_id = Insert;
        }
        //jika sudah pernah melakukan recording
        else {
            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("amount_total", total);
            update.put("name", daily_recordingID);
            update.put("warehouse_id", flockID);
            //update query
            long Update = database.update("sale_order", update, Filter, null);
            SO_id = Update;
        }
        return SO_id;
    }

    public void create_stock_move(String origin, int harga, int qty, String src_flockID, String dest_flockID, String scrap){

        if (dest_flockID != "0" && !dest_flockID.equals("Mortality")){
            String house[] = dest_flockID.split(" - ");
            String flock= house[1];
            //Get toFlock ID
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ flock + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String FlockID = "";
            if(cursorFlock.moveToFirst())
                FlockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();
            dest_flockID = FlockID;
        }
        if (dest_flockID.equals("Mortality")){
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ dest_flockID + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String FlockID = "";
            if(cursorFlock.moveToFirst())
                FlockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();
            dest_flockID = FlockID;
        }


        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT * FROM stock_move WHERE origin = '"+origin+"' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        //jika belum ada recording untuk recording ID tersebut
        if (ID.isEmpty()){
            ContentValues insert = new ContentValues();
            insert.put("origin", origin);
            insert.put("price_unit", harga);
            insert.put("product_qty", qty);
            insert.put("location_id", src_flockID);
            insert.put("warehouse_id", src_flockID);
            insert.put("scrapped", scrap);
            insert.put("product_id", 1);
            insert.put("name", "Ayam");
            //berisi destinasi id untk customer, harus di define di stock.location
            insert.put("location_dest_id", dest_flockID);
            long Insert = database.insert("stock_move", null, insert);
        }
        //jika sudah pernah melakukan recording
        else {
            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("origin", origin);
            update.put("price_unit", harga);
            update.put("product_qty", qty);
            update.put("location_id", src_flockID);
            update.put("warehouse_id", src_flockID);
            update.put("scrapped", scrap);
            update.put("product_id", 1);
            update.put("name", "Ayam");
            //berisi destinasi id untk customer, harus di define di stock.location
            update.put("location_dest_id", dest_flockID);
            //update query
            long Update = database.update("stock_move", update, Filter, null);
        }
    }
    public void create_stock_picking(String origin, String src_flockID, String dest_flockID, String flock_name){
        if (dest_flockID != "0" && !dest_flockID.equals("Mortality")){
            String house[] = dest_flockID.split(" - ");
            String flock= house[1];
            //Get toFlock ID
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ flock + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String FlockID = "";
            if(cursorFlock.moveToFirst())
                FlockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();
            dest_flockID = FlockID;
        }
        if (dest_flockID.equals("Mortality")){
            String selectQueryFlock= "SELECT id FROM flock WHERE name = '"+ dest_flockID + "' LIMIT 1";
            Cursor cursorFlock = database.rawQuery(selectQueryFlock, null);
            String FlockID = "";
            if(cursorFlock.moveToFirst())
                FlockID  =  cursorFlock.getString(cursorFlock.getColumnIndex("id"));
            cursorFlock.close();
            dest_flockID = FlockID;
        }

        //cari di cache_total_bird apakah sudah pernah melakukan recording untuk ini atau belum, jika belum maka lakukan insert
        String selectQuery= "SELECT * FROM stock_picking WHERE origin = '"+origin+"' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String ID = "";
        if(cursor.moveToFirst())
            ID  =  cursor.getString(cursor.getColumnIndex("id"));
        cursor.close();
        String name = flock_name+"/OUT/"+ID;
        //jika belum ada recording untuk recording ID tersebut
        if (ID.isEmpty()){
            ContentValues insert = new ContentValues();
            insert.put("origin", origin);
            insert.put("location_id", src_flockID);
            insert.put("name", name);
            insert.put("location_dest_id", dest_flockID);
            //berisi destinasi id untk customer, harus di define di stock.location
            insert.put("location_dest_id", dest_flockID);
            long Insert = database.insert("stock_picking", null, insert);
        }
        //jika sudah pernah melakukan recording
        else {
            Log.d("zzzs","update cache_total_bird");
            //ambil id
            String Filter = "id=" + ID;
            //memasukkan ke content values
            ContentValues update = new ContentValues();
            //masukkan data sesuai dengan kolom pada database
            update.put("origin", origin);
            update.put("location_id", src_flockID);
            update.put("name", name+ID);
            update.put("location_dest_id", dest_flockID);
            //update query
            long Update = database.update("stock_picking", update, Filter, null);
        }
    }

    public int count_total_bird_by_hatch_date(String flockID, String dateRecording, String hatch_date){
        int transfer_out = help.getBirdTransferOutToday(flockID, dateRecording, hatch_date);
        int mortality = help.getMortalityToday(flockID, dateRecording, hatch_date);
        int spent_hen = help.getSpentHenToday(flockID, dateRecording, hatch_date);

        //bird_qty ini merupakan jumlah dari ayam recording sebelumnya dengan transfer in untuk tanggal chick in ini, atau
        //jumlah dari jumlah ayam yg didistribusikan + transfer in
        int bird_qty = help.getTotal(flockID,dateRecording,hatch_date);

        //total adalah pengurangan antara jumlah ayam-transfer_out-mortality
        int total = bird_qty - transfer_out - mortality - spent_hen;
        Log.d("zzzs","bird quantity ="+bird_qty);
        Log.d("zzzs","transfer out ="+transfer_out);
        Log.d("zzzs","mortality ="+mortality);
        Log.d("zzzs","spent hen ="+spent_hen);

        return total;
    }

    //fungsi ini untuk mengecek jumlah total dari dari masing2 hatch date, jika kosong maka akan di
    // masukkan kedalam date flock empty
    public void count_totalBird_flock(String flockID, String dateRecording, long daily_recordingID){
        int total=0;
        String query = "SELECT * FROM cache_total_bird WHERE daily_recording_id = '"+daily_recordingID+"'";
        Cursor cursor = database.rawQuery(query, null);
        for (int i = 0; i<cursor.getCount(); i++){
            cursor.moveToPosition(i);
            total= total+ cursor.getInt(cursor.getColumnIndex("total_bird"));
        }
        if (total == 0){
            createDateFlockEmpty(flockID,dateRecording);
        }
        else
            deleteDateFlockEmpty(flockID,dateRecording);
    }

    //DELETE UNTUK MELAKUKAN INSERT ULANG / EDIT
    public void delete_from_recording_entry(long id){
        String query_production = "DELETE FROM recording_production WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_production);
        String query_feed = "DELETE FROM recording_feed_consumption WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_feed);
        String query_ovk = "DELETE FROM recording_medication_vaccination WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_ovk);
        String query_mortality = "DELETE FROM recording_mortality WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_mortality);

        String query_chick_in = "DELETE FROM daily_recording_chick_in WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_chick_in);
        String cache_total_bird = "DELETE FROM cache_total_bird WHERE daily_recording_id = '"+id+"'";
        database.execSQL(cache_total_bird);
    }
    //DELETE UNTUK INSERT ULANG/ EDIT MENU TRANSFER
    public void  delete_from_transfer_entry(long id){
        String query_transfer = "DELETE FROM bird_transfer WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_transfer);
        String query_spent_hen = "DELETE FROM recording_chick_out WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_spent_hen);

        String query_chick_in = "DELETE FROM daily_recording_chick_in WHERE daily_recording_id = '"+id+"'";
        database.execSQL(query_chick_in);
        String cache_total_bird = "DELETE FROM cache_total_bird WHERE daily_recording_id = '"+id+"'";
        database.execSQL(cache_total_bird);
    }


    //fungsi ini berguna untuk mengubah state New pada tabel cache_hatch_date
    public void update_New_cache_hatch_date(String flockID, String dateRecording){
        //query untuk melakukan update New pada cache_hatch_date
        String query = "UPDATE cache_hatch_date set new = 0  WHERE flock_id = '"+flockID+"' AND timestamp = '"+dateRecording+"'";
        database.execSQL(query);
    }
    //fungsi untuk mengupdate database yang telah melakukan edit
    public void update_edit_cache_total_bird(String flockId, String recordingDate, String hatch_date){
        //mengupdate jumlah ayam berdasarkan hatch date yang di masukkan
        String Query= "SELECT * from daily_recording where flock_id = '"+flockId+"' AND recording_date > '"+recordingDate+"' " +
                "ORDER BY recording_date ASC";
        Cursor cursor = database.rawQuery(Query, null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String value = new String();
            Long recording_id = cursor.getLong(cursor.getColumnIndex("id"));
            String dateRecording = cursor.getString(cursor.getColumnIndex("recording_date"));
            create_cache_total_bird(flockId,dateRecording,recording_id,hatch_date);
        }
    }


    public long getRecordingID(String flock, String recording_date){
        String selectQuery= "SELECT id FROM daily_recording WHERE flock_id = '"+ flock + "' and recording_date = '"+recording_date+"' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getLong(cursor.getColumnIndex("id"));
        cursor.close();
        return ID;
    }
    public long getChickInID(String hatch_date){
        String selectQuery= "SELECT id FROM chick_in WHERE hatch_date = '"+ hatch_date + "' LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        long ID = 0;
        if(cursor.moveToFirst())
            ID  =  cursor.getInt(cursor.getColumnIndex("id"));
        cursor.close();
        return ID;
    }
    public ArrayList<String> getEggName(){
        ArrayList eggName = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM egg_quality",null);

        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            eggName.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        return eggName;
    }
    public ArrayList<String> getMortalityCategory(){
        ArrayList mortalityCategory = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM mortality_category",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            mortalityCategory.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        return mortalityCategory;
    }
    public ArrayList<String> getFlock(){
        ArrayList flock = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT * FROM flock ",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            flock.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        return flock;
    }
    public ArrayList<String> getFlockExcept(String id){
        ArrayList flock = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor= null;
        cursor = dbase.rawQuery("SELECT flock.name, location.name FROM flock INNER JOIN location on flock.location_id = location.id " +
                "where flock.id != '"+id+"' ORDER BY location.name ASC",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            String flock_name = cursor.getString(0).toString();
            String location_name = cursor.getString(1).toString();
            String combine = location_name+" - "+flock_name;
            flock.add(combine);
        }
        return flock;
    }
    public ArrayList<String> getFeedName(){
        ArrayList feed = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM feed ",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            feed.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        return feed;
    }
    public ArrayList<String> getMedicineName(){
        ArrayList medicine = new ArrayList();
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery("SELECT * FROM medication_vaccination ",null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            medicine.add(cursor.getString(cursor.getColumnIndex("product_name")));
        }
        return medicine;
    }
    public ArrayList<String> getLocation(String location_name){
        //jika bukan nama kosong maka mengambil semua
        final ArrayList location = new ArrayList();
        final SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = null;
        if (location_name == ""){
            cursor = dbase.rawQuery("SELECT * FROM location ",null);
            cursor.moveToFirst();
            for (int i=0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                location.add(cursor.getString(cursor.getColumnIndex("name")));
            }
//            String module = "layerfarm_android";
//            String function_name = "layerfarm_android_get_farm";
//            String[] args = {};
//            Parameter parameter = new Parameter(module, function_name, args);
//
//            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//            String token2 = RetrofitData.getInstance().getToken2();
//            Call<List<Farm>> call = apiInterfaceJson.getFarm(token2, parameter);
//
//            call.enqueue(new Callback<List<Farm>>() {
//                @Override
//                public void onResponse(Call<List<Farm>> call, Response<List<Farm>> response) {
//                    List<Farm> farmSyncList = response.body();
//
//                    for (int i=0; i < farmSyncList.size(); i++){
//                        location.add(farmSyncList.get(i).getName());
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<List<Farm>> call, Throwable t) {
//                    String alert = t.getMessage();
//                    //Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                }
//            });

        }
        else if (location_name != "") {
            cursor = dbase.rawQuery("SELECT * FROM location where name = '" + location_name + "' ", null);
            if(cursor.moveToFirst())
                location.add(cursor.getString(cursor.getColumnIndex("rid")));
            cursor.close();
        }

        return location;

    }

    public String gethatchDateMaxBird(String flockId){
        //mencari tanggal last recording yg baru saja di update oleh flock
        String Query= "SELECT chick_in.hatch_date as dateMax, MAX(chick_in_distribution.number_of_bird) FROM chick_in\n" +
                "INNER JOIN chick_in_distribution on chick_in_distribution.chick_in_id = chick_in.id \n" +
                "WHERE flock_id = '"+flockId+"' and chick_in_distribution.number_of_bird > 0";
        SQLiteDatabase dbase = db.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(Query, null);
        String hatchDateMax = "";
        if(cursor.moveToFirst())
            hatchDateMax  =  cursor.getString(cursor.getColumnIndex("dateMax"));
        cursor.close();
        Log.d("zzzs","hatchDate = "+hatchDateMax);

        return hatchDateMax;
    }
}
