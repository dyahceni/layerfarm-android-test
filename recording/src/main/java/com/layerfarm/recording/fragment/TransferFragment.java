package com.layerfarm.recording.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingSpentHen;
import com.layerfarm.layerfarm.model.DailyRecordingTransfer;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.activity_option.OptionToFlock;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.recording.model.SpentHen;
import com.layerfarm.layerfarm.DatabaseHelper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferFragment extends android.support.v4.app.Fragment {
    helper help;
    private DBDataSource dataSource;
    DatabaseHelper db;
    EditText totalBirdMOve;
    private BottomNavigationView bottomNavigationView;
    Spinner spn_chick_in;
    String recordingDate;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status;
    ViewGroup vg_spenthent, profileView, more_transfer;
    private ProgressBar pgsBar;

    private ArrayList<RecordingEntry.Hatching> hatchDate;
    private ArrayList<SpentHen> ListSpentHent;
    ArrayList<String> flock_items;
    ArrayList<String> chick_in_items;
    TextView house_profile_title, tx_house_profile;
    HashMap<Integer, String> options_hatch_date, options_houses;

    Integer tranfer_child =0;


    //variable yg didapatkan dari transfer
    String birds_transfer = "";
    String hatch_date = "";
    String to_flock = "";
    String flock = "";
    String location = "";

    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_transfer, container, false);

        help = new helper(getContext());
        help.open();
        db = new DatabaseHelper(getContext());
        dataSource =  new DBDataSource(getContext());
        dataSource.open();
        final Button Submit = (Button) view.findViewById(R.id.Submit);
        pgsBar = (ProgressBar) view.findViewById(R.id.pBar);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pgsBar.setVisibility(v.VISIBLE);
                if (CheckBeforeSubmit()){
                    Submit();
                }else{
                    pgsBar.setVisibility(v.GONE);
                    AskOption("Please fill the data");
                }
            }
        });
        TextView tx_more_transfer = (TextView) view.findViewById(R.id.add_more_transfer);
        tx_more_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewTransfer();
            }
        });


        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

        //inisialisasi items untuk spinner chick in
        //fungsi memasukkan feed ke dalam array yg kemudian digunakan di spinner
        options_hatch_date = RecordingEntry.getInstance().getTransfer().getOptions_hatch_date();
        Collection<String> hatch_ = options_hatch_date.values();
        chick_in_items = new ArrayList<>(hatch_);


        //inisialisasi items untuk spinner house
        options_houses = RecordingEntry.getInstance().getTransfer().getOptions_houses();
        Collection<String> house_ = options_houses.values();
        flock_items = new ArrayList<>(house_);


        more_transfer = (ViewGroup) view.findViewById(R.id.transfer);
        vg_spenthent = (ViewGroup) view.findViewById(R.id.spent_hent);

        hatchDate = RecordingEntry.getInstance().getProfil_kandang().getHatching();
//        for (int i =0; i< hatchDate.size();i++){
//
//        }
        profileView = (ViewGroup) view.findViewById(R.id.bird_profile);
        tx_house_profile = (TextView) view.findViewById(R.id.House);
        house_profile_title = (TextView) view.findViewById(R.id.house_profile_title);
//        tx_house_profile.setText("House : "+location_name+" - "+flock_name+" - "+flock_type+" "+flock_period);
        house_profile_title.setText(RecordingEntry.getInstance().getProfil_kandang().getTitle());
        String markup_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getMarkup();
        String prefix_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getPrevix();
        tx_house_profile.setText(prefix_kandang+markup_kandang);
        for (int i =0; i< hatchDate.size();i++){
            //Log.d("zzzs","hatch date: "+hatchDate.get(i).getHatchDate());
            //Log.d("zzzs","strain: "+hatchDate.get(i).getStrain());
            ViewHouseProfile(hatchDate.get(i));
            ViewSpentHent(hatchDate.get(i).getHatch_date(), hatchDate.get(i).getStrain());
            //ViewMortality(hatchDate.get(i).getHatchDate(), hatchDate.get(i).getStrain());
        }
        long ID = dataSource.getRecordingID(flock_id,recordingDate);
//        Log.d("zzzs","ID = "+ID);
        //CHICK OUT
        if (DailyRecordingManager.getInstance().getDailyRecording().getAfkir().isEmpty()){
            if (!RecordingEntry.getInstance().getTransfer().getAfkir_value().isEmpty()){
                ArrayList<DailyRecordingSpentHen> list_spenthen = RecordingEntry.getInstance().getTransfer().getAfkir_value();
                DailyRecordingManager.getInstance().getDailyRecording().setAfkir(list_spenthen);
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getAfkir().isEmpty()){
            Log.d("zzz","spent hen ada isinya "+DailyRecordingManager.getInstance().getDailyRecording().getAfkir());
            setSpentHen();
        }

        //TRANSFER
        if (DailyRecordingManager.getInstance().getDailyRecording().getPindahKandang().isEmpty()){
            if (!RecordingEntry.getInstance().getTransfer().getTransfer_value().isEmpty()){
                ArrayList<DailyRecordingTransfer> list_transfer = RecordingEntry.getInstance().getTransfer().getTransfer_value();
                DailyRecordingManager.getInstance().getDailyRecording().setPindahKandang(list_transfer);
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getPindahKandang().isEmpty()){
            setTransfer();
        }
        else
            ViewTransfer();
//        if (ID != 0){
//            Log.d("zzzs", "sudah pernah recording = "+ID);
//            setTransfer(ID);
//            setSpentHen(ID, hatchDate);
//        }

        return view;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Log.d("zzz","index child = "+tranfer_child);
                String result=data.getStringExtra("result");
                Log.d("zzz","result activityResult = "+result);
                View childView = more_transfer.getChildAt(tranfer_child);
                EditText ed_flock = (EditText)(childView.findViewById(R.id.ed_to_flock));
                Log.d("zzz","result Edittext = "+ed_flock.getText().toString());
//                total_feed = (EditText)(childView.findViewById(R.id.total_feed));
                Log.d("zzz","result activityResult = "+result);
                ed_flock.setText(result);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
//        if (requestCode == 2) {
//            if(resultCode == Activity.RESULT_OK){
//                Log.d("zzz","index child = "+feed_child);
//                String result=data.getStringExtra("result");
//                Log.d("zzz","result activityResult = "+result);
//                View childView = more_medicine.getChildAt(feed_child);
//                EditText ed_medicine_type = (EditText)(childView.findViewById(R.id.ed_medvac_type));
//                Log.d("zzz","result Edittext = "+ed_medicine_type.getText().toString());
////                total_feed = (EditText)(childView.findViewById(R.id.total_feed));
//                Log.d("zzz","result activityResult = "+result);
//                ed_medicine_type.setText(result);
//
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//                //Write your code if there's no result
//            }
//        }
    }
    public boolean CheckBeforeSubmit(){
        boolean valid1 = true, valid2 = true;
        int childCount = more_transfer.getChildCount()-1;
        Log.d("Layerfarm", "child count : "+childCount);
        for (int c=0; c< childCount; c++){
            DailyRecordingTransfer transfer = new DailyRecordingTransfer();
            View childView = more_transfer.getChildAt(c);

            EditText totalBirdMOve = (EditText)(childView.findViewById(R.id.ed_total_bird_move));
            Spinner spn_chick_in = (Spinner) (childView.findViewById(R.id.spn_hatch_date));
//            Spinner spn_flock = (Spinner) (childView.findViewById(R.id.spn_house));
            EditText ed_flock = (EditText)(childView.findViewById(R.id.ed_to_flock));
            Log.d("Layerfarm","totalbird move : "+totalBirdMOve.getText().toString());
//            if (totalBirdMOve.getText().toString().isEmpty() || ed_flock.getText().toString().isEmpty()){
//
//            }

            if (totalBirdMOve.getText().toString().isEmpty()){
                totalBirdMOve.setError("Please fill the data");
                valid1 = false;
                break;
            }else if (ed_flock.getText().toString().isEmpty()){
                ed_flock.setError("Please fill the data");
                valid1 = false;
                break;
            }

        }

        int childCount1 = vg_spenthent.getChildCount();
        for (int c =0; c<childCount1;c++){
            DailyRecordingSpentHen spent_hent = new DailyRecordingSpentHen();
            View childView = vg_spenthent.getChildAt(c);
            TextView tx_chick_in = (TextView)(childView.findViewById(R.id.spentHent_hatchDate));
            String[] chick = tx_chick_in.getText().toString().split(" ");
            Log.d("Layerfarm","tx chick in : "+tx_chick_in.getText().toString());
            int chick_in = Integer.parseInt(hatchDate.get(c).getTgl_tetas_nid());
            EditText ed_total_bird = (EditText) (childView.findViewById(R.id.ed_total_spent));
            String total_bird = ed_total_bird.getText().toString();
            EditText ed_total_weight = (EditText)(childView.findViewById(R.id.ed_total_weight_spent));
            String total_weight = ed_total_weight.getText().toString();
            boolean andnot = total_bird.isEmpty() && total_weight.isEmpty();
            boolean and = total_bird.isEmpty() || total_weight.isEmpty();
            if (and && !andnot) {
                if (ed_total_bird.getText().toString().isEmpty()){
                    ed_total_bird.setError("Please fill the data");
                    valid2 = false;
                    break;
                }else if (ed_total_weight.getText().toString().isEmpty()){
                    ed_total_weight.setError("Please fill the data");
                    valid2 = false;
                    break;
                }
            }
        }
        if (valid2 && valid1){
            return true;
        }else return false;
    }
    public void Submit() {
        pgsBar.setVisibility(View.GONE);
        //BUAT MENGIRIM KE SERVER
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";

//        DailyRecording.getInstance().setHouseId(flock_id);
//        DailyRecording.getInstance().setRecordingDate(recordingDate);
        ArrayList<DailyRecordingSpentHen> spentHens = getListSpentHent();
        ArrayList<DailyRecordingTransfer> transfers = getListTransfer();
//        DailyRecording dailyRecording = new DailyRecording(flock_id,recordingDate,production);
//        args.setProduction(production);
//        DailyRecording.getInstance().setProduction(production);


        Log.d("Layerfarm","flock_id = "+flock_id);
        final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        args.setHouseId(flock_id);
        args.setRecordingDate(recordingDate);
        args.setAfkir(spentHens);
        args.setPindahKandang(transfers);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);
        Log.d("zzz","paramater ="+parameter);


        ApiInterface apiInterfaceJson;
        apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] recording = response.body();
                if (recording[0].equals("RECORDING: Hatch date: : Mortality/selection and/or transfer must less than  birds in this house.")){
                    pgsBar.setVisibility(View.GONE);
                    AskOption(recording[0]);
                }else{
                    pgsBar.setVisibility(View.GONE);
                    SuccessDialog();
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                pgsBar.setVisibility(View.GONE);
                dataSource.create_recording(recordingDate,flock_id,args);
                FailedDialog();
            }
        });

    }
    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Warning!!")
                .setMessage(notif)
                .setIcon(R.drawable.camera)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    private AlertDialog SuccessDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording berhasil ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    private AlertDialog FailedDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording gagal ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        getActivity().finish();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    public void ViewSpentHent(String name, String strain){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View SpentHentInf = inflater.inflate(R.layout.spent_hent, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView hatch_date = (TextView) SpentHentInf.findViewById(R.id.spentHent_hatchDate);
        hatch_date.setText("Hatchching "+name+" | "+strain);
        final EditText ed_total_spent = (EditText) SpentHentInf.findViewById(R.id.ed_total_spent);
        final EditText ed_total_weight_spent = (EditText) SpentHentInf.findViewById(R.id.ed_total_weight_spent);

        ed_total_spent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!ed_total_weight_spent.getText().toString().isEmpty())
                        ed_total_spent.setError("Fill Data");
                } else {
                    ed_total_spent.setError(null);
                    if (ed_total_weight_spent.getText().toString().isEmpty())
                        ed_total_weight_spent.setError("Fill Data");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed_total_weight_spent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!ed_total_spent.getText().toString().isEmpty())
                        ed_total_weight_spent.setError("Fill Data");
                } else {
                    ed_total_weight_spent.setError(null);
                    if (ed_total_spent.getText().toString().isEmpty())
                        ed_total_spent.setError("Fill Data");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        vg_spenthent.addView(SpentHentInf);
    }
    public void ViewTransfer(){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View transferView = inflater.inflate(R.layout.more_transfer, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);

        spn_chick_in = (Spinner) transferView.findViewById(R.id.spn_hatch_date);
        spn_chick_in.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, chick_in_items));
        spn_chick_in.setSelected(true);

//        spn_flock = (Spinner) transferView.findViewById(R.id.spn_house);
//        spn_flock.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, flock_items));
//        spn_flock.setSelected(true);

        final EditText ed_flock = (EditText)(transferView.findViewById(R.id.ed_to_flock));
        ed_flock.setTag(more_transfer.getChildCount()-1);
        ed_flock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), OptionToFlock.class);
                Log.d("zzz","child selected = "+ed_flock.getTag());
                tranfer_child = Integer.parseInt(ed_flock.getTag().toString());
                startActivityForResult(i, 1);
            }
        });

        totalBirdMOve = (EditText) transferView.findViewById(R.id.ed_total_bird_move);
        totalBirdMOve.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //On user changes the text
                if(s.toString().trim().length()==0) {
                    if (!ed_flock.getText().toString().isEmpty())
                        totalBirdMOve.setError("Fill Data");
                } else {
                    totalBirdMOve.setError(null);
                    if (ed_flock.getText().toString().isEmpty())
                        ed_flock.setError("Fill Data");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed_flock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //On user changes the text
                if(s.toString().trim().length()==0) {
                    if (!totalBirdMOve.getText().toString().isEmpty()){
                        ed_flock.setError("Fill data");
                    }
                } else {
                    ed_flock.setError(null);
                    if (totalBirdMOve.getText().toString().isEmpty()){
                        totalBirdMOve.setError("Fill data");
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        more_transfer.addView(transferView,more_transfer.getChildCount()-1);
    }
    public void ViewHouseProfile(RecordingEntry.Hatching hatching){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View house = inflater.inflate(R.layout.house_profile, null);

        TextView hatch = (TextView) house.findViewById(R.id.hatching);
        TextView total_bird = (TextView) house.findViewById(R.id.total_bird);
        TextView bird_age = (TextView) house.findViewById(R.id.bird_age);
        hatch.setText("["+hatching.getHatch_date()+"]");
        total_bird.setText(hatching.getTotal_bird_title()+hatching.getTotal_bird_value());
        bird_age.setText(hatching.getAge_bird_title()+hatching.getAge_bird_value());
        profileView.addView(house);
    }
    public ArrayList<DailyRecordingSpentHen> getListSpentHent() {
        ArrayList<DailyRecordingSpentHen> ListSpentHent = new ArrayList<>();
        int childCount = vg_spenthent.getChildCount();
        for (int c =0; c<childCount;c++){
            DailyRecordingSpentHen spent_hent = new DailyRecordingSpentHen();
            View childView = vg_spenthent.getChildAt(c);
            TextView tx_chick_in = (TextView)(childView.findViewById(R.id.spentHent_hatchDate));
            String[] chick = tx_chick_in.getText().toString().split(" ");
            Log.d("Layerfarm","tx chick in : "+tx_chick_in.getText().toString());
            int chick_in = Integer.parseInt(hatchDate.get(c).getTgl_tetas_nid());
            EditText ed_total_bird = (EditText) (childView.findViewById(R.id.ed_total_spent));
            String total_bird = ed_total_bird.getText().toString();
            EditText ed_total_weight = (EditText)(childView.findViewById(R.id.ed_total_weight_spent));
            String total_weight = ed_total_weight.getText().toString();
            if (!total_bird.isEmpty() || !total_weight.isEmpty()){
                spent_hent.setHatch_date_move(chick_in);
                spent_hent.setJumlah_afkir(total_bird);
                spent_hent.setAfkir_weight(total_weight);
                ListSpentHent.add(spent_hent);
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListSpentHent;
    }
    public ArrayList<DailyRecordingTransfer> getListTransfer(){
        ArrayList<DailyRecordingTransfer> ListTransfer = new ArrayList<>();
        int childCount = more_transfer.getChildCount()-1;
        Log.d("Layerfarm", "child count : "+childCount);
        for (int c=0; c< childCount; c++){
            DailyRecordingTransfer transfer = new DailyRecordingTransfer();
            View childView = more_transfer.getChildAt(c);

            EditText totalBirdMOve = (EditText)(childView.findViewById(R.id.ed_total_bird_move));
            Spinner spn_chick_in = (Spinner) (childView.findViewById(R.id.spn_hatch_date));
//            Spinner spn_flock = (Spinner) (childView.findViewById(R.id.spn_house));
            EditText ed_flock = (EditText)(childView.findViewById(R.id.ed_to_flock));
            Log.d("Layerfarm","totalbird move : "+totalBirdMOve.getText().toString());
//            if (totalBirdMOve.getText().toString().isEmpty() || ed_flock.getText().toString().isEmpty()){
//
//            }

            if (!totalBirdMOve.getText().toString().isEmpty() && !ed_flock.getText().toString().isEmpty()){
                String BirdMove = totalBirdMOve.getText().toString();
                String HatchDate = spn_chick_in.getSelectedItem().toString();
                int HatchDate_nid = getKey(options_hatch_date, HatchDate);
                String toHouse = ed_flock.getText().toString();
                int toHouse_nid = getKey(options_houses,toHouse);
                transfer.setHatch_date_move(String.valueOf(HatchDate_nid));
                transfer.setTo_house(String.valueOf(toHouse_nid));
                transfer.setNumber_hen_move(BirdMove);
                ListTransfer.add(transfer);
            }

        }
        return ListTransfer;
    }
//    public void HatchItems(){
//        //inisialisasi items untuk spinner hatch date
//        ArrayList<HatchDate> hatchDate = help.getHatchDate(flock_id, recordingDate);
//        chick_in_items= new String[hatchDate.size()];
//        for (int i =0; i<hatchDate.size(); i++){
//            chick_in_items[i]= hatchDate.get(i).getHatchDate();
//        }
//    }

    public void setTransfer(){
        ArrayList<DailyRecordingTransfer> ListTransfer = DailyRecordingManager.getInstance().getDailyRecording().getPindahKandang();
        for (int i =0; i< ListTransfer.size(); i++){
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View transferView = inflater.inflate(R.layout.more_transfer, null);
            // Add the new row before the add field button.
            //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);

            spn_chick_in = (Spinner) transferView.findViewById(R.id.spn_hatch_date);
            ArrayAdapter hatchDate_Adapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, chick_in_items);
            spn_chick_in.setAdapter(hatchDate_Adapt);
            spn_chick_in.setSelected(true);
            int hatchDate_nid = Integer.parseInt(ListTransfer.get(i).getHatch_date_move());
            String hatchDate_name = options_hatch_date.get(hatchDate_nid);
            int hatchDate_position =hatchDate_Adapt.getPosition(hatchDate_name);
            spn_chick_in.setSelection(hatchDate_position);

//            spn_flock = (Spinner) transferView.findViewById(R.id.spn_house);
//            ArrayAdapter flock_Adapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, flock_items);
//            spn_flock.setAdapter(flock_Adapt);
//            spn_flock.setSelected(true);
            EditText ed_flock = (EditText) transferView.findViewById(R.id.ed_to_flock);
            int flock_nid = Integer.parseInt(ListTransfer.get(i).getTo_house());
            String flock_name = options_houses.get(flock_nid);
//            int flock_position = flock_Adapt.getPosition(flock_name);
            ed_flock.setText(flock_name);

            totalBirdMOve = (EditText) transferView.findViewById(R.id.ed_total_bird_move);
            totalBirdMOve.setText(ListTransfer.get(i).getNumber_hen_move());

            more_transfer.addView(transferView,more_transfer.getChildCount()-1);
        }

//
//
//
//        SQLiteDatabase dbase = db.getReadableDatabase();
//        String selectQuery="SELECT * FROM bird_transfer INNER JOIN chick_in on bird_transfer.chick_in_id = chick_in.id INNER JOIN flock\n" +
//                "on bird_transfer.to_flock_id = flock.id WHERE daily_recording_id = '"+id+"'";
//        Cursor cursor = dbase.rawQuery(selectQuery,null);
//
//
//        if(cursor.moveToFirst()) {
//            birds_transfer = cursor.getString(cursor.getColumnIndex("number_of_bird"));
//            hatch_date = cursor.getString(cursor.getColumnIndex("hatch_date"));
//            flock = cursor.getString(cursor.getColumnIndex("flock.name"));
//            location = cursor.getString(cursor.getColumnIndex("location.name"));
//        }
//        to_flock = location+" - "+flock;
//        totalBirdMOve.setText(birds_transfer);
//        Spinner chick_in = (Spinner) view.findViewById(R.id.spn_hatch_date);
//        ArrayAdapter myAdapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, chick_in_items);
//        chick_in.setAdapter(myAdapt);
//        chick_in.setSelected(true);
//        int position =myAdapt.getPosition(hatch_date);
//        chick_in.setSelection(position);
//
//        Spinner flock_tujuan = (Spinner) view.findViewById(R.id.spn_house);
//        ArrayAdapter Adapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, flock_items);
//        flock_tujuan.setAdapter(Adapt);
//        flock_tujuan.setSelected(true);
//        int position_flock =Adapt.getPosition(to_flock);
//        flock_tujuan.setSelection(position_flock);
//        cursor.close();
    }
    public void setSpentHen(){
        ArrayList<DailyRecordingSpentHen> spentHen = DailyRecordingManager.getInstance().getDailyRecording().getAfkir();
        int childCount = vg_spenthent.getChildCount();
        for (int c =0; c<childCount;c++){
//            DailyRecording.SpentHen spent_hent = new DailyRecording.SpentHen();
            View childView = vg_spenthent.getChildAt(c);
            TextView tx_chick_in = (TextView)(childView.findViewById(R.id.spentHent_hatchDate));
            String hatch_nid = hatchDate.get(c).getTgl_tetas_nid();

            EditText ed_total_bird = (EditText) (childView.findViewById(R.id.ed_total_spent));
//            String total_bird = ed_total_bird.getText().toString();
            EditText ed_total_weight = (EditText)(childView.findViewById(R.id.ed_total_weight_spent));
//            String total_weight = ed_total_weight.getText().toString();

            for (int a =0; a< spentHen.size(); a++){
                if (Integer.toString(spentHen.get(a).getHatch_date_move()).equals(hatch_nid)){
                    ed_total_bird.setText(spentHen.get(a).getJumlah_afkir());
                    ed_total_weight.setText(spentHen.get(a).getAfkir_weight());
                }
            }
//            if (!total_bird.isEmpty() || !total_weight.isEmpty()){
//                spent_hent.setHatch_date_move(chick_in);
//                spent_hent.setJumlah_afkir(total_bird);
//                spent_hent.setAfkir_weight(total_weight);
//                ListSpentHent.add(spent_hent);
//            }
        }
//        for (int i =0; i< hatchDate.size(); i++){
//            String chick_in = "";
//            String bird = "";
//            String weight = "";
////            if(cursor.moveToFirst()) {
////                chick_in = cursor.getString(cursor.getColumnIndex("hatch_date"));
////                bird = cursor.getString(cursor.getColumnIndex("number_of_bird"));
////                weight = cursor.getString(cursor.getColumnIndex("total_weight"));
////            }
//            int hatch_date_nid = getKey(options_hatch_date, )
//            for (int a =0; a< spentHen.size(); a++){
////                if (spentHen.get(a).getHatch_date_move())
//            }
//            if (!chick_in.isEmpty()){
//                View childView = vg_spenthent.getChildAt(i);
//                EditText ed_total_bird = (EditText) (childView.findViewById(R.id.ed_total_spent));
//                EditText ed_total_weight = (EditText)(childView.findViewById(R.id.ed_total_weight_spent));
//                ed_total_bird.setText(bird);
//                ed_total_weight.setText(weight);
//            }
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            ArrayList<DailyRecordingSpentHen> spentHens = getListSpentHent();
            ArrayList<DailyRecordingTransfer> transfers = getListTransfer();
            final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
            args.setHouseId(flock_id);
            args.setRecordingDate(recordingDate);
            args.setAfkir(spentHens);
            args.setPindahKandang(transfers);
            args.setForceDate("TRUE");
            DailyRecordingManager.getInstance().setDailyRecording(args);
        }catch (Exception e){

        }

    }

    //    public void insert (long id){
//        String BirdMove = totalBirdMOve.getText().toString();
//        String HatchDate = spn_chick_in.getSelectedItem().toString();
//        String toHouse = spn_flock.getSelectedItem().toString();
//        if (!BirdMove.isEmpty()){
//            dataSource.createTransfer(BirdMove, HatchDate, toHouse, id, recordingDate, flock_id);
//            String origin = "INT"+id;
//            int harga = 1000;
//            int total_bird = Integer.parseInt(BirdMove);
//            dataSource.create_stock_move(origin, harga, total_bird, flock_id, toHouse, "false");
//            dataSource.create_stock_picking(origin,flock_id, toHouse, flock_name);
//        }
//        ArrayList<SpentHen> list_spent_hent = getListSpentHent();
//        if (list_spent_hent.size() != 0){
//            dataSource.createSpentHen(list_spent_hent, id);
//            int total_bird =0;
//            for(int i =0; i< list_spent_hent.size(); i++){
//                total_bird += Integer.parseInt(list_spent_hent.get(i).getTotalBird());
//            }
//            int harga = 1000;
//            int total = total_bird * harga;
//            long ChickOut = dataSource.create_sale_order(id,total,flock_id);
//            String origin = "SO"+id;
//            dataSource.create_sale_order_line(ChickOut, total_bird, harga);
//            dataSource.create_stock_move(origin, harga, total_bird, flock_id, "0", "false");
//            dataSource.create_stock_picking(origin,flock_id,"0",flock_name);
//        }
//        //membuat di table cache
//        for (int i =0; i< hatchDate.size(); i++){
//            dataSource.create_cache_total_bird(flock_id,recordingDate,id,hatchDate.get(i).getHatch_date());
//
//        }
//    }
    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
