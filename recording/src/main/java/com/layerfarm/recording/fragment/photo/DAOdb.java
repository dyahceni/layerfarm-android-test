package com.layerfarm.recording.fragment.photo;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class DAOdb {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public DAOdb(Context context) {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    /**
     * close any database object
     */
    public void close() {
        dbHelper.close();
    }


    public long addImage(ModelPhoto image) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_PATH, image.getPath());
        cv.put(DatabaseHelper.COLUMN_TITLE, image.getTitle());
        cv.put(DatabaseHelper.COLUMN_DESCRIPTION, image.getDescription());
        cv.put(DatabaseHelper.COLUMN_DATETIME, System.currentTimeMillis());
        cv.put(DatabaseHelper.COLUMN_ID_RECORDING_PHOTO,image.getId_recording());
        return database.insert(DatabaseHelper.TABLE_NAME_PHOTO, null, cv);
    }

    /**
     * delete the given image from database
     *
     * @param image
     */
    public void deleteImage(ModelPhoto image) {
        String whereClause =
                DatabaseHelper.COLUMN_TITLE + "=? AND " + DatabaseHelper.COLUMN_DATETIME +
                        "=?";
        String[] whereArgs = new String[]{image.getTitle(),
                String.valueOf(image.getDatetimeLong())};
        database.delete(DatabaseHelper.TABLE_NAME_PHOTO, whereClause, whereArgs);
    }

    /**
     * @return all image as a List
     */
    public List<ModelPhoto> getImages() {
        List<ModelPhoto> MyPhoto = new ArrayList<>();
        Cursor cursor =
                database.query(DatabaseHelper.TABLE_NAME_PHOTO, null, null, null, null,
                        null, DatabaseHelper.COLUMN_DATETIME + " DESC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ModelPhoto ModelPhoto = cursorToMyImage(cursor);
            MyPhoto.add(ModelPhoto);
            cursor.moveToNext();
        }
        cursor.close();
        return MyPhoto;
    }

    /**
     * read the cursor row and convert the row to a MyImage object
     *
     * @param cursor
     * @return MyImage object
     */
    private ModelPhoto cursorToMyImage(Cursor cursor) {
        ModelPhoto image = new ModelPhoto();
        image.setPath(
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PATH)));
        image.setTitle(
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TITLE)));
        image.setDatetime(cursor.getLong(
                cursor.getColumnIndex(DatabaseHelper.COLUMN_DATETIME)));
        image.setDescription(cursor.getString(
                cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCRIPTION)));
        image.setId_recording(cursor.getLong(
                cursor.getColumnIndex(DatabaseHelper.COLUMN_ID_RECORDING_PHOTO)));
        return image;
    }
}
