package com.layerfarm.recording.fragment.photo;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.layerfarm.recording.R;
import com.layerfarm.recording.database.DBDataSource;

import java.util.ArrayList;

public class PhotoMainFragment extends android.support.v4.app.Fragment {

    //final Context c = getActivity();
    private ArrayList<ModelPhoto> images;
    private ImageAdapter imageAdapter;
    private ListView listView;
    private Uri mCapturedImageURI;
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private DAOdb daOdb;
    private Button addPhotoButton;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, recordingDate;
    private DBDataSource dataSource;

    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_photo_main, container, false);


        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(com.layerfarm.setting.R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom_dialog_box);
                dialog.setTitle("Choose Photo Source");
                Button btnExit = (Button) dialog.findViewById(R.id.btnExit);
                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.btnChoosePath).setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        activeGallery();
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.btnTakePhoto).setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        activeTakePhoto();
                        dialog.dismiss();
                    }
                });

                // show dialog on screen
                dialog.show();

            }
        });
        // Construct the data source
        images = new ArrayList();
        // Create the adapter to convert the array to views
        imageAdapter = new ImageAdapter(getActivity().getApplicationContext(),images);
        // Attach the adapter to a ListView
        listView = (ListView) view.findViewById(R.id.main_list_view);
        listView.setAdapter(imageAdapter);
        addItemClickListener(listView);
        initDB();

        return view;
    }

    /**
     * initialize database
     */
    private void initDB() {
        daOdb = new DAOdb(getActivity());
        //        add images from database to images ArrayList
        for (ModelPhoto mi : daOdb.getImages()) {
            images.add(mi);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        //addPhotoButton = (Button) view.findViewById(R.id.btnAdd);
        //addPhotoButton.setOnClickListener(new OnaddPhotoButtonClickListener());
    }

    private class OnaddPhotoButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

        }
    }
    /*
    public void btnAddOnClick(View view) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.custom_dialog_box);
        dialog.setTitle("Alert Dialog View");
        Button btnExit = (Button) dialog.findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btnChoosePath).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                activeGallery();
            }
        });
        dialog.findViewById(R.id.btnTakePhoto).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                activeTakePhoto();
            }
        });

        // show dialog on screen
        dialog.show();
    }
    */

    /**
     * take a photo
     */
    private void activeTakePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            String fileName = "temp.jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * to gallery
     */
    private void activeGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Dialog
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        final View mView = layoutInflaterAndroid.inflate(R.layout.android_user_input_dialog, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Entry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here

                        switch (requestCode) {
                            case RESULT_LOAD_IMAGE:
                                //Toast.makeText(getActivity().getApplicationContext(),"Gallery", Toast.LENGTH_SHORT).show();

                                if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {

                                    //final long id_recording_data = dataSource.getRecordingID(flock_id,recordingDate);

                                    Uri selectedImage = data.getData();
                                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                                    cursor.moveToFirst();
                                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                    String picturePath = cursor.getString(columnIndex);
                                    cursor.close();
                                    final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                                    final String gallery = (String) (userInputDialogEditText.getText().toString());

                                    ModelPhoto image = new ModelPhoto();
                                    image.setTitle("");
                                    image.setDescription(" " +gallery);
                                    image.setDatetime(System.currentTimeMillis());
                                    image.setPath(picturePath);
                                    //image.setId_recording(id_recording_data);
                                    imageAdapter.add(image);
                                    //                    images.add(image);
                                    daOdb.addImage(image);
                                }

                            case REQUEST_IMAGE_CAPTURE:

                /*
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = managedQuery(mCapturedImageURI, projection, null, null, null);
                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String picturePath = cursor.getString(column_index_data);
                    MyImage image = new MyImage();
                    image.setTitle("Test");
                    image.setDescription("test take a photo and add it to list view");
                    image.setDatetime(System.currentTimeMillis());
                    image.setPath(picturePath);
                    imageAdapter.add(image);
                    //                    images.add(image);
                    daOdb.addImage(image);
                }
                */



                                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                                    String[] projection = {MediaStore.Images.Media.DATA};
                                    Cursor cursor = getActivity().managedQuery(mCapturedImageURI, projection, null, null, null);
                                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                                    cursor.moveToFirst();
                                    //Toast.makeText(getApplicationContext(),desc, Toast.LENGTH_SHORT).show();
                                    String picturePath = cursor.getString(column_index_data);
                                    final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                                    final String gallery = (String) (userInputDialogEditText.getText().toString());
                                    ModelPhoto image = new ModelPhoto();
                                    image.setTitle("Test");
                                    image.setDescription(" " +gallery);
                                    image.setDatetime(System.currentTimeMillis());
                                    image.setPath(picturePath);
                                    imageAdapter.add(image);
                                    //                    images.add(image);
                                    daOdb.addImage(image);
                                }



                        }

                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                                //getActivity().finish();

                            }
                        });
        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    /**
     * item clicked listener used to implement the react action when an item is clicked.
     *
     * @param listView
     */
    private void addItemClickListener(final ListView listView) {
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ModelPhoto image = (ModelPhoto) listView.getItemAtPosition(position);

                DisplayImageFragment fragmentPicture = new DisplayImageFragment();

                Bundle args = new Bundle();
                args.putString("IMAGE", (new Gson()).toJson(image));
                fragmentPicture.setArguments(args);

                getFragmentManager().beginTransaction()
                        .replace(R.id.frame_test, fragmentPicture)
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();


                //Intent intent = new Intent(getActivity().getBaseContext(), DisplayImageFragment.class);
                //intent.putExtra("IMAGE", (new Gson()).toJson(image));
                //Bundle b = new Bundle();
                //b.putString("IMAGE", (new Gson()).toJson(image));

                //startActivity(intent);
            }
        });
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        // Save the user's current game state
        if (mCapturedImageURI != null) {
            outState.putString("mCapturedImageURI", mCapturedImageURI.toString());
        }
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);
    }


    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onActivityCreated(savedInstanceState);

        // Restore state members from saved instance
        if (savedInstanceState.containsKey("mCapturedImageURI")) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString("mCapturedImageURI"));
        }
    }
}
