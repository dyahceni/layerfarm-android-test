package com.layerfarm.recording.fragment.photo;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layerfarm.recording.R;

import org.json.JSONException;
import org.json.JSONObject;

public class DisplayImageFragment extends android.support.v4.app.Fragment {

    private ModelPhoto image;
    private ImageView imageView;
    private TextView description;
    private String jstring;

    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_display_image, container, false);


        imageView = (ImageView) view.findViewById(R.id.display_image_view);
        description = (TextView) view.findViewById(R.id.text_view_description);
        //Bundle extras = getActivity().getIntent().getExtras();
        Bundle extras = getArguments();
        //int value = bundle.getInt("friendsID");

        if (extras != null) {
            jstring = extras.getString("IMAGE");
        }
        image = getMyImage(jstring);
        description.setText(image.toString());
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        imageView.setImageBitmap(ImageResizer
                .decodeSampledBitmapFromFile(image.getPath(), width, height));

        return view;
    }

    private ModelPhoto getMyImage(String image) {
        try {
            JSONObject job = new JSONObject(image);
            return (new ModelPhoto(
                    job.getString("title"),
                    job.getString("description"),
                    job.getString("path"),
                    job.getLong("datetimeLong"),
                    job.getLong("id_recording")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void btnBackOnClick(View v) {
        //startActivity(new Intent(this, MainActivity.class));
        //finish();
    }

    public void btnDeleteOnClick(View v) {
        DAOdb db = new DAOdb(getActivity());
        db.deleteImage(image);
        db.close();
        //startActivity(new Intent(this, MainActivity.class));
        //finish();
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        // Save the user's current game state
        if (jstring != null) {
            outState.putString("jstring", jstring);
        }
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onActivityCreated(savedInstanceState);

        // Restore state members from saved instance
        if (savedInstanceState.containsKey("jstring")) {
            jstring = savedInstanceState.getString("jstring");
        }
    }


}
