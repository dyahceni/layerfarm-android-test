package com.layerfarm.recording.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.icu.text.AlphabeticIndex;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingBodyWeight;
import com.layerfarm.layerfarm.model.DailyRecordingFeed;
import com.layerfarm.layerfarm.model.DailyRecordingOvk;
import com.layerfarm.layerfarm.model.DailyRecordingSpentHen;
import com.layerfarm.layerfarm.model.DailyRecordingTransfer;
import com.layerfarm.layerfarm.model.Farm;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.fragment.bodyweight.BodyWeightMainFragment;
import com.layerfarm.recording.fragment.photo.PhotoMainFragment;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.DailyRecordingManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    helper help;
    private DBDataSource dataSource;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status, recordingDate, recording_nid;
    long ID;
    TextView date;
    private ProgressBar pgsBar;
    String tag ="Layerfarm";
    Boolean btm_state = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
//        setupUI(findViewById(R.id.layout_main_fragment));

        help = new helper(this);
        help.open();
        dataSource =  new DBDataSource(this);
        dataSource.open();

        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText("Entry Data");

        final RelativeLayout rootView = (RelativeLayout) findViewById(R.id.layout_main_fragment);
        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        pgsBar.setVisibility(View.VISIBLE);
        Bundle extras = getIntent().getExtras();
        recordingDate = extras.getString("DATE");
        status = extras.getString("status");
        flock_id = extras.getString("id_flock");
        flock_name = extras.getString("name_flock");
        flock_type = extras.getString("type");
        flock_period = extras.getString("period");
        location_id = extras.getString("loc_id");
        location_name = extras.getString("loc_name");
        recording_nid = extras.getString("nid");
        Log.d(tag, "status : "+status);
        Log.d(tag, "nid : "+recording_nid);
        Log.d(tag, "recordingDate : "+recordingDate);
        Log.d(tag,"flock_id : "+flock_id);
        Log.d(tag, "flock_name : "+flock_name);
        Log.d(tag, "flock_type : "+flock_type);
        Log.d(tag, "flock_period : "+flock_period);
        Log.d(tag, "location_id : "+location_id);
        Log.d(tag, "location_name : "+location_name);


        date = (TextView) findViewById(R.id.recording_date);
        Log.d("Layerfarm","Recording date = "+recordingDate);
        // inisialisasi BottomNavigaionView
        final BottomNavigationView bottomNavigationView = findViewById(R.id.btm_nav);
        bottomNavigationView.setVisibility(View.GONE);
        //recording untuk menu recording yang baru mulai recording
//        if (recordingDate == null && status == null){
        Log.d("zzzs","recordingdate == null");
//            recordingDate = help.getStartRecordingDate(flock_id);
//            Log.d("zzzs", "recordingDated = " + recordingDate);
//            ID = dataSource.getRecordingID(flock_id, recordingDate);
//            if (ID == 0) {
//                status = "new";
//            } else
//                status = "update";
//            String module = "layerfarm";
//            String function_name = "layerfarm_get_next_recording_date";
//            String[] args = {flock_id};
//            Log.d("Layerfarm","flock_id = "+flock_id);
//            Parameter parameter = new Parameter(module, function_name, args);
//
//            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
//            String token2 = RetrofitData.getInstance().getToken2();
//            Call<List<String>> call = apiInterfaceJson.getNextRecordingDate(token2, parameter);
//
//            call.enqueue(new Callback<List<String>>() {
//                @Override
//                public void onResponse(Call<List<String>> call, Response<List<String>> response) {
//                    recordingDate = response.body().get(0);
//                    date.setText(recordingDate);
//
////                    for (int i=0; i < farmSyncList.size(); i++){
////                        location.add(farmSyncList.get(i).getName());
////                    }
//                    // kita set default nya Home Fragment
//                    loadFragment(new RecordingFragment());
//                }
//
//                @Override
//                public void onFailure(Call<List<String>> call, Throwable t) {
//                    String alert = t.getMessage();
//                    Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                }
//            });
//        if (status.equals("edit") || status.equals("add")) {
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_recording_per_house";
        String[] args;
        if (status.equals("edit")) {
            args = new String[]{recording_nid, "edit"};
        } else {
            args = new String[]{flock_id, "add"};
        }
        Log.d("Layerfarm", "flock_id = " + flock_id);
        Parameter parameter = new Parameter(module, function_name, args);

        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<RecordingEntry> call = apiInterfaceJson.getRecordingEntry(token2, parameter);

        call.enqueue(new Callback<RecordingEntry>() {
            @Override
            public void onResponse(Call<RecordingEntry> call, Response<RecordingEntry> response) {
                RecordingEntry recording = response.body();
                pgsBar.setVisibility(View.GONE);
                btm_state = true;
//                bottomNavigationView.setVisibility(View.VISIBLE);
                RecordingEntry.getInstance().setProfil_kandang(recording.getProfil_kandang());
                RecordingEntry.getInstance().setRecording(recording.getRecording());
                RecordingEntry.getInstance().setTransfer(recording.getTransfer());
                RecordingEntry.getInstance().setBody_weight(recording.getBody_weight());
                RecordingEntry.getInstance().setEnvirontment(recording.getEnvirontment());
                Log.d("Body weight", "bw response = "+recording.getBody_weight().getBw_value());
                Log.d("Body weight", "bw = "+RecordingEntry.getInstance().getBody_weight().getBw_value());
                recordingDate = recording.getProfil_kandang().getRecording_date();
                date.setText(recordingDate);
                SetData();

                // kita set default nya Home Fragment
                loadFragment(new RecordingFragment());
            }

            @Override
            public void onFailure(Call<RecordingEntry> call, Throwable t) {
                pgsBar.setVisibility(View.GONE);
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks recording failure = " + t.getMessage());
            }
        });
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.getMenu().findItem(R.id.action_clipboard).setChecked(true);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();
                Log.d("keyboard", "screenHeight = " + screenHeight);
                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("keyboard", "keypadHeight = " + keypadHeight);
                //int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
                //Log.d("keyboard", "heightDiff = " + heightDiff);
                if (keypadHeight > screenHeight * 0.15) {
                    Log.d("keyboard", "keyboard opened");
                    bottomNavigationView.animate()
                            .translationY(bottomNavigationView.getHeight()).setDuration(1000).start();
                    bottomNavigationView.setVisibility(View.GONE);
                } else {
                    Log.d("keyboard", "keyboard closed");
                    if (btm_state) {
                        bottomNavigationView.setVisibility(View.VISIBLE);
                        bottomNavigationView.animate()
                                .translationY(0).setDuration(300).start();
                    }
                }
            }
        });

    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onBackPressed() {

        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setIcon(R.drawable.check_mark)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        RecordingEntry.getInstance().setBody_weight(null);
                        RecordingEntry.getInstance().setProfil_kandang(null);
                        RecordingEntry.getInstance().setRecording(null);
                        RecordingEntry.getInstance().setTransfer(null);
                        DailyRecording dailyRecording = new DailyRecording();
                        DailyRecordingBodyWeight dailyRecordingBodyWeight = new DailyRecordingBodyWeight();
                        DailyRecordingManager.getInstance().setDailyRecording(dailyRecording);
                        DailyRecordingManager.getInstance().setBodyWeight(dailyRecordingBodyWeight);
                        finish();
//                        Fragment fragment = new RecordingFragment();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","load fragment");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_test, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        int i1 = menuItem.getItemId();
        if (i1 == R.id.action_feedback) {
            fragment = new TransferFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","ARGUMENT FRAGMENT");
            Log.d("zzzs", "recordingDate= "+recordingDate);
            Log.d("zzzs", "flock_id= "+flock_id);
            Log.d("zzzs","flock_name= "+flock_name);
            Log.d("zzzs", "flock_type= "+flock_type);
            Log.d("zzzs","flock_period= "+flock_period);
            Log.d("zzzs","location_name= "+location_name);
            Log.d("zzzs","location_id= "+location_id);
            Log.d("zzzs","status= "+status);
            Toast.makeText(getApplicationContext(), "Feedback clicked", Toast.LENGTH_SHORT).show();
        }
        else if (i1 == R.id.action_parameter) {
            fragment = new BodyWeightMainFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","ARGUMENT FRAGMENT");
            Log.d("zzzs", "recordingDate= "+recordingDate);
            Log.d("zzzs", "flock_id= "+flock_id);
            Log.d("zzzs","flock_name= "+flock_name);
            Log.d("zzzs", "flock_type= "+flock_type);
            Log.d("zzzs","flock_period= "+flock_period);
            Log.d("zzzs","location_name= "+location_name);
            Log.d("zzzs","location_id= "+location_id);
            Log.d("zzzs","status= "+status);
            Toast.makeText(getApplicationContext(), "Body Weight clicked", Toast.LENGTH_SHORT).show();
//        } else if (i1 == R.id.action_camera){
//            fragment = new PhotoMainFragment();
//            Bundle bundle = new Bundle();
//            bundle.putString("DATE",recordingDate);
//            bundle.putString("id_flock",flock_id);
//            bundle.putString("name_flock",flock_name);
//            bundle.putString("type",flock_type);
//            bundle.putString("period", flock_period);
//            bundle.putString("loc_name", location_name);
//            bundle.putString("loc_id", location_id);
//            bundle.putString("status",status);
//            fragment.setArguments(bundle);
//            Log.d("zzzs","ARGUMENT FRAGMENT");
//            Log.d("zzzs", "recordingDate= "+recordingDate);
//            Log.d("zzzs", "flock_id= "+flock_id);
//            Log.d("zzzs","flock_name= "+flock_name);
//            Log.d("zzzs", "flock_type= "+flock_type);
//            Log.d("zzzs","flock_period= "+flock_period);
//            Log.d("zzzs","location_name= "+location_name);
//            Log.d("zzzs","location_id= "+location_id);
//            Log.d("zzzs","status= "+status);
//            Toast.makeText(getApplicationContext(), "Photo Recording clicked", Toast.LENGTH_SHORT).show();

        }else if (i1 == R.id.action_clipboard){
            fragment = new RecordingFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","ARGUMENT FRAGMENT");
            Log.d("zzzs", "recordingDate= "+recordingDate);
            Log.d("zzzs", "flock_id= "+flock_id);
            Log.d("zzzs","flock_name= "+flock_name);
            Log.d("zzzs", "flock_type= "+flock_type);
            Log.d("zzzs","flock_period= "+flock_period);
            Log.d("zzzs","location_name= "+location_name);
            Log.d("zzzs","location_id= "+location_id);
            Log.d("zzzs","status= "+status);
            Toast.makeText(getApplicationContext(), "Recording clicked", Toast.LENGTH_SHORT).show();
        } else if (i1 == R.id.action_factory){
            fragment = new EnvirontmentFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DATE",recordingDate);
            bundle.putString("id_flock",flock_id);
            bundle.putString("name_flock",flock_name);
            bundle.putString("type",flock_type);
            bundle.putString("period", flock_period);
            bundle.putString("loc_name", location_name);
            bundle.putString("loc_id", location_id);
            bundle.putString("status",status);
            fragment.setArguments(bundle);
            Log.d("zzzs","ARGUMENT ENVIRONTMENT FRAGMENT");
            Log.d("zzzs", "recordingDate= "+recordingDate);
            Log.d("zzzs", "flock_id= "+flock_id);
            Log.d("zzzs","flock_name= "+flock_name);
            Log.d("zzzs", "flock_type= "+flock_type);
            Log.d("zzzs","flock_period= "+flock_period);
            Log.d("zzzs","location_name= "+location_name);
            Log.d("zzzs","location_id= "+location_id);
            Log.d("zzzs","status= "+status);
            Toast.makeText(getApplicationContext(), "Environtment clicked", Toast.LENGTH_SHORT).show();
        }
        return loadFragment(fragment);


    }

    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage(notif)
                .setIcon(R.drawable.camera)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void close(View view){
        AlertDialog diaBox = AskOption();
        diaBox.show();
    }

    public void SetData(){
        //FEED
        if (!RecordingEntry.getInstance().getRecording().getFeed_value().isEmpty()){
            ArrayList<DailyRecordingFeed> list_feed = new ArrayList<>();
            Set<Map.Entry<Integer, String>> hash_feed = RecordingEntry.getInstance().getRecording().getFeed_value().entrySet();
            for (Map.Entry<Integer, String> entry : hash_feed) {
                DailyRecordingFeed data = new DailyRecordingFeed();
                data.setAmount(entry.getValue());
                data.setFeed_id(Integer.toString(entry.getKey()));
                list_feed.add(data);
            }
            DailyRecordingManager.getInstance().getDailyRecording().setFeed(list_feed);
        }

        //OVK
        if (!RecordingEntry.getInstance().getRecording().getMedivac_value().isEmpty()){
            ArrayList<DailyRecordingOvk> list_ovk = new ArrayList<>();
            Set<Map.Entry<Integer, String>> hash_ovk = RecordingEntry.getInstance().getRecording().getMedivac_value().entrySet();
            for (Map.Entry<Integer, String> entry : hash_ovk) {
                DailyRecordingOvk data = new DailyRecordingOvk();
                data.setAmount(entry.getValue());
                data.setOvk_id(Integer.toString(entry.getKey()));
                list_ovk.add(data);
            }
            DailyRecordingManager.getInstance().getDailyRecording().setOvk(list_ovk);
        }

        //PRODUCTION
        if (!RecordingEntry.getInstance().getRecording().getProductionValues().isEmpty()){
            ArrayList<RecordingEntry.ProductionValue> list_production = RecordingEntry.getInstance().getRecording().getProductionValues();
            HashMap<String,String> hash_production = new HashMap<>();
            for (int i =0; i< list_production.size(); i++){
                String egg_name = list_production.get(i).getName();
                String qty = list_production.get(i).getProduction_kg();
                String weight = list_production.get(i).getProduction_weight();
                hash_production.put(egg_name, qty);
                hash_production.put(egg_name+"_weight", weight);
            }
            DailyRecordingManager.getInstance().getDailyRecording().setProduction(hash_production);
        }

        //MORTALITY
        if (!RecordingEntry.getInstance().getRecording().getMortality_value().isEmpty()){
            ArrayList<LinkedHashMap<String, String>> list_mortality = RecordingEntry.getInstance().getRecording().getMortality_value();
//                HashMap<String,String> hash_mortality = new HashMap<>();
//                hash_mortality = list_mortality;
            for (int i=0; i< list_mortality.size(); i++){
                Log.d("mortality","list mortality = "+list_mortality.get(i).toString());
            }
            DailyRecordingManager.getInstance().getDailyRecording().setMortality(list_mortality);
            Log.d("mortality","daily recording empty");
        }

        //CHICK OUT
        if (!RecordingEntry.getInstance().getTransfer().getAfkir_value().isEmpty()){
            ArrayList<DailyRecordingSpentHen> list_spenthen = RecordingEntry.getInstance().getTransfer().getAfkir_value();
            DailyRecordingManager.getInstance().getDailyRecording().setAfkir(list_spenthen);
        }

        //TRANSFER
        if (!RecordingEntry.getInstance().getTransfer().getTransfer_value().isEmpty()){
            ArrayList<DailyRecordingTransfer> list_transfer = RecordingEntry.getInstance().getTransfer().getTransfer_value();
            DailyRecordingManager.getInstance().getDailyRecording().setPindahKandang(list_transfer);
        }

        //BODY WEIGHT
        if (!RecordingEntry.getInstance().getBody_weight().getBw_value().isEmpty()){
            String[] bw = RecordingEntry.getInstance().getBody_weight().getBw_value().split(" ");
            DailyRecordingManager.getInstance().getBodyWeight().setBodyWeight(bw);
        }

        //ENVIRONTMENT
        if (!RecordingEntry.getInstance().getEnvirontment().getLight_duration().isEmpty()){
            LinkedHashMap<String, String> light_duration= RecordingEntry.getInstance().getEnvirontment().getLight_duration();
            ArrayList<String> light_dur = new ArrayList<>();
            light_dur.addAll(light_duration.values());
            DailyRecordingManager.getInstance().getDailyRecording().setLightDuration(light_dur);
        }
        if (!RecordingEntry.getInstance().getEnvirontment().getHouse_temperature().isEmpty()){
            LinkedHashMap<String, String> temperature = RecordingEntry.getInstance().getEnvirontment().getHouse_temperature();
            ArrayList<String> env_temperature = new ArrayList<>();
            env_temperature.addAll(temperature.values());
            DailyRecordingManager.getInstance().getDailyRecording().setTemp(env_temperature);
        }
        if (!RecordingEntry.getInstance().getEnvirontment().getHouse_humidity().isEmpty()){
            LinkedHashMap<String, String> humidity = RecordingEntry.getInstance().getEnvirontment().getHouse_humidity();
            ArrayList<String> env_humidity = new ArrayList<>();
            env_humidity.addAll(humidity.values());
            DailyRecordingManager.getInstance().getDailyRecording().setHumid(env_humidity);
        }
        if (!RecordingEntry.getInstance().getEnvirontment().getLight_intensity().isEmpty()){
            String light_intensity = RecordingEntry.getInstance().getEnvirontment().getLight_intensity();
            DailyRecordingManager.getInstance().getDailyRecording().setLightIntensity(light_intensity);
        }

    }
}