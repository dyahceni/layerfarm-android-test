package com.layerfarm.recording.fragment.bodyweight;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingBodyWeight;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.model.SubmitDailyRecordingBw;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.model.BodyWeight;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.setting.DataHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BodyWeightMainFragment extends android.support.v4.app.Fragment {

    //Declare Variable
    ListView listViewBodyWeight;
    AlertDialog.Builder dialog;
    List<BodyWeight> itemList = new ArrayList<BodyWeight>();
    AdapterBodyWeight adapter;
    DataHelper SQLite = new DataHelper(getActivity());

    Button saveButton;
    EditText bodyWeightEditText;
    private ProgressBar pgsBar;


    public static final String TAG_ID = "id";
    public static final String TAG_DAILY_RECORDING = "daily_recording_id";
    public static final String TAG_BODY_WEIGHT = "body_weight";

    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status;
    String recordingDate;

    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_body_weight_main, container, false);

        bodyWeightEditText = (EditText) view.findViewById(R.id.bodyWeightEditText);
        saveButton = (Button) view.findViewById(com.layerfarm.setting.R.id.saveButton);
        pgsBar = (ProgressBar) view.findViewById(R.id.pBar);


        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

        //jika sudah ada isinya
        if (DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight().length != 0){
            DailyRecordingBodyWeight a = DailyRecordingManager.getInstance().getBodyWeight();
            String[] bw_data = a.getBodyWeight();
            String val = TextUtils.join(" ",bw_data);
            bodyWeightEditText.setText(val);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                pgsBar.setVisibility(arg0.VISIBLE);
                save();
                //getActivity().finish();


            }
        });

//
//        FloatingActionButton fab = (FloatingActionButton) view.findViewById(com.layerfarm.setting.R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                /*
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                        */
//                //BodyWeightInputFragment fragmentInputBW = new BodyWeightInputFragment();
//                //buat object fragmentkedua
//
//                /*
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.frame_test, fragmentInputBW)
//                        //menggant fragment
//                        .addToBackStack(null)
//                        //menyimpan fragment
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        //transisi fragment
//                        .commit();
//                //mengeksekusi fragment transaction
//
//*/
//                BodyWeightInputFragment fragment = new BodyWeightInputFragment();
//                Bundle arguments = new Bundle();
//                arguments.putString("DATE",recordingDate);
//                arguments.putString("id_flock",flock_id);
//                arguments.putString("name_flock",flock_name);
//                arguments.putString("type",flock_type);
//                arguments.putString("period", flock_period);
//                arguments.putString("loc_name", location_name);
//                arguments.putString("loc_id", location_id);
//                arguments.putString("status",status);
//                fragment.setArguments(arguments);
//                FragmentManager fragmentManager = getFragmentManager();
//                final FragmentTransaction ft = fragmentManager.beginTransaction();
//                ft.replace(R.id.frame_test, fragment);
//                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                ft.commit();
//
////                FragmentManager fragmentManager = getFragmentManager();
////                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////                fragmentTransaction.replace(R.id.frame_test, new BodyWeightInputFragment());
////
////                //fragmentTransaction.addToBackStack(null);
////                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
////                fragmentTransaction.commit();
//
//            }
//        });

//        SQLite = new DataHelper(getActivity().getApplicationContext());
//        listViewBodyWeight = (ListView) view.findViewById(com.layerfarm.recording.R.id.list_view_body_weight);
//        adapter = new AdapterBodyWeight(getActivity(), itemList);
//        listViewBodyWeight.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//        listViewBodyWeight.invalidateViews();

/*
        listViewBodyWeight.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                final String idx = itemList.get(position).getId();
                final String daily_recording_id = itemList.get(position).getDaily_recording_id();
                final String body_weight = itemList.get(position).getBody_weight();

                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(getActivity().getWindow().getContext());
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:


                                Intent intent = new Intent(MedicationVaccinationNewActivity.this, UpdateMedicationVaccination.class);
                                intent.putExtra(TAG_ID, idx);
                                intent.putExtra(TAG_PRODUCT_NAME, product_name);
                                intent.putExtra(TAG_VENDOR_ID, vendor_id);
                                intent.putExtra(TAG_CAPACITY, capacity);
                                intent.putExtra(TAG_UNIT, unit);
                                intent.putExtra(TAG_TYPE, type);
                                intent.putExtra(TAG_STATUS, status);
                                intent.putExtra(TAG_RID, rid);
                                startActivity(intent);
                                break;

                            case 1:

                                SQLite.deleteBodyWeight(Integer.parseInt(idx));
                                //SQLite.deleteMedicationVaccination(idx);
                                itemList.clear();
                                getAllData();
                                break;

                        }
                    }
                }).show();



            }
        });
        */


        getAllData();

        return view;
    }
    private void save() {



        if (String.valueOf(bodyWeightEditText.getText()).equals(null) || String.valueOf(bodyWeightEditText.getText()).equals("")) {
            Toast.makeText(getActivity().getApplicationContext(),
                    "Please input body weight first ...", Toast.LENGTH_SHORT).show();
        } else {

        /*
        String data = bodyWeightEditText.getText().toString().trim();
        String[] items = data.split("-");
        for (String item : items)
        {
            SQLite.insertBodyWeight(1,(Float.parseFloat(item)));
        }
        */

//            SQLite = new DataHelper(getActivity());
//            SQLiteDatabase db = database.getWritableDatabase();
//            //get last record daily recording
//            String selectQuery = "SELECT id FROM daily_recording ORDER BY id DESC LIMIT 1";
//            Cursor cursor = db.rawQuery(selectQuery, null);
//            String strlastdailyrecording = "";
//            if (cursor.moveToFirst())
//                strlastdailyrecording = cursor.getString(cursor.getColumnIndex("id"));
//            cursor.close();


            String data = bodyWeightEditText.getText().toString();
            final String[] bw = data.split(" ");
            //            SQLite.insertBodyWeightData(strlastdailyrecording, data);

            String module = "layerfarm";
            String function_name = "layerfarm_save_daily_recording";

//        DailyRecording.getInstance().setHouseId(flock_id);
//        DailyRecording.getInstance().setRecordingDate(recordingDate);
//        DailyRecording dailyRecording = new DailyRecording(flock_id,recordingDate,production);
//        args.setProduction(production);
//        DailyRecording.getInstance().setProduction(production);

            final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
            args.setHouseId(flock_id);
            args.setRecordingDate(recordingDate);
            args.setForceDate("TRUE");
            DailyRecordingManager.getInstance().setDailyRecording(args);
            SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
            String token2 = RetrofitData.getInstance().getToken2();
            Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
            call.enqueue(new Callback<String[]>() {
                @Override
                public void onResponse(Call<String[]> call, Response<String[]> response) {
                    String[] recording = response.body();
                    if (recording[0].matches("[0-9]+")){
                        String module = "bw_recording";
                        String function_name = "bw_recording_save_body_weight";
                        final DailyRecordingBodyWeight args = DailyRecordingManager.getInstance().getBodyWeight();

                        args.setRecordingDate(recordingDate);
                        args.setHouseId(flock_id);
                        args.setBodyWeight(bw);

                        DailyRecordingManager.getInstance().setBodyWeight(args);
                        SubmitDailyRecordingBw parameter = new SubmitDailyRecordingBw(module, function_name, args);

                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                        String token2 = RetrofitData.getInstance().getToken2();
                        Call<String[]> callbw = apiInterfaceJson.submitDailyRecordingBw(token2, parameter);
                        callbw.enqueue(new Callback<String[]>() {
                            @Override
                            public void onResponse(Call<String[]> call, Response<String[]> response) {
                                String[] recording = response.body();
                                pgsBar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Entry Body Weight Successfully nid: "+ recording[0], Toast.LENGTH_LONG).show();
//                                BodyWeightMainFragment fragment = new BodyWeightMainFragment();
//                                Bundle arguments = new Bundle();
//                                arguments.putString("DATE",recordingDate);
//                                arguments.putString("id_flock",flock_id);
//                                arguments.putString("name_flock",flock_name);
//                                arguments.putString("type",flock_type);
//                                arguments.putString("period", flock_period);
//                                arguments.putString("loc_name", location_name);
//                                arguments.putString("loc_id", location_id);
//                                arguments.putString("status",status);
//                                fragment.setArguments(arguments);
//                                FragmentManager fragmentManager = getFragmentManager();
//                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.frame_test, fragment);
//                                //fragmentTransaction.addToBackStack(null);
//                                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                                fragmentTransaction.commit();
                            }

                            @Override
                            public void onFailure(Call<String[]> call, Throwable t) {
                                String alert = t.getMessage();
                                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                                pgsBar.setVisibility(View.GONE);
//                    dataSource.create_recording(recordingDate,flock_id,args);
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<String[]> call, Throwable t) {
                    String alert = t.getMessage();
                    Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                    dataSource.create_recording(recordingDate,flock_id,args);
                }
            });

        }
        //getActivity().finish();
        //}
    }

    private void getAllData() {
//        ArrayList<HashMap<String, String>> row = SQLite.getAllBodyWeight();
        Log.d("Layerfarm", "length : "+DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight().length);
        if ( DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight().length == 0){
            if (!RecordingEntry.getInstance().getBody_weight().getBw_value().isEmpty()){
//                String bw = RecordingEntry.getInstance().getBody_weight().getBw_value();
                String bw = RecordingEntry.getInstance().getBody_weight().getBw_value();
                Log.d("Layerfarm", "body weight = 0 => "+ bw);
                String[] bw_split = bw.split(" ");
                DailyRecordingManager.getInstance().getBodyWeight().setBodyWeight(bw_split);
            }
        }

        if (DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight().length != 0){
            BodyWeight data = new BodyWeight();
//            DailyRecordingBodyWeight a = DailyRecordingManager.getInstance().getBodyWeight();
            String[] bw_data = DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight();
            String val = TextUtils.join(" ",bw_data);
            Log.d("Layerfarm", "body weight != 0 => "+ val);
            data.setBody_weight(val);
            bodyWeightEditText.setText(val);
//            itemList.add(data);
        }

//        for (int i = 0; i < row.size(); i++) {
//            String id = row.get(i).get(TAG_ID);
//            String daily_recording_id = row.get(i).get(TAG_DAILY_RECORDING);
//            String body_weight = row.get(i).get(TAG_BODY_WEIGHT);
//
//            BodyWeight data = new BodyWeight();
//
//            data.setId(id);
//            data.setDaily_recording_id(daily_recording_id);
//            data.setBody_weight(body_weight);
//
//            itemList.add(data);
//        }

//        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            String data = bodyWeightEditText.getText().toString();
            final String[] bw = data.split(" ");
            final DailyRecordingBodyWeight args = DailyRecordingManager.getInstance().getBodyWeight();

            args.setRecordingDate(recordingDate);
            args.setHouseId(flock_id);
            args.setBodyWeight(bw);

            DailyRecordingManager.getInstance().setBodyWeight(args);
        }catch (Exception e){

        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        itemList.clear();
        getAllData();
    }

}
