package com.layerfarm.recording.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingSpentHen;
import com.layerfarm.layerfarm.model.DailyRecordingTransfer;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.recording.model.HatchDate;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnvirontmentFragment extends android.support.v4.app.Fragment {
    private String Date = "DATE";
    helper help;
    private DBDataSource dataSource;
    DatabaseHelper db;
    String recordingDate;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status;
    private BottomNavigationView bottomNavigationView;
    EditText tx_light_intensity,tx_temperature_morning,tx_temperature_noon,tx_temperature_evening,tx_humidity_morning,tx_humidity_noon,
            tx_humidity_evening;
    Spinner duration_start, duration_finish;
    TextView tx_house_profile, house_profile_title;
    private ProgressBar pgsBar;
    private ArrayList<RecordingEntry.Hatching> hatchDate;
    ArrayAdapter<CharSequence> adapter;
    String[] duration = {"00:00",
            "00:15",
            "00:30",
            "00:45",
            "01:00",
            "01:15",
            "01:30",
            "01:45",
            "02:00",
            "02:15",
            "02:30",
            "02:45",
            "03:00",
            "03:15",
            "03:30",
            "03:45",
            "04:00",
            "04:15",
            "04:30",
            "04:45",
            "05:00",
            "05:15",
            "05:30",
            "05:45",
            "06:00",
            "06:15",
            "06:30",
            "06:45",
            "07:00",
            "07:15",
            "07:30",
            "07:45",
            "08:00",
            "08:15",
            "08:30",
            "08:45",
            "09:00",
            "09:15",
            "09:30",
            "09:45",
            "10:00",
            "10:15",
            "10:30",
            "10:45",
            "11:00",
            "11:15",
            "11:30",
            "11:45",
            "12:00",
            "12:15",
            "12:30",
            "12:45",
            "13:00",
            "13:15",
            "13:30",
            "13:45",
            "14:00",
            "14:15",
            "14:30",
            "14:45",
            "15:00",
            "15:15",
            "15:30",
            "15:45",
            "16:00",
            "16:15",
            "16:30",
            "16:45",
            "17:00",
            "17:15",
            "17:30",
            "17:45",
            "18:00",
            "18:15",
            "18:30",
            "18:45",
            "19:00",
            "19:15",
            "19:30",
            "19:45",
            "20:00",
            "20:15",
            "20:30",
            "20:45",
            "21:00",
            "21:15",
            "21:30",
            "21:45",
            "22:00",
            "22:15",
            "22:30",
            "22:45",
            "23:00",
            "23:15",
            "23:30",
            "23:45",
            "24:00",
};
    ViewGroup profileView;
    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_environtment, container, false);
        dataSource =  new DBDataSource(getContext());
        dataSource.open();

        help = new helper(getContext());
        help.open();
        db = new DatabaseHelper(getContext());
        pgsBar = (ProgressBar) view.findViewById(R.id.pBar);
        Button submit = (Button) view.findViewById(R.id.Submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pgsBar.setVisibility(v.VISIBLE);
                if (CheckBeforeSubmit()){
                    Submit();
                }else{
                    AskOption(getResources().getString(R.string.warning_duration));
                }
            }
        });
        tx_light_intensity = (EditText) view.findViewById(R.id.light_intensity);
        tx_temperature_morning = (EditText) view.findViewById(R.id.temperature_morning);
        tx_temperature_noon = (EditText) view.findViewById(R.id.temperature_noon);
        tx_temperature_evening = (EditText) view.findViewById(R.id.temperature_evening);
        tx_humidity_morning = (EditText) view.findViewById(R.id.humidity_morning);
        tx_humidity_noon = (EditText) view.findViewById(R.id.humidity_noon);
        tx_humidity_evening = (EditText) view.findViewById(R.id.humidity_evening);
        tx_house_profile = (TextView) view.findViewById(R.id.House);
        house_profile_title = (TextView) view.findViewById(R.id.house_profile_title);
        duration_start = (Spinner) view.findViewById(R.id.duration_start);
        duration_finish = (Spinner) view.findViewById(R.id.duration_finish);

        adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, duration);

        duration_start.setAdapter(adapter);
        duration_start.setSelected(true);

        duration_finish.setAdapter(adapter);
        duration_finish.setSelected(true);

        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

//        Log.d("zzzs","FRAGMENT ENVIRONTMENT");
//        Log.d("zzzs", "recordingDate= "+recordingDate);
//        Log.d("zzzs", "flock_id= "+flock_id);
//        Log.d("zzzs","flock_name= "+flock_name);
//        Log.d("zzzs", "flock_type= "+flock_type);
//        Log.d("zzzs","flock_period= "+flock_period);
//        Log.d("zzzs","location_name= "+location_name);
//        Log.d("zzzs","location_id= "+location_id);
//        Log.d("zzzs","status= "+status);




        hatchDate = RecordingEntry.getInstance().getProfil_kandang().getHatching();
        profileView = (ViewGroup) view.findViewById(R.id.bird_profile);
        //menampilkan profile untuk masing- masing
//        tx_house_profile.setText("House : "+location_name+" - "+flock_name+" - "+flock_type+" "+flock_period);
        house_profile_title.setText(RecordingEntry.getInstance().getProfil_kandang().getTitle());
        String markup_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getMarkup();
        String prefix_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getPrevix();
        tx_house_profile.setText(prefix_kandang+markup_kandang);

        for (int i =0; i< hatchDate.size();i++){
            //Log.d("zzzs","hatch date: "+hatchDate.get(i).getHatchDate());
            //Log.d("zzzs","strain: "+hatchDate.get(i).getStrain());
            ViewHouseProfile(hatchDate.get(i));
            //ViewMortality(hatchDate.get(i).getHatchDate(), hatchDate.get(i).getStrain());
        }

//        long ID = dataSource.getRecordingID(flock_id, recordingDate);
//        if (ID != 0){
//            setEnvirontmnet(ID);
//        }

        if (!DailyRecordingManager.getInstance().getDailyRecording().getLightIntensity().isEmpty()){
            tx_light_intensity.setText(DailyRecordingManager.getInstance().getDailyRecording().getLightIntensity());
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getLightDuration().isEmpty()){
            ArrayList<String> light_duration = DailyRecordingManager.getInstance().getDailyRecording().getLightDuration();
            int pos_start = adapter.getPosition(light_duration.get(0));
            int pos_end = adapter.getPosition(light_duration.get(1));
            duration_start.setSelection(pos_start);
            duration_finish.setSelection(pos_end);

        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getTemp().isEmpty()){
            ArrayList<String> temp = DailyRecordingManager.getInstance().getDailyRecording().getTemp();
            tx_temperature_morning.setText(temp.get(0));
            tx_temperature_noon.setText(temp.get(1));
            tx_temperature_evening.setText(temp.get(2));
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getHumid().isEmpty()){
            ArrayList<String> humid = DailyRecordingManager.getInstance().getDailyRecording().getHumid();
            tx_humidity_morning.setText(humid.get(0));
            tx_humidity_noon.setText(humid.get(1));
            tx_humidity_evening.setText(humid.get(2));
        }

        return view;
    }
    public boolean CheckBeforeSubmit(){
        boolean valid = true;
        String starttext = duration_start.getSelectedItem().toString();
        starttext = starttext.replace(":","");
        String finishtext = duration_finish.getSelectedItem().toString();
        finishtext = finishtext.replace(":","");
        int start = Integer.parseInt(starttext);
        int finish = Integer.parseInt(finishtext);
        if (start > finish){
            valid = false;
        }
//        else if (tx_temperature_morning.getText().toString().isEmpty()){
//            tx_temperature_morning.setError("Please fill the data");
//            valid = false;
//        }else if (tx_temperature_noon.getText().toString().isEmpty()){
//            tx_temperature_noon.setError("Please fill the data");
//            valid = false;
//        }else if (tx_temperature_evening.getText().toString().isEmpty()){
//            tx_temperature_evening.setError("Please fill the data");
//            valid = false;
//        }else if (tx_humidity_morning.getText().toString().isEmpty()){
//            tx_humidity_morning.setError("Please fill the data");
//            valid = false;
//        }else if (tx_humidity_noon.getText().toString().isEmpty()){
//            tx_humidity_noon.setError("Please fill the data");
//            valid = false;
//        }else if (tx_humidity_evening.getText().toString().isEmpty()){
//            tx_humidity_evening.setError("Please fill the data");
//            valid = false;
//        }
        return valid;
    }
    public void Submit(){

        String light_intensity = tx_light_intensity.getText().toString();
        String temperature_morning = tx_temperature_morning.getText().toString();
        String temperature_noon = tx_temperature_noon.getText().toString();
        String temperature_evening = tx_temperature_evening.getText().toString();
        String humidity_morning = tx_humidity_morning.getText().toString();
        String humidity_noon = tx_humidity_noon.getText().toString();
        String humidity_evening = tx_humidity_evening.getText().toString();
        String start_duration = duration_start.getSelectedItem().toString();
        String end_duration = duration_finish.getSelectedItem().toString();

        ArrayList<String> light_duration = new ArrayList<>();
        light_duration.add(start_duration);
        light_duration.add(end_duration);
        ArrayList<String> temp = new ArrayList<>();
        temp.add(temperature_morning);
        temp.add(temperature_noon);
        temp.add(temperature_evening);
        ArrayList<String> humid = new ArrayList<>();
        humid.add(humidity_morning);
        humid.add(humidity_noon);
        humid.add(humidity_evening);


        //BUAT MENGIRIM KE SERVER
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";
        Log.d("Layerfarm","flock_id = "+flock_id);
        DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        args.setHouseId(flock_id);
        args.setRecordingDate(recordingDate);
        args.setLightIntensity(light_intensity);
        args.setLightDuration(light_duration);
        args.setTemp(temp);
        args.setHumid(humid);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


        ApiInterface apiInterfaceJson;
        apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] recording = response.body();
//                SubmitDialog();
                pgsBar.setVisibility(View.GONE);
                SuccessDialog();
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                pgsBar.setVisibility(View.GONE);
                FailedDialog();
            }
        });



    }
    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Warning!!")
                .setMessage(notif)
                .setIcon(R.drawable.camera)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    private AlertDialog SubmitDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording berhasil ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        getActivity().finish();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    private AlertDialog SuccessDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording berhasil ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    private AlertDialog FailedDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording gagal ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    public void ViewHouseProfile(RecordingEntry.Hatching hatching){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View house = inflater.inflate(R.layout.house_profile, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView hatch = (TextView) house.findViewById(R.id.hatching);
        TextView total_bird = (TextView) house.findViewById(R.id.total_bird);
        TextView bird_age = (TextView) house.findViewById(R.id.bird_age);
        hatch.setText("["+hatching.getHatch_date()+"]");
        total_bird.setText(hatching.getTotal_bird_title()+hatching.getTotal_bird_value());
        bird_age.setText(hatching.getAge_bird_title()+hatching.getAge_bird_value());
        profileView.addView(house);
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            String light_intensity = tx_light_intensity.getText().toString();
            String temperature_morning = tx_temperature_morning.getText().toString();
            String temperature_noon = tx_temperature_noon.getText().toString();
            String temperature_evening = tx_temperature_evening.getText().toString();
            String humidity_morning = tx_humidity_morning.getText().toString();
            String humidity_noon = tx_humidity_noon.getText().toString();
            String humidity_evening = tx_humidity_evening.getText().toString();
            String start_duration = duration_start.getSelectedItem().toString();
            String end_duration = duration_finish.getSelectedItem().toString();

            ArrayList<String> light_duration = new ArrayList<>();
            light_duration.add(start_duration);
            light_duration.add(end_duration);
            ArrayList<String> temp = new ArrayList<>();
            temp.add(temperature_morning);
            temp.add(temperature_noon);
            temp.add(temperature_evening);
            ArrayList<String> humid = new ArrayList<>();
            humid.add(humidity_morning);
            humid.add(humidity_noon);
            humid.add(humidity_evening);


            //BUAT MENGIRIM KE SERVER
            String module = "layerfarm";
            String function_name = "layerfarm_save_daily_recording";
            Log.d("Layerfarm","flock_id = "+flock_id);
            DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
            args.setHouseId(flock_id);
            args.setRecordingDate(recordingDate);
            args.setLightIntensity(light_intensity);
            args.setLightDuration(light_duration);
            args.setTemp(temp);
            args.setHumid(humid);
            args.setForceDate("TRUE");
            DailyRecordingManager.getInstance().setDailyRecording(args);
        }catch (Exception e){

        }

    }
    public void setEnvirontmnet(long id){
        SQLiteDatabase database;
        database= db.getWritableDatabase();
        String selectQuery= "SELECT * FROM daily_recording WHERE id = '"+id+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String light_intensity = "";
        String temperature_morning = "";
        String temperature_noon = "";
        String temperature_evening = "";
        String humidity_morning = "";
        String humidity_noon="";
        String humidity_evening= "";
        String light_duration_start = "";
        String light_duration_end = "";
        if(cursor.moveToFirst()) {
            light_intensity = cursor.getString(cursor.getColumnIndex("light_intensity"));
            temperature_morning = cursor.getString(cursor.getColumnIndex("cage_temperature_1"));
            temperature_noon = cursor.getString(cursor.getColumnIndex("cage_temperature_2"));
            temperature_evening = cursor.getString(cursor.getColumnIndex("cage_temperature_3"));
            humidity_morning = cursor.getString(cursor.getColumnIndex("humidity_1"));
            humidity_noon = cursor.getString(cursor.getColumnIndex("humidity_2"));
            humidity_evening = cursor.getString(cursor.getColumnIndex("humidity_3"));
            light_duration_start = cursor.getString(cursor.getColumnIndex("light_duration_start"));
            light_duration_end = cursor.getString(cursor.getColumnIndex("light_duration_end"));
        }
        cursor.close();
        tx_light_intensity.setText(light_intensity);
        tx_temperature_morning.setText(temperature_morning);
        tx_temperature_noon.setText(temperature_noon);
        tx_temperature_evening.setText(temperature_evening);
        tx_humidity_morning.setText(humidity_morning);
        tx_humidity_noon.setText(humidity_noon);
        tx_humidity_evening.setText(humidity_evening);
    }
    public void updateEnvirontment(long id){
        SQLiteDatabase database;
        database = db.getWritableDatabase();
        String light_intensity = tx_light_intensity.getText().toString();
        String temperature_morning = tx_temperature_morning.getText().toString();
        String temperature_noon = tx_temperature_noon.getText().toString();
        String temperature_evening = tx_temperature_evening.getText().toString();
        String humidity_morning = tx_humidity_morning.getText().toString();
        String humidity_noon = tx_humidity_noon.getText().toString();
        String humidity_evening = tx_humidity_evening.getText().toString();

        ContentValues environtment = new ContentValues();
        environtment.put("light_intensity", light_intensity);
        environtment.put("cage_temperature_1", temperature_morning);
        environtment.put("cage_temperature_2", temperature_noon);
        environtment.put("cage_temperature_3", temperature_evening);
        environtment.put("humidity_1", humidity_morning);
        environtment.put("humidity_2", humidity_noon);
        environtment.put("humidity_3", humidity_evening);

        String Filter = "id =" + id;

        //update query
        long updateRecording = database.update("daily_recording", environtment, Filter, null);
    }
}
