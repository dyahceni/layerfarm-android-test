package com.layerfarm.recording.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingFeed;
import com.layerfarm.layerfarm.model.DailyRecordingOvk;
import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.activity_option.OptionFeed;
import com.layerfarm.recording.activity_option.OptionMedicine;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.layerfarm.model.EggValue;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.recording.model.Feed;
import com.layerfarm.recording.model.HatchDate;
import com.layerfarm.recording.model.Medicine;
import com.layerfarm.recording.model.Mortality.Mortality;
import com.layerfarm.recording.model.Mortality.MortalityData;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class RecordingFragment extends android.support.v4.app.Fragment {
    private Button submit;
    private EditText edNote,edMortality, total_feed;
    private TextView txMortality, tx_recording_date, tx_house_profile, tx_add_more_feed, tx_add_more_ovk, house_profile_title;
    private ViewGroup more_feed, more_medicine, mortalityView, eggsView, profileView, mortalityItemsView;
    private BottomNavigationView bottomNavigationView;

    //inisialisasi array list untuk masing2 pengelompokan di recording
    private ArrayList<Feed> ListFeed;
    private ArrayList<Medicine> ListMedicine;
    private ArrayList<EggValue> ListEggs;
    private ArrayList<MortalityData> ListMortality;
    private ArrayList<String> mortalityCategory;
    private List<RecordingEntry.Production> eggsName;

    private Spinner feed_type;
    private Spinner medication_name;
    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status;
    private ProgressBar pgsBar;
    ArrayList<String> feed_value, ovk_value;
    ArrayList<RecordingEntry.Hatching> hatchDate;

    Integer feed_child =0 , medicine_child = 0;

    //inisialisasi kontroller/ Data Source
    DatabaseHelper db;
    helper help;
    String recordingDate;
    private DBDataSource dataSource;
    private int i = 0;
    private Handler hdlr = new Handler();
    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recording_entry, container, false);

        edNote = (EditText) view.findViewById(R.id.Note);
        house_profile_title = (TextView) view.findViewById(R.id.house_profile_title);
        tx_house_profile = (TextView) view.findViewById(R.id.House);
        pgsBar = (ProgressBar) view.findViewById(R.id.pBar);

        submit = (Button) view.findViewById(R.id.Submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pgsBar.setVisibility(v.VISIBLE);
                if (CheckBeforeSubmit()){
                    Submit();
                }else{
                    pgsBar.setVisibility(v.GONE);
                    AskOption("Please fill the data");
                }
            }
        });
        //instantiasi kelas DBDataSource
        dataSource =  new DBDataSource(getContext());
        dataSource.open();

        help = new helper(getContext());
        help.open();
        db = new DatabaseHelper(getContext());


        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

//        Set<Map.Entry<String, String>> entries = RecordingEntry.getInstance().getRecording().getMortality_type().entrySet();
//        for (Map.Entry<String, String> entry : entries) {
//            Log.d("Layerfarm","key : "+entry.getKey());
//            Log.d("Layerfarm","value : "+entry.getValue());
//        }


        //deklarasi view group
        more_feed = (ViewGroup) view.findViewById(R.id.more_feed);
        more_medicine = (ViewGroup) view.findViewById(R.id.more_medicine);
        profileView = (ViewGroup) view.findViewById(R.id.bird_profile);
        mortalityView = (ViewGroup) view.findViewById(R.id.mortality);
        eggsView = (ViewGroup) view.findViewById(R.id.eggs);

        //eggName adalah jenis jenis telur
        eggsName = RecordingEntry.getInstance().getRecording().getProduction();
        //mendapatkan hatch date untuk flock ini
//        hatchDate = help.getHatchDate(flock_id,recordingDate);
        hatchDate = RecordingEntry.getInstance().getProfil_kandang().getHatching();

        //mendapatkan kategori kematian dari ayam
        Collection<String> mortality = RecordingEntry.getInstance().getRecording().getMortality_type().values();
        mortalityCategory = new ArrayList<>(mortality);

        //fungsi memasukkan feed ke dalam array yg kemudian digunakan di spinner
        Collection<String> feed = RecordingEntry.getInstance().getRecording().getOptions_feed().values();
        feed_value = new ArrayList<>(feed);


        //fungsi memasukkan medicine ke dalam array yg kemudian digunakan di spinner
        Collection<String> ovk = RecordingEntry.getInstance().getRecording().getOptions_medivac().values();
        ovk_value =  new ArrayList<>(ovk);

        tx_add_more_feed = (TextView) view.findViewById(R.id.add_more_feed);
        tx_add_more_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewFeed();
            }
        });

        tx_add_more_ovk = (TextView) view.findViewById(R.id.add_more_ovk);
        tx_add_more_ovk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewMedicine();
            }
        });



        //menampilkan box eggs
        for (int i =0; i< eggsName.size();i++){
            ViewEggsValue(eggsName.get(i));
        }

//        //menampilkan mortality ayam
//        for (int i =0; i< hatchDate.size();i++){
//            ViewMortality(hatchDate.get(i).getHatch_date(), hatchDate.get(i).getStrain());
//        }

        //menampilkan profile untuk masing- masing
//        tx_house_profile.setText("House : "+location_name+" - "+flock_name+" - "+flock_type+" "+flock_period);
        house_profile_title.setText(RecordingEntry.getInstance().getProfil_kandang().getTitle());
        String markup_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getMarkup();
        String prefix_kandang = RecordingEntry.getInstance().getProfil_kandang().getKandang().getPrevix();
        tx_house_profile.setText(prefix_kandang+markup_kandang);
        for (int i =0; i< hatchDate.size();i++){
            //Log.d("zzzs","hatch date: "+hatchDate.get(i).getHatchDate());
            //Log.d("zzzs","strain: "+hatchDate.get(i).getStrain());
//            ViewHouseProfile(hatchDate.get(i).getHatch_date(),hatchDate.get(i));
            ViewHouseProfile(hatchDate.get(i));
            ViewMortality(hatchDate.get(i).getHatch_date(), hatchDate.get(i).getStrain());
            //ViewMortality(hatchDate.get(i).getHatchDate(), hatchDate.get(i).getStrain());
        }
        //FEED
        if (DailyRecordingManager.getInstance().getDailyRecording().getFeed().isEmpty()){
            if (!RecordingEntry.getInstance().getRecording().getFeed_value().isEmpty()){
                ArrayList<DailyRecordingFeed> list_feed = new ArrayList<>();
                Set<Map.Entry<Integer, String>> hash_feed = RecordingEntry.getInstance().getRecording().getFeed_value().entrySet();
                for (Map.Entry<Integer, String> entry : hash_feed) {
                    DailyRecordingFeed data = new DailyRecordingFeed();
                    data.setAmount(entry.getValue());
                    data.setFeed_id(Integer.toString(entry.getKey()));
                    list_feed.add(data);
                }
                DailyRecordingManager.getInstance().getDailyRecording().setFeed(list_feed);
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getFeed().isEmpty()){
            setFeedConstumption();
        }
        else {
            ViewFeed();
        }
        //OVK
        if (DailyRecordingManager.getInstance().getDailyRecording().getOvk().isEmpty()){
            if (!RecordingEntry.getInstance().getRecording().getMedivac_value().isEmpty()){
                ArrayList<DailyRecordingOvk> list_ovk = new ArrayList<>();
                Set<Map.Entry<Integer, String>> hash_ovk = RecordingEntry.getInstance().getRecording().getMedivac_value().entrySet();
                for (Map.Entry<Integer, String> entry : hash_ovk) {
                    DailyRecordingOvk data = new DailyRecordingOvk();
                    data.setAmount(entry.getValue());
                    data.setOvk_id(Integer.toString(entry.getKey()));
                    list_ovk.add(data);
                }
                DailyRecordingManager.getInstance().getDailyRecording().setOvk(list_ovk);
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getOvk().isEmpty()){
            setOVKConsumption();
        }
        else {
            ViewMedicine();
        }

        //PRODUCTION
        if (DailyRecordingManager.getInstance().getDailyRecording().getProduction().isEmpty()){
            if (!RecordingEntry.getInstance().getRecording().getProductionValues().isEmpty()){
                ArrayList<RecordingEntry.ProductionValue> list_production = RecordingEntry.getInstance().getRecording().getProductionValues();
                HashMap<String,String> hash_production = new HashMap<>();
                for (int i =0; i< list_production.size(); i++){
                    String egg_name = list_production.get(i).getName();
                    String qty = list_production.get(i).getProduction_kg();
                    String weight = list_production.get(i).getProduction_weight();
                    hash_production.put(egg_name, qty);
                    hash_production.put(egg_name+"_weight", weight);
                }
                DailyRecordingManager.getInstance().getDailyRecording().setProduction(hash_production);
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getProduction().isEmpty()){
            setEggProduction();
        }

        //MORTALITY
        if (DailyRecordingManager.getInstance().getDailyRecording().getMortality().isEmpty()){
            if (!RecordingEntry.getInstance().getRecording().getMortality_value().isEmpty()){
                ArrayList<LinkedHashMap<String, String>> list_mortality = RecordingEntry.getInstance().getRecording().getMortality_value();
//                HashMap<String,String> hash_mortality = new HashMap<>();
//                hash_mortality = list_mortality;
                for (int i=0; i< list_mortality.size(); i++){
                Log.d("mortality","list mortality = "+list_mortality.get(i).toString());
                }
                DailyRecordingManager.getInstance().getDailyRecording().setMortality(list_mortality);
                Log.d("mortality","daily recording empty");
            }
        }
        if (!DailyRecordingManager.getInstance().getDailyRecording().getMortality().isEmpty()){
            Log.d("mortality","daily recording not empty");
            setRecordingMortality();
        }

        return view;

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Log.d("zzz","index child = "+feed_child);
                String result=data.getStringExtra("result");
                Log.d("zzz","result activityResult = "+result);
                View childView = more_feed.getChildAt(feed_child);
                EditText ed_feed_type = (EditText)(childView.findViewById(R.id.ed_feed_type));
                Log.d("zzz","result Edittext = "+ed_feed_type.getText().toString());
//                total_feed = (EditText)(childView.findViewById(R.id.total_feed));
                Log.d("zzz","result activityResult = "+result);
                ed_feed_type.setText(result);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){
                Log.d("zzz","index child = "+feed_child);
                String result=data.getStringExtra("result");
                Log.d("zzz","result activityResult = "+result);
                View childView = more_medicine.getChildAt(feed_child);
                EditText ed_medicine_type = (EditText)(childView.findViewById(R.id.ed_medvac_type));
                Log.d("zzz","result Edittext = "+ed_medicine_type.getText().toString());
//                total_feed = (EditText)(childView.findViewById(R.id.total_feed));
                Log.d("zzz","result activityResult = "+result);
                ed_medicine_type.setText(result);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
    public boolean CheckBeforeSubmit(){
        boolean valid1 =true, valid2 = true;
        //Check Feed
        int childCount = more_feed.getChildCount()-2;
        for (int c =0; c<childCount;c++){
            View childView = more_feed.getChildAt(c);
            EditText total_feed = (EditText)(childView.findViewById(R.id.total_feed));
            String total = (String)(total_feed.getText().toString());
            EditText feed_type = (EditText)(childView.findViewById(R.id.ed_feed_type));
            String type = feed_type.getText().toString();
            boolean andnot = type.isEmpty() && total.isEmpty();
            boolean and = type.isEmpty() || total.isEmpty();
            if (and && !andnot) {
                if (type.isEmpty()){
                    feed_type.setError("Please fill the data");
                    valid1 = false;
                    break;
                }else if (total.isEmpty()){
                    total_feed.setError("Please fill the data");
                    valid1 = false;
                    break;
                }
            }
        }
        //Check Medivac
        int childCount1 = more_medicine.getChildCount()-2;
        for (int c =0; c<childCount1;c++){
            View childView = more_medicine.getChildAt(c);
            EditText total_medicine = (EditText)(childView.findViewById(R.id.total_medicine));
            String total = (String)(total_medicine.getText().toString());
            EditText medication_name = (EditText)(childView.findViewById(R.id.ed_medvac_type));
            String name = medication_name.getText().toString();
            boolean andnot = name.isEmpty() && total.isEmpty();
            boolean and = name.isEmpty() || total.isEmpty();
            if (and && !andnot) {
                if (name.isEmpty()){
                    medication_name.setError("Please fill the data");
                    valid2 = false;
                    break;
                }else if (total.isEmpty()){
                    total_medicine.setError("Please fill the data");
                    valid2 = false;
                    break;
                }
            }
        }
        if (valid2 && valid1){
            return true;
        }else return false;
    }
    public void Submit(){
        long ID = dataSource.getRecordingID(flock_id, recordingDate);
        Log.d("Layerfarm","Submit - flock_id :"+flock_id);
        Log.d("Layerfarm","Submit - recording date :"+recordingDate);
        String module = "layerfarm";
        String function_name = "layerfarm_save_daily_recording";

        HashMap<String, String> production = getListEggs();
        String note = edNote.getText().toString();

        Log.d("Layerfarm","flock_id = "+flock_id);
        final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
        args.setHouseId(flock_id);
        args.setRecordingDate(recordingDate);
        args.setProduction(production);
        args.setOvk(getListMedicine());
        args.setFeed(getListFeed());
        args.setMortality(getListMortality());
        args.setNote(note);
        args.setForceDate("TRUE");
        DailyRecordingManager.getInstance().setDailyRecording(args);
        SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                String[] recording = response.body();
                if (recording[0].equals("RECORDING: Hatch date: : Mortality/selection and/or transfer must less than  birds in this house.")){
                    pgsBar.setVisibility(View.GONE);
                    AskOption(recording[0]);
                }else{
                    pgsBar.setVisibility(View.GONE);
                    SuccessDialog();
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
//                dataSource.create_recording(recordingDate,flock_id,args);
                pgsBar.setVisibility(View.GONE);
                FailedDialog();
            }
        });

    }

    private AlertDialog AskOption(String notif) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Warning!!")
                .setMessage(notif)
                .setIcon(R.drawable.camera)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    private AlertDialog SuccessDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording berhasil ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }
    private AlertDialog FailedDialog() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity())
                .setTitle("Recording Success")
                .setMessage("Recording gagal ditambahkan")
                .setIcon(R.drawable.camera)
                .setNeutralButton("Oke", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        return myQuittingDialogBox;

    }

//    public void AddMoreFeed() {
//        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View feedView = inflater.inflate(R.layout.more_feed, null);
//        // Add the new row before the add field button.
//        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
//        Spinner feed_type = (Spinner)(feedView.findViewById(R.id.feed_type));
//        feed_type.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, feed_value));
//        feed_type.setSelected(true);
//        more_feed.addView(feedView,more_feed.getChildCount()-2);
//    }
//    public void AddMoreMedicine(){
//        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View medicineView = inflater.inflate(R.layout.more_medicine,null);
//        // Add the new row before the add field button.
//        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
//        medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
//        medication_name.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, ovk_value));
//        medication_name.setSelected(true);
//        more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
//    }

    public void ViewMortality(String hatch, String strain){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mortalityInf = inflater.inflate(R.layout.mortality, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView Hatch = (TextView) mortalityInf.findViewById(R.id.hatch_date);
        Hatch.setText("Hatching "+ hatch +"| "+strain);
        mortalityItemsView = (ViewGroup) mortalityInf.findViewById(R.id.mortality_items);
        //membuat nama textview di mortalitycategory
        for (int i =0; i<mortalityCategory.size(); i++){
            ViewMortalityItems(mortalityCategory.get(i));
        }
        mortalityView.addView(mortalityInf);
    }
    public void ViewMortalityItems(String name){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mortalityInf = inflater.inflate(R.layout.mortality_items, null);

        TextView mortality_name = (TextView) (mortalityInf.findViewById(R.id.mortality_name));
        mortality_name.setText(name);
        mortalityItemsView.addView(mortalityInf);
    }
    public void ViewFeed(){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View feedView = inflater.inflate(R.layout.more_feed, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        Log.d("zzz","child selected awal = "+(more_feed.getChildCount()));
        final EditText ed_feed_type = (EditText)(feedView.findViewById(R.id.ed_feed_type));
        final EditText total_feed = (EditText)(feedView.findViewById(R.id.total_feed));
        ed_feed_type.setTag(more_feed.getChildCount()-2);
        ed_feed_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), OptionFeed.class);
                Log.d("zzz","child selected = "+ed_feed_type.getTag());
                feed_child = Integer.parseInt(ed_feed_type.getTag().toString());
                startActivityForResult(i, 1);
            }
        });
        ed_feed_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!total_feed.getText().toString().isEmpty())
                        ed_feed_type.setError("Fill Data");
                } else {
                    ed_feed_type.setError(null);
                    if (total_feed.getText().toString().isEmpty())
                        total_feed.setError("Fill Data");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        total_feed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!ed_feed_type.getText().toString().isEmpty())
                        total_feed.setError("Fill Data");
                } else {
                    total_feed.setError(null);
                    if (ed_feed_type.getText().toString().isEmpty())
                        ed_feed_type.setError("Fill Data");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        more_feed.addView(feedView,more_feed.getChildCount()-2);
    }
    public void ViewMedicine(){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View medicineView = inflater.inflate(R.layout.more_medicine,null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        Log.d("zzz","child selected awal = "+(more_medicine.getChildCount()));
        final EditText ed_medicine_type = (EditText)(medicineView.findViewById(R.id.ed_medvac_type));
        final EditText total_medicine = (EditText)(medicineView.findViewById(R.id.total_medicine));
        ed_medicine_type.setTag(more_medicine.getChildCount()-2);
        ed_medicine_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), OptionMedicine.class);
                Log.d("zzz","child selected = "+ed_medicine_type.getTag());
                feed_child = Integer.parseInt(ed_medicine_type.getTag().toString());
                startActivityForResult(i, 2);
            }
        });
        ed_medicine_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!total_medicine.getText().toString().isEmpty())
                        ed_medicine_type.setError("Fill Data");
                } else {
                    ed_medicine_type.setError(null);
                    if (total_medicine.getText().toString().isEmpty())
                        total_medicine.setError("Fill Data");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        total_medicine.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()==0) {
                    if (!ed_medicine_type.getText().toString().isEmpty())
                        total_medicine.setError("Fill Data");
                } else {
                    total_medicine.setError(null);
                    if (ed_medicine_type.getText().toString().isEmpty())
                        ed_medicine_type.setError("Fill Data");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
//        medication_name.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, ovk_value));
//        medication_name.setSelected(true);
        more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
    }
    public void ViewEggsValue(RecordingEntry.Production eggs){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View eggsInf = inflater.inflate(R.layout.egg_quality_view, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        TextView eggsName = (TextView) eggsInf.findViewById(R.id.eggs_name);
        TextView weightName = (TextView) eggsInf.findViewById(R.id.weight_name);
        eggsName.setText(eggs.getProduction_eggs());
        weightName.setText(eggs.getProduction_weight());
        eggsView.addView(eggsInf);
    }
    //    public void ViewHouseProfile(String hatch_date, String strain, int total){
    public void ViewHouseProfile(RecordingEntry.Hatching hatching){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View house = inflater.inflate(R.layout.house_profile, null);
        // Add the new row before the add field button.
        //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
//        int total = help.getTotal(flock_id,recordingDate,hatchDate);

        TextView hatch = (TextView) house.findViewById(R.id.hatching);
        TextView total_bird = (TextView) house.findViewById(R.id.total_bird);
        TextView bird_age = (TextView) house.findViewById(R.id.bird_age);
        hatch.setText("["+hatching.getHatch_date()+"]");
        total_bird.setText(hatching.getTotal_bird_title()+hatching.getTotal_bird_value());
        bird_age.setText(hatching.getAge_bird_title()+hatching.getAge_bird_value());
        profileView.addView(house);
    }

    //untuk mendapatkan data dari eggs box
    public HashMap<String, String> getListEggs(){
        int childCount = eggsView.getChildCount();
        HashMap<String, String> production  = new HashMap<>();
        for (int c =0; c<childCount;c++){
            View childView = eggsView.getChildAt(c);
            ArrayList<RecordingEntry.Production> eggs_component = RecordingEntry.getInstance().getRecording().getProduction();

            EditText qty_eggs = (EditText)(childView.findViewById(R.id.quality_eggs));
            String total_eggs = (String)(qty_eggs.getText().toString());
            EditText qty_weight = (EditText) (childView.findViewById(R.id.quality_weight));
            String weight = (String)(qty_weight.getText().toString());

            String eggs_name = eggs_component.get(c).getName();
            if (!total_eggs.isEmpty() || !weight.isEmpty()) {
                production.put(eggs_name, total_eggs);
                production.put(eggs_name+"_weight", weight);
            }
        }

        return production;
    }

    //untuk mendapatkan data dari feed box
    public ArrayList<DailyRecordingFeed> getListFeed() {
        ArrayList<DailyRecordingFeed> ListFeed = new ArrayList<>();
        int childCount = more_feed.getChildCount()-2;
        HashMap<Integer, String> feed = RecordingEntry.getInstance().getRecording().getOptions_feed();
        for (int c =0; c<childCount;c++){
            View childView = more_feed.getChildAt(c);
            EditText total_feed = (EditText)(childView.findViewById(R.id.total_feed));
            String total = (String)(total_feed.getText().toString());
            EditText feed_type = (EditText)(childView.findViewById(R.id.ed_feed_type));
//            Spinner feed_type = (Spinner)(childView.findViewById(R.id.feed_type));
            String type = feed_type.getText().toString();
            if (!type.isEmpty()){
                String feed_nid = Integer.toString(getKey(feed, type));
                if(!total.isEmpty()){
                    DailyRecordingFeed _feed = new DailyRecordingFeed();
                    _feed.setFeed_id(feed_nid);
                    _feed.setAmount(total);
                    ListFeed.add(_feed);
                }
            }

        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        return ListFeed;
    }

    //untuk mendapatkan data dari box medicine
    public ArrayList<DailyRecordingOvk> getListMedicine() {
        ArrayList<DailyRecordingOvk> ListMedicine = new ArrayList<>();
        int childCount = more_medicine.getChildCount()-2;
        HashMap<Integer, String> medicine = RecordingEntry.getInstance().getRecording().getOptions_medivac();
        for (int c =0; c<childCount;c++){
            View childView = more_medicine.getChildAt(c);
            EditText total_medicine = (EditText)(childView.findViewById(R.id.total_medicine));
            String total = (String)(total_medicine.getText().toString());
            EditText medication_name = (EditText)(childView.findViewById(R.id.ed_medvac_type));
//            Spinner medication_name = (Spinner)(childView.findViewById(R.id.medication_name));
            String name = medication_name.getText().toString();
            if (!name.isEmpty()){
                String ovk_nid = Integer.toString(getKey(medicine, name));
                if (!total.isEmpty()){
                    DailyRecordingOvk _ovk = new DailyRecordingOvk();
                    _ovk.setOvk_id(ovk_nid);
                    _ovk.setAmount(total);
                    ListMedicine.add(_ovk);
                }
            }

        }
        return ListMedicine;
    }

    //untuk mendapatkan data dari mortality box
    public ArrayList<LinkedHashMap<String, String>> getListMortality() {
        ArrayList<LinkedHashMap<String, String>> list_mortality = new ArrayList<>();
        ArrayList<MortalityData> ListMortality = new ArrayList<>();
        HashMap<String, String> mortality_category = RecordingEntry.getInstance().getRecording().getMortality_type();
        int childCount = mortalityView.getChildCount();
        Log.d("hasil", "count mortality: "+childCount);
        for (int c =0; c<childCount;c++){
            LinkedHashMap<String, String> list_items = new LinkedHashMap<>();
            View childView = mortalityView.getChildAt(c);
            ArrayList<Mortality> List = new ArrayList<>();
            TextView hatch = (TextView) (childView.findViewById(R.id.hatch_date));
            String[] split_txt = hatch.getText().toString().split(" ");
            String[] hatch_date = split_txt[1].split("\\|");
            Log.d("hasil","hatch "+hatch_date[0]);
            int mortalitychild = mortalityItemsView.getChildCount();
            Log.d("hasil","items "+mortalitychild);
            ViewGroup child = childView.findViewById(R.id.mortality_items);
            for (int i =0; i< child.getChildCount(); i++){
                View childMortality = child.getChildAt(i);
                Mortality mor = new Mortality();
                TextView txMortality = (TextView) (childMortality.findViewById(R.id.mortality_name));
                EditText edMortality = (EditText) (childMortality.findViewById(R.id.mortality_value));
                Log.d("hasil","edmortality "+edMortality.getText().toString());
                Log.d("hasil","txmortality "+txMortality.getText().toString());
                String mortality_name = getKey(mortality_category, txMortality.getText().toString());
                Log.d("Layerfarm","mortality_name : "+mortality_name);
                //jika edit text tidak kosong
                if (!edMortality.getText().toString().matches("")) {
                    list_items.put(mortality_name,edMortality.getText().toString());
                    mor.setValue(edMortality.getText().toString());
                    mor.setName(txMortality.getText().toString());
                    Log.d("hasil", txMortality.getText().toString()+" : "+edMortality.getText().toString());
                    List.add(mor);
                }
            }
            if(List.size()!=0){
                list_items.put("hatch_date_move", hatchDate.get(c).getTgl_tetas_nid());
                MortalityData mortData = new MortalityData();
                mortData.setHatch_date(hatch_date[0]);
                mortData.setMortality(List);
                ListMortality.add(mortData);
                list_mortality.add(list_items);
                Log.d("hasil", "add to list");
            }
        }
        for (int b =0; b< ListMortality.size(); b++){
            Log.d("hasil", "hatch_date: "+ListMortality.get(b).getHatch_date());
            for (int a = 0; a<ListMortality.get(b).getMortality().size(); a++){
                Mortality mor = ListMortality.get(b).getMortality().get(a);
                Log.d("hasil", "name: "+mor.getName());
                Log.d("hasil", "value"+mor.getValue());
            }
        }
        //Toast.makeText(getContext(),showallPrompt,Toast.LENGTH_LONG).show();
        for (int c=0; c<list_mortality.size(); c++){
            Log.d("mortality", "list = "+list_mortality.get(c).toString());
        }

        return list_mortality;
    }


    public void  setNote(long id){
        SQLiteDatabase database;
        database= db.getWritableDatabase();
        String selectQuery= "SELECT * FROM daily_recording WHERE id = '"+id+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        String note = "";
        if(cursor.moveToFirst()) {
            note = cursor.getString(cursor.getColumnIndex("note"));
        }
        cursor.close();
        edNote.setText(note);

    }
    //mendapatkan produksi telur yang telah di recording sebelumnya
    public ArrayList<EggValue> getEggProduction(long recording_id, ArrayList<String> eggs){
        ArrayList<EggValue> value = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        for (int i =0; i < eggs.size(); i++){
            EggValue eggValue = new EggValue();
            String selectQuery= "SELECT * FROM recording_production INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id \n" +
                    "where daily_recording_id = '"+recording_id+"' and name = '"+eggs.get(i)+"'";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String egg_number = "";
            String egg_weight = "";
            long ID = 0;
            if(cursor.moveToFirst()) {
                ID = cursor.getLong(cursor.getColumnIndex("id"));
                egg_number = cursor.getString(cursor.getColumnIndex("egg_number"));
                egg_weight = cursor.getString(cursor.getColumnIndex("egg_weight"));
                Log.d("zzzs","dicatat masuk if");
            }
            Log.d("zzzs", eggs.get(i)+" = "+ egg_number);
            eggValue.setId(ID);
            eggValue.setName(eggs.get(i));
            eggValue.setValue_eggs(egg_number);
            eggValue.setValue_weight(egg_weight);
            value.add(eggValue);
            cursor.close();
        }
        return value;
    }
    //set nilai kedalam edit text produksi telur
    public void setEggProduction(){
        int childCount = eggsView.getChildCount();
        Log.d("Layerfarm","child count : "+childCount);
        HashMap<String, String> production_recording = DailyRecordingManager.getInstance().getDailyRecording().getProduction();
        ArrayList<RecordingEntry.Production> production_entry = RecordingEntry.getInstance().getRecording().getProduction();
        for (int c =0; c<childCount;c++){
            View childView = eggsView.getChildAt(c);
            EditText qty_eggs = (EditText)(childView.findViewById(R.id.quality_eggs));
            EditText qty_weight = (EditText) (childView.findViewById(R.id.quality_weight));
            String category_eggs = production_entry.get(c).getName();
            String category_weight = category_eggs+"_weight";
            if(production_recording.containsKey(category_eggs)){
                qty_eggs.setText(production_recording.get(category_eggs));
            }
            if (production_recording.containsKey(category_weight)){
                qty_weight.setText(production_recording.get(category_weight));
            }
        }
    }

    //mendapatkan pakan digunakan yang telah di recording
    public ArrayList<Feed> getFeedConsumption(long recording_id){
        ArrayList<Feed> feeds = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM recording_feed_consumption INNER JOIN feed on recording_feed_consumption.feed_id = feed.id \n" +
                "WHERE daily_recording_id = '"+recording_id+"'";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String name = "";
        String amount = "";
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            Feed pakan = new Feed();
            name = cursor.getString(cursor.getColumnIndex("name"));
            amount = cursor.getString(cursor.getColumnIndex("feed_amount"));
            pakan.setName(name);
            pakan.setValue(amount);
            feeds.add(pakan);
        }
        return feeds;
    }
    //set nilai kedalam edit text konsumsi pakan
    public void setFeedConstumption(){
        ArrayList<DailyRecordingFeed> feeds = DailyRecordingManager.getInstance().getDailyRecording().getFeed();
        HashMap<Integer, String> feed_recording = RecordingEntry.getInstance().getRecording().getOptions_feed();
        for (int i =0; i< feeds.size(); i++){
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View feedView = inflater.inflate(R.layout.more_feed, null);
            // Add the new row before the add field button.
            //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
//            Spinner feed_type = (Spinner)(feedView.findViewById(R.id.feed_type));
            EditText feed_type = (EditText)(feedView.findViewById(R.id.ed_feed_type));
            EditText amount = (EditText)(feedView.findViewById(R.id.total_feed));
//            ArrayAdapter myAdapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, feed_value);
//            feed_type.setAdapter(myAdapt);
//            feed_type.setSelected(true);
            int key = Integer.parseInt(feeds.get(i).getFeed_id());
            String feed_name = feed_recording.get(key);
            Log.d("Layerfarm","feed_name : "+feed_name);
//            int position =myAdapt.getPosition(feed_name);
//            feed_type.setSelection(position);
            feed_type.setText(feed_name);
            amount.setText(feeds.get(i).getAmount());
            more_feed.addView(feedView,more_feed.getChildCount()-2);
        }
    }

    //mendapatkan ovk yang telah digunakan di recording
    public ArrayList<Medicine> getOVKConsumption(long recording_id){
        ArrayList<Medicine> medicines = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        String selectQuery= "SELECT * FROM recording_medication_vaccination INNER JOIN medication_vaccination \n" +
                "on medication_vaccination.id = recording_medication_vaccination.medication_vaccination_id \n" +
                "WHERE daily_recording_id = '"+recording_id+"'";
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        String name = "";
        String amount = "";
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            Medicine obat = new Medicine();
            name = cursor.getString(cursor.getColumnIndex("product_name"));
            amount = cursor.getString(cursor.getColumnIndex("amount_used"));
            obat.setName(name);
            obat.setValue(amount);
            medicines.add(obat);
        }
        return medicines;
    }
    //set nilai kedalam edit text penggunaan obat2an
    public void setOVKConsumption(){
        ArrayList<DailyRecordingOvk> ovk = DailyRecordingManager.getInstance().getDailyRecording().getOvk();
        HashMap<Integer, String> medivac_type = RecordingEntry.getInstance().getRecording().getOptions_medivac();
        for (int i =0; i< ovk.size(); i++){
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View medicineView = inflater.inflate(R.layout.more_medicine,null);
            // Add the new row before the add field button.
            //parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
//            Spinner medication_name = (Spinner)(medicineView.findViewById(R.id.medication_name));
            EditText medication_name = (EditText)(medicineView.findViewById(R.id.ed_medvac_type));
            EditText total_medicine = (EditText)(medicineView.findViewById(R.id.total_medicine));
//            ArrayAdapter myAdapt = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, ovk_value);
//            medication_name.setAdapter(myAdapt);
//            medication_name.setSelected(true);
            int key = Integer.parseInt(ovk.get(i).getOvk_id());
            String name = medivac_type.get(key);
//            int position =myAdapt.getPosition(name);
            medication_name.setText(name);
            Log.d("Layerfarm","ovk_name : "+name);
//            medication_name.setSelection(position);
            total_medicine.setText(ovk.get(i).getAmount());
            Log.d("Layerfarm","ovk_name : "+ovk.get(i).getAmount());
            more_medicine.addView(medicineView, more_medicine.getChildCount()-2);
        }
    }

    //mendapatkan nilai mortality untuk masing2 hatch date
    public ArrayList<MortalityData> getRecordingMortality(long recording_id, ArrayList<HatchDate> hatch){
        ArrayList<MortalityData> mortalityData = new ArrayList<>();
        SQLiteDatabase dbase = db.getReadableDatabase();
        for (int a =0; a < hatch.size(); a++){
            MortalityData data = new MortalityData();
            data.setHatch_date(hatch.get(a).getHatchDate());
            String selectQuery= "SELECT * FROM recording_mortality INNER JOIN chick_in on recording_mortality.chick_in_id = chick_in.id INNER JOIN\n" +
                    "mortality_category on recording_mortality.mortality_category_id = mortality_category.id\n" +
                    "WHERE recording_mortality.daily_recording_id = '"+recording_id+"' AND chick_in.hatch_date = '"+hatch.get(a).getHatchDate()+"'";
            Cursor cursor = dbase.rawQuery(selectQuery, null);
            String name = "";
            String amount = "";
            ArrayList<Mortality> mortalities = new ArrayList<>();
            for (int i=0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Mortality mor = new Mortality();
                name = cursor.getString(cursor.getColumnIndex("name"));
                amount = cursor.getString(cursor.getColumnIndex("number_of_bird"));
                mor.setName(name);
                mor.setValue(amount);
                mortalities.add(mor);
            }
            data.setMortality(mortalities);
            mortalityData.add(data);
        }
        return mortalityData;
    }
    //set nilai kedalam edit text kematian
    public void setRecordingMortality(){
        ArrayList<LinkedHashMap<String, String>> mortality_recording = DailyRecordingManager.getInstance().getDailyRecording().getMortality();
//        HashMap<String, String> mor_category = RecordingEntry.getInstance().getRecording().getMortality_type();
        LinkedHashMap<String, String> mortality_category = RecordingEntry.getInstance().getRecording().getMortality_type();;
        Log.d("mortality","mor_category = "+mortality_category.toString());
//        Log.d("zzzs","mortality.size() = "+mortality_recording.size());
        int childCount = mortalityView.getChildCount();
//        Log.d("zzzs","childCount = "+childCount);

        for (int c =0; c<childCount;c++){
            View childView = mortalityView.getChildAt(c);
            TextView hatch = (TextView) (childView.findViewById(R.id.hatch_date));
            String[] split_txt = hatch.getText().toString().split(" ");
            String[] hatch_date = split_txt[1].split("\\|");
            int mortalitychild = mortalityItemsView.getChildCount();
            ViewGroup child = childView.findViewById(R.id.mortality_items);
            String hatch_nid = hatchDate.get(c).getTgl_tetas_nid();
            Log.d("mortality","hatch nid = "+hatch_nid);

            for (int b =0 ; b < mortality_recording.size(); b++){
                if(mortality_recording.get(b).containsValue(hatch_nid)){
                    for (int i =0; i< child.getChildCount(); i++){
                        View childMortality = child.getChildAt(i);
                        TextView txMortality = (TextView) (childMortality.findViewById(R.id.mortality_name));
                        EditText edMortality = (EditText) (childMortality.findViewById(R.id.mortality_value));
                        String keys = txMortality.getText().toString();
                        String name = getKey(mortality_category, keys);
                        Log.d("mortality","keys = "+keys);
                        Log.d("mortality","name = "+name);
                        if(mortality_recording.get(b).containsKey(name)){
                            edMortality.setText(mortality_recording.get(b).get(name));
                        }
                    }
                }
            }
        }
    }
    public void insert(long id){
//        String note = null;
//        ArrayList<EggValue> list_egg = getListEggs();
//        ArrayList<MortalityData> list_mortality = getListMortality();
//        ArrayList<Feed> list_feed = getListFeed();
//        ArrayList<Medicine> list_medicine = getListMedicine();
//        if(edNote.getText()!=null){
//            //memasukkan kedalam group recording
//            note = edNote.getText().toString();
//        }
//        if (!note.isEmpty()) {
//            dataSource.create_note(id, note);
//        }
//        if (hatchDate.size() != 0){
//            dataSource.createDaily_recording_chick_in(hatchDate,id);
//        }
//        if (list_feed.size()!=0){
//            dataSource.createFeed(list_feed,id);
//        }
//        if (list_medicine.size()!=0){
//            dataSource.createMedicine(list_medicine,id);
//        }
//        if (list_egg.size()!=0){
//            dataSource.createEggValue(list_egg,id);
//        }
//        if (list_mortality.size()!=0){
//            dataSource.createMortality(list_mortality, id);
//            String origin = "MOR"+id;
//            int harga = 0;
//            int total = 0;
//            for (int i=0; i < list_mortality.size(); i++){
//                ArrayList<Mortality> mor = list_mortality.get(i).getMortality();
//                for (int a =0 ; a< mor.size(); a++){
//                    total += Integer.parseInt(mor.get(a).getValue());
//                }
//            }
//            dataSource.create_stock_move(origin,harga,total,flock_id,"Mortality", "true");
//            dataSource.create_stock_picking(origin,flock_id,"Mortality",flock_name);
//        }
//        //membuat di table cache
//        for (int i =0; i< hatchDate.size(); i++){
//            dataSource.create_cache_total_bird(flock_id,recordingDate,id,hatchDate.get(i).getHatchDate());
//        }
//        dataSource.createCacheMortality(id);
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            HashMap<String, String> production = getListEggs();
            String note = edNote.getText().toString();
            final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
            args.setHouseId(flock_id);
            args.setRecordingDate(recordingDate);
            args.setProduction(production);
            args.setOvk(getListMedicine());
            args.setFeed(getListFeed());
            args.setMortality(getListMortality());
            args.setNote(note);
            args.setForceDate("TRUE");
            DailyRecordingManager.getInstance().setDailyRecording(args);
        }catch (Exception e){

        }
    }

    public <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
