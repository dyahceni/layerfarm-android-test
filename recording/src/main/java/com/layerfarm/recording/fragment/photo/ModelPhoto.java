package com.layerfarm.recording.fragment.photo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ModelPhoto {

    private String title, description, path;
    private long datetimeLong,id_recording;
    private SimpleDateFormat df = new SimpleDateFormat("MMMM d, yy  h:mm");

    public ModelPhoto(String title, String description, String path,
                   long datetimeLong, long id_recording) {
        this.title = title;
        this.description = description;
        this.path = path;
        this.datetimeLong = datetimeLong;
        this.id_recording = id_recording;
    }

    public ModelPhoto() {
    }

    public String getTitle() { return title; }

    public Calendar getDatetime() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datetimeLong);
        return cal;
    }

    public void setDatetime(long datetimeLong) {
        this.datetimeLong = datetimeLong;
    }

    public void setDatetime(Calendar datetime) {
        this.datetimeLong = datetime.getTimeInMillis();
    }

    public String getDescription() { return description; }

    public void setTitle(String title) { this.title = title; }

    public long getDatetimeLong() { return datetimeLong; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPath(String path) { this.path = path; }

    public String getPath() { return path; }

    public long getId_recording() { return id_recording; }

    public void setId_recording(long id_recording) { this.id_recording = id_recording; }

    /*
    @Override public String toString() {
        return "Title:" + title + "   " + df.format(getDatetime().getTime()) +
                "\nDescription:" + description;
    }
    */
    @Override public String toString() {
        return df.format(getDatetime().getTime()) +
                "\nDescription:" + description;
    }
}
