package com.layerfarm.recording.fragment.bodyweight;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.layerfarm.recording.R;
import com.layerfarm.recording.model.BodyWeight;

import java.util.List;

public class AdapterBodyWeight extends BaseAdapter{

    private Activity activity;
    private LayoutInflater inflater;
    private List<BodyWeight> items;

    public AdapterBodyWeight(Activity activity, List<BodyWeight> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_body_weight_row, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView daily_recording_id = (TextView) convertView.findViewById(R.id.daily_recording_id);
        TextView body_weight = (TextView) convertView.findViewById(R.id.body_weight);

        BodyWeight bodyWeightModel = items.get(position);

        //id.setText(bodyWeightModel.getId());
        daily_recording_id.setText("Daily Recording ID : "+bodyWeightModel.getDaily_recording_id());
        body_weight.setText("Body Weight : "+bodyWeightModel.getBody_weight());

        return convertView;
    }
}
