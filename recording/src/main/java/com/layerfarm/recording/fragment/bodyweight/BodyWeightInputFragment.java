package com.layerfarm.recording.fragment.bodyweight;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DailyRecording;
import com.layerfarm.layerfarm.model.DailyRecordingBodyWeight;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.model.SubmitDailyRecording;
import com.layerfarm.layerfarm.model.SubmitDailyRecordingBw;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.model.DailyRecordingManager;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BodyWeightInputFragment extends android.support.v4.app.Fragment {

    protected Cursor cursor;

    DataHelper SQLite = new DataHelper(getActivity());
    DatabaseHelper database = new DatabaseHelper(getContext());
    Button saveButton;
    EditText bodyWeightEditText;
    Spinner spDailyRecording;
    private ProgressBar pgsBar;
    public String[] daily_recording_value;

    private String flock_id, flock_name, flock_type, flock_period, location_name, location_id, status;
    String recordingDate;
    private DBDataSource dataSource;

    View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_body_weight_input, container, false);

        dataSource =  new DBDataSource(getContext());
        dataSource.open();

        bodyWeightEditText = (EditText) view.findViewById(R.id.bodyWeightEditText);
        saveButton = (Button) view.findViewById(com.layerfarm.setting.R.id.saveButton);
        pgsBar = (ProgressBar) view.findViewById(R.id.pBar);

        recordingDate = getArguments().getString("DATE");
        flock_id = getArguments().getString("id_flock");
        flock_name = getArguments().getString("name_flock");
        flock_type = getArguments().getString("type");
        flock_period = getArguments().getString("period");
        location_name = getArguments().getString("loc_name");
        location_id = getArguments().getString("loc_id");
        status = getArguments().getString("status");

        //jika sudah ada isinya
        if (DailyRecordingManager.getInstance().getBodyWeight().getBodyWeight().length != 0){
            DailyRecordingBodyWeight a = DailyRecordingManager.getInstance().getBodyWeight();
            String[] bw_data = a.getBodyWeight();
            String val = TextUtils.join(" ",bw_data);
            bodyWeightEditText.setText(val);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                pgsBar.setVisibility(arg0.VISIBLE);
                save();
                //getActivity().finish();


            }
        });

        return view;
    }

    // Make blank all Edit Text
    private void blank() {
        bodyWeightEditText.requestFocus();
    }

    // Save data to SQLite database
    private void save() {



        if (String.valueOf(bodyWeightEditText.getText()).equals(null) || String.valueOf(bodyWeightEditText.getText()).equals("")) {
            Toast.makeText(getActivity().getApplicationContext(),
                    "Please input body weight first ...", Toast.LENGTH_SHORT).show();
        } else {

        /*
        String data = bodyWeightEditText.getText().toString().trim();
        String[] items = data.split("-");
        for (String item : items)
        {
            SQLite.insertBodyWeight(1,(Float.parseFloat(item)));
        }
        */

//            SQLite = new DataHelper(getActivity());
//            SQLiteDatabase db = database.getWritableDatabase();
//            //get last record daily recording
//            String selectQuery = "SELECT id FROM daily_recording ORDER BY id DESC LIMIT 1";
//            Cursor cursor = db.rawQuery(selectQuery, null);
//            String strlastdailyrecording = "";
//            if (cursor.moveToFirst())
//                strlastdailyrecording = cursor.getString(cursor.getColumnIndex("id"));
//            cursor.close();


            String data = bodyWeightEditText.getText().toString();
            final String[] bw = data.split(" ");
            //            SQLite.insertBodyWeightData(strlastdailyrecording, data);

            String module = "layerfarm";
            String function_name = "layerfarm_save_daily_recording";

//        DailyRecording.getInstance().setHouseId(flock_id);
//        DailyRecording.getInstance().setRecordingDate(recordingDate);
//        DailyRecording dailyRecording = new DailyRecording(flock_id,recordingDate,production);
//        args.setProduction(production);
//        DailyRecording.getInstance().setProduction(production);

            final DailyRecording args = DailyRecordingManager.getInstance().getDailyRecording();
            args.setHouseId(flock_id);
            args.setRecordingDate(recordingDate);
            args.setForceDate("TRUE");
            DailyRecordingManager.getInstance().setDailyRecording(args);
            SubmitDailyRecording parameter = new SubmitDailyRecording(module, function_name, args);


            ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
            String token2 = RetrofitData.getInstance().getToken2();
            Call<String[]> call = apiInterfaceJson.submitDailyRecording(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
            call.enqueue(new Callback<String[]>() {
                @Override
                public void onResponse(Call<String[]> call, Response<String[]> response) {
                    String[] recording = response.body();
                    if (recording[0].matches("[0-9]+")){
                        String module = "bw_recording";
                        String function_name = "bw_recording_save_body_weight";
                        final DailyRecordingBodyWeight args = DailyRecordingManager.getInstance().getBodyWeight();

                        args.setRecordingDate(recordingDate);
                        args.setHouseId(flock_id);
                        args.setBodyWeight(bw);

                        DailyRecordingManager.getInstance().setBodyWeight(args);
                        SubmitDailyRecordingBw parameter = new SubmitDailyRecordingBw(module, function_name, args);

                        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
                        String token2 = RetrofitData.getInstance().getToken2();
                        Call<String[]> callbw = apiInterfaceJson.submitDailyRecordingBw(token2, parameter);
                        callbw.enqueue(new Callback<String[]>() {
                            @Override
                            public void onResponse(Call<String[]> call, Response<String[]> response) {
                                String[] recording = response.body();
                                pgsBar.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), "Entry Body Weight Successfully nid: "+ recording[0], Toast.LENGTH_LONG).show();
                                BodyWeightMainFragment fragment = new BodyWeightMainFragment();
                                Bundle arguments = new Bundle();
                                arguments.putString("DATE",recordingDate);
                                arguments.putString("id_flock",flock_id);
                                arguments.putString("name_flock",flock_name);
                                arguments.putString("type",flock_type);
                                arguments.putString("period", flock_period);
                                arguments.putString("loc_name", location_name);
                                arguments.putString("loc_id", location_id);
                                arguments.putString("status",status);
                                fragment.setArguments(arguments);
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_test, fragment);
                                //fragmentTransaction.addToBackStack(null);
                                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                fragmentTransaction.commit();
                            }

                            @Override
                            public void onFailure(Call<String[]> call, Throwable t) {
                                String alert = t.getMessage();
                                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                                pgsBar.setVisibility(View.GONE);
//                    dataSource.create_recording(recordingDate,flock_id,args);
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<String[]> call, Throwable t) {
                    String alert = t.getMessage();
                    Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                    dataSource.create_recording(recordingDate,flock_id,args);
                }
            });

        }
        //getActivity().finish();
        //}
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            String data = bodyWeightEditText.getText().toString();
            final String[] bw = data.split(" ");
            final DailyRecordingBodyWeight args = DailyRecordingManager.getInstance().getBodyWeight();

            args.setRecordingDate(recordingDate);
            args.setHouseId(flock_id);
            args.setBodyWeight(bw);

            DailyRecordingManager.getInstance().setBodyWeight(args);
        }catch (Exception e){

        }

    }
}
