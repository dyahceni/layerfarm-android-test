package com.layerfarm.recording.activity_option;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


import com.layerfarm.layerfarm.model.RecordingEntry;
import com.layerfarm.recording.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class OptionFeed extends AppCompatActivity {

    private static OptionFeed staticInstance;
    EditText search;
    ListView product_list;

    // Listview Data
    String products[] = {"Ayam", "Telur", "Obat2an", "Pakan", "Jagung",};
    ArrayList<String> feed_value = new ArrayList<>();

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    public static OptionFeed getInstance(){
        Log.d("product_activity","static= "+staticInstance);
        if (staticInstance == null){
            staticInstance = new OptionFeed();
        }
        return staticInstance;
    }
    private static String product_items;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option_feed);
        search = (EditText) findViewById(R.id.search);
        product_list = (ListView) findViewById(R.id.items);

        //fungsi memasukkan feed ke dalam array yg kemudian digunakan di spinner
        Collection<String> feed = RecordingEntry.getInstance().getRecording().getOptions_feed().values();
        feed_value = new ArrayList<>(feed);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, feed_value);
        product_list.setAdapter(adapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // When user changed the Text
                OptionFeed.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        product_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = product_list.getItemAtPosition(position).toString();
                setProduct(s);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",s);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
//                finish();
            }
        });
    }
    public void setProduct(String product){
        this.product_items = product;
    }
    public String getProduct(){
        return product_items;
    }
}
