package com.layerfarm.recording.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.recording.R;
import com.layerfarm.recording.fragment.MainFragment;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.recording.model.History;

import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    ArrayList<History> data;
    String status;
    public HistoryAdapter(ArrayList<History> items, String status){
        data = items;
        this.status = status;
//        status = status;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txHistory;
        TextView txDate;
        CardView card;
        public ViewHolder(View itemView) {
            super(itemView);
            txHistory = (TextView) itemView.findViewById(R.id.history_rv);
            card = (CardView) itemView.findViewById(R.id.date_history);
            txDate = (TextView) itemView.findViewById(R.id.history_date);
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_rv, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String date = data.get(position).getDate();
        ArrayList<Flock> flock_data = data.get(position).getFlock_data();
        final String text = data.get(position).getData();
        final String location_name = flock_data.get(0).getLocation_name();
        final String location_id = flock_data.get(0).getLocation_id();
        final String flock_id = flock_data.get(0).getFlock_id();
        final String flock_name = flock_data.get(0).getFlock_name();
        final String flock_type = flock_data.get(0).getType();
        final String flock_period = flock_data.get(0).getPeriod();
        final String nid = data.get(position).getRecording_id();
        Log.d("Layerfarm", "position : "+position);
        Log.d("Layerfarm","nid history adapter : "+nid);
        Log.d("Layerfarm", "txDate : "+date);
        holder.txDate.setText(date);
        holder.txHistory.setText(text);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Context context = view.getContext();
                Intent i = new Intent(context, MainFragment.class);
                i.putExtra("DATE", date);
                i.putExtra("nid", nid);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status",status);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
