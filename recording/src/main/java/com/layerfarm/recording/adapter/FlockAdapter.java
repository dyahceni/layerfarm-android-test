package com.layerfarm.recording.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layerfarm.recording.R;
import com.layerfarm.recording.activity.RecordingSubmenu;
import com.layerfarm.recording.fragment.MainFragment;
import com.layerfarm.recording.model.Flock;

import java.util.ArrayList;

public class FlockAdapter extends RecyclerView.Adapter<FlockAdapter.ViewHolder> {
    private ArrayList<Flock> flock_items;
    private String loc_id="loc_id";
    private String loc_name = "loc_name";
    private String id_flock = "id_flock";
    private String name_flock = "name_flock";
    private String type = "type";
    private String period = "period";

    public FlockAdapter(ArrayList<Flock> inputData){
        flock_items = inputData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView kandang;
        public TextView jenis;
        public CardView card;
        public ViewHolder(View v) {
            super(v);
            kandang = (TextView) v.findViewById(R.id.kandang);
            jenis = (TextView) v.findViewById(R.id.jenis);
            card = (CardView) v.findViewById(R.id.cv_flock);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // membuat view baru
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.flock, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        final String flock_name = flock_items.get(position).getFlock_name();
        final String flock_id = flock_items.get(position).getFlock_id();
        final String flock_type = flock_items.get(position).getType();
        final String flock_period = flock_items.get(position).getPeriod();
        final String location_id = flock_items.get(position).getLocation_id();
        final String location_name = flock_items.get(position).getLocation_name();
        final String status = "add";

        Log.d("flock_id","flock:"+flock_id);
        Log.d("flock_id","name:"+flock_name);
        Log.d("flock_id","flock_type:"+flock_type);
        Log.d("flock_id","flock_period:"+flock_period);
        Log.d("flock_id","location_id:"+location_id);
        Log.d("flock_id","location_name:"+location_name);

        holder.kandang.setText(flock_name);
        holder.jenis.setText(flock_type+" "+flock_period);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("pencet", "ini dipencet = "+position);
                // TODO Auto-generated method stub
                Context context = view.getContext();
//                Intent i = new Intent(context, MainFragment.class);
                Intent i = new Intent(context, RecordingSubmenu.class);
                i.putExtra("loc_name", location_name);
                i.putExtra("loc_id", location_id);
                i.putExtra("id_flock",flock_id);
                i.putExtra("name_flock",flock_name);
                i.putExtra("type",flock_type);
                i.putExtra("period",flock_period);
                i.putExtra("status", status);
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return flock_items.size();
    }
}
