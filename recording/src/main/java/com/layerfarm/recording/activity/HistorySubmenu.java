package com.layerfarm.recording.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RecordingHistory;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.adapter.HistoryAdapter;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.helper.helper;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.recording.model.HatchDate;
import com.layerfarm.recording.model.History;
import com.layerfarm.recording.model.TransferIn;
import com.layerfarm.recording.model.TransferOut;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistorySubmenu extends Activity {
    String flock_id;
    String flock_name;
    String flock_type;
    String flock_period;
    String location_id;
    String location_name;
    String nid;
    TextView location, flock;

    helper help;
    DatabaseHelper dbase;
    private DBDataSource dataSource;

    ArrayList<String> data;
    ArrayList<Flock> flock_data = new ArrayList<>();

    private RecyclerView rvHistory;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar progressBar;
    Boolean isScrolling = false;
    Boolean isLoading = false;
    int currentItems, totalItems, scrollOutItems, prevItems;
    ArrayList<History> history = new ArrayList<>();
    String TAG = "Layerfarm";
    int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_submenu);

        help = new helper(this);
        help.open();
        dbase = new DatabaseHelper(this);
        dataSource =  new DBDataSource(this);
        dataSource.open();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
//        setSupportActionBar(toolbar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText("Recording History");
        Bundle extras = getIntent().getExtras();
        flock_id = extras.getString("id_flock");
        flock_name = extras.getString("name_flock");
        flock_type = extras.getString("type");
        flock_period = extras.getString("period");
        location_id = extras.getString("loc_id");
        location_name = extras.getString("loc_name");


        Flock data_flock =new Flock();
        data_flock.setFlock_id(flock_id);
        data_flock.setFlock_name(flock_name);
        data_flock.setType(flock_type);
        data_flock.setPeriod(flock_period);
        data_flock.setLocation_id(location_id);
        data_flock.setLocation_name(location_name);
        flock_data.add(data_flock);

        location = (TextView) findViewById(R.id.location_title);
        flock = (TextView) findViewById(R.id.flock_title);
        progressBar = (ProgressBar) findViewById(R.id.progress_circular) ;

        location.setText(location_name);
        flock.setText(flock_name);

        rvHistory = (RecyclerView) findViewById(R.id.content);
        rvHistory.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(layoutManager);
        adapter = new HistoryAdapter(history,"edit");
//        adapter.notifyDataSetChanged();
        rvHistory.setAdapter(adapter);

        fetchData();

        rvHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
//                    Log.d(TAG, "Scrolling...");
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
//                Log.d(TAG,"currentitems = "+currentItems);
                Log.d(TAG,"totalitems = "+totalItems+" dy= "+dy+" dx= "+dx);
//                Log.d(TAG, "scrollOutitems = "+scrollOutItems);
                if (!isLoading){
//                    isLoading = false;
//                    prevItems = totalItems;

                    if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == history.size() - 1){

//                    Log.d(TAG, "Scrolling...");
                        isScrolling = false;
                        fetchData();
                    }
                }

//                if (isScrolling && (currentItems + scrollOutItems == totalItems)){
//                    Log.d(TAG, "Scrolling...");
//                    isScrolling = false;
//                    fetchData();
//                }
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                page = 0;
                history.clear();
                fetchData();
//                Insert();
                Log.d("zzzs","HISTORY LAGI REFRESH");
            }
        });


    }
    private void fetchData(){
        progressBar.setVisibility(View.VISIBLE);
        isLoading = true;
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_recording_history";
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        String[] args = {flock_id, Integer.toString(page)};
        Parameter parameter = new Parameter(module,function_name, args);
        Call<RecordingHistory> call = apiInterfaceJson.RecordingHistory(token2, parameter);
//        Log.d("Layerfarm","args : "+parameter.getArgs().getHouseId());
        call.enqueue(new Callback<RecordingHistory>() {
            @Override
            public void onResponse(Call<RecordingHistory> call, Response<RecordingHistory> response) {
                RecordingHistory recording = response.body();
                ArrayList<String> header = recording.getHeader();
                LinkedHashMap<String, ArrayList<String>> data = recording.getRows();
                Log.d(TAG, "data: "+data.toString());
                if (!data.isEmpty()){
                    for (Map.Entry<String, ArrayList<String>> entry : data.entrySet()) {
                        nid = entry.getKey();

                        ArrayList<String> value = entry.getValue();
                        History hist = new History();
                        hist.setDate(value.get(0));
                        hist.setFlock_data(flock_data);
                        String content = "";
                        for (int i=1; i< value.size(); i++){
                            String str = value.get(i);
                            str = str.replaceAll("</br>","\n");
                            content += header.get(i)+ " : "+str+"\n";
                        }
                        hist.setRecording_id(entry.getKey());
                        hist.setData(content);
                        history.add(hist);
//                    adapter.notifyDataSetChanged();
                    }
//                    adapter = new HistoryAdapter(history,"edit");
                    adapter.notifyDataSetChanged();
//                    rvHistory.setAdapter(adapter);
                    page++;
                }
                else {
                    Toast.makeText(HistorySubmenu.this, "no more page",Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"no more page can load");
                }
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }

            @Override
            public void onFailure(Call<RecordingHistory> call, Throwable t) {
                String alert = t.getMessage();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                AskOption();
            }
        });
    }
    public String[][] getRecordingProduction(String id){

        SQLiteDatabase db = dbase.getReadableDatabase();
        String Query= "SELECT * from daily_recording INNER JOIN recording_production on daily_recording.id = recording_production.daily_recording_id\n" +
                "INNER JOIN egg_quality on egg_quality.id = recording_production.egg_quality_id\n" +
                "where daily_recording.id = '"+id+"'";
        Cursor cursor = db.rawQuery(Query, null);
        cursor.moveToFirst();
        String[][] production = new String[cursor.getCount()][3];
        //index 0 merupakan nama dari egg quality
        //index 1 jumlah berdasarkan butiran telur
        //index 2 jumlah berdasarkan berat telur
        for (int i=0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            production[i][0] = cursor.getString(cursor.getColumnIndex("name"));
            production[i][1] = cursor.getString(cursor.getColumnIndex("egg_number"));
            production[i][2] = cursor.getString(cursor.getColumnIndex("egg_weight"));
        }
        return production;
    }
    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Connection Error. Please try again later.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void close(View view){
        finish();
    }
}
