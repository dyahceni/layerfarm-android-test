package com.layerfarm.recording.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.adapter.FlockAdapter;
import com.layerfarm.recording.adapter.HistoryFlockAdapter;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.recording.helper.*;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity {
    helper help;
    DatabaseHelper dbase;
    private Spinner spn_location;
    private DBDataSource dataSource;
    ArrayList <Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    TabLayout mTabLayout;
    String flock_name;
    String location_name;
    ArrayList<String> location = new ArrayList<>();
    DashboardGrowerModel.GrowerPerformanceDetail resp;
    int tab_position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        help = new helper(this);
        help.open();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
//        setSupportActionBar(toolbar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.title);
        toolbar_title.setText("Recording History");

        dbase = new DatabaseHelper(this);
        dataSource =  new DBDataSource(this);
        dataSource.open();
        sync();

//        ArrayList<String> location = dataSource.getLocation("");
        spn_location = (Spinner) findViewById(R.id.find_location);
//        spn_location.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, location));
//        spn_location.setSelected(true);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onTabTapped(tab_position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mTabLayout = (TabLayout)findViewById(com.layerfarm.setting.R.id.tabs);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabTapped(
                        tab.getPosition());
                tab_position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabTapped(tab.getPosition());
            }
        });
        rvFlock = (RecyclerView) findViewById(R.id.rv_flock_history);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);


    }
    private void onTabTapped(int position) {
        switch (position) {
            case 0:
                SubmitActive();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(this, "Active clicked", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                SubmitInactive();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(this, "Inactive clicked", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                SubmitAll();
                adapter.notifyDataSetChanged();
//                search.setText("");
                Toast.makeText(this, "All clicked", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Tapped " + position, Toast.LENGTH_SHORT);
        }

    }
    public void SubmitAll(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            Flock flock = new Flock();
            flock.setFlock_id(items.get(i).getFlock_nid());
            flock.setFlock_name(items.get(i).getFlock_name());
            flock.setLocation_id(items.get(i).getFarm_nid());
            flock.setType(items.get(i).getFlock_type());
            flock.setPeriod(items.get(i).getFlock_period());
            flock.setStatus(items.get(i).getFlock_status());
            flock.setLocation_name(items.get(i).getFarm_name());
            flock_items.add(flock);
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void SubmitActive(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            if (items.get(i).getFlock_status().equalsIgnoreCase("active")){
                Flock flock = new Flock();
                flock.setFlock_id(items.get(i).getFlock_nid());
                flock.setFlock_name(items.get(i).getFlock_name());
                flock.setLocation_id(items.get(i).getFarm_nid());
                flock.setType(items.get(i).getFlock_type());
                flock.setPeriod(items.get(i).getFlock_period());
                flock.setStatus(items.get(i).getFlock_status());
                flock.setLocation_name(items.get(i).getFarm_name());
                flock_items.add(flock);
            }
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void SubmitInactive(){
        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        String loc_id = dataSource.getLocation(location).get(0);
//        Log.d("zzz","loc_id : "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase db = dbase.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = db.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
//            cursor.moveToPosition(i);
//            Log.d("zzz","name flock : "+cursor.getString(cursor.getColumnIndex("name")));
            if (items.get(i).getFlock_status().equalsIgnoreCase("inactive")){
                Flock flock = new Flock();
                flock.setFlock_id(items.get(i).getFlock_nid());
                flock.setFlock_name(items.get(i).getFlock_name());
                flock.setLocation_id(items.get(i).getFarm_nid());
                flock.setType(items.get(i).getFlock_type());
                flock.setPeriod(items.get(i).getFlock_period());
                flock.setStatus(items.get(i).getFlock_status());
                flock.setLocation_name(items.get(i).getFarm_name());
                flock_items.add(flock);
            }
        }
        adapter = new HistoryFlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }
    public void sync(){
        final ProgressDialog progress = new ProgressDialog(HistoryActivity.this);
//                                progress.setTitle("Loading");
//        progress.setMessage("Loading...");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        progress.setContentView(R.layout.custom_progressdialog);
        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_selection_flock";
        String[] args = {};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.getFlockLocationSelection(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                progress.dismiss();
                resp = response.body();
                if (resp.getLocation_flock() != null && resp.getLocation_flock().size()>0) {
                    Set<String> keySet = resp.getLocation_flock().keySet();
                    location = new ArrayList<>(keySet);
                    spn_location.setAdapter(new ArrayAdapter(HistoryActivity.this, android.R.layout.simple_spinner_dropdown_item, location));
                    spn_location.setSelected(true);
                }
            }
            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                progress.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                AskOption();
            }
        });

    }
    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Connection Error. Please try again later.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void close(View view){
        finish();
    }
}
