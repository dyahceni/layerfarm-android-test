package com.layerfarm.recording.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.layerfarm.layerfarm.model.DashboardGrowerModel;
import com.layerfarm.layerfarm.model.Parameter;
import com.layerfarm.layerfarm.model.RetrofitData;
import com.layerfarm.layerfarm.service.ApiInterface;
import com.layerfarm.recording.R;
import com.layerfarm.recording.adapter.FlockAdapter;
import com.layerfarm.recording.database.DBDataSource;
import com.layerfarm.recording.model.Flock;
import com.layerfarm.setting.DataHelper;
import com.layerfarm.layerfarm.DatabaseHelper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RecordingMainActivity extends Activity {
    DatabaseHelper db;
    private Spinner spn_location;
    private DBDataSource dataSource;
    ArrayList <Flock> flock_items;
    private RecyclerView rvFlock;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> location = new ArrayList<>();
    DashboardGrowerModel.GrowerPerformanceDetail resp;
    ScrollView content_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording_main);

        dataSource =  new DBDataSource(this);
        dataSource.open();

        db = new DatabaseHelper(this);
        content_view = (ScrollView)findViewById(R.id.content_view);
        sync();

        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar_main);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.title);
        mTitle.setText("Recording");

        spn_location = (Spinner) findViewById(R.id.find_location);

        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Layerfarm","testing");
                Submit();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rvFlock = (RecyclerView) findViewById(R.id.rv_flock);
        rvFlock.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvFlock.setLayoutManager(layoutManager);


    }

    public void Submit(){

        String location = spn_location.getSelectedItem().toString();

        ArrayList<DashboardGrowerModel.GrowerPerformanceDetailLocationFlock> items = resp.getLocation_flock().get(location);

//        adapter = new DashboardGrowerPerformanceDetailPopulationAdapter(items, index);
//        rvFlock.setAdapter(adapter);

//        String location = spn_location.getSelectedItem().toString();
        //mendapatkan id dari location tersebut
        //jika terdapat paramater berupa nama lokasi pada method getLocation() maka hasil output berupa id dari nama lokasi tsb
        //dan hanya memberikan 1 buah output dengan index 0
//        Log.d("Layerfarm","location = "+location);
//        String loc_id = items.get()dataSource.getLocation(location).get(0);
//        Log.d("Layerfarm","loc_id = "+loc_id);

        //mencari flock dengan id location tersebut
        flock_items = new ArrayList();
//        SQLiteDatabase dbase = db.getReadableDatabase();
//        Cursor cursor= null;
//        cursor = dbase.rawQuery("SELECT * FROM flock where location_id = '"+loc_id+"'",null);
//        cursor.moveToFirst();
        for (int i=0; i < items.size(); i++){
            Flock flock = new Flock();
            flock.setFlock_id(items.get(i).getFlock_nid());
            flock.setFlock_name(items.get(i).getFlock_name());
            flock.setLocation_id(items.get(i).getFarm_nid());
            flock.setType(items.get(i).getFlock_type());
            flock.setPeriod(items.get(i).getFlock_period());
            flock.setStatus(items.get(i).getFlock_status());
            flock.setLocation_name(items.get(i).getFarm_name());
            flock_items.add(flock);
        }
        adapter = new FlockAdapter(flock_items);
        rvFlock.setAdapter(adapter);
    }

    public void sync(){
        final ProgressDialog progress = new ProgressDialog(RecordingMainActivity.this);
//                                progress.setTitle("Loading");
//        progress.setMessage("Loading...");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        progress.setContentView(R.layout.custom_progressdialog);

        String module = "layerfarm_android";
        String function_name = "layerfarm_android_get_selection_flock";
        String[] args = {"Recording"};
        Parameter parameter = new Parameter(module, function_name, args);
        ApiInterface apiInterfaceJson = RetrofitData.getInstance().getApiInterfaceJson();
        String token2 = RetrofitData.getInstance().getToken2();
        Call<DashboardGrowerModel.GrowerPerformanceDetail> call = apiInterfaceJson.getFlockLocationSelection(token2, parameter);

        call.enqueue(new Callback<DashboardGrowerModel.GrowerPerformanceDetail>() {
            @Override
            public void onResponse(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Response<DashboardGrowerModel.GrowerPerformanceDetail> response) {
                progress.dismiss();
                resp = response.body();
                Log.d("resp", "resp = "+resp);
                if (resp.getLocation_flock() != null && resp.getLocation_flock().size()>0) {
                    content_view.setVisibility(View.VISIBLE);
                    Set<String> keySet = resp.getLocation_flock().keySet();
                    location = new ArrayList<>(keySet);
                    spn_location.setAdapter(new ArrayAdapter(RecordingMainActivity.this, android.R.layout.simple_spinner_dropdown_item, location));
                    spn_location.setSelected(true);
                }
            }
            @Override
            public void onFailure(Call<DashboardGrowerModel.GrowerPerformanceDetail> call, Throwable t) {
                progress.dismiss();
                Log.d("Layerfarm", "Flocks failure = " + t.getMessage());
                AskOption();
            }
        });

    }
    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setTitle("Warning!!")
                .setMessage("Connection Error. Please try again later.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
        return myQuittingDialogBox;
    }
    public void close(View view){
        finish();
//        Intent intent = new Intent(this, PricelistMainActivity.class);
//        startActivity(intent);
    }
}
